<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Locationwiseservices extends Model
{
    protected $table = 'locationwiseservices';

    protected $fillable = ['locationid','serviceid','businessid' ];
				   
	public function business()
    {
        return $this->hasMany('App\models\Business', 'businessid');
    }
	public function services()
    {
        return $this->hasMany('App\models\Services', 'serviceid');
    }
	public function locations()
    {
        return $this->hasMany('App\models\Locations', 'locationid');
    }
}
