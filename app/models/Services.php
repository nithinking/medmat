<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Services extends Model
{
    protected $table = 'services';

    protected $fillable = ['servicename'];
				   
	
}
