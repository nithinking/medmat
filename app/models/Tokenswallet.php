<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Tokenswallet extends Model
{
    use HasApiTokens;
    use Notifiable;
	
    protected $table = 'tokenswallet';

    protected $fillable = ['userid','createdby','tokenimage','tokenbackimage','prescriptionid','isused','repeats','tokenstatus' ];
						   
	public function users()
    {
        return $this->belongsTo('App\User', 'userid','userid');
    }	
	public function prescriptiondetails()
    {
        return $this->hasMany('App\models\Prescriptiondetails', 'prescriptiondetails','prescriptionid');
    }			
}
