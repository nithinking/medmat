<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Locationdeliverytimings extends Model
{
    protected $table = 'locationdeliverytimings';

    protected $fillable = ['locationid','dayoftheweek','businessid','deliverywindow' ];
				   
	public function business()
    {
        return $this->belongsTo('App\models\Business', 'businessid');
    }	
	public function locations()
    {
        return $this->belongsTo('App\models\Locations', 'locationid');
    }
}
