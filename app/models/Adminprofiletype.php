<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Adminprofiletype extends Model
{
    use HasApiTokens;
    use Notifiable;
	
	protected $table = 'adminprofiletype';

    protected $fillable = ['profileid','userid','locationid','businessid' ];
				   
	public function healthprofile()
    {
        return $this->belongsTo('App\models\Healthprofile', 'profileid','profileid');
    }
	public function locations()
    {
        return $this->belongsTo('App\models\Locations', 'locationid','locationid');
    }
	public function business()
    {
        return $this->belongsTo('App\models\Business', 'businessid','businessid');
    }
}
