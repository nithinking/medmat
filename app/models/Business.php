<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Business extends Model
{
    protected $table = 'business';

    protected $fillable = ['businessid','businessname','website','parentbusinessid' ];
				   
		
}
