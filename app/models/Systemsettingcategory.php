<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Systemsettingcategory extends Model
{
     use HasApiTokens;
    use Notifiable;
	
    protected $table = 'systemsettingcategory';
	protected $fillable = ['systemsettingcategoryid','systemsettingcategoryname'];
}
