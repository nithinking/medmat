<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Prescriptionitemdetails extends Model
{
     use HasApiTokens;
    use Notifiable;
	
    protected $table = 'prescriptionitemdetails';

    protected $fillable = ['prescriptionid'];
						   
	public function prescriptiondetails()
    {
        return $this->hasMany('App\models\Prescriptiondetails', 'prescriptiondetails','prescriptionid');
    }	
}
