<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Pathologylocations extends Model
{
   use HasApiTokens;
    use Notifiable;
	
	protected $table = 'pathology_locations';

    protected $fillable = ['locationname','locationaddress','status'];
}
