<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Basket extends Model
{
	use HasApiTokens;
    use Notifiable;
	
    protected $table = 'basket';

    protected $fillable = ['profileid','createddate','scriptid' ];
				   
	public function healthprofile()
    {
        return $this->belongsTo('App\models\Healthprofile', 'profileid','profileid');
    }
	public function prescriptiondetails()
    {
        return $this->belongsTo('App\models\Prescriptiondetails', 'scriptid','prescriptionid');
    }
}
