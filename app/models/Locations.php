<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Locations extends Model
{
    protected $table = 'locations';

    protected $fillable = ['locationid','businessid','locationname','address','phone','website','email','postcode' ];
				   
	public function business()
    {
        return $this->belongsTo('App\models\Business', 'businessid');
    }
}
