<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Prescriptiondetails extends Model
{
    use HasApiTokens;
    use Notifiable;
	
    protected $table = 'prescriptiondetails';

    protected $fillable = ['prescriptionid','userid','prescriptiontype','prescriptionimage','prescriptionbackimage','barcode' ];
						   
	public function user()
    {
        return $this->belongsTo('App\User', 'userid','userid');
    }					   
}
