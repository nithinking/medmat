<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\Users as Authenticatable;
class Users extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
	
	protected $fillable = [
        'username', 'password','firstname','profiletypeid','hashcode','mobile'
    ];
	
	 protected $hidden = [
        'password', 'remember_token','hashcode'
    ];
	
	 protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	protected $primaryKey = 'userid';
	public function AauthAcessToken(){
    return $this->hasMany('\App\OauthAccessToken');
}
