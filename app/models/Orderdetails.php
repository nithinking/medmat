<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Orderdetails extends Model
{
	use HasApiTokens;
    use Notifiable;
	
    protected $table = 'orderdetails';

    protected $fillable = ['profileid','userid','locationid','prescriptionid','deliveryaddressid','orderstatus','basketid','isGeneric','speakwithpharmacist' ];
				   
	public function healthprofile()
    {
        return $this->belongsTo('App\models\Healthprofile', 'profileid','profileid');
    }
	public function locations()
    {
        return $this->belongsTo('App\models\Locations', 'locationid','locationid');
    }
	public function prescriptiondetails()
    {
        return $this->belongsTo('App\models\Prescriptiondetails', 'prescriptionid','prescriptionid');
    }
	public function deliveryaddress()
    {
        return $this->belongsTo('App\models\Deliveryaddress', 'deliveryaddressid','deliveryaddressid');
    }
	public function basket()
    {
        return $this->belongsTo('App\models\Basket', 'basketid','basketid');
    }
	public function user()
    {
        return $this->belongsTo('App\User', 'userid','userid');
    }
}
