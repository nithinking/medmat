<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Healthprofile extends Model
{
    protected $table = 'healthprofile';

    protected $fillable = ['medicarenumber','medicare_exp','ihi','ihi_exp','safetynet','parentuser_flag','profile_relation' ];
				   protected $primaryKey = 'userid';
	public function user()
    {
        return $this->belongsTo('App\User', 'userid','userid');
    }	
}
