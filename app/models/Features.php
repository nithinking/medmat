<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Features extends Model
{
    use HasApiTokens;
    use Notifiable;
	
	protected $table = 'features';

    protected $fillable = ['programid','featurename'];
	
	public function programs()
    {
        return $this->belongsTo('App\models\Programs', 'programid','programid');
    }
}
