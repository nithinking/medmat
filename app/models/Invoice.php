<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Invoice extends Model
{
	use HasApiTokens;
    use Notifiable;
	
    protected $table = 'invoice';

    protected $fillable = ['userid','orderid','profileid'];
				   
	public function healthprofile()
    {
        return $this->belongsTo('App\models\Healthprofile', 'profileid','profileid');
    }
	public function user()
    {
        return $this->belongsTo('App\User', 'userid','userid');
    }
	public function orderdetails()
    {
        return $this->belongsTo('App\models\Orderdetails', 'orderid','orderid');
    }
}
