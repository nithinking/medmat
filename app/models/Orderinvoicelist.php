<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Orderinvoicelist extends Model
{
	use HasApiTokens;
    use Notifiable;
	
    protected $table = 'orderinvoicelist';

    protected $fillable = ['invoiceid','orderid'];
				   
	public function invoice()
    {
        return $this->belongsTo('App\models\Invoice', 'invoiceid','invoiceid');
    }
	
}
