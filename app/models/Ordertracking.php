<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Ordertracking extends Model
{
    use HasApiTokens;
    use Notifiable;
	
    protected $table = 'ordertracking';

    protected $fillable = ['orderid','orderstatus','statuscreatedat','statusupdatedat'];
						   
	
	public function orderdetails()
    {
        return $this->belongsTo('App\models\Orderdetails', 'orderid','orderid');
    }
}
