<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Basketitemlist extends Model
{
    use HasApiTokens;
    use Notifiable;
	
    protected $table = 'basketitemlist';

    protected $fillable = ['basketid'];
						   
	
	public function basket()
    {
        return $this->belongsTo('App\models\Basket', 'basketid','basketid');
    }
}
