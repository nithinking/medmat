<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Orderbasketlist extends Model
{
    use HasApiTokens;
    use Notifiable;
	
    protected $table = 'orderbasketlist';

    protected $fillable = ['orderid','basketid','locationid','deliveryaddressid','prescriptionid'];
						   
	
	public function orderdetails()
    {
        return $this->hasMany('App\models\Orderdetails', 'orderid','orderid');
    }
	public function basket()
    {
        return $this->hasMany('App\models\Basket', 'basketid','basketid');
    }
	public function locations()
    {
        return $this->belongsTo('App\models\Locations', 'locationid','locationid');
    }
	public function deliveryaddress()
    {
        return $this->belongsTo('App\models\Deliveryaddress', 'deliveryaddressid','deliveryaddressid');
    }
	public function prescriptiondetails()
    {
        return $this->hasMany('App\models\Prescriptiondetails', 'prescriptionid','prescriptionid');
    }
}
