<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Deliveryaddress extends Model
{
	use HasApiTokens;
    use Notifiable;
	
    protected $table = 'deliveryaddresses';

    protected $fillable = ['firstname','addressline1','addressline2','suburb','postcode','state','mobile','lattitude','longitude'];
	//protected $primaryKey = 'userid';
	public function users()
    {
        return $this->belongsTo('App\User', 'userid','userid');
    }
}
