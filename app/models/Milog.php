<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Milog extends Model
{
    use HasApiTokens;
    use Notifiable;
	
	protected $table = 'mi_logs';
	
    //protected $fillable = ['profiletypeid','featureid'];
	
	/*public function profiletypes()
    {
        return $this->belongsTo('App\models\Profiletypes', 'profiletypeid','profiletypeid');
    }
	public function features()
    {
        return $this->belongsTo('App\models\Features', 'featureid','featureid');
    }*/
	protected $guarded = array();

    public function settransactionidAttribute($value)
    {
        $this->attributes['transactionid'] = Crypt::encryptString($value);
    }

    public function setscidsourceAttribute($value)
    {
        $this->attributes['scidsource'] = Crypt::encryptString($value);
    }

    public function setscidAttribute($value)
    {
        $this->attributes['scid'] = Crypt::encryptString($value);
    }

    public function setsocihiidAttribute($value)
    {
        $this->attributes['socihiid'] = Crypt::encryptString($value);
    }
	public function setresponseAttribute($value)
    {
        $this->attributes['response'] = Crypt::encryptString($value);
    }
	public function setmedicationbrandnameAttribute($value)
    {
        $this->attributes['medicationbrandname'] = Crypt::encryptString($value);
    }
	public function setquantityAttribute($value)
    {
        $this->attributes['quantity'] = Crypt::encryptString($value);
    }
	public function setstrengthAttribute($value)
    {
        $this->attributes['strength'] = Crypt::encryptString($value);
    }
	public function setrepeatsAttribute($value)
    {
        $this->attributes['repeats'] = Crypt::encryptString($value);
    }
	public function setformAttribute($value)
    {
        $this->attributes['form'] = Crypt::encryptString($value);
    }
	public function setdosageinstructionsAttribute($value)
    {
        $this->attributes['dosageinstructions'] = Crypt::encryptString($value);
    }

    public function gettransaction_idAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }

    public function getscidsourceAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }

    public function getscidAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }

    public function getsocihiidAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }public function getresponseAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }
public function getmedicationbrandnameAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }
public function getquantityAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }
public function getstrengthAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }
public function getrepeatsAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }
public function getformAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }
public function getdosageinstructionsAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
    }


    public function getData()
    {
        return static::orderBy('created_at','desc');
    }

    public function storeData($input)
    {
    	return static::create($input);
    }
}
