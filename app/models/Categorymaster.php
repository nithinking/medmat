<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Categorymaster extends Model
{
   // use HasApiTokens;
   // use Notifiable;
	
    protected $table = 'catalouge_category_master';

    protected $fillable = ['CategoryLv1','categorystatus','vieworder'];
}
