<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Useraccess extends Model
{
    use HasApiTokens;
    use Notifiable;
	
	protected $table = 'useraccess';

    protected $fillable = ['profiletypeid','featureid'];
	
	public function profiletypes()
    {
        return $this->belongsTo('App\models\Profiletypes', 'profiletypeid','profiletypeid');
    }
	public function features()
    {
        return $this->belongsTo('App\models\Features', 'featureid','featureid');
    }
}
