<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 
use App\models\Orderdetails; 
use App\models\Orderprescriptions; 
use App\models\Ordertracking; 
use App\models\Prescriptiondetails; 
use App\models\Orderprescriptionitemdetails; 
use Illuminate\Support\Facades\Auth; 
use Validator;
class OrderDetailstoPharmacyController extends Controller
{
    public function getorderdetails(Request $request,$orderId=null)
	{
		//$orderId = base64_decode($orderId);
		$orderData = Orderdetails:: join ('orderprescriptions','orderprescriptions.orderId', '=', 'orderdetails.orderId')
								  ->join ('orderprescriptionitemdetails','orderprescriptionitemdetails.orderId' , '=' , 'orderdetails.orderId')
								  ->join ('ordertracking','ordertracking.orderId', '=' , 'orderdetails.orderId')								  
								  ->join ('prescriptiondetails','prescriptiondetails.prescriptionId', '=' , 'orderdetails.prescriptionId')								  
								  ->join ('tokenswallet','tokenswallet.prescriptionId', '=' , 'orderdetails.prescriptionId')								  
								  ->join ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								  ->join ('pharmacy','pharmacy.pharmacyId', '=' , 'orderdetails.pharmacyId')
								  ->join ('users','users.id', '=' , 'orderdetails.createdBy')	
								  ->select('orderdetails.*','ordertracking.*','pharmacy.pharmacyName','pharmacy.pharmacyAddress','pharmacy.pharmacyPincode','pharmacy.pharmacyLogo','pharmacy.pharmacyPhone',
								  'prescriptiondetails.userId','prescriptiondetails.prescriptiontype','prescriptiondetails.prescriptionTitle','prescriptiondetails.prescriptionImage'
								  ,'prescriptiondetails.prescriptionBackImage','prescriptiondetails.barcode','tokenswallet.*','deliveryaddresses.*','users.address as defaultaddress','users.mobile as mobilenumber','users.profileImage as myImage','users.email as emailaddress')
								  ->where('orderdetails.orderId' , '=' , $orderId)->get();
		
            
			
		return view('orderdetails',compact('orderData')); 
	}
	
	public function updateOrderStatus(Request $request,$orderId)
	{
		//$orderData = $request->$orderId;
		//echo $request->$orderId;
		echo "madhoo";
		exit;
		//return view('orderdetails',compact('orderData'));
	}
	
}
