<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use DB;

use Illuminate\Support\Facades\Auth; 

class PaymentCallBackController extends Controller {
    public function paymentsuccess(){
        return view('success_new');
    }
    public function paymentfailure(){
        return view('failure_new');
    }
    public function paymentcancel(){
        return view('failure');
    }

}