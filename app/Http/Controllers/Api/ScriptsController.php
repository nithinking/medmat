<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User; 
use App\models\Prescriptiondetails; 
use App\models\Prescriptionitemdetails; 
use App\models\Tokenswallet; 
use App\models\Healthprofile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Sunra\PhpSimple\HtmlDomParser;

class ScriptsController extends Controller
{
    public $successStatus = 200;
	
	/**
      @OA\Post(
          path="/v2/addPrescriptions",
          tags={"Scripts"},
          summary="Add Prescriptions",
          operationId="AddPrescriptions",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="prescriptionuserid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="prescriptiontype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptiontitle",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptionimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptionbackimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="barcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugstrength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugquantity",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="repeats",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugdata",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="prescriptionfor",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="medilistid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 
	public function addPrescriptions(Request $request)
	{
	
		$response = (object)array();
		$user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
		 $healthprofile = Healthprofile::where('userid', $user->userid)->where('parentuser_flag' , '1')->get();
		 $prescription = new Prescriptiondetails;
		 $prescription->userid = $user->userid;
		 $prescription->prescriptiontype = $request->prescriptiontype;
		 $prescription->prescriptiontitle = $request->prescriptiontitle;
		  if($request->prescriptiontype == "1" && $request->barcode !="")
			 {
				 if(strtolower(substr($request->barcode,0,5))=='https')
				 {
					 $code = explode("/",$request->barcode);
					 if($code[2] == "ausscripts-int.medisecure.com.au")
					{					
						$barcode = $code[4];
					}
					else
					{					
						
					if($code[3] == "scripts")
						$barcode = $code[4];
					else
						$barcode = $code[3];
					}
				 }
				 else
				 {
					 $barcode =$request->barcode;
				 }
				
				//$code = explode("/",$request->barcode);
				//https://ausscripts-int.medisecure.com.au/scripts/MPK00009002000607
				//https://egw-etp-int-qrcode-web-au-se.azurewebsites.net/21KR32CTBBKDTY17B1
				
										
			 }
			 else 
			 {
				$barcode = $request->barcode;
			 }
			
				 if((($request->prescriptiontype == "3") && ($request->prescriptionimage !="" && $request->prescriptionbackimage!="") || 
					  ($request->prescriptiontype == "1" || $request->prescriptiontype =="2") && ($request->barcode !="")))
				 {
					
					 
					 if($request->prescriptiontype != "1" && $request->prescriptionimage !="")
					 {
					 $prescriptionfrontimage=$this->base64_to_jpeg($request->prescriptionimage,time().$user->userid."front.jpeg");
					 }
					 else
					 {
						 $prescriptionfrontimage = $request->prescriptionimage;
					 }
					 if($request->prescriptionbackimage != '')
					 {
					 $prescriptionbackimage= $this->base64_to_jpeg($request->prescriptionbackimage,time().$user->userid."back.jpeg");
					 }
					 else
					 {
						 $prescriptionbackimage = '';
					 }
					 if($request->prescriptiontype == "1" || $request->prescriptiontype == "2")
					 {
						
						$prescimg = "https://ausscripts-int.medisecure.com.au/scripts/".$barcode;
						$html = HtmlDomParser::file_get_html($prescimg);
						if(!empty($html->find('h5')))
						{
							//Toget Patient Name
							foreach($html->find('h5') as $pname)
							$patientname = str_replace("Electronic Prescription for ","",strip_tags($pname));
							
							$d = 0;
							$img = array($d => $html->find('img'));
							//Toget QRCode and Barcode
							 foreach($img as $ele => $element){						     
								$medicationdetails['qrcode'] = $element[0]->src; 
								$d++;
							}
						
						$prescriptionfrontimage=$this->qrcodeimage($medicationdetails['qrcode'],time().$user->userid."qrcode.jpeg");
						
						//Drug Details
						$i = 0;
						$val=array($i => $html->find('h3')); 
						foreach($val as  $d  => $drugname)
						{
							
							$drugdetails = explode('-',strip_tags($drugname[$i]));
							$drugnameVal =  trim($drugdetails[0]);
							// $drugform =  trim($drugdetails[1]);
							// $drugstrength =  trim($drugdetails[2]);	
							 $drugform =  '';
							 $drugstrength =  '';
							$quantity=  trim($drugdetails[1]);
							//$repeats =  "0";//str_replace("Supplies Remaining","",trim($drugdetails[4]));
							$prescriptionissuer = "Medisecure";
							if($quantity == "")
							{
								$drugquantity = 0;
								$drugpack = '';
							}
							else
							{
								
								$drugqua = explode('*',strip_tags($quantity));
								if(count($drugqua) == 2)
								{
									
									$qua = trim($drugqua[0]);
									$drugquantity = trim(str_replace("Qty ","",trim($drugqua[0])));									
									$drugpack = $drugqua[1];
								}
								else{
									$drugquantity = $drugqua[0];
									$drugpack = '1';
								}
								}
							
							$i++;
							
							foreach($html->find('h6') as $rpts)
							$repeats = str_replace("Supplies Remaining","",strip_tags($rpts));
						}
						}
						else
						{
							$prescimg = "https://ausscripts.erx.com.au/scripts/".$barcode;
							
							
							$html = HtmlDomParser::file_get_html($prescimg);
							
							//Toget Patient Name
							foreach($html->find('h5') as $pname)
							$patientname = str_replace("Electronic Prescription for ","",strip_tags($pname));
							
							$d = 0;
							$img = array($d => $html->find('img'));
							//Toget QRCode and Barcode
							 foreach($img as $ele => $element){						     
								$medicationdetails['qrcode'] = $element[0]->src; 
								$d++;
							}
							$prescriptionfrontimage=$this->qrcodeimage($medicationdetails['qrcode'],time().$user->userid."qrcode.jpeg");
							
							//Drug Details
						$i = 0;
						$val=array($i => $html->find('h3')); 
						foreach($val as  $d  => $drugname)
						{
							
							$drugdetails = explode(' ',strip_tags($drugname[$i]));
							$drugnameVal =  trim($drugdetails[0]);
							$drugform =  trim($drugdetails[1]);
							$drugstrength =  trim($drugdetails[2]);
							$drugpack = '1';							
							$prescriptionissuer = "ERX";							
							$drugquantity = trim($drugdetails[3]);
							$i++;
							foreach($html->find('h6') as $rpts)
							//$strval = strcmp()
							$repeats = str_replace("Supplies Remaining","",strip_tags($rpts));
							//print_r($repeats[0]);exit;
							$repeats = $repeats[0];
							
						}
						}
						// if($request->medilistid !="")
						// {
							// $getmedicationname = Prescriptionitemdetails :: where('prescriptionitemid','=',$request->medilistid)->get();
							// $medlistmedication = $getmedicationname[0]['drugname'];
							// if($medlistmedication == $drugnameVal)
							// {		
								
							// }
							// else
							// {
								
								 // $response->msg 		= "Medication name is different in the added script. Please check and upload again";
								 // $response->status 		= $this->successStatus;
								 // return json_encode($response);
							// }
						// }
					 }
					 else
					 {
							$drugnameVal =  "Paper Script";
							$drugform =  "";
							$drugstrength =  "";
							$drugquantity=  "0";
							$repeats =  0;
							$drugpack = '';
							$prescriptionissuer = "Medisecure";
							$patientname = "";
					 }
					
					 $prescription->prescriptionimage = $prescriptionfrontimage;
					 $prescription->prescriptionbackimage = $prescriptionbackimage;
					 $prescription->prescriptionstatus = '1';
					 $prescription->barcode = $barcode;
					 $prescription->save();
					 $prescriptionid = $prescription->id;
					 
					 $drugimagedetails = $this->getcmiimages($drugnameVal);
					 
					 if(!empty($drugimagedetails))
					 {
					 foreach ($drugimagedetails as $imagedetails) {
						$medimage   = (!empty($imagedetails->PhotoID)) ? $imagedetails->PhotoID : '';
						$mimsimage   = (!empty($imagedetails->image)) ? $imagedetails->image : '';
						$cmicode   = (!empty($imagedetails->cmi)) ? $imagedetails->cmi : '';
						
						//print_r($imagedetails);exit;
					 }
					 }
					 else{
						 $medimage   = '';
						$mimsimage   = '';
						$cmicode   = '';
					 }
					 //print_r($drugimagedetails[0]['drug']);exit;
					 if($prescriptionid!="")
					 {
						 $prescriptionItems = new Prescriptionitemdetails;
						 $prescriptionItems->prescriptionid = $prescriptionid;
						 $prescriptionItems->prescriptionuserid = $request->prescriptionuserid;
						 $prescriptionItems->profileid = $request->profileid;
						 $prescriptionItems->drugname = $drugnameVal;
						 $prescriptionItems->drugform = $drugform;
						 $prescriptionItems->drugstrength = $drugstrength;
						 $prescriptionItems->drugquantity = $drugquantity;
						 $prescriptionItems->drugpack = $drugpack;
						 $prescriptionItems->repeats = $repeats;
						 $prescriptionItems->referrencetable = "";
						 $prescriptionItems->drugdata = "Medmate";
						 $prescriptionItems->prescriptionfor = $patientname;
						 $prescriptionItems->prescriptionissuer = $prescriptionissuer;
						 
						
						if($request->medilistid !="")
						{
							$prescriptionItems->medilistid = $request->medilistid;
							
							$prescriptionItems->ismedlist = "1";
							$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', \DB::raw('"'.$request->medilistid.'"'))
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.ismedlist' => 0]);
															
							
						}
						else
						{
							$prescriptionItems->medilistid ="";
							if($request->prescriptiontype == "1" || $request->prescriptiontype == "2")
							{
							 $prescriptionItems->ismedlist = "0";
							}
							else{
								$prescriptionItems->ismedlist = "1";
							}
						}
						$prescriptionItems->medstatus = "medlist";
						$prescriptionItems->medimage = $medimage;
						$prescriptionItems->mimsimage = $mimsimage;						
						$prescriptionItems->isscript = '1';
						$prescriptionItems->cmicode = $cmicode;
						
						 $prescriptionItems->save();
						 
						 $scriptsInfo = new Tokenswallet;
						 $scriptsInfo->userid = $user->userid;
						 $scriptsInfo->createdby = $user->userid;
						 $scriptsInfo->tokenimage = $prescriptionfrontimage;
						 $scriptsInfo->tokenbackimage = $prescriptionbackimage;
						 $scriptsInfo->prescriptionid = $prescriptionid;
						 $scriptsInfo->isused = 0;
						 $scriptsInfo->repeats = 0;
						 $scriptsInfo->tokenstatus = 1;
						 $scriptsInfo->save();
						 $scriptsInfo->prescriptiontype = $request->prescriptiontype;
						 $scriptsInfo->prescriptiontitle = $request->prescriptiontitle;
						 
						 
						 
						 $scriptsInfo->barcode = $barcode;
						
							$response->scriptsInfo 	= $scriptsInfo;
							$response->prescriptionItems 	=  $prescriptionItems;
								
							$response->msg 		= trans('messages.prescription_Add');
							$response->status 		= $this->successStatus;
						// return response()->json(['scriptsInfo' => $scriptsInfo], $this->successStatus);
					 
					 }
				 }
				 else
				 {
					 $response->msg 		= trans('messages.prescription_err');
						$response->status 		= $this->successStatus;
				 }
			 //}
			 //else
				// {
					// $response->msg 		= "This prescription is already available. Please check and upload correct prescription.";
					// $response->status 		= $this->successStatus;
				// }
		return json_encode($response); 		 
		 
	}
	
	/**
      @OA\Get(
          path="/v2/getPrescriptions",
          tags={"Scripts"},
          summary="Get Prescriptions",
          operationId="GetPrescriptions",
		  security={{"bearerAuth": {}} },
      
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function getPrescriptions()
	 {
		 
		 $user = Auth::user();
		/* $prescriptionDetails = Prescriptiondetails ::join('tokenswallet','tokenswallet.prescriptionid', '=' ,'prescriptiondetails.prescriptionid')	
		                                             //->select('Prescriptiondetails.*','prescriptionitemdetails.*','tokenswallet.*')
		                                             ->select('prescriptiondetails.prescriptionid','prescriptiondetails.userid','prescriptiondetails.prescriptiontitle','prescriptiondetails.prescriptiontype','prescriptiondetails.prescriptionimage','prescriptiondetails.prescriptionbackimage','prescriptiondetails.barcode','tokenswallet.*')
													 ->where('prescriptiondetails.userid', '=', $user->userid)
													 ->where('tokenswallet.createdby', '=', $user->userid)
													 ->where('tokenswallet.tokenstatus', '=', '1')
                                                     ->get();*/
        $prescriptionDetails = DB::table('prescriptiondetails as P')
                                ->select('P.prescriptionid','P.userid','P.prescriptiontitle','P.prescriptiontype','P.prescriptionimage','P.prescriptionbackimage','P.barcode','T.*','PI.*')
                                ->join('tokenswallet as T','T.prescriptionid','=','P.prescriptionid')
                                ->join('prescriptionitemdetails as PI','PI.prescriptionid','=','P.prescriptionid')
                                ->where('P.userid', '=', $user->userid)
								->where('T.createdby', '=', $user->userid)
								->where('P.prescriptionstatus', '=', '1')
								->where('T.tokenstatus', '=', '1')
                                ->get();
                                for($i=0;$i<count($prescriptionDetails);$i++){
                                    $prescriptionDetails[$i]->prescriptionimage=url('/prescriptions/'.$prescriptionDetails[$i]->prescriptionimage);
                                    $prescriptionDetails[$i]->prescriptionbackimage=url('/prescriptions/'.$prescriptionDetails[$i]->prescriptionbackimage);

                                }
		 return response()->json(['prescriptionDetails' => $prescriptionDetails], $this->successStatus); 
     }
     public function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
    
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $base64_string ) );

        $file = public_path('prescriptions/'). $output_file;
        file_put_contents($file, $ifp);
        rename($output_file, 'prescriptions/'.$output_file);
        // clean up the file resource
        fclose( $ifp ); 
        return $output_file; 
    }
		
	/**
      @OA\post(
          path="/v2/removePrescription",
          tags={"Scripts"},
          summary="Remove Prescription",
          operationId="RemovePrescription",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="prescriptionid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function removePrescription(Request $request)
	 {
		 
		 $user = Auth::user();
		
		
        $prescriptionDetails = Prescriptiondetails :: where('prescriptiondetails.prescriptionid', '=', $request->prescriptionid)
														  ->where('prescriptiondetails.userid', '=', $request->userid)
															->update(['prescriptiondetails.prescriptionstatus' => '0']);
															
		$prescriptionDetails = Tokenswallet :: where('tokenswallet.prescriptionid', '=', $request->prescriptionid)
													->where('tokenswallet.userid', '=', $request->userid)
													->update(['tokenswallet.tokenstatus' => '0']);
		
		$success['message'] = trans('messages.remove_prescription');
		 return response()->json(['success' => $success], $this->successStatus); 
     }
	/**
      @OA\Post(
          path="/v2/addtomedlist",
          tags={"MedList"},
          summary="Add to Medlist",
          operationId="Add to Medlist",
		  security={{"bearerAuth": {}} },
      
          
         @OA\Parameter(
              name="prescriptionuserid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Parameter(
              name="drugname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugstrength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugquantity",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="repeats",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="referrence",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="PhotoID",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        @OA\Parameter(
              name="mimsimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="drugpack",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        
          @OA\Parameter(
              name="script",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),  
          @OA\Parameter(
              name="price",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="cmicode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */

		public function addtomedlist(Request $request)
		{
	
			$response = (object)array();
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			
			$prescriptionItems = new Prescriptionitemdetails;
			$prescriptionItems->prescriptionid = $request->prescriptionid;
			$prescriptionItems->prescriptionuserid = $request->prescriptionuserid;
			$prescriptionItems->profileid = $request->profileid;
			$prescriptionItems->drugname = $request->drugname;
			$prescriptionItems->drugform = $request->drugform;
			$prescriptionItems->drugstrength = $request->drugstrength;
			
			if($request->drugquantity!="")
			$prescriptionItems->drugquantity = $request->drugquantity;
			else
			$prescriptionItems->drugquantity = 1;	
			
			if($request->drugpack!="")
			$prescriptionItems->drugpack = $request->drugpack;
			else
			$prescriptionItems->drugpack = 1;
		
			if($request->price!="")
			$prescriptionItems->price = $request->price;
			else
			$prescriptionItems->price = 0.00;
		
			$prescriptionItems->repeats = $request->repeats;
			$prescriptionItems->referrencetable = $request->referrence;
			$prescriptionItems->drugdata = "Medmate";
			$prescriptionItems->prescriptionfor = $user->firstname;
			
			$prescriptionItems->medimage = $request->PhotoID;
			$prescriptionItems->mimsimage = $request->mimsimage;
			$prescriptionItems->isscript = $request->script;
			$prescriptionItems->cmicode = $request->cmicode;
			
			if($request->prescriptionid == "")
			$prescriptionItems->prescriptionissuer = 'OTC';
			else
			$prescriptionItems->prescriptionissuer = 'Medisecure';		
			$prescriptionItems->ismedlist = "1";
			
			if($request->script == "1")
			$prescriptionItems->medstatus = "Add Your Script";
			else
			$prescriptionItems->medstatus = "medlist";
		
			$prescriptionItems->medimage = "";
			$prescriptionItems->save();
			
			
			$response->Medmatelist 	=  $prescriptionItems;							
			$response->msg 		= trans('messages.add_medlist');
			$response->status 		= $this->successStatus;
			
			return json_encode($response); 	
		}
		/**
			@OA\Get(
			  path="/v2/getmedlist",
			  tags={"MedList"},
			  summary="Get MedList",
			  operationId="Get MedList",
			  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
			 @OA\Response(
				  response=200,
				  description="Success",
				  @OA\MediaType(
					  mediaType="application/json",
				  )
			  ),
			  @OA\Response(
				  response=401,
				  description="Unauthorized"
			  ),
			  @OA\Response(
				  response=400,
				  description="Invalid request"
			  ),
			  @OA\Response(
				  response=404,
				  description="not found"
			  ),
		  )
		 */
		public function getmedlist(Request $request)
		{
			 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			$getmedicationdetails = Prescriptionitemdetails :: join('healthprofile','healthprofile.profileid','=','prescriptionitemdetails.profileid')
																->where('healthprofile.profileid', '=', $request->profileid)
																->where('prescriptionitemdetails.ismedlist', '=', '1')
																->get();
			$noofrows = $getmedicationdetails->count();
						
				for($i=0; $i < $noofrows ; $i++)
				{	
					if($getmedicationdetails[$i]['prescriptionid']!="")
					{
					$prescriptiondetails[$i] = Prescriptiondetails :: where('prescriptiondetails.prescriptionid', '=',  $getmedicationdetails[$i]['prescriptionid'] )
																		->select('prescriptiondetails.prescriptiontype','prescriptiondetails.prescriptionimage')
																		->get();
					$getmedicationdetails[$i]['prescriptiontype']	= 	$prescriptiondetails[$i];
					}
					if($getmedicationdetails[$i]['medilistid']!="")
					{
						$medilistdetails[$i] = Prescriptionitemdetails :: where('prescriptionitemdetails.medilistid' ,'=' ,$getmedicationdetails[$i]['medilistid'] )
																		  ->where('prescriptionitemdetails.ismedlist', '=', '1')
																		->get();
						$count = $medilistdetails[$i]->count();
						
						for($j=0;$j<$count;$j++)
						{
							//print_r($medilistdetails[$i][$j]['prescriptionid']);exit;
							$medilistItems[$j] = Prescriptiondetails :: join('prescriptionitemdetails','prescriptionitemdetails.prescriptionid' ,'prescriptiondetails.prescriptionid')
																	->where('prescriptiondetails.prescriptionid',   $medilistdetails[$i][$j]['prescriptionid'] )
																	->where('prescriptionitemdetails.prescriptionid',   $medilistdetails[$i][$j]['prescriptionid'] )
																	->get();
							
							$getmedicationdetails[$i]['medilistItems']	= 	$medilistItems[$j];
						}
						
																		
					}
				}
			return response()->json(['getmedicationdetails' => $getmedicationdetails], $this->successStatus);
		}
		/**
			@OA\post(
			  path="/v2/removemedlist",
			  tags={"MedList"},
			  summary="Remove MedList",
			  operationId="Remove MedList",
			  security={{"bearerAuth": {}} },
		  
			@OA\Parameter(
				  name="prescriptionitemid",
				  in="query",
				  required=true,
				  @OA\Schema(
					  type="string"
				  )
			  ),
			@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
			),    
        
			 @OA\Response(
				  response=200,
				  description="Success",
				  @OA\MediaType(
					  mediaType="application/json",
				  )
			  ),
			  @OA\Response(
				  response=401,
				  description="Unauthorized"
			  ),
			  @OA\Response(
				  response=400,
				  description="Invalid request"
			  ),
			  @OA\Response(
				  response=404,
				  description="not found"
			  ),
		  )
		 */
		public function removemedlist(Request $request)
		{
			 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			$medicationdetails = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $request->prescriptionitemid)
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.ismedlist' => '0']);
			$success['message']= trans('messages.remove_medlist');
			$success['medicationdetails'] = $medicationdetails;														
			return response()->json(['success' => $success], $this->successStatus);
		}
		
		/**
			@OA\post(
			  path="/v2/editrepeatsinmedlist",
			  tags={"MedList"},
			  summary="Edit MedList",
			  operationId="Edit MedList",
			  security={{"bearerAuth": {}} },
		  
			@OA\Parameter(
				  name="prescriptionitemid",
				  in="query",
				  required=true,
				  @OA\Schema(
					  type="string"
				  )
			  ),
			@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
			),    
			@OA\Parameter(
              name="repeats",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
			),    
        
			 @OA\Response(
				  response=200,
				  description="Success",
				  @OA\MediaType(
					  mediaType="application/json",
				  )
			  ),
			  @OA\Response(
				  response=401,
				  description="Unauthorized"
			  ),
			  @OA\Response(
				  response=400,
				  description="Invalid request"
			  ),
			  @OA\Response(
				  response=404,
				  description="not found"
			  ),
		  )
		 */
		public function editrepeatsinmedlist(Request $request)
		{
			 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			$medicationdetails = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $request->prescriptionitemid)
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.repeats' => $request->repeats]);
			$success['message']= trans('messages.repeats_edit_medlist');
			$success['medicationdetails'] = $medicationdetails;													
																
			return response()->json(['success' => $success], $this->successStatus);
		}
		
		private function getcmiimages($drugname)
		{
			
			//print_r($drugname);exit;
			$catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,PI.image from catalouge_master as C left join packitemimages as PI on C.MIMsProductCode=PI.prodcode where C.MimsFormCode=PI.formcode and C.MimsPackCode=PI.packcode and C.itemNameDisplay = '".$drugname."'");                
			
			if(count($catalouge_data) > 0)
			{
				return $catalouge_data;
			}
			else{
				
				$mims_data=DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode`  left join packitemimages as PI on P.prodcode=PI.prodcode where  P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode  and `P`.`ActualProduct` ='".$drugname."'");
				
				return $mims_data;
			}
		}
		
		public function qrcodeimage($base64_string ,$output_file) {
		// open the output file for writing
		//$output_file="qrcode.jpeg";
		//$base64_string="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABLeSURBVHhe7ZJBkttAEsT2/5/e5YHiphiQMeWqlih6EIFLB5HdI/s/G/+9swY177QLbaaroTtvJh7eRoOad9qFNtPV0J03Ew9vo0HNO+1Cm+lq6M6biYe30aDmnXahzXQ1dOfNxMPbaFDzTrvQZroauvNm4uFtNKh5p11oM10N3Xkz8fA2GtS80y60ma6G7ryZeHgbDWreaRfaTFdDd95MPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDw0qJnUoCbtQpupQU1Fg5pJDWpSg5pJDWpO4uGhQc2kBjVpF9pMDWoqGtRMalCTGtRMalBzEg8PDWomNahJu9BmalBT0aBmUoOa1KBmUoOak3h4aFAzqUFN2oU2U4OaigY1kxrUpAY1kxrUnMTDQ4OaSQ1q0i60mRrUVDSomdSgJjWomdSg5iQeHhrUTGpQk3ahzdSgpqJBzaQGNalBzaQGNSfx8NCgZlKDmrQLbaYGNRUNaiY1qEkNaiY1qDmJh4cGNZMa1KRdaDM1qKloUDOpQU1qUDOpQc1JPDw0qJnUoCY1qJm0C22+U4Oa1KBmUoOak3h4aFAzqUFNalAzaRfafKcGNalBzaQGNSfx8NCgZlKDmtSgZtIutPlODWpSg5pJDWpO4uGhQc2kBjWpQc2kXWjznRrUpAY1kxrUnMTDQ4OaSQ1qUoOaSbvQ5js1qEkNaiY1qDmJh4cGNZMa1KQGNZN2oc13alCTGtRMalBzEg8PDWomNahJDWom7UKb79SgJjWomdSg5iQeHhrUTGpQkxrUTNqFNt+pQU1qUDOpQc1JPDw0qJnUoCY1qKloUDNpF9pMDWpSg5pJDWpO4uGhQc2kBjWpQU1Fg5pJu9BmalCTGtRMalBzEg8PDWomNahJDWoqGtRM2oU2U4Oa1KBmUoOak3h4aFAzqUFNalBT0aBm0i60mRrUpAY1kxrUnMTDQ4OaSQ1qUoOaigY1k3ahzdSgJjWomdSg5iQeHhrUTGpQkxrUVDSombQLbaYGNalBzaQGNSfx8NCgZlKDmtSgpqJBzaRdaDM1qEkNaiY1qDmJh4cGNZMa1KQGNRUNaibtQpupQU1qUDOpQc1JPDy8OvTm1KDmmzSoSbvQZnp16M0n8fDw6tCbU4Oab9KgJu1Cm+nVoTefxMPDq0NvTg1qvkmDmrQLbaZXh958Eg8Prw69OTWo+SYNatIutJleHXrzSTw8vDr05tSg5ps0qEm70GZ6dejNJ/Hw8OrQm1ODmm/SoCbtQpvp1aE3n8TDw6tDb04Nar5Jg5q0C22mV4fefBIPD68OvTk1qPkmDWrSLrSZXh1680k8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcTDy8jQY1qUFNalCTGtSkBjWpQc3NxMPbaFCTGtSkBjWpQU1qUJMa1NxMPLyNBjWpQU1qUJMa1KQGNalBzc3Ew9toUJMa1KQGNalBTWpQkxrU3Ew8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcyv3v/OUF+KOFBjVpF9qs+Muf+f2FBPpPlRrUpF1os+Ivf+b3FxLoP1VqUJN2oc2Kv/yZ319IoP9UqUFN2oU2K/7yZ35/IYH+U6UGNWkX2qz4y5/5/YUE+k+VGtSkXWiz4i9/5vcXEug/VWpQk3ahzYq//JnfX0ig/1SpQU3ahTYr/vJnlv9C9I+SdqHNtAttVuxCm5Ouhu6c1KCm5L6zDLw07EKbaRfarNiFNiddDd05qUFNyX1nGXhp2IU20y60WbELbU66GrpzUoOakvvOMvDSsAttpl1os2IX2px0NXTnpAY1JfedZeClYRfaTLvQZsUutDnpaujOSQ1qSu47y8BLwy60mXahzYpdaHPS1dCdkxrUlNx3loGXhl1oM+1CmxW70Oakq6E7JzWoKbnvLAMvDbvQZtqFNit2oc1JV0N3TmpQUxQPxzSoST8NvSk1qKnYhTYnNahJDWomNbZvOJzSoCb9NPSm1KCmYhfanNSgJjWomdTYvuFwSoOa9NPQm1KDmopdaHNSg5rUoGZSY/uGwykNatJPQ29KDWoqdqHNSQ1qUoOaSY3tGw6nNKhJPw29KTWoqdiFNic1qEkNaiY1tm84nNKgJv009KbUoKZiF9qc1KAmNaiZ1Ni+4XBKg5r009CbUoOail1oc1KDmtSgZlJj+4bDKQ1q0k9Db0oNaip2oc1JDWpSg5pJDf2CRtNPQ29KDWpSg5oraVCTGtRMuhq688n9u5dgFH4aelNqUJMa1FxJg5rUoGbS1dCdT+7fvQSj8NPQm1KDmtSg5koa1KQGNZOuhu58cv/uJRiFn4belBrUpAY1V9KgJjWomXQ1dOeT+3cvwSj8NPSm1KAmNai5kgY1qUHNpKuhO5/cv3sJRuGnoTelBjWpQc2VNKhJDWomXQ3d+eT+3UswCj8NvSk1qEkNaq6kQU1qUDPpaujOJ/fvXoJR+GnoTalBTWpQcyUNalKDmklXQ3c+uX/3EowKGtSkV4feXHE1dGfFLrQ5qUFNyX3nJRgVNKhJrw69ueJq6M6KXWhzUoOakvvOSzAqaFCTXh16c8XV0J0Vu9DmpAY1Jfedl2BU0KAmvTr05oqroTsrdqHNSQ1qSu47L8GooEFNenXozRVXQ3dW7EKbkxrUlNx3XoJRQYOa9OrQmyuuhu6s2IU2JzWoKbnvvASjggY16dWhN1dcDd1ZsQttTmpQU3LfeQlGBQ1q0qtDb664GrqzYhfanNSgpuS+swy8NOxCm+mnoTdNalCTfhp6U2pQM+p+zzLw0rALbaafht40qUFN+mnoTalBzaj7PcvAS8MutJl+GnrTpAY16aehN6UGNaPu9ywDLw270Gb6aehNkxrUpJ+G3pQa1Iy637MMvDTsQpvpp6E3TWpQk34aelNqUDPqfs8y8NKwC22mn4beNKlBTfpp6E2pQc2o+z3LwEvDLrSZfhp606QGNemnoTelBjWj7vcsAy8Nu9Bm+mnoTZMa1KSfht6UGtSMut/z1+DoG+1Cm2kX2pzUoGZSg5rUoKaiQc2T+3d/DY6+0S60mXahzUkNaiY1qEkNaioa1Dy5f/fX4Ogb7UKbaRfanNSgZlKDmtSgpqJBzZP7d38Njr7RLrSZdqHNSQ1qJjWoSQ1qKhrUPLl/99fg6BvtQptpF9qc1KBmUoOa1KCmokHNk/t3fw2OvtEutJl2oc1JDWomNahJDWoqGtQ8uX/31+DoG+1Cm2kX2pzUoGZSg5rUoKaiQc2T+3d/DY6+0S60mXahzUkNaiY1qEkNaioa1JzEw8MutPlODWoqGtSk3w79TZMa1KQGNSfx8LALbb5Tg5qKBjXpt0N/06QGNalBzUk8POxCm+/UoKaiQU367dDfNKlBTWpQcxIPD7vQ5js1qKloUJN+O/Q3TWpQkxrUnMTDwy60+U4Naioa1KTfDv1NkxrUpAY1J/HwsAttvlODmooGNem3Q3/TpAY1qUHNSTw87EKb79SgpqJBTfrt0N80qUFNalBzEg8Pu9DmOzWoqWhQk3479DdNalCTGtQ8uX/3EozCLrSZdqHNdDV056QGNWkX2nynXWjzyf27l2AUdqHNtAttpquhOyc1qEm70OY77UKbT+7fvQSjsAttpl1oM10N3TmpQU3ahTbfaRfafHL/7iUYhV1oM+1Cm+lq6M5JDWrSLrT5TrvQ5pP7dy/BKOxCm2kX2kxXQ3dOalCTdqHNd9qFNp/cv3sJRmEX2ky70Ga6GrpzUoOatAttvtMutPnk/t1LMAq70GbahTbT1dCdkxrUpF1o8512oc0n9+9eglHYhTbTLrSZrobunNSgJu1Cm++0C22exMMfa1CTdqHNO2lQkxrUXMkutJka2zcc/lSDmrQLbd5Jg5rUoOZKdqHN1Ni+4fCnGtSkXWjzThrUpAY1V7ILbabG9g2HP9WgJu1Cm3fSoCY1qLmSXWgzNbZvOPypBjVpF9q8kwY1qUHNlexCm6mxfcPhTzWoSbvQ5p00qEkNaq5kF9pMje0bDn+qQU3ahTbvpEFNalBzJbvQZmps33D4Uw1q0i60eScNalKDmivZhTZTo/+CL4d+tLQLbVY0qJl0NXRnRYOakvvOPwv+KGEX2qxoUDPpaujOigY1Jfedfxb8UcIutFnRoGbS1dCdFQ1qSu47/yz4o4RdaLOiQc2kq6E7KxrUlNx3/lnwRwm70GZFg5pJV0N3VjSoKbnv/LPgjxJ2oc2KBjWTroburGhQU3Lf+WfBHyXsQpsVDWomXQ3dWdGgpuS+88+CP0rYhTYrGtRMuhq6s6JBTVE8vI1daLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHr6LXWizokFNxS60mRrUpAY1qUFN2mXb4OG72IU2KxrUVOxCm6lBTWpQkxrUpF22DR6+i11os6JBTcUutJka1KQGNalBTdpl2+Dhu9iFNisa1FTsQpupQU1qUJMa1KRdtg0evotdaLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHn54dejNaRfarGhQk66G7kxXQ3emBjUVje0bDh9eHXpz2oU2KxrUpKuhO9PV0J2pQU1FY/uGw4dXh96cdqHNigY16WroznQ1dGdqUFPR2L7h8OHVoTenXWizokFNuhq6M10N3Zka1FQ0tm84fHh16M1pF9qsaFCTrobuTFdDd6YGNRWN7RsOH14denPahTYrGtSkq6E709XQnalBTUVj+4bDh1eH3px2oc2KBjXpaujOdDV0Z2pQU9HYvuHw4dWhN6ddaLOiQU26GrozXQ3dmRrUVDS2bzh8aFAzqUFN2oU20y60mXahzW/SoKaisX3D4UODmkkNatIutJl2oc20C21+kwY1FY3tGw4fGtRMalCTdqHNtAttpl1o85s0qKlobN9w+NCgZlKDmrQLbaZdaDPtQpvfpEFNRWP7hsOHBjWTGtSkXWgz7UKbaRfa/CYNaioa2zccPjSomdSgJu1Cm2kX2ky70OY3aVBT0di+4fChQc2kBjVpF9pMu9Bm2oU2v0mDmorG9g2HDw1qJjWoSbvQZtqFNtMutPlNGtRUNLZvOHxoUDOpQU1qUJMa1KQGNd+kQU1qUDOpsX3D4UODmkkNalKDmtSgJjWo+SYNalKDmkmN7RsOHxrUTGpQkxrUpAY1qUHNN2lQkxrUTGps33D40KBmUoOa1KAmNahJDWq+SYOa1KBmUmP7hsOHBjWTGtSkBjWpQU1qUPNNGtSkBjWTGts3HD40qJnUoCY1qEkNalKDmm/SoCY1qJnU2L7h8KFBzaQGNalBTWpQkxrUfJMGNalBzaTG9g2HDw1qJjWoSQ1qUoOa1KDmmzSoSQ1qJjW2bzh8aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4eHXozWkX2qy4GrqzokHNlexCmyfx8PDq0JvTLrRZcTV0Z0WDmivZhTZP4uHh1aE3p11os+Jq6M6KBjVXsgttnsTDw6tDb0670GbF1dCdFQ1qrmQX2jyJh4dXh96cdqHNiquhOysa1FzJLrR5Eg8Prw69Oe1CmxVXQ3dWNKi5kl1o8yQeHl4denPahTYrroburGhQcyW70OZJPDy8OvTmtAttVlwN3VnRoOZKdqHNk3h4G7vQZtqFNlODmtSgJl0N3Zka1KQGNSfx8DZ2oc20C22mBjWpQU26GrozNahJDWpO4uFt7EKbaRfaTA1qUoOadDV0Z2pQkxrUnMTD29iFNtMutJka1KQGNelq6M7UoCY1qDmJh7exC22mXWgzNahJDWrS1dCdqUFNalBzEg9vYxfaTLvQZmpQkxrUpKuhO1ODmtSg5iQe3sYutJl2oc3UoCY1qElXQ3emBjWpQc1JPLyNXWgz7UKbqUFNalCTrobuTA1qUoOa//uf//4PH72d67VqytUAAAAASUVORK5CYII=";
		//$base64_string="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABLeSURBVHhe7ZJBkttAEsT2/5/e5YHiphiQMeWqlih6EIFLB5HdI/s/G/&#x2B;9swY177QLbaaroTtvJh7eRoOad9qFNtPV0J03Ew9vo0HNO&#x2B;1Cm&#x2B;lq6M6biYe30aDmnXahzXQ1dOfNxMPbaFDzTrvQZroauvNm4uFtNKh5p11oM10N3Xkz8fA2GtS80y60ma6G7ryZeHgbDWreaRfaTFdDd95MPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDw0qJnUoCbtQpupQU1Fg5pJDWpSg5pJDWpO4uGhQc2kBjVpF9pMDWoqGtRMalCTGtRMalBzEg8PDWomNahJu9BmalBT0aBmUoOa1KBmUoOak3h4aFAzqUFN2oU2U4OaigY1kxrUpAY1kxrUnMTDQ4OaSQ1q0i60mRrUVDSomdSgJjWomdSg5iQeHhrUTGpQk3ahzdSgpqJBzaQGNalBzaQGNSfx8NCgZlKDmrQLbaYGNRUNaiY1qEkNaiY1qDmJh4cGNZMa1KRdaDM1qKloUDOpQU1qUDOpQc1JPDw0qJnUoCY1qJm0C22&#x2B;U4Oa1KBmUoOak3h4aFAzqUFNalAzaRfafKcGNalBzaQGNSfx8NCgZlKDmtSgZtIutPlODWpSg5pJDWpO4uGhQc2kBjWpQc2kXWjznRrUpAY1kxrUnMTDQ4OaSQ1qUoOaSbvQ5js1qEkNaiY1qDmJh4cGNZMa1KQGNZN2oc13alCTGtRMalBzEg8PDWomNahJDWom7UKb79SgJjWomdSg5iQeHhrUTGpQkxrUTNqFNt&#x2B;pQU1qUDOpQc1JPDw0qJnUoCY1qKloUDNpF9pMDWpSg5pJDWpO4uGhQc2kBjWpQU1Fg5pJu9BmalCTGtRMalBzEg8PDWomNahJDWoqGtRM2oU2U4Oa1KBmUoOak3h4aFAzqUFNalBT0aBm0i60mRrUpAY1kxrUnMTDQ4OaSQ1qUoOaigY1k3ahzdSgJjWomdSg5iQeHhrUTGpQkxrUVDSombQLbaYGNalBzaQGNSfx8NCgZlKDmtSgpqJBzaRdaDM1qEkNaiY1qDmJh4cGNZMa1KQGNRUNaibtQpupQU1qUDOpQc1JPDy8OvTm1KDmmzSoSbvQZnp16M0n8fDw6tCbU4Oab9KgJu1Cm&#x2B;nVoTefxMPDq0NvTg1qvkmDmrQLbaZXh958Eg8Prw69OTWo&#x2B;SYNatIutJleHXrzSTw8vDr05tSg5ps0qEm70GZ6dejNJ/Hw8OrQm1ODmm/SoCbtQpvp1aE3n8TDw6tDb04Nar5Jg5q0C22mV4fefBIPD68OvTk1qPkmDWrSLrSZXh1680k8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcTDy8jQY1qUFNalCTGtSkBjWpQc3NxMPbaFCTGtSkBjWpQU1qUJMa1NxMPLyNBjWpQU1qUJMa1KQGNalBzc3Ew9toUJMa1KQGNalBTWpQkxrU3Ew8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcyv3v/OUF&#x2B;KOFBjVpF9qs&#x2B;Muf&#x2B;f2FBPpPlRrUpF1os&#x2B;Ivf&#x2B;b3FxLoP1VqUJN2oc2Kv/yZ319IoP9UqUFN2oU2K/7yZ35/IYH&#x2B;U6UGNWkX2qz4y5/5/YUE&#x2B;k&#x2B;VGtSkXWiz4i9/5vcXEug/VWpQk3ahzYq//JnfX0ig/1SpQU3ahTYr/vJnlv9C9I&#x2B;SdqHNtAttVuxCm5Ouhu6c1KCm5L6zDLw07EKbaRfarNiFNiddDd05qUFNyX1nGXhp2IU20y60WbELbU66GrpzUoOakvvOMvDSsAttpl1os2IX2px0NXTnpAY1JfedZeClYRfaTLvQZsUutDnpaujOSQ1qSu47y8BLwy60mXahzYpdaHPS1dCdkxrUlNx3loGXhl1oM&#x2B;1CmxW70Oakq6E7JzWoKbnvLAMvDbvQZtqFNit2oc1JV0N3TmpQUxQPxzSoST8NvSk1qKnYhTYnNahJDWomNbZvOJzSoCb9NPSm1KCmYhfanNSgJjWomdTYvuFwSoOa9NPQm1KDmopdaHNSg5rUoGZSY/uGwykNatJPQ29KDWoqdqHNSQ1qUoOaSY3tGw6nNKhJPw29KTWoqdiFNic1qEkNaiY1tm84nNKgJv009KbUoKZiF9qc1KAmNaiZ1Ni&#x2B;4XBKg5r009CbUoOail1oc1KDmtSgZlJj&#x2B;4bDKQ1q0k9Db0oNaip2oc1JDWpSg5pJDf2CRtNPQ29KDWpSg5oraVCTGtRMuhq688n9u5dgFH4aelNqUJMa1FxJg5rUoGbS1dCdT&#x2B;7fvQSj8NPQm1KDmtSg5koa1KQGNZOuhu58cv/uJRiFn4belBrUpAY1V9KgJjWomXQ1dOeT&#x2B;3cvwSj8NPSm1KAmNai5kgY1qUHNpKuhO5/cv3sJRuGnoTelBjWpQc2VNKhJDWomXQ3d&#x2B;eT&#x2B;3UswCj8NvSk1qEkNaq6kQU1qUDPpaujOJ/fvXoJR&#x2B;GnoTalBTWpQcyUNalKDmklXQ3c&#x2B;uX/3EowKGtSkV4feXHE1dGfFLrQ5qUFNyX3nJRgVNKhJrw69ueJq6M6KXWhzUoOakvvOSzAqaFCTXh16c8XV0J0Vu9DmpAY1Jfedl2BU0KAmvTr05oqroTsrdqHNSQ1qSu47L8GooEFNenXozRVXQ3dW7EKbkxrUlNx3XoJRQYOa9OrQmyuuhu6s2IU2JzWoKbnvvASjggY16dWhN1dcDd1ZsQttTmpQU3LfeQlGBQ1q0qtDb664GrqzYhfanNSgpuS&#x2B;swy8NOxCm&#x2B;mnoTdNalCTfhp6U2pQM&#x2B;p&#x2B;zzLw0rALbaafht40qUFN&#x2B;mnoTalBzaj7PcvAS8MutJl&#x2B;GnrTpAY16aehN6UGNaPu9ywDLw270Gb6aehNkxrUpJ&#x2B;G3pQa1Iy637MMvDTsQpvpp6E3TWpQk34aelNqUDPqfs8y8NKwC22mn4beNKlBTfpp6E2pQc2o&#x2B;z3LwEvDLrSZfhp606QGNemnoTelBjWj7vcsAy8Nu9Bm&#x2B;mnoTZMa1KSfht6UGtSMut/z1&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1Dy5f/fX4Ogb7UKbaRfanNSgZlKDmtSgpqJBzZP7d38Njr7RLrSZdqHNSQ1qJjWoSQ1qKhrUPLl/99fg6BvtQptpF9qc1KBmUoOa1KCmokHNk/t3fw2OvtEutJl2oc1JDWomNahJDWoqGtQ8uX/31&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1JzEw8MutPlODWoqGtSk3w79TZMa1KQGNSfx8LALbb5Tg5qKBjXpt0N/06QGNalBzUk8POxCm&#x2B;/UoKaiQU367dDfNKlBTWpQcxIPD7vQ5js1qKloUJN&#x2B;O/Q3TWpQkxrUnMTDwy60&#x2B;U4Naioa1KTfDv1NkxrUpAY1J/HwsAttvlODmooGNem3Q3/TpAY1qUHNSTw87EKb79SgpqJBTfrt0N80qUFNalBzEg8Pu9DmOzWoqWhQk3479DdNalCTGtQ8uX/3EozCLrSZdqHNdDV056QGNWkX2nynXWjzyf27l2AUdqHNtAttpquhOyc1qEm70OY77UKbT&#x2B;7fvQSjsAttpl1oM10N3TmpQU3ahTbfaRfafHL/7iUYhV1oM&#x2B;1Cm&#x2B;lq6M5JDWrSLrT5TrvQ5pP7dy/BKOxCm2kX2kxXQ3dOalCTdqHNd9qFNp/cv3sJRmEX2ky70Ga6GrpzUoOatAttvtMutPnk/t1LMAq70GbahTbT1dCdkxrUpF1o8512oc0n9&#x2B;9eglHYhTbTLrSZrobunNSgJu1Cm&#x2B;&#x2B;0C22exMMfa1CTdqHNO2lQkxrUXMkutJka2zcc/lSDmrQLbd5Jg5rUoOZKdqHN1Ni&#x2B;4fCnGtSkXWjzThrUpAY1V7ILbabG9g2HP9WgJu1Cm3fSoCY1qLmSXWgzNbZvOPypBjVpF9q8kwY1qUHNlexCm6mxfcPhTzWoSbvQ5p00qEkNaq5kF9pMje0bDn&#x2B;qQU3ahTbvpEFNalBzJbvQZmps33D4Uw1q0i60eScNalKDmivZhTZTo/&#x2B;CL4d&#x2B;tLQLbVY0qJl0NXRnRYOakvvOPwv&#x2B;KGEX2qxoUDPpaujOigY1Jfedfxb8UcIutFnRoGbS1dCdFQ1qSu47/yz4o4RdaLOiQc2kq6E7KxrUlNx3/lnwRwm70GZFg5pJV0N3VjSoKbnv/LPgjxJ2oc2KBjWTroburGhQU3Lf&#x2B;WfBHyXsQpsVDWomXQ3dWdGgpuS&#x2B;88&#x2B;CP0rYhTYrGtRMuhq6s6JBTVE8vI1daLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHr6LXWizokFNxS60mRrUpAY1qUFN2mXb4OG72IU2KxrUVOxCm6lBTWpQkxrUpF22DR6&#x2B;i11os6JBTcUutJka1KQGNalBTdpl2&#x2B;Dhu9iFNisa1FTsQpupQU1qUJMa1KRdtg0evotdaLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHn54dejNaRfarGhQk66G7kxXQ3emBjUVje0bDh9eHXpz2oU2KxrUpKuhO9PV0J2pQU1FY/uGw4dXh96cdqHNigY16WroznQ1dGdqUFPR2L7h8OHVoTenXWizokFNuhq6M10N3Zka1FQ0tm84fHh16M1pF9qsaFCTrobuTFdDd6YGNRWN7RsOH14denPahTYrGtSkq6E709XQnalBTUVj&#x2B;4bDh1eH3px2oc2KBjXpaujOdDV0Z2pQU9HYvuHw4dWhN6ddaLOiQU26GrozXQ3dmRrUVDS2bzh8aFAzqUFN2oU20y60mXahzW/SoKaisX3D4UODmkkNatIutJl2oc20C21&#x2B;kwY1FY3tGw4fGtRMalCTdqHNtAttpl1o85s0qKlobN9w&#x2B;NCgZlKDmrQLbaZdaDPtQpvfpEFNRWP7hsOHBjWTGtSkXWgz7UKbaRfa/CYNaioa2zccPjSomdSgJu1Cm2kX2ky70OY3aVBT0di&#x2B;4fChQc2kBjVpF9pMu9Bm2oU2v0mDmorG9g2HDw1qJjWoSbvQZtqFNtMutPlNGtRUNLZvOHxoUDOpQU1qUJMa1KQGNd&#x2B;kQU1qUDOpsX3D4UODmkkNalKDmtSgJjWo&#x2B;SYNalKDmkmN7RsOHxrUTGpQkxrUpAY1qUHNN2lQkxrUTGps33D40KBmUoOa1KAmNahJDWq&#x2B;SYOa1KBmUmP7hsOHBjWTGtSkBjWpQU1qUPNNGtSkBjWTGts3HD40qJnUoCY1qEkNalKDmm/SoCY1qJnU2L7h8KFBzaQGNalBTWpQkxrUfJMGNalBzaTG9g2HDw1qJjWoSQ1qUoOa1KDmmzSoSQ1qJjW2bzh8aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4eHXozWkX2qy4GrqzokHNlexCmyfx8PDq0JvTLrRZcTV0Z0WDmivZhTZP4uHh1aE3p11os&#x2B;Jq6M6KBjVXsgttnsTDw6tDb0670GbF1dCdFQ1qrmQX2jyJh4dXh96cdqHNiquhOysa1FzJLrR5Eg8Prw69Oe1CmxVXQ3dWNKi5kl1o8yQeHl4denPahTYrroburGhQcyW70OZJPDy8OvTmtAttVlwN3VnRoOZKdqHNk3h4G7vQZtqFNlODmtSgJl0N3Zka1KQGNSfx8DZ2oc20C22mBjWpQU26GrozNahJDWpO4uFt7EKbaRfaTA1qUoOadDV0Z2pQkxrUnMTD29iFNtMutJka1KQGNelq6M7UoCY1qDmJh7exC22mXWgzNahJDWrS1dCdqUFNalBzEg9vYxfaTLvQZmpQkxrUpKuhO1ODmtSg5iQe3sYutJl2oc3UoCY1qElXQ3emBjWpQc1JPLyNXWgz7UKbqUFNalCTrobuTA1qUoOa//uf//4PH72d67VqytUAAAAASUVORK5CYII=";
		//  $str="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABLeSURBVHhe7ZJBkttAEsT2/5/e5YHiphiQMeWqlih6EIFLB5HdI/s/G/&#x2B;9swY177QLbaaroTtvJh7eRoOad9qFNtPV0J03Ew9vo0HNO&#x2B;1Cm&#x2B;lq6M6biYe30aDmnXahzXQ1dOfNxMPbaFDzTrvQZroauvNm4uFtNKh5p11oM10N3Xkz8fA2GtS80y60ma6G7ryZeHgbDWreaRfaTFdDd95MPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDw0qJnUoCbtQpupQU1Fg5pJDWpSg5pJDWpO4uGhQc2kBjVpF9pMDWoqGtRMalCTGtRMalBzEg8PDWomNahJu9BmalBT0aBmUoOa1KBmUoOak3h4aFAzqUFN2oU2U4OaigY1kxrUpAY1kxrUnMTDQ4OaSQ1q0i60mRrUVDSomdSgJjWomdSg5iQeHhrUTGpQk3ahzdSgpqJBzaQGNalBzaQGNSfx8NCgZlKDmrQLbaYGNRUNaiY1qEkNaiY1qDmJh4cGNZMa1KRdaDM1qKloUDOpQU1qUDOpQc1JPDw0qJnUoCY1qJm0C22&#x2B;U4Oa1KBmUoOak3h4aFAzqUFNalAzaRfafKcGNalBzaQGNSfx8NCgZlKDmtSgZtIutPlODWpSg5pJDWpO4uGhQc2kBjWpQc2kXWjznRrUpAY1kxrUnMTDQ4OaSQ1qUoOaSbvQ5js1qEkNaiY1qDmJh4cGNZMa1KQGNZN2oc13alCTGtRMalBzEg8PDWomNahJDWom7UKb79SgJjWomdSg5iQeHhrUTGpQkxrUTNqFNt&#x2B;pQU1qUDOpQc1JPDw0qJnUoCY1qKloUDNpF9pMDWpSg5pJDWpO4uGhQc2kBjWpQU1Fg5pJu9BmalCTGtRMalBzEg8PDWomNahJDWoqGtRM2oU2U4Oa1KBmUoOak3h4aFAzqUFNalBT0aBm0i60mRrUpAY1kxrUnMTDQ4OaSQ1qUoOaigY1k3ahzdSgJjWomdSg5iQeHhrUTGpQkxrUVDSombQLbaYGNalBzaQGNSfx8NCgZlKDmtSgpqJBzaRdaDM1qEkNaiY1qDmJh4cGNZMa1KQGNRUNaibtQpupQU1qUDOpQc1JPDy8OvTm1KDmmzSoSbvQZnp16M0n8fDw6tCbU4Oab9KgJu1Cm&#x2B;nVoTefxMPDq0NvTg1qvkmDmrQLbaZXh958Eg8Prw69OTWo&#x2B;SYNatIutJleHXrzSTw8vDr05tSg5ps0qEm70GZ6dejNJ/Hw8OrQm1ODmm/SoCbtQpvp1aE3n8TDw6tDb04Nar5Jg5q0C22mV4fefBIPD68OvTk1qPkmDWrSLrSZXh1680k8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcTDy8jQY1qUFNalCTGtSkBjWpQc3NxMPbaFCTGtSkBjWpQU1qUJMa1NxMPLyNBjWpQU1qUJMa1KQGNalBzc3Ew9toUJMa1KQGNalBTWpQkxrU3Ew8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcyv3v/OUF&#x2B;KOFBjVpF9qs&#x2B;Muf&#x2B;f2FBPpPlRrUpF1os&#x2B;Ivf&#x2B;b3FxLoP1VqUJN2oc2Kv/yZ319IoP9UqUFN2oU2K/7yZ35/IYH&#x2B;U6UGNWkX2qz4y5/5/YUE&#x2B;k&#x2B;VGtSkXWiz4i9/5vcXEug/VWpQk3ahzYq//JnfX0ig/1SpQU3ahTYr/vJnlv9C9I&#x2B;SdqHNtAttVuxCm5Ouhu6c1KCm5L6zDLw07EKbaRfarNiFNiddDd05qUFNyX1nGXhp2IU20y60WbELbU66GrpzUoOakvvOMvDSsAttpl1os2IX2px0NXTnpAY1JfedZeClYRfaTLvQZsUutDnpaujOSQ1qSu47y8BLwy60mXahzYpdaHPS1dCdkxrUlNx3loGXhl1oM&#x2B;1CmxW70Oakq6E7JzWoKbnvLAMvDbvQZtqFNit2oc1JV0N3TmpQUxQPxzSoST8NvSk1qKnYhTYnNahJDWomNbZvOJzSoCb9NPSm1KCmYhfanNSgJjWomdTYvuFwSoOa9NPQm1KDmopdaHNSg5rUoGZSY/uGwykNatJPQ29KDWoqdqHNSQ1qUoOaSY3tGw6nNKhJPw29KTWoqdiFNic1qEkNaiY1tm84nNKgJv009KbUoKZiF9qc1KAmNaiZ1Ni&#x2B;4XBKg5r009CbUoOail1oc1KDmtSgZlJj&#x2B;4bDKQ1q0k9Db0oNaip2oc1JDWpSg5pJDf2CRtNPQ29KDWpSg5oraVCTGtRMuhq688n9u5dgFH4aelNqUJMa1FxJg5rUoGbS1dCdT&#x2B;7fvQSj8NPQm1KDmtSg5koa1KQGNZOuhu58cv/uJRiFn4belBrUpAY1V9KgJjWomXQ1dOeT&#x2B;3cvwSj8NPSm1KAmNai5kgY1qUHNpKuhO5/cv3sJRuGnoTelBjWpQc2VNKhJDWomXQ3d&#x2B;eT&#x2B;3UswCj8NvSk1qEkNaq6kQU1qUDPpaujOJ/fvXoJR&#x2B;GnoTalBTWpQcyUNalKDmklXQ3c&#x2B;uX/3EowKGtSkV4feXHE1dGfFLrQ5qUFNyX3nJRgVNKhJrw69ueJq6M6KXWhzUoOakvvOSzAqaFCTXh16c8XV0J0Vu9DmpAY1Jfedl2BU0KAmvTr05oqroTsrdqHNSQ1qSu47L8GooEFNenXozRVXQ3dW7EKbkxrUlNx3XoJRQYOa9OrQmyuuhu6s2IU2JzWoKbnvvASjggY16dWhN1dcDd1ZsQttTmpQU3LfeQlGBQ1q0qtDb664GrqzYhfanNSgpuS&#x2B;swy8NOxCm&#x2B;mnoTdNalCTfhp6U2pQM&#x2B;p&#x2B;zzLw0rALbaafht40qUFN&#x2B;mnoTalBzaj7PcvAS8MutJl&#x2B;GnrTpAY16aehN6UGNaPu9ywDLw270Gb6aehNkxrUpJ&#x2B;G3pQa1Iy637MMvDTsQpvpp6E3TWpQk34aelNqUDPqfs8y8NKwC22mn4beNKlBTfpp6E2pQc2o&#x2B;z3LwEvDLrSZfhp606QGNemnoTelBjWj7vcsAy8Nu9Bm&#x2B;mnoTZMa1KSfht6UGtSMut/z1&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1Dy5f/fX4Ogb7UKbaRfanNSgZlKDmtSgpqJBzZP7d38Njr7RLrSZdqHNSQ1qJjWoSQ1qKhrUPLl/99fg6BvtQptpF9qc1KBmUoOa1KCmokHNk/t3fw2OvtEutJl2oc1JDWomNahJDWoqGtQ8uX/31&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1JzEw8MutPlODWoqGtSk3w79TZMa1KQGNSfx8LALbb5Tg5qKBjXpt0N/06QGNalBzUk8POxCm&#x2B;/UoKaiQU367dDfNKlBTWpQcxIPD7vQ5js1qKloUJN&#x2B;O/Q3TWpQkxrUnMTDwy60&#x2B;U4Naioa1KTfDv1NkxrUpAY1J/HwsAttvlODmooGNem3Q3/TpAY1qUHNSTw87EKb79SgpqJBTfrt0N80qUFNalBzEg8Pu9DmOzWoqWhQk3479DdNalCTGtQ8uX/3EozCLrSZdqHNdDV056QGNWkX2nynXWjzyf27l2AUdqHNtAttpquhOyc1qEm70OY77UKbT&#x2B;7fvQSjsAttpl1oM10N3TmpQU3ahTbfaRfafHL/7iUYhV1oM&#x2B;1Cm&#x2B;lq6M5JDWrSLrT5TrvQ5pP7dy/BKOxCm2kX2kxXQ3dOalCTdqHNd9qFNp/cv3sJRmEX2ky70Ga6GrpzUoOatAttvtMutPnk/t1LMAq70GbahTbT1dCdkxrUpF1o8512oc0n9&#x2B;9eglHYhTbTLrSZrobunNSgJu1Cm&#x2B;&#x2B;0C22exMMfa1CTdqHNO2lQkxrUXMkutJka2zcc/lSDmrQLbd5Jg5rUoOZKdqHN1Ni&#x2B;4fCnGtSkXWjzThrUpAY1V7ILbabG9g2HP9WgJu1Cm3fSoCY1qLmSXWgzNbZvOPypBjVpF9q8kwY1qUHNlexCm6mxfcPhTzWoSbvQ5p00qEkNaq5kF9pMje0bDn&#x2B;qQU3ahTbvpEFNalBzJbvQZmps33D4Uw1q0i60eScNalKDmivZhTZTo/&#x2B;CL4d&#x2B;tLQLbVY0qJl0NXRnRYOakvvOPwv&#x2B;KGEX2qxoUDPpaujOigY1Jfedfxb8UcIutFnRoGbS1dCdFQ1qSu47/yz4o4RdaLOiQc2kq6E7KxrUlNx3/lnwRwm70GZFg5pJV0N3VjSoKbnv/LPgjxJ2oc2KBjWTroburGhQU3Lf&#x2B;WfBHyXsQpsVDWomXQ3dWdGgpuS&#x2B;88&#x2B;CP0rYhTYrGtRMuhq6s6JBTVE8vI1daLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHr6LXWizokFNxS60mRrUpAY1qUFN2mXb4OG72IU2KxrUVOxCm6lBTWpQkxrUpF22DR6&#x2B;i11os6JBTcUutJka1KQGNalBTdpl2&#x2B;Dhu9iFNisa1FTsQpupQU1qUJMa1KRdtg0evotdaLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHn54dejNaRfarGhQk66G7kxXQ3emBjUVje0bDh9eHXpz2oU2KxrUpKuhO9PV0J2pQU1FY/uGw4dXh96cdqHNigY16WroznQ1dGdqUFPR2L7h8OHVoTenXWizokFNuhq6M10N3Zka1FQ0tm84fHh16M1pF9qsaFCTrobuTFdDd6YGNRWN7RsOH14denPahTYrGtSkq6E709XQnalBTUVj&#x2B;4bDh1eH3px2oc2KBjXpaujOdDV0Z2pQU9HYvuHw4dWhN6ddaLOiQU26GrozXQ3dmRrUVDS2bzh8aFAzqUFN2oU20y60mXahzW/SoKaisX3D4UODmkkNatIutJl2oc20C21&#x2B;kwY1FY3tGw4fGtRMalCTdqHNtAttpl1o85s0qKlobN9w&#x2B;NCgZlKDmrQLbaZdaDPtQpvfpEFNRWP7hsOHBjWTGtSkXWgz7UKbaRfa/CYNaioa2zccPjSomdSgJu1Cm2kX2ky70OY3aVBT0di&#x2B;4fChQc2kBjVpF9pMu9Bm2oU2v0mDmorG9g2HDw1qJjWoSbvQZtqFNtMutPlNGtRUNLZvOHxoUDOpQU1qUJMa1KQGNd&#x2B;kQU1qUDOpsX3D4UODmkkNalKDmtSgJjWo&#x2B;SYNalKDmkmN7RsOHxrUTGpQkxrUpAY1qUHNN2lQkxrUTGps33D40KBmUoOa1KAmNahJDWq&#x2B;SYOa1KBmUmP7hsOHBjWTGtSkBjWpQU1qUPNNGtSkBjWTGts3HD40qJnUoCY1qEkNalKDmm/SoCY1qJnU2L7h8KFBzaQGNalBTWpQkxrUfJMGNalBzaTG9g2HDw1qJjWoSQ1qUoOa1KDmmzSoSQ1qJjW2bzh8aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4eHXozWkX2qy4GrqzokHNlexCmyfx8PDq0JvTLrRZcTV0Z0WDmivZhTZP4uHh1aE3p11os&#x2B;Jq6M6KBjVXsgttnsTDw6tDb0670GbF1dCdFQ1qrmQX2jyJh4dXh96cdqHNiquhOysa1FzJLrR5Eg8Prw69Oe1CmxVXQ3dWNKi5kl1o8yQeHl4denPahTYrroburGhQcyW70OZJPDy8OvTmtAttVlwN3VnRoOZKdqHNk3h4G7vQZtqFNlODmtSgJl0N3Zka1KQGNSfx8DZ2oc20C22mBjWpQU26GrozNahJDWpO4uFt7EKbaRfaTA1qUoOadDV0Z2pQkxrUnMTD29iFNtMutJka1KQGNelq6M7UoCY1qDmJh7exC22mXWgzNahJDWrS1dCdqUFNalBzEg9vYxfaTLvQZmpQkxrUpKuhO1ODmtSg5iQe3sYutJl2oc3UoCY1qElXQ3emBjWpQc1JPLyNXWgz7UKbqUFNalCTrobuTA1qUoOa//uf//4PH72d67VqytUAAAAASUVORK5CYII=";
		  $base64_string=html_entity_decode($base64_string);
		//echo $base64_string;exit;
        $ifp = fopen( $output_file, 'wb' ); 
		
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        $file = public_path('prescriptions/'). $output_file;
        file_put_contents($file, $ifp);
        rename($output_file, 'prescriptions/'.$output_file);
        // clean up the file resource
        fclose( $ifp ); 
        return $output_file; 
    }
	/**
      @OA\Post(
          path="/v2/addmedlistfromscript",
          tags={"MedList"},
          summary="Add to Medlist From Script",
          operationId="Add to Medlist From Script",
		  security={{"bearerAuth": {}} },
      
          
         @OA\Parameter(
              name="prescriptionuserid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), @OA\Parameter(
              name="prescriptionitemid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         
        @OA\Parameter(
              name="drugname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugstrength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugquantity",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="repeats",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="referrence",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="PhotoID",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        @OA\Parameter(
              name="mimsimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="drugpack",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        
          @OA\Parameter(
              name="script",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  
          @OA\Parameter(
              name="price",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="cmicode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  
		   
        
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function addmedlistfromscript(Request $request)
	{
		$medicationdetails = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=',$request->prescriptionitemid)
															->where('prescriptionitemdetails.prescriptionuserid' ,'=', $request->prescriptionuserid)
															->where('prescriptionitemdetails.profileid', '=',$request->profileid)															
															->where('prescriptionitemdetails.prescriptionid', '=',\DB::raw('"'.$request->prescriptionid.'"'))															
															->update(['prescriptionitemdetails.ismedlist' => 1,'prescriptionitemdetails.drugname'=>$request->drugname,'prescriptionitemdetails.drugform'=>$request->drugform,'prescriptionitemdetails.drugstrength'=>$request->drugstrength,'prescriptionitemdetails.drugquantity'=>$request->drugquantity,'prescriptionitemdetails.drugpack'=>$request->drugpack,'prescriptionitemdetails.price'=>$request->price,'prescriptionitemdetails.medimage'=>$request->PhotoID,'prescriptionitemdetails.mimsimage'=>$request->mimsimage,'prescriptionitemdetails.cmicode'=>$request->cmicode]);
		 
		$success['message']= trans('messages.add_medlist');
		$success['medicationdetails'] = $medicationdetails;	 
        return response()->json(['success' => $success] ,$this->successStatus);
	}
	
	/**
    *  @OA\Post(
    *     path="/v2/scriptsearch",
    *      tags={"Scripts"},
    *      summary="Script medication search",
    *      operationId="Script medication search",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="drugname",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugstrength",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*		@OA\Parameter(
    *          name="userid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function scriptsearch(Request $request)
	{
        $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($request->userid);
        $healthprofile = Healthprofile::where('userid', $request->userid)->get();
        
                     
      // $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price,C.Medicare,C.Con_Pens_Vet_ConSafty,C.SafetyNet,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.itemNameDisplay LIKE '".$request->drugname."%' and C.Strength LIKE '%".$request->drugstrength."%'");
	   
       $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price,C.Medicare,C.Con_Pens_Vet_ConSafty,C.SafetyNet,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode) as image from catalouge_master as C  where C.itemNameDisplay LIKE '".$request->drugname ."%' and C.Strength LIKE '%".$request->drugstrength."%' UNION select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price,C.Medicare,C.Con_Pens_Vet_ConSafty,C.SafetyNet,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.itemNameDisplay LIKE '".$request->drugname."%'");        
	   
       $mims_data = DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode` left join packitemimages as PI on P.prodcode=PI.prodcode where P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode and `P`.`ActualProduct` LIKE '".$request->drugname."%'");
       $drug_details=array();
        
        for($i=0;$i<count($catalouge_data);$i++){
            $catalouge_data[$i]->reference="Catalouge";
			 if(count($healthprofile)>0){
					if($catalouge_data[$i]->script == "OTC")
					{
						//echo "OTC";exit;
						 $p=$catalouge_data[$i]->standard_price;
						 $gst =(($catalouge_data[$i]->GST)*$p)/100;
						 $price =$p+ $gst;
					}
					else
					{						
                                if($healthprofile[0]->safetyNet!=null){
									//echo "123";exit;
                                    $p=$catalouge_data[$i]->SafetyNet;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                else if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null || $healthprofile[0]->veteranaffairsnumber){
                                   // echo "222";exit;
									$p=$catalouge_data[$i]->Con_Pens_Vet_ConSafty;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                else if($healthprofile[0]->medicarenumber!=null){
                                  // echo "666";exit;
								   $p=$catalouge_data[$i]->Medicare;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                 //  echo "678";exit;
								   $p=$catalouge_data[$i]->standard_price;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
					}
                    }
           // $p=(float)$catalouge_data[$i]->price;
			//$gst =(float)(($catalouge_data[$i]->GST)*$p)/100;
			$catalouge_data[$i]->price =number_format(($price), 2, '.', '');
           // $catalouge_data[$i]->strength=$catalouge_data[$i]->active.$catalouge_data[$i]->active_units;
            if($catalouge_data[$i]->script=='Prescription'){$catalouge_data[$i]->script="Yes";}else{$catalouge_data[$i]->script="No";}
            $drug_details[]=$catalouge_data[$i];
        }
        for($i=0;$i<count($mims_data);$i++){
           // $mims_data[$i]->strength=$mims_data[$i]->active.$mims_data[$i]->active_units;
            $mims_data[$i]->reference="MIMS";
            $mims_data[$i]->ShortDescription=$mims_data[$i]->drug;
            $mims_data[$i]->price="";
            $drug_details[]=$mims_data[$i];
        }
        if(!empty($drug_details)){
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.succ_message');
        }else{
            $response->drug_details 	= $drug_details;
            $response->msg 		= trans('messages.no_data');  
        }
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        }
}
