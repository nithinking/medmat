<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Healthprofile; 
use App\models\Deliveryaddress; 
use App\models\Gplocations; 
use App\models\Medicalconditions; 
use App\models\Allergies; 
use App\models\Immunisation; 
use App\models\Notifications; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
use Helper;
use Session;
use Config;
use App;
use DateTime;
use Response;
    /**
  @OA\Info(
      description="",
      version="2.0.0",
      title="Medmate API",
 )
 
* @OA\SecurityScheme(
*      securityScheme="bearerAuth",
*      in="header",
*      name="bearerAuth",
*      type="http",
*      scheme="bearer",
*      bearerFormat="JWT",
* ),
 */
class Usercontroller extends Controller
{

 
 public $successStatus = 200;
/**
      @OA\Post(
          path="/v2/register",
          tags={"User"},
          summary="userSignUp",
          operationId="signup",
		  
		  @OA\Parameter(
              name="firstname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),  
          
           @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="refferal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  
          
		  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),   
      
          @OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Parameter(
              name="password",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="c_password",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),	
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 
		public function register(Request $request) {    
		
			 $validator = Validator::make($request->all(), 
						  [ 
						  'username' => 'required|email|unique:users',
						  'firstname' => 'required',
						  'password' => 'required',  
						  'c_password' => 'required|same:password', 
						 ]);   
			 if ($validator->fails()) {          
				   return response()->json(['error'=>$validator->errors()], 401);                        
				   }    
			 $input = $request->all();  
			 $input['password'] = bcrypt($input['password']);
			 $input['profiletypeid'] = 1;
			 $input['refferal'] = $request->refferal;
			 $input['knownas'] = $request->firstname;
			 $input['hashcode'] = md5($input['password']);
			// print_r($input);
			 $user = User::create($input); 
			// print_r($user);exit;
			  if($user){
				  $healthprofile = new Healthprofile;
					$healthprofile->userid =  $user->userid;
					$healthprofile->parentuser_flag = '1';
				 $healthprofile->profile_relation = 'self';
				 $healthprofile->save();
					
				$link=url('verifyaccount/'.base64_encode($user->userid));
				$subject="Medmate – Complete your registration";
				//$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Verify Email</button></a>";
						  // $body_message = trans('messages.Verify_Email_Template');
                   
              // $body_message 	= 	str_replace('verify_button',$content,$body_message);
				$body_message ='
				<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <title>Email Verification</title>
	</head>
	<style>
		body  {
			font-family: "Nunito" !important;font-size: 18px !important;
		}
		.top {
			display: flex !important;flex-direction: row !important;width: 700px !important;justify-content: space-between !important;
		}
		.heading {
		   color: #1d9bd8 !important;font-size: xx-large;font-weight: 600 !important;
		}
		.total {
			width: 700px !important !important;  margin: auto !important;
		}
		.bottom {
			text-align: center !important;color :#7e7e7e !important;
		}
		.one {
			background-color: #1d9bd8 !important;color: white !important;border: 0px solid !important;border-radius: 10px !important;height: 35px !important;margin-left: 300px !important;margin-top: 50px !important;
		}
	</style>
		<body style="font-family: "Nunito" !important;font-size: 18px !important;">
		<div class="total" style="width: 700px !important !important;  margin: auto !important;">
			<div class="top" style="display: flex !important;flex-direction: row !important;width: 700px !important;justify-content: space-between !important;">
				<p style="padding-top: 15px !important;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" alt=""></p>
				<p class="heading" style="padding-left: 90px !important; color: #1d9bd8 !important;font-size: xx-large;font-weight: 600 !important;">Registration Verification</p>
			</div>
			<div>
			<p>Hello '.$request->firstname.',</p>
        <p>To complete the medmate registration process please click:</p>
        <p style="height: 150px !important;width:700px !important;background-color: #f2f2f2 !important;"><a href="'.$link.'">
		<button class="one" style="background-color: #1d9bd8 !important;color: white !important;border: 0px solid !important;border-radius: 10px !important;height: 35px !important;margin-left: 300px !important;margin-top: 50px !important;">Verify Email</button></a></p>
        <p>If you have any questions, please email <a href="">enquires@medmate.com.au</a> </p>
    </div>
    <div class="bottom" style="text-align: center !important;color :#7e7e7e !important;">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
							medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
				';
               $message['to']           = $user->username;
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   
			  
			   
			   $wbody_message='
			   <!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
* {
  box-sizing: border-box !important;
}

.row::after {
  content: "" !important;
  clear: both !important;
  display: table !important;
}

[class*="col-"] {
  float: left !important;
  padding: 15px !important;
}


.footer {
  text-align: center !important;
  font-size: 12px !important;
  padding: 15px !important;
}

/* For mobile phones: */
[class*="col-"] {
  width: 100% !important;
}

@media only screen and (min-width: 768px) {
  /* For desktop: */
  .col-1 {width: 8.33% !important;}
  .col-2 {width: 16.66% !important;}
  .col-3 {width: 25% !important;}
  .col-4 {width: 33.33% !important;}
  .col-5 {width: 41.66% !important;}
  .col-6 {width: 50% !important;}
  .col-7 {width: 58.33% !important;}
  .col-8 {width: 66.66% !important;}
  .col-9 {width: 75%!important;}
  .col-10 {width: 83.33% !important;}
  .col-11 {width: 91.66% !important;}
  .col-12 {width: 100% !important;}
}
p {
  line-height: 1.8 !important;
}
</style>
</head>
<body style="font-family: "Roboto";">

<div class="header" style="margin: auto;width: 65% !important;margin-bottom: 0px !important;" >
    <p style="float: left !important;" ><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" alt=""></p>
    <p style="color: #47c6f4 !important;float: right !important;font-size:x-large !important;">Thank you for registering to Medmate</p>
    <br> <br><br><br>
    <br> <br><br><br>
</div>
<div style="margin: auto;width: 65%;">
    <p ><span style="color: #47c6f4;">Hello </span> '.$request->firstname.', </p>
</div>


<div style="margin: auto;width: 65% !important;padding-left: 2% !important;text-align: center !important;margin-top:0px !important;">
    <h2 style="color: #47c6f4;font-weight: 400 !important;"> We hope you’re enjoying Medmate </h2>
    <p>
        Thanks for registering with medmate! Our aim is to help you better coordinate your healthcare. Check out what you can do with medmate below.
    </p>
</div>
<br/>
<div  style="margin: auto;width: 65% !important;background-color: #f2f2f2 !important;padding-left: 4% !important;padding-right: 4% !important;padding-top: 1% !important;">
    <h2 style="font-weight: 200 !important;">Here’s what we can do with Medmate:</h2>
    <p style="margin-bottom: 0px !important;">
    <span  style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; Shop with e-scripts.</span> Add your electronic script tokens to your cart or store them to use later. Load from SMS or scan QR code. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; Shop with paper scripts.</span> Still using traditional paper scripts? You can scan these for ordering as well. Just remember to have them available to hand over to the courier on delivery. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; List your medications</span>  in Meds List. Search for your meds and view detailed safety information about each of your meds. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; Find the best value</span> for prescription and non-prescription medications. Add to cart and then check out the savings with each of your local pharmacies. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; Home delivery.</span> Too busy to get to the pharmacy? No worries. Just shop for all your pharmacy items including scripts and organise a delivery time with your local pharmacy when it suits you. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; Book doctors’ appointments.</span> Find your regular medical clinics in My Doctors and book your next appointment (face to face or telehealth) directly from medmate. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; Create your own Health Profile.</span>  This is for you to keep track of your allergies, medical conditions and basic health information. You can share this with the pharmacist to make sure that you receive accurate advice about medication safety. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; Care for family members.</span> Care for family members. You can view meds lists for each of the members of your family in one place and shop for everyone. Its easy to make sure everyone you care about is up to date with their meds. <br> <br>

    <span style="color: #47c6f4 !important;">&#10004;&nbsp;&nbsp;&nbsp; E-referrals.</span> Just had a telehealth consultation and need to get a blood test? Medmate will get the e-referral to you and help you find a collection centre (NSW only). <br> <br>
    </p>
</div>

<div class="footer" style="text-align: center !important;  font-size: 12px !important;  padding: 15px !important;margin: auto !important;width: 65% !important;margin-top: opx !important;">
  <p>
    You received this email because you have registered with Medmate. If this was sent to you by mistake, please contact support. <br>
    Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
    medmate.com.au</p>
</div>

</body>
</html>
			   ';
			   
			   $wsubject="Welcome to Medmate";
			   //$wbody_message = trans('messages.Welcome_Email');
			   $wmessage['to']           = $user->username;
               $wmessage['subject']      = $wsubject;
               $wmessage['body_message'] = $wbody_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $wmessage, function ($wm) use ($wmessage)  {
               $wm->to($wmessage['to'])
                  ->subject($wmessage['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($wmessage['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
               
			}
			 
			 $success['token'] =  $user->createToken('AppName')->accessToken;
			 $user_details = User::findOrFail($user->userid);
             $profile_completeness=10;
			 if(isset($user_details->firstname)){$profile_completeness=$profile_completeness+10;}
			 if(isset($user_details->lastname)){$profile_completeness=$profile_completeness+10;}
			 $success['profile_completeness']=$profile_completeness;
			 $success['user_details'] = $user_details;
			 return response()->json(['success'=>$success], $this->successStatus);
 
			 
 
		}
		/**
      @OA\Post(
          path="/v2/login",
          tags={"User"},
          summary="Login",
          operationId="login",
      
          @OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Parameter(
              name="password",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function login(){ 
		if(Auth::attempt(['username' => request('username'), 'password' => request('password') ,'profiletypeid' => '1','userstatus' => '1'])){ 
		   $user = Auth::user(); 
		   $success['token'] =  $user->createToken('AppName')-> accessToken; 
		   $success['user_details']=$user;
		    $profile_completeness=0;
		   if(isset($user->firstname)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->lastname)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->dob)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->username)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->mobile)){$profile_completeness=$profile_completeness+10;}
		   $healthprofile = Healthprofile::where('userid', $user->userid)->where('parentuser_flag' , '1')->get();
		   $success['healthprofile']=$healthprofile;
		   $familyMembers = User :: where('parentuserid' , $user->userid)->get();
		
			$noofrows = $familyMembers->count();
			for($i=0; $i < $noofrows ; $i++)
			{
				if($familyMembers[$i]['profilepic']!="")
				{
				$familyMemLogo[$i] = url('/profilepic/'.$familyMembers[$i]['profilepic']);
				$familyMembers[$i]['profilepic']	= 	$familyMemLogo[$i];
				}
				else
					$familyMembers[$i]['profilepic'] ="";
				$familyMemberHealthInfo[$i] = Healthprofile :: where('userid' , $familyMembers[$i]['userid'])->get();			
				
				$familyMembers[$i]['familyMemberHealthInfo']	= 	$familyMemberHealthInfo[$i];
				
				$success['familyMembers'] =$familyMembers;
			}
		   if(isset($healthprofile->medicarenumber) || isset($healthprofile->heathcarecard) || isset($healthprofile->veteranaffairsnumber) || isset($healthprofile->safetynet))
		   {
			   $profile_completeness=$profile_completeness+10;
		   }
		   $success['profile_completeness'] = $profile_completeness;
		   $success['familyMembers'] = $familyMembers;
			return response()->json(['success' => $success], $this-> successStatus); 
		  } else{ 
		   return response()->json(['error'=>'Unauthorised'], 401); 
		   } 
	}
	/**
      @OA\Get(
          path="/v2/getUser",
          tags={"User"},
          summary="UserProfile",
          operationId="User Profile",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getUser() {
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->where('parentuser_flag' , '1')->get();
		$addressDetails = Deliveryaddress::where('userid', $user->userid)->get();
		$today = date('Y-m-d');
        $age =  date_diff(date_create($user_details['dob']), date_create($today))->y;
		$user['age'] = $age;
		if(!empty($user->profilepic))
		{
			$user->profilepic = url('/profilepic/'.$user->profilepic);
		}
		elseif($user->profilepic == "")
		{
			$user->profilepic ="";
		}
		$familyMembers = User :: where('parentuserid' , $user->userid)->where('userstatus' , 1)->get();
		
		$noofrows = $familyMembers->count();
		for($i=0; $i < $noofrows ; $i++)
		{
			if($familyMembers[$i]['profilepic']!="")
			{
			$familyMemLogo[$i] = url('/profilepic/'.$familyMembers[$i]['profilepic']);
			$familyMembers[$i]['profilepic']	= 	$familyMemLogo[$i];
			}
			else
				$familyMembers[$i]['profilepic'] ="";
			$familyMemberHealthInfo[$i] = Healthprofile :: where('userid' , $familyMembers[$i]['userid'])->get();			
		   
			$familyprofile_completeness[$i]=0;
			if(isset($familyMembers[$i]['firstname']) && $familyMembers[$i]['firstname']!="null")
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+20;}
			if(isset($familyMembers[$i]['lastname']) && $familyMembers[$i]['lastname']!="null")
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+10;}
			if(isset($familyMembers[$i]['dob']) && $familyMembers[$i]['dob']!=null)
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+10;}
			if(isset($familyMembers[$i]['mobile'])&& $familyMembers[$i]['mobile']!=null)
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+10;}		 
			if(isset($familyMembers[$i]['profilepic'])&& $familyMembers[$i]['profilepic']!="")
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+30;}
			if(isset($familyMemberHealthInfo[$i][0]['medicarenumber']) || isset($familyMemberHealthInfo[$i][0]['heathcarecard']) || isset($familyMemberHealthInfo[$i][0]['veteransaffairscard']) || isset($familyMemberHealthInfo[$i][0]['safetynet']))
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+20;}
			
			// $familyprofile_completeness = $familyprofile_completeness[$i];
		 
		 $today = date('Y-m-d');
		 $familyMembers[$i]['age'] =  date_diff(date_create($familyMembers[$i]['dob']), date_create($today))->y;
		 $familyMembers[$i]['familyMemberHealthInfo']	= 	$familyMemberHealthInfo[$i];
		 $familyMembers[$i]['familyprofile_completeness'] = $familyprofile_completeness[$i];
		}
		$user['familyMembers'] =$familyMembers;
		$user['userhealthprofile'] = $healthprofile;
		$user['usetraddressDetails'] = $addressDetails;
		$profile_completeness=0;
		 if(isset($user->firstname)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->lastname)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->dob)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->username)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->mobile)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->emailverified)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->profilepic)){$profile_completeness=$profile_completeness+20;}
		 if(isset($healthprofile[0]['medicarenumber']) || isset($healthprofile[0]['heathcarecard']) || isset($healthprofile[0]['veteransaffairscard']) || isset($healthprofile[0]['safetynet'])){$profile_completeness=$profile_completeness+20;}
		 $user['profile_completeness'] = $profile_completeness;
		
		$success['user_details']=$user;
		
		return response()->json(['success' => $user], $this->successStatus); 
	}
	/**
      @OA\Post(
          path="/v2/updateProfile",
          tags={"User"},
          summary="UserProfile Update",
          operationId="User Profile Update",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="middlename",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="knownas",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profilepic",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			 			 
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="sex",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="addressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="addressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="suburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="state",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="postcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="defaultaddress",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="receiveinfo",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="permissionforsms",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="occupation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ), @OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			  
			
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ismedicare",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumberpos",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipreferencenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="ihi",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ihi_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvano",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvanoexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="expectedduedate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="procedureyear",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateProfile(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
				 
		
				 $user->firstname = $request['firstname'];
				 $user->middlename = $request['middlename'];
				 $user->lastname = $request['lastname'];
				 $user->knownas = $request['knownas'];
				 
				 if($user->profilepic == "" && $request['profilepic'] !="")
				 {
					
					$user->profilepic=$this->base64_to_jpeg($request['profilepic'],time().$user->userid."pic.jpeg");
				 
				 }				
				  elseif($user->profilepic !="" || !empty($user->profilepic))
				 {
					 
					 $supported_image = array(						
						'jpeg'
					);

					$src_file_name = $request['profilepic'];
					$ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
					if (in_array($ext, $supported_image))
					{	
						//echo "12fdsfds3";exit;
					 $user->profilepic = $user->profilepic;
					}
					else
					{
						//echo "123";exit;
					$user->profilepic = $this->base64_to_jpeg($request['profilepic'],time().$user->userid."pic.jpeg");	
					}
				 }
				 elseif($request['profilepic'] =="")
				 {
					  
					 $request['profilepic'] = $request['profilepic'];
				 }
				 
				 $user->dob = $request['dob'];
				 $today = date('Y-m-d');
        		 $age =  date_diff(date_create($request['dob']), date_create($today))->y;
				 $user->sex = $request['sex'];				
				 $user->homephone = $request['homephone'];
				 $user->mobile = $request['mobile'];
				 $user->permissionforsms = $request['permissionforsms'];				 
				 $user->occupation = $request['occupation'];
				 $user->emergencycontactname = $request['emergencycontactname'];
				 $user->emergencycontactnumber = $request['emergencycontactnumber'];
				 $user->defaultaddress = $request['defaultaddress'];				 
				 $user->receiveinfo = $request['receiveinfo'];				 
				 
				 $user->save();
				 $user['age'] = $age;
				 // Address Add
				 
					 //DB::enableQueryLog();
					  // if ( Deliveryaddress::where('userid',  $user->userid)->where('addresstype' , 'defaultaddress')->exists()) {	
						// $deliveryAddress = Deliveryaddress::join('users','users.userid','=', 'deliveryaddresses.userid')
															// ->where('deliveryaddresses.userid',  $user->userid)

															// ->where('users.defaultaddress' , '1')->first();	
						if ( Deliveryaddress::where('userid',  $user->userid)->where('addresstype' , 'profileaddress')->exists()) {	
						// $deliveryAddress = Deliveryaddress::join('users','users.userid','=', 'deliveryaddresses.userid')
															// ->where('deliveryaddresses.userid',  $user->userid)
															// ->where('deliveryaddresses.addresstype',  "profileaddress")
															// ->first();
									
						// $deliveryAddress->addressline1 = $request['addressline1'];						
						// $deliveryAddress->addressline2 = $request['addressline2'];
						// $deliveryAddress->suburb = $request['suburb'];
						// $deliveryAddress->state = $request['state'];
						// $deliveryAddress->postcode = $request['postcode'];				
						// $deliveryAddress->mobile = $request->mobile;
						// $deliveryAddress->lattitude = "";
						// $deliveryAddress->longitude = "";
						// $deliveryAddress->save();  
						// print_r(DB::getQueryLog());exit;
						$deliveryAddress = Deliveryaddress::join('users','users.userid','=', 'deliveryaddresses.userid')
															 ->where('deliveryaddresses.userid',  $user->userid)
															 ->where('deliveryaddresses.addresstype',  "profileaddress")
															 ->update(['deliveryaddresses.addressline1' => $request['addressline1'],'deliveryaddresses.addressline2' => $request['addressline2'],'deliveryaddresses.suburb' => $request['suburb'],'deliveryaddresses.state' => $request['state'],'deliveryaddresses.postcode' => $request['postcode']   ]);
					  }
					  else{
						$deliveryAddress = new Deliveryaddress;
						$deliveryAddress->userid = $user->userid;
						$deliveryAddress->firstname = $request['firstname'];						
						$deliveryAddress->addresstype = "profileaddress";						
						$deliveryAddress->addressline1 = $request['addressline1'];
						$deliveryAddress->addressline2 = $request['addressline2'];
						$deliveryAddress->suburb = $request['suburb'];
						$deliveryAddress->state = $request['state'];
						$deliveryAddress->postcode = $request['postcode'];				
						//$deliveryAddress->mobile = $request->mobile;
						$deliveryAddress->lattitude = "";
						$deliveryAddress->longitude = "";
						$deliveryAddress->save();
					  }
					 
				 
				 if ( Healthprofile::where('userid',  $user->userid)->where('parentuser_flag' , '1')->exists()) {
					$healthprofile = Healthprofile::where('userid',  $user->userid)->where('parentuser_flag' , '1')->first();
					
				  $healthprofile->gpname = $request['gpname'];
				  $healthprofile->gpclinicname = $request['gpclinicname'];
				  $healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				  $healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				  $healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				  $healthprofile->gpclinicstate = $request['gpclinicstate'];
				  $healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				  $healthprofile->gpclinicphone = $request['gpclinicphone'];
				  $healthprofile->gplink = $request['gplink'];
				  
				  
				  $healthprofile->ismedicare = $request['ismedicare'];
				  $healthprofile->medicarenumber = $request['medicarenumber'];
				  $healthprofile->medicarenumberpos = $request['medicarenumberpos'];
				 $healthprofile->medicare_exp = $request['medicare_exp'];
				 $healthprofile->heathcarecard = $request['heathcarecard'];
				 
				 $healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				 $healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				 $healthprofile->healthfundname = $request['healthfundname'];
				 $healthprofile->healthfundmembershipnumber = $request['healthfundmembershipnumber'];
				 $healthprofile->healthfundmembershipreferencenumber = $request['healthfundmembershipreferencenumber'];
				 
				 $healthprofile->safetynet = $request['safetynet'];
				 $healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				 $healthprofile->veteranaffairsexp = $request['veteranaffairsexp'];
				 $healthprofile->ihi = $request['ihi'];
				 $healthprofile->ihi_exp = $request['ihi_exp'];
				 $healthprofile->dvano = $request['dvano'];				 
				 $healthprofile->dvanoexp = $request['dvanoexp'];
				 
				 $healthprofile->smoking = $request['smoking'];
				 
				 $healthprofile->pregnency = $request['pregnency'];
				 $healthprofile->expectedduedate = $request['expectedduedate'];
				 
				 $healthprofile->lactation = $request['lactation'];
				 $healthprofile->anyallergies = $request['anyallergies'];
				 $healthprofile->allergies = $request['allergies'];
				 $healthprofile->medicalconditions = $request['medicalconditions'];
				 
				 $healthprofile->oncontraception = $request['oncontraception'];
				 $healthprofile->nameofcontraception = $request['nameofcontraception'];
				 
				 $healthprofile->weight = $request['weight'];
				 $healthprofile->medicalprocedure = $request['medicalprocedure'];
				 $healthprofile->descriptionforprocedure = $request['descriptionforprocedure'];
				 $healthprofile->procedureyear = $request['procedureyear'];
				 
				 $healthprofile->parentuser_flag = '1';
				 $healthprofile->profile_relation = 'self';
				 $healthprofile->save();
				 //$healthprofile->save();
				}
				else{
					$healthprofile = new Healthprofile;
					$healthprofile->userid =  $user->userid;
					$healthprofile->gpname = $request['gpname'];
				    $healthprofile->gpclinicname = $request['gpclinicname'];
				    $healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				    $healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				    $healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				    $healthprofile->gpclinicstate = $request['gpclinicstate'];
				    $healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				    $healthprofile->gpclinicphone = $request['gpclinicphone'];
				  
				  
				 $healthprofile->ismedicare = $request['ismedicare'];
				 $healthprofile->medicarenumber = $request['medicarenumber'];
				 $healthprofile->medicare_exp = $request['medicare_exp'];
				 $healthprofile->heathcarecard = $request['heathcarecard'];
				 
				 $healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				 $healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				 $healthprofile->healthfundname = $request['healthfundname'];
				 $healthprofile->healthfundmembershipnumber = $request['healthfundmembershipnumber'];
				 $healthprofile->healthfundmembershipreferencenumber = $request['healthfundmembershipreferencenumber'];
				 
				 $healthprofile->safetynet = $request['safetynet'];
				 $healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				 $healthprofile->veteranaffairsexp = $request['veteranaffairsexp'];
				 $healthprofile->ihi = $request['ihi'];
				 $healthprofile->ihi_exp = $request['ihi_exp'];
				 $healthprofile->dvano = $request['dvano'];				 
				 $healthprofile->dvanoexp = $request['dvanoexp'];
				 
				 $healthprofile->smoking = $request['smoking'];
				 $healthprofile->allergies = $request['allergies'];
				 $healthprofile->medicalconditions = $request['medicalconditions'];
				 
				 $healthprofile->pregnency = $request['pregnency'];
				 $healthprofile->expectedduedate = $request['expectedduedate'];
				 
				 $healthprofile->lactation = $request['lactation'];
				 
				 $healthprofile->oncontraception = $request['oncontraception'];
				 $healthprofile->nameofcontraception = $request['nameofcontraception'];
					 $healthprofile->parentuser_flag = '1';
					 $healthprofile->profile_relation = 'self';
					 $healthprofile->save();
				}
				 
				 
				$response->userDetails 	= $user;
				$response->healthprofile 	= $healthprofile;
				$response->AddressDetails 	= $deliveryAddress;
				$response->msg 		= trans('messages.update_profile');
				$response->status 		= $this->successStatus;
			
		 return json_encode($response);
	}
	/**
      @OA\Post(
          path="/v2/updateDeviceDetails",
          tags={"Device Details"},
          summary="update Device Details",
          operationId="updateDeviceDetails",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="deviceid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="devicetoken",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="devicetype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	public function updateDeviceDetails(Request $request)
	{
		$response=(object)array();
		$devicedetails=(object)array();
		 $user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
		// print_r($request);exit;
		 if($request->devicetoken!="" || $request->devicetype!="" || $user->userid!="")
		 {
			//$user->userid = $user->userid;
			$user->deviceid = $request->deviceid;
			$user->devicetoken = $request->devicetoken;
			$user->devicetype = $request->devicetype;
			$user->save();			
			//$response->deviceDetails 	= $devicedetails;
			$response->msg 		= "Device Details added successfully";
			$response->status 		= $this->successStatus;				
		}
		else
		{
			$response->msg 		= trans('messages.general_messgae');
			$response->status 		= $this->successStatus;
		}
		return json_encode($response);
	}
/**
      @OA\post(
          path="/v2/logout",
          tags={"User"},
          summary="User Logout",
          operationId="User Logout",
    security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function logout(Request $request)
		{ 
			$request->user()->token()->revoke();
			return response()->json([
				'message' => trans('messages.logout_messgae')
			]);
        
		}
		
	/**
      @OA\post(
          path="/v2/gplocations",
          tags={"GeneralPractioner"},
          summary="gplocations",
          operationId="gplocations",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="gpname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function gplocations(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$gpLocations = Gplocations :: where('gpname', 'like', '%'.$request->gpname.'%')->get();
			return response()->json(['gpLocations' => $gpLocations], $this->successStatus);
        
		}	
		
		public function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
    
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $base64_string ) );

        $file = public_path('profilepic/'). $output_file;
        file_put_contents($file, $ifp);
        rename($output_file, 'profilepic/'.$output_file);
        // clean up the file resource
        fclose( $ifp ); 
        return $output_file; 
    }
	
	/**
	 @OA\Post(
          path="/v2/addFamilyMember",
          tags={"User"},
          summary="Add FamilyMember",
          operationId="Add FamilyMember",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="middlename",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="knownas",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profilepic",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			 			 
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="sex",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="permissionforsms",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="occupation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ), @OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			  
			
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ismedicare",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipreferencenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="ihi",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ihi_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvano",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvanoexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="expectedduedate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="procedureyear",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profile_relation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
		 @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 
	public function addFamilyMember(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 //$user_details = User::findOrFail($user->userid);
		
			$familyMember = new User;
				
		 
				$familyMember->username = $request->username;
				$familyMember->password = $request->password;
				$familyMember->firstname = $request->firstname;
				$familyMember->middlename = $request->middlename;
				$familyMember->lastname = $request->lastname;
				$familyMember->knownas = $request->knownas;
				$familyMember->mobile = $request->mobile;
				$familyMember->permissionforsms = $request->permissionforsms;
				$familyMember->dob = $request->dob;
				 $today = date('Y-m-d');
        		 $age =  date_diff(date_create($request['dob']), date_create($today))->y;
				if($request->profilepic!= "")
				{					
					$profilepic=$this->base64_to_jpeg($request->profilepic,time()."-familipic.jpeg");				
				}
				else
				{
					$profilepic = '';
				}
				$familyMember->profilepic = $profilepic;
				$familyMember->sex = $request->sex;
				$familyMember->defaultaddress = $request->defaultaddress;
				$familyMember->homephone = $request->homephone;
				$familyMember->occupation = $request->occupation;
				$familyMember->emergencycontactname = $request->emergencycontactname;
				$familyMember->emergencycontactnumber = $request->emergencycontactnumber;
				$familyMember->occupation = $request->occupation;
				$familyMember->profiletypeid = "6";
				$familyMember->userstatus = "1";
				$familyMember->emailverified = "0";
				$familyMember->hashcode = $request->hashcode;
				$familyMember->parentuserid = $user->userid;
				
				$familyMember->save();
				$familyMember['age'] = $age;
		
		$familyuserid = $familyMember->userid;
		if($familyuserid!="")
		{
			$familyhealthprofile = new Healthprofile;
			$familyhealthprofile->userid = $familyuserid;
			$familyhealthprofile->gpname = $request->gpname;
			$familyhealthprofile->gpclinicname = $request->gpclinicname;
			$familyhealthprofile->gplink = $request->gplink;
			$familyhealthprofile->gpclinicaddressline1 = $request->gpclinicaddressline1;
			$familyhealthprofile->gpclinicaddressline2 = $request->gpclinicaddressline2;
			$familyhealthprofile->gpclinicsuburb = $request->gpclinicsuburb;
			$familyhealthprofile->gpclinicstate = $request->gpclinicstate;
			$familyhealthprofile->gpclinicpostcode = $request->gpclinicpostcode;
			$familyhealthprofile->gpclinicphone = $request->gpclinicphone;
			$familyhealthprofile->medicarenumber = $request->medicarenumber;
			$familyhealthprofile->ismedicare = $request->ismedicare;
			$familyhealthprofile->medicare_exp = $request->medicare_exp;
			$familyhealthprofile->heathcarecard = $request->heathcarecard;
			$familyhealthprofile->pensionerconcessioncard = $request->pensionerconcessioncard;
			$familyhealthprofile->concessionsafetynetcard = $request->concessionsafetynetcard;
			$familyhealthprofile->healthfundname = $request->healthfundname;
			$familyhealthprofile->healthfundmembershipnumber = $request->healthfundmembershipnumber;
			$familyhealthprofile->healthfundmembershipreferencenumber = $request->healthfundmembershipreferencenumber;
			$familyhealthprofile->ihi = $request->ihi;
			$familyhealthprofile->ihi_exp = $request->ihi_exp;
			$familyhealthprofile->dvano = $request->dvano;
			$familyhealthprofile->dvanoexp = $request->dvanoexp;
			$familyhealthprofile->veteranaffairsnumber = $request->veteranaffairsnumber;
			$familyhealthprofile->veteranaffairsexp = $request->veteranaffairsexp;
			$familyhealthprofile->safetynet = $request->safetynet;
			$familyhealthprofile->anyallergies = $request->anyallergies;
			$familyhealthprofile->allergies = $request->allergies;
			$familyhealthprofile->medicalconditions = $request->medicalconditions;
			$familyhealthprofile->pregnency = $request->pregnency;
			$familyhealthprofile->expectedduedate = $request->expectedduedate;
			$familyhealthprofile->lactation = $request->lactation;
			$familyhealthprofile->smoking = $request->smoking;
			$familyhealthprofile->oncontraception = $request->oncontraception;
			$familyhealthprofile->nameofcontraception = $request->nameofcontraception;
			$familyhealthprofile->weight = $request->weight;
			$familyhealthprofile->medicalprocedure = $request->medicalprocedure;
			$familyhealthprofile->descriptionforprocedure = $request->descriptionforprocedure;
			$familyhealthprofile->procedureyear = $request->procedureyear;
			$familyhealthprofile->profile_relation = $request->profile_relation;
			$familyhealthprofile->parentuser_flag = "0";
			$familyhealthprofile->save();
		}
		
			$familyprofile_completeness=0;
			 if(isset($familyMember->firstname)){$familyprofile_completeness=$familyprofile_completeness+20;}
			 if(isset($familyMember->lastname)){$familyprofile_completeness=$familyprofile_completeness+10;}
			 if(isset($familyMember->dob)){$familyprofile_completeness=$familyprofile_completeness+10;}
			 if(isset($familyMember->mobile)){$familyprofile_completeness=$familyprofile_completeness+10;}
			 if(isset($familyMember->profilepic) && $familyMember->profilepic!='')
			 {$familyprofile_completeness=$familyprofile_completeness+30;}
			 if(isset($familyhealthprofile->medicarenumber) || isset($familyhealthprofile->heathcarecard) || isset($familyhealthprofile->veteransaffairscard) || isset($familyhealthprofile->safetynet))
			 {$familyprofile_completeness=$familyprofile_completeness+20;}
		
			
		$response->familyMember 	= $familyMember;
		$response->familyhealthprofile 	= $familyhealthprofile;
		$response->familyprofile_completeness 	= $familyprofile_completeness;
		
		return json_encode($response);
		
	}
	
	/**
	 @OA\Post(
          path="/v2/updateFamilyMember",
          tags={"User"},
          summary="Update FamilyMember",
          operationId="Update FamilyMember",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="middlename",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="knownas",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profilepic",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			 			 
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="sex",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="permissionforsms",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="occupation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			  
			
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="ismedicare",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipreferencenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="ihi",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ihi_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvano",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvanoexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="expectedduedate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="procedureyear",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profile_relation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
		 @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 
	public function updateFamilyMember(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 //$user_details = User::findOrFail($user->userid);
		
		if($request->userid !="" && $request->profileid !="")
		{			
				
		 if ( User::where('userid',  $request->userid)->where('parentuserid' , $user->userid)->where('userstatus' , "1")->exists()) {
				$familyMember = User::where('userid',  $request->userid)->where('parentuserid' , $user->userid)->first();
			$familyMember->username = $request->username;
			$familyMember->password = $request->password;
			$familyMember->firstname = $request->firstname;
			$familyMember->middlename = $request->middlename;
			$familyMember->lastname = $request->lastname;
			$familyMember->knownas = $request->knownas;
			$familyMember->mobile = $request->mobile;
			$familyMember->permissionforsms = $request->permissionforsms;
			$familyMember->dob = $request->dob;
			$today = date('Y-m-d');
        	$age =  date_diff(date_create($request['dob']), date_create($today))->y;
			if($familyMember->profilepic == "" && $request['profilepic'] !="")
			{
			
				$familyMember->profilepic=$this->base64_to_jpeg($request['profilepic'],time()."-familipic.jpeg");
				
			}
			 elseif($familyMember->profilepic !="" || !empty($familyMember->profilepic))
				 {
					 
					 $supported_image = array(						
						'jpeg'
					);

					$src_file_name = $request['profilepic'];
					$ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
					if (in_array($ext, $supported_image))
					{	
						//echo "12fdsfds3";exit;
					 $familyMember->profilepic = $familyMember->profilepic;
					}
					else
					{
						//echo "123";exit;
					$familyMember->profilepic=$this->base64_to_jpeg($request['profilepic'],time()."-familipic.jpeg");
					}
				 }
			elseif($request['profilepic'] =="")
			{
				$familyMember->profilepic = "";
			}
			
			$familyMember->sex = $request->sex;
			$familyMember->defaultaddress = $request->defaultaddress;
			$familyMember->homephone = $request->homephone;
			$familyMember->occupation = $request->occupation;
			$familyMember->emergencycontactname = $request->emergencycontactname;
			$familyMember->emergencycontactnumber = $request->emergencycontactnumber;
			$familyMember->occupation = $request->occupation;
			$familyMember->profiletypeid = "6";
			$familyMember->userstatus = "1";
			$familyMember->emailverified = "0";
			$familyMember->hashcode = $request->hashcode;
			$familyMember->parentuserid = $user->userid;
			
			$familyMember->save();
			
        	$familyMember['age'] =	 $age;
		
		$familyuserid = $familyMember->userid;
		
		if ( Healthprofile::where('userid',  $request->userid)->where('parentuser_flag' , '0')->exists()) {
				$healthprofile = Healthprofile::where('userid',  $request->userid)->where('parentuser_flag' , '0')->first();
					
				$healthprofile->gpname = $request['gpname'];
				$healthprofile->gpclinicname = $request['gpclinicname'];
				$healthprofile->gplink = $request['gplink'];
				$healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				$healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				$healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				$healthprofile->gpclinicstate = $request['gpclinicstate'];
				$healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				$healthprofile->gpclinicphone = $request['gpclinicphone'];


				$healthprofile->ismedicare = $request['ismedicare'];
				$healthprofile->medicarenumber = $request['medicarenumber'];
				$healthprofile->medicare_exp = $request['medicare_exp'];
				$healthprofile->heathcarecard = $request['heathcarecard'];

				$healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				$healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				$healthprofile->healthfundname = $request['healthfundname'];
				$healthprofile->healthfundmembershipnumber = $request['healthfundmembershipnumber'];
				$healthprofile->healthfundmembershipreferencenumber = $request['healthfundmembershipreferencenumber'];

				$healthprofile->safetynet = $request['safetynet'];
				$healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				$healthprofile->veteranaffairsexp = $request['veteranaffairsexp'];
				$healthprofile->ihi = $request['ihi'];
				$healthprofile->ihi_exp = $request['ihi_exp'];
				$healthprofile->dvano = $request['dvano'];				 
				$healthprofile->dvanoexp = $request['dvanoexp'];

				$healthprofile->smoking = $request['smoking'];

				$healthprofile->pregnency = $request['pregnency'];
				$healthprofile->expectedduedate = $request['expectedduedate'];

				$healthprofile->lactation = $request['lactation'];
				$healthprofile->anyallergies = $request['anyallergies'];
				$healthprofile->allergies = $request['allergies'];
				$healthprofile->medicalconditions = $request['medicalconditions'];
				$healthprofile->oncontraception = $request['oncontraception'];
				$healthprofile->nameofcontraception = $request['nameofcontraception'];

				$healthprofile->weight = $request['weight'];
				$healthprofile->medicalprocedure = $request['medicalprocedure'];
				$healthprofile->descriptionforprocedure = $request['descriptionforprocedure'];
				$healthprofile->procedureyear = $request['procedureyear'];

				//$healthprofile->parentuser_flag = '0';
				$healthprofile->profile_relation = $request['profile_relation'];
				$healthprofile->save();
				}
				
						
		 
			$response->familyMember 	= $familyMember;
			$response->familyhealthprofile 	= $healthprofile;
			$response->msg 		= trans('messages.update_profile');
			$response->status 		= $this->successStatus;
		}
		else
		 {
			 $response->msg 		= trans('messages.general_messgae');
			$response->status 		= $this->successStatus;
		 }
			
			return json_encode($response);
		}
	}
	
	/**
      @OA\Get(
          path="/v2/getAllProfiles",
          tags={"User"},
          summary="All Profiles",
          operationId="All Profiles",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getAllProfiles() {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		
		$profile = User :: leftJoin('healthprofile','healthprofile.userid', '=' ,'users.userid')
							 ->where('users.parentuserid', '=',  $user->userid);
							//->union($userProfile);
							 //->get();
		
		$profileDetails = User :: leftJoin('healthprofile','healthprofile.userid', '=' ,'users.userid')
							 ->where('users.userid', '=',  $user->userid)
							 ->union($profile)
							 ->get();
		$numrows = $profileDetails->count();
		for($i=0;$i<$numrows;$i++)
		{
			$today = date('Y-m-d');
			$age[$i] =  date_diff(date_create($profileDetails[$i]['dob']), date_create($today))->y;
			$profileDetails[$i]['age'] =$age[$i];
		}
		$response->profileDetails 	=  $profileDetails;		
		$response->status 		= $this->successStatus;
		
		
		return json_encode($response); 
	}
	
	/**
      @OA\Post(
          path="/v2/updatehealthprofile",
          tags={"User"},
          summary="Update Healthprofile",
          operationId="Update Healthprofile",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			@OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="heathcarecard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	public function updatehealthprofile(Request $request)
	{
		if ( Healthprofile::where('profileid',  $request->profileid)->exists()) {
					$healthprofile = Healthprofile::where('profileid',  $request->profileid)->first();
				
				$userdetails = User::where('userid',  $healthprofile->userid)->get();
				$bday = $userdetails[0]['dob'];
				$today = date('Y-m-d');
        		$age =  date_diff(date_create($bday), date_create($today))->y;	
				$healthprofile->ismedicare = $request['ismedicare'];
				$healthprofile->medicarenumber = $request['medicarenumber'];
				$healthprofile->medicare_exp = $request['medicare_exp'];
				$healthprofile->heathcarecard = $request['heathcarecard'];
				$healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				$healthprofile->safetynet = $request['safetynet'];
				$healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				
				$healthprofile->weight = $request['weight'];
				$healthprofile->smoking = $request['smoking'];
				$healthprofile->pregnency = $request['pregnency'];
				$healthprofile->oncontraception = $request['oncontraception'];
				$healthprofile->nameofcontraception = $request['nameofcontraception'];
				$healthprofile->descriptionforprocedure = $request['descriptionforprocedure'];
				$healthprofile->lactation = $request['lactation'];
				$healthprofile->anyallergies = $request['anyallergies'];
				$healthprofile->allergies = $request['allergies'];
				$healthprofile->medicalconditions = $request['medicalconditions'];
				$healthprofile->save();
				$healthprofile['age'] = $age ;
				
				return response()->json(['healthprofile' => $healthprofile], $this->successStatus);
		}
	}
	
	/**
      @OA\post(
          path="/v2/updategpinfo",
          tags={"User"},
          summary="Update gplocations Info",
          operationId="Update gplocations Info",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ), @OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function updategpinfo(Request $request)
		{ 
			if ( Healthprofile::where('profileid',  $request->profileid)->exists()) {
					$healthprofile = Healthprofile::where('profileid',  $request->profileid)->first();
				
				  $userdetails = User::where('userid',  $healthprofile->userid)->get();	
				  
				  $healthprofile->gpclinicname = $request['gpclinicname'];
				  $healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				  $healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				  $healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				  $healthprofile->gpclinicstate = $request['gpclinicstate'];
				  $healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				  $healthprofile->gpclinicphone = $request['gpclinicphone'];
				  $healthprofile->gplink = $request['gplink'];
				  $healthprofile->save();
				
				return response()->json(['healthprofile' => $healthprofile], $this->successStatus);
			}
        
		}
		
		/**
      @OA\post(
          path="/v2/medicalconditions",
          tags={"User"},
          summary="Medical Conditions",
          operationId="Medical Conditions",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="name",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function medicalconditions(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$medicalconditions = Medicalconditions :: where('name', 'like', '%'.$request->name.'%')->get();
			return response()->json(['medicalconditions' => $medicalconditions], $this->successStatus);
        
		}
		
		/**
      @OA\post(
          path="/v2/allergies",
          tags={"User"},
          summary="Allergies",
          operationId="Allergies",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="allergyname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function allergies(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$allergies = Allergies :: where('allergyname', 'like', '%'.$request->allergyname.'%')->get();
			return response()->json(['allergies' => $allergies], $this->successStatus);
        
		}
		
		
		/**
      @OA\post(
          path="/v2/vaccination",
          tags={"User"},
          summary="Vaccin Information",
          operationId="Vaccin Information",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="vaccinename",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function vaccination(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$vaccination = Immunisation :: where('vaccinename', 'like', '%'.$request->vaccinename.'%')->get();
			return response()->json(['vaccination' => $vaccination], $this->successStatus);
        
		}
		
		
		/**
      @OA\post(
          path="/v2/forgotpassword",
          tags={"User"},
          summary="Forgotpassword",
          operationId="Forgotpassword",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function forgotpassword(Request $request)
		{ 
		$response=(object)array();
		
			$users_existance=User :: where('username',$request->username)
									->where('userstatus','=',1)
									->where('profiletypeid','=',1)
									->get();
			if($users_existance->count() > 0)
			{				
			if($users_existance[0]['username'] != $request->username)
			{
				$response->msg 		= trans('messages.email_not_exist');
				$response->status 		= $this->successStatus; 
			}
			else
			{
				$link=url('resetpass/'.$users_existance[0]['hashcode']);
				$subject="Medmate – Forgot your password";
				//$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Reset Password</button></a>";
				//$body_message = trans('messages.Forgot_password_email');
				//$body_message 	= 	str_replace('Reset_password_button',$content,$body_message);
				$body_message 	= 	'
				<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <title>Document</title>
</head>
<style>
    body  {
        font-family: "Nunito";font-size: 18px;
    }
    .top {
        display: flex;flex-direction: row;width: 700px;justify-content: space-between;
    }
    .heading {
       color: #1d9bd8;font-size: xx-large;font-weight: 600;
    }
    .total {
        width: 700px;  margin: auto;
    }
    .bottom {
        text-align: center;color :#7e7e7e;
    }
    .one {
        background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;width: 120px;height: 35px;margin-left: 300px;margin-top: 50px;
    }
</style>
<body>
<div class="total">
    <div class="top">
        <p style="padding-top: 15px;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAd0AAABqCAMAAADOW3slAAAAn1BMVEX///8dmtZYWFpBqNxRUVMAldQAltRQUFJ/vONquOJUVFZLS01HR0r09PSwsLG73fGGxefX19diYmOnp6jLy8za7vh4v+QAktOv2O+GhoehoaLj8voon9jE4/NXsN/39/d7e3zu9fvm5uZeXmBra23V1dXg4OCf0OvCwsN/f4CWy+rS6/bp9fuSkpOampv1/P44ODu9vb1itOBMrd1ApNpRVRAqAAAOnklEQVR4nO2deXuyOhPGFVmO4L7UDat1rVrrW+v3/2wvi9JkZgJBeUyL3n+c6zySBDo/EkIyMxQKrHb9Rvm7lAcdy9vxqPBUpG7DNQyzmBcZpnnqH1Qb9Zfoo1w0VAPJXIa7Vm3XX6F+Dtn6Ml67qk2rXo18svVkunXVxlWtl9zC9WQ+ON5cw/VG57FqA6vUOt9wvd77odrE6tTNz1uQQOa3ahur03fu6RZ7fdVGVqVx/uEWiyfVVlalV9WWv4d6Dzpv/niErls0y6rtrEb1h6BbLD7mivPLY9Dt7VQbWom+Vdv9PjIecztBtdnvJKOh2tBKpNrsd5L5n2pDK5Fqs2clo8cJHn7S/ctyx11WoyM4/qT7l+UCJ6oyeBV40v3LetIlpQZG5nrSJaUGRubKC91Zk9PyxubUwMhceaHbrjis/ndjc2pgZK680K1ZGqvKjc2pgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJpbCgaQQOEOL0C8bZQcKQDjvzmgxkpknpEFVifsuM7qy5XC6bs4QCy9VEsrnle9tXZ1mVKp+ObrXZqbUHg3271mmS7WNEvAvLxUqmcfqvv/soFEbj9X8uAcMwXxv93ahweNvVt68ytIyeW26s+3VP68bxJHdPmL1Tebs+1ylGl5eOLr8RExlm0tm3NFv3pM0H74S5VrXp/FJg0W4mkJosveZ0x/Ll6Nqidrlnvqa8wtK+Cm1IdxIJNt8cLjTba9q2vf86ttf+KpGu0X/jtA7MZJrlHePvfRgfgfWM7zUXRvmxdeP5GuZxPWJdyA+jdeI9YRrf2xF3HS/njBCp6FYrNqtKJ/y1trD0yK62Mx8Cvp2pZtk/BSx72iGpFojmggraPrR/2+HO7/i/reYtT4u5xmvROmte41v/WujMxYguCBkQRNz0fesZR5R9YvfNdDXjVEe+/m+NOExug8o3tCubcXzN1zE6zUejaKany5nFCWzSWfDdRtOs1jtTabUB1vTtORCOt+8tVFzTdG3od0HQQfWg+QoqzV9Mm2l8Uls4uHX/gqb8gCJFlwqQPDQiA5oNMo5jfBKgMoskW1+7o3B8Nkp0qFf31ciA7idhLtsaRuNhTdPRcf8GwKNhcIaBI4A0nQnoCipQdJtTkm1wyfqQHcIl6LqCtDGXmH1h3pERjdcoxeWhWYvgvghDgbbmrXSrU9q4zj6sMdkLjG/blPvEqgXHgZ8KWvNWujVbxDYoOGWmhIl0eyehVQO85knManSiMCWEBXTJeyI2VKR+I90JGpWjY59+hclUTGuO59fNWPNXVrWb6IqGheiK9J8rSqRrxkTPlf0xMS4pRRePs0ZiHPyhhGv14jObjIs30LXep9SoG8r58iqI4XrGXMDZbDxcTZvv+eOp6IpvxJ8r+sGbSDc2m5dbLMZn++rDqABTJhivBHtvAlwP7w10tXkcDXtWGMQa1Bnyp16Jb5VLk/w/U9FdJLbO4k2im2DUxKB2GGMoF2npgv6eOoIvFd1Y6fuvpKGQm1lNWvE9lziDX02S7iax5wZXdBlPbqNbSBxmd1znTR6WQ404OEb6yPns6MKehqXv2TMPJDoXqO9Xk6PbTigVlT5f0o10k1ViMUnHWfbZR694XidUhnQlxEysOlKdi5M83aV04+E7/L+n22fs/C2PiVkMu+aS7krXYtaRUo/LKehOFtKNn8fmf073jcGUIr1B96fW8Yqz3pWuvYiafpccOllJ061RZWxbp+bowVz/39P9mVcZL2mqRckr09wTka6gG6wKE8uHP3b0x0WdGhytaEGyJajpOI6waVm6Vdy4bc0Xm8/NQkNXFXZeObqj9X/l8jbOzPVGudwgc5lvo20mcml5vd1uqdeu0S1dNz1dXWsvZ7PVu3BSaluDzmo2a7aJJUnrsnq/pBhZ2qDW6XRqU5tu+7zO7G896fAO0C9y2kTXtTad8L6afc3hVTn+IrkM3UO5Z5ieeiUR37VpeCUMw9jiY+NzJzRx1z1s3V6wVdsj1pDPgOh3rtH66HoqrwVrKWnphmtSvlbITqGZF5f3nsknqh49ePcE+Xm0F1GtWVTbAd3Z59DXFCx0DC/67KBxwdbYLSF4VcHjQoLu6GfjRpBw8Ri99hh44nTphD3UdcdMQvfeK6y4Cw9SE+bR63lj1zR6ZZJvSrrsvKhArVzpU2ZFqgbr24PwwAR33Qq7t1OobojOrTMFYnbvO6CqDhbJ4FX5r+HJdA/sYYPqSWx+Z/xuejgvTaBEq1tuwdEsvYF6wQObeonqs/uERpFax0pH1/pk61bhJqvfEbitvk8dHg5/hwAukxu2KsYrSXcAtnMXcPcRbE74d2wyXT5f2ekNHo+G3rO1Ufd26d/7YDUZ5dkN0vkTa5Awn7RJ4E1HFxjqHXVeC2yLI/7hz0NY0eL33H3t0cNXju4EnNPBuxf8sO7fcol0R7wt8ZP1ABYb0VAaHu+BEXSEdoJg0+EyFxqYce5DYladii5iAJ5+mj0FBb4AopDCBNbTB+jCChM085Wju+TvHG43/6wmX0SvJtOFH8RA8HagABq8wyyyLvgV2t8/N9/0m9/p0Xh+cFG9YgndAul2EWA3ADbG42sVAKqQv2oatbePhm85uuCITbnt8XeXtUykeyjBArCjNKAh4dw4sDT8ldgbRJsFR9/tA/4FW3xXEANKGrqoZ6JtHgc52IBeWgkKzMBdwy9AR4IrTnJ0+ceu/omaLcBlUOsrke4bpICenyjBM5w2Bw9uE1Qjv5HjojJ4d4h093DhbCANXWyp6oK3P9rChU/YkC7slQ7tVfcORgY5uvyAbk8HhDbw70qiu0MbtKATos6N9vMDuvDDIqMuITAsePMuNMzXSb8rVCzV7j2e+oD5CX58ghXHkC58Grdon7oZGBnk6MKpACm+yCaRLrbmK1/gAz8HwYtt2Hev+KaXNxnvwWr0d7Hop4EkXaKL8cOgjUfYJUUXvJLgEf8sMPuVoltNvzuhLeR8IuPojuBxmm7xiq/GjHqYriDjcAkUy5SuPkQFOjJ0iRlzILggxRwS0p1dQVe7nW4XHhf03SvoHjy68DdixuwLzuR/A12iWijKr+osId1kf55/Qle2716Ruv5gSNOFz/pfQZec1xaedEN5r7Z/ue9mOjJf89y9I91rRmYXrXAV0Az9F9OdCv4uwCAHdK967rp4VkUscREXpYYufCMiHNl9ZfJG9MvoXuFh4c2q0Pc36U9SosUqJXQlVzPgOqccXbDAMm9J6G504XrDbpws710brYyNUFL8QEld/C504UqkvUH1fMENpqtWIolNBEL3omuCJcVtz0iWVw2tIFMvvNg7RwlduEmn6VQIGXJ+v2oXQUftUroX3SIA8EZAkuIGt6SCUmghTAndwgYuFy5QxUIVbd/L0W3ybVM7gFh3o2uAE2/JBygS3Dik1iKJQBQ1dMGkmdrgrWKf5Ot27yt05DCvu9FF32qT++4vmjQXDiirA+FJq4ZuE3stgqpVwmlL0vMGeuQJZuS+ZhdHvbvRRdPaD2FmDeNU/vl/3C8PfHx+j3KTVkMXe+Ro1oaFsKRC+GS95sDAYAvi/gurvXaZzt2NLh5j34g4XT/twnf/wLhqwTfZgu8oW4zgGS7ppqmILnSn04JcGbNwe7ja3JORvZJ0UZyJrdWwg8bsa2rp0QPhfnQJx2SG0plV8bT1h2Jm39GgfNy7jZOftsr0itOrJIroNqkoB8ta7Ift4X7h0GvFknTRe7LX8qLGbiFPmu2p7afUsO9Pl+qFo61HyfQd3YN8ZK/r88OZoYt9b0LtxvX6WLhprIgu8qoJZeu6hcIMUtMlI02s6acf6NCptfce2XM0iwq69Ddwu/V14+Wlsa2z4fOszwDyqpGRKrrYozlZsnTxm3JAUveDlByLvX1U0EVb7GKxdK/6dK4quoLOmw1d5IkrkhK68pw4B/jYvCsCKaObFMt3E92VZHS2ErpJKVR+tGPhXJFYQR3dwjB18L08XXps/jV0sVs5LX658YqYYnV0ySCzrOhSYUgU3fu/7/oyJINxwWIyzDWWLIV0Z4nJFa7yVj9LCq90zpts6crG3wO6ZuJ87PAbfDPOasanybFbn1d43kTCUWa/h65k74UbQSg+EOhQBk90lXQLzbj0ZrY9i9nMk8i+3Y7L/qCYrtcPJabAO7gEbaLgbVaH4y2ZBDOnW5iJ0/3p8xWZBfQsmdzqHTo7wK+gWzTJcGpeXRz+eRI/ew+vxu35mTndSNcbPyt0B3OmVTqD71lSmfOrotbPsvRLzOL96RaLvWPCLGlMeT4K35bHxZsz+ELdTLewbBE5lC07MPutdL3BYVMR9V+9Mv+JR1VBt2gaLzE9sU/uHfl5nalO/+Enmk2kyyVouAddP9F+hXtC6o59zsXPZ87XHaaS9FcvZsM5ypFk61Zl/slmV4duSjBNwT+h6w/Pxzr1/B2Ny67YacP8hhn6u40ws0pSLALX8av/4z9QjeluuE9YV3BQQQc0QUb7zdqbeeX81YtKa/9+2a97B4GZTJVaxWIV++3sZXs6d5xLUadiT/c1sKOPTH9scDoiE5/4Ai8YwgtfgnYwNw33uN5F37A4vI269UbJjf0wglfLPfa7I//953AY7bbf0RZiwjn5HYwl/3F5jGbFF8BeEFXQhIhAddb5qrVrX8uZ3CdulsM2K1Ek0k/zS695T1/eGar4FDgLBXRLROILUB0zsYmoJfdUeg1UOrmm1DdrTMMMKnkV2HMnnfOKQJcc6EPKu+mvy7xiqToXQokR8qjEJOF51fgROu9JtZWVSc7z9E9LkADxEYTXhfKmpFXqXIsOq8uRrokuzY9e8o0XRYk+mHKNN/ts8X9NdBaoPMgUfqXwgdQv5pOv8XpFFrT86aOcQ76Ge40vdC7VbbjiT9r/PRmmeSK/wvGw2vUb5e9SHnQsb+GHHx9Q/wcKM58K8oW5AQAAAABJRU5ErkJggg==" width="180" height="40"></p>
        <p class="heading">Password Reset </p>
    </div>
    <div>
        <p>Forgot your password?</p>
        <p>No problem. Click the below link and create a new password.</p>
        <p style="height: 150px;width:700px;background-color: #f2f2f2;"><a href="'.$link.'"><button class="one">Reset Password </button></a></p>
        <p>Didn’t ask to reset your password? No worries. <br> Ignore this email and we’ll keep it the same.</p>
        <p>If you have any questions, please email <a href="">enquires@medmate.com.au</a> </p>
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
            Empowering Healthcare Consumers with Choice, Value, & Convenience <br> medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
				';
				$message['to']           = $users_existance[0]['username'];
                $message['subject']      = $subject;
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			}
			}
			else
			{
				$response->msg 		= trans('messages.email_not_exist');
				$response->status 		= $this->successStatus; 
			}
			
			return json_encode($response); 
		}
		
		public function testPush(Request $request){
            $this->common = new CommonController($request);
            $response=(object)array();
             if($request->devicetype==1){
            //$registration_ids[0]=  'fyWvqFdv3DI:APA91bEJ7RSmmoZTXuLREnGeeAKHMoJ_3PCo5WkfobO1k6xWcpZP15fp4qkxh274WkAfFMjzFkHMvEDP3K4KnrgBNNFfQ_zyghsJ2Bqrckp6KgzAUqnkqYBjkHxGCRZD6g6tUQVe9K5R';
            $registration_ids[0]=  $request->device_token;
                  // $data=array('body' => 'Hello world');
                    $data= (object) array(
                        'google.delivered_priority'           => 'high',
                         'google.original_priority'      => "high",
                         'orderid'        => "12",
						 'usertoken' =>"e1Armo1o9kJjq6nOU4i_Lb:APA91bHH6xYPFEOXarHG3ho_4Za50T6UTxlMqxaamw4qukgOw_2SlHtOzmftTbYWhnfWFPua193ZXFSrM3y0y-3inzrQTAaBzT86keX3YBIgFrnKKk0foumlb4VNz74Hum_oy0F4B8TV"
                          );
						  
   
                   $fields = array('registration_ids' => $registration_ids,'body'=>'helloworld','data' => $data,"notificationType" => "1", "title"=> "test",'priority' => 'high', 'notification' => array(
        'title' => 'This is title',
        'body' => 'This is body'
    ));
             $msgEn = 'hello ';
            // $msgAr = 'h12';
			print_r(json_encode($fields)); 
             $re=$this->common->sendPushNotification($fields);
			// print_r($re);exit;
			 }elseif($request->devicetype==2){
            // print_r($fields); die('end');
            //$this->common->notification('en',38,2,$msgEn,$msgAr,100,1);
            $this->common->iPhonePushNotification('hello world',$request->devicetoken,'C','test','1');
            
             }
             $response->msg 		= "success";
             $response->status 		= $this->successStatus; 
             return json_encode($response);    
        }
		
	/**
      @OA\Get(
          path="/v2/getNotifications",
          tags={"Notifications"},
          summary="Notifications",
          operationId="Notifications",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getNotifications() {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);

		$notifications = Notifications :: where('userid', '=', $user->userid)->where('status','=',1)->get();
			return response()->json(['notifications' => $notifications], $this->successStatus);
		
		
	}
	
	/**
      @OA\Get(
          path="/v2/getNotificationsCount",
          tags={"Notifications"},
          summary="Notifications Count",
          operationId="Notifications Count",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getNotificationsCount() {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);

		$notifications = Notifications :: where('userid', '=', $user->userid)->where('isopen','=',1)->get();
		$notificicationscount = $notifications->count();
			return response()->json(['notificicationscount' => $notificicationscount], $this->successStatus);
		
		
	}
	
	/**
      @OA\post(
          path="/v2/updatenotification",
          tags={"Notifications"},
          summary="Update Notifications",
          operationId="Update Notifications",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="notificationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function updatenotification(Request $request)
		{ 
			$notifications = Notifications :: where('notificationid', '=', $request->notificationid)
												->update(['notifications.isopen' => '0']);
			
			
			return response()->json(['notifications' => $notifications], $this->successStatus);
		}
	/**
      @OA\Get(
          path="/v2/getFamilyMember",
          tags={"User"},
          summary="FamilyUserProfile",
          operationId="FamilyUser Profile",
   security={

      {"bearerAuth": {}}
    },
	
	@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getFamilyMember(Request $request) {
		$response=(object)array();
		$user = Auth::user();
		$userdetails = User :: where('userid', $request->userid)
								->where('userstatus' , 1)
								->where('parentuserid' , $user->userid)
								->get();
		$healthprofile = Healthprofile::where('userid', $request->userid)->get();
		
		$today = date('Y-m-d');
        $age =  date_diff(date_create($userdetails[0]['dob']), date_create($today))->y;
		if(!empty($userdetails[0]['profilepic']))
		{
			$userdetails[0]['profilepic'] = url('/profilepic/'.$userdetails[0]['profilepic']);
		}
		elseif($userdetails[0]['profilepic'] == "")
		{
			$userdetails[0]['profilepic'] ="";
		}
		
		$family_profile_completeness=0;
		if(isset($userdetails[0]['firstname'])){$family_profile_completeness=$family_profile_completeness+20;}
		if(isset($userdetails[0]['lastname'])){$family_profile_completeness=$family_profile_completeness+10;}
		if(isset($userdetails[0]['dob'])){$family_profile_completeness=$family_profile_completeness+10;}
		if(isset($userdetails[0]['mobile'])){$family_profile_completeness=$family_profile_completeness+10;}
		if(isset($userdetails[0]['profilepic'])){$family_profile_completeness=$family_profile_completeness+30;}
		if(isset($healthprofile[0]['medicarenumber']) || isset($healthprofile[0]['heathcarecard']) || isset($healthprofile[0]['veteransaffairscard']) || isset($healthprofile[0]['safetynet']) || isset($healthprofile[0]['pensionerconcessioncard']) || isset($healthprofile[0]['concessionsafetynetcard']))
		{$family_profile_completeness=$family_profile_completeness+20;}
		
		
		$response->family_profile_completeness 	=  $family_profile_completeness;
		$response->userdetails 	=  $userdetails;
		$response->healthprofile 	=  $healthprofile;
		$response->age 	=  $age;
		
		$response->msg 		= "success";
		$response->status 		= $this->successStatus; 
		return json_encode($response);
	}
	/**
      @OA\Post(
          path="/v2/emailverification",
          tags={"User"},
          summary="Email Verification",
          operationId="Email Verification",
   security={

      {"bearerAuth": {}}
    },
	
	@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */
	public function emailverification(Request $request)
	{
		$response=(object)array();
		
		$user = User :: where('userid',$request->userid)
						->where('profiletypeid',1)
						->where('userstatus',1)
						->get();
						
		if($user->count() > 0){
				$link=url('verifyaccount/'.base64_encode($request->userid));
				$subject="Medmate – Complete your registration";
				//$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Verify Email</button></a>";
						  // $body_message = trans('messages.Verify_Email_Template');
                   
              // $body_message 	= 	str_replace('verify_button',$content,$body_message);
				$body_message ='
				<!DOCTYPE html>
			<html lang="en">
			<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
			<title>Email Verification</title>
			</head>
			<style>
				body  {
					font-family: "Nunito";font-size: 18px;
				}
				.top {
					display: flex;flex-direction: row;width: 700px;justify-content: space-between;
				}
				.heading {
				   color: #1d9bd8;font-size: xx-large;font-weight: 600;
				}
				.total {
					width: 700px;  margin: auto;
				}
				.bottom {
					text-align: center;color :#7e7e7e;
				}
				.one {
					background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;width: 120px;height: 35px;margin-left: 300px;margin-top: 50px;
				}
			</style>
				<body>
				<div class="total">
					<div class="top">
						<p style="padding-top: 15px;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAd0AAABqCAMAAADOW3slAAAAn1BMVEX///8dmtZYWFpBqNxRUVMAldQAltRQUFJ/vONquOJUVFZLS01HR0r09PSwsLG73fGGxefX19diYmOnp6jLy8za7vh4v+QAktOv2O+GhoehoaLj8voon9jE4/NXsN/39/d7e3zu9fvm5uZeXmBra23V1dXg4OCf0OvCwsN/f4CWy+rS6/bp9fuSkpOampv1/P44ODu9vb1itOBMrd1ApNpRVRAqAAAOnklEQVR4nO2deXuyOhPGFVmO4L7UDat1rVrrW+v3/2wvi9JkZgJBeUyL3n+c6zySBDo/EkIyMxQKrHb9Rvm7lAcdy9vxqPBUpG7DNQyzmBcZpnnqH1Qb9Zfoo1w0VAPJXIa7Vm3XX6F+Dtn6Ml67qk2rXo18svVkunXVxlWtl9zC9WQ+ON5cw/VG57FqA6vUOt9wvd77odrE6tTNz1uQQOa3ahur03fu6RZ7fdVGVqVx/uEWiyfVVlalV9WWv4d6Dzpv/niErls0y6rtrEb1h6BbLD7mivPLY9Dt7VQbWom+Vdv9PjIecztBtdnvJKOh2tBKpNrsd5L5n2pDK5Fqs2clo8cJHn7S/ctyx11WoyM4/qT7l+UCJ6oyeBV40v3LetIlpQZG5nrSJaUGRubKC91Zk9PyxubUwMhceaHbrjis/ndjc2pgZK680K1ZGqvKjc2pgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJpbCgaQQOEOL0C8bZQcKQDjvzmgxkpknpEFVifsuM7qy5XC6bs4QCy9VEsrnle9tXZ1mVKp+ObrXZqbUHg3271mmS7WNEvAvLxUqmcfqvv/soFEbj9X8uAcMwXxv93ahweNvVt68ytIyeW26s+3VP68bxJHdPmL1Tebs+1ylGl5eOLr8RExlm0tm3NFv3pM0H74S5VrXp/FJg0W4mkJosveZ0x/Ll6Nqidrlnvqa8wtK+Cm1IdxIJNt8cLjTba9q2vf86ttf+KpGu0X/jtA7MZJrlHePvfRgfgfWM7zUXRvmxdeP5GuZxPWJdyA+jdeI9YRrf2xF3HS/njBCp6FYrNqtKJ/y1trD0yK62Mx8Cvp2pZtk/BSx72iGpFojmggraPrR/2+HO7/i/reYtT4u5xmvROmte41v/WujMxYguCBkQRNz0fesZR5R9YvfNdDXjVEe+/m+NOExug8o3tCubcXzN1zE6zUejaKany5nFCWzSWfDdRtOs1jtTabUB1vTtORCOt+8tVFzTdG3od0HQQfWg+QoqzV9Mm2l8Uls4uHX/gqb8gCJFlwqQPDQiA5oNMo5jfBKgMoskW1+7o3B8Nkp0qFf31ciA7idhLtsaRuNhTdPRcf8GwKNhcIaBI4A0nQnoCipQdJtTkm1wyfqQHcIl6LqCtDGXmH1h3pERjdcoxeWhWYvgvghDgbbmrXSrU9q4zj6sMdkLjG/blPvEqgXHgZ8KWvNWujVbxDYoOGWmhIl0eyehVQO85knManSiMCWEBXTJeyI2VKR+I90JGpWjY59+hclUTGuO59fNWPNXVrWb6IqGheiK9J8rSqRrxkTPlf0xMS4pRRePs0ZiHPyhhGv14jObjIs30LXep9SoG8r58iqI4XrGXMDZbDxcTZvv+eOp6IpvxJ8r+sGbSDc2m5dbLMZn++rDqABTJhivBHtvAlwP7w10tXkcDXtWGMQa1Bnyp16Jb5VLk/w/U9FdJLbO4k2im2DUxKB2GGMoF2npgv6eOoIvFd1Y6fuvpKGQm1lNWvE9lziDX02S7iax5wZXdBlPbqNbSBxmd1znTR6WQ404OEb6yPns6MKehqXv2TMPJDoXqO9Xk6PbTigVlT5f0o10k1ViMUnHWfbZR694XidUhnQlxEysOlKdi5M83aV04+E7/L+n22fs/C2PiVkMu+aS7krXYtaRUo/LKehOFtKNn8fmf073jcGUIr1B96fW8Yqz3pWuvYiafpccOllJ061RZWxbp+bowVz/39P9mVcZL2mqRckr09wTka6gG6wKE8uHP3b0x0WdGhytaEGyJajpOI6waVm6Vdy4bc0Xm8/NQkNXFXZeObqj9X/l8jbOzPVGudwgc5lvo20mcml5vd1uqdeu0S1dNz1dXWsvZ7PVu3BSaluDzmo2a7aJJUnrsnq/pBhZ2qDW6XRqU5tu+7zO7G896fAO0C9y2kTXtTad8L6afc3hVTn+IrkM3UO5Z5ieeiUR37VpeCUMw9jiY+NzJzRx1z1s3V6wVdsj1pDPgOh3rtH66HoqrwVrKWnphmtSvlbITqGZF5f3nsknqh49ePcE+Xm0F1GtWVTbAd3Z59DXFCx0DC/67KBxwdbYLSF4VcHjQoLu6GfjRpBw8Ri99hh44nTphD3UdcdMQvfeK6y4Cw9SE+bR63lj1zR6ZZJvSrrsvKhArVzpU2ZFqgbr24PwwAR33Qq7t1OobojOrTMFYnbvO6CqDhbJ4FX5r+HJdA/sYYPqSWx+Z/xuejgvTaBEq1tuwdEsvYF6wQObeonqs/uERpFax0pH1/pk61bhJqvfEbitvk8dHg5/hwAukxu2KsYrSXcAtnMXcPcRbE74d2wyXT5f2ekNHo+G3rO1Ufd26d/7YDUZ5dkN0vkTa5Awn7RJ4E1HFxjqHXVeC2yLI/7hz0NY0eL33H3t0cNXju4EnNPBuxf8sO7fcol0R7wt8ZP1ABYb0VAaHu+BEXSEdoJg0+EyFxqYce5DYladii5iAJ5+mj0FBb4AopDCBNbTB+jCChM085Wju+TvHG43/6wmX0SvJtOFH8RA8HagABq8wyyyLvgV2t8/N9/0m9/p0Xh+cFG9YgndAul2EWA3ADbG42sVAKqQv2oatbePhm85uuCITbnt8XeXtUykeyjBArCjNKAh4dw4sDT8ldgbRJsFR9/tA/4FW3xXEANKGrqoZ6JtHgc52IBeWgkKzMBdwy9AR4IrTnJ0+ceu/omaLcBlUOsrke4bpICenyjBM5w2Bw9uE1Qjv5HjojJ4d4h093DhbCANXWyp6oK3P9rChU/YkC7slQ7tVfcORgY5uvyAbk8HhDbw70qiu0MbtKATos6N9vMDuvDDIqMuITAsePMuNMzXSb8rVCzV7j2e+oD5CX58ghXHkC58Grdon7oZGBnk6MKpACm+yCaRLrbmK1/gAz8HwYtt2Hev+KaXNxnvwWr0d7Hop4EkXaKL8cOgjUfYJUUXvJLgEf8sMPuVoltNvzuhLeR8IuPojuBxmm7xiq/GjHqYriDjcAkUy5SuPkQFOjJ0iRlzILggxRwS0p1dQVe7nW4XHhf03SvoHjy68DdixuwLzuR/A12iWijKr+osId1kf55/Qle2716Ruv5gSNOFz/pfQZec1xaedEN5r7Z/ue9mOjJf89y9I91rRmYXrXAV0Az9F9OdCv4uwCAHdK967rp4VkUscREXpYYufCMiHNl9ZfJG9MvoXuFh4c2q0Pc36U9SosUqJXQlVzPgOqccXbDAMm9J6G504XrDbpws710brYyNUFL8QEld/C504UqkvUH1fMENpqtWIolNBEL3omuCJcVtz0iWVw2tIFMvvNg7RwlduEmn6VQIGXJ+v2oXQUftUroX3SIA8EZAkuIGt6SCUmghTAndwgYuFy5QxUIVbd/L0W3ybVM7gFh3o2uAE2/JBygS3Dik1iKJQBQ1dMGkmdrgrWKf5Ot27yt05DCvu9FF32qT++4vmjQXDiirA+FJq4ZuE3stgqpVwmlL0vMGeuQJZuS+ZhdHvbvRRdPaD2FmDeNU/vl/3C8PfHx+j3KTVkMXe+Ro1oaFsKRC+GS95sDAYAvi/gurvXaZzt2NLh5j34g4XT/twnf/wLhqwTfZgu8oW4zgGS7ppqmILnSn04JcGbNwe7ja3JORvZJ0UZyJrdWwg8bsa2rp0QPhfnQJx2SG0plV8bT1h2Jm39GgfNy7jZOftsr0itOrJIroNqkoB8ta7Ift4X7h0GvFknTRe7LX8qLGbiFPmu2p7afUsO9Pl+qFo61HyfQd3YN8ZK/r88OZoYt9b0LtxvX6WLhprIgu8qoJZeu6hcIMUtMlI02s6acf6NCptfce2XM0iwq69Ddwu/V14+Wlsa2z4fOszwDyqpGRKrrYozlZsnTxm3JAUveDlByLvX1U0EVb7GKxdK/6dK4quoLOmw1d5IkrkhK68pw4B/jYvCsCKaObFMt3E92VZHS2ErpJKVR+tGPhXJFYQR3dwjB18L08XXps/jV0sVs5LX658YqYYnV0ySCzrOhSYUgU3fu/7/oyJINxwWIyzDWWLIV0Z4nJFa7yVj9LCq90zpts6crG3wO6ZuJ87PAbfDPOasanybFbn1d43kTCUWa/h65k74UbQSg+EOhQBk90lXQLzbj0ZrY9i9nMk8i+3Y7L/qCYrtcPJabAO7gEbaLgbVaH4y2ZBDOnW5iJ0/3p8xWZBfQsmdzqHTo7wK+gWzTJcGpeXRz+eRI/ew+vxu35mTndSNcbPyt0B3OmVTqD71lSmfOrotbPsvRLzOL96RaLvWPCLGlMeT4K35bHxZsz+ELdTLewbBE5lC07MPutdL3BYVMR9V+9Mv+JR1VBt2gaLzE9sU/uHfl5nalO/+Enmk2kyyVouAddP9F+hXtC6o59zsXPZ87XHaaS9FcvZsM5ypFk61Zl/slmV4duSjBNwT+h6w/Pxzr1/B2Ny67YacP8hhn6u40ws0pSLALX8av/4z9QjeluuE9YV3BQQQc0QUb7zdqbeeX81YtKa/9+2a97B4GZTJVaxWIV++3sZXs6d5xLUadiT/c1sKOPTH9scDoiE5/4Ai8YwgtfgnYwNw33uN5F37A4vI269UbJjf0wglfLPfa7I//953AY7bbf0RZiwjn5HYwl/3F5jGbFF8BeEFXQhIhAddb5qrVrX8uZ3CdulsM2K1Ek0k/zS695T1/eGar4FDgLBXRLROILUB0zsYmoJfdUeg1UOrmm1DdrTMMMKnkV2HMnnfOKQJcc6EPKu+mvy7xiqToXQokR8qjEJOF51fgROu9JtZWVSc7z9E9LkADxEYTXhfKmpFXqXIsOq8uRrokuzY9e8o0XRYk+mHKNN/ts8X9NdBaoPMgUfqXwgdQv5pOv8XpFFrT86aOcQ76Ge40vdC7VbbjiT9r/PRmmeSK/wvGw2vUb5e9SHnQsb+GHHx9Q/wcKM58K8oW5AQAAAABJRU5ErkJggg==" width="180" height="40"></p>
						<p class="heading">Registration Verification</p>
					</div>
					<div>
					<p>Hello '.$user[0]['firstname'].',</p>
				<p>To complete the medmate registration process please click:</p>
				<p style="height: 150px;width:700px;background-color: #f2f2f2;"><a href="'.$link.'"><button class="one">Verify Email</button></a></p>
				<p>If you have any questions, please email <a href="">enquires@medmate.com.au</a> </p>
			</div>
			<div class="bottom">
				<p>
					Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
					Empowering Healthcare Consumers with Choice, Value, & Convenience <br> medmate.com.au
				</p>
			</div>
		</div>
		</body>
		</html>
				';
               $message['to']           = $user[0]['username'];
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   
				$response->msg 		= "success";
				$response->status 	= $this->successStatus;                
			}
			else
			{
				$response->msg 		= "Userid is not Valid";
				$response->status 	= $this->successStatus;
			}
			return json_encode($response);
	}
	
	/**
      @OA\post(
          path="/v2/updatemedicareinfo",
          tags={"User"},
          summary="Update Medicare Info",
          operationId="Update Medicare Info",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="ismedicare",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
 	    @OA\Parameter(
              name="heathcarecard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function updatemedicareinfo(Request $request)
		{ 
			if ( Healthprofile::where('profileid',  $request['profileid'])->exists()) {
					$healthprofile = Healthprofile::where('profileid',  $request['profileid'])->first();
				
				  $userdetails = User::where('userid',  $request['userid'])->get();	
				  
				$healthprofile->ismedicare = $request['ismedicare'];
				$healthprofile->medicarenumber = $request['medicarenumber'];
				$healthprofile->medicare_exp = $request['medicare_exp'];
				$healthprofile->heathcarecard = $request['heathcarecard'];
				$healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				$healthprofile->safetynet = $request['safetynet'];
				$healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				$healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				$healthprofile->save();
				
				return response()->json(['healthprofile' => $healthprofile], $this->successStatus);
			}
        
		}
}
