<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\models\Business;
use App\models\Locations;
use App\models\Services;
use App\models\Locationwiseservices;
use App\models\Locationdeliverytimings;
use App\models\Postcode;
use App\models\Healthprofile;
use App\models\Cart;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class BusinessController extends Controller
{
    public $successStatus = 200;
	/**
      @OA\Get(
          path="/v2/getPharmacies",
          tags={"Pharmacies"},
          summary="Pharmacies Information",
          operationId="Pharmacies Information",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 
	  public function getPharmacies(Request $request)
	 {
		 $user = Auth::user();
		 
		 // $s5postcodematches = Locations :: where('postcode' , '=' , $request->postcode)
										// ->where('locationstatus' , '=' , 1)
										// ->select('locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.pharmacists','locations.services','locations.promomessage');
		 $business=array();
		 //->orWhere('locations.featured_postcode','=',$request->postcode)	
		 $s5locations =DB::select("select `locations`.`locationname`, `locations`.`isfeatured`, `locations`.`address`, `locations`.`suburb`, `locations`.`state`, `locations`.`postcode`, `locations`.`deliveryfee`, `locations`.`ordervaluequlifiing`, `locations`.`subscriptiontype`, `locations`.`address`, `locations`.`locationid`, `locations`.`logo`, `locations`.`photo`, `locations`.`shortdesc`, `locations`.`timings`, `locations`.`HealthProfessionals`, `locations`.`AboutHealthProfessionals`, `locations`.`services`, `locations`.`promomessage`, `locations`.`featured_postcode` from locations inner join business on `business`.`businessid` = `locations`.`businessid` where `locations`.`locationstatus` = 1 and `locations`.`featured_postcode` like '%".$request->postcode."%'");
         /*$s5locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								 ->where('locations.locationstatuss' , '=' , 1)	
								// ->where('locations.postcode' , '=' , $request->postcode)								
								 //->orWhere('locations.displaypostcodes' , '=' , $request->postcode)	
								//->where('locations.isfeatured' , '=' , 1)
								->Where('locations.featured_postcode' , 'like' , "'%".$request->postcode."%'")																							
								//->where('locations.subscriptiontype' , '=' ,5)								
								->select('locations.locationname','locations.isfeatured','locations.address','locations.suburb','locations.state','locations.postcode','locations.deliveryfee','locations.ordervaluequlifiing','locations.subscriptiontype','locations.address','locations.locationid','locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.HealthProfessionals','locations.AboutHealthProfessionals','locations.services','locations.promomessage','locations.featured_postcode')								
								->get();*/
								//print_r($s5locations);exit;
								for($i=0;$i<count($s5locations);$i++){
									$s5locations[$i]->standardprice=$this->computeprice($s5locations[$i]->locationid,$user->userid,1);
									$s5locations[$i]->computedprice=$this->computeprice($s5locations[$i]->locationid,$user->userid,2);
									$featured_postcodes=explode(",",$s5locations[$i]->featured_postcode);
									if(count($featured_postcodes)>1){
										if(in_array($request->postcode,$featured_postcodes)){
											$s5locations[$i]->isfeatured=1;
										}else{
											$s5locations[$i]->isfeatured=0;
										}
									}else{
									if($s5locations[$i]->featured_postcode==$request->postcode){
										$s5locations[$i]->isfeatured=1;
									}else{$s5locations[$i]->isfeatured=0;}}
								array_push($business,$s5locations[$i]);
								}
								//print_r($business);exit;
			//$business=array_push($business,$s5locations);	
			
		$s4locations=DB::select("select `locations`.`locationname`, `locations`.`address`, `locations`.`suburb`, `locations`.`state`, `locations`.`postcode`, `locations`.`deliveryfee`, `locations`.`ordervaluequlifiing`, `locations`.`subscriptiontype`, `locations`.`address`, `locations`.`locationid`, `locations`.`logo`, `locations`.`photo`, `locations`.`shortdesc`, `locations`.`timings`, `locations`.`HealthProfessionals`, `locations`.`AboutHealthProfessionals`, `locations`.`services`, `locations`.`promomessage`, `locations`.`featured_postcode` from locations inner join business on `business`.`businessid` = `locations`.`businessid` where `locations`.`locationstatus` = 1 and `locations`.`displaypostcodes` like '%".$request->postcode."%' or `locations`.`displaypostcodes` like '%ALL%' order by `locations`.`subscriptiontype` desc");
		/*$s4locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
									->where('locations.locationstatus' , '=' , 1)
								// ->where('locations.postcode' , '=' , $request->postcode)								
								 ->Where('locations.displaypostcodes' , 'like' , "'%".$request->postcode."%'")	
								 ->orWhere('locations.displaypostcodess','like',"'%ALL%'")	
								//->where('locations.subscriptiontype' , '=' ,4)	
								//->orWhere('locations.subscriptiontype' , '=' ,5)
								//->orWhere('locations.subscriptiontype' , '=' ,3)	
								//->orWhere('locations.locationid','=',1)
								->select('locations.locationname','locations.address','locations.suburb','locations.state','locations.postcode','locations.deliveryfee','locations.ordervaluequlifiing','locations.subscriptiontype','locations.address','locations.locationid','locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.HealthProfessionals','locations.AboutHealthProfessionals','locations.services','locations.promomessage','locations.featured_postcode')								
								->orderBy('locations.subscriptiontype','DESC')
								->get();*/
								//print_r($s4locations);exit;
								for($i=0;$i<count($s4locations);$i++){
									$s4locations[$i]->standardprice=$this->computeprice($s4locations[$i]->locationid,$user->userid,1);
									$s4locations[$i]->computedprice=$this->computeprice($s4locations[$i]->locationid,$user->userid,2);
								if(count($s5locations)==count($s4locations) && $s5locations[$i]->locationid==$s4locations[$i]->locationid){
									$business=$business;
								}
								else if(count($business)>0 && $business[0]->locationid!=$s4locations[$i]->locationid){									
								array_push($business,$s4locations[$i]);}else if(count($business)==0){$business=$s4locations;}
								}
								//$business=array_unique($business);
		//$business=array_push($business,$s4locations);						
					
								
		// $s2locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								// ->where('locations.postcode' , '=' , $request->postcode)								
								// ->orWhere('locations.displaypostcodes' , '=' , $request->postcode)	
								// ->where('locations.locationstatus' , '=' , 1)
								// ->where('locations.subscriptiontype' , '=' ,2)																					
								// ->select('locations.subscriptiontype','locations.address','locations.locationid','locations.photo','locations.shortdesc','locations.timings','locations.pharmacists')								
								// ->orderBy('locations.subscriptiontype','DESC')
								// ->get();
		
		// $s1locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								// ->where('locations.postcode' , '=' , $request->postcode)								
								// ->orWhere('locations.displaypostcodes' , '=' , $request->postcode)
								// ->where('locations.locationstatus' , '=' , 1)
								// ->where('locations.subscriptiontype' , '=' ,1)																						
								// ->select('locations.subscriptiontype','locations.address','locations.locationid','locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.pharmacists','locations.services','locations.promomessage')								
								// ->orderBy('locations.subscriptiontype','DESC')
								//->get();
		//$business = $s5locations->union($s4locations)->get();
		/*
		$business = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								// ->where('locations.postcode' , '=' , $request->postcode)								
								// ->orWhere('locations.displaypostcodes' , '=' , $request->postcode)								
								//->where('locations.isfeatured' , '=' , 1)								
								->where('locations.locationstatus' , '=' , 1)	
								->where('locations.subscriptiontype' , '=' ,5)	
								->orWhere('locations.subscriptiontype' , '=' ,4)	
								->orWhere('locations.subscriptiontype' , '=' ,3)	
								->select('locations.locationname','locations.isfeatured','locations.address','locations.suburb','locations.state','locations.postcode','locations.deliveryfee','locations.ordervaluequlifiing','locations.rating','locations.subscriptiontype','locations.address','locations.locationid','locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.HealthProfessionals','locations.AboutHealthProfessionals','locations.services','locations.promomessage')								
								->orderBy('locations.subscriptiontype','DESC')
								->get();
								for($i=0;$i<count($business);$i++){
									$business[$i]->standardprice=$this->computeprice($business[$i]->locationid,$user->userid,1);
									$business[$i]->computedprice=$this->computeprice($business[$i]->locationid,$user->userid,2);
								}
*/
		// $business['s5locations'] = $s5locations;
		// $business['s4locations'] = $s4locations;
		// $business['s2locations'] = $s2locations;
		// $business['s1locations'] = $s1locations;
								
        return response()->json(['pharmacyInformation' => $business] ,$this->successStatus); 
       
	 }
	 /**
      @OA\post(
          path="/v2/getSuburbs",
          tags={"Suburbs"},
          summary="Suburbs",
          operationId="Suburbs",
		  security={{"bearerAuth": {}} },
		  @OA\Parameter(
              name="suburb",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	  public function getSuburbs(Request $request)
	 {
	 
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$suburbs = Postcode :: where('locality', 'like', '%'.$request->suburb.'%')->select('postcodeid','postcode','locality')->get();
				
								
        return response()->json(['suburbs' => $suburbs] ,$this->successStatus); 
       
	 }
	 /**
      @OA\post(
          path="/v2/getDeliveryWindow",
          tags={"Pharmacies"},
          summary="Pharmacies Information",
          operationId="Pharmacies Information",
		  security={{"bearerAuth": {}} },
		  @OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			@OA\Parameter(
              name="dayoftheweek",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="currenttime",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="deliverytype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	  public function getDeliveryWindow(Request $request)
	 {
	 
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		if($request->dayoftheweek == "Sunday" || $request->dayoftheweek == "Saturday" )
		$weekday = "Monday";
		else
		$weekday = $request->dayoftheweek;	
	
		$deliveryslots = Locationdeliverytimings :: where('locationdeliverytimings.locationid', '=',  $request->locationid)
													->where('locationdeliverytimings.dayoftheweek', '=',  $weekday)
													->where('locationdeliverytimings.deliverytype', '=',  $request->deliverytype)
													->select('deliverywindow')
													->get();
		
		$timeVal =explode(':',strip_tags( $request->currenttime));
		
		$currenttime = $timeVal[0]+2;
		
		
		
		$currentslots=	Locationdeliverytimings :: where('locationdeliverytimings.locationid', '=',  $request->locationid)
													->where('locationdeliverytimings.dayoftheweek', '=',  $weekday)
													->where('locationdeliverytimings.deliverytype', '=',  $request->deliverytype)
													->where('locationdeliverytimings.timings', '>=',  $currenttime)
													->select('deliverywindow')
													->get();
								
        return response()->json(['currentslots' => $currentslots,'deliveryslots' => $deliveryslots] ,$this->successStatus); 
       
	 }
	 
	  public function getNewPharmacies(Request $request)
	 {
		 $user = Auth::user();
		 
		 // $s5postcodematches = Locations :: where('postcode' , '=' , $request->postcode)
										// ->where('locationstatus' , '=' , 1)
										// ->select('locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.pharmacists','locations.services','locations.promomessage');
		 
		 
         $s5locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								->where('locations.postcode' , '=' , $request->postcode)								
								->orWhere('locations.displaypostcodes' , '=' , $request->postcode)								
								->where('locations.isfeatured' , '=' , 1)								
								->where('locations.locationstatus' , '=' , 1)								
								->where('locations.subscriptiontype' , '=' ,5)								
								->select('locations.subscriptiontype','locations.address','locations.locationid','locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.HealthProfessionals','locations.AboutHealthProfessionals','locations.services','locations.promomessage')								
								->get();
								
		$s4locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								->where('locations.postcode' , '=' , $request->postcode)								
								->orWhere('locations.displaypostcodes' , '=' , $request->postcode)	
								->where('locations.locationstatus' , '=' , 1)
								->where('locations.subscriptiontype' , '=' ,4)								
								->orWhere('locations.subscriptiontype' , '=' ,3)																						
								->select('locations.subscriptiontype','locations.address','locations.locationid','locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.HealthProfessionals','locations.AboutHealthProfessionals','locations.services','locations.promomessage')								
								->orderBy('locations.subscriptiontype','DESC')
								->get();
								
		$s2locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								->where('locations.postcode' , '=' , $request->postcode)								
								->orWhere('locations.displaypostcodes' , '=' , $request->postcode)	
								->where('locations.locationstatus' , '=' , 1)
								->where('locations.subscriptiontype' , '=' ,2)																					
								->select('locations.subscriptiontype','locations.address','locations.locationid','locations.photo','locations.shortdesc','locations.timings','locations.HealthProfessionals','locations.AboutHealthProfessionals')								
								->orderBy('locations.subscriptiontype','DESC')
								->get();
		
		$s1locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								->where('locations.postcode' , '=' , $request->postcode)								
								->orWhere('locations.displaypostcodes' , '=' , $request->postcode)
								->where('locations.locationstatus' , '=' , 1)
								->where('locations.subscriptiontype' , '=' ,1)																						
								->select('locations.subscriptiontype','locations.address','locations.locationid','locations.logo','locations.photo','locations.shortdesc','locations.timings','locations.HealthProfessionals','locations.AboutHealthProfessionals','locations.services','locations.promomessage')								
								->orderBy('locations.subscriptiontype','DESC')
								->get();
		//$business = $s5locations->union($s4locations)->get();

		$business['s5locations'] = $s5locations;
		$business['s4locations'] = $s4locations;
		$business['s2locations'] = $s2locations;
		$business['s1locations'] = $s1locations;
		 
						//$noofrows = $business->count();
						
						//for($i=0; $i < $noofrows ; $i++)
						//{						
							
							//$locationLogo[$i] = url('/pharmacy/'.$business[$i]['logo']);
							//$business[$i]['logo']	= 	$locationLogo[$i];
							//$locationdeliverytimings[$i] = Locationdeliverytimings :: where('locationdeliverytimings.businessid', '=',  $business[$i]['businessid'] )
							//														 ->get();
							
							//$locationwiseservices[$i] = Locationwiseservices :: join('services','services.serviceid', '=' ,'locationwiseservices.serviceid')
							//													 ->where('locationwiseservices.businessid', '=',  $business[$i]['businessid'] )
							//													 ->get();
							
														
							
							
							//$business[$i]['locationwiseservices']	= 	$locationwiseservices[$i];
							//$business[$i]['locationdeliverytimings']	= 	$locationdeliverytimings[$i];
						//}
				
								
        return response()->json(['pharmacyInformation' => $business] ,$this->successStatus); 
       
	 }
	 
	  public function getPharmaciesoldversion(Request $request)
	 {
		 $user = Auth::user();
		 
		 $postcodematches = Locations :: where('postcode' , '=' , $request->postcode);
		 
		 
         $locations = Locations:: join('business','business.businessid', '=' ,'locations.businessid')
								->where('locations.postcode' , '!=' , $request->postcode)								
								->select('locations.*');								
								//->get();
		
		$business = $postcodematches->union($locations)->get();

		
		 
						$noofrows = $business->count();
						
						for($i=0; $i < $noofrows ; $i++)
						{						
							
							$locationLogo[$i] = url('/pharmacy/'.$business[$i]['logo']);
							$business[$i]['logo']	= 	$locationLogo[$i];
							$locationdeliverytimings[$i] = Locationdeliverytimings :: where('locationdeliverytimings.businessid', '=',  $business[$i]['businessid'] )
																					 ->get();
							
							$locationwiseservices[$i] = Locationwiseservices :: join('services','services.serviceid', '=' ,'locationwiseservices.serviceid')
																				 ->where('locationwiseservices.businessid', '=',  $business[$i]['businessid'] )
																				 ->get();
							
														
							
							
							$business[$i]['locationwiseservices']	= 	$locationwiseservices[$i];
							$business[$i]['locationdeliverytimings']	= 	$locationdeliverytimings[$i];
						}
				
								
        return response()->json(['pharmacyInformation' => $business] ,$this->successStatus); 
       
	 }
	 public function computeprice($locationid,$userid,$type){
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$cart_items= Cart :: where('enduserid', $userid)->where('cartstatus' , '1')->get();
		$totalprice=0;
		if($type==2){
		for($i=0;$i<count($cart_items);$i++){
			$cart_items[$i]->price=$this->compute_price($locationid,$cart_items[$i]->name,$cart_items[$i]->profileid);
			
			if($cart_items[$i]->price!="NA"){
			$totalprice=$totalprice+($cart_items[$i]->price * $cart_items[$i]->drugpack);
			
			}
		}
	}else if($type==1){
		for($i=0;$i<count($cart_items);$i++){
			$cart_items[$i]->price=$this->compute_price1($locationid,$cart_items[$i]->name,$cart_items[$i]->profileid);
			if($cart_items[$i]->price!="NA"){
			$totalprice=$totalprice+($cart_items[$i]->price * $cart_items[$i]->drugpack);}
	}
}
	return round($totalprice,2);
	 }
	 public function compute_price($location,$drug,$profile){
		$price="NA";
		$price_details = DB::table('catalouge_master as C')
								->select('CP.*','C.standard_price','C.Medicare','C.Con_Pens_Vet_ConSafty','C.SafetyNet','C.ShortDescription','C.GST','C.ItemCommodityClass')
								->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
								->where([
									['C.itemNameDisplay',$drug],
									['CP.location_id',$location]
									])
								->get();
							  
								$healthprofile = DB::table('healthprofile')->where('profileid',$profile)->get();
								if(count($price_details)>0){
								if($price_details[0]->ItemCommodityClass=='OTC')
								{
									//echo "6666";exit;
									$p=$price_details[0]->StandardPrice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									return round($price,2);
									
								}
								else
								{
									if(count($healthprofile)>0){
									
										if($healthprofile[0]->safetynet!=null){
											//echo "1111";exit;
											$p=$price_details[0]->SafetyNet;
											$gst =(($price_details[0]->GST)*$p)/100;
											$price =$p+ $gst;
											return round($price,2);
											
										}
										if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null || $healthprofile[0]->veteranaffairsnumber){
											//echo "22222";exit;
											$p=$price_details[0]->Con_Pens_Vet_ConSafty;
											$gst =(($price_details[0]->GST)*$p)/100;
											$price =$p+ $gst;
											return round($price,2);
										}
										if($healthprofile[0]->medicarenumber!=null){
											//echo "3333";exit;
											$p=$price_details[0]->Medicare;
											$gst =(($price_details[0]->GST)*$p)/100;
											$price =$p+ $gst;
											return round($price,2);
										}else{
											//echo "444444";exit;
											$p=$price_details[0]->StandardPrice;
											$gst =(($price_details[0]->GST)*$p)/100;
											$price =$p+ $gst;
											return round($price,2);
										}
		
									}
									
								}
								}
								else{
									$price="NA";
									return round($price,2);
								}
							//	echo $price."mmm";exit;
								
								
		
			}
			public function compute_price1($location,$drug,$profile){
				$price="NA";
				$price_details = DB::table('catalouge_master as C')
										->select('C.standard_price','C.ShortDescription','C.GST','C.ItemCommodityClass')
										->where([
											['C.itemNameDisplay',$drug],
												])
										->get();
									  
										$healthprofile = DB::table('healthprofile')->where('userid',$profile)->get();
										if(count($price_details)>0){
											//echo "55555";exit;
											$p=$price_details[0]->standard_price;
											$gst =(($price_details[0]->GST)*$p)/100;
											$price =$p+ $gst;
											return round($price,2);
											
										}
										else{
											$price="NA";
											return round($price,2);
										}
										//return round($price,2);
										
				
					}
		
}
