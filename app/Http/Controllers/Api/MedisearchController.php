<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Prescriptionitemdetails; 
use App\models\Healthprofile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class MedisearchController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/medisearch",
    *      tags={"medisearch"},
    *      summary="medisearch",
    *      operationId="medisearch",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="medication",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	 @OA\Parameter(
    *          name="userid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function medisearch(Request $request)
	{
        $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($request->userid);
        $healthprofile = Healthprofile::where('userid', $request->userid)->get();
        /*$catalouge_data = DB::table('catalouge_master as C')
                        ->select('C.itemNameDisplay as drug','C.MedmateItemCode as drugcode','C.itemCommodityClass as script','C.PhotoID','C.MIMsCMI as cmi')
                        ->where('C.itemNameDisplay','LIKE','%'.$request->medication.'%')
                        ->get();*/
       // $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,PI.image,F.form,C.active,C.active_units,C.units_per_pack as pack from catalouge_master as C left join packitemimages as PI on C.MIMsProductCode=PI.prodcode  left join formdat as F on C.MimsFormCode=F.formcode where C.MIMsProdCode=F.prodcode and C.Mimsformcode=F.formcode and   C.itemNameDisplay LIKE '%".$request->medication."%'");                
       $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price,C.Medicare,C.Con_Pens_Vet_ConSafty,C.SafetyNet,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.itemNameDisplay LIKE '%".$request->medication."%' and C.ItemStatus=1"); 
       //$mims_data=DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image,F.form,PA.active,PA.active_units,PA.units_per_pack as pack from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode`  left join packitemimages as PI on P.prodcode=PI.prodcode left join packdat as PA on P.packcode=PA.packcode where  P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode  and `P`.`ActualProduct` LIKE '%".$request->medication."%'");
       $mims_data = DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode` left join packitemimages as PI on P.prodcode=PI.prodcode where P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode and `P`.`ActualProduct` LIKE '%".$request->medication."%'");
       $drug_details=array();
        
        for($i=0;$i<count($catalouge_data);$i++){
            $catalouge_data[$i]->reference="Catalouge";
			if(count($healthprofile)>0){
					if($catalouge_data[$i]->script == "OTC")
					{
						//echo "OTC";exit;
						 $p=$catalouge_data[$i]->standard_price;
						 $gst =(($catalouge_data[$i]->GST)*$p)/100;
						 $price =$p+ $gst;
					}
					else
					{						
                                if($healthprofile[0]->safetyNet!=null){
									//echo "123";exit;
                                    $p=$catalouge_data[$i]->SafetyNet;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                else if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null || $healthprofile[0]->veteranaffairsnumber){
                                   // echo "222";exit;
									$p=$catalouge_data[$i]->Con_Pens_Vet_ConSafty;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                else if($healthprofile[0]->medicarenumber!=null){
                                  // echo "666";exit;
								   $p=$catalouge_data[$i]->Medicare;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                 //  echo "678";exit;
								   $p=$catalouge_data[$i]->standard_price;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
					}
                    }
           // $p=(float)$catalouge_data[$i]->price;
			//$gst =(float)(($catalouge_data[$i]->GST)*$p)/100;
			$catalouge_data[$i]->price =number_format(($price), 2, '.', '');			
           // $catalouge_data[$i]->strength=$catalouge_data[$i]->active.$catalouge_data[$i]->active_units;
            if($catalouge_data[$i]->script=='Prescription'){$catalouge_data[$i]->script="Yes";}else{$catalouge_data[$i]->script="No";}
            $drug_details[]=$catalouge_data[$i];
        }
        for($i=0;$i<count($mims_data);$i++){
           // $mims_data[$i]->strength=$mims_data[$i]->active.$mims_data[$i]->active_units;
            $mims_data[$i]->reference="MIMS";
            $mims_data[$i]->ShortDescription=$mims_data[$i]->drug;
            $mims_data[$i]->price="";
            $drug_details[]=$mims_data[$i];
        }
        if(!empty($drug_details)){
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.succ_message');
        }else{
            $response->drug_details 	= $drug_details;
            $response->msg 		= trans('messages.no_data');  
        }
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        }
   }
