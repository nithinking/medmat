<?php

namespace App\Http\Controllers\api;

require_once '/var/www/html/medmate-backend/vendor/autoload.php';

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DB;
use App\User; 
use App\models\Conformance;
use App\models\Prescriptiondetails; 
use App\models\Prescriptionitemdetails; 
use App\models\Tokenswallet; 
use App\models\Healthprofile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Sunra\PhpSimple\HtmlDomParser;
use phpseclib\Crypt\RSA as Crypt_RSA;
use Firebase\JWT\JWT;

class ConformanceController extends Controller
{
    public $successStatus = 200;
	
	/**
      @OA\Post(
          path="/v2/registerapp",
          tags={"Scripts"},
          summary="Appregistration",
          operationId="Appregistration",
		  //security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="appname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Parameter(
              name="appendpoint",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 
	public function registerapp(Request $request)
	{
	
		$response = (object)array();
		$date=date('Y-m-d');
		$conformance=new Conformance();
		$conformance->application_name=$request->appname;
		$conformance->appendpoint=$request->endpoint;
		$conformance->conformance_key=$this->subject_token();
		$conformance->passpharse=$request->appname;
		$conformance->keytype=1;
		$conformance->created_by=1;
		$conformance->conformance_date=$date;
		$conformance->created_at=date('Y-m-d H:i:s');
		$conformance->save();
		if($conformance){
			$success['token']=$conformance->conformance_key;
			$success['message']="App registerd Successfully";
			return response()->json(['success' => $success], $this->successStatus); 
		}
				 
		 
	}
	   public function subject_token()
	{
		$rsa = new Crypt_RSA();
		$key="medmate";
		//<RSAKeyValue><Modulus></D></RSAKeyValue>
		$publickey="<RSAKeyValue><Modulus>xUcmE8gzFIgD7OLnojv0IuclG/au31DJC6l6HwuZpToHfEZbwC9N72B2rebVXfOgY3IETXjPx2jmJgVlUKNrhICLGn79LmnygXW51evp9tdVQrMCjfRdq+1y0+elZJHCbC9NACY9aE6ZcQKjCuSHx8cZKlhQQbBYrWq/Ao3AvyAo0ZYWaC3FJg2RUKfC+M2AydUPOGezTrwV9hGKmh8vrvHNLXMKhi/HKb68UoXyirG4WxqbeuESQK1vs5paohCMMog+DT9gLEIaqhkoyH09dYBKgWj8t7QHIlIsLtdLPwKLMO8wd0u3mOcaH1qsVBWfgNrSPNV3kZ01kI1FhNno+Q==</Modulus><Exponent>AQAB</Exponent><P>01VsRBWK4DpBYHId0r7/PuSr73Mo3KKdFov2iE7QP0vmCcxf3Ou8fwT4o+5nUcaFGqNBrCl62uGZ0ROS1uFNnGRQjiuouFhx4kPL1JlEkJCgYV+Y8BaGZNNpSLmuRsX6Ls4zVgwBET5eDhK1GypDynLtXMAuB9qCcDCiez2Jy8c=</P><Q>7vk26zKCUuKWAAJk17fZY9bPOtmFeccDVAxUgvVV7cVZVBnuY2+ZQU2Jz5S8NQvUNaPJI1EfNYEK5E5sK9ZJ0h6O6iz5qOzC+2cCt9YGWNy1ReKTakom/45ovcl9J8xZbuh+adN5u9Y7/KeIJPXYgTcsbiPX0P/chmvy6UybJT8=</Q><DP>yIFclgAmYfWDf6DlwMSr4R0OL8H/+81g4zpp7gtwsw0stREtxqn2jkDGMqqHZmPGd257aX91a2PuDxrWD391pjLa5vhvPG/VpQoTwqKkFdDSCXCs5uVIHzMhyfk5azZ81pmWxUXgYV7d19ZR8/MWnGaL45sGUTc02IKcNfc93JM=</DP><DQ>vYC+/i/ljNjF3mgHk9DlAPkdCCvqXrFkgEfu1JK8e3rd5YVSt2OOAmz8dbIatW4V8BtbqzfvKbyU6IsP2ItrqsY0ypqzuDNYQ4hibWm5SspqrWqJP+ZhmpJRmP6o0uCHox2W8VCT3lJ661Xm7nhOIdmg0A54W/Ip7Ca6UCtBdH0=</DQ><InverseQ>sG+GWdQ847hCUiTTDHNsAxq+iu/LPuvnfcARLWdlL4Ov0Naw/rwIOeDXIe+5qHCubZr5Hoc5wwn/4iNrKZTdfFc1R5AVWexQ2Mxs0sSdqhIUQ7qxOu+cVjcD9uEpHa5oN4oSqExsE3cmepOYIX5LUuxczs3Ex+ORnSg31tJgbpU=</InverseQ><D>p92Q4whZnsDWRj4VyBCn/wMcqpzP1KpTSJ8fkUc3qEamk/Lyo6gc8vHsSrG/IUDrW3NrgdipRQ9Xt0akHWfRV2Bh/VvNaC8y8UIRrZwe67HzlNvp8ozbIC7epL3aGKeG/rOJG/VuD1HCc01BG0W23CHoogWf1SWPb5EDk9K3Dml55Td3v/i3CyuXi9vs7LhGM4P+rS5/g1x2B0eelrNarIFm+uytMqVvaLqdYgpniud/OILwndPOHPbkRAihWJ1Sy2QbR0LIDCdRnhII1EVlB8XwPyEDKnY1CwSACYImNrJq1LdvFV/qDkXcMbhFmDwyQOzjqYiRj29h9gsY33wZ+Q==</D></RSAKeyValue>";
		$rsa = new Crypt_RSA();
    //$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
	
    $privateKey=$rsa->loadKey($publickey);
		$rsa->setEncryptionMode(Crypt_RSA::ENCRYPTION_PKCS1);	//private key in xml
   		$date=date('Y-m-d H:i:s');
		//$exp=strtotime(date('Y-m-d H:i:s', strtotime('+1 day', $date)));
		$exp=strtotime("+ 365 day");
		$newTime = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +1 minutes"));
		$oTime= date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -1 minutes"));
		$payload = array(
		"sub"=>"Medmate",
		"name"=>"medmate",
		"iss" => "MedMate",
		"aud" => "http://apps.fred.com.au",
		"exp" => $exp,
		"iat" => $newTime,
		"nbf" => $oTime
		);
$jwt = JWT::encode($payload, $rsa, "RS256");
return $jwt;	
        }
	public function getlogs(Request $request){
		 $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=logile.csv",
          );
	$user=DB::table('conformance_admin_users')->where('username',$request->username)->get();
	if(count($user)>0){
	$conformance=DB::table('conformance')->where('id',$user[0]->application_id)->get();
	if($conformance[0]->passpharse==$request->passpharse){
	$logdetails=DB::table("mi_logs")->where('application_id',$user[0]->application_id)->whereRaw("(created_at >= ? AND created_at <= ?)",[$request->form." 00:00:00", $request->to." 23:59:59"])->get();
	//print_r($logdetails);exit;
    $columns = array('transactionid', 'scidsource', 'scid','external_user_id', 'socihiid', 'medicationbrandname', 'quantity', 'strength', 'repeats', 'form', 'dosageinstructions','response','status','issuedate');

    $callback = function() use ($logdetails, $columns)
    {
        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);

        foreach($logdetails as $review) {
            fputcsv($file, array($this->getvalue($review->transactionid), $this->getvalue($review->scidsource), $this->getvalue($review->scid),$review->external_user_id, $this->getvalue($review->socihiid), $this->getvalue($review->medicationbrandname), $this->getvalue($review->quantity), $this->getvalue($review->strength), $this->getvalue($review->repeats), $this->getvalue($review->form), $this->getvalue($review->dosageinstructions),$this->getvalue($review->response),$this->getvalue($review->status),$this->getvalue($review->issuedate)));
        }
        fclose($file);
    };
   return \Response::stream($callback, 200, $headers);}else{
		$success['message']="Unauthorized";
		return response()->json(['success' => $success], 401); 
	}}
	else{
		$success['message']="Unauthorized";
		return response()->json(['success' => $success], 401); 
	}
	}	
public function getvalue($value){
			try {
            return Crypt::decryptString($value);
        } catch (\Exception $e) {
            return $value;
        }
		}	
}
