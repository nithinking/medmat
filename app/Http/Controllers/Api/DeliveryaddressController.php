<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Deliveryaddress; 
use App\models\Features; 
use App\models\Profiletypes; 
use App\models\Programs; 
use App\models\Useraccess; 
use Illuminate\Support\Facades\Auth; 
use Validator;
class DeliveryaddressController extends Controller
{
    public $successStatus = 200;
    
	/**
      @OA\Post(
          path="/v2/addDeliveryAddress",
          tags={"DeliveryAddress"},
          summary="Add Delivery Address",
          operationId="deliveryAddress",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
          
		   @OA\Parameter(
              name="addresstype",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		   @OA\Parameter(
              name="addressline1",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="addressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="suburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="state",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		 
			@OA\Parameter(
              name="lattitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="longitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function addDeliveryAddress(Request $request) {
		 $validator = Validator::make($request->all(), 
              [ 
              'firstname' => 'required|firstname', 
              'streetaddress' => 'required|streetaddress', 
              'postcode' => 'required|postcode', 
              'country' => 'required|country', 
              'mobile' => 'required|mobile',
              'latitude' => 'required|latitude',
              'longitude' => 'required|longitude',
			  
             ]);
		 $response 	   = (object)array();
		 $user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
	
		$useraccess = Useraccess ::  where('profiletypeid', $user->profiletypeid)
									->whereRaw("FIND_IN_SET('9',featureid)")->get();
		 if($useraccess->count() > 0  )
		 {			 
		 $deliveryAddress = new Deliveryaddress;
			if($request->firstname != '' ||  $request->streetaddress !='' || $request->postcode !='' || $request->mobile !='' || $request->lattitude!="" || $request->longitude!='')
			{
				$deliveryAddress->userid = $user->userid;
				$deliveryAddress->firstname = $request->firstname;
				$deliveryAddress->addresstype = $request->addresstype;
				$deliveryAddress->addressline1 = $request->addressline1;
				$deliveryAddress->addressline2 = $request->addressline2;
				$deliveryAddress->suburb = $request->suburb;
				$deliveryAddress->postcode = $request->postcode;
				$deliveryAddress->state = $request->state;
				//$deliveryAddress->mobile = $request->mobile;
				$deliveryAddress->lattitude = $request->lattitude;
				$deliveryAddress->longitude = $request->longitude;
				$deliveryAddress->deliveryaddresssts = '1';
				$deliveryAddress->save();
				$response->deliveryAddressDetails 	= $user;
				$response->msg 		= "Delivery Address Added";
				$response->status 		= $this->successStatus;
				
			}
			else
			{
				$response->msg 		= trans('messages.general_messgae');
				$response->status 		= $this->successStatus;
			}
		 }
		 else
			{
				$response->msg 		= trans('messages.user_feature_messgae');
				$response->status 		= $this->successStatus;
			}
			return json_encode($response);
	}
	/**
      @OA\Get(
          path="/v2/getDeliveryAddresses",
          tags={"DeliveryAddress"},
          summary="Get delivery Address",
          operationId="delivery Address",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getDeliveryAddresses(){
		 $user = Auth::user();
		
         $deliveryaddresses = Deliveryaddress::where('userid', $user->userid)->where('deliveryaddresssts', "1")->get();
		
        return response()->json(['deliveryaddresses' => $deliveryaddresses], $this->successStatus); 
      
  }
  /**
      @OA\Get(
          path="/v2/removeDeliveryAddresses",
          tags={"DeliveryAddress"},
          summary="Remove delivery Address",
          operationId="delivery Address",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="deliveryaddressid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function removeDeliveryAddresses(Request $request){
		 $user = Auth::user();
		
         $deliveryaddresses = Deliveryaddress:: where('deliveryaddresses.userid', '=', $request->userid)
													->where('deliveryaddresses.deliveryaddressid', '=', $request->deliveryaddressid )
													->update(['deliveryaddresses.deliveryaddresssts' => '0']);
		$success['message'] = trans('messages.Remove_delivery_address');
        return response()->json(['deliveryaddresses' => $deliveryaddresses], $this->successStatus); 
      
  }
}
