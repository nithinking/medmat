<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Cart;
use App\models\Healthprofile;
use App\models\Prescriptiondetails; 
use App\models\Prescriptionitemdetails; 
use App\models\Locations; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class CartController extends Controller
{
    public $successStatus = 200;
	/**
      @OA\Post(
          path="/v2/addtoCart",
          tags={"Cart"},
          summary="Add To Cart",
          operationId="Add To Cart",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  
		  @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptiontype",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="medlistid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="name",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="form",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="strength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="quantity",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="drugpack",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="scriptavailable",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  
		  @OA\Parameter(
              name="referrence",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
	    @OA\Parameter(
              name="image",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
	    @OA\Parameter(
              name="cartprice",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
	    
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function addtoCart(Request $request)
	 {
		 $user = Auth::user();
		 
		 $cartVal = Cart :: where('enduserid' , '=' ,$user->userid)
							->where('prescriptionid', '=' ,$request->prescriptionid)
							->where('cartstatus', '=' ,1)
							->get();
					
		 $cartrows = $cartVal->count();
		 if($cartrows == 0)
		 {
		 $cart = new Cart;
		 
		 $cart->enduserid = $user->userid;
		 $cart->userid = $request->userid;
		 $cart->profileid = $request->profileid;
		 $cart->prescriptionid = $request->prescriptionid;
		 $cart->prescriptiontype = $request->prescriptiontype;
		 $cart->medlistid = $request->medlistid;
		 $cart->prescriptionfor = $request->prescriptionfor;
		 $cart->name = $request->name;
		 $cart->quantity = $request->quantity;
		 $cart->drugpack = $request->drugpack;
		 $cart->strength = $request->strength;
		 $cart->referrencetable = $request->referrence;
		 $cart->form = $request->form;
		 $cart->image = $request->image;
		 if($request->price!='')
		 $cart->cartprice = $request->price;	
		else
		 $cart->cartprice = '0.00';
		 $cart->scriptavailable = $request->scriptavailable;		 
		 if($cart->cartid !="")
				$cart->itemgrouptype = "Pharmacy Basket";
		 elseif($cart->cartid !="" && $cart->medlistid !="")
				$cart->itemgrouptype = "Pharmacy Basket";
		 elseif($cart->cartid =="" && $cart->medlistid !="")
				$cart->itemgrouptype = "Health Store Basket";
		 else	
				$cart->itemgrouptype = "Health Store Basket";
		$cart->cartstatus ='1';
		$cart->save();
		 }
		 else
		 {
			 $qty = $cartVal[0]['drugpack']+$request->drugpack;
			$cartUpdate = Cart :: where('cart.enduserid' , '=' ,$user->userid)
									->where('cart.prescriptionid', '=' ,$request->prescriptionid)
									->update(['cart.drugpack' => $qty]); 
			$cart = Cart :: where('cart.enduserid' , '=' ,$user->userid)
						  ->where('cart.prescriptionid', '=' ,$request->prescriptionid)
						  ->get();
		 }
		 
		if($request->prescriptionid !="")
		{
		$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$request->prescriptionid.'"'))
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.medstatus' => 'InCart']);
															
		//To Check if uploaded script is related Medlist Medication script
		$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$request->prescriptionid.'"'))
													->where('prescriptionitemdetails.profileid', '=', $request->profileid)
													->get();
		$count = $getmedlistid->count();
		if($count > 0)
		{
			if($count == 1)
			$medlistdetails = $getmedlistid[0]['medilistid'];
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
											->where('prescriptionitemdetails.profileid', '=', $request->profileid)
											->update(['prescriptionitemdetails.medstatus' => 'InCart']);
		}
		
		}
		else
		{
		$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $request->medlistid)
											->where('prescriptionitemdetails.profileid', '=', $request->profileid)
											->update(['prescriptionitemdetails.medstatus' => 'InCart']);
		}
		
        return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	 }
	 /**
      @OA\Get(
          path="/v2/getCartItems",
          tags={"Cart"},
          summary="Get Cart Items",
          operationId="Get Cart Items",
		  security={{"bearerAuth": {}} },
		
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function getCartItems(Request $request)
	 {
		 $response=(object)array();
		 $user = Auth::user();
		 
		 $cart = Cart :: where('enduserid', $user->userid)->where('cartstatus' , '1')->get();
		 //print_r($cart);exit;
		 $nooforders = $cart->count();
		for($i=0;$i<$nooforders;$i++)
		{
			$presctipriontype[$i] = Prescriptiondetails :: leftJoin('prescriptionitemdetails','prescriptionitemdetails.prescriptionid' ,'=','prescriptiondetails.prescriptionid')	
														->where('prescriptiondetails.prescriptionid', '=',$cart[$i]['prescriptionid'])
														->orWhere('prescriptionitemdetails.prescriptionid', '=',\DB::raw('"'.$cart[$i]['prescriptionid'].'"'))
															->select('prescriptiondetails.*','prescriptionitemdetails.*')->get();
			
			
			//$cart[$i]['prescriptiondetails']	= 	$presctipriontype[$i];
		}
		
		 $location = Locations :: where('locationid', 1)->select('locations.ordervaluequlifiing')->get();
		 $deliveryfee = $location[0]['ordervaluequlifiing'];
		
				$response->cart 	= $cart;
				$response->deliveryfee 	= $deliveryfee;
				$response->status 		= $this->successStatus;
			
		 return json_encode($response);
       // return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	 }
		/**
		@OA\Post(
          path="/v2/removeCartItems",
          tags={"Cart"},
          summary="Remove Cart Item",
          operationId="Remove Cart Item",
		  security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="cartid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ),
          
        @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
        ),
        @OA\Response(
              response=401,
              description="Unauthorized"
        ),
        @OA\Response(
              response=400,
              description="Invalid request"
        ),
        @OA\Response(
              response=404,
              description="not found"
        ),
      )
     */
	public function removeCartItems(Request $request)
	{
		 $user = Auth::user();
		 
		 $cart = Cart :: where('cart.enduserid', '=',$user->userid)->where('cart.cartid' ,'=', $request->cartid)
						 ->update(['cart.cartstatus' => '0']);
		 
		 $cartItemDetails = Cart :: where('cart.cartid' ,'=', $request->cartid)->get();
		 
		 $medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cartItemDetails[0]['medlistid'])
											->update(['prescriptionitemdetails.medstatus' => 'medlist']); 
		
		$medlistinfo = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cartItemDetails[0]['medlistid'])
												->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[0]['profileid'])
												->get();
		if($medlistinfo->count() > 0)
		{
			if($medlistinfo[0]['medilistid'] != "")
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $medlistinfo[0]['medilistid'])
											->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[0]['profileid'])
											->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
		}
											
        return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	}
	
		/**
		@OA\Post(
          path="/v2/updateCartItem",
          tags={"Cart"},
          summary="Update Quanity In Cart Item",
          operationId="Update Quanity In Cart Item",
		  security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="cartid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ), 
		@OA\Parameter(
              name="drugpack",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ), 
			
        @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
        ),
        @OA\Response(
              response=401,
              description="Unauthorized"
        ),
        @OA\Response(
              response=400,
              description="Invalid request"
        ),
        @OA\Response(
              response=404,
              description="not found"
        ),
      )
     */
	public function updateCartItem(Request $request)
	{
		 $user = Auth::user();
		 
		 $cart = Cart :: where('cart.enduserid', '=',$user->userid)->where('cart.cartid' ,'=', $request->cartid)
						 ->update(['cart.drugpack' => $request->drugpack]);
		 
		 
        return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	}
	 
	 
}
