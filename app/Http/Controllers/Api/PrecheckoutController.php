<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Cart;
use App\models\Prescriptionitemdetails; 
use App\models\Healthprofile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class PrecheckoutController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/precheckout",
    *      tags={"precheckout"},
    *      summary="precheckout",
    *      operationId="precheckout",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
	*	  @OA\Parameter(
    *          name="cartitems",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	  		      
    *     
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function preCheckout(Request $request)
	{
		//ini_set('memory_limit', '-1');
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get();
		//print_r($request);exit;
		$cart_items=json_decode($request->cartitems);
		if(json_last_error()!==0){
			$cart_items= Cart :: where('enduserid', $user->userid)->where('cartstatus' , '1')->get();
		}
		for($i=0;$i<count($cart_items);$i++){
            $cart_items[$i]->price=$this->compute_price($request->locationid,$cart_items[$i]->name,$cart_items[$i]->profileid);
        }
        $response->cartitems 	= $cart_items;
        $response->msg 		= trans('messages.price_computed');
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        }
    public function compute_price($location,$drug,$profile){
$price="NA";
$price_details = DB::table('catalouge_master as C')
						->select('CP.*','C.ShortDescription','C.GST','C.ItemCommodityClass')
						->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
						->where([
							['C.itemNameDisplay',$drug],
							['CP.location_id',$location]
							])
                        ->get();
                      
                        $healthprofile = DB::table('healthprofile')->where('profileid',$profile)->get();
						if(count($price_details)>0){
						if($price_details[0]->ItemCommodityClass=='OTC')
						{
							$p=$price_details[0]->StandardPrice;
							$gst =(($price_details[0]->GST)*$p)/100;
							$price =$p+ $gst;
							
						}
						else
						{
							if(count($healthprofile)>0){
							
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                else if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null || $healthprofile[0]->veteranaffairsnumber){
                                    $p=$price_details[0]->Con_Pens_Vet_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                else if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->StandardPrice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }

                            }
							
						}
                        }
						else{
                            $price="NA";
                        }
                        return round($price,2);;
                        

    }    	
		
}
