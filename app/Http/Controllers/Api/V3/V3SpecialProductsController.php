<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Prescriptionitemdetails; 
use App\models\Healthprofile; 
use App\models\Locations; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class V3SpecialProductsController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/specialproducts",
    *      tags={"Special Products"},
    *      summary="Special Products",
    *      operationId="Special Products",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),	  
    *     	      
    *     
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function specialproducts(Request $request)
	{
		$response = (object)array();
		$user = Auth::user();
        $user_details = User::findOrFail($user->userid);
		
		
		$business = Locations :: where('locationid', $request->locationid)->get();
		
		
        $special_items = DB::table('catalouge_master as C')
						->select('C.itemNameDisplay','C.MedmateItemCode','C.itemCommodityClass','C.PhotoID','CO.SalePrice as price','CO.SalePrice as StandardPrice','C.GST')
						->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						->where([
							//['CO.SalePrice','!=',NULL],
							['CO.SalePrice','!=',''],
							//['CO.SalePrice','!=',0],
							['CO.businessid',$business[0]['OTCCatalogueBusinessID']],
							['CO.location_id',$business[0]['OTCCatalogueLocationID']],						
                            ['CO.Availability',1],
                            ['CO.Pricing_Tier',$business[0]['OTCPricingTier']],
                            ['C.itemCommodityClass','OTC'],
							['C.ItemStatus',1]
							])
						/*->orwhere([ ['CO.businessid',$business[0]['OTCCatalogueBusinessID']],
									['CO.location_id',0],
									['CO.Pricing_Tier',$business[0]['OTCPricingTier']],
									['CO.SalePrice','!=',''],
									['CO.Availability',1],
									['C.itemCommodityClass','OTC'],
									['C.ItemStatus',1]
              					])*/
						->orwhere([ ['CO.businessid',0], 
									['CO.location_id',0],
									['CO.Pricing_Tier',$business[0]['OTCPricingTier']],
									['CO.SalePrice','!=',''],
									['CO.Availability',1],
									['C.itemCommodityClass','OTC'],
									['C.ItemStatus',1]
              					])
                        ->get();
						
		// if(count($special_items) == 0)
		// {
			// $special_items = DB::table('catalouge_master as C')
						// ->select('C.itemNameDisplay','C.MedmateItemCode','C.itemCommodityClass','C.PhotoID','CO.SalePrice as price','CO.SalePrice as StandardPrice','C.GST')
						// ->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						// ->where([
							// ['CO.SalePrice','!=',NULL],
							// ['CO.SalePrice','!=',''],
							// ['CO.SalePrice','!=',0],
                            // ['CO.businessid',0],							
                            // ['CO.Availability',1],
							//['CO.Pricing_Tier',$business[0]['OTCPricingTier']]
                            // ['C.itemCommodityClass','OTC'],
							// ['C.ItemStatus',1]
							// ])
								
                        // ->get();
		// }			
                        for($i=0;$i<count($special_items);$i++){
                            $p=(float)$special_items[$i]->price;
							$gst =(float)(($special_items[$i]->GST)*$p)/100;
							$special_items[$i]->price =number_format(($p+ $gst), 2, '.', '');
                        }
                      
        $response->specialproducts 	= $special_items;
        $response->msg 		= trans('messages.succ_message');
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        }
    		
}
