<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Cart;
use App\models\Deliveryaddress;
use App\models\Locations;
use App\models\Prescriptionitemdetails; 
use App\models\Doordashlogs; 
use App\models\Healthprofile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class V3PrecheckoutController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/precheckout",
    *      tags={"precheckout"},
    *      summary="precheckout",
    *      operationId="precheckout",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
	*	  @OA\Parameter(
    *          name="cartitems",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	  		
	*	  @OA\Parameter(
    *          name="deliveryaddressid",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	  	@OA\Parameter(
    *          name="ordertotal",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	  		      
    *     
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function preCheckout(Request $request)
	{
		//ini_set('memory_limit', '-1');
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get();
		$price_change=0;
		$istbc=0;
		//print_r($request->cartitems);exit;
		//$cart_items=json_decode($request->cartitems);

		$cart_items=$request->cartitems;
		//echo count($cart_items);exit;
		//print_r($cart_items);exit;
		//if(json_last_error()!==0){
		if($cart_items == ""){
			$cart_items= Cart :: where('enduserid', $user->userid)->where('cartstatus' , '1')->get();			
		}
		//print_r($request);exit;
		$card_type=['private'=>1,'medicare'=>2,'concession'=>3,'safteynet'=>4,'Generic_private'=>5,'Generic_Medicare'=>6,'Generic_Concession'=>7,'Generic_safetyNet'=>8];
		for($i=0;$i<count($cart_items);$i++){
			$cart_items[$i] = (object)$cart_items[$i];
			if($cart_items[$i]->isgeneric==1){
				$cart_items[$i]->name=str_replace("Generic of ","",$cart_items[$i]->name);
			}
            $cart_items[$i]->price_obj=$this->compute_price($request->locationid,$cart_items[$i]->name,$cart_items[$i]->strength,$cart_items[$i]->quantity,$cart_items[$i]->profileid,$cart_items[$i]->isgeneric);
			//print_r($cart_items[$i]->price_obj);exit;
			if($cart_items[$i]->isgeneric==1){
				$cart_items[$i]->name="Generic of ".$cart_items[$i]->name;
			}
			$prices=explode("+",$cart_items[$i]->price_obj);
			$cart_items[$i]->price=round($prices[0],2);
			if($prices[0]=="NA"){
				$cart_items[$i]->price="0.00"; 
				$istbc=1;
				DB::table('cart')->where('cartid',$cart_items[$i]->cartid)->update(['istbc' => 1]);
			}
			$cart_items[$i]->cardtype=$prices[1];
			
			//DB::table('cart')->where('cartid',$cart_items[$i]->cartid)->update(['cardtype' => $card_type[$cart_items[$i]->cardtype]]);
			if($cart_items[$i]->prescriptiontype==1&&$cart_items[$i]->price!=(int)$cart_items[$i]->cartprice){
				$price_change=1;
			}
			}
		//Test DD API KEY
		//"Authorization: Bearer 22590509529bb2aa58e00387ef617843179e8eee",
		if($request->deliveryaddressid !='')
		{
			$deleiveryaddress = Deliveryaddress :: where('deliveryaddressid',  $request->deliveryaddressid)->get();
			
			//$deliverypartner = Deliverypartner :: where('locationid',$request->locationid)->get();
			$deliverypartner = DB::select("select * from deliverypartner where locationid=".$request->locationid." and partnerstatus=1"); 
			/*if(count($deliverypartner)>0){
			$deliverypartner = DB::select("select * from deliverypartner where locationid='0' and partnerstatus=1"); 	
			}*/
			$locationname = Locations :: where('locationid','=',$request->locationid)->get();
				date_default_timezone_set($locationname[0]['timezone']);
				$cmptime = date("H");
				$todaydate = date("Y-m-d\TH:i:s\Z");
			for($i=0;$i<count($deliverypartner);$i++)
			{
				if($deliverypartner[$i]->partnername == "DoorDash")
				{
				if($cmptime > $deliverypartner[$i]->starttime && $cmptime < $deliverypartner[$i]->endtime)
				{
					
						$pickupaddress = Locations :: where('locationid',$request->locationid)->get();
				$pickupcity = 	$pickupaddress[0]['suburb'];	
				$pickupstate =  $pickupaddress[0]['state'];
				$pickupstreet =  $pickupaddress[0]['streetaddress'];
				$pickuppostcode =  $pickupaddress[0]['postcode'];
				
				$deleiverycity = $deleiveryaddress[0]['suburb'];
				$deleiverystate = $deleiveryaddress[0]['state'];
				$deleiverystreet = $deleiveryaddress[0]['addressline1'];
				$deleiverypostcode = $deleiveryaddress[0]['postcode'];
				$orderamount = str_replace(".","",$request->ordertotal);
				//$dtime = $todaydate;
				$dtime = gmdate("Y-m-d\TH:i:s\Z");
				//echo "GMDate".$dtime;exit;
						
						// "pickup_address": {
						// "city": "hawthorn",
						// "state": "Victoria",
						// "street": "174 power street",
						// "unit": "",
						// "zip_code": "3122"
						// },
				$curl = curl_init();
				$vars = '{
						
						"pickup_address": {
						"city": "'.$pickupcity.'",
						"state": "'.$pickupstate.'",
						"street": "'.$pickupstreet.'",
						"unit": "",
						"zip_code": "'.$pickuppostcode.'"
						},
						"dropoff_address": {
						"city": "'.$deleiverycity.'",
						"state": "'.$deleiverystate.'",
						"street": "'.$deleiverystreet.'",
						"unit": "",
						"zip_code": "'.$deleiverypostcode.'"
						},
						"order_value": "'.$orderamount.'",
						"delivery_time": "'.$dtime.'",
						"external_store_id": "'.$request->locationid.'",
						"external_business_name": "Medmate"
						}';
						
						//print_r($vars);exit;
			curl_setopt_array($curl, array(
			  CURLOPT_URL => env('DOORDASH_ESTIMATES_URL'),			  
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>$vars,
			  CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".env('DOORDASH_KEY'),
				"Content-Type: application/json",				
			  ),
			));

			$res = curl_exec($curl);

			curl_close($curl);
			$resval = json_decode($res);
			//$reqlog = json_decode($res,true);
			//print_r($resval);
			//print_r($resval->field_errors[0]->error);
			//exit;
			//echo $resval->id;exit;
			
			//$Doordashlogs->storeData($input);
			$Doordashlogs=new Doordashlogs;
			$Doordashlogs->doordashapiurl=env('DOORDASH_ESTIMATES_URL');
			$Doordashlogs->urltype="Estimates";
			// $Doordashlogs->uniqueorderid="";
			// $Doordashlogs->orderid="";
			$Doordashlogs->pickupcity=$pickupcity;
			$Doordashlogs->pickupstate=$pickupstate;
			$Doordashlogs->pickupstreet=$pickupstreet;
			// $Doordashlogs->pickupunit="";
			 $Doordashlogs->pickupzip_code=$pickuppostcode;
			// $Doordashlogs->pickup_phone_number="";
			$Doordashlogs->dropoffcity=$deleiverycity;
			$Doordashlogs->dropoffstate=$deleiverystate;
			$Doordashlogs->dropoffstreet=$deleiverystreet;
			// $Doordashlogs->dropoffunit="";
			$Doordashlogs->dropoffzip_code=$deleiverypostcode;
			// $Doordashlogs->customerphone_number="";
			// $Doordashlogs->customerbusiness_name="";
			// $Doordashlogs->customerfirst_name="";
			// $Doordashlogs->customerlast_name="";
			// $Doordashlogs->customeremail="";
			// $Doordashlogs->send_notifications="";
			$Doordashlogs->order_value=$orderamount;
			 $Doordashlogs->pickup_time=$dtime;
			// $Doordashlogs->pickup_business_name="";
			// $Doordashlogs->pickup_instructions="";
			// $Doordashlogs->dropoff_instructions="";
			// $Doordashlogs->external_delivery_id="";
			// $Doordashlogs->driver_reference_tag="";
			// $Doordashlogs->external_store_id="";
			// $Doordashlogs->contains_alcohol="";
			// $Doordashlogs->num_items="";
			// $Doordashlogs->signature_required="";
			// $Doordashlogs->allow_unattended_delivery="";	
			 $Doordashlogs->jsonrequest=$vars;	
			//$Doordashlogs->jsonresponse=$reqlog;	
			$Doordashlogs->save();
				if(isset($resval->id) && $resval->id != "")
				{
					$response->estimateid 	= $resval->id;
					$response->cartitems 	= $cart_items;
					$response->istbc = $istbc;
					if($price_change==1){
					$response->price_change_msg="Your script prices may have been adjusted to match entitlement details provided.";
						}else{
					$response->price_change_msg="";
						}
					$response->msg 		= trans('messages.price_computed');
					$response->status 		= $this->successStatus;
					return json_encode($response);
				}
				// elseif(isset($resval->field_errors) && $resval->field_errors != "")
				// {
					//print_r($resval->field_errors);exit;
					// $response->estimateid 	= $resval->id;
					// $response->cartitems 	= $cart_items;
					// $response->msg 		= trans('messages.price_computed');
					// $response->status 		= $this->successStatus;
					// return json_encode($response);
				// }
			//$input=array();
			
			
				$locationname = Locations :: where('locationid','=',$request->locationid)->get();
				date_default_timezone_set($locationname[0]['timezone']);
				
			}
			
			}
			else
			{
				$response->cartitems 	= $cart_items;
				$response->msg 		= trans('messages.price_computed');
				$response->istbc = $istbc;
				if($price_change==1){
				$response->price_change_msg="Your script prices may have been adjusted to match entitlement details provided.";
				}else{
				$response->price_change_msg="";
				}
				$response->status 		= $this->successStatus;
				return json_encode($response);
			}				
			}				
			
		
        }
		else
		{
			$response->cartitems 	= $cart_items;
			$response->istbc = $istbc;
			if($price_change==1){
				$response->price_change_msg="Your script prices may have been adjusted to match entitlement details provided.";
			}else{
				$response->price_change_msg="";
			}
			$response->msg 		= trans('messages.price_computed');
			$response->status 		= $this->successStatus;
			return json_encode($response);
		}
		
		}
    public function compute_price($location,$drug,$strength,$quantity,$profile,$isgeneric){
		$price="NA";
		if($location !="")
		{
		$business = Locations :: where('locationid', $location)->get();
		}
		//echo "Madhoo";print_r($location);exit;
        //if(empty($price_details) || count($price_details)==0){
		    
		//$price_details = DB::select("select CP.*,C.ShortDescription,C.GST,C.ItemCommodityClass from catalouge_master as C LEFT JOIN catalouge_pricing as CP ON C.MedmateItemCode = CP.medmateItemcode LEFT JOIN catalouge_item_knownas as CK ON C.MedmateItemCode = CK.MedmateItemCode where CK.Item_knownas LIKE '".$drug ."%' and CP.location_id=".$location."");        
$price_details = DB::select("select  `CO`.StandardPrice,CO.SalePrice, `CS`.medicare,CS.Con_Pens_ConSafty,CS.Veterans,CS.SafetyNet,CS.StandardPrice,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,CS.status, `CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode`  where ((`CO`.`businessid` = ".$business[0]['OTCCatalogueBusinessID']." and CO.location_id=".$location." and `C`.`itemNameDisplay` LIKE '%".$drug."%' and C.Strength LIKE '%".$strength."%'  and `CO`.`Pricing_Tier` = ".$business[0]['OTCPricingTier']." ) or (`CO`.`businessid` = ".$business[0]['OTCCatalogueBusinessID']." and CO.location_id=0 and `C`.`itemNameDisplay` LIKE '%".$drug."%' and C.Strength LIKE '%".$strength."%'  and `CO`.`Pricing_Tier` = ".$business[0]['OTCPricingTier']." )) or ((`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=".$location." and `C`.`itemNameDisplay` LIKE '%".$drug."%' and C.Strength LIKE '%".$strength."%' and C.units_per_pack = '".$quantity."'  and  `CS`.`Pricing_Tier` = ".$business[0]['ScriptPricingTier']." and `CS`.`status` = 1)or(`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=0 and `C`.`itemNameDisplay` LIKE '%".$drug."%' and C.Strength LIKE '%".$strength."%' and C.units_per_pack = '".$quantity."'  and  `CS`.`Pricing_Tier` = ".$business[0]['ScriptPricingTier']." ))");        
if(count($price_details)==0){
$price_details = DB::select("select  `CO`.StandardPrice,CO.SalePrice, `CS`.medicare,CS.Con_Pens_ConSafty,CS.Veterans,CS.SafetyNet,CS.StandardPrice,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,CS.status, `CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode` left join catalouge_item_knownas as CK on `C`.`MedmateItemCode` = `CK`.`MedmateItemCode` where ((`CO`.`businessid` = ".$business[0]['OTCCatalogueBusinessID']." and CO.location_id=".$location." and `CK`.`item_knownas` LIKE '%".$drug."%' and CK.Strength LIKE '%".$strength."%' and `CO`.`Pricing_Tier` = ".$business[0]['OTCPricingTier']." ) or (`CO`.`businessid` = ".$business[0]['OTCCatalogueBusinessID']." and CO.location_id=0 and `CK`.`item_knownas` LIKE '%".$drug."%' and CK.Strength LIKE '%".$strength."%' and `CO`.`Pricing_Tier` = ".$business[0]['OTCPricingTier']." )) or ((`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=".$location." and `CK`.`item_knownas` LIKE '%".$drug."%' and CK.Strength LIKE '%".$strength."%' and C.units_per_pack = '".$quantity."' and `CS`.`Pricing_Tier` = ".$business[0]['ScriptPricingTier']." and `CS`.`status` = 1)or(`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=0 and `CK`.`item_knownas` LIKE '%".$drug."%' and CK.Strength LIKE '%".$strength."%'and CK.units_per_pack = '".$quantity."' and`CS`.`Pricing_Tier` = ".$business[0]['ScriptPricingTier']." ))");        	
/*if(count($price_details)==0){
$price_details = DB::select("select  `CO`.StandardPrice,CO.SalePrice, `CS`.medicare,CS.Con_Pens_ConSafty,CS.Veterans,CS.SafetyNet,CS.StandardPrice,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,CS.status, `CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode`  where (`CO`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$drug."%' and `CO`.`Pricing_Tier` = ".$business[0]['OTCPricingTier']." ) or (`CS`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$drug."%' and `CS`.`Pricing_Tier` = ".$business[0]['ScriptPricingTier']." and `CS`.`status` = 1)");        
if(count($price_details)==0){
	$price_details = DB::select("select  `CO`.StandardPrice,CO.SalePrice, `CS`.medicare,CS.Con_Pens_ConSafty,CS.Veterans,CS.SafetyNet,CS.StandardPrice,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,CS.status, `CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode` left join `catalouge_item_knownas` as `CK` on `C`.`MedmateItemCode` = `CK`.`MedmateItemCode` where (`CO`.`businessid` = 0 and `CK`.`item_knownas` LIKE '%".$drug."%' and `CO`.`Pricing_Tier` = ".$business[0]['OTCPricingTier']." ) or (`CS`.`businessid` = 0 and `CK`.`item_knownas` LIKE '%".$drug."%' and `CS`.`Pricing_Tier` = ".$business[0]['ScriptPricingTier']." and `CS`.`status` = 1)");        
}
}*/
}	
	   //}              
                        $healthprofile = DB::table('healthprofile')->where('profileid',$profile)->get();
		 				 //print_r($isgeneric);
						 //print_r($price_details[0]->Generic_SafetyNet);exit;
						 if(count($price_details)>0){
							 
						if($price_details[0]->ItemCommodityClass=='OTC')
						{
							if($price_details[0]->SalePrice !=0 || $price_details[0]->SalePrice!=NULL)
							{
								$p=$price_details[0]->SalePrice;
								$gst =(($price_details[0]->GST)*$p)/100;
								$price =$p+ $gst;
								$price=$price."+standard";
							}
							else
							{
								$p=$price_details[0]->OTCStandardprice;
								$gst =(($price_details[0]->GST)*$p)/100;
								$price =$p+ $gst;
								$price=$price."+standard";
							}
							
						}
						else
						{
							if(count($healthprofile)>0){
							
                                if($healthprofile[0]->safetynet!=null){
									if($isgeneric==1 && $price_details[0]->Generic_SafetyNet!=NULL){
									$p=$price_details[0]->Generic_SafetyNet;}else{
                                    $p=$price_details[0]->SafetyNet;}
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									$price=$price."+safteynet";
									//if($isgeneric==1 && $price_details[0]->Generic_SafetyNet!=NULL){
									//$p=$price_details[0]->Generic_SafetyNet;
									//$gst =(($price_details[0]->GST)*$p)/100;
									//$price =$p+ $gst;
									//$price=$price."+safteynet";
									//}								
                                }
                                else if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                    if($isgeneric==1 && $price_details[0]->Generic_Con_Pens_ConSafty!=NULL){
									$p=$price_details[0]->Generic_Con_Pens_ConSafty;}else{
									$p=$price_details[0]->Con_Pens_ConSafty;}
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									$price=$price."+concession";
									//if($isgeneric==1 && $price_details[0]->Generic_Con_Pens_ConSafty!=NULL){
									//$p=$price_details[0]->Generic_Con_Pens_ConSafty;
									//$gst =(($price_details[0]->GST)*$p)/100;
									//$price =$p+ $gst;
									//$price=$price."+concession";
									//}
                                }
								else if($healthprofile[0]->veteranaffairsnumber!=null){
                                    if($isgeneric==1 && $price_details[0]->Generic_Veterans!=NULL){
									$p=$price_details[0]->Generic_Veterans;}else{
									$p=$price_details[0]->Veterans;}
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									$price=$price."+concession";
									//if($isgeneric==1 && $price_details[0]->Generic_Veterans!=NULL){
									//$p=$price_details[0]->Generic_Veterans;
									//$gst =(($price_details[0]->GST)*$p)/100;
									//$price =$p+ $gst;
									//$price=$price."+concession";
									//}
                                }
                                else if($healthprofile[0]->medicarenumber!=null){
									if($isgeneric==1 && $price_details[0]->Generic_Medicare!=NULL){
									$p=$price_details[0]->Generic_Medicare;}else{
                                    $p=$price_details[0]->medicare;}
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									$price=$price."+medicare";
									//if($isgeneric==1 && $price_details[0]->Generic_Medicare!=NULL){
									//$p=$price_details[0]->Generic_Medicare;
									//$gst =(($price_details[0]->GST)*$p)/100;
									//$price =$p+ $gst;
									//$price=$price."+medicare";
									//}
                                }else{
									if($isgeneric==1 && $price_details[0]->Generic_StandardPrice!=NULL){
									$p=$price_details[0]->Generic_StandardPrice;}else{
                                    $p=$price_details[0]->ScriptStandardprice;}
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									$price=$price."+standard";
									//if($isgeneric==1 && $price_details[0]->Generic_StandardPrice!=NULL){
									//$p=$price_details[0]->Generic_StandardPrice;
									//$gst =(($price_details[0]->GST)*$p)/100;
									//$price =$p+ $gst;
									//$price=$price."+standard";
									//}
                                }

                            }
							
						}
                        }
						else{
							
							$cardtype="standard";							
							if($healthprofile[0]->medicarenumber!=null){$cardtype="medicare";}
							if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){$cardtype="concession";}
							if($healthprofile[0]->veteranaffairsnumber!=null){$cardtype="concession";}
							if($healthprofile[0]->safetynet!=null){$cardtype="safteynet";}
                            $price="NA+".$cardtype;
                        }
                        return $price;
                        

    }   
	/**
      @OA\Get(
          path="/v3/getpromodeliverylimit",
          tags={"promodeliverylimit"},
          summary="promodeliverylimit",
          operationId="promodeliverylimit",
		  security={{"bearerAuth": {}} },
      
          
		  @OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		 
		  	
         
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function getpromodeliverylimit(Request $request)
	{
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get();
		$isscript=0;
		$cartinfo = Cart :: where('enduserid' ,'=', $user->userid)->where('scriptavailable' ,'=', 1)->where('cartstatus', '=' ,1)->get();
		if(count($cartinfo)>0)
		{
			$isscript=1;
		}
		$script="";
		if($isscript==1){
			$script=" and scriptval=1";
		}else{
			$script=" and scriptval=2";
		}
		
		$deliverylimit = DB::select("select deliverylimit from deliverypartner where locationid=".$request->locationid." and partnername like '%Standard Delivery%' and partnerstatus=1".$script); 
		$response->deliverylimit 	= $deliverylimit;
		$response->status 		= $this->successStatus;
		return json_encode($response);	
	}
		
}
