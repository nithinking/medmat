<?php

namespace App\Http\Controllers\api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Deliveryaddress;
use App\models\Cart;
use App\models\Healthprofile;
use App\models\Prescriptiondetails; 
use App\models\Prescriptionitemdetails; 
use App\models\Locations; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class V3CartController extends Controller
{
    public $successStatus = 200;
	/**
      @OA\Post(
          path="/v3/addtoCart",
          tags={"Cart"},
          summary="Add To Cart",
          operationId="Add To Cart",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  
		  @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptiontype",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="medlistid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="name",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="form",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="strength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="quantity",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="drugpack",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="scriptavailable",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  
		  @OA\Parameter(
              name="referrence",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
	    @OA\Parameter(
              name="image",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
	    @OA\Parameter(
              name="cartprice",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
	    @OA\Parameter(
              name="itemschedule",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
	    
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function addtoCart(Request $request)
	 {
		 // print_r($request->userid);
		 // print_r($request->itemschedule);
		 
		 // exit;
		 $user = Auth::user();
		 $healthprofile = Healthprofile::where('userid',  $user->userid)->where('parentuser_flag' , '1')->first();
		 $cartVal = Cart :: where('enduserid' , '=' ,$user->userid)
							->where('prescriptionid', '=' ,$request->prescriptionid)
							->where('cartstatus', '=' ,1)
							->get();
					
		 $cartrows = $cartVal->count();
		 if($cartrows == 0)
		 {
		 $cart = new Cart;
		 
		 $cart->enduserid = $user->userid;
		 $cart->userid = $request->userid;
		 $cart->profileid = $request->profileid;
		 $cart->locationid = $request->locationid;
		 $cart->prescriptionid = $request->prescriptionid;
		 $cart->prescriptiontype = $request->prescriptiontype;
		 $cart->medlistid = $request->medlistid;
		 $cart->prescriptionfor = $request->prescriptionfor;
		 $cart->name = $request->name;
		 if(isset($request->generic)){
		 $cart->isgeneric=$request->generic;}
		 else{
			 $cart->isgeneric=0;
		 }
		 if(isset($request->type)&&$request->type==2){
			 $cart->type=$request->type;
		 }
		 $cart->quantity = $request->quantity;
		 $cart->drugpack = $request->drugpack;
		 $cart->strength = $request->strength;
		 $cart->referrencetable = $request->referrence;		
		 $cart->form = $request->form;
		 $cart->image = $request->image;
		 $cart->cardtype= $request->cardType;
		 if($request->price!='')
		 $cart->cartprice = $request->price;	
		else
		 $cart->cartprice = '0.00';
		 $cart->scriptavailable = $request->scriptavailable;		 
		 if($cart->cartid !="")
				$cart->itemgrouptype = "Pharmacy Basket";
		 elseif($cart->cartid !="" && $cart->medlistid !="")
				$cart->itemgrouptype = "Pharmacy Basket";
		 elseif($cart->cartid =="" && $cart->medlistid !="")
				$cart->itemgrouptype = "Health Store Basket";
		 else	
				$cart->itemgrouptype = "Health Store Basket";
		$cart->itemschedule = $request->itemschedule;
		$cart->cartstatus ='1';
		$cart->save();
		 }
		 else
		 {
			 $qty = $cartVal[0]['drugpack']+$request->drugpack;
			$cartUpdate = Cart :: where('cart.enduserid' , '=' ,$user->userid)
									->where('cart.prescriptionid', '=' ,\DB::raw('"'.$request->prescriptionid.'"'))
									->update(['cart.drugpack' => $qty]); 
			$cart = Cart :: where('cart.enduserid' , '=' ,$user->userid)
						  ->where('cart.prescriptionid', '=' ,$request->prescriptionid)
						  ->get();
		 }
		 
		if($request->prescriptionid !="")
		{
		$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$request->prescriptionid.'"'))
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.medstatus' => 'InCart']);
		if($request->prescriptiontype==1){
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$request->prescriptionid.'"'))
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.medstatus' => 'InCart','prescriptionitemdetails.ismedlist'=>1]);
		
		}			
		//To Check if uploaded script is related Medlist Medication script
		$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$request->prescriptionid.'"'))
													->where('prescriptionitemdetails.profileid', '=', $request->profileid)
													->get();
		$count = $getmedlistid->count();
		if($count > 0)
		{
			if($count == 1)
			$medlistdetails = $getmedlistid[0]['medilistid'];
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
											->where('prescriptionitemdetails.profileid', '=', $request->profileid)
											->update(['prescriptionitemdetails.medstatus' => 'InCart']);
		}
		
		}
		else
		{
		$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $request->medlistid)
											->where('prescriptionitemdetails.profileid', '=', $request->profileid)
											->update(['prescriptionitemdetails.medstatus' => 'InCart']);
		}
		if($healthprofile->medicarenumber!=null || $healthprofile->medicarenumber!=''){
		//$cart->medicarestatus=1;
		//$cart->concessionstatus=1;}else{
		$cart->medicarestatus=2;
		$cart->concessionstatus=2;
		}
        return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	 }
	 /**
      @OA\Get(
          path="/v3/getCartItems",
          tags={"Cart"},
          summary="Get Cart Items",
          operationId="Get Cart Items",
		  security={{"bearerAuth": {}} },
		   @OA\Parameter(
              name="locationid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function getCartItems(Request $request)
	 {
		 $response=(object)array();
		 $user = Auth::user();
		 if(isset($request->type)&&$request->type==2){
			 $cart = Cart :: leftJoin('prescriptionitemdetails','prescriptionitemdetails.prescriptionid' ,'=','cart.prescriptionid')->where('enduserid', $user->userid)->where('cartstatus' , '1')->where('cart.type','2')->select('cart.*','prescriptionitemdetails.script_url')->get();
		 }else{
		 $cart = Cart :: leftJoin('prescriptionitemdetails','prescriptionitemdetails.prescriptionid' ,'=','cart.prescriptionid')->where('enduserid', $user->userid)->where('cartstatus' , '1')->where('cart.type','1')->select('cart.*','prescriptionitemdetails.script_url')->get();
		 }
		 /*if(isset($request->type)&&$request->type==2){
		 $cart = Cart :: where('enduserid', $user->userid)->where('cartstatus' , '1')->where('type','2')->get();
		 }else{
		 $cart = Cart :: where('enduserid', $user->userid)->where('cartstatus' , '1')->where('type','1')->get();
		 }*/
		 //print_r($cart);exit;
		 $nooforders = $cart->count();
		 if($cart->count()>0)
		 {
			 $locationname = Locations :: select('locationname')->where('locationid', $cart[0]['locationid'])->get();
			 $response->locationname = $locationname;
		 }
		for($i=0;$i<$nooforders;$i++)
		{
			$image=$cart[$i]['image'];
			if(strpos($image, '.jpg.bmp') == true){
				$exp=explode(".jpg",$image);
				$image=$exp[0].".jpg";				
			}
			if(strpos($image, '.jpg.jpg') == true){
				$exp=explode(".jpg",$image);
				$image=$exp[0].".jpg";				
			}
			if(strpos($image, '.bmp.jpg') == true){
				$exp=explode(".bmp",$image);
				$image=$exp[0].".bmp";				
			}
			if(strpos($image, '.bmp.bmp') == true){
				$exp=explode(".bmp",$image);
				$image=$exp[0].".bmp";				
			}
			$cart[$i]['image']=$image;
			/*$presctipriontype[$i] = Prescriptiondetails :: leftJoin('prescriptionitemdetails','prescriptionitemdetails.prescriptionid' ,'=','prescriptiondetails.prescriptionid')	
														->where('prescriptiondetails.prescriptionid', '=',$cart[$i]['prescriptionid'])
														->orWhere('prescriptionitemdetails.prescriptionid', '=',\DB::raw('"'.$cart[$i]['prescriptionid'].'"'))
															//->select('prescriptiondetails.*','prescriptionitemdetails.*')->get();
															->select('prescriptionitemdetails.script_url')->get();
			if(count($presctipriontype[$i])>0){
			$cart[$i]['script_url']=$presctipriontype[$i][0]->script_url;}
			else{
				$cart[$i]['script_url']="";
			}*/
			//$cart[$i]->cardtype=$this->getcardtype($cart[$i]->enduserid,$cart[$i]->cartprice);
			//$cart[$i]['prescriptiondetails']	= 	$presctipriontype[$i];
		}
		$isscript=0;
		 
		 if(isset($request->type)&&$request->type==2){
			 $cartinfo = Cart :: where('enduserid' ,'=', $user->userid)->where('scriptavailable' ,'=', 1)->where('cartstatus', '=' ,1)->where('type','2')->get();
		 }else{
			 $cartinfo = Cart :: where('enduserid' ,'=', $user->userid)->where('scriptavailable' ,'=', 1)->where('cartstatus', '=' ,1)->where('type',1)->get();
		 }
		if(count($cartinfo)>0)
		{
			$isscript=1;
		}
		if($request->locationid!="")
		{
			$script="";
		if($isscript==1){
			$script=" and scriptval=1";
		}else{
			$script=" and scriptval=2";
		}
		$deliveryAmount = DB::select("select *  from deliverypartner where locationid=".$request->locationid." and partnername like '%Standard Delivery%' and partnerstatus=1".$script); 
		if(count($deliveryAmount)==0){
			$deliveryAmount = DB::select("select *  from deliverypartner where locationid=".$request->locationid."  and partnerstatus=1".$script);
		}if(count($deliveryAmount)==0){
			$deliveryAmount = DB::select("select *  from deliverypartner where locationid=0  and partnerstatus=1".$script);
		}
		$deliveryfee = $deliveryAmount[0]->deliverylimit;
		$deliverycharge = $deliveryAmount[0]->deliverycharge;
		$response->deliveryfee 	= $deliveryfee;
		$response->deliverycharge = $deliverycharge;
		}
		$response->cart 	    = $cart;
		
		
		$response->status 		= $this->successStatus;
			
		 return json_encode($response);
       // return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	 }
		/**
		@OA\Post(
          path="/v3/removeCartItems",
          tags={"Cart"},
          summary="Remove Cart Item",
          operationId="Remove Cart Item",
		  security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="cartid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ),
          
        @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
        ),
        @OA\Response(
              response=401,
              description="Unauthorized"
        ),
        @OA\Response(
              response=400,
              description="Invalid request"
        ),
        @OA\Response(
              response=404,
              description="not found"
        ),
      )
     */
	public function removeCartItems(Request $request)
	{
		 $user = Auth::user();
		 
		 $cart = Cart :: where('cart.enduserid', '=',$user->userid)->where('cart.cartid' ,'=', $request->cartid)
						 ->update(['cart.cartstatus' => '0']);
		 
		 $cartItemDetails = Cart :: where('cart.cartid' ,'=', $request->cartid)->get();
		 
		 $medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cartItemDetails[0]['medlistid'])
											->update(['prescriptionitemdetails.medstatus' => 'medlist']); 
		
		$medlistinfo = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cartItemDetails[0]['medlistid'])
												->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[0]['profileid'])
												->get();
		if($cartItemDetails[0]['prescriptiontype'] == "1")
		{
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cartItemDetails[0]['prescriptionid'].'"'))
											->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[0]['profileid'])
											->update(['prescriptionitemdetails.medstatus' => 'medlist','prescriptionitemdetails.ismedlist'=>1]);
		}										
		if($medlistinfo->count() > 0)
		{
			if($medlistinfo[0]['medilistid'] != "")
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $medlistinfo[0]['medilistid'])
											->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[0]['profileid'])
											->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
		}
		
		if($cartItemDetails[0]['prescriptiontype'] == "3")
		{
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cartItemDetails[0]['prescriptionid'].'"'))
											->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[0]['profileid'])
											->update(['prescriptionitemdetails.medstatus' => 'medlist']);
		}
		return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	}
	
		/**
		@OA\Post(
          path="/v3/updateCartItem",
          tags={"Cart"},
          summary="Update Quanity In Cart Item",
          operationId="Update Quanity In Cart Item",
		  security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="cartid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ), 
		@OA\Parameter(
              name="drugpack",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ), 
			
        @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
        ),
        @OA\Response(
              response=401,
              description="Unauthorized"
        ),
        @OA\Response(
              response=400,
              description="Invalid request"
        ),
        @OA\Response(
              response=404,
              description="not found"
        ),
      )
     */
	public function updateCartItem(Request $request)
	{
		 $user = Auth::user();
		 
		 $cart = Cart :: where('cart.enduserid', '=',$user->userid)->where('cart.cartid' ,'=', $request->cartid)
						 ->update(['cart.drugpack' => $request->drugpack]);
		 
		 
        return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	}
	
	/**
		@OA\Post(
          path="/v3/removeAllCartItems",
          tags={"Cart"},
          summary="Remove Cart Item",
          operationId="Remove Cart Item",
		  security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ),
          
        @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
        ),
        @OA\Response(
              response=401,
              description="Unauthorized"
        ),
        @OA\Response(
              response=400,
              description="Invalid request"
        ),
        @OA\Response(
              response=404,
              description="not found"
        ),
      )
     */
	public function removeAllCartItems(Request $request)
	{
		 $user = Auth::user();
		
		 
		$cartItemDetails = Cart :: where('cart.enduserid' ,'=', $request->userid)->where('cartstatus',1)->get();
		//print_r(count($cartItemDetails));exit;
		 for($i=0;$i<count($cartItemDetails);$i++)
		 {
			
			if(isset($cartItemDetails[$i]['prescriptiontype']) && $cartItemDetails[$i]['prescriptiontype'] == "1" )
			{
				
				
											
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cartItemDetails[$i]['medlistid'])
											->update(['prescriptionitemdetails.medstatus' => 'medlist']); 
		
				$medlistinfo = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cartItemDetails[$i]['medlistid'])
												->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[$i]['profileid'])
												->get();
				
				$addtomedlist = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cartItemDetails[$i]['prescriptionid'].'"'))
											->update(['prescriptionitemdetails.ismedlist' => 1]);
				$addtomedliststs = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cartItemDetails[$i]['prescriptionid'].'"'))
											->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				 if($medlistinfo->count()>0)
				{
					
					for($m=0;$m<$medlistinfo->count();$m++)
					{
						if($medlistinfo[$m]['medilistid'] != "" )
						$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $medlistinfo[$m]['medilistid'])
														->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[$i]['profileid'])
														->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
					}
				}
			}
			 elseif(isset($cartItemDetails[$i]['prescriptiontype']) && $cartItemDetails[$i]['prescriptiontype'] == "3")
				{ 
			 $medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cartItemDetails[$i]['prescriptionid'].'"'))
											 ->where('prescriptionitemdetails.profileid', '=', $cartItemDetails[$i]['profileid'])
											 ->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				 }
			
				 
		//$cart = Cart :: where('enduserid', '=',$request->userid)->where('prescriptionid',\DB::raw('"'.$cartItemDetails[$i]['prescriptionid'].'"'))->update(['cartstatus' => 0]);	
			 		 }
		 $cart = Cart :: where('enduserid', '=',$request->userid)->update(['cartstatus' => 0]);									
        $lastselectaddress = Deliveryaddress:: where('deliveryaddresses.userid', '=', $user->userid)->where('currentselectaddress',1)->where('deliveryaddresssts',1)->update(['currentselectaddress'=>0]);
		$usercurrentlocation = User :: where('userid' , $user->userid)->where('userstatus' , 1)->update(['currentlocationid'=>null]);				
        
		return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	}
	/**
		@OA\Post(
          path="/v3/changeCartOwner",
          tags={"Cart"},
          summary="changeCartOwner",
          operationId="changeCartOwner",
		  security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="guestUserID",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ), 
		@OA\Parameter(
              name="userID",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ), 
		@OA\Parameter(
              name="guestProfileID",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ),
		@OA\Parameter(
              name="profileID",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ), 

			
        @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
        ),
        @OA\Response(
              response=401,
              description="Unauthorized"
        ),
        @OA\Response(
              response=400,
              description="Invalid request"
        ),
        @OA\Response(
              response=404,
              description="not found"
        ),
      )
     */
	public function changeCartOwner(Request $request)
	{
		 $user = Auth::user();	
		 $cart = DB::table('cart')->where('enduserid',$request->guestUserID)->where('profileid',$request->guestProfileID)->where('cartstatus',1)->get();
		 $ocart = DB::table('cart')->where('enduserid',$request->userID)->where('profileid',$request->profileID)->where('cartstatus',1)->get();
		 $norders=$ocart->count();
		 for($j=0;$j<$norders;$j++){
			DB::table('cart')->where('cartid',$cart[$j]->cartid) 
						 ->update(['cartstatus' => 0]); 
		 }
		 $nooforders=$cart->count();
		 for($i=0;$i<$nooforders;$i++){
			 DB::table('cart')->where('cartid',$cart[$i]->cartid) 
						 ->update(['enduserid' => $request->userID,'userid'=>$request->userID,'profileid'=>$request->profileID]);
		 }
		 $cart = DB::table('cart')->where('enduserid',$request->userID)->where('profileid',$request->profileID)->where('cartstatus',1)->get();
        return response()->json(['cart' => $cart] ,$this->successStatus); 
       
	}
	
	 
}
