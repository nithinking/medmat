<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Orderdetails;
use App\models\Ordertracking;
use App\models\Orderbasketlist;
use App\models\Deliveryaddress;
use App\models\Adminprofiletype; 
use App\models\Healthprofile; 
use App\models\Basket; 
use App\models\Basketitemlist; 
use App\models\Invoice; 
use App\models\Orderinvoicelist; 
use App\models\Locations;  
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class V3PaymentController extends Controller
{
    public $successStatus = 200;
    /**
      @OA\Post(
          path="/v2/payment",
          tags={"Payment"},
          summary="Payment",
          operationId="payment",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="order_id",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="amount",
              in="query",
              required=true,
              @OA\Schema(
                  type="float"
              )
              ),
			  @OA\Parameter(
              name="deliverypartnername",
              in="query",
              required=false,
              @OA\Schema(
                  type="float"
              )
              ),
			  @OA\Parameter(
              name="deliveryfee",
              in="query",
              required=false,
              @OA\Schema(
                  type="float"
              )
              ),
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
    public function createinvoice(Request $request){
        $response = (object)array();
        $invoiceReference=$request->order_id;
        $order_id=$request->order_id;
		$amount=round($request->amount, 2);
		if($request->deliverypartnername!="" && $request->deliveryfee!="")
			{
			DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['deliverypartnername' => $request->deliverypartnername,'deliveryfee' => $request->deliveryfee]);

			}
		if(env('ENV') == "TEST")	
		$paymenturl="https://test-app.medmate.com.au/stripe/".$amount."/".$order_id;
		else if(env('ENV') == "DEV")	
		$paymenturl="https://dev-app.medmate.com.au/stripe/".$amount."/".$order_id;
		else
		$paymenturl="https://pharmacy.medmate.com.au/stripe/".$amount."/".$order_id;
     /*   $vars=array (
            'invoiceReference' => $invoiceReference,
            'providerNumber' => 'MEDMATE1',
            'nonClaimableItems' => 
            array (
              0 => 
              array (
                'reference' => '01',
                'description' => $order_id,
                'chargeAmount' => $request->amount,
              ),
            ),
            'callbackUrls' => 
            array (
              0 => 
              array (
                'url' => 'https://pharmacy.medmate.com.au/success',
                'event' => 'success',
              ),
              1 => 
              array (
                'url' => 'https://pharmacy.medmate.com.au/failure',
                'event' => 'failure',
              ),
              2 => 
              array (
                'event' => 'cancel',
                'url' => 'https://pharmacy.medmate.com.au/cancel',
              ),
            ),
            'webhooks' => 
            array (
              0 => 
              array (
                'method' => 'get',
                'url' => 'https://pharmacy.medmate.com.au/orders/6EB309C5-07A4-40DD-BD4A-FD78302A9570',
                'event' => 'invoiceBalancePaid',
              ),
            ),
        );
		//print_r(json_encode($vars));exit;
        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api-au.medipass.io/v3/transactions/invoices",*/
  /*$vars=array (
            'invoiceReference' => $invoiceReference,
            'providerNumber' => '2429581T',
            'nonClaimableItems' => 
            array (
              0 => 
              array (
                'reference' => '01',
                'description' => $order_id,
                'chargeAmount' => $request->amount,
              ),
            ),
            'callbackUrls' => 
            array (
              0 => 
              array (
                'url' => 'http://13.211.147.93/success',
                'event' => 'success',
              ),
              1 => 
              array (
                'url' => 'http://13.211.147.93/failure',
                'event' => 'failure',
              ),
              2 => 
              array (
                'event' => 'cancel',
                'url' => 'http://13.211.147.93/cancel',
              ),
            ),
            'webhooks' => 
            array (
              0 => 
              array (
                'method' => 'get',
                'url' => 'http://13.211.147.93/orders/6EB309C5-07A4-40DD-BD4A-FD78302A9570',
                'event' => 'invoiceBalancePaid',
              ),
            ),
        );
		//print_r(json_encode($vars));exit;
        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://stg-api-au.medipass.io/v3/transactions/invoices",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
 // CURLOPT_POSTFIELDS =>"{\n  \"invoiceReference\" : \"abcd54324\",\n  \"providerNumber\" : \"2429581T\",\n  \"nonClaimableItems\": [\n    {\n        \"reference\": \"01\",\n        \"description\": \"Order 54989\",\n        \"chargeAmount\": \"41.23\"\n        \n    }\n  ],\n  \"callbackUrls\": [\n    {\n      \"url\": \"https://pharmacy.medmate.com.au/success\",\n      \"event\": \"success\"\n    },\n    {\n      \"url\": \"https://pharmacy.medmate.com.au/failure\",\n      \"event\": \"failure\"\n    },\n    {\n      \"event\": \"cancel\",\n      \"url\": \"https://pharmacy.medmate.com.au/cancel\"\n    }\n  ],\n  \"webhooks\": [\n    {\n      \"method\": \"get\",\n      \"url\": \"https://pharmacy.medmate.com.au/orders/6EB309C5-07A4-40DD-BD4A-FD78302A9570\",\n      \"event\": \"invoiceBalancePaid\"\n    }\n  ]\n}",
 CURLOPT_POSTFIELDS=>json_encode($vars), 
 CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer 5ecf52545c6ee5005784421d:4za40PgN7IQFD2t94Jsq_WoLhRdOz54c0anJJSsdYjE",
    "Content-Type: application/json",
    "x-appid: medmate-web"
  ),*/
 /*CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer 5ee6d6701aaee7004b74c711:WUI-V9OznrNOe63cf-4bicDLFc56PFJAvCfYlvHbmLc",
    "Content-Type: application/json",
    "x-appid: medmate-web"
  ),*/
//));
//$server_output = curl_exec($curl);

//print_r($server_output);exit;
//curl_close($curl);
//if(isset($server_output)){
//$server_output=json_decode($server_output);
//DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['transactionId' => $server_output->transactionId,'invoice_id' => $server_output->_id]);
$response->invoice_details 	= $paymenturl;
$response->msg 		= trans('messages.succ_message');
$response->status 		= $this->successStatus;
//}
return json_encode($response);	

    }
/**
      @OA\Post(
          path="/v2/paymentstatus",
          tags={"Payment"},
          summary="PaymentStatus",
          operationId="paymentstatus",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="order_id",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="payment_status",
              in="query",
              required=true,
              @OA\Schema(
                  type="integer"
              )
              ),
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */    
public function paymentstatus(Request $request){
  $response = (object)array();
  //$orderdetails=DB::table('orderdetails')->where('uniqueorderid',$request->order_id)->get();
  //if($orderdetails[0]->paymentstatus==0 || $orderdetails[0]->paymentstatus==NULL){
  DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['paymentstatus' => $request->payment_status]);
if($request->payment_status==1){
$response->msg 		= "Success";
DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['isnew' => 0]);
$orderid = Orderdetails :: where('uniqueorderid',  $request->order_id)->get();
$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$orderid[0]->orderid)->where('ordertracking.trackingstatus','=',1)->get();
		
$noofrows = $ordertrack->count();

for($i=0;$i<$noofrows;$i++)	
{
	$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$orderid[0]->orderid)
										 ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
									     ->update(['ordertracking.trackingstatus' => '0']);
	
									
}

if($orderstsChange)
{
	date_default_timezone_set('UTC');
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $orderid[0]->orderid;
			$orderTracking->orderstatus = "Process";
			$orderTracking->orderremarks = '';
			$orderTracking->isnew = '1';
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	
			$this->paymentemail($orderid[0]->orderid,$request->order_id);
}
//$orderid = $orderid[0]->orderid;

			
}
else if($request->payment_status==0)
{$response->msg = trans('messages.failure_message');}
else{$response->msg="Cancel";}
  //}
  //else{
	//  $response->msg 		= "Success";
  //}
$response->status 		= $this->successStatus;
return json_encode($response);
}
  
 public function paymentemail($orderid,$uniqueorderid)
 {
	 
	 $orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')								
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')								 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','locations.*','orderdetails.deliveryfee as orderdeliveryfee','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.locationid','locations.email','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , 'Process')
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
								  $userpreffereddate=$orderData[0]->userpreffereddate;
								  $userprefferedtime=$orderData[0]->userprefferedtime;
								  
								 // print_r($orderData);exit;
		
        //echo   $orderData[0]['myuserid'];exit;
		$locationname = Locations :: where('locationid','=',$orderData[0]['locationid'])->get();
		//$user_details = User::findOrFail($user->userid);
		$user_details = User :: where('userid', $orderData[0]['myuserid'])->get();
		
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		$deliveryaddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();									
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->distinct()
									->select('orderbasketlist.*')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Process")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$deliverydate = $orderdate = date('d F Y', strtotime($orderData[0]['userpreffereddate']));
		$placeddate = date('d F Y', strtotime($orderData[0]['orderDate']));
		$deliveryfee=0;
		$prescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->get();
        $nqty=0;$nprice=0;
					if($prescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$prescriptionitems->count();$np++)
						{
							$nqty = $nqty+$prescriptionitems[$np]['drugpack'];
							$priceval = $prescriptionitems[$np]['drugpack']*$prescriptionitems[$np]['originalprice'];
							$nprice = $priceval+$nprice;
						}
					} 
			$nonprescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 0)
                                              ->get();
        $qty=0;$npriceval =0;
		//'.$tbctext.'
					if($nonprescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$nonprescriptionitems->count();$np++)
						{
							$qty = $qty+$nonprescriptionitems[$np]['drugpack'];
							$npval=$nonprescriptionitems[$np]['drugpack']*$nonprescriptionitems[$np]['originalprice'];
							$npriceval = $npriceval+$npval;
						}
					} 
			$subtotal = $npriceval+$nprice;
			$gst=0;
			$gstcalc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->get();
			for($i=0;$i<$gstcalc->count();$i++)
			{
				$gstcalcval = $this->compute_price($orderData[0]['locationid'],$gstcalc[$i]['prescriptionid'],$orderData[0]['myuserid']);
				$gst = $gst+$gstcalcval;
			}
			$orderid=$orderData[0]->orderid;
						if(env('ENV') == "TEST")			
			$link = "https://test-app.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			else if(env('ENV') == "DEV")
			$link = "https://dev-app.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			else
			$link = "https://pharmacy.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			
			$body_message = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Order Confirmation</h1>  
                                                <p>Hi '.$orderData[0]['fname'].',<br>
                                                Thank you for your recent purchase. Your order has been successfully sent to the pharmacy for fulfilment.</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Your Order Details</h3>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetails"> 
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td class="orderDetailsTop">
                                                            Order: '.$uniqueorderid.'<br>
                                                            Placed on '.$placeddate.'
                                                        </td>        
                                                        <td width="20" style="width:20px;">&nbsp;</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetailsPurchases"> 
                                                                <thead>
                                                                    <tr>
                                                                        <th>Items</th>
                                                                        <th>Qty</th>
                                                                        <th>Price</th>
                                                                    </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Prescription Items<br>
                                                                            
                                                                        </td>
                                                                        <td>'.$nqty.'</td>
                                                                        <td>'.$nprice.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Non-Prescription Items</td>
                                                                        <td>'.$qty.'</td>
                                                                        <td>'.$npriceval.'</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                        <td height="40" style="height:40px;">&nbsp;</td>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Subtotal</td>
                                                        <td>'.$subtotal.'</td>
                                                    </tr>';
                                                    if($orderData[0]['ordertype'] == "Delivery")
                                                    {
                                                    $body_message .=   '<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Delivery&nbsp;Fee</td>
                                                        <td>'.$orderData[0]['orderdeliveryfee'].'</td>
                                                    </tr>';
                                                    }
                                                    $body_message .='<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Order&nbsp;Total</td> 
                                                        <td>'.$orderData[0]['orderTotal'].'</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>GST</td>
                                                        <td>'.$gst.'</td>
                                                    </tr> 
                                                </table>


                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4>Your Details:</h4>
                                                            '.$orderData[0]['fname'].' '.$orderData[0]['lname'].' <br> '.$orderData[0]['mobilenumber'].' <br>   
                                                              ';
                                                              if($orderData[0]['ordertype'] == "Delivery")
                                                              {
                                                                $body_message .=' '.$orderData[0]['addr1'].' '.$orderData[0]['addr2'].' <br>'.$orderData[0]['addsuburb'].', '.$orderData[0]['addstate'].', '.$orderData[0]['addpostcode'].' <br> Australia
                                                              ';
                                                              }
                                                            $body_message .='
                                                        </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$orderData[0]['locationname'].' <br>'.$orderData[0]['phone'].' <br> '.$orderData[0]['address'].' <br> '.$orderData[0]['suburb'].', '.$orderData[0]['state'].','.$orderData[0]['postcode'].'  <br> Australia
                                                        </td>                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
			';
	
			   $message['to']           = $orderData[0]['emailaddress'];
			   
               $message['subject']      = env('ENV')." Medmate – Order #".$uniqueorderid." has been confirmed";
              
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])				  
                  ->subject($message['subject'])				  
                  //->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
				  ->bcc('orders@medmate.com.au','Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			//Email to pharmacy
			
			
			if(env('ENV') == "TEST")
			$link = "https://test-app.medmate.com.au/pharmacy/viewprocessorder/".$orderid;
			else if(env('ENV') == "DEV")	
			$link = "https://dev-app.medmate.com.au/pharmacy/viewprocessorder/".$orderid;
			else
			$link = "https://pharmacy.medmate.com.au/pharmacy/viewprocessorder/".$orderid;
		if($locationname[0]->locationstatus==1 && $orderData[0]->type==1){
			$body_message1='
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order to pharmacy</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {       
                color: #000000 !important;         
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new order.</h1>  
                                                <p>Hello '.$orderData[0]['locationname'].',<br>
                                                A new order has been confirmed and finalised by the customer. <br>
                                                Use the below link to review and process the order.<p>
                                                <h3>Order Details</h3>
                                                <p>'.$orderData[0]['ordertype'].'<br>
                                                '.$deliverydate.', '.$orderData[0]['userprefferedtime'].'</p>

                                                <center>
                                                <a href="'.$link.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/view-order-btn.png" alt="View Order" />
                                                </a>
                                                </center>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>';}elseif($locationname[0]->locationstatus==2 && $orderData[0]->type==1){
				$body_message1='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new Purchase Order.</h1>  
                                                <p>Hello&nbsp;'.$locationname[0]['locationname'].',<br>
                                                A new purchase order has been sent  to your pharmacy from a local customer. <a href="https://medmate.com.au/purchase-order-faqs/" target="_blank">Learn more.</a> 
</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Order Details</h3>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetails"> 
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td class="orderDetailsTop">
                                                            Order: '.$uniqueorderid.'<br>
                                                            Placed on '.$placeddate.'
                                                        </td>        
                                                        <td width="20" style="width:20px;">&nbsp;</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetailsPurchases"> 
                                                                <thead>
                                                                    <tr>
                                                                        <th>Items</th>
                                                                        <th>Qty</th> 
																		<th>Price</th>
                                                                    </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <b>Prescription Items</b><br>
                                                                            
                                                                        </td></tr>';
																		for($i=0;$i<$prescriptionitems->count();$i++)
						{
							$body_message1 .='<tr>
																		<td>'.$prescriptionitems[$i]['drugname'].'</td>
                                                                        <td>'.$prescriptionitems[$i]['drugpack'].'</td>
																		<td>'.number_format((float)($prescriptionitems[$i]['drugpack']*$prescriptionitems[$i]['originalprice']), 2, '.', '').'</td>
                                                                        
                                                                    </tr>';
						}
						$body_message1 .='<tr>
                                                                        <td colspan="2"><b>Non-Prescription Items</b></td>
                                                                        </tr>';
																		for($j=0;$j<$nonprescriptionitems->count();$j++){
																	$body_message1 .='<tr>
																		<td>'.$nonprescriptionitems[$j]['drugname'].'</td>
                                                                        <td>'.$nonprescriptionitems[$j]['drugpack'].'</td>
                                                                        <td>'.number_format((float)($nonprescriptionitems[$j]['drugpack']*$nonprescriptionitems[$j]['originalprice']), 2, '.', '').'</td>
                                                                    </tr>';
						}
                                                             $body_message1 .='   </tbody>
  
                                                            </table>
                                                        </td>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                        <td height="40" style="height:40px;">&nbsp;</td>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Subtotal</td>
                                                        <td>'.$subtotal.'</td>
                                                    </tr>';
                                                    if($orderData[0]['ordertype'] == "Delivery")
                                                    {
                                                    $body_message1 .=   '<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Delivery&nbsp;Fee</td>
                                                        <td>'.$orderData[0]['orderdeliveryfee'].'</td>
                                                    </tr>';
                                                    }
                                                    $body_message1 .='<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Order&nbsp;Total</td> 
                                                        <td>'.$orderData[0]['orderTotal'].'</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>GST</td>
                                                        <td>'.$gst.'</td>
                                                    </tr> 
                                                </table>
												
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="30%">&nbsp;</td>
                                                        <td><a href="https://pharmacy.medmate.com.au/rejectorder/'.$orderData[0]->orderid.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/reject-order-btn.png">
                                                </a></td><td width="20" style="width:20px;padding-left:60px;">
                                                <a href="'.$link.'" target="_blank">
												<img src="https://pharmacy.medmate.com.au/accept-order-btn.png">
                                                    
                                                </a></td>
                                                        <td width="30%">&nbsp;</td>
                                                    </tr>
                                                   </table>



                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4> Customer Details:</h4>
                                                            '.$user_details[0]->firstname.'<br>
															Accept PO to view additional details. </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$locationname[0]->locationname.'<br>
							'.$locationname[0]->phone.'<br>
							'.$locationname[0]->address.'<br>
							'.$locationname[0]->suburb.'</td>                                                        
                                                    </tr>
                                                </table>
												<br>
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable">
												<tr><td colspan="3"></td></tr>
												<tr>
                                            <td colspan=3 valign="top">1. Medmate has collected payment on behalf of the customer. ​</td></tr>
											<tr><td colspan=3 valign="top">2. A Medmate customer service representative will contact you for confirmation and payment of order, and provide you access to the Purchase Order. Please call (03) 9894 7831 for any queries.​</td></tr>
											<tr><td colspan=3 valign="top">3. The Pharmacy warrants that:​<br>
											<ul>
											<li>it has the authority and capacity to enter into and fulfil its obligations in accordance with this Purchase Order;​</li>
											<li>it is, and will remain, a licensed Pharmacy; and​</li>
											<li>it has, and will maintain, all necessary permits, licences and registrations to operate as a Pharmacy.​</li>
											<li>By accepting this Purchase Order you are accepting all Pharmacy Obligations and Warranties as outlined in <a href="https://medmate.com.au/terms-and-conditions/">Terms and Conditions</a> ​</li>
											</ul></td></tr>
											</table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faqs/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2021 Medmate Australia Pty Ltd​
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>';
			}elseif($orderData[0]->type==2){
				$body_message1='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new in store order.​</h1>  
                                                <p>Hello  &nbsp;&nbsp;'.$locationname[0]['locationname'].',<br>
                                                A new order has been confirmed and sent  to your pharmacy from a local customer for in store payment. The customer will pay for the order during pick up. ​
</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Order Details</h3>

                                                <p>'.$orderData[0]['ordertype'].'<br>
                                                '.$deliverydate.', '.$orderData[0]['userprefferedtime'].'</p>

                                                
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="30%">&nbsp;</td>
                                                        <td><a href="'.$link.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/view-order-btn.png" alt="View Order" />
                                                </a></td>
                                                        <td width="30%">&nbsp;</td>
                                                    </tr>
                                                   </table>



                                                                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2021 Medmate Australia Pty Ltd​
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>';
			}
$appendtext = "";
if(env('ENV') == "")
{
	$userfullname = $orderData[0]['fname']." ".$orderData[0]['lname'];
	$strVal = stripos($userfullname,"test");
	if($strVal!="")
	{
			$appendtext = $userfullname;
	}
	else{
			$appendtext = "";
			
	}
}
			   
			    $message1['to']           = $orderData[0]['email'];
			  //  $message1['to']           = $orderData[0]['email'];
               //$message1['subject']      = "Medmate – Order #".$uniqueorderid." has been confirmed";
               $message1['subject']      = $appendtext." ".env('ENV')." Medmate – Order #".$uniqueorderid." has been confirmed";
               $message1['body_message'] = $body_message1;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message1, function ($m1) use ($message1)  {
               $m1->to($message1['to'])
                  ->subject($message1['subject'])
                  //->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
				  ->bcc('orders@medmate.com.au','Medmate')
               ->setBody($message1['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
 } 
 
 
public function compute_price($location,$drug,$profile){
$price="NA";
$gst=0;
$business = Locations :: where('locationid', $location)->get();
// $price_details = DB::table('catalouge_master as C')
						// ->select('CP.*','C.ShortDescription','C.GST','C.ItemCommodityClass')
						// ->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
						// ->where([
							// ['C.MedmateItemCode',$drug],
							// ['CP.location_id',$location]
							// ])
                        // ->get();
$price_details = DB::table('catalouge_master as C')
						->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST','C.ItemCommodityClass')
						->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
						// ->where([
							// ['C.itemNameDisplay',$drug],
							// ])
						->where([ //['CO.location_id',$location],
									['CO.businessid',0],
									['CO.Pricing_Tier',1],
									['C.itemNameDisplay','LIKE','"%'.$drug.'%"'],
									//['CO.businessid',$business[0]['businessid']],
							])
						->orwhere([ //['CS.location_id',$location],
									['CS.businessid',0],
									['CS.Pricing_Tier',1],
									['C.itemNameDisplay','LIKE','"%'.$drug.'%"'],	
									//['CS.businessid',$business[0]['businessid']],
							])
                        ->get();                        
                        $healthprofile = DB::table('healthprofile')->where('userid',$profile)->get();
						if(count($price_details)>0){
						if($price_details[0]->ItemCommodityClass=='OTC')
						{
							$p=$price_details[0]->OTCStandardprice;
							$gst =(($price_details[0]->GST)*$p)/100;
							$price =$p+ $gst;
							
						}
						else
						{
							if(count($healthprofile)>0){
							
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                    $p=$price_details[0]->Con_Pens_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								if($healthprofile[0]->veteranaffairsnumber !=null){
                                    $p=$price_details[0]->Veterans;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->ScriptStandardprice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }

                            }
							
						}
                        }
						else{
                            $price="NA";
                        }
                        return round($gst,2);
                        

    }  
}