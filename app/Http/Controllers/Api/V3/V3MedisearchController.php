<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Prescriptionitemdetails; 
use App\models\Healthprofile; 
use App\models\Locations; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class V3MedisearchController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/medisearch",
    *      tags={"medisearch"},
    *      summary="medisearch",
    *      operationId="medisearch",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="medication",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*    @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	 @OA\Parameter(
    *          name="userid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function medisearch(Request $request)
	{
        $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($request->userid);
        $healthprofile = Healthprofile::where('userid', $request->userid)->get();
		
        $business = Locations :: where('locationid',$request->locationid)->get();
		
	    $catalouge_data = DB::select("select CO.StandardPrice as OTCStandardprice,CS.medicare,CS.Con_Pens_ConSafty,CS.Veterans,CS.SafetyNet,CS.StandardPrice as ScriptStandardprice,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C LEFT JOIN catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode LEFT JOIN cataloguescriptpricing as CS ON C.MedmateItemCode = CS.medmateItemcode where (CS.location_id = ".$request->locationid." AND CS.Pricing_Tier=".$business[0]['ScriptPricingTier']." AND C.itemNameDisplay LIKE '%".$request->medication."%' and C.ItemStatus=1)  OR (CO.location_id = ".$request->locationid." AND CO.Pricing_Tier=".$business[0]['OTCPricingTier']."  AND C.itemNameDisplay LIKE '%".$request->medication."%' and C.ItemStatus=1) "); 
	    if(count($catalouge_data)==0)
		{
		$catalouge_data = DB::select("select CO.StandardPrice as OTCStandardprice,CS.medicare,CS.Con_Pens_ConSafty,CS.Veterans,CS.SafetyNet,CS.StandardPrice as ScriptStandardprice,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C LEFT JOIN catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode LEFT JOIN cataloguescriptpricing as CS ON C.MedmateItemCode = CS.medmateItemcode where (CS.businessid=".$business[0]['businessid']." AND CS.Pricing_Tier=".$business[0]['ScriptPricingTier']." AND C.itemNameDisplay LIKE '%".$request->medication."%' and C.ItemStatus=1) OR (CO.businessid=".$business[0]['businessid']." AND CO.Pricing_Tier=".$business[0]['OTCPricingTier']."  AND C.itemNameDisplay LIKE '%".$request->medication."%' and C.ItemStatus=1) "); 
		}
		elseif(count($catalouge_data)==0){
	    $catalouge_data = DB::select("select CO.StandardPrice as OTCStandardprice,CS.medicare,CS.Con_Pens_ConSafty,CS.Veterans,CS.SafetyNet,CS.StandardPrice as ScriptStandardprice,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C LEFT JOIN catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode LEFT JOIN cataloguescriptpricing as CS ON C.MedmateItemCode = CS.medmateItemcode where (CS.businessid=0 AND CS.Pricing_Tier=".$business[0]['ScriptPricingTier']." AND C.itemNameDisplay LIKE '%".$request->medication."%' and C.ItemStatus=1) OR (CO.businessid=0 AND CO.Pricing_Tier=".$business[0]['OTCPricingTier']." AND C.itemNameDisplay LIKE '%".$request->medication."%' and C.ItemStatus=1)"); 
		}
       
	    $mims_data = DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode` left join packitemimages as PI on P.prodcode=PI.prodcode where P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode and `P`.`ActualProduct` LIKE '%".$request->medication."%'");
        $drug_details=array();
        
        for($i=0;$i<count($catalouge_data);$i++){
            $catalouge_data[$i]->reference="Catalouge";
			if(count($healthprofile)>0){
					if($catalouge_data[$i]->script == "OTC")
					{
						//echo "OTC";exit;
						 $p=$catalouge_data[$i]->OTCStandardprice;
						 $gst =(($catalouge_data[$i]->GST)*$p)/100;
						 $price =$p+ $gst;
					}
					else
					{						
                                if($healthprofile[0]->safetyNet!=null){
									//echo "123";exit;
                                    $p=$catalouge_data[$i]->SafetyNet;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                else if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                   // echo "222";exit;
									$p=$catalouge_data[$i]->Con_Pens_ConSafty;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								else if($healthprofile[0]->veteranaffairsnumber){
                                   // echo "222";exit;
									$p=$catalouge_data[$i]->Veterans;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                else if($healthprofile[0]->medicarenumber!=null){
                                  // echo "666";exit;
								   $p=$catalouge_data[$i]->medicare;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                 //  echo "678";exit;
								   $p=$catalouge_data[$i]->ScriptStandardprice;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
					}
                    }
           
			$catalouge_data[$i]->price =number_format(($price), 2, '.', '');			
          
            if($catalouge_data[$i]->script=='Prescription'){$catalouge_data[$i]->script="Yes";}else{$catalouge_data[$i]->script="No";}
            $drug_details[]=$catalouge_data[$i];
        }
        for($i=0;$i<count($mims_data);$i++){
           
            $mims_data[$i]->reference="MIMS";
            $mims_data[$i]->ShortDescription=$mims_data[$i]->drug;
            $mims_data[$i]->price="";
            $drug_details[]=$mims_data[$i];
        }
        if(!empty($drug_details)){
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.succ_message');
        }else{
            $response->drug_details 	= $drug_details;
            $response->msg 		= trans('messages.no_data');  
        }
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        }
   }
