<?php

namespace App\Http\Controllers\api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User; 
use App\models\Prescriptiondetails; 
use App\models\Prescriptionitemdetails; 
use App\models\Tokenswallet; 
use App\models\Healthprofile; 
use App\models\Locations; 
use App\models\Miuser; 
use App\models\Milog;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Support\Facades\Crypt;
use phpseclib\Crypt\RSA as Crypt_RSA;
use Firebase\JWT\JWT;
use Mail;
class V3ScriptsController extends Controller
{
    public $successStatus = 200;
	
	/**
      @OA\Post(
          path="/v3/addPrescriptions",
          tags={"Scripts"},
          summary="Add Prescriptions",
          operationId="AddPrescriptions",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="prescriptionuserid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="prescriptiontype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptiontitle",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptionimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="prescriptionbackimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="barcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugstrength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="itemschedule",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugquantity",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="repeats",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugdata",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="prescriptionfor",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="medilistid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 
	public function addPrescriptions(Request $request)
	{
	
		$response = (object)array();
		$user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
		 $healthprofile = Healthprofile::where('userid', $user->userid)->where('parentuser_flag' , '1')->get();
		 $prescription = new Prescriptiondetails;
		 $prescription->userid = $user->userid;
		 $prescription->prescriptiontype = $request->prescriptiontype;
		 $prescription->prescriptiontitle = $request->prescriptiontitle;
		  if($request->prescriptiontype == "1" && $request->barcode !="")
			 {
				 if(strtolower(substr($request->barcode,0,5))=='https')
				 {
					 $code = explode("/",$request->barcode);
					 if($code[2] == "ausscripts-int.medisecure.com.au")
					{					
						$barcode = $code[4];
					}
					else
					{					
						
					if($code[3] == "scripts")
						$barcode = $code[4];
					else
						$barcode = $code[3];
					}
				 }
				 else
				 {
					 $barcode =$request->barcode;
				 }
				
				//$code = explode("/",$request->barcode);
				//https://ausscripts-int.medisecure.com.au/scripts/MPK00009002000607
				//https://egw-etp-int-qrcode-web-au-se.azurewebsites.net/21KR32CTBBKDTY17B1
				
										
			 }
			 else 
			 {
				$barcode = $request->barcode;
			 }
			$barcode=strtoupper($barcode);
				 if((($request->prescriptiontype == "3") && ($request->prescriptionimage !="") || 
					  ($request->prescriptiontype == "1" || $request->prescriptiontype =="2") && ($request->barcode !="")))
				 {
					
					 
					 if($request->prescriptiontype != "1" && $request->prescriptionimage !="")
					 {
					 $prescriptionfrontimage=$this->base64_to_jpeg($request->prescriptionimage,time().$user->userid."front.jpeg");
					 }
					 else
					 {
						 $prescriptionfrontimage = $request->prescriptionimage;
					 }
					 if($request->prescriptionbackimage != '')
					 {
					 $prescriptionbackimage= $this->base64_to_jpeg($request->prescriptionbackimage,time().$user->userid."back.jpeg");
					 }
					 else
					 {
						 $prescriptionbackimage = '';
					 }
					 if($request->prescriptiontype == "1" || $request->prescriptiontype == "2")
					 {
                        $conformance=DB::table('conformance')->where('id',1)->where('status',1)->get();
                        if(count($conformance)>0){
                       $miuser=Miuser::where('application_id',1)->where('external_user_id',$user->userid)->get();
                       if(count($miuser)>0){
							   //if(($miuser[0]->device_id==$request->device_id || (isset($request->type)&& $request->type==2))  && $miuser[0]->external_user_id=$user->userid){
							   if($miuser[0]->device_id==$request->device_id  && $miuser[0]->external_user_id=$user->userid){
                               if($miuser[0]->status==1 && $user->mi_accept==1){
                               $drug_details=$this->subjecttoken($user->userid,$request->device_id,$barcode);
                        if(is_array($drug_details)){
                           $patientname=$drug_details['patientname'];
                           $drugnameVal =  trim($drug_details['drugname']);
                           $drugform =  trim($drug_details['format']);
                           $drugstrength =  trim($drug_details['strength']);
                           $drugpack = '1';		
                           if($drug_details['source']=='ms'){$drug_details['source']='Medisecure';}
                           if($drug_details['source']=='erx'){$drug_details['source']='ERX';}
                           $prescriptionissuer = $drug_details['source'];							
                           $drugquantity = trim($drug_details['quantity']);
                           $repeats = trim($drug_details['repeats']);	
                           $dosage_instructions=$drug_details['dosage_instructions'];
                           if($prescriptionissuer=='ERX'){
                               $script_url=env('MK_SCRIPT_URL_ERX').$barcode;
                           }else{
                               $script_url=env('MK_SCRIPT_URL_MEDISECURE').$barcode;
                           }						
                        }else{
                            $response->msg 		= $drug_details;
                            $response->status 		= $this->successStatus;
                            return json_encode($response); 
                        }
                               }else{
                                   $response->msg="You have not authorised Medmate to access your e-Scripts. Would you like to authorise Medmate to access your e-Scripts?";
                           $response->status=401;
                           return json_encode($response);
                               }									
                       }else{
                              $response->msg="You Have logged in from different Device . Would you continue to access your e-Scripts from this device?";
                              $response->status=401;
                              return json_encode($response);
                           }
              
                       }else{
                           $response->msg="You have not authorised Medmate to access your e-Scripts. Would you like to authorise Medmate to access your e-Scripts?";
                           $response->status=401;
                           return json_encode($response);
                        }}else{
                            $response->msg="Medmate is unable to reach the Mobile e-Prescription Exchange Service. Please try again later";
                           $response->status=401;
                           return json_encode($response);
                        }
						/*
						$prescimg = "https://ausscripts-int.medisecure.com.au/scripts/".$barcode;
						$html = HtmlDomParser::file_get_html($prescimg);
						if(!empty($html->find('h5')))
						{
							//Toget Patient Name
							foreach($html->find('h5') as $pname)
							$patientname = str_replace("Electronic Prescription for ","",strip_tags($pname));
							
							$d = 0;
							$img = array($d => $html->find('img'));
							//Toget QRCode and Barcode
							 foreach($img as $ele => $element){						     
								$medicationdetails['qrcode'] = $element[0]->src; 
								$d++;
							}
						
						$prescriptionfrontimage=$this->qrcodeimage($medicationdetails['qrcode'],time().$user->userid."qrcode.jpeg");
						
						//Drug Details
						$i = 0;
						$val=array($i => $html->find('h3')); 
						foreach($val as  $d  => $drugname)
						{
							
							$drugdetails = explode('-',strip_tags($drugname[$i]));
							$drugnameVal =  trim($drugdetails[0]);
							// $drugform =  trim($drugdetails[1]);
							// $drugstrength =  trim($drugdetails[2]);	
							 $drugform =  '';
							 $drugstrength =  '';
							$quantity=  trim($drugdetails[1]);
							//$repeats =  "0";//str_replace("Supplies Remaining","",trim($drugdetails[4]));
							$prescriptionissuer = "Medisecure";
							if($quantity == "")
							{
								$drugquantity = 0;
								$drugpack = '';
							}
							else
							{
								
								$drugqua = explode('*',strip_tags($quantity));
								if(count($drugqua) == 2)
								{
									
									$qua = trim($drugqua[0]);
									$drugquantity = trim(str_replace("Qty ","",trim($drugqua[0])));									
									$drugpack = $drugqua[1];
								}
								else{
									$drugquantity = $drugqua[0];
									$drugpack = '1';
								}
								}
							
							$i++;
							
							foreach($html->find('h6') as $rpts)
							$repeats = str_replace("Supplies Remaining","",strip_tags($rpts));
						}
						}
						else
						{
							$prescimg = "https://ausscripts.erx.com.au/scripts/".$barcode;
							//$prescimg="https://egw-etp-int-qrcode-web-au-se.azurewebsites.net/".$barcode;
							
							$html = HtmlDomParser::file_get_html($prescimg);
							 
							//Toget Patient Name
							foreach($html->find('h5') as $pname)
							$patientname = str_replace("Electronic Prescription for ","",strip_tags($pname));
							//print_r($patientname);exit;
							$d = 0;
							$img = array($d => $html->find('img'));
							//Toget QRCode and Barcode
							 foreach($img as $ele => $element){	
								$medicationdetails['qrcode'] = $element[0]->src; 
								$d++;
							}
							$prescriptionfrontimage=$this->qrcodeimage($medicationdetails['qrcode'],time().$user->userid."qrcode.jpeg");
							
							//Drug Details
						$i = 0;
						$val=array($i => $html->find('h3')); 
						foreach($val as  $d  => $drugname)
						{
							
							$drugdetails = explode(' ',strip_tags($drugname[$i]));
							$drugnameVal =  trim($drugdetails[0]);
							$drugform =  trim($drugdetails[1]);
							$drugstrength =  trim($drugdetails[2]);
							$drugpack = '1';							
							$prescriptionissuer = "ERX";							
							$drugquantity = trim($drugdetails[3]);
							$i++;
							foreach($html->find('h6') as $rpts)
							//$strval = strcmp()
							$repeats = str_replace("Supplies Remaining","",strip_tags($rpts));
							//print_r($repeats[0]);exit;
							$repeats = $repeats[0];
							
						}
						}
						// if($request->medilistid !="")
						// {
							// $getmedicationname = Prescriptionitemdetails :: where('prescriptionitemid','=',$request->medilistid)->get();
							// $medlistmedication = $getmedicationname[0]['drugname'];
							// if($medlistmedication == $drugnameVal)
							// {		
								
							// }
							// else
							// {
								
								 // $response->msg 		= "Medication name is different in the added script. Please check and upload again";
								 // $response->status 		= $this->successStatus;
								 // return json_encode($response);
							// }
                        // }
                        */
					 }
					 else
					 {
							$drugnameVal =  "Paper Script";
							$drugform =  "";
							$drugstrength =  "";
							$drugquantity=  "0";
							$repeats =  0;
							$drugpack = '';
							$prescriptionissuer = "Medisecure";
							$patientname = "";
							$script_url = "";
							$dosage_instructions= "";
					 }
					
					 $prescription->prescriptionimage = $prescriptionfrontimage;
					 $prescription->prescriptionbackimage = $prescriptionbackimage;
					 $prescription->prescriptionstatus = '1';
					 $prescription->barcode = $barcode;
					 $prescriptionid = "";
					if($request->prescriptiontype == 1)
					{						
					
					$prescexist = DB :: select("select * from prescriptiondetails as p left join prescriptionitemdetails as pi on p.prescriptionid = pi.prescriptionid where p.userid=".$user->userid." and p.barcode= '".$barcode."' and  p.prescriptiontype=1 and pi.ismedlist=1 and pi.prescriptionuserid='".$user->userid."' and pi.drugname like '%".$drugnameVal."%'");
					$countprescexist = count($prescexist);
					if(count($prescexist)== 0){						
					  $prescription->save();
						$prescriptionid = $prescription->id;					  
					}
					 else
					{								
						$scriptsInfo = Tokenswallet :: where('userid',$user->userid)->where('prescriptionid',\DB::raw('"'.$prescexist[0]->prescriptionid.'"'))->get();	
						
						$prescriptionItems = Prescriptionitemdetails :: where('prescriptionid',\DB::raw('"'.$prescexist[0]->prescriptionid.'"'))->where('prescriptionuserid',$user->userid)->get();
						//$scriptInfo[0]->prescriptiontype=1;
						$response->scriptsInfo 	= $scriptsInfo[0];
						$prescriptionItems[0]->drugname=$drugnameVal;
						$prescriptionItems[0]->drugstrength=$drugstrength;
						$prescriptionItems[0]->drugform=$drugform;
						$prescriptionItems[0]->drugquantity=$drugquantity;
						$response->prescriptionItems 	=  $prescriptionItems[0];
						$response->msg 		= "This prescription is already available in your Scripts. Please upload from MyScripts";
					}
					}
					elseif($request->prescriptiontype == 3)
					{
						$prescription->save();	
						$prescriptionid = $prescription->id;						
					}
					 
					 
					 $drugimagedetails = $this->getcmiimages($drugnameVal);
					 
					 if(!empty($drugimagedetails))
					 {
					 foreach ($drugimagedetails as $imagedetails) {
						$medimage   = (!empty($imagedetails->PhotoID)) ? $imagedetails->PhotoID : '';
						$mimsimage   = (!empty($imagedetails->image)) ? $imagedetails->image : '';
						$cmicode   = (!empty($imagedetails->cmi)) ? $imagedetails->cmi : '';
						
						//print_r($imagedetails);exit;
					 }
					 }
					 else{
						 $medimage   = '';
						$mimsimage   = '';
						$cmicode   = '';
					 }
					
					 if($request->itemschedule !="")
						 $itemschedule = $request->itemschedule;
					 else
						 $itemschedule = "S";
					 if($prescriptionid!="")
					 {
						 $prescriptionItems = new Prescriptionitemdetails;
						 $prescriptionItems->prescriptionid = $prescriptionid;
						 $prescriptionItems->prescriptionuserid = $request->prescriptionuserid;
						 $prescriptionItems->profileid = $request->profileid;
						 $prescriptionItems->drugname = $drugnameVal;
						 $prescriptionItems->drugform = $drugform;
						 $prescriptionItems->drugstrength = $drugstrength;
						 $prescriptionItems->drugquantity = $drugquantity;
                         $prescriptionItems->itemschedule = $itemschedule;
                         $prescriptionItems->script_url = $script_url;
                         $prescriptionItems->dosage_instructions=$dosage_instructions;
						 $prescriptionItems->drugpack = $drugpack;
						 $prescriptionItems->repeats = $repeats;
						 $prescriptionItems->referrencetable = "";
						 $prescriptionItems->drugdata = "Medmate";
						 $prescriptionItems->prescriptionfor = $patientname;
						 $prescriptionItems->prescriptionissuer = $prescriptionissuer;
						 
						
						if($request->medilistid !="")
						{
							$prescriptionItems->medilistid = $request->medilistid;
							
							$prescriptionItems->ismedlist = "1";
							$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', \DB::raw('"'.$request->medilistid.'"'))
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.ismedlist' => 0]);
															
							
						}
						else
						{
							$prescriptionItems->medilistid ="";
							if($request->prescriptiontype == "1" || $request->prescriptiontype == "2")
							{
							 $prescriptionItems->ismedlist = "0";
							}
							else{
								$prescriptionItems->ismedlist = "1";
							}
						}
						$prescriptionItems->medstatus = "medlist";
						$prescriptionItems->medimage = $medimage;
						$prescriptionItems->mimsimage = $mimsimage;						
						$prescriptionItems->isscript = '1';
						$prescriptionItems->cmicode = $cmicode;
						
						 $prescriptionItems->save();
						 
						 $scriptsInfo = new Tokenswallet;
						 $scriptsInfo->userid = $user->userid;
						 $scriptsInfo->createdby = $user->userid;
						 $scriptsInfo->tokenimage = $prescriptionfrontimage;
						 $scriptsInfo->tokenbackimage = $prescriptionbackimage;
						 $scriptsInfo->prescriptionid = $prescriptionid;
						 $scriptsInfo->isused = 0;
						 $scriptsInfo->repeats = 0;
						 $scriptsInfo->tokenstatus = 1;
						 $scriptsInfo->save();
						 $scriptsInfo->prescriptiontype = $request->prescriptiontype;
						 $scriptsInfo->prescriptiontitle = $request->prescriptiontitle;
						 
						 $scriptsInfo->barcode = $barcode;
						
						if($request->prescriptiontype == 1)
						{
							//echo $countprescexist;exit;
							
							if($countprescexist == 0)
							{
								$response->scriptsInfo 	= $scriptsInfo;
								$response->prescriptionItems =  $prescriptionItems;
								$response->msg 		= trans('messages.prescription_Add');
							}
							
						}
						elseif($request->prescriptiontype == 3)
						{
							$response->scriptsInfo 	= $scriptsInfo;
							$response->prescriptionItems =  $prescriptionItems;
							$response->msg 		= trans('messages.prescription_Add');
						}
						
							//$response->msg 		= trans('messages.prescription_Add');
							$response->status 		= $this->successStatus;					 
					 }
				 }
				 else
				 {
					$response->msg 		= trans('messages.prescription_err');
					$response->status 		= $this->successStatus;
				 }
			 
		return json_encode($response); 		 
		 
	}
    public function subjecttoken($user_id,$device_id,$qrcode)
	{
		try{
		$user_details=DB::table('users')->where('userid',$user_id)->get();
		$qr=explode("/",$qrcode);
		if(count($qr)>1){
			$qrcode=end($qr);
		}else{
			$qrcode=$qrcode;
		}
		$rsa = new Crypt_RSA();
		$key="medmate";
		//<RSAKeyValue><Modulus></D></RSAKeyValue>
		//$publickey="<RSAKeyValue><Modulus>xUcmE8gzFIgD7OLnojv0IuclG/au31DJC6l6HwuZpToHfEZbwC9N72B2rebVXfOgY3IETXjPx2jmJgVlUKNrhICLGn79LmnygXW51evp9tdVQrMCjfRdq+1y0+elZJHCbC9NACY9aE6ZcQKjCuSHx8cZKlhQQbBYrWq/Ao3AvyAo0ZYWaC3FJg2RUKfC+M2AydUPOGezTrwV9hGKmh8vrvHNLXMKhi/HKb68UoXyirG4WxqbeuESQK1vs5paohCMMog+DT9gLEIaqhkoyH09dYBKgWj8t7QHIlIsLtdLPwKLMO8wd0u3mOcaH1qsVBWfgNrSPNV3kZ01kI1FhNno+Q==</Modulus><Exponent>AQAB</Exponent><P>01VsRBWK4DpBYHId0r7/PuSr73Mo3KKdFov2iE7QP0vmCcxf3Ou8fwT4o+5nUcaFGqNBrCl62uGZ0ROS1uFNnGRQjiuouFhx4kPL1JlEkJCgYV+Y8BaGZNNpSLmuRsX6Ls4zVgwBET5eDhK1GypDynLtXMAuB9qCcDCiez2Jy8c=</P><Q>7vk26zKCUuKWAAJk17fZY9bPOtmFeccDVAxUgvVV7cVZVBnuY2+ZQU2Jz5S8NQvUNaPJI1EfNYEK5E5sK9ZJ0h6O6iz5qOzC+2cCt9YGWNy1ReKTakom/45ovcl9J8xZbuh+adN5u9Y7/KeIJPXYgTcsbiPX0P/chmvy6UybJT8=</Q><DP>yIFclgAmYfWDf6DlwMSr4R0OL8H/+81g4zpp7gtwsw0stREtxqn2jkDGMqqHZmPGd257aX91a2PuDxrWD391pjLa5vhvPG/VpQoTwqKkFdDSCXCs5uVIHzMhyfk5azZ81pmWxUXgYV7d19ZR8/MWnGaL45sGUTc02IKcNfc93JM=</DP><DQ>vYC+/i/ljNjF3mgHk9DlAPkdCCvqXrFkgEfu1JK8e3rd5YVSt2OOAmz8dbIatW4V8BtbqzfvKbyU6IsP2ItrqsY0ypqzuDNYQ4hibWm5SspqrWqJP+ZhmpJRmP6o0uCHox2W8VCT3lJ661Xm7nhOIdmg0A54W/Ip7Ca6UCtBdH0=</DQ><InverseQ>sG+GWdQ847hCUiTTDHNsAxq+iu/LPuvnfcARLWdlL4Ov0Naw/rwIOeDXIe+5qHCubZr5Hoc5wwn/4iNrKZTdfFc1R5AVWexQ2Mxs0sSdqhIUQ7qxOu+cVjcD9uEpHa5oN4oSqExsE3cmepOYIX5LUuxczs3Ex+ORnSg31tJgbpU=</InverseQ><D>p92Q4whZnsDWRj4VyBCn/wMcqpzP1KpTSJ8fkUc3qEamk/Lyo6gc8vHsSrG/IUDrW3NrgdipRQ9Xt0akHWfRV2Bh/VvNaC8y8UIRrZwe67HzlNvp8ozbIC7epL3aGKeG/rOJG/VuD1HCc01BG0W23CHoogWf1SWPb5EDk9K3Dml55Td3v/i3CyuXi9vs7LhGM4P+rS5/g1x2B0eelrNarIFm+uytMqVvaLqdYgpniud/OILwndPOHPbkRAihWJ1Sy2QbR0LIDCdRnhII1EVlB8XwPyEDKnY1CwSACYImNrJq1LdvFV/qDkXcMbhFmDwyQOzjqYiRj29h9gsY33wZ+Q==</D></RSAKeyValue>";
		$publickey=env('MK_PUBLIC_KEY');
		$rsa = new Crypt_RSA();
         //$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
	
         $privateKey=$rsa->loadKey($publickey);
		$rsa->setEncryptionMode(Crypt_RSA::ENCRYPTION_PKCS1);	//private key in xml
   		$date=date('Y-m-d H:i:s');
		//$exp=strtotime(date('Y-m-d H:i:s', strtotime('+1 day', $date)));
		$exp=strtotime("+ 1 day");
		$newTime = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +1 minutes"));
		$oTime= date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -1 minutes"));
		$payload = array(
		"sub"=>"Medmate",
		"name"=>"medmate",
		"iss" => "MedMate",
		"aud" => env('MK_AUD'),
		"exp" => $exp,
		"iat" => $newTime,
		"nbf" => $oTime
		);
$jwt = JWT::encode($payload, $rsa, "RS256");
//$auth_token="Bearer ".$jwt;curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//print_r($auth_token);exit;
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => env('MK_AUTH_URL'),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_SSL_VERIFYPEER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Atoken-exchange&client_id=".env('MK_AUTH_CLIENT_ID')."&client_secret=".env('MK_AUTH_CLIENT_SECRET')."&scope=patient/MedicationRequest.read&subject_token=".$jwt."&subject_token_type=urn%3Aietf%3Aparams%3Aoauth%3Atoken-type%3Ajwt",
  CURLOPT_HTTPHEADER => array(
    "Ocp-Apim-Subscription-Key: ".env('MK_SUBSCRIPTION_KEY'),
   // "Authorization: ".$auth_token
  ),
));
$response = curl_exec($curl);
curl_close($curl);
$data=json_decode($response);
if(isset($data->access_token)){
$access_token="Bearer ".$data->access_token;
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => env('MK_URL')."mkfacade/MedicationRequest?identifier=".$qrcode,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_SSL_VERIFYPEER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Ocp-Apim-Subscription-Key: ".env('MK_SUBSCRIPTION_KEY'),
    "Authorization: ".$access_token
  ),
));
$response = curl_exec($curl);
curl_close($curl);
$xml = simplexml_load_string($response);
$json = json_encode($xml);
$arr = json_decode($json,true);
if(isset($arr['entry'])){
	if(isset($arr['entry']['resource']['MedicationRequest'])){
$strength='';
$format=$arr['entry']['resource']['MedicationRequest']['contained'][1]['Medication']['form']['text']['@attributes']['value'];
foreach($arr['entry']['resource']['MedicationRequest']['extension'] as $key=>$val)
			{		
				$url = explode('/',$arr['entry']['resource']['MedicationRequest']['extension'][$key]['@attributes'] ['url']);
				$end = end(($url));
				if(strtolower($end) == "quantity")
				{
					$quantity = $arr['entry']['resource']['MedicationRequest']['extension'][$key]['valueString']['@attributes']['value'];			
				}
				$url1=explode('/',$arr['entry']['resource']['MedicationRequest']['extension'][$key]['@attributes'] ['url']);
				$end1=end($url);
				if(strtolower($end1) == "medication-brand-name")
				{
					$drugname=$arr['entry']['resource']['MedicationRequest']['extension'][$key]['valueString']['@attributes']['value'];
				}
			}
if(isset($arr['entry']['resource']['MedicationRequest']['contained'][1]['Medication']['extension'])){
	$url=explode('/',$arr['entry']['resource']['MedicationRequest']['contained'][1]['Medication']['extension']['@attributes'] ['url']);
	$end = end(($url));
				if(strtolower($end) == "medication-strength")
				{
					$strength = $arr['entry']['resource']['MedicationRequest']['contained'][1]['Medication']['extension']['valueString']['@attributes']['value'];			
				}
}	
$dosage_instructions="";
if(isset($arr['entry']['resource']['MedicationRequest']['dosageInstruction']['text']['@attributes']['value'])){
$dosage_instructions=$arr['entry']['resource']['MedicationRequest']['dosageInstruction']['text']['@attributes']['value'];
}
$repeats=$arr['entry']['resource']['MedicationRequest']['dispenseRequest']['numberOfRepeatsAllowed']['@attributes']['value'];
$drug_array=array();
$drug_array['patientname']=$arr['entry']['resource']['MedicationRequest']['contained'][0]['Patient']['name']['text']['@attributes']['value'];
$drug_array['format']=$format;
$drug_array['quantity']=$quantity;
$drug_array['drugname']=$drugname;
$drug_array['strength']=$strength;
$drug_array['repeats']=$repeats;
$drug_array['dosage_instructions']=$dosage_instructions;
$scidsource=$arr['entry']['resource']['MedicationRequest']['id']['@attributes']['value'];
$source=explode("-",$scidsource);
$drug_array['source']=$source[0];
if(isset($arr['entry']['resource']['MedicationRequest']['identifier']['value']['@attributes']['value'])){
$socid=$arr['entry']['resource']['MedicationRequest']['identifier']['value']['@attributes']['value'];}
else{
	$socid=$qrcode;
}
$input=array();
$log=new Milog();
$input['application_id']=1;
$input['application_name']='medmate';
$input['transactionid']=$arr['entry']['fullUrl']['@attributes']['value'];
$input['scidsource']=$arr['entry']['resource']['MedicationRequest']['id']['@attributes']['value'];
$input['scid']=$arr['entry']['resource']['MedicationRequest']['identifier']['value']['@attributes']['value'];
$input['socihiid']=$arr['entry']['resource']['MedicationRequest']['contained'][0]['Patient']['identifier']['value']['@attributes']['value'];
$input['external_user_id']=$user_id;
$input['mobile_device_token']=$device_id;
$input['response']=$response;
$input['medicationbrandname']=$drugname;
$input['quantity']=$quantity;
$input['strength']=$strength;
$input['repeats']=$repeats;
$input['form']=$format;
$input['dosageinstructions']=$arr['entry']['resource']['MedicationRequest']['dosageInstruction']['text']['@attributes']['value'];
$input['status']=$arr['entry']['resource']['MedicationRequest']['status']['@attributes']['value'];
$input['issuedate']=date('Y-m-d',strtotime($arr['entry']['resource']['MedicationRequest']['authoredOn']['@attributes']['value']));
$log->storeData($input);
//$log->save();
DB::table('mi_users')->where('external_user_id',$user_id)->where('device_id',$device_id)->update(array('socid'=>$socid));
return $drug_array;
	}else{
$input=array();
$log=new Milog();
$input['application_id']=1;
$input['application_name']='medmate';
$input['transactionid']="";
$input['scidsource']="";
$input['scid']=$qrcode;
$input['socihiid']="";
$input['external_user_id']=$user_id;
$input['mobile_device_token']=$device_id;
$input['response']=$response;
$input['medicationbrandname']="";
$input['quantity']="";
$input['strength']="";
$input['repeats']="";
$input['form']="";
$input['dosageinstructions']="";
$input['status']="Expired";
$log->storeData($input);
//$log->save();
$subject=env('ENV')." Medmate Script Error";
$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Below Script is not matching in catalogue.</p>
        <p>
		 <span style="color: #1d9bd8;"> User Details</span>   <br>	
			User Name: '.$user_details[0]->firstname.'<br>
			Email : '.$user_details[0]->username.' <br>
         <span style="color: #1d9bd8;"> Item Details</span>   <br>		  
		Item Name :  <br>
		strength  :  <br>
		form      :  <br>
		quantity  :  <br>
		repeats   :  <br>
		scriptID  : '.$qrcode.' <br>
		Error Response :'.$arr["entry"]["resource"]["OperationOutcome"]["issue"]["details"]["text"]["@attributes"]["value"].'<br><br>
        </p>
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijaytalluri88@gmail', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
            

		//return $arr['entry']['resource']['OperationOutcome']['issue']['details']['text']['@attributes']['value'];
		//return "This script is no longer available. Please check and upload again.";
		if(strpos($arr['entry']['resource']['OperationOutcome']['issue']['details']['text']['@attributes']['value'],"expired")==true){
		return "This prescription has expired or has been ceased.";
		}else{
		return "This prescription has already been used.";}
	}
//print_r($arr['entry']['resource']['MedicationRequest']['contained'][1]['Medication']['form']['text']['@attributes']['value']);exit;
//print_r($response);exit;
//echo "Encode:\n" . print_r($jwt, true) . "\n";
}else{
$input=array();
$log=new Milog();
$input['application_id']=1;
$input['application_name']='medmate';
$input['transactionid']="";
$input['scidsource']="";
$input['scid']="";
$input['socihiid']="";
$input['external_user_id']=$user_id;
$input['mobile_device_token']=$device_id;
$input['response']=$response;
$input['medicationbrandname']="";
$input['quantity']="";
$input['strength']="";
$input['repeats']="";
$input['form']="";
$input['dosageinstructions']="";
$input['status']="Aborted";
$log->storeData($input);
$subject=env('ENV')." Medmate Script Error";
$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Below Script is not matching in catalogue.</p>
        <p>
		 <span style="color: #1d9bd8;"> User Details</span>   <br>	
			User Name: '.$user_details[0]->firstname.'<br>
			Email : '.$user_details[0]->username.' <br>
         <span style="color: #1d9bd8;"> Item Details</span>   <br>		  
		Item Name :  <br>
		strength  :  <br>
		form      :  <br>
		quantity  :  <br>
		repeats   :  <br>
		scriptID  : '.$qrcode.' <br>
		Error Response : This Prescription has been ceased by the prescriber. The operation has been aborted.<br><br>
        </p>
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijaytalluri88@gmail.com', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }

//$log->save();
	return  "This prescription has expired or has been ceased.";
}
}else{
	$subject=env('ENV')." MK API ERROR";
$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Below Script is not matching in catalogue.</p>
        <p>
		 <span style="color: #1d9bd8;"> User Details</span>   <br>	
			User Name: '.$user_details[0]->firstname.'<br>
			Email : '.$user_details[0]->username.' <br>
         <span style="color: #1d9bd8;"> Item Details</span>   <br>		  
		Item Name :  <br>
		strength  :  <br>
		form      :  <br>
		quantity  :  <br>
		repeats   :  <br>
		scriptID  : '.$qrcode.' <br>
		Error Response :'.$data->message.'<br><br>
        </p>
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijaytalluri88@gmail', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   return  "The prescription service is not available. Please try again later";

}
		}catch(Exception $e) {
  $emessage=$e->getMessage();
  $subject=env('ENV')." Medmate Script Error";
$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Below Script is not matching in catalogue.</p>
        <p>
		 <span style="color: #1d9bd8;"> User Details</span>   <br>	
			User Name: '.$user_details[0]->firstname.'<br>
			Email : '.$user_details[0]->username.' <br>
         <span style="color: #1d9bd8;"> Item Details</span>   <br>		  
		Item Name :  <br>
		strength  :  <br>
		form      :  <br>
		quantity  :  <br>
		repeats   :  <br>
		scriptID  : '.$qrcode.' <br>
		Error Response : '.$emessage.'<br><br>
        </p>
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijaytalluri88@gmail.com', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
}
    }
    /**
      @OA\POST(
          path="/v3/updatemiaccess",
          tags={"Scripts"},
          summary="updatemiaccess",
          operationId="updatemiaccess",
          security={{"bearerAuth": {}} },
          @OA\Parameter(
              name="mi_access",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),   
           @OA\Parameter(
              name="device_id",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),   
      
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function updatemiaccess(Request $request)
    {
        $response = (object)array();
        $user = Auth::user();
        $miusers=Miuser::where('application_id',1)->where('external_user_id',$user->userid)->get();
        if(count($miusers)>0){
            if($request->type==2){
			DB::table('mi_users')->where('application_id',1)->where('external_user_id',$user->userid)->update(array('device_id'=>$request->device_id,'status'=>1));}else{
				DB::table('mi_users')->where('application_id',1)->where('device_id',$request->device_id)->where('external_user_id',$user->userid)->update(array('status'=>1));
			}
			DB::table('users')->where('userid',$user->userid)->update(array('mi_accept'=>1));
		}else{
                  $miuser=new Miuser();
                  $miuser->application_id=1;
                  $miuser->device_id=$request->device_id;
                  $miuser->external_user_id=$user->userid;
                  $miuser->first_name=$user->first_name;
                  $miuser->last_name=$user->last_name;
				  $miuser->dob=$user->dob;
				  $miuser->email=$user->username;
                  $miuser->createdby=1;
                  $miuser->status=1;
                  $miuser->password=$user->password;
                  $miuser->save();
        }
       $response->msg="Updated Success";
       $response->status 		= $this->successStatus;
       return json_encode($response); 
    }
	/**
      @OA\POST(
          path="/v3/deactivateuser",
          tags={"Scripts"},
          summary="deactivateuser",
          operationId="deactivateuser",
          security={{"bearerAuth": {}} },
          @OA\Parameter(
              name="mi_access",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),   
           
      
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function deactivateuser(Request $request)
    {
        $response = (object)array();
        $user = Auth::user();
        $miusers=Miuser::where('application_id',1)->where('external_user_id',$user->userid)->get();
        if(count($miusers)>0){
            DB::table('mi_users')->where('application_id',1)->where('external_user_id',$user->userid)->update(array('status'=>0));
			DB::table('users')->where('userid',$user->userid)->update(array('mi_accept'=>0));
		}
       $response->msg="User Deactivated Success";
       $response->status 		= $this->successStatus;
       return json_encode($response); 
    }
      /**
      @OA\POST(
          path="/v3/updatdeviceinfo",
          tags={"Scripts"},
          summary="updatdeviceinfo",
          operationId="updatdeviceinfo",
          security={{"bearerAuth": {}} },
          @OA\Parameter(
              name="mi_access",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),   
           @OA\Parameter(
              name="device_id",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),   
      
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function updatedeviceinfo(Request $request)
    {
        $response = (object)array();
        $user = Auth::user();
        $miusers=Miuser::where('application_id',1)->where('external_user_id',$user->userid)->get();
        DB::table('users')->where('userid',$user->userid)->update(array('device_id'=>$request->device_id));
        if(count($miusers)>0){
            DB::table('mi_users')->where('application_id',1)->where('external_user_id',$user->userid)->update(array('device_id'=>$request->device_id,'status'=>1));
        }else{
                  $miuser=new Miuser();
                  $miuser->application_id=1;
                  $miuser->device_id=$request->device_id;
                  $miuser->external_user_id=$user->userid;
                  $miuser->first_name=$user->first_name;
                  $miuser->last_name=$user->last_name;
				  $miuser->dob=$user->dob;
				  $miuser->email=$user->username;
                  $miuser->createdby=1;
                  $miuser->status=1;
                  $miuser->password=$user->password;
                  $miuser->save();
        }
       $response->msg="Updated Success";
       $response->status 		= $this->successStatus;
       return json_encode($response); 
    }
	
	/**
      @OA\Get(
          path="/v3/getPrescriptions",
          tags={"Scripts"},
          summary="Get Prescriptions",
          operationId="GetPrescriptions",
		  security={{"bearerAuth": {}} },
      
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function getPrescriptions()
	 {
		 
		 $user = Auth::user();
		/* $prescriptionDetails = Prescriptiondetails ::join('tokenswallet','tokenswallet.prescriptionid', '=' ,'prescriptiondetails.prescriptionid')	
		                                             //->select('Prescriptiondetails.*','prescriptionitemdetails.*','tokenswallet.*')
		                                             ->select('prescriptiondetails.prescriptionid','prescriptiondetails.userid','prescriptiondetails.prescriptiontitle','prescriptiondetails.prescriptiontype','prescriptiondetails.prescriptionimage','prescriptiondetails.prescriptionbackimage','prescriptiondetails.barcode','tokenswallet.*')
													 ->where('prescriptiondetails.userid', '=', $user->userid)
													 ->where('tokenswallet.createdby', '=', $user->userid)
													 ->where('tokenswallet.tokenstatus', '=', '1')
                                                     ->get();*/
        $prescriptionDetails = DB::table('prescriptiondetails as P')
                                ->select('P.prescriptionid','P.userid','P.prescriptiontitle','P.prescriptiontype','P.prescriptionimage','P.prescriptionbackimage','P.barcode','T.*','PI.*')
                                ->join('tokenswallet as T','T.prescriptionid','=','P.prescriptionid')
                                ->join('prescriptionitemdetails as PI','PI.prescriptionid','=','P.prescriptionid')
                                ->where('P.userid', '=', $user->userid)
								->where('T.createdby', '=', $user->userid)
								->where('P.prescriptionstatus', '=', '1')
								->where('T.tokenstatus', '=', '1')
                                ->get();
                                for($i=0;$i<count($prescriptionDetails);$i++){
                                    $prescriptionDetails[$i]->prescriptionimage=url('/prescriptions/'.$prescriptionDetails[$i]->prescriptionimage);
                                    $prescriptionDetails[$i]->prescriptionbackimage=url('/prescriptions/'.$prescriptionDetails[$i]->prescriptionbackimage);

                                }
		 return response()->json(['prescriptionDetails' => $prescriptionDetails], $this->successStatus); 
     }
     public function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
    
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $base64_string ) );

        $file = public_path('prescriptions/'). $output_file;
        file_put_contents($file, $ifp);
        rename($output_file, 'prescriptions/'.$output_file);
        // clean up the file resource
        fclose( $ifp ); 
        return $output_file; 
    }
		
	/**
      @OA\post(
          path="/v3/removePrescription",
          tags={"Scripts"},
          summary="Remove Prescription",
          operationId="RemovePrescription",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="prescriptionid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function removePrescription(Request $request)
	 {
		 
		 $user = Auth::user();
		
		
        $prescriptionDetails = Prescriptiondetails :: where('prescriptiondetails.prescriptionid', '=', $request->prescriptionid)
														  ->where('prescriptiondetails.userid', '=', $request->userid)
															->update(['prescriptiondetails.prescriptionstatus' => '0']);
															
		$prescriptionDetails = Tokenswallet :: where('tokenswallet.prescriptionid', '=', $request->prescriptionid)
													->where('tokenswallet.userid', '=', $request->userid)
													->update(['tokenswallet.tokenstatus' => '0']);
		
		$success['message'] = trans('messages.remove_prescription');
		 return response()->json(['success' => $success], $this->successStatus); 
     }
	/**
      @OA\Post(
          path="/v3/addtomedlist",
          tags={"MedList"},
          summary="Add to Medlist",
          operationId="Add to Medlist",
		  security={{"bearerAuth": {}} },
      
          
         @OA\Parameter(
              name="prescriptionuserid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Parameter(
              name="drugname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugstrength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugquantity",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="repeats",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="referrence",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="PhotoID",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        @OA\Parameter(
              name="mimsimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="drugpack",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        
          @OA\Parameter(
              name="script",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),  
          @OA\Parameter(
              name="price",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="cmicode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */

		public function addtomedlist(Request $request)
		{
			$image=$request->PhotoID;
			$image1=$request->mimsimage;
			if(strpos($image,env('APP_URL'))==true){
				$image=basename($image,'.jpg');			
			}
			if(strpos($image1,env('APP_URL'))==true){
				$image1=basename($image1,'.bmp');
			}
			$response = (object)array();
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			
			$prescriptionItems = new Prescriptionitemdetails;
			$prescriptionItems->prescriptionid = $request->prescriptionid;
			$prescriptionItems->prescriptionuserid = $request->prescriptionuserid;
			$prescriptionItems->profileid = $request->profileid;
			$prescriptionItems->drugname = $request->drugname;
			$prescriptionItems->drugform = $request->drugform;
			$prescriptionItems->drugstrength = $request->drugstrength;
			
			if($request->drugquantity!="")
			$prescriptionItems->drugquantity = $request->drugquantity;
			else
			$prescriptionItems->drugquantity = 1;	
			
			if($request->drugpack!="")
			$prescriptionItems->drugpack = $request->drugpack;
			else
			$prescriptionItems->drugpack = 1;
		
			if($request->price!="")
			$prescriptionItems->price = $request->price;
			else
			$prescriptionItems->price = 0.00;
		
			$prescriptionItems->repeats = $request->repeats;
			$prescriptionItems->referrencetable = $request->referrence;
			$prescriptionItems->drugdata = "Medmate";
			$prescriptionItems->prescriptionfor = $user->firstname;
			
			$prescriptionItems->medimage = $image;
			$prescriptionItems->mimsimage = $image1;
			$prescriptionItems->isscript = $request->script;
			$prescriptionItems->cmicode = $request->cmicode;
			
			if($request->prescriptionid == "")
			$prescriptionItems->prescriptionissuer = 'OTC';
			else
			$prescriptionItems->prescriptionissuer = 'Medisecure';		
			$prescriptionItems->ismedlist = "1";
			
			if($request->script == "1")
			$prescriptionItems->medstatus = "Add Your Script";
			else
			$prescriptionItems->medstatus = "medlist";
		
			$prescriptionItems->medimage = "";
			$prescriptionItems->save();
			
			
			$response->Medmatelist 	=  $prescriptionItems;							
			$response->msg 		= trans('messages.add_medlist');
			$response->status 		= $this->successStatus;
			
			return json_encode($response); 	
		}
		/**
			@OA\Get(
			  path="/v3/getmedlist",
			  tags={"MedList"},
			  summary="Get MedList",
			  operationId="Get MedList",
			  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
			 @OA\Response(
				  response=200,
				  description="Success",
				  @OA\MediaType(
					  mediaType="application/json",
				  )
			  ),
			  @OA\Response(
				  response=401,
				  description="Unauthorized"
			  ),
			  @OA\Response(
				  response=400,
				  description="Invalid request"
			  ),
			  @OA\Response(
				  response=404,
				  description="not found"
			  ),
		  )
		 */
		public function getmedlist(Request $request)
		{
			 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			if(!isset($request->isaccess)){
                $checkprescription = Prescriptionitemdetails :: join('healthprofile','healthprofile.profileid','=','prescriptionitemdetails.profileid')
                                                                   ->join('prescriptiondetails','prescriptiondetails.prescriptionid','=','prescriptionitemdetails.prescriptionid')
                                                                    ->where('healthprofile.profileid', '=', $request->profileid)
                                                                    ->where('prescriptionitemdetails.ismedlist', '=', '1')
                                                                    ->where('prescriptionitemdetails.isscript','=','1')
                                                                    ->whereIn('prescriptiondetails.prescriptiontype', array(1, 2))
                                                                    ->get();
                        if($checkprescription->count()>0){	
                            if($user->mi_accept==1){
                $getmedicationdetails = Prescriptionitemdetails :: join('healthprofile','healthprofile.profileid','=','prescriptionitemdetails.profileid')
                                                                    ->where('healthprofile.profileid', '=', $request->profileid)
                                                                    ->where('prescriptionitemdetails.ismedlist', '=', '1')																
                                                                    ->get();
                        }else{
                            $response = (object)array();
                            $response->msg="You have not authorised Medmate to access your e-Scripts. Would you like to authorise Medmate to access your e-Scripts?";
                            $response->status=401;
                            return json_encode($response);
                        }
                        }else{
                                        $getmedicationdetails = Prescriptionitemdetails :: join('healthprofile','healthprofile.profileid','=','prescriptionitemdetails.profileid')
                                                                    ->where('healthprofile.profileid', '=', $request->profileid)
                                                                    ->where('prescriptionitemdetails.ismedlist', '=', '1')																
                                                                    ->get();
                        }
                }else{
                    $getmedicationdetails = Prescriptionitemdetails :: join('healthprofile','healthprofile.profileid','=','prescriptionitemdetails.profileid')
                                                                    ->where('healthprofile.profileid', '=', $request->profileid)
                                                                    ->where('prescriptionitemdetails.ismedlist', '=', '1')
                                                                    ->where('prescriptionitemdetails.isscript','=','0')
                                                                    ->get();
                }												
                $noofrows = $getmedicationdetails->count();
                            
                    for($i=0; $i < $noofrows ; $i++)
                    {	
					$getmedicationdetails[$i]['price']=null;
				$im=explode("/",$getmedicationdetails[$i]['mimsimage']);
				if(count($im)>2){
					$getmedicationdetails[$i]['mimsimage']=basename($getmedicationdetails[$i]['mimsimage'],'.bmp');
				}
				$im1=explode("/",$getmedicationdetails[$i]['medimage']);
				if(count($im1)>2){
					$getmedicationdetails[$i]['medimage']=basename($getmedicationdetails[$i]['medimage'],'.jpg');
				}
						if($getmedicationdetails[$i]['mimsimage']!=null || $getmedicationdetails[$i]['mimsimage']!=''){
						 $getmedicationdetails[$i]['mimsimage']=env('APP_URL')."mimsimages/".$getmedicationdetails[$i]['mimsimage'].".bmp";
						}
						if($getmedicationdetails[$i]['medimage']!=null || $getmedicationdetails[$i]['medimage']!=''){
						$getmedicationdetails[$i]['mimsimage']=env('APP_URL')."catalouge/".$getmedicationdetails[$i]['medimage'].".jpg";
						$getmedicationdetails[$i]['medimage']='';
						}
                        if($getmedicationdetails[$i]['prescriptionid']!="")
                        {
                        $prescriptiondetails[$i] = Prescriptiondetails :: where('prescriptiondetails.prescriptionid', '=',  $getmedicationdetails[$i]['prescriptionid'] )
                                                                            ->select('prescriptiondetails.prescriptiontype','prescriptiondetails.prescriptionimage')
                                                                            ->get();
                        $getmedicationdetails[$i]['prescriptiontype']	= 	$prescriptiondetails[$i];
                        }
                        if($getmedicationdetails[$i]['medilistid']!="")
                        {
                            $medilistdetails[$i] = Prescriptionitemdetails :: where('prescriptionitemdetails.medilistid' ,'=' ,$getmedicationdetails[$i]['medilistid'] )
                                                                              ->where('prescriptionitemdetails.ismedlist', '=', '1')
                                                                            ->get();
                            $count = $medilistdetails[$i]->count();
                            
                            for($j=0;$j<$count;$j++)
                            {
                                //print_r($medilistdetails[$i][$j]['prescriptionid']);exit;
                                $medilistItems[$j] = Prescriptiondetails :: join('prescriptionitemdetails','prescriptionitemdetails.prescriptionid' ,'prescriptiondetails.prescriptionid')
                                                                        ->where('prescriptiondetails.prescriptionid',   $medilistdetails[$i][$j]['prescriptionid'] )
                                                                        ->where('prescriptionitemdetails.prescriptionid',   $medilistdetails[$i][$j]['prescriptionid'] )
                                                                        ->get();
                                
                                $getmedicationdetails[$i]['medilistItems']	= 	$medilistItems[$j];
                            }
                            
                                                                            
                        }
                    }
                            return response()->json(['getmedicationdetails' => $getmedicationdetails], $this->successStatus);
		}
		/**
			@OA\post(
			  path="/v3/removemedlist",
			  tags={"MedList"},
			  summary="Remove MedList",
			  operationId="Remove MedList",
			  security={{"bearerAuth": {}} },
		  
			@OA\Parameter(
				  name="prescriptionitemid",
				  in="query",
				  required=true,
				  @OA\Schema(
					  type="string"
				  )
			  ),
			@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
			),    
        
			 @OA\Response(
				  response=200,
				  description="Success",
				  @OA\MediaType(
					  mediaType="application/json",
				  )
			  ),
			  @OA\Response(
				  response=401,
				  description="Unauthorized"
			  ),
			  @OA\Response(
				  response=400,
				  description="Invalid request"
			  ),
			  @OA\Response(
				  response=404,
				  description="not found"
			  ),
		  )
		 */
		public function removemedlist(Request $request)
		{
			 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			$medicationdetails = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $request->prescriptionitemid)
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.ismedlist' => '0']);
			$success['message']= trans('messages.remove_medlist');
			$success['medicationdetails'] = $medicationdetails;														
			return response()->json(['success' => $success], $this->successStatus);
		}
		
		/**
			@OA\post(
			  path="/v3/editrepeatsinmedlist",
			  tags={"MedList"},
			  summary="Edit MedList",
			  operationId="Edit MedList",
			  security={{"bearerAuth": {}} },
		  
			@OA\Parameter(
				  name="prescriptionitemid",
				  in="query",
				  required=true,
				  @OA\Schema(
					  type="string"
				  )
			  ),
			@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
			),    
			@OA\Parameter(
              name="repeats",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
			),    
        
			 @OA\Response(
				  response=200,
				  description="Success",
				  @OA\MediaType(
					  mediaType="application/json",
				  )
			  ),
			  @OA\Response(
				  response=401,
				  description="Unauthorized"
			  ),
			  @OA\Response(
				  response=400,
				  description="Invalid request"
			  ),
			  @OA\Response(
				  response=404,
				  description="not found"
			  ),
		  )
		 */
		public function editrepeatsinmedlist(Request $request)
		{
			 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			
			$healthprofile = Healthprofile::where('userid', $user->userid)->get();
			$medicationdetails = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $request->prescriptionitemid)
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.repeats' => $request->repeats]);
			$success['message']= trans('messages.repeats_edit_medlist');
			$success['medicationdetails'] = $medicationdetails;													
																
			return response()->json(['success' => $success], $this->successStatus);
		}
		
		private function getcmiimages($drugname)
		{
			
			//print_r($drugname);exit;
			$catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,PI.image from catalouge_master as C left join packitemimages as PI on C.MIMsProductCode=PI.prodcode where C.MimsFormCode=PI.formcode and C.MimsPackCode=PI.packcode and C.itemNameDisplay = '".$drugname."'");                
			
			if(count($catalouge_data) > 0)
			{
				return $catalouge_data;
			}
			else{
				
				$mims_data=DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode`  left join packitemimages as PI on P.prodcode=PI.prodcode where  P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode  and `P`.`ActualProduct` ='".$drugname."'");
				
				return $mims_data;
			}
		}
		
		public function qrcodeimage($base64_string ,$output_file) {
		// open the output file for writing
		//$output_file="qrcode.jpeg";
		//$base64_string="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABLeSURBVHhe7ZJBkttAEsT2/5/e5YHiphiQMeWqlih6EIFLB5HdI/s/G/+9swY177QLbaaroTtvJh7eRoOad9qFNtPV0J03Ew9vo0HNO+1Cm+lq6M6biYe30aDmnXahzXQ1dOfNxMPbaFDzTrvQZroauvNm4uFtNKh5p11oM10N3Xkz8fA2GtS80y60ma6G7ryZeHgbDWreaRfaTFdDd95MPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDw0qJnUoCbtQpupQU1Fg5pJDWpSg5pJDWpO4uGhQc2kBjVpF9pMDWoqGtRMalCTGtRMalBzEg8PDWomNahJu9BmalBT0aBmUoOa1KBmUoOak3h4aFAzqUFN2oU2U4OaigY1kxrUpAY1kxrUnMTDQ4OaSQ1q0i60mRrUVDSomdSgJjWomdSg5iQeHhrUTGpQk3ahzdSgpqJBzaQGNalBzaQGNSfx8NCgZlKDmrQLbaYGNRUNaiY1qEkNaiY1qDmJh4cGNZMa1KRdaDM1qKloUDOpQU1qUDOpQc1JPDw0qJnUoCY1qJm0C22+U4Oa1KBmUoOak3h4aFAzqUFNalAzaRfafKcGNalBzaQGNSfx8NCgZlKDmtSgZtIutPlODWpSg5pJDWpO4uGhQc2kBjWpQc2kXWjznRrUpAY1kxrUnMTDQ4OaSQ1qUoOaSbvQ5js1qEkNaiY1qDmJh4cGNZMa1KQGNZN2oc13alCTGtRMalBzEg8PDWomNahJDWom7UKb79SgJjWomdSg5iQeHhrUTGpQkxrUTNqFNt+pQU1qUDOpQc1JPDw0qJnUoCY1qKloUDNpF9pMDWpSg5pJDWpO4uGhQc2kBjWpQU1Fg5pJu9BmalCTGtRMalBzEg8PDWomNahJDWoqGtRM2oU2U4Oa1KBmUoOak3h4aFAzqUFNalBT0aBm0i60mRrUpAY1kxrUnMTDQ4OaSQ1qUoOaigY1k3ahzdSgJjWomdSg5iQeHhrUTGpQkxrUVDSombQLbaYGNalBzaQGNSfx8NCgZlKDmtSgpqJBzaRdaDM1qEkNaiY1qDmJh4cGNZMa1KQGNRUNaibtQpupQU1qUDOpQc1JPDy8OvTm1KDmmzSoSbvQZnp16M0n8fDw6tCbU4Oab9KgJu1Cm+nVoTefxMPDq0NvTg1qvkmDmrQLbaZXh958Eg8Prw69OTWo+SYNatIutJleHXrzSTw8vDr05tSg5ps0qEm70GZ6dejNJ/Hw8OrQm1ODmm/SoCbtQpvp1aE3n8TDw6tDb04Nar5Jg5q0C22mV4fefBIPD68OvTk1qPkmDWrSLrSZXh1680k8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcTDy8jQY1qUFNalCTGtSkBjWpQc3NxMPbaFCTGtSkBjWpQU1qUJMa1NxMPLyNBjWpQU1qUJMa1KQGNalBzc3Ew9toUJMa1KQGNalBTWpQkxrU3Ew8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcyv3v/OUF+KOFBjVpF9qs+Muf+f2FBPpPlRrUpF1os+Ivf+b3FxLoP1VqUJN2oc2Kv/yZ319IoP9UqUFN2oU2K/7yZ35/IYH+U6UGNWkX2qz4y5/5/YUE+k+VGtSkXWiz4i9/5vcXEug/VWpQk3ahzYq//JnfX0ig/1SpQU3ahTYr/vJnlv9C9I+SdqHNtAttVuxCm5Ouhu6c1KCm5L6zDLw07EKbaRfarNiFNiddDd05qUFNyX1nGXhp2IU20y60WbELbU66GrpzUoOakvvOMvDSsAttpl1os2IX2px0NXTnpAY1JfedZeClYRfaTLvQZsUutDnpaujOSQ1qSu47y8BLwy60mXahzYpdaHPS1dCdkxrUlNx3loGXhl1oM+1CmxW70Oakq6E7JzWoKbnvLAMvDbvQZtqFNit2oc1JV0N3TmpQUxQPxzSoST8NvSk1qKnYhTYnNahJDWomNbZvOJzSoCb9NPSm1KCmYhfanNSgJjWomdTYvuFwSoOa9NPQm1KDmopdaHNSg5rUoGZSY/uGwykNatJPQ29KDWoqdqHNSQ1qUoOaSY3tGw6nNKhJPw29KTWoqdiFNic1qEkNaiY1tm84nNKgJv009KbUoKZiF9qc1KAmNaiZ1Ni+4XBKg5r009CbUoOail1oc1KDmtSgZlJj+4bDKQ1q0k9Db0oNaip2oc1JDWpSg5pJDf2CRtNPQ29KDWpSg5oraVCTGtRMuhq688n9u5dgFH4aelNqUJMa1FxJg5rUoGbS1dCdT+7fvQSj8NPQm1KDmtSg5koa1KQGNZOuhu58cv/uJRiFn4belBrUpAY1V9KgJjWomXQ1dOeT+3cvwSj8NPSm1KAmNai5kgY1qUHNpKuhO5/cv3sJRuGnoTelBjWpQc2VNKhJDWomXQ3d+eT+3UswCj8NvSk1qEkNaq6kQU1qUDPpaujOJ/fvXoJR+GnoTalBTWpQcyUNalKDmklXQ3c+uX/3EowKGtSkV4feXHE1dGfFLrQ5qUFNyX3nJRgVNKhJrw69ueJq6M6KXWhzUoOakvvOSzAqaFCTXh16c8XV0J0Vu9DmpAY1Jfedl2BU0KAmvTr05oqroTsrdqHNSQ1qSu47L8GooEFNenXozRVXQ3dW7EKbkxrUlNx3XoJRQYOa9OrQmyuuhu6s2IU2JzWoKbnvvASjggY16dWhN1dcDd1ZsQttTmpQU3LfeQlGBQ1q0qtDb664GrqzYhfanNSgpuS+swy8NOxCm+mnoTdNalCTfhp6U2pQM+p+zzLw0rALbaafht40qUFN+mnoTalBzaj7PcvAS8MutJl+GnrTpAY16aehN6UGNaPu9ywDLw270Gb6aehNkxrUpJ+G3pQa1Iy637MMvDTsQpvpp6E3TWpQk34aelNqUDPqfs8y8NKwC22mn4beNKlBTfpp6E2pQc2o+z3LwEvDLrSZfhp606QGNemnoTelBjWj7vcsAy8Nu9Bm+mnoTZMa1KSfht6UGtSMut/z1+DoG+1Cm2kX2pzUoGZSg5rUoKaiQc2T+3d/DY6+0S60mXahzUkNaiY1qEkNaioa1Dy5f/fX4Ogb7UKbaRfanNSgZlKDmtSgpqJBzZP7d38Njr7RLrSZdqHNSQ1qJjWoSQ1qKhrUPLl/99fg6BvtQptpF9qc1KBmUoOa1KCmokHNk/t3fw2OvtEutJl2oc1JDWomNahJDWoqGtQ8uX/31+DoG+1Cm2kX2pzUoGZSg5rUoKaiQc2T+3d/DY6+0S60mXahzUkNaiY1qEkNaioa1JzEw8MutPlODWoqGtSk3w79TZMa1KQGNSfx8LALbb5Tg5qKBjXpt0N/06QGNalBzUk8POxCm+/UoKaiQU367dDfNKlBTWpQcxIPD7vQ5js1qKloUJN+O/Q3TWpQkxrUnMTDwy60+U4Naioa1KTfDv1NkxrUpAY1J/HwsAttvlODmooGNem3Q3/TpAY1qUHNSTw87EKb79SgpqJBTfrt0N80qUFNalBzEg8Pu9DmOzWoqWhQk3479DdNalCTGtQ8uX/3EozCLrSZdqHNdDV056QGNWkX2nynXWjzyf27l2AUdqHNtAttpquhOyc1qEm70OY77UKbT+7fvQSjsAttpl1oM10N3TmpQU3ahTbfaRfafHL/7iUYhV1oM+1Cm+lq6M5JDWrSLrT5TrvQ5pP7dy/BKOxCm2kX2kxXQ3dOalCTdqHNd9qFNp/cv3sJRmEX2ky70Ga6GrpzUoOatAttvtMutPnk/t1LMAq70GbahTbT1dCdkxrUpF1o8512oc0n9+9eglHYhTbTLrSZrobunNSgJu1Cm++0C22exMMfa1CTdqHNO2lQkxrUXMkutJka2zcc/lSDmrQLbd5Jg5rUoOZKdqHN1Ni+4fCnGtSkXWjzThrUpAY1V7ILbabG9g2HP9WgJu1Cm3fSoCY1qLmSXWgzNbZvOPypBjVpF9q8kwY1qUHNlexCm6mxfcPhTzWoSbvQ5p00qEkNaq5kF9pMje0bDn+qQU3ahTbvpEFNalBzJbvQZmps33D4Uw1q0i60eScNalKDmivZhTZTo/+CL4d+tLQLbVY0qJl0NXRnRYOakvvOPwv+KGEX2qxoUDPpaujOigY1Jfedfxb8UcIutFnRoGbS1dCdFQ1qSu47/yz4o4RdaLOiQc2kq6E7KxrUlNx3/lnwRwm70GZFg5pJV0N3VjSoKbnv/LPgjxJ2oc2KBjWTroburGhQU3Lf+WfBHyXsQpsVDWomXQ3dWdGgpuS+88+CP0rYhTYrGtRMuhq6s6JBTVE8vI1daLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHr6LXWizokFNxS60mRrUpAY1qUFN2mXb4OG72IU2KxrUVOxCm6lBTWpQkxrUpF22DR6+i11os6JBTcUutJka1KQGNalBTdpl2+Dhu9iFNisa1FTsQpupQU1qUJMa1KRdtg0evotdaLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHn54dejNaRfarGhQk66G7kxXQ3emBjUVje0bDh9eHXpz2oU2KxrUpKuhO9PV0J2pQU1FY/uGw4dXh96cdqHNigY16WroznQ1dGdqUFPR2L7h8OHVoTenXWizokFNuhq6M10N3Zka1FQ0tm84fHh16M1pF9qsaFCTrobuTFdDd6YGNRWN7RsOH14denPahTYrGtSkq6E709XQnalBTUVj+4bDh1eH3px2oc2KBjXpaujOdDV0Z2pQU9HYvuHw4dWhN6ddaLOiQU26GrozXQ3dmRrUVDS2bzh8aFAzqUFN2oU20y60mXahzW/SoKaisX3D4UODmkkNatIutJl2oc20C21+kwY1FY3tGw4fGtRMalCTdqHNtAttpl1o85s0qKlobN9w+NCgZlKDmrQLbaZdaDPtQpvfpEFNRWP7hsOHBjWTGtSkXWgz7UKbaRfa/CYNaioa2zccPjSomdSgJu1Cm2kX2ky70OY3aVBT0di+4fChQc2kBjVpF9pMu9Bm2oU2v0mDmorG9g2HDw1qJjWoSbvQZtqFNtMutPlNGtRUNLZvOHxoUDOpQU1qUJMa1KQGNd+kQU1qUDOpsX3D4UODmkkNalKDmtSgJjWo+SYNalKDmkmN7RsOHxrUTGpQkxrUpAY1qUHNN2lQkxrUTGps33D40KBmUoOa1KAmNahJDWq+SYOa1KBmUmP7hsOHBjWTGtSkBjWpQU1qUPNNGtSkBjWTGts3HD40qJnUoCY1qEkNalKDmm/SoCY1qJnU2L7h8KFBzaQGNalBTWpQkxrUfJMGNalBzaTG9g2HDw1qJjWoSQ1qUoOa1KDmmzSoSQ1qJjW2bzh8aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4eHXozWkX2qy4GrqzokHNlexCmyfx8PDq0JvTLrRZcTV0Z0WDmivZhTZP4uHh1aE3p11os+Jq6M6KBjVXsgttnsTDw6tDb0670GbF1dCdFQ1qrmQX2jyJh4dXh96cdqHNiquhOysa1FzJLrR5Eg8Prw69Oe1CmxVXQ3dWNKi5kl1o8yQeHl4denPahTYrroburGhQcyW70OZJPDy8OvTmtAttVlwN3VnRoOZKdqHNk3h4G7vQZtqFNlODmtSgJl0N3Zka1KQGNSfx8DZ2oc20C22mBjWpQU26GrozNahJDWpO4uFt7EKbaRfaTA1qUoOadDV0Z2pQkxrUnMTD29iFNtMutJka1KQGNelq6M7UoCY1qDmJh7exC22mXWgzNahJDWrS1dCdqUFNalBzEg9vYxfaTLvQZmpQkxrUpKuhO1ODmtSg5iQe3sYutJl2oc3UoCY1qElXQ3emBjWpQc1JPLyNXWgz7UKbqUFNalCTrobuTA1qUoOa//uf//4PH72d67VqytUAAAAASUVORK5CYII=";
		//$base64_string="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABLeSURBVHhe7ZJBkttAEsT2/5/e5YHiphiQMeWqlih6EIFLB5HdI/s/G/&#x2B;9swY177QLbaaroTtvJh7eRoOad9qFNtPV0J03Ew9vo0HNO&#x2B;1Cm&#x2B;lq6M6biYe30aDmnXahzXQ1dOfNxMPbaFDzTrvQZroauvNm4uFtNKh5p11oM10N3Xkz8fA2GtS80y60ma6G7ryZeHgbDWreaRfaTFdDd95MPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDw0qJnUoCbtQpupQU1Fg5pJDWpSg5pJDWpO4uGhQc2kBjVpF9pMDWoqGtRMalCTGtRMalBzEg8PDWomNahJu9BmalBT0aBmUoOa1KBmUoOak3h4aFAzqUFN2oU2U4OaigY1kxrUpAY1kxrUnMTDQ4OaSQ1q0i60mRrUVDSomdSgJjWomdSg5iQeHhrUTGpQk3ahzdSgpqJBzaQGNalBzaQGNSfx8NCgZlKDmrQLbaYGNRUNaiY1qEkNaiY1qDmJh4cGNZMa1KRdaDM1qKloUDOpQU1qUDOpQc1JPDw0qJnUoCY1qJm0C22&#x2B;U4Oa1KBmUoOak3h4aFAzqUFNalAzaRfafKcGNalBzaQGNSfx8NCgZlKDmtSgZtIutPlODWpSg5pJDWpO4uGhQc2kBjWpQc2kXWjznRrUpAY1kxrUnMTDQ4OaSQ1qUoOaSbvQ5js1qEkNaiY1qDmJh4cGNZMa1KQGNZN2oc13alCTGtRMalBzEg8PDWomNahJDWom7UKb79SgJjWomdSg5iQeHhrUTGpQkxrUTNqFNt&#x2B;pQU1qUDOpQc1JPDw0qJnUoCY1qKloUDNpF9pMDWpSg5pJDWpO4uGhQc2kBjWpQU1Fg5pJu9BmalCTGtRMalBzEg8PDWomNahJDWoqGtRM2oU2U4Oa1KBmUoOak3h4aFAzqUFNalBT0aBm0i60mRrUpAY1kxrUnMTDQ4OaSQ1qUoOaigY1k3ahzdSgJjWomdSg5iQeHhrUTGpQkxrUVDSombQLbaYGNalBzaQGNSfx8NCgZlKDmtSgpqJBzaRdaDM1qEkNaiY1qDmJh4cGNZMa1KQGNRUNaibtQpupQU1qUDOpQc1JPDy8OvTm1KDmmzSoSbvQZnp16M0n8fDw6tCbU4Oab9KgJu1Cm&#x2B;nVoTefxMPDq0NvTg1qvkmDmrQLbaZXh958Eg8Prw69OTWo&#x2B;SYNatIutJleHXrzSTw8vDr05tSg5ps0qEm70GZ6dejNJ/Hw8OrQm1ODmm/SoCbtQpvp1aE3n8TDw6tDb04Nar5Jg5q0C22mV4fefBIPD68OvTk1qPkmDWrSLrSZXh1680k8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcTDy8jQY1qUFNalCTGtSkBjWpQc3NxMPbaFCTGtSkBjWpQU1qUJMa1NxMPLyNBjWpQU1qUJMa1KQGNalBzc3Ew9toUJMa1KQGNalBTWpQkxrU3Ew8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcyv3v/OUF&#x2B;KOFBjVpF9qs&#x2B;Muf&#x2B;f2FBPpPlRrUpF1os&#x2B;Ivf&#x2B;b3FxLoP1VqUJN2oc2Kv/yZ319IoP9UqUFN2oU2K/7yZ35/IYH&#x2B;U6UGNWkX2qz4y5/5/YUE&#x2B;k&#x2B;VGtSkXWiz4i9/5vcXEug/VWpQk3ahzYq//JnfX0ig/1SpQU3ahTYr/vJnlv9C9I&#x2B;SdqHNtAttVuxCm5Ouhu6c1KCm5L6zDLw07EKbaRfarNiFNiddDd05qUFNyX1nGXhp2IU20y60WbELbU66GrpzUoOakvvOMvDSsAttpl1os2IX2px0NXTnpAY1JfedZeClYRfaTLvQZsUutDnpaujOSQ1qSu47y8BLwy60mXahzYpdaHPS1dCdkxrUlNx3loGXhl1oM&#x2B;1CmxW70Oakq6E7JzWoKbnvLAMvDbvQZtqFNit2oc1JV0N3TmpQUxQPxzSoST8NvSk1qKnYhTYnNahJDWomNbZvOJzSoCb9NPSm1KCmYhfanNSgJjWomdTYvuFwSoOa9NPQm1KDmopdaHNSg5rUoGZSY/uGwykNatJPQ29KDWoqdqHNSQ1qUoOaSY3tGw6nNKhJPw29KTWoqdiFNic1qEkNaiY1tm84nNKgJv009KbUoKZiF9qc1KAmNaiZ1Ni&#x2B;4XBKg5r009CbUoOail1oc1KDmtSgZlJj&#x2B;4bDKQ1q0k9Db0oNaip2oc1JDWpSg5pJDf2CRtNPQ29KDWpSg5oraVCTGtRMuhq688n9u5dgFH4aelNqUJMa1FxJg5rUoGbS1dCdT&#x2B;7fvQSj8NPQm1KDmtSg5koa1KQGNZOuhu58cv/uJRiFn4belBrUpAY1V9KgJjWomXQ1dOeT&#x2B;3cvwSj8NPSm1KAmNai5kgY1qUHNpKuhO5/cv3sJRuGnoTelBjWpQc2VNKhJDWomXQ3d&#x2B;eT&#x2B;3UswCj8NvSk1qEkNaq6kQU1qUDPpaujOJ/fvXoJR&#x2B;GnoTalBTWpQcyUNalKDmklXQ3c&#x2B;uX/3EowKGtSkV4feXHE1dGfFLrQ5qUFNyX3nJRgVNKhJrw69ueJq6M6KXWhzUoOakvvOSzAqaFCTXh16c8XV0J0Vu9DmpAY1Jfedl2BU0KAmvTr05oqroTsrdqHNSQ1qSu47L8GooEFNenXozRVXQ3dW7EKbkxrUlNx3XoJRQYOa9OrQmyuuhu6s2IU2JzWoKbnvvASjggY16dWhN1dcDd1ZsQttTmpQU3LfeQlGBQ1q0qtDb664GrqzYhfanNSgpuS&#x2B;swy8NOxCm&#x2B;mnoTdNalCTfhp6U2pQM&#x2B;p&#x2B;zzLw0rALbaafht40qUFN&#x2B;mnoTalBzaj7PcvAS8MutJl&#x2B;GnrTpAY16aehN6UGNaPu9ywDLw270Gb6aehNkxrUpJ&#x2B;G3pQa1Iy637MMvDTsQpvpp6E3TWpQk34aelNqUDPqfs8y8NKwC22mn4beNKlBTfpp6E2pQc2o&#x2B;z3LwEvDLrSZfhp606QGNemnoTelBjWj7vcsAy8Nu9Bm&#x2B;mnoTZMa1KSfht6UGtSMut/z1&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1Dy5f/fX4Ogb7UKbaRfanNSgZlKDmtSgpqJBzZP7d38Njr7RLrSZdqHNSQ1qJjWoSQ1qKhrUPLl/99fg6BvtQptpF9qc1KBmUoOa1KCmokHNk/t3fw2OvtEutJl2oc1JDWomNahJDWoqGtQ8uX/31&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1JzEw8MutPlODWoqGtSk3w79TZMa1KQGNSfx8LALbb5Tg5qKBjXpt0N/06QGNalBzUk8POxCm&#x2B;/UoKaiQU367dDfNKlBTWpQcxIPD7vQ5js1qKloUJN&#x2B;O/Q3TWpQkxrUnMTDwy60&#x2B;U4Naioa1KTfDv1NkxrUpAY1J/HwsAttvlODmooGNem3Q3/TpAY1qUHNSTw87EKb79SgpqJBTfrt0N80qUFNalBzEg8Pu9DmOzWoqWhQk3479DdNalCTGtQ8uX/3EozCLrSZdqHNdDV056QGNWkX2nynXWjzyf27l2AUdqHNtAttpquhOyc1qEm70OY77UKbT&#x2B;7fvQSjsAttpl1oM10N3TmpQU3ahTbfaRfafHL/7iUYhV1oM&#x2B;1Cm&#x2B;lq6M5JDWrSLrT5TrvQ5pP7dy/BKOxCm2kX2kxXQ3dOalCTdqHNd9qFNp/cv3sJRmEX2ky70Ga6GrpzUoOatAttvtMutPnk/t1LMAq70GbahTbT1dCdkxrUpF1o8512oc0n9&#x2B;9eglHYhTbTLrSZrobunNSgJu1Cm&#x2B;&#x2B;0C22exMMfa1CTdqHNO2lQkxrUXMkutJka2zcc/lSDmrQLbd5Jg5rUoOZKdqHN1Ni&#x2B;4fCnGtSkXWjzThrUpAY1V7ILbabG9g2HP9WgJu1Cm3fSoCY1qLmSXWgzNbZvOPypBjVpF9q8kwY1qUHNlexCm6mxfcPhTzWoSbvQ5p00qEkNaq5kF9pMje0bDn&#x2B;qQU3ahTbvpEFNalBzJbvQZmps33D4Uw1q0i60eScNalKDmivZhTZTo/&#x2B;CL4d&#x2B;tLQLbVY0qJl0NXRnRYOakvvOPwv&#x2B;KGEX2qxoUDPpaujOigY1Jfedfxb8UcIutFnRoGbS1dCdFQ1qSu47/yz4o4RdaLOiQc2kq6E7KxrUlNx3/lnwRwm70GZFg5pJV0N3VjSoKbnv/LPgjxJ2oc2KBjWTroburGhQU3Lf&#x2B;WfBHyXsQpsVDWomXQ3dWdGgpuS&#x2B;88&#x2B;CP0rYhTYrGtRMuhq6s6JBTVE8vI1daLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHr6LXWizokFNxS60mRrUpAY1qUFN2mXb4OG72IU2KxrUVOxCm6lBTWpQkxrUpF22DR6&#x2B;i11os6JBTcUutJka1KQGNalBTdpl2&#x2B;Dhu9iFNisa1FTsQpupQU1qUJMa1KRdtg0evotdaLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHn54dejNaRfarGhQk66G7kxXQ3emBjUVje0bDh9eHXpz2oU2KxrUpKuhO9PV0J2pQU1FY/uGw4dXh96cdqHNigY16WroznQ1dGdqUFPR2L7h8OHVoTenXWizokFNuhq6M10N3Zka1FQ0tm84fHh16M1pF9qsaFCTrobuTFdDd6YGNRWN7RsOH14denPahTYrGtSkq6E709XQnalBTUVj&#x2B;4bDh1eH3px2oc2KBjXpaujOdDV0Z2pQU9HYvuHw4dWhN6ddaLOiQU26GrozXQ3dmRrUVDS2bzh8aFAzqUFN2oU20y60mXahzW/SoKaisX3D4UODmkkNatIutJl2oc20C21&#x2B;kwY1FY3tGw4fGtRMalCTdqHNtAttpl1o85s0qKlobN9w&#x2B;NCgZlKDmrQLbaZdaDPtQpvfpEFNRWP7hsOHBjWTGtSkXWgz7UKbaRfa/CYNaioa2zccPjSomdSgJu1Cm2kX2ky70OY3aVBT0di&#x2B;4fChQc2kBjVpF9pMu9Bm2oU2v0mDmorG9g2HDw1qJjWoSbvQZtqFNtMutPlNGtRUNLZvOHxoUDOpQU1qUJMa1KQGNd&#x2B;kQU1qUDOpsX3D4UODmkkNalKDmtSgJjWo&#x2B;SYNalKDmkmN7RsOHxrUTGpQkxrUpAY1qUHNN2lQkxrUTGps33D40KBmUoOa1KAmNahJDWq&#x2B;SYOa1KBmUmP7hsOHBjWTGtSkBjWpQU1qUPNNGtSkBjWTGts3HD40qJnUoCY1qEkNalKDmm/SoCY1qJnU2L7h8KFBzaQGNalBTWpQkxrUfJMGNalBzaTG9g2HDw1qJjWoSQ1qUoOa1KDmmzSoSQ1qJjW2bzh8aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4eHXozWkX2qy4GrqzokHNlexCmyfx8PDq0JvTLrRZcTV0Z0WDmivZhTZP4uHh1aE3p11os&#x2B;Jq6M6KBjVXsgttnsTDw6tDb0670GbF1dCdFQ1qrmQX2jyJh4dXh96cdqHNiquhOysa1FzJLrR5Eg8Prw69Oe1CmxVXQ3dWNKi5kl1o8yQeHl4denPahTYrroburGhQcyW70OZJPDy8OvTmtAttVlwN3VnRoOZKdqHNk3h4G7vQZtqFNlODmtSgJl0N3Zka1KQGNSfx8DZ2oc20C22mBjWpQU26GrozNahJDWpO4uFt7EKbaRfaTA1qUoOadDV0Z2pQkxrUnMTD29iFNtMutJka1KQGNelq6M7UoCY1qDmJh7exC22mXWgzNahJDWrS1dCdqUFNalBzEg9vYxfaTLvQZmpQkxrUpKuhO1ODmtSg5iQe3sYutJl2oc3UoCY1qElXQ3emBjWpQc1JPLyNXWgz7UKbqUFNalCTrobuTA1qUoOa//uf//4PH72d67VqytUAAAAASUVORK5CYII=";
		//  $str="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABLeSURBVHhe7ZJBkttAEsT2/5/e5YHiphiQMeWqlih6EIFLB5HdI/s/G/&#x2B;9swY177QLbaaroTtvJh7eRoOad9qFNtPV0J03Ew9vo0HNO&#x2B;1Cm&#x2B;lq6M6biYe30aDmnXahzXQ1dOfNxMPbaFDzTrvQZroauvNm4uFtNKh5p11oM10N3Xkz8fA2GtS80y60ma6G7ryZeHgbDWreaRfaTFdDd95MPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDy8OvTm1KAmNahJDWrS1dCdqUFNenXozSfx8PDq0JtTg5rUoCY1qElXQ3emBjXp1aE3n8TDw6tDb04NalKDmtSgJl0N3Zka1KRXh958Eg8Prw69OTWoSQ1qUoOadDV0Z2pQk14devNJPDw0qJnUoCbtQpupQU1Fg5pJDWpSg5pJDWpO4uGhQc2kBjVpF9pMDWoqGtRMalCTGtRMalBzEg8PDWomNahJu9BmalBT0aBmUoOa1KBmUoOak3h4aFAzqUFN2oU2U4OaigY1kxrUpAY1kxrUnMTDQ4OaSQ1q0i60mRrUVDSomdSgJjWomdSg5iQeHhrUTGpQk3ahzdSgpqJBzaQGNalBzaQGNSfx8NCgZlKDmrQLbaYGNRUNaiY1qEkNaiY1qDmJh4cGNZMa1KRdaDM1qKloUDOpQU1qUDOpQc1JPDw0qJnUoCY1qJm0C22&#x2B;U4Oa1KBmUoOak3h4aFAzqUFNalAzaRfafKcGNalBzaQGNSfx8NCgZlKDmtSgZtIutPlODWpSg5pJDWpO4uGhQc2kBjWpQc2kXWjznRrUpAY1kxrUnMTDQ4OaSQ1qUoOaSbvQ5js1qEkNaiY1qDmJh4cGNZMa1KQGNZN2oc13alCTGtRMalBzEg8PDWomNahJDWom7UKb79SgJjWomdSg5iQeHhrUTGpQkxrUTNqFNt&#x2B;pQU1qUDOpQc1JPDw0qJnUoCY1qKloUDNpF9pMDWpSg5pJDWpO4uGhQc2kBjWpQU1Fg5pJu9BmalCTGtRMalBzEg8PDWomNahJDWoqGtRM2oU2U4Oa1KBmUoOak3h4aFAzqUFNalBT0aBm0i60mRrUpAY1kxrUnMTDQ4OaSQ1qUoOaigY1k3ahzdSgJjWomdSg5iQeHhrUTGpQkxrUVDSombQLbaYGNalBzaQGNSfx8NCgZlKDmtSgpqJBzaRdaDM1qEkNaiY1qDmJh4cGNZMa1KQGNRUNaibtQpupQU1qUDOpQc1JPDy8OvTm1KDmmzSoSbvQZnp16M0n8fDw6tCbU4Oab9KgJu1Cm&#x2B;nVoTefxMPDq0NvTg1qvkmDmrQLbaZXh958Eg8Prw69OTWo&#x2B;SYNatIutJleHXrzSTw8vDr05tSg5ps0qEm70GZ6dejNJ/Hw8OrQm1ODmm/SoCbtQpvp1aE3n8TDw6tDb04Nar5Jg5q0C22mV4fefBIPD68OvTk1qPkmDWrSLrSZXh1680k8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcTDy8jQY1qUFNalCTGtSkBjWpQc3NxMPbaFCTGtSkBjWpQU1qUJMa1NxMPLyNBjWpQU1qUJMa1KQGNalBzc3Ew9toUJMa1KQGNalBTWpQkxrU3Ew8vI0GNalBTWpQkxrUpAY1qUHNzcTD22hQkxrUpAY1qUFNalCTGtTcyv3v/OUF&#x2B;KOFBjVpF9qs&#x2B;Muf&#x2B;f2FBPpPlRrUpF1os&#x2B;Ivf&#x2B;b3FxLoP1VqUJN2oc2Kv/yZ319IoP9UqUFN2oU2K/7yZ35/IYH&#x2B;U6UGNWkX2qz4y5/5/YUE&#x2B;k&#x2B;VGtSkXWiz4i9/5vcXEug/VWpQk3ahzYq//JnfX0ig/1SpQU3ahTYr/vJnlv9C9I&#x2B;SdqHNtAttVuxCm5Ouhu6c1KCm5L6zDLw07EKbaRfarNiFNiddDd05qUFNyX1nGXhp2IU20y60WbELbU66GrpzUoOakvvOMvDSsAttpl1os2IX2px0NXTnpAY1JfedZeClYRfaTLvQZsUutDnpaujOSQ1qSu47y8BLwy60mXahzYpdaHPS1dCdkxrUlNx3loGXhl1oM&#x2B;1CmxW70Oakq6E7JzWoKbnvLAMvDbvQZtqFNit2oc1JV0N3TmpQUxQPxzSoST8NvSk1qKnYhTYnNahJDWomNbZvOJzSoCb9NPSm1KCmYhfanNSgJjWomdTYvuFwSoOa9NPQm1KDmopdaHNSg5rUoGZSY/uGwykNatJPQ29KDWoqdqHNSQ1qUoOaSY3tGw6nNKhJPw29KTWoqdiFNic1qEkNaiY1tm84nNKgJv009KbUoKZiF9qc1KAmNaiZ1Ni&#x2B;4XBKg5r009CbUoOail1oc1KDmtSgZlJj&#x2B;4bDKQ1q0k9Db0oNaip2oc1JDWpSg5pJDf2CRtNPQ29KDWpSg5oraVCTGtRMuhq688n9u5dgFH4aelNqUJMa1FxJg5rUoGbS1dCdT&#x2B;7fvQSj8NPQm1KDmtSg5koa1KQGNZOuhu58cv/uJRiFn4belBrUpAY1V9KgJjWomXQ1dOeT&#x2B;3cvwSj8NPSm1KAmNai5kgY1qUHNpKuhO5/cv3sJRuGnoTelBjWpQc2VNKhJDWomXQ3d&#x2B;eT&#x2B;3UswCj8NvSk1qEkNaq6kQU1qUDPpaujOJ/fvXoJR&#x2B;GnoTalBTWpQcyUNalKDmklXQ3c&#x2B;uX/3EowKGtSkV4feXHE1dGfFLrQ5qUFNyX3nJRgVNKhJrw69ueJq6M6KXWhzUoOakvvOSzAqaFCTXh16c8XV0J0Vu9DmpAY1Jfedl2BU0KAmvTr05oqroTsrdqHNSQ1qSu47L8GooEFNenXozRVXQ3dW7EKbkxrUlNx3XoJRQYOa9OrQmyuuhu6s2IU2JzWoKbnvvASjggY16dWhN1dcDd1ZsQttTmpQU3LfeQlGBQ1q0qtDb664GrqzYhfanNSgpuS&#x2B;swy8NOxCm&#x2B;mnoTdNalCTfhp6U2pQM&#x2B;p&#x2B;zzLw0rALbaafht40qUFN&#x2B;mnoTalBzaj7PcvAS8MutJl&#x2B;GnrTpAY16aehN6UGNaPu9ywDLw270Gb6aehNkxrUpJ&#x2B;G3pQa1Iy637MMvDTsQpvpp6E3TWpQk34aelNqUDPqfs8y8NKwC22mn4beNKlBTfpp6E2pQc2o&#x2B;z3LwEvDLrSZfhp606QGNemnoTelBjWj7vcsAy8Nu9Bm&#x2B;mnoTZMa1KSfht6UGtSMut/z1&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1Dy5f/fX4Ogb7UKbaRfanNSgZlKDmtSgpqJBzZP7d38Njr7RLrSZdqHNSQ1qJjWoSQ1qKhrUPLl/99fg6BvtQptpF9qc1KBmUoOa1KCmokHNk/t3fw2OvtEutJl2oc1JDWomNahJDWoqGtQ8uX/31&#x2B;DoG&#x2B;1Cm2kX2pzUoGZSg5rUoKaiQc2T&#x2B;3d/DY6&#x2B;0S60mXahzUkNaiY1qEkNaioa1JzEw8MutPlODWoqGtSk3w79TZMa1KQGNSfx8LALbb5Tg5qKBjXpt0N/06QGNalBzUk8POxCm&#x2B;/UoKaiQU367dDfNKlBTWpQcxIPD7vQ5js1qKloUJN&#x2B;O/Q3TWpQkxrUnMTDwy60&#x2B;U4Naioa1KTfDv1NkxrUpAY1J/HwsAttvlODmooGNem3Q3/TpAY1qUHNSTw87EKb79SgpqJBTfrt0N80qUFNalBzEg8Pu9DmOzWoqWhQk3479DdNalCTGtQ8uX/3EozCLrSZdqHNdDV056QGNWkX2nynXWjzyf27l2AUdqHNtAttpquhOyc1qEm70OY77UKbT&#x2B;7fvQSjsAttpl1oM10N3TmpQU3ahTbfaRfafHL/7iUYhV1oM&#x2B;1Cm&#x2B;lq6M5JDWrSLrT5TrvQ5pP7dy/BKOxCm2kX2kxXQ3dOalCTdqHNd9qFNp/cv3sJRmEX2ky70Ga6GrpzUoOatAttvtMutPnk/t1LMAq70GbahTbT1dCdkxrUpF1o8512oc0n9&#x2B;9eglHYhTbTLrSZrobunNSgJu1Cm&#x2B;&#x2B;0C22exMMfa1CTdqHNO2lQkxrUXMkutJka2zcc/lSDmrQLbd5Jg5rUoOZKdqHN1Ni&#x2B;4fCnGtSkXWjzThrUpAY1V7ILbabG9g2HP9WgJu1Cm3fSoCY1qLmSXWgzNbZvOPypBjVpF9q8kwY1qUHNlexCm6mxfcPhTzWoSbvQ5p00qEkNaq5kF9pMje0bDn&#x2B;qQU3ahTbvpEFNalBzJbvQZmps33D4Uw1q0i60eScNalKDmivZhTZTo/&#x2B;CL4d&#x2B;tLQLbVY0qJl0NXRnRYOakvvOPwv&#x2B;KGEX2qxoUDPpaujOigY1Jfedfxb8UcIutFnRoGbS1dCdFQ1qSu47/yz4o4RdaLOiQc2kq6E7KxrUlNx3/lnwRwm70GZFg5pJV0N3VjSoKbnv/LPgjxJ2oc2KBjWTroburGhQU3Lf&#x2B;WfBHyXsQpsVDWomXQ3dWdGgpuS&#x2B;88&#x2B;CP0rYhTYrGtRMuhq6s6JBTVE8vI1daLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHr6LXWizokFNxS60mRrUpAY1qUFN2mXb4OG72IU2KxrUVOxCm6lBTWpQkxrUpF22DR6&#x2B;i11os6JBTcUutJka1KQGNalBTdpl2&#x2B;Dhu9iFNisa1FTsQpupQU1qUJMa1KRdtg0evotdaLOiQU3FLrSZGtSkBjWpQU3aZdvg4bvYhTYrGtRU7EKbqUFNalCTGtSkXbYNHn54dejNaRfarGhQk66G7kxXQ3emBjUVje0bDh9eHXpz2oU2KxrUpKuhO9PV0J2pQU1FY/uGw4dXh96cdqHNigY16WroznQ1dGdqUFPR2L7h8OHVoTenXWizokFNuhq6M10N3Zka1FQ0tm84fHh16M1pF9qsaFCTrobuTFdDd6YGNRWN7RsOH14denPahTYrGtSkq6E709XQnalBTUVj&#x2B;4bDh1eH3px2oc2KBjXpaujOdDV0Z2pQU9HYvuHw4dWhN6ddaLOiQU26GrozXQ3dmRrUVDS2bzh8aFAzqUFN2oU20y60mXahzW/SoKaisX3D4UODmkkNatIutJl2oc20C21&#x2B;kwY1FY3tGw4fGtRMalCTdqHNtAttpl1o85s0qKlobN9w&#x2B;NCgZlKDmrQLbaZdaDPtQpvfpEFNRWP7hsOHBjWTGtSkXWgz7UKbaRfa/CYNaioa2zccPjSomdSgJu1Cm2kX2ky70OY3aVBT0di&#x2B;4fChQc2kBjVpF9pMu9Bm2oU2v0mDmorG9g2HDw1qJjWoSbvQZtqFNtMutPlNGtRUNLZvOHxoUDOpQU1qUJMa1KQGNd&#x2B;kQU1qUDOpsX3D4UODmkkNalKDmtSgJjWo&#x2B;SYNalKDmkmN7RsOHxrUTGpQkxrUpAY1qUHNN2lQkxrUTGps33D40KBmUoOa1KAmNahJDWq&#x2B;SYOa1KBmUmP7hsOHBjWTGtSkBjWpQU1qUPNNGtSkBjWTGts3HD40qJnUoCY1qEkNalKDmm/SoCY1qJnU2L7h8KFBzaQGNalBTWpQkxrUfJMGNalBzaTG9g2HDw1qJjWoSQ1qUoOa1KDmmzSoSQ1qJjW2bzh8aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4aFAzqUFN2oU206tDb65oUDPpaujOk3h4eHXozWkX2qy4GrqzokHNlexCmyfx8PDq0JvTLrRZcTV0Z0WDmivZhTZP4uHh1aE3p11os&#x2B;Jq6M6KBjVXsgttnsTDw6tDb0670GbF1dCdFQ1qrmQX2jyJh4dXh96cdqHNiquhOysa1FzJLrR5Eg8Prw69Oe1CmxVXQ3dWNKi5kl1o8yQeHl4denPahTYrroburGhQcyW70OZJPDy8OvTmtAttVlwN3VnRoOZKdqHNk3h4G7vQZtqFNlODmtSgJl0N3Zka1KQGNSfx8DZ2oc20C22mBjWpQU26GrozNahJDWpO4uFt7EKbaRfaTA1qUoOadDV0Z2pQkxrUnMTD29iFNtMutJka1KQGNelq6M7UoCY1qDmJh7exC22mXWgzNahJDWrS1dCdqUFNalBzEg9vYxfaTLvQZmpQkxrUpKuhO1ODmtSg5iQe3sYutJl2oc3UoCY1qElXQ3emBjWpQc1JPLyNXWgz7UKbqUFNalCTrobuTA1qUoOa//uf//4PH72d67VqytUAAAAASUVORK5CYII=";
		  $base64_string=html_entity_decode($base64_string);
		//echo $base64_string;exit;
        $ifp = fopen( $output_file, 'wb' ); 
		
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        $file = public_path('prescriptions/'). $output_file;
        file_put_contents($file, $ifp);
        rename($output_file, 'prescriptions/'.$output_file);
        // clean up the file resource
        fclose( $ifp ); 
        return $output_file; 
    }
	/**
      @OA\Post(
          path="/v3/addmedlistfromscript",
          tags={"MedList"},
          summary="Add to Medlist From Script",
          operationId="Add to Medlist From Script",
		  security={{"bearerAuth": {}} },
      
          
         @OA\Parameter(
              name="prescriptionuserid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), @OA\Parameter(
              name="prescriptionitemid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         
        @OA\Parameter(
              name="drugname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugstrength",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="drugquantity",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
         @OA\Parameter(
              name="repeats",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="referrence",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="PhotoID",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        @OA\Parameter(
              name="mimsimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="drugpack",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ), @OA\Parameter(
              name="itemschedule",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),       
        
          @OA\Parameter(
              name="script",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  
          @OA\Parameter(
              name="price",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
        @OA\Parameter(
              name="cmicode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  @OA\Parameter(
              name="drugform",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  
		   
        
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function addmedlistfromscript(Request $request)
	{
		$medicationdetails = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=',$request->prescriptionitemid)
															->where('prescriptionitemdetails.prescriptionuserid' ,'=', $request->prescriptionuserid)
															->where('prescriptionitemdetails.profileid', '=',$request->profileid)															
															->where('prescriptionitemdetails.prescriptionid', '=',\DB::raw('"'.$request->prescriptionid.'"'))															
															->update(['prescriptionitemdetails.ismedlist' => 1,'prescriptionitemdetails.drugname'=>$request->drugname,'prescriptionitemdetails.drugform'=>$request->drugform,'prescriptionitemdetails.drugstrength'=>$request->drugstrength,'prescriptionitemdetails.drugquantity'=>$request->drugquantity,'prescriptionitemdetails.drugpack'=>$request->drugpack,'prescriptionitemdetails.price'=>$request->price,'prescriptionitemdetails.medimage'=>$request->PhotoID,'prescriptionitemdetails.mimsimage'=>$request->mimsimage,'prescriptionitemdetails.cmicode'=>$request->cmicode,'prescriptionitemdetails.itemschedule'=>$request->itemschedule]);
		 
		$success['message']= trans('messages.add_medlist');
		$success['medicationdetails'] = $medicationdetails;	 
        return response()->json(['success' => $success] ,$this->successStatus);
	}
	
	/**
    *  @OA\Post(
    *     path="/v3/scriptsearch",
    *      tags={"Scripts"},
    *      summary="Script medication search",
    *      operationId="Script medication search",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="drugname",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugstrength",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugform",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),,@OA\Parameter(
    *          name="drugrepeats",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),,@OA\Parameter(
    *          name="drugscriptID",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),,@OA\Parameter(
    *          name="drugqty",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*		@OA\Parameter(
    *          name="userid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*  @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function scriptsearch(Request $request)
	{
		try{
        $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($request->userid);
        $healthprofile = Healthprofile::where('userid', $request->userid)->get();
        $user_details=DB::table('users')->where('userid',$request->userid)->get();
        $business = Locations :: where('locationid', $request->locationid)->get();    
			$type=1;
			if(isset($request->type)){
				$type=$request->type;
			}
        $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.OriginatingItemCode,C.MIMsCMI as cmi,CS.StandardPrice,CS.medicare,CS.Con_Pens_ConSafty,CS.SafetyNet,CS.Veterans,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode limit 0,1) as image from catalouge_master as C   LEFT JOIN cataloguescriptpricing as CS ON C.MedmateItemCode = CS.medmateItemcode where  (`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=".$request->locationid." and CS.Pricing_Tier=".$business[0]['ScriptPricingTier']."  and LOWER(C.itemNameDisplay) LIKE '".strtolower($request->drugname) ."%' and   LOWER(C.Strength) LIKE '%".strtolower($request->drugstrength)."%'   and   C.units_per_pack = '".$request->drugqty."') OR (`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=0  and CS.Pricing_Tier=".$business[0]['ScriptPricingTier']."  and LOWER(C.itemNameDisplay) LIKE '".strtolower($request->drugname) ."%' and   LOWER(C.Strength) LIKE '%".strtolower($request->drugstrength)."%'   and   C.units_per_pack = '".$request->drugqty."')");        
	   if(count($catalouge_data)==0)
	   {
		$catalouge_data = DB::select("select CK.Item_knownas as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.OriginatingItemCode,C.MIMsCMI as cmi,CS.StandardPrice,CS.medicare,CS.Con_Pens_ConSafty,CS.SafetyNet,CS.Veterans,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where  prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode limit 0,1) as image from catalouge_master as C  LEFT JOIN cataloguescriptpricing as CS ON C.MedmateItemCode = CS.medmateItemcode left join catalouge_item_knownas as CK on C.MedmateItemCode=CK.MedmateItemCode where  (`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=".$request->locationid." and CS.Pricing_Tier=".$business[0]['ScriptPricingTier']." and LOWER(CK.Item_knownas) LIKE '".strtolower($request->drugname) ."%' and LOWER(CK.Strength) LIKE '%".strtolower($request->drugstrength)."%' and CK.units_per_pack = '".$request->drugqty."') OR (`CS`.`businessid` = ".$business[0]['ScriptCatalogueBusinessID']." and CS.location_id=0  and CS.Pricing_Tier=".$business[0]['ScriptPricingTier']."  and LOWER(CK.Item_knownas) LIKE '".strtolower($request->drugname) ."%' and LOWER(CK.Strength) LIKE '%".strtolower($request->drugstrength)."%' and CK.units_per_pack = '".$request->drugqty."')");
		 if(count($catalouge_data)==0){
		 $catalouge_data = DB::select(" select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.OriginatingItemCode,  C.MIMsCMI as cmi,CS.StandardPrice,CS.medicare,CS.Con_Pens_ConSafty,CS.SafetyNet,CS.Veterans,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,C.GST,  C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and formcode=C.MimsFormCode and packcode=C.MimsPackCode limit 0,1) as image from catalouge_master as C LEFT JOIN cataloguescriptpricing as CS ON C.MedmateItemCode = CS.medmateItemcode where CS.businessid=0 and CS.location_id=0 and CS.Pricing_Tier=".$business[0]['ScriptPricingTier']." and LOWER(C.itemNameDisplay) LIKE '".strtolower($request->drugname) ."%' and LOWER(C.Strength) LIKE '%".strtolower($request->drugstrength)."%' and C.units_per_pack = '".$request->drugqty."'");          
	  // print_r($catalouge_data);exit;
	   if(count($catalouge_data)==0){
		   $catalouge_data = DB::select("select CK.Item_knownas as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.OriginatingItemCode,C.MIMsCMI as cmi,CS.StandardPrice,CS.medicare,CS.Con_Pens_ConSafty,CS.SafetyNet,CS.Veterans,CS.Generic_StandardPrice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where  prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode limit 0,1) as image from catalouge_master as C  LEFT JOIN cataloguescriptpricing as CS ON C.MedmateItemCode = CS.medmateItemcode left join catalouge_item_knownas as CK on C.MedmateItemCode=CK.MedmateItemCode   where CS.businessid=0 and CS.location_id=0 and CS.Pricing_Tier=".$business[0]['ScriptPricingTier']." and LOWER(CK.Item_knownas) LIKE '".strtolower($request->drugname) ."%' and LOWER(CK.Strength) LIKE '%".strtolower($request->drugstrength)."%' and CK.units_per_pack = '".$request->drugqty."' ");
	   }
	   }
	   }
	   $mims_data = DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode` left join packitemimages as PI on P.prodcode=PI.prodcode where P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode and `P`.`ActualProduct` LIKE '".$request->drugname."%'");
		
	 // if(empty($catalouge_data) || count($catalouge_data)==0){
		 
	   $drug_details=array();
        $price_data=array();
        for($i=0;$i<count($catalouge_data);$i++){
			if($catalouge_data[$i]->PhotoID!=''||$catalouge_data[$i]->PhotoID!=null){
				$catalouge_data[$i]->image_url=env('APP_URL').'catalogue/'.$catalouge_data[$i]->PhotoID.'.jpeg';				
			}elseif($catalouge_data[$i]->image!=null || $catalouge_data[$i]->image){
				$catalouge_data[$i]->image_url=env('APP_URL').'mimsimages/'.$catalouge_data[$i]->image.'.bmp';
			}else{
				$catalouge_data[$i]->image_url='';
			}
			$catalouge_data[$i]->reference="Catalouge";
			$catalouge_data[$i]->showgeneric=0;
			if($catalouge_data[$i]->Generic_StandardPrice!=null){
				$catalouge_data[$i]->generic_drug="Generic of ".$catalouge_data[$i]->drug;
				$catalouge_data[$i]->showgeneric=1;
			}
			/*if($catalouge_data[$i]->OriginatingItemCode!=''){
				$genericinfo=DB::SELECT("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode limit 0,1) as image from catalouge_master as C where C.MedmateItemCode='".$catalouge_data[$i]->OriginatingItemCode."'");
				$catalouge_data[$i]->generic_drug=$genericinfo[0]->drug;
				$catalouge_data[$i]->showgeneric=1;
				if($genericinfo[0]->PhotoID!=''||$genericinfo[0]->PhotoID!=null){
				$catalouge_data[$i]->generic_image_url=env('APP_URL').'catalogue/'.$generic[0]->PhotoID.'.jpeg';				
			}elseif($generic[0]->image!=null || $genericinfo[0]->image){
				$catalouge_data[$i]->generic_image_url=env('APP_URL').'mimsimages/'.$generic[0]->image.'.bmp';
			}else{
				$catalouge_data[$i]->generic_image_url='';
			}
			
			}else{
				$catalouge_data[$i]->generic_drug='';
				$catalouge_data[$i]->generic_image_url='';
			}*/
			 if(count($healthprofile)>0){
					if($catalouge_data[$i]->script == "OTC")
					{
						//echo "OTC";exit;
						  $p=$catalouge_data[$i]->StandardPrice;
						 $gst =(($catalouge_data[$i]->GST)*$p)/100;
						 $price =$p+ $gst;
					}
					else
					{						
                                
                                    $p=$catalouge_data[$i]->SafetyNet;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['safetyNet']=number_format(($price), 2, '.', '');
									if($catalouge_data[$i]->Generic_SafetyNet!=NULL){
									$p=$catalouge_data[$i]->Generic_SafetyNet;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Generic_safetyNet']=number_format(($price), 2, '.', '');
									}else{
										$price_data['Generic_safetyNet']='';
									}
                                
									$p=$catalouge_data[$i]->Con_Pens_ConSafty;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Concession']=number_format(($price), 2, '.', '');
									if($catalouge_data[$i]->Generic_Con_Pens_ConSafty!=NULL){
									$p=$catalouge_data[$i]->Generic_Con_Pens_ConSafty;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Generic_Concession']=number_format(($price), 2, '.', '');
									}else{
										$price_data['Generic_Concession']='';
									}

									
									$p=$catalouge_data[$i]->Veterans;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Concession']=number_format(($price), 2, '.', '');
									if($catalouge_data[$i]->Generic_Veterans!=NULL){
									$p=$catalouge_data[$i]->Generic_Veterans;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Generic_Concession']=number_format(($price), 2, '.', '');
									}else{
										$price_data['Generic_Concession']='';
									}

                                
								   $p=$catalouge_data[$i]->medicare;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Medicare']=number_format(($price), 2, '.', '');
									if($catalouge_data[$i]->Generic_Medicare!=NULL){
									$p=$catalouge_data[$i]->Generic_Medicare;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Generic_Medicare']=number_format(($price), 2, '.', '');
									}else{
										$price_data['Generic_Medicare']='';
									}

                                
								   $p=$catalouge_data[$i]->StandardPrice;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Private']=number_format(($price), 2, '.', '');
									if($catalouge_data[$i]->Generic_StandardPrice!=NULL){
									$p=$catalouge_data[$i]->Generic_StandardPrice;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Generic_Private']=number_format(($price), 2, '.', '');
									}else{
										$price_data['Generic_Private']='';
									}

                                
					}
                    }else{
						$p=$catalouge_data[$i]->StandardPrice;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Private']=number_format(($price), 2, '.', '');
									if($catalouge_data[$i]->Generic_StandardPrice!=NULL){
									$p=$catalouge_data[$i]->Generic_StandardPrice;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Generic_Private']=number_format(($price), 2, '.', '');
									}else{
										$price_data['Generic_Private']='';
									}
					}
           
			$catalouge_data[$i]->price =number_format(($price), 2, '.', '');
			$catalouge_data[$i]->price_data=$price_data;
           
            if($catalouge_data[$i]->script=='Prescription'){$catalouge_data[$i]->script="Yes";}else{$catalouge_data[$i]->script="No";}
            $drug_details[]=$catalouge_data[$i];
        }
         
		
        if(!empty($drug_details)){
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.succ_message');
        }else{
			$subject=env('ENV')." e-Script not matching item in catalog";
			
			if(strtolower(substr($request->drugscriptID,0,5))=='https')
				 {
					 $code = explode("/",$request->drugscriptID);
					 if($code[2] == "ausscripts.erx.com.au")
					{					
						$barcode = $code[4];
					}
					else
					{					
						
					if($code[3] == "scripts")
						$barcode = $code[4];
					else
						$barcode = $code[3];
					}
				 }
			
						
			if($type==1){
			$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Below Script is not matching in catalogue.</p>
        <p>
		 <span style="color: #1d9bd8;"> User Details</span>   <br>	
			User Name: '.$user_details[0]->firstname.'<br>
			Email : '.$user_details[0]->username.' <br>
         <span style="color: #1d9bd8;"> Item Details</span>   <br>		  
		Item Name : '.$request->drugname.' <br>
		strength  : '.$request->drugstrength.' <br>
		form      : '.$request->drugform.' <br>
		quantity  : '.$request->drugqty.' <br>
		repeats   : '.$request->drugrepeats.' <br>
		scriptID  : '.$barcode.' <br><br>
        </p>
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijya.talluri@roundsqr.net', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                  //echo 'Error:'.$e->getMessage();
                 // return;
               }
			   $subject=env('ENV')."  Medmate - e-Script with no price";
			// $subject="e-Script not matching item in catalog;	
			if(strtolower(substr($request->drugscriptID,0,5))=='https')
				 {
					 $code = explode("/",$request->drugscriptID);
					 if($code[2] == "ausscripts.erx.com.au")
					{					
						$barcode = $code[4];
					}
					else
					{					
						
					if($code[3] == "scripts")
						$barcode = $code[4];
					else
						$barcode = $code[3];
					}
				 }
			
						
				
			$body_message1 = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
		 <p>
		Dear valued Medmate customer,<br><br>

Our system has identified that we were unable to match a price to a prescription you have scanned.<br><br>

Our team is constantly updating the catalogue with new items and pricing.<br><br>

If you would like to order or obtain a price for this item, simply:<br><br>

Add the script to your cart<br>
Send the order to your pharmacy of choice<br>
Pharmacy will return a quote to you which will appear in the "Messages" folder<br>
If you are happy with the price, select "Proceed to Payment"<br>
Once payment has been made, order will be sent back to your pharmacy of choice for processing<br><br><br>
 

Please note: this is an automated message<br><br>
Regards<br>
Medmate Team
		</p>
		
       
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';
/*
 <p>
		Hello '.$user_details[0]->firstname.', <br> Thanks so much for scanning a script. The system has notified a Medmate pharmacist that the script did not match to a price. They will review and update the price in the system. We will come back to you shortly when this has been updated by the pharmacist.
        </p>
*/
			   $message['to']           = $user_details[0]->username;
               $message['subject']      = $subject;
               $message['body_message'] = $body_message1;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				 // ->bcc('imran.ansari@roundsqr.net', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
			}}
            $response->drug_details 	= $drug_details;
            $response->msg 		= trans('messages.no_data');  
        }
		$response->status 		= $this->successStatus;
		return json_encode($response);	
	 }catch(Exception $e) {
  $emessage=$e->getMessage();
  if(strtolower(substr($request->drugscriptID,0,5))=='https')
				 {
					 $code = explode("/",$request->drugscriptID);
					 if($code[2] == "ausscripts.erx.com.au")
					{					
						$barcode = $code[4];
					}
					else
					{					
						
					if($code[3] == "scripts")
						$barcode = $code[4];
					else
						$barcode = $code[3];
					}
				 }else{$barcode=$request->drugscriptID;}
  $subject=env('ENV')." Medmate Script Error";
$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Below Script is not matching in catalogue.</p>
        <p>
		 <span style="color: #1d9bd8;"> User Details</span>   <br>	
			User Name: '.$user_details[0]->firstname.'<br>
			Email : '.$user_details[0]->username.' <br>
         <span style="color: #1d9bd8;"> Item Details</span>   <br>		  
		Item Name :  <br>
		strength  :  <br>
		form      :  <br>
		quantity  :  <br>
		repeats   :  <br>
		scriptID  : '.$barcode.' <br>
		Error Response : '.$emessage.'<br><br>
        </p>
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijaytalluri88@gmail.com', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
}
        }
	/**
    *  @OA\Post(
    *     path="/v3/genescriptsearch",
    *      tags={"Scripts"},
    *      summary="Script medication search",
    *      operationId="Script medication search",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="drugname",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugstrength",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugform",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugrepeats",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugscriptID",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),@OA\Parameter(
    *          name="drugqty",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*		@OA\Parameter(
    *          name="userid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*      @OA\Parameter(
    *          name="genericuserchoice",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*      @OA\Parameter(
    *          name="prescriptionid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function genescriptsearch(Request $request)
	{
        $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($request->userid);
        $healthprofile = Healthprofile::where('userid', $request->userid)->get();
        $user_details=DB::table('users')->where('userid',$request->userid)->get();
		
		$userchoice = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', $request->prescriptionid)
														  ->where('prescriptionitemdetails.profileid', '=', $request->profileid)
															->update(['prescriptionitemdetails.genericuserchoice' => $request->genericuserchoice]);
                     
      
	  // where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$request->medilistid.'"'))
	  // $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price,C.Medicare,C.Con_Pens_Vet_ConSafty,C.SafetyNet,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.itemNameDisplay LIKE '".$request->drugname."%' and C.Strength LIKE '%".$request->drugstrength."%'");
	   
       //$catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price,C.Medicare,C.Con_Pens_Vet_ConSafty,C.SafetyNet,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode) as image from catalouge_master as C  where C.itemNameDisplay LIKE '".$request->drugname ."%' and C.Strength LIKE '%".$request->drugstrength."%' and C.units_per_pack = ".$request->drugqty." UNION select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price,C.Medicare,C.Con_Pens_Vet_ConSafty,C.SafetyNet,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.itemNameDisplay LIKE '".$request->drugname."%'");        
       $catalouge_data = DB::select("select CO.StandardPrice as OTCStandardprice,CS.Generic_Medicare,CS.Generic_Con_Pens_ConSafty,CS.Generic_Veterans,CS.Generic_SafetyNet,CS.Generic_StandardPrice as ScriptStandardprice,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode) as image from catalouge_master as C LEFT JOIN catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode LEFT JOIN cataloguescriptpricing as CS ON C.OriginatingItemCode = CS.medmateItemcode where C.itemNameDisplay LIKE '".$request->drugname ."%' and C.Strength LIKE '%".$request->drugstrength."%' and C.units_per_pack = ".$request->drugqty." ");        
	   
       $mims_data = DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode` left join packitemimages as PI on P.prodcode=PI.prodcode where P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode and `P`.`ActualProduct` LIKE '".$request->drugname."%'");
       $drug_details=array();
        $price_data=array();
        for($i=0;$i<count($catalouge_data);$i++){
			if($catalouge_data[$i]->PhotoID!=''||$catalouge_data[$i]->PhotoID!=null){
				$catalouge_data[$i]->image_url=env('APP_URL').'catalogue/'.$catalouge_data[$i]->PhotoID.'.jpg';				
			}elseif($catalouge_data[$i]->image!=null || $catalouge_data[$i]->image){
				$catalouge_data[$i]->image_url=env('APP_URL').'mimsimage/'.$catalouge_data[$i]->image.'.jpg';
			}else{
				$catalouge_data[$i]->image_url='';
			}
			$catalouge_data[$i]->reference="Catalouge";
			 if(count($healthprofile)>0){
					if($catalouge_data[$i]->script == "OTC")
					{
						//echo "OTC";exit;
						 $p=$catalouge_data[$i]->OTCStandardprice;
						 $gst =(($catalouge_data[$i]->GST)*$p)/100;
						 $price =$p+ $gst;
					}
					else
					{						
                                
                                    $p=$catalouge_data[$i]->Generic_SafetyNet;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['safetyNet']=number_format(($price), 2, '.', '');									
                                
									$p=$catalouge_data[$i]->Generic_Con_Pens_ConSafty;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Concession']=number_format(($price), 2, '.', '');
									
									$p=$catalouge_data[$i]->Generic_Veterans;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Veterans']=number_format(($price), 2, '.', '');
                                
								    $p=$catalouge_data[$i]->Generic_Medicare;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Medicare']=number_format(($price), 2, '.', '');
                                
								    $p=$catalouge_data[$i]->Generic_StandardPrice;
									$gst =(($catalouge_data[$i]->GST)*$p)/100;
									$price =$p+ $gst;
									$price_data['Private']=number_format(($price), 2, '.', '');
                                
					}
                    }
           // $p=(float)$catalouge_data[$i]->price;
			//$gst =(float)(($catalouge_data[$i]->GST)*$p)/100;
			$catalouge_data[$i]->price =number_format(($price), 2, '.', '');
			$catalouge_data[$i]->price_data=$price_data;
           // $catalouge_data[$i]->strength=$catalouge_data[$i]->active.$catalouge_data[$i]->active_units;
            if($catalouge_data[$i]->script=='Prescription'){$catalouge_data[$i]->script="Yes";}else{$catalouge_data[$i]->script="No";}
            $drug_details[]=$catalouge_data[$i];
        }
         /*for($i=0;$i<count($mims_data);$i++){
           // $mims_data[$i]->strength=$mims_data[$i]->active.$mims_data[$i]->active_units;
            $mims_data[$i]->reference="MIMS";
            $mims_data[$i]->ShortDescription=$mims_data[$i]->drug;
            $mims_data[$i]->price="";
            $drug_details[]=$mims_data[$i];
        }*/
        if(!empty($drug_details)){
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.succ_message');
        }else{
			$subject=env('ENV')." e-Script not matching item in catalog";
			// $subject="e-Script not matching item in catalog;	
			if(strtolower(substr($request->drugscriptID,0,5))=='https')
				 {
					 $code = explode("/",$request->drugscriptID);
					 if($code[2] == "ausscripts.erx.com.au")
					{					
						$barcode = $code[4];
					}
					else
					{					
						
					if($code[3] == "scripts")
						$barcode = $code[4];
					else
						$barcode = $code[3];
					}
				 }
			
						
				
			$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Below Script is not matching in catalogue.</p>
        <p>
		 <span style="color: #1d9bd8;"> User Details</span>   <br>	
			User Name: '.$user_details[0]->firstname.'<br>
			Email : '.$user_details[0]->username.' <br>
         <span style="color: #1d9bd8;"> Item Details</span>   <br>		  
		Item Name : '.$request->drugname.' <br>
		strength  : '.$request->drugstrength.' <br>
		form      : '.$request->drugform.' <br>
		quantity  : '.$request->drugqty.' <br>
		repeats   : '.$request->drugrepeats.' <br>
		scriptID  : '.$barcode.' <br><br>
        </p>
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijya.talluri@roundsqr.net', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                  //echo 'Error:'.$e->getMessage();
                 // return;
               }
			   $subject=env('ENV')."  Medmate - e-Script with no price";
			// $subject="e-Script not matching item in catalog;	
			if(strtolower(substr($request->drugscriptID,0,5))=='https')
				 {
					 $code = explode("/",$request->drugscriptID);
					 if($code[2] == "ausscripts.erx.com.au")
					{					
						$barcode = $code[4];
					}
					else
					{					
						
					if($code[3] == "scripts")
						$barcode = $code[4];
					else
						$barcode = $code[3];
					}
				 }
			
						
				
			$body_message1 = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
		 <p>
		Dear valued Medmate customer,<br><br>

Our system has identified that we were unable to match a price to a prescription you have scanned.<br><br>

Our team is constantly updating the catalogue with new items and pricing.<br><br>

If you would like to order or obtain a price for this item, simply:<br><br>

Add the script to your cart<br>
Send the order to your pharmacy of choice<br>
Pharmacy will return a quote to you which will appear in the "Messages" folder<br>
If you are happy with the price, select "Proceed to Payment"<br>
Once payment has been made, order will be sent back to your pharmacy of pharmacy for processing<br><br><br>
 

Please note: this is an automated message<br><br>
Regards<br>
Medmate Team
		</p>
		
       
        
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';
/*
 <p>
		Hello '.$user_details[0]->firstname.', <br> Thanks so much for scanning a script. The system has notified a Medmate pharmacist that the script did not match to a price. They will review and update the price in the system. We will come back to you shortly when this has been updated by the pharmacist.
        </p>
*/
			   $message['to']           = $user_details[0]->username;
               $message['subject']      = $subject;
               $message['body_message'] = $body_message1;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				 // ->bcc('imran.ansari@roundsqr.net', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
            $response->drug_details 	= $drug_details;
            $response->msg 		= trans('messages.no_data');  
        }
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        }
}
