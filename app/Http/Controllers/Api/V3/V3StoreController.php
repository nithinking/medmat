<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Prescriptionitemdetails; 
use App\models\Healthprofile; 
use App\models\Business;
use App\models\Locations;
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class V3StoreController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/storeitems",
    *      tags={"Store Items"},
    *      summary="Store Items",
    *      operationId="storeitems",
    *	  security={{"bearerAuth": {}} },
    *     @OA\Parameter(
    *          name="category",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*    @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
	*	  @OA\Parameter(
    *          name="limit",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	  		      
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function storeitems(Request $request)
	{
        $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
        $healthprofile = Healthprofile::where('userid', $user->userid)->get();
        $category='';
        //$limit='10';
		//and C.ViewOrder is Not Null
		$business = Locations :: where('locationid',$request->locationid)->get();
		//echo $business[0]['businessid'];exit;
		$limit=" and C.ViewOrder is Not Null order by C.ViewOrder asc limit 10";
        $store_procedure="store_items";
		if(isset($request->category)){
            $category=" and SC.categoryid='".$request->category."'"; 
			$limit="";
			$store_procedure="store_items_by_category";
        }
             
	   $drug_details=array();
	   
       $category_data=array();
      
      $categories=DB::select("select DISTINCT SC.categoryid,SC.CategoryLv1,SC.vieworder from catalouge_category_master as SC where SC.CategoryLv1!='Prescriptions'".$category." order by SC.vieworder");
	
	 for($i=0;$i<count($categories);$i++){
		  if($categories[$i]->categoryid=='10'){			 
			 
			 // $catalouge_data[$categories[$i]->CategoryLv1] = DB::select("select CO.StandardPrice as price,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.ShortDescription,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.ShortDescription,C.GST,C.ViewOrder,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C left join  catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode where C.itemCommodityClass ='OTC' and C.ItemStatus=1 and (CO.businessid=".$business[0]['businessid']." and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.CategoryLv1 like '%".$categories[$i]->categoryid."%') or (CO.location_id=".$request->locationid." and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.CategoryLv1 like '%".$categories[$i]->categoryid."%') ".$limit); 
				$catalouge_data[$categories[$i]->CategoryLv1]=DB::select("CALL  ".$store_procedure."(".$business[0]['OTCPricingTier'].", ".$business[0]['OTCCatalogueBusinessID'].",".$business[0]['locationid'].",".$categories[$i]->categoryid.")");
				if(count($catalouge_data[$categories[$i]->CategoryLv1]) == 0)
			  {
			  //$catalouge_data[$categories[$i]->CategoryLv1] = DB::select("select CO.StandardPrice as price,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode, C.ShortDescription,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.ShortDescription,  C.GST,C.ViewOrder,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C   left join  catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode    where C.itemCommodityClass ='OTC' and C.ItemStatus=1 and CO.businessid=0 and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.CategoryLv1 like '%".$categories[$i]->categoryid."%'".$limit); 
			  $catalouge_data[$categories[$i]->CategoryLv1]=DB::select("CALL  ".$store_procedure."(".$business[0]['OTCPricingTier'].", 0,null,".$categories[$i]->categoryid.")");
			  
}
		  }else{
			//$catalouge_data[$categories[$i]->CategoryLv1] = DB::select("select CO.StandardPrice as price,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.ShortDescription,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.ShortDescription,C.GST,C.ViewOrder,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C left join  catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode where C.itemCommodityClass ='OTC' and C.ItemStatus=1 and (CO.businessid=".$business[0]['businessid']." and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.CategoryLv1 like '%".$categories[$i]->categoryid."%') or (CO.location_id=".$request->locationid." and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.CategoryLv1 like '%".$categories[$i]->categoryid."%') ".$limit); 
			 $catalouge_data[$categories[$i]->CategoryLv1]=DB::select("CALL  ".$store_procedure."(".$business[0]['OTCPricingTier'].", ".$business[0]['OTCCatalogueBusinessID'].",".$business[0]['locationid'].",".$categories[$i]->categoryid.")"); 
			  if(count($catalouge_data[$categories[$i]->CategoryLv1]) == 0)
			  {
			  //$catalouge_data[$categories[$i]->CategoryLv1] = DB::select("select CO.StandardPrice as price,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode, C.ShortDescription,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.ShortDescription,  C.GST,C.ViewOrder,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C   left join  catalogueotcpricing as CO ON C.MedmateItemCode = CO.medmateItemcode    where C.itemCommodityClass ='OTC' and C.ItemStatus=1 and CO.businessid=0 and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.CategoryLv1 like '%".$categories[$i]->categoryid."%'".$limit); 
			  $catalouge_data[$categories[$i]->CategoryLv1]=DB::select("CALL  ".$store_procedure."(".$business[0]['OTCPricingTier'].", 0,null,".$categories[$i]->categoryid.")");
			  }  
          
		  }
		
                 for($j=0;$j<count($catalouge_data[$categories[$i]->CategoryLv1]);$j++){
            $catalouge_data[$categories[$i]->CategoryLv1][$j]->reference="Catalouge";
            $p=(float)$catalouge_data[$categories[$i]->CategoryLv1][$j]->price;
							$gst =(float)(($catalouge_data[$categories[$i]->CategoryLv1][$j]->GST)*$p)/100;
							$catalouge_data[$categories[$i]->CategoryLv1][$j]->price =number_format(($p+ $gst), 2, '.', '');
			if(isset($catalouge_data[$categories[$i]->CategoryLv1][$j]->PhotoID)){
				$catalouge_data[$categories[$i]->CategoryLv1][$j]->PhotoID="https://pharmacy.medmate.com.au/catalouge/".$catalouge_data[$categories[$i]->CategoryLv1][$j]->PhotoID.".jpg";
			}else if(isset($catalouge_data[$categories[$i]->CategoryLv1][$j]->image)){
				$catalouge_data[$categories[$i]->CategoryLv1][$j]->image="https://pharmacy.medmate.com.au/mimsimages/".$catalouge_data[$categories[$i]->CategoryLv1][$j]->image.".jpg";
			}
           // $catalouge_data[$i]->strength=$catalouge_data[$i]->active.$catalouge_data[$i]->active_units;
            if($catalouge_data[$categories[$i]->CategoryLv1][$j]->script=='Prescription'){$catalouge_data[$categories[$i]->CategoryLv1][$j]->script="Yes";}else{$catalouge_data[$categories[$i]->CategoryLv1][$j]->script="No";}
            }
			$drug_details[$i] = new \stdClass(); 
			$drug_details[$i]->categoryid=$categories[$i]->categoryid;
			$drug_details[$i]->name=$categories[$i]->CategoryLv1;
			$drug_details[$i]->values=$catalouge_data[$categories[$i]->CategoryLv1];
           
    }
    
        if(!empty($drug_details)){
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.succ_message');
        }else{
            $response->drug_details 	= $drug_details;
            $response->msg 		=trans('messages.no_data');  
        }
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        
   }
   /**
    *  @OA\Post(
    *     path="/v2/storesearch",
    *      tags={"Store Items"},
    *      summary="Store Items",
    *      operationId="storesearch",
    *	  security={{"bearerAuth": {}} },
    *     @OA\Parameter(
    *          name="medication",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*    @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
	*	  		      
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
   public function storesearch(Request $request){   
    $response = (object)array();
    $user = Auth::user();
    $user_details = User::findOrFail($user->userid);
    $healthprofile = Healthprofile::where('userid', $user->userid)->get();
   $business = Locations :: where('locationid',$request->locationid)->get();
   $catalouge_data = DB::select("select CO.StandardPrice as price,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C  left join  catalogueotcpricing as CO ON C.MedmateItemCode = CO.MedmateItemCode where C.ItemCommodityClass='OTC' and C.ItemStatus=1 and (CO.location_id=".$request->locationid." and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.itemNameDisplay LIKE '%".$request->medication."%') or  (CO.businessid=".$business[0]['businessid']." and CO.Pricing_Tier=".$business[0]['OTCPricingTier']." and C.itemNameDisplay LIKE '%".$request->medication."%') "); 
   if(count($catalouge_data)==0)
   {
	$catalouge_data = DB::select("select CO.StandardPrice as price,C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.GST,C.ShortDescription,C.ItemSchedule,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  limit 0,1) as image from catalouge_master as C  left join  catalogueotcpricing as CO ON C.MedmateItemCode = CO.MedmateItemCode where C.ItemCommodityClass='OTC' and C.ItemStatus=1 and CO.businessid=0  and C.itemNameDisplay LIKE '%".$request->medication."%' and  CO.Pricing_Tier=".$business[0]['OTCPricingTier']." "); 
   }  
   $drug_details=array();
    
    for($i=0;$i<count($catalouge_data);$i++){
        $catalouge_data[$i]->reference="Catalouge";
        $p=(float)$catalouge_data[$i]->price;
							$gst =(float)(($catalouge_data[$i]->GST)*$p)/100;
							$catalouge_data[$i]->price =number_format(($p+ $gst), 2, '.', '');
			
		if(isset($catalouge_data[$i]->PhotoID)){
				$catalouge_data[$i]->PhotoID="https://pharmacy.medmate.com.au/catalouge/".$catalouge_data[$i]->PhotoID.".jpg";
			}else if(isset($catalouge_data[$i]->image)){
				$catalouge_data[$i]->image="https://pharmacy.medmate.com.au/mimsimages/".$catalouge_data[$i]->image.".jpg";
			}
       // $catalouge_data[$i]->strength=$catalouge_data[$i]->active.$catalouge_data[$i]->active_units;
        if($catalouge_data[$i]->script=='Prescription'){$catalouge_data[$i]->script="Yes";}else{$catalouge_data[$i]->script="No";}
        $drug_details[]=$catalouge_data[$i];
    }
    if(!empty($drug_details)){
    $response->drug_details 	= $drug_details;
    $response->msg 		= trans('messages.succ_message');
    }else{
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.no_data');  
    }
    $response->status 		= $this->successStatus;
    return json_encode($response);	
    }

   
}