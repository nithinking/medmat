<?php

namespace App\Http\Controllers\Api\V3;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Orderdetails; 
use App\models\Basket; 
use App\models\Cart; 
use App\models\Ordertracking; 
use App\models\Orderinvoicelist; 
use App\models\Invoice; 
use App\models\Orderbasketlist; 
use App\models\Basketitemlist; 
use App\models\Prescriptiondetails; 
use App\models\Prescriptionitemdetails; 
use App\models\Doordashlogs; 
use App\models\Tokenswallet; 
use App\models\Orderprescriptionitemdetails; 
use App\models\Healthprofile; 
use App\models\Deliveryaddress; 
use App\models\Locations; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class V3PlaceorderController extends Controller
{
    public $successStatus = 200;
	/**
      @OA\Post(
          path="/v3/placeOrder",
          tags={"place Order"},
          summary="place Order",
          operationId="place Order",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="ordertype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="userpreffereddate",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="userprefferedtime",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="orderinstructions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ), 
		  @OA\Parameter(
              name="deliveryinstructions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ), 
		  @OA\Parameter(
              name="orderTotal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="deliveryfee",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="isGeneric",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),
		 @OA\Parameter(
              name="speakwithpharmacist",
              in="query",
              required=true,
              @OA\Schema(
                  type="integer"
              )
          ),
		  
		  @OA\Parameter(
              name="deliveryaddressid",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),
		  	
		  	  @OA\Parameter(
              name="itemimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),
		  
		  	@OA\Parameter(
              name="usermobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),  @OA\Parameter(
              name="estimateid",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),  @OA\Parameter(
              name="deliverypartnername",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  
         
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
    public function placeOrder(Request $request)
	{
		
		//print_r($request);exit;
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get();
		//$cart = Cart :: where ('enduserid', $user->userid)->where('cartstatus', '1')->get();
		//$noofrows = $cart->count();
				
		$cart =$request->cartitems;
        $locationname = Locations :: where('locationid','=',$request->locationid)->get();
		//print_r($locationname[0]['timezone']);exit;
		date_default_timezone_set($locationname[0]['timezone']);
		$todaydate = date("Y-m-d\TH:i:s\Z");
		$cart_itemschedule = DB::select("select * from cart where enduserid=".$user->userid." and cartstatus=1 and type=1 and itemschedule LIKE 's%'"); 
		if(isset($request->type)&&$request->type==2){
			$cart_itemschedule = DB::select("select * from cart where enduserid=".$user->userid." and cartstatus=1 and type=2 and itemschedule LIKE 's%'"); 
		}
		$itemschedulecount = count($cart_itemschedule);
		//Test DD API KEY
		//"Authorization: Bearer 22590509529bb2aa58e00387ef617843179e8eee",
			$orderDetails = new Orderdetails;
						
			$orderDetails->uniqueorderid = mt_rand(100000, 999999).$user->userid;
			$orderDetails->enduserid = $user->userid;
			$orderDetails->locationid = $request->locationid;
			$orderDetails->deliveryaddressid = $request->deliveryaddressid;
			$orderDetails->orderinstructions = $request->orderinstructions;
			$orderDetails->type=($request->type!='')?$request->type:'1';
			if($itemschedulecount > 0)
			{
				$orderDetails->deliveryinstructions = "Cannot be left unattended ".$request->deliveryinstructions;
			}
			else{
				$orderDetails->deliveryinstructions = "Attempt to hand deliver ".$request->deliveryinstructions;
			}
			
			$orderDetails->pickupinstructions = $locationname[0]['pickupinstruction'];
			if($request->deliverypartnername === "DoorDash")
			{
				$orderDetails->estimateid = $request->estimateid;
			}
			else
			{
				$request->estimateid = "";
				$orderDetails->estimateid = "";
			}	
			$orderDetails->deliverypartnername = $request->deliverypartnername;
			$orderDetails->isGeneric = $request->isGeneric;			
			$orderDetails->orderTotal = $request->orderTotal;
			$orderDetails->speakwithpharmacist = $request->speakwithpharmacist;
			$orderDetails->orderDate = date('Y-m-d H:i:s');			
			$orderDetails->ordertype = $request->ordertype;			
			$orderDetails->deliveryfee = $request->deliveryfee;
			
			$orderDetails->userpreffereddate = $request->userpreffereddate;
			$orderDetails->userprefferedtime = $request->userprefferedtime;
			$orderDetails->isnew = 1;
			
			$deliverypartner = DB::select("select * from deliverypartner where locationid=".$request->locationid." and partnerstatus=1"); 
			if($request->deliveryaddressid !="" && $request->estimateid!="")
			{
				$ordersts = "99";
				$orderDetails->orderstatus = $ordersts;
				
				$deleiveryaddress = Deliveryaddress :: where('deliveryaddressid',  $request->deliveryaddressid)->get();
				$orderDetails->save();
			}
			elseif($request->Istbc==0){
				$ordersts = "Pending Payment";
				$orderDetails->orderstatus = $ordersts;
				$orderDetails->save();
			}
			else
			{
				$ordersts = "Waiting For Review";
				$orderDetails->orderstatus = $ordersts;
				$orderDetails->save();
			}
			
			//LastsearchAddress
			// if($request->ordertype=="Delivery" && $request->deliveryaddressid!="")
			// {
				// $deliveryaddress = Deliveryaddress:: where('deliveryaddresses.userid', '=', $user->userid)													
													// ->update(['deliveryaddresses.currentselectaddress' => '0']);
													
				// $deliveryaddresses = Deliveryaddress:: where('deliveryaddresses.userid', '=', $user->userid)
													// ->where('deliveryaddresses.deliveryaddressid', '=', $request->deliveryaddressid )
													// ->update(['deliveryaddresses.currentselectaddress' => '1']);
			// }
			//$userlocation = User :: where('userid', $user->userid)->update(['locationid' => $request->locationid]);
			$orderid = $orderDetails->id;
			$uniqueorderid = $orderDetails->uniqueorderid;
			
			if($request->deliverypartnername === "DoorDash" && $ordersts === "99")
			{
				
					$pickupaddress = Locations :: where('locationid',$request->locationid)->get();
					$pickupcity = 	$pickupaddress[0]['suburb'];	
					$pickupstate =  $pickupaddress[0]['state'];
					$pickupstreet =  $pickupaddress[0]['streetaddress'];
					$pickuppostcode =  $pickupaddress[0]['postcode'];
					
					$deleiverycity = $deleiveryaddress[0]['suburb'];
					$deleiverystate = $deleiveryaddress[0]['state'];
					$deleiverystreet = $deleiveryaddress[0]['addressline1'];
					$deleiverypostcode = $deleiveryaddress[0]['postcode'];					
					$orderamount = str_replace(".","",number_format($request->orderTotal, 2, '.', ''));
					$mobile = '+61'.substr(str_replace(" ","",$pickupaddress[0]['phone']),1);
					$cmobile = '+61'.substr($request->usermobile,1);
					//echo $orderamount;exit;
					if($user_details[0]['lastname']!="")
					{
					$lastname = $user_details[0]['lastname'];
					}
					else
					$lastname = $user_details[0]['firstname'];
					$utctime = gmdate("Y-m-d\TH:i:s\Z");					
					//$utctime = $todaydate;					
					
					// "pickup_address": {
							// "city": "hawthorn",
							// "state": "Victoria",
							// "street": "174 power street",
							// "unit": "10",
							// "zip_code": "3122"
							// },
					
					$curl = curl_init();
					$vars = '{
							"pickup_address": {
						"city": "'.$pickupcity.'",
						"state": "'.$pickupstate.'",
						"street": "'.$pickupstreet.'",
						"unit": "",
						"zip_code": "'.$pickuppostcode.'"
						},  
						  "pickup_phone_number": "'.$mobile.'",
						  "pickup_instructions": "'.$locationname[0]['pickupinstruction'].'",
						  "dropoff_address": {
						"city": "'.$deleiverycity.'",
						"state": "'.$deleiverystate.'",
						"street": "'.$deleiverystreet.'",
						"unit": "",
						"zip_code": "'.$deleiverypostcode.'"
						},
						  "customer": {
							"phone_number": "'.$cmobile.'",
							"first_name": "'.$user_details[0]['firstname'].'",
							"last_name": "'.$lastname.'",
							"email": "'.$user_details[0]['username'].'",
							"should_send_notifications": false
						  },
						  "dropoff_instructions": "'.$request->deliveryinstructions.'",
						  "order_value": "'.$orderamount.'",
						  "tip": 0,
						  "delivery_time": "'.$utctime.'",
						  "external_delivery_id": "'.$orderid.'",
						  "contains_alcohol": false,
						  "barcode_scanning_required": false,
						  "num_items": "'.count($cart).'",
						  "external_store_id": "'.$request->locationid.'",
						  "external_business_name": "Medmate",
						  "signature_required": true
						}';
						
						//print_r($vars);
					curl_setopt_array($curl, array(
					  CURLOPT_URL => env('DOORDASH_VALIDATION_URL'),
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS =>$vars,
					  CURLOPT_HTTPHEADER => array(
						"Authorization: Bearer ".env('DOORDASH_KEY'),
						"Content-Type: application/json",						
					  ),
					));

					$res = curl_exec($curl);

					curl_close($curl);
					$resval = json_decode($res);
					//print_r($resval);exit;
					//print_r($resval->valid);
			if(isset($resval->valid) && $resval->valid == 1)
			{
				//echo "madhoo";exit;
				
				$orderstat = Orderdetails :: where('orderdetails.orderid', '=', $orderid)
                                            ->update(['orderdetails.orderstatus' => "Waiting For Review"]);
				$ordersts = "Waiting For Review";
			}
			elseif(isset($resval->field_errors) && $resval->field_errors != "")
			{
				$response->msg 		= trans('messages.doordasherrmsg');
				$response->status 		= $this->successStatus;
				return json_encode($response);
			}
			
			$Doordashlogs=new Doordashlogs;
			
			$Doordashlogs->doordashapiurl=env('DOORDASH_VALIDATION_URL');
			$Doordashlogs->urltype="Validations";
			$Doordashlogs->uniqueorderid=$uniqueorderid;
			$Doordashlogs->orderid=$orderid;
			$Doordashlogs->pickupcity=$pickupcity;
			$Doordashlogs->pickupstate=$pickupstate;
			$Doordashlogs->pickupstreet=$pickupstreet;
			$Doordashlogs->pickupunit="";
			$Doordashlogs->pickupzip_code=$pickuppostcode;
			$Doordashlogs->pickup_phone_number=$mobile;
			$Doordashlogs->dropoffcity=$deleiverycity;
			$Doordashlogs->dropoffstate=$deleiverystate;
			$Doordashlogs->dropoffstreet=$deleiverystreet;
			$Doordashlogs->dropoffunit="";
			$Doordashlogs->dropoffzip_code=$deleiverypostcode;
			$Doordashlogs->customerphone_number=$cmobile;
			$Doordashlogs->customerbusiness_name="";
			$Doordashlogs->customerfirst_name=$user_details[0]['firstname'];
			$Doordashlogs->customerlast_name=$lastname;
			$Doordashlogs->customeremail=$user_details[0]['username'];
			$Doordashlogs->send_notifications="false";
			$Doordashlogs->order_value=$orderamount;
			$Doordashlogs->pickup_time=$utctime;
			$Doordashlogs->pickup_business_name="";
			$Doordashlogs->pickup_instructions=$locationname[0]['pickupinstruction'];
			$Doordashlogs->dropoff_instructions=$request->deliveryinstructions;
			$Doordashlogs->external_delivery_id=$uniqueorderid;
			$Doordashlogs->driver_reference_tag=$uniqueorderid;
			$Doordashlogs->external_store_id=$request->locationid;
			$Doordashlogs->contains_alcohol="false";
			$Doordashlogs->num_items=count($cart);
			$Doordashlogs->signature_required="";
			$Doordashlogs->allow_unattended_delivery="";	
			$Doordashlogs->jsonrequest=$vars;	
			//$Doordashlogs->jsonresponse=$resval;
			$Doordashlogs->save();
			//$Doordashlogs->storeData($input);
			}
			
			if(isset($request->usermobile) &&  $request->usermobile!= "")
            {
                $usermobileupdate = User :: where('users.userid', '=', $user->userid)
                                            ->update(['users.mobile' => $request->usermobile]);
            }
            
			
				
		if($orderid!="" && $ordersts!="99")
			{
			//date_default_timezone_set($locationname[0]['timezone']);
			date_default_timezone_set('UTC');
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $orderid;
			if($request->Istbc==0){
				$orderTracking->orderstatus = "Pending Payment";				
			}elseif(isset($request->type) && $request->type==2){
				$orderTracking->orderstatus = "Widget Order";
			}
			else{			
			$orderTracking->orderstatus = "Waiting For Review";
			}
			//$orderTracking->orderstatus= "Waiting For Review";
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();				
		for($i=0;$i<count($cart);$i++){
		//print_r($orderid);exit;
			
			$basket = new Basket;
			$basket->profileid = $cart[$i]['profileid'];
			$basket->orderid = $orderid;
			$basket->createddate = date('Y-m-d');;
			$basket->scriptid  = $cart[$i]['prescriptionid'];
			if(!empty($cart[$i]['referrencetable']))
			$basket->refferencetable  = $cart[$i]['referrencetable'];
		else
			$basket->refferencetable  ='';
			//$basket->catalogueid  = $cart[$i]['prescriptionid'];
			$basket->medlistid  = $cart[$i]['medlistid'];
			$basket->basketstatus  = 1;
			$basket->save();
			//print_r($basket);exit;
			$basketid = $basket->id;
			
			$basketitemlist = new Basketitemlist;
			$basketitemlist->basketid = $basketid;
			$basketitemlist->itemstatus = '1';
			$basketitemlist->save();			
			
			$invoice = new Invoice;
			$invoice->orderid = $orderid;
			$invoice->userid = $cart[$i]['userid'];
			$invoice->profileid = $cart[$i]['profileid'];			
			$invoice->invoicestatus = "1";			
			$invoice->save();				
			$invoiceid = $invoice->id;
												 
			$orderbasketlist = new Orderbasketlist;
			$orderbasketlist->orderid = $orderid;
			$orderbasketlist->userid = $cart[$i]['userid']; 
			$orderbasketlist->profileid = $cart[$i]['profileid'];
			$orderbasketlist->basketid  = $basketid;
			$orderbasketlist->locationid  = $request->locationid;
			$orderbasketlist->deliveryaddressid  = $request->deliveryaddressid;
			$orderbasketlist->prescriptionid   = $cart[$i]['prescriptionid'];
			$orderbasketlist->medlistid   = $cart[$i]['medlistid'];
			$orderbasketlist->drugname   = $cart[$i]['name'];
			$orderbasketlist->drugquantity   = $cart[$i]['quantity'];
			$orderbasketlist->drugpack   = $cart[$i]['drugpack'];
			$orderbasketlist->prescriptiontype   = $cart[$i]['prescriptiontype'];
			$orderbasketlist->basketliststatus   = 1;
			$orderbasketlist->isscript   = $cart[$i]['scriptavailable'];
			$orderbasketlist->itemimage   = $cart[$i]['image'];
			$orderbasketlist->cardtype = $cart[$i]['cardtype'];
			$orderbasketlist->istbc = $cart[$i]['istbc'];
			$orderbasketlist->isgeneric = $cart[$i]['isgeneric'];
			if($cart[$i]['price'] == 'NA')
			{
			$orderbasketlist->originalprice   = '0.00';
			}
			else
			{
			$orderbasketlist->originalprice   = $cart[$i]['price'];
			}
			$orderbasketlist->save();
				
			$orderinvoicelist = new Orderinvoicelist;
			$orderinvoicelist->invoiceid = $invoiceid;
			$orderinvoicelist->orderid  = $orderid;
			$orderinvoicelist->orderinvoicestatus = "1";
			$orderinvoicelist->save();
			
			if($cart[$i]['prescriptionid'] !="")
			{
			
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cart[$i]['prescriptionid'].'"'))
															  ->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
																->update(['prescriptionitemdetails.medstatus' => 'Ordered']);
			
			//To Check if uploaded script is related Medlist Medication script
			$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cart[$i]['prescriptionid'].'"'))
													->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
													->get();
			$count = $getmedlistid->count();
			if($count > 0)
			{
				if($count == 1)
				$medlistdetails = $getmedlistid[0]['medilistid'];
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
												->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
												->update(['prescriptionitemdetails.medstatus' => 'Ordered']);
			}
		
			}
			else
			{
				
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cart[$i]['medlistid'])
												->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
												->update(['prescriptionitemdetails.medstatus' => 'Ordered']);
			}
			
		}
			$locationname = Locations :: where('locationid','=',$request->locationid)->get();
			$basketdetails = Basket :: where('orderid' , $orderid)->get();
			$invoicedetails = Invoice :: where('orderid' , $orderid)->get();
			$orderbasketlistdetails = Orderbasketlist :: where('orderid' , $orderid)->get();
			$getuniqueid = Orderdetails :: where('orderid' , $orderid)->get();
			$orderinvoicelistdetails = Orderinvoicelist :: where('orderid' , $orderid)->get();
			$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$request->deliveryaddressid)->get();
			
			$removeCart = Cart :: where('enduserid', $user->userid)->where('type',1)->delete();
			
			if(isset($request->type)&& $request->type==2){
				$removeCart = Cart :: where('enduserid', $user->userid)->where('type',2)->delete();
			}
			
			$msg = "Great work, your order has been sent to '".$locationname[0]['locationname']."'. The Pharmacy will review & confirm your order and update the availability of items and price if required.";
			
			
			$response->orderDetails 	= $orderDetails;
			$response->orderTracking 	= $orderTracking;
			$response->basket 	= $basketdetails;
			$response->orderbasketlist = $orderbasketlistdetails;			
			$response->msg 		= $msg;
			$response->status 		= $this->successStatus;
			if($request->Istbc==1){
			$this->placeorderemails($request->locationid,$orderid,$request->deliveryaddressid,$user->userid,$request->userpreffereddate,$request->userprefferedtime,$request->deliveryfee,$request->orderTotal);
			} 
		
		
			
		}
		else
		{
			$response->msg 		= trans('messages.required_message');
			$response->status 		= $this->successStatus;
		}
		
			
		return json_encode($response);
	}
	
	public function placeorderemails($locationid, $orderid, $deliveryaddressid,$userid,$userpreffereddate,$userprefferedtime,$deliveryfee,$ordertotal)
	{
		
		$user = Auth::user();
		$locationname = Locations :: where('locationid','=',$locationid)->get();
		//$user_details = User::findOrFail($user->userid);
		$user_details = User :: where('userid', $userid)->get();
		$orderData = Orderdetails::where('orderid',$orderid)->get();
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$locationname = Locations :: where('locationid','=',$locationid)->get();
		$basketdetails = Basket :: where('orderid' , $orderid)->get();
		$invoicedetails = Invoice :: where('orderid' , $orderid)->get();
		$orderbasketlistdetails = Orderbasketlist :: where('orderid' , $orderid)->get();
		$getuniqueid = Orderdetails :: where('orderid' , $orderid)->get();
		$orderinvoicelistdetails = Orderinvoicelist :: where('orderid' , $orderid)->get();
		$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$deliveryaddressid)->get();
		
		//Pharmacy Email
		
			$orderday = $userpreffereddate;
			$orderdate = date('d F Y', strtotime($userpreffereddate));
			$placeddate = date('d F Y', strtotime($getuniqueid[0]['orderDate']));
			// $link = "https://pharmacy.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			// $subject="Medmate – New Order Request #".$getuniqueid[0]['uniqueorderid'];
			if(env('ENV') == "TEST")			
			$link = "https://test-app.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			else if(env('ENV') == "DEV")
			$link = "https://dev-app.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			else
			$link = "https://pharmacy.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			if($locationname[0]->locationstatus==1){			
			$subject=env('ENV')." Medmate – New Order Request #".$getuniqueid[0]['uniqueorderid']; 
			}else{
				$subject = env('ENV')." Medmate – New Order Request #".$getuniqueid[0]['uniqueorderid']; 
			}
			$uniqueorderid = $getuniqueid[0]['uniqueorderid'];
			$prescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->get();
											  
					$nqty=0;$nprice=0;	
					if($prescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$prescriptionitems->count();$np++)
						{
							$nqty = $nqty+$prescriptionitems[$np]['drugpack'];
							$priceval = $prescriptionitems[$np]['drugpack']*$prescriptionitems[$np]['originalprice'];
							$nprice = $priceval+$nprice;
						}
					}
			$tbc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->where('orderbasketlist.originalprice' , '=' , '0.00')
                                             ->get();
			$tbccount = $tbc->count();
			$tbctext='';
			if($tbccount > 0)
			{
				$tbctext=  "(".$tbccount ."scripts with pricing to be confirmed by pharmacy)"; 
			}
			else
				$tbctext='';
			$nonprescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 0)
                                              ->get();
					$qty=0; $npriceval=0;
					if($nonprescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$nonprescriptionitems->count();$np++)
						{
							$qty = $qty+$nonprescriptionitems[$np]['drugpack'];
							$npval=$nonprescriptionitems[$np]['drugpack']*$nonprescriptionitems[$np]['originalprice'];
							$npriceval = $npriceval+$npval;
						}
					} 
			
				$subtotal = $npriceval+$nprice;
			$gst=0;
			$gstcalc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->get();
			for($i=0;$i<$gstcalc->count();$i++)
			{
				$gstcalcval = $this->compute_price($locationid,$gstcalc[$i]['prescriptionid'],$user->userid);
				$gst = $gst+$gstcalcval;
			}
			$deliverydate = $orderdate = date('d F Y', strtotime($orderData[0]['userpreffereddate']));
			if($locationname[0]->locationstatus==1 && $orderData[0]->type==1){
			$body_message = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order to pharmacy</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {       
                color: #000000 !important;         
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new quote request.</h1>  
                                                <p>Hello '.$locationname[0]['locationname'].',<br>
                                                A new quote has been requested by a customer. <br>
                                                Use the below link to review the order.<p>
                                                <h3>Order Details</h3>
                                                <p>'.$orderData[0]['ordertype'].'<br>
                                                '.$userpreffereddate.', '.$userprefferedtime.'</p>

                                                <center>
                                                <a href="'.$link.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/view-order-btn.png" alt="View Order" />
                                                </a>
                                                </center>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
			';}else if($locationname[0]->locationstatus==2 && $orderData[0]->type==1){
				$body_message = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new Quote Request.</h1>  
                                                <p>Hello&nbsp;'.$locationname[0]['locationname'].',<br>
                                                A new quote has been requested by a local customer. <a href="https://medmate.com.au/purchase-order-faqs/" target="_blank">Learn more.</a> 
</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Order Details</h3>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetails"> 
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td class="orderDetailsTop">
                                                            Order: '.$uniqueorderid.'<br>
                                                            Placed on '.$placeddate.'
                                                        </td>        
                                                        <td width="20" style="width:20px;">&nbsp;</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetailsPurchases"> 
                                                                <thead>
                                                                    <tr>
                                                                        <th>Items</th>
                                                                        <th>Qty</th>
                                                                         </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <b>Prescription Items</b><br>
                                                                            
                                                                        </td></tr>';
																		for($i=0;$i<$prescriptionitems->count();$i++)
						{
							$body_message .='<tr>
																		<td>'.$prescriptionitems[$i]['drugname'].'</td>
                                                                        <td>'.$prescriptionitems[$i]['drugpack'].'</td>
                                                                        
                                                                    </tr>';
						}
						$body_message .='<tr>
                                                                        <td colspan="2"><b>Non-Prescription Items</b></td>
                                                                        </tr>';
																		for($j=0;$j<$nonprescriptionitems->count();$j++){
																	$body_message .='<tr>
																		<td>'.$nonprescriptionitems[$j]['drugname'].'</td>
                                                                        <td>'.$nonprescriptionitems[$j]['drugpack'].'</td>
                                                                        
                                                                    </tr>';
						}
                                                             $body_message .='   </tbody>
                                                            </table>
                                                        </td>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                        <td height="40" style="height:40px;">&nbsp;</td>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                </table>

                                                
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="30%">&nbsp;</td>
                                                        <td><a href="https://pharmacy.medmate.com.au/rejectorder/'.$orderData[0]->orderid.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/reject-quote-btn.png">
                                                </a></td><td width="20" style="width:20px;padding-left:60px;">
                                                <a href="'.$link.'" target="_blank">
												<img src="https://pharmacy.medmate.com.au/provide-quote-btn.png">
                                                    
                                                </a></td>
                                                        <td width="30%">&nbsp;</td>
                                                    </tr>
                                                   </table>



                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4> Customer Details:</h4>
                                                            '.$user_details[0]->firstname.'<br>
															Accept Quote  to view additional details of customer including entitlement.​ </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$locationname[0]->locationname.'<br>
							'.$locationname[0]->phone.'<br>
							'.$locationname[0]->address.'<br>
							'.$locationname[0]->suburb.'<br>
 Australia
                                                        </td>                                                        
                                                    </tr>
                                                </table>
												<br>
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable">
												<tr><td colspan="3"></td></tr>
												<tr>
                                            <td colspan=3 valign="top">1. Medmate will collect payment on behalf of the customer when order is finalised. ​</td></tr>
											<tr><td colspan=3 valign="top">2. A Medmate customer service representative will contact you to provide you access to view the quote request, or please call (03) 9894 7831​​</td></tr>
											<tr><td colspan=3 valign="top">3. The Pharmacy warrants that:​<br>
											<ul>
											<li>it has the authority and capacity to enter into and fulfil its obligations in accordance with this Quote Request;​</li>
											<li>it is, and will remain, a licensed Pharmacy; and​</li>
											<li>it has, and will maintain, all necessary permits, licences and registrations to operate as a Pharmacy.​</li>
											<li>By accepting this Quote Request you are accepting all Pharmacy Obligations and Warranties as outlined in  <a href="https://medmate.com.au/terms-and-conditions/">Terms and Conditions</a> ​</li>
											</ul></td></tr>
											</table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2021 Medmate Australia Pty Ltd​
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
			';
			}elseif($orderData[0]->type==2 && $locationname[0]->locationstatus==2){
				$body_message='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new in store order.​</h1>  
                                                <p>Hello  &nbsp;&nbsp;'.$locationname[0]['locationname'].',<br>
                                                A new order has been confirmed and sent  to your pharmacy from a local customer for in store payment. <a href="https://medmate.com.au/purchase-order-faqs/" target="_blank">Learn more.</a> 
</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Order Details</h3>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetails"> 
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td class="orderDetailsTop">
                                                            Order: '.$uniqueorderid.'<br>
                                                            Placed on '.$placeddate.'
                                                        </td>        
                                                        <td width="20" style="width:20px;">&nbsp;</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetailsPurchases"> 
                                                               
															   <thead>
                                                                    <tr>
                                                                        <th>Items</th>
                                                                        <th>Qty</th>                                                                                                                                       </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                         <tr>
                                                                        <td colspan="2">
                                                                            Prescription Items<br>
                                                                            
                                                                        </td></tr>';
																		for($i=0;$i<$prescriptionitems->count();$i++)
						{
							$body_message .='<tr>
																		<td>'.$prescriptionitems[$i]['drugname'].'</td>
                                                                        <td>'.$prescriptionitems[$i]['drugpack'].'</td>
                                                                        
                                                                    </tr>';
						}
						                                     $body_message .='   </tbody>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                        <td height="40" style="height:40px;">&nbsp;</td>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                </table>

                                                
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="30%">&nbsp;</td>
                                                        <td><a href="https://pharmacy.medmate.com.au/rejectorder/'.$orderData[0]->orderid.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/reject-order-btn.png">
                                                </a></td><td width="20" style="width:20px;padding-left:60px;">
                                                <a href="'.$link.'" target="_blank">
												<img src="https://pharmacy.medmate.com.au/accept-order-btn.png">
                                                    
                                                </a></td>
                                                        <td width="30%">&nbsp;</td>
                                                    </tr>
                                                   </table>



                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4> Customer Details:</h4>
                                                            '.$user_details[0]->firstname.'<br>
															Accept Purchase Order  to view additional details of customer including entitlement.​ </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$locationname[0]->locationname.'<br>
							'.$locationname[0]->phone.'<br>
							'.$locationname[0]->address.'<br>
							'.$locationname[0]->suburb.'</td>                                                        
                                                    </tr>
                                                </table>
												<br>
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable">
												<tr><td colspan="3"></td></tr>
												<tr>
                                            <td colspan=3 valign="top">1. A Medmate customer service representative will contact you to provide you access to view the Purchase Order, or please call (03) 9894 7831​​</td></tr>
											<tr><td colspan=3 valign="top">2. The Pharmacy warrants that:​<br>
											<ul>
											<li>it has the authority and capacity to enter into and fulfil its obligations in accordance with this Purchase Order;​</li>
											<li>it is, and will remain, a licensed Pharmacy; and​</li>
											<li>it has, and will maintain, all necessary permits, licences and registrations to operate as a Pharmacy.​</li>
											<li>By accepting this Purchase Order you are accepting all Pharmacy Obligations and Warranties as outlined in  <a href="https://medmate.com.au/terms-and-conditions/">Terms and Conditions</a> ​</li>
											</ul></td></tr>
											</table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faqs/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2021 Medmate Australia Pty Ltd​
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>';
			}elseif($orderData[0]->type==2 && $locationname[0]->locationstatus==1){
				$body_message='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new in store order.​</h1>  
                                                <p>Hello&nbsp;'.$locationname[0]['locationname'].',<br>
                                                A new order has been confirmed and sent  to your pharmacy from a local customer for in-store payment.<br><br> The customer will pay for the order during pick up. ​
</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Order Details</h3>

                                                <p>'.$orderData[0]['ordertype'].'<br>
                                                '.$deliverydate.', '.$orderData[0]['userprefferedtime'].'</p>

                                                
												<table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="30%">&nbsp;</td>
                                                        <td><a href="'.$link.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/view-order-btn.png" alt="View Order" />
                                                </a></td>
                                                        <td width="30%">&nbsp;</td>
                                                    </tr>
                                                   </table>



                                                                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faqs/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2021 Medmate Australia Pty Ltd​
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>';
			}

			   $message['to']           = $locationname[0]['email'];
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])				  
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
				  ->bcc('orders@medmate.com.au', 'Medmate')
				  ->bcc('vijaytalluri88@gmail.com', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			//}   
			   //Email to Customer
							
			if($orderData[0]->type==1){
			$body_message1 = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$getuniqueid[0]['uniqueorderid'].'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Price Requested</h1>  
                                                <p>Hi '.$user_details[0]['firstname'].',<br>
                                                Your order has been placed with the selected Pharmacy. The Pharmacy will review your order and update the availability of items and price. You can view the status of your order within the Medmate app. The Pharmacy will contact you if there are any issues.</p>
                                                
												<div class="line">&nbsp;</div>
												<br>
                                                
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4>Your Details:</h4>
                                                            '.$user_details[0]['firstname'].' '.$user_details[0]['lastname'].' <br> '.$user_details[0]['mobile'].' <br>   
                                                              ';
                                                              if($deliveryaddress->count() > 0)
                                                              {
                                                                $body_message1 .=' '.$deliveryaddress[0]['addressline1'].' '.$deliveryaddress[0]['addressline2'].' <br>'.$deliveryaddress[0]['suburb'].', '.$deliveryaddress[0]['state'].', '.$deliveryaddress[0]['postcode'].' <br> Australia
                                                              ';
                                                              }
                                                            $body_message1 .='
                                                        </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$locationname[0]['locationname'].' <br>'.$locationname[0]['phone'].' <br> '.$locationname[0]['address'].' <br> '.$locationname[0]['suburb'].', '.$locationname[0]['suburb'].', '.$locationname[0]['state'].' <br> Australia
                                                        </td>                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
			';}else{
				$body_message1 = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$getuniqueid[0]['uniqueorderid'].'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Price Requested</h1>  
                                                <p>Hi '.$user_details[0]['firstname'].',<br>
                                                Thank you for your recent purchase. Your order has been successfully sent to the pharmacy for fulfilment. Please pay for the order at the Pharmacy.​</p>
                                                
												<div class="line">&nbsp;</div>
												<br>
                                                
                                                </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
			';
			}
			   $message1['to']           = $user_details[0]['username'];
              // $message1['subject']      = "Medmate – Placed Order details #".$getuniqueid[0]['uniqueorderid'];
               $message1['subject']      = env('ENV')." Medmate – Placed Order details #".$getuniqueid[0]['uniqueorderid'];
               $message1['body_message'] = $body_message1;
               // echo ($message['body_message']); die;
			  // ->from('medmate-app@medmate.com.au', 'Medmate')
               try{
				Mail::send([], $message1, function ($m1) use ($message1)  {
               $m1->to($message1['to'])
                  ->subject($message1['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message1['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
	}
	
	public function compute_price($location,$drug,$profile){
$price="NA";
$gst=0;
if($location !="")
{
$business = Locations :: where('locationid', $location)->get();
}
 $price_details = DB :: select("select  `CO`.*, `CS`.*, `CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode` where (`CO`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$drug."%' and `CO`.`Pricing_Tier` = 1 and `CO`.`status` = 1) or (`CS`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$drug."%' and `CS`.`Pricing_Tier` = 1 and `CS`.`status` = 1)");
  
                    
                        $healthprofile = DB::table('healthprofile')->where('profileid',$profile)->get();
						if(count($price_details)>0){
							
						if($price_details[0]->ItemCommodityClass=='OTC')
						{
							$p=$price_details[0]->OTCStandardprice;
							$gst =(($price_details[0]->GST)*$p)/100;
							$price =$p+ $gst;
							
						}
						else
						{
							if(count($healthprofile)>0){
							
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                    $p=$price_details[0]->Con_Pens_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								if($healthprofile[0]->veteranaffairsnumber!=null){
                                    $p=$price_details[0]->Veterans;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->ScriptStandardprice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }

                            }
							
						}
                        }
						else{
                            $price="NA";
                        }
                        return round($gst,2);
                        

    } 
	/**
      @OA\Get(
          path="/v3/myOrders",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
      
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrders()
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get(); 
		
		$orders = Orderdetails :: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')								 							
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')									
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								->where('orderdetails.enduserid','=',$user->userid)
								->distinct()
								->select('locations.*','orderdetails.*','ordertracking.orderstatus as status')
								->where('ordertracking.trackingstatus','=',1)
								->orderBy('orderdetails.created_at', 'DESC')
								->get();
		$nooforders = $orders->count();
		
		for($i=0;$i<$nooforders;$i++)
		{			
			$orderList[$i] = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->distinct()
									->select('orderbasketlist.*')
									->where('ordertracking.orderid' , '=' , $orders[$i]['orderid'])
									->where('basket.orderid' , '=' , $orders[$i]['orderid'])
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orders[$i]['orderid'])
									->get();
			// if(!empty($orderList[$i]))
			// {
			// for($p=0; $p< $orderList[$i]->count();$p++)
			// {
				// $prescriptionDetails[$p] = Prescriptiondetails :: where('prescriptiondetails.prescriptionid','=',$orderList[$i][$p]['prescriptionid'])->get();
				// $prescriptionItemDetails[$p] = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid','=',$orderList[$i][$p]['prescriptionid'])
																			// ->where('prescriptionitemdetails.profileid','=',$orderList[$i][$p]['profileid'])
																			// ->get();
				
				//$orderList[$i][$p]['prescriptionDetails']	= 	$prescriptionDetails[$p];
				//$orderList[$i][$p]['prescriptionItemDetails']	= 	$prescriptionItemDetails[$p];
			// }
			$orders[$i]['orderItems']	= 	$orderList[$i];
			}
			
			
		
		
		
		 return response()->json(['myOrders' => $orders], $this->successStatus); 
     }
	 
	 /**
      @OA\Get(
          path="/v3/myOrderSummary",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrderSummary(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get(); 
		
		$orders = Orderdetails :: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')									
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								->where('enduserid','=',$user->userid)
								->where('ordertracking.trackingstatus','=',1)
								->where('orderdetails.orderid','=',$request->orderid)
								->distinct()
								->select('locations.*','orderdetails.*','ordertracking.orderstatus as status')
								->orderBy('orderdetails.created_at', 'DESC')
								->get();
		$nooforders = $orders->count();
		
		for($i=0;$i<$nooforders;$i++)
		{			
			$orderList[$i] = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->distinct()
									->select('orderbasketlist.*')
									->where('ordertracking.orderid' , '=' , $orders[$i]['orderid'])
									->where('basket.orderid' , '=' , $orders[$i]['orderid'])
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orders[$i]['orderid'])
									->get();
			
			
			
			$orders[$i]['orderItems']	= 	$orderList[$i];
			
			
				
		}
		
		
		 return response()->json(['myOrders' => $orders], $this->successStatus); 
     }
	 
	 /**
      @OA\post(
          path="/v3/myOrderItemUpdate",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderbasketid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="orderTotal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="drugpack",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ),		  
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrderItemUpdate(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.orderbasketid','=',$request->orderbasketid)
										->where('orderbasketlist.userid','=',$user->userid)
										->where('orderbasketlist.basketliststatus', '=',1)
										->update(['orderbasketlist.drugpack' => $request->drugpack]);
										
		if($request->orderTotal!='')
		{			
		$ordertotalUpdate = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
											->update(['orderdetails.orderTotal' => $request->orderTotal]);
		}
		return response()->json(['itemupdate' => $orderBasket], $this->successStatus); 
    }
	
	/**
      @OA\post(
          path="/v3/myOrderItemRemove",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderbasketid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderTotal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
			
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrderItemRemove(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.orderbasketid','=',$request->orderbasketid)
										->where('orderbasketlist.userid','=',$user->userid)
										->update(['orderbasketlist.basketliststatus' => 0]);
										
		if($request->orderTotal!='')
		{			
		$ordertotalUpdate = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
											->update(['orderdetails.orderTotal' => $request->orderTotal]);
		}
		return response()->json(['itemremoved' => $orderBasket], $this->successStatus); 
     }
	 
	 
	/**
      @OA\post(
          path="/v3/cancelOrder",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderremarks",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function cancelOrder(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		
		
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
										->where('orderdetails.enduserid','=',$user->userid)
										->update(['orderdetails.orderstatus' => 'Cancelled']);
		$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)->where('ordertracking.trackingstatus','=',1)->get();
		
		$noofrows = $ordertrack->count();
		
		for($i=0;$i<$noofrows;$i++)	
		{
			$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)
											  ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
											->update(['ordertracking.trackingstatus' => '0']);
											
		}								
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $request->orderid;
			$orderTracking->orderstatus = "Cancelled";
			$orderTracking->isnew = '1';
			$orderTracking->orderremarks = $request->orderremarks;
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	

		//Medlist Status Change After Cancellation
		
			$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.userid','=',$user->userid)->get();
			
			$noofItems = $orderBasket->count();
			for($b=0;$b<$noofItems;$b++)
			{
										
				if($orderBasket[$b]['prescriptionid'] !="")
				{
				
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
																  ->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
																  ->where('prescriptionitemdetails.isscript', '=', 1)															  
																	->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				
				//To Check if uploaded script is related Medlist Medication script
				$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
															->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
															->get();
				$count = $getmedlistid->count();
				if($count > 0)
				{
					if($count == 1)
					$medlistdetails = $getmedlistid[0]['medilistid'];
					$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
													->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				}
		
				}
				else
				{
					
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $orderBasket[$b]['medlistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$i]['profileid'])
													->where('prescriptionitemdetails.isscript', '=', 0)
													->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				}
			}
		
		return response()->json(['cancelledItem' => $orderTracking], $this->successStatus); 
     }
	 
	/**
      @OA\post(
          path="/v3/completeOrder",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			
			
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function completeOrder(Request $request)
	 {
		 $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		
		
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
										->where('orderdetails.enduserid','=',$user->userid)
										->update(['orderdetails.orderstatus' => 'Completed']);
		$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)->where('ordertracking.trackingstatus','=',1)->get();
		
		$noofrows = $ordertrack->count();
		
		for($i=0;$i<$noofrows;$i++)	
		{
			$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)
											  ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
											->update(['ordertracking.trackingstatus' => '0']);
											
		}								
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $request->orderid;
			$orderTracking->orderstatus = "Completed";
			$orderTracking->isnew = '1';
			$orderTracking->orderremarks = '';
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	
	 //Medlist Status Change After Completetion
		
			$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.userid','=',$user->userid)->get();
			
			$noofItems = $orderBasket->count();
			for($b=0;$b<$noofItems;$b++)
			{
										
				if($orderBasket[$b]['prescriptionid'] !="")
				{
				
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
																  ->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
																  ->where('prescriptionitemdetails.isscript', '=', 1)															  
																	->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				
				//To Check if uploaded script is related Medlist Medication script
				$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
															->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
															->get();
				$count = $getmedlistid->count();
				if($count > 0)
				{
					if($count == 1)
					$medlistdetails = $getmedlistid[0]['medilistid'];
					$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
													->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				}
		
				}
				else
				{
					
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $orderBasket[$b]['medlistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$i]['profileid'])
													->where('prescriptionitemdetails.isscript', '=', 0)
													->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				}
			}
		
		return response()->json(['completedOrder' => $orderTracking], $this->successStatus); 
     }
	 
	
/**
      @OA\Get(
          path="/v3/getdeliverypartners",
          tags={"delivery partners"},
          summary="delivery partners",
          operationId="delivery partners",
		  security={{"bearerAuth": {}} },
      
          
		  @OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		 @OA\Parameter(
              name="deliveryaddressid",
              in="query",
              required=true,
              @OA\Schema(
                  type="integer"
              )
          ),
		  	@OA\Parameter(
              name="ordertotal",
              in="query",
              required=true,
              @OA\Schema(
                  type="decimal"
              )
          ),
		   @OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="decimal"
              )
          ),
         
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
		public function getdeliverypartners(Request $request)
		{
			$response = (object)array();
			$deliverypartnerdetails = array();
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$deleiveryaddress = Deliveryaddress :: where('deliveryaddressid',  $request->deliveryaddressid)->get();
			$pickupaddress = Locations :: where('locationid',$request->locationid)->get();
			
				$pickupcity = 	$pickupaddress[0]['suburb'];	
				$pickupstate =  $pickupaddress[0]['state'];
				$pickupstreet =  $pickupaddress[0]['streetaddress'];
				$pickuppostcode =  $pickupaddress[0]['postcode'];
				
				$deleiverycity = $deleiveryaddress[0]['suburb'];
				$deleiverystate = $deleiveryaddress[0]['state'];
				$deleiverystreet = $deleiveryaddress[0]['addressline1'];
				$deleiverypostcode = $deleiveryaddress[0]['postcode'];
			//$addressFrom = '2/117-119 Cranbourne-Frankston Rd, Langwarrin 3910';
			//$addressTo = '101 Princes Hwy, Dandenong South VIC 3175, Australia';
			//$addressTo = '130 Quarry Rd, Langwarrin VIC 3910, Australia';
			
			//$addressTo = '301-131 Church Street,hawthorn,VIC,3122';
			$addressFrom = $pickupstreet." ,".$pickupcity." ,".$pickupstate." ,".$pickuppostcode;
			$addressTo = $deleiverystreet." ,".$deleiverycity." ,".$deleiverystate." ,".$deleiverypostcode;
			$distance = $this->getDistance($addressFrom, $addressTo, "K");
			$isscript=0;
			if($request->orderid != "")
			{
				$orderid = Orderdetails :: where('uniqueorderid', '=' ,$request->orderid)->get();
				$orderbasket = orderbasketlist :: where('orderid', '=' ,$orderid[0]['orderid'])->where('basketliststatus' ,'=', 1)->where('isscript' ,'=', 1)->get();
				
				if(count($orderbasket)>0)
				{
					$isscript=1;
				}
				
			}
			else
			{
				$cartinfo = DB::select("select * from cart where scriptavailable=1 and cartstatus=1 and enduserid = ".$user->userid." ");
				
				if(count($cartinfo)>0)
				{
					$isscript=1;
				}
				
			}
			$script="";
			if($isscript==1){
				$script=" and scriptval=1";
			}else{
				$script=" and scriptval=2";
			}
			$deliverypartner = DB::select("select * from deliverypartner where locationid=".$request->locationid." and partnerstatus=1".$script); 
			if(count($deliverypartner)==0){
				$deliverypartner = DB::select("select * from deliverypartner where locationid='0' and partnerstatus=1".$script);
			}
			for($i=0;$i<count($deliverypartner);$i++)
			{
				if($request->ordertotal >= $deliverypartner[$i]->deliverylimit)
				{
					$deliverypartner[$i]->finaldeliveryfee = "0.00";
				}
				elseif($distance <= $deliverypartner[$i]->basekms)
				{
					//$deliverypartner[$i]->finaldeliveryfee = round($deliverypartner[$i]->deliverycharge , 2);
					$deliverypartner[$i]->finaldeliveryfee = number_format((float)$deliverypartner[$i]->deliverycharge, 2, '.', '');
				}
				elseif($distance > $deliverypartner[$i]->basekms)
				{
					$distancediff = $distance - $deliverypartner[$i]->basekms;
					$extrakmcharge = $distancediff * $deliverypartner[$i]->extrakmcharge;
					//$deliverypartner[$i]->finaldeliveryfee = round($deliverypartner[$i]->deliverycharge + $extrakmcharge ,2);
					$deliverypartner[$i]->finaldeliveryfee = number_format((float)($deliverypartner[$i]->deliverycharge + $extrakmcharge), 2, '.', '');
				}
				else
				{
					//$deliverypartner[$i]->finaldeliveryfee = round($deliverypartner[$i]->deliverycharge , 2);
					$deliverypartner[$i]->finaldeliveryfee = number_format((float)$deliverypartner[$i]->deliverycharge, 2, '.', '');
				}
				
				//$timenow = gmdate("Y-m-d\TH:i:s");
				
				//$timeval = gmdate("H");
				//$locationtimings = DB::select("select * from pharmacyopenclosetimings where locationid=".$request->locationid." and pharmacytimestatus=1"); 
				 $locationname = Locations :: where('locationid','=',$request->locationid)->get();
				 date_default_timezone_set($locationname[0]['timezone']);
				 $cmptime = date("H");
			 
				
				if($deliverypartner[$i]->partnername != "Standard Delivery"){
				if($distance <= $deliverypartner[$i]->maxkmallowed)
				{
				if($cmptime > $deliverypartner[$i]->starttime && $cmptime < $deliverypartner[$i]->endtime)
				{				
				
				//echo $distance;
					array_push($deliverypartnerdetails,$deliverypartner[$i]);					
					
				}
				
				}
				}
				else
				{
					 //unset($deliverypartner[$i]);
					 if($deliverypartner[$i]->partnername == "Standard Delivery")
					 {
						array_push($deliverypartnerdetails,$deliverypartner[$i]);
					 }
				}
			
			}
					$response->deliveryoptions 	= $deliverypartnerdetails;
					$response->status 		= $this->successStatus;
					return json_encode($response);
			
			
		}
		
		  public function getDistance($addressFrom, $addressTo, $unit = ''){
    // Google API key
    $apiKey = 'AIzaSyCvYESmh7woXSFQnOLOr9WhN-A_GYUEKhE';
    
    // Change address format
    $formattedAddrFrom    = str_replace(' ', '+', $addressFrom);
    $formattedAddrTo     = str_replace(' ', '+', $addressTo);
    
    // Geocoding API request with start address
   // $geocodeFrom = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddrFrom.'&sensor=false&key='.$apiKey);
    
	$curl = curl_init();
	$url="https://maps.googleapis.com/maps/api/geocode/json?address=".$formattedAddrFrom."&sensor=false&key=".$apiKey."";
			curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
			));

			$geocodeFrom = curl_exec($curl);

			curl_close($curl);
			$outputFrom = json_decode($geocodeFrom); 
    if(!empty($outputFrom->error_message)){
        return $outputFrom->error_message;
    }
    
    // Geocoding API request with end address
    //$geocodeTo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddrTo.'&sensor=false&key='.$apiKey);
	$curlgeocodeTo = curl_init();
			$urlgeocodeTo="https://maps.googleapis.com/maps/api/geocode/json?address=".$formattedAddrTo."&sensor=false&key=".$apiKey."";
			curl_setopt_array($curlgeocodeTo, array(
		  CURLOPT_URL => $urlgeocodeTo,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
			));

			$geocodeTo = curl_exec($curlgeocodeTo);

			curl_close($curlgeocodeTo);
			
    $outputTo = json_decode($geocodeTo);
    if(!empty($outputTo->error_message)){
        return $outputTo->error_message;
    }
    
    // Get latitude and longitude from the geodata
    $latitudeFrom    = $outputFrom->results[0]->geometry->location->lat;
    $longitudeFrom    = $outputFrom->results[0]->geometry->location->lng;
    $latitudeTo        = $outputTo->results[0]->geometry->location->lat;
    $longitudeTo    = $outputTo->results[0]->geometry->location->lng;
    
    // Calculate distance between latitude and longitude
    $theta    = $longitudeFrom - $longitudeTo;
    $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
    $dist    = acos($dist);
    $dist    = rad2deg($dist);
    $miles    = $dist * 60 * 1.1515;
    
    // Convert unit and return distance
    $unit = strtoupper($unit);
    if($unit == "K"){
	//return round($miles * 1.609344, 2).' km';
	return round($miles * 1.609344, 2);
	}
    // elseif($unit == "M"){
        // return round($miles * 1609.344, 2).' meters';
    // }else{
        // return round($miles, 2).' miles';
    // }
}
	 
}
