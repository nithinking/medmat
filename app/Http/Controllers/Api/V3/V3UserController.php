<?php

namespace App\Http\Controllers\api\V3;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Healthprofile; 
use App\models\Deliveryaddress; 
use App\models\Gplocations; 
use App\models\Locations; 
use App\models\Medicalconditions; 
use App\models\Allergies; 
use App\models\Immunisation; 
use App\models\Notifications; 
use App\models\Miuser;
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
use Helper;
use Session;
use Config;
use App;
use DateTime;
use Response;
    /**
  @OA\Info(
      description="",
      version="2.0.0",
      title="Medmate API",
 )
 
* @OA\SecurityScheme(
*      securityScheme="bearerAuth",
*      in="header",
*      name="bearerAuth",
*      type="http",
*      scheme="bearer",
*      bearerFormat="JWT",
* ),
 */
class V3Usercontroller extends Controller
{

 
 public $successStatus = 200;
/**
      @OA\Post(
          path="/v3/register",
          tags={"User"},
          summary="userSignUp",
          operationId="signup",
		  
		  @OA\Parameter(
              name="firstname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),  
          
           @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="refferal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),  
          
		  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),   
      
          @OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Parameter(
              name="password",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="type",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="c_password",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),	
          @OA\Parameter(
              name="accept_terms",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),	
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 
		public function register(Request $request) {    
		
			 $validator = Validator::make($request->all(), 
						  [ 
						  'username' => 'required|email|unique:users',
						  'firstname' => 'required',
						  'password' => 'required',  
                          'c_password' => 'required|same:password', 
                          'accept_terms'=>'required'
						 ]);   
			 if ($validator->fails()) {          
				   return response()->json(['error'=>$validator->errors()], 401);                        
				   }    
				   $pass='admin123';
                   $input = $request->all();  
                   $input['password'] = bcrypt($input['password']);
				   $input['master_password'] = bcrypt($pass);
				   if(isset($request->type)&& $request->type==2){
                   $input['profiletypeid'] = 2;}
				   elseif(isset($request->profile)&& $request->profile==9){
					   $input['profiletypeid']=9;
				   }else{
					   $input['profiletypeid'] = 1;
				   }				   
                   $input['refferal'] = $request->refferal;
                   $input['knownas'] = $request->firstname;
                   $input['hashcode'] = md5($input['password']);
                   $input['accept_terms']=$request->accept_terms;
                   $input['mi_accept']=$request->accept_terms;
                   $input['deviceid']=$request->device_id;
                  /// print_r($input);
			 $user = User::create($input); 
			// print_r($user);exit;
			  if($user){
                $miuser=new Miuser();
                $miuser->application_id=1;
                $miuser->device_id=$request['device_id'];
                $miuser->external_user_id=$user->userid;
                $miuser->first_name=$request['first_name'];
                $miuser->last_name=$request['last_name'];
                $miuser->email=$request['username'];
                $miuser->dob=$request['dob'];
                $miuser->createdby=1;
                $miuser->password=bcrypt($request['password']);
                $miuser->save();
              
				  $healthprofile = new Healthprofile;
					$healthprofile->userid =  $user->userid;
					$healthprofile->parentuser_flag = '1';
				 $healthprofile->profile_relation = 'self';
				 $healthprofile->save();
					
				$link=url('verifyaccount/'.base64_encode($user->userid));
				
				$subject=env('ENV')." Medmate – Complete your registration";
				//$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Verify Email</button></a>";
						  // $body_message = trans('messages.Verify_Email_Template');
                   
              // $body_message 	= 	str_replace('verify_button',$content,$body_message);
				$body_message ='
				<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <title>Email Verification</title>
	</head>
	<style>
		body  {
			font-family: "Nunito" !important;font-size: 18px !important;
		}
		.top {
			display: flex !important;flex-direction: row !important;width: 700px !important;justify-content: space-between !important;
		}
		.heading {
		   color: #1d9bd8 !important;font-size: xx-large;font-weight: 600 !important;
		}
		.total {
			width: 700px !important !important;  margin: auto !important;
		}
		.bottom {
			text-align: center !important;color :#7e7e7e !important;
		}
		.one {
			background-color: #1d9bd8 !important;color: white !important;border: 0px solid !important;border-radius: 10px !important;height: 35px !important;margin-left: 300px !important;margin-top: 50px !important;
		}
	</style>
		<body style="font-family: "Nunito" !important;font-size: 18px !important;">
		<div class="total" style="width: 700px !important !important;  margin: auto !important;">
			<div class="top" style="display: flex !important;flex-direction: row !important;width: 700px !important;justify-content: space-between !important;">
				<p style="padding-top: 15px !important;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" alt=""></p>
				<p class="heading" style="padding-left: 90px !important; color: #1d9bd8 !important;font-size: xx-large;font-weight: 600 !important;">Registration Verification</p>
			</div>
			<div>
			<p>Hello '.$request->firstname.',</p>
        <p>To complete the medmate registration process please click:</p>
        <p style="height: 150px !important;width:700px !important;background-color: #f2f2f2 !important;"><a href="'.$link.'">
		<button class="one" style="background-color: #1d9bd8 !important;color: white !important;border: 0px solid !important;border-radius: 10px !important;height: 35px !important;margin-left: 300px !important;margin-top: 50px !important;">Verify Email</button></a></p>
        <p>If you have any questions, please email <a href="">enquires@medmate.com.au</a> </p>
    </div>
    <div class="bottom" style="text-align: center !important;color :#7e7e7e !important;">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
							medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
				';
               $message['to']           = $user->username;
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   
			  $vlink=url('verifyaccount/'.base64_encode($user->userid));
			   
			   $wbody_message='
			   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>welome email</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
            .bodyContent img.welcome-image {
                margin-bottom: 4px;
            }
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            .bodyContent .list {
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
					max-width:600px !important;
					width:100% !important;
				}
                .welcome-image {
                    display: none;
                }
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                    	<tr>
                                			<td width="50" height="64" style="height:64px;width:50px;">&nbsp;</td>
                                			<td height="64" style="height:64px;">&nbsp;</td>
                                			<td width="50" height="64" style="height:64px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Welcome to Medmate.</h1>  
                                                <p>Hi '.$request->firstname.',<br>
                                                We hope you are enjoying your experience with Medmate.<br>
                                                Our goal is to deliver better health for you and your family.</p>
                                                <h4 style="margin-bottom: 13px;">Features & Benefits of Medmate</h4>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" class="list">
                                                            -  Use eScripts or paper scripts<br>
                                                            -  Express delivery in under 90 minutes*<br>
                                                            -  Bring the pharmacy to you, wherever you are<br>
                                                            -  Choose your preferred pharmacy <br>
                                                            -  Manage all your prescriptions in one place<br>
                                                            -  Shop off-the-shelf pharmacy items online<br>
                                                            -  Click-&-Collect also available
                                                        </td>
                                                        <td valign="bottom">
                                                            <img src="https://pharmacy.medmate.com.au/images/emailtemp/welcome.png" class="welcome-image" alt=""/>                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="30" style="height:30px;width:50px;">&nbsp;</td>
                                			<td height="30" style="height:30px;">&nbsp;</td>
                                			<td width="50" height="30" style="height:30px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
									<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                    	
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent">                                                
												<h2 style="font-size: 20px;font-weight: 500;font-style: normal;letter-spacing: normal;line-height: 30px;margin-bottom: 12px;">To complete your Medmate registration please verify your email.</h2>
												<div class="line">&nbsp;</div>
                                                <p style="margin: 0 0 18px;">&nbsp;</p>
                                                <center><a href="'.$vlink.'" target="_blank" >
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/verify-button.png" alt="Verify Email" />
                                                </a></center>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>                                    	
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <p style="font-size:12px;margin-bottom: 17px;margin-top: 20px;line-height: 100%;">*Delivery times may vary based on your location and peak hours.</p>
                                                <div class="line"></div>
                                                <br>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
			   ';
			   
				$wsubject=env('ENV')." Welcome to Medmate";
			   //$wbody_message = trans('messages.Welcome_Email');
			   $wmessage['to']           = $user->username;
               $wmessage['subject']      = $wsubject;
               $wmessage['body_message'] = $wbody_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $wmessage, function ($wm) use ($wmessage)  {
               $wm->to($wmessage['to'])
                  ->subject($wmessage['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($wmessage['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
               
			}
			 
			 $success['token'] =  $user->createToken('AppName')->accessToken;
			 $user_details = User::findOrFail($user->userid);
             $profile_completeness=10;
			 if(isset($user_details->firstname)){$profile_completeness=$profile_completeness+10;}
			 if(isset($user_details->lastname)){$profile_completeness=$profile_completeness+10;}
			 $success['profile_completeness']=$profile_completeness;
			 $success['user_details'] = $user_details;
			 return response()->json(['success'=>$success], $this->successStatus);
 
			 
 
		}
		/**
      @OA\Post(
          path="/v3/login",
          tags={"User"},
          summary="Login",
          operationId="login",
      
          @OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Parameter(
              name="password",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function login(Request $request){ 
	$profiletype=1;
	$user = User::where('username', '=', $request->username)->first();
if (empty($user)) {
    abort(404);
}
if(isset($request->type) && $request->type==2){
		$profiletype=2;
	}
		//if(Auth::attempt(['username' => request('username'), 'password' => request('password') ,'profiletypeid' => $profiletype,'userstatus' => '1'])){ 
		if(Auth::attempt(['username' => $request->username, 'password' => $request->password ,'profiletypeid' => $profiletype,'userstatus' => '1'])){ 
		   $user = Auth::user(); 
		   $success['token'] =  $user->createToken('AppName')-> accessToken; 
		   $success['user_details']=$user;
		    $profile_completeness=0;
		   if(isset($user->firstname)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->lastname)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->dob)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->username)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->mobile)){$profile_completeness=$profile_completeness+10;}
		   $healthprofile = Healthprofile::where('userid', $user->userid)->where('parentuser_flag' , '1')->get();
		   $success['healthprofile']=$healthprofile;
		   $familyMembers = User :: where('parentuserid' , $user->userid)->get();
		
			$noofrows = $familyMembers->count();
			for($i=0; $i < $noofrows ; $i++)
			{
				if($familyMembers[$i]['profilepic']!="")
				{
				$familyMemLogo[$i] = url('/profilepic/'.$familyMembers[$i]['profilepic']);
				$familyMembers[$i]['profilepic']	= 	$familyMemLogo[$i];
				}
				else
					$familyMembers[$i]['profilepic'] ="";
				$familyMemberHealthInfo[$i] = Healthprofile :: where('userid' , $familyMembers[$i]['userid'])->get();			
				
				$familyMembers[$i]['familyMemberHealthInfo']	= 	$familyMemberHealthInfo[$i];
				
				$success['familyMembers'] =$familyMembers;
			}
		   if(isset($healthprofile->medicarenumber) || isset($healthprofile->heathcarecard) || isset($healthprofile->veteranaffairsnumber) || isset($healthprofile->safetynet))
		   {
			   $profile_completeness=$profile_completeness+10;
		   }
		   $success['profile_completeness'] = $profile_completeness;
		   $success['familyMembers'] = $familyMembers;
			return response()->json(['success' => $success], $this-> successStatus); 
		  }
		  else if(isset($user)){//echo "hi";exit;
		  //elseif(Auth::attempt(['username' => request('username'), 'master_password' => request('password') ,'profiletypeid' => $profiletype,'userstatus' => '1'])){ 
		  // $user = Auth::user(); 
		   //print_r($user);exit;
		   $success['token'] =  $user->createToken('AppName')-> accessToken; 
		   $success['user_details']=$user;
		    $profile_completeness=0;
		   if(isset($user->firstname)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->lastname)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->dob)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->username)){$profile_completeness=$profile_completeness+10;}
		   if(isset($user->mobile)){$profile_completeness=$profile_completeness+10;}
		   $healthprofile = Healthprofile::where('userid', $user->userid)->where('parentuser_flag' , '1')->get();
		   $success['healthprofile']=$healthprofile;
		   $familyMembers = User :: where('parentuserid' , $user->userid)->get();
		
			$noofrows = $familyMembers->count();
			for($i=0; $i < $noofrows ; $i++)
			{
				if($familyMembers[$i]['profilepic']!="")
				{
				$familyMemLogo[$i] = url('/profilepic/'.$familyMembers[$i]['profilepic']);
				$familyMembers[$i]['profilepic']	= 	$familyMemLogo[$i];
				}
				else
					$familyMembers[$i]['profilepic'] ="";
				$familyMemberHealthInfo[$i] = Healthprofile :: where('userid' , $familyMembers[$i]['userid'])->get();			
				
				$familyMembers[$i]['familyMemberHealthInfo']	= 	$familyMemberHealthInfo[$i];
				
				$success['familyMembers'] =$familyMembers;
			}
		   if(isset($healthprofile->medicarenumber) || isset($healthprofile->heathcarecard) || isset($healthprofile->veteranaffairsnumber) || isset($healthprofile->safetynet))
		   {
			   $profile_completeness=$profile_completeness+10;
		   }
		   $success['profile_completeness'] = $profile_completeness;
		   $success['familyMembers'] = $familyMembers;
			return response()->json(['success' => $success], $this-> successStatus); 
		  }  else{ 
		   return response()->json(['error'=>'Unauthorised'], 401); 
		   } 
	}
	/**
      @OA\Get(
          path="/v3/getUser",
          tags={"User"},
          summary="UserProfile",
          operationId="User Profile",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getUser() {
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->where('parentuser_flag' , '1')->get();
		$addressDetails = Deliveryaddress::where('userid', $user->userid)->get();
		$today = date('Y-m-d');
        $age =  date_diff(date_create($user_details['dob']), date_create($today))->y;
		$user['age'] = $age;
		if($healthprofile[0]->medicarenumber!=null || $healthprofile[0]->medicarenumber!=''){
		//$healthprofile[0]['medicarestatus']=1;
		//$healthprofile[0]['concessionstatus']=1;}else{
		$healthprofile[0]['medicarestatus']=2;
		$healthprofile[0]['concessionstatus']=2;
		}
		//print_r($healthprofile);exit;
		if(!empty($user->profilepic))
		{
			$user->profilepic = url('/profilepic/'.$user->profilepic);
		}
		elseif($user->profilepic == "")
		{
			$user->profilepic ="";
		}
		$familyMembers = User :: where('parentuserid' , $user->userid)->where('userstatus' , 1)->get();
		
		$noofrows = $familyMembers->count();
		for($i=0; $i < $noofrows ; $i++)
		{
			if($familyMembers[$i]['profilepic']!="")
			{
			$familyMemLogo[$i] = url('/profilepic/'.$familyMembers[$i]['profilepic']);
			$familyMembers[$i]['profilepic']	= 	$familyMemLogo[$i];
			}
			else
				$familyMembers[$i]['profilepic'] ="";
			$familyMemberHealthInfo[$i] = Healthprofile :: where('userid' , $familyMembers[$i]['userid'])->get();			
		   
			$familyprofile_completeness[$i]=0;
			if(isset($familyMembers[$i]['firstname']) && $familyMembers[$i]['firstname']!="null")
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+20;}
			if(isset($familyMembers[$i]['lastname']) && $familyMembers[$i]['lastname']!="null")
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+10;}
			if(isset($familyMembers[$i]['dob']) && $familyMembers[$i]['dob']!=null)
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+10;}
			if(isset($familyMembers[$i]['mobile'])&& $familyMembers[$i]['mobile']!=null)
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+10;}		 
			if(isset($familyMembers[$i]['profilepic'])&& $familyMembers[$i]['profilepic']!="")
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+30;}
			if(isset($familyMemberHealthInfo[$i][0]['medicarenumber']) || isset($familyMemberHealthInfo[$i][0]['heathcarecard']) || isset($familyMemberHealthInfo[$i][0]['veteransaffairscard']) || isset($familyMemberHealthInfo[$i][0]['safetynet']))
			{$familyprofile_completeness[$i]=$familyprofile_completeness[$i]+20;}
			
			// $familyprofile_completeness = $familyprofile_completeness[$i];
		 
		 $today = date('Y-m-d');
		 $familyMembers[$i]['age'] =  date_diff(date_create($familyMembers[$i]['dob']), date_create($today))->y;
		 $familyMembers[$i]['familyMemberHealthInfo']	= 	$familyMemberHealthInfo[$i];
		 $familyMembers[$i]['familyprofile_completeness'] = $familyprofile_completeness[$i];
		}		
		$user['familyMembers'] =$familyMembers;
		$user['userhealthprofile'] = $healthprofile;
		//print_r($user['userhealthprofile']);exit;
		$user['usetraddressDetails'] = $addressDetails;
		$profile_completeness=0;
		 if(isset($user->firstname)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->lastname)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->dob)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->username)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->mobile)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->emailverified)){$profile_completeness=$profile_completeness+10;}
		 if(isset($user->profilepic)){$profile_completeness=$profile_completeness+20;}
		 if(isset($healthprofile[0]['medicarenumber']) || isset($healthprofile[0]['heathcarecard']) || isset($healthprofile[0]['veteransaffairscard']) || isset($healthprofile[0]['safetynet'])){$profile_completeness=$profile_completeness+20;}
		 $user['profile_completeness'] = $profile_completeness;
		
		$success['user_details']=$user;
		
		return response()->json(['success' => $user], $this->successStatus); 
	}
	/**
      @OA\Post(
          path="/v3/updateProfile",
          tags={"User"},
          summary="UserProfile Update",
          operationId="User Profile Update",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="middlename",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="knownas",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profilepic",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			 			 
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="sex",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="addressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="addressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="suburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="state",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="postcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="defaultaddress",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="receiveinfo",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
              @OA\Parameter(
              name="mi_accept",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
              @OA\Parameter(
              name="device_id",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="permissionforsms",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="occupation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ), @OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			  
			
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ismedicare",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumberpos",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipreferencenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="ihi",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ihi_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvano",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvanoexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="expectedduedate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="procedureyear",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateProfile(Request $request)
    {
		$response=(object)array();
		$this->common = new CommonController($request);
		 $user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
				 
		
				 $user->firstname = $request['firstname'];
				 $user->middlename = $request['middlename'];
				 $user->lastname = $request['lastname'];
				 $user->knownas = $request['knownas'];
                 $user->mi_accept=$request['mi_accept'];
                 if(isset($request['mi_accept'])){
                    $mi_accept=$request['mi_accept'];
                    if($mi_accept==0){$mi_accept=2;}else{$mi_accept=1;}
                   $mi_users=DB::table('mi_users')->where('application_id','=',1)->where('external_user_id','=',$user->userid)->get();
                   if(count($mi_users)>0){
                    DB::table('mi_users')->where('id',$mi_users[0]->id)->update(array('status'=>$mi_accept));
                    }else{
                       $miuser=new Miuser();
                       $miuser->application_id=1;
                       $miuser->device_id=$request['device_id'];
                       $miuser->external_user_id=$user->userid;
                       $miuser->first_name=$user->first_name;
                       $miuser->last_name=$user->last_name;
                       $miuser->dob=$user->dob;
                       $miuser->status=$mi_accept;
                       $miuser->email=$user->username;
                       $miuser->createdby=1;
                       $miuser->password=$user->password;
                       $miuser->save();
                    }
                    
                }
				 if($user->profilepic == "" && $request['profilepic'] !="")
				 {
					
					$user->profilepic=$this->base64_to_jpeg($request['profilepic'],time().$user->userid."pic.jpeg");
				 
				 }				
				  elseif($user->profilepic !="" || !empty($user->profilepic))
				 {
					 
					 $supported_image = array(						
						'jpeg'
					);

					$src_file_name = $request['profilepic'];
					$ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
					if (in_array($ext, $supported_image))
					{	
						//echo "12fdsfds3";exit;
					 $user->profilepic = $user->profilepic;
					}
					else
					{
						//echo "123";exit;
					$user->profilepic = $this->base64_to_jpeg($request['profilepic'],time().$user->userid."pic.jpeg");	
					}
				 }
				 elseif($request['profilepic'] =="")
				 {
					  
					 $request['profilepic'] = $request['profilepic'];
				 }
				 
				 $user->dob = $request['dob'];
				 $today = date('Y-m-d');
        		 $age =  date_diff(date_create($request['dob']), date_create($today))->y;
				 $user->sex = $request['sex'];				
				 $user->homephone = $request['homephone'];
				 $user->mobile = $request['mobile'];
				 $user->permissionforsms = $request['permissionforsms'];				 
				 $user->occupation = $request['occupation'];
				 $user->emergencycontactname = $request['emergencycontactname'];
				 $user->emergencycontactnumber = $request['emergencycontactnumber'];
				 $user->defaultaddress = $request['defaultaddress'];				 
				 $user->receiveinfo = $request['receiveinfo'];				 
				 
				 $user->save();
				 $user['age'] = $age;
				 // Address Add
				 
					 //DB::enableQueryLog();
					  // if ( Deliveryaddress::where('userid',  $user->userid)->where('addresstype' , 'defaultaddress')->exists()) {	
						// $deliveryAddress = Deliveryaddress::join('users','users.userid','=', 'deliveryaddresses.userid')
															// ->where('deliveryaddresses.userid',  $user->userid)

															// ->where('users.defaultaddress' , '1')->first();	
						if ( Deliveryaddress::where('userid',  $user->userid)->where('addresstype' , 'profileaddress')->exists()) {	
						// $deliveryAddress = Deliveryaddress::join('users','users.userid','=', 'deliveryaddresses.userid')
															// ->where('deliveryaddresses.userid',  $user->userid)
															// ->where('deliveryaddresses.addresstype',  "profileaddress")
															// ->first();
									
						// $deliveryAddress->addressline1 = $request['addressline1'];						
						// $deliveryAddress->addressline2 = $request['addressline2'];
						// $deliveryAddress->suburb = $request['suburb'];
						// $deliveryAddress->state = $request['state'];
						// $deliveryAddress->postcode = $request['postcode'];				
						// $deliveryAddress->mobile = $request->mobile;
						// $deliveryAddress->lattitude = "";
						// $deliveryAddress->longitude = "";
						// $deliveryAddress->save();  
						// print_r(DB::getQueryLog());exit;
						$deliveryAddress = Deliveryaddress::join('users','users.userid','=', 'deliveryaddresses.userid')
															 ->where('deliveryaddresses.userid',  $user->userid)
															 ->where('deliveryaddresses.addresstype',  "profileaddress")
															 ->update(['deliveryaddresses.addressline1' => $request['addressline1'],'deliveryaddresses.addressline2' => $request['addressline2'],'deliveryaddresses.suburb' => $request['suburb'],'deliveryaddresses.state' => $request['state'],'deliveryaddresses.postcode' => $request['postcode']   ]);
					  }
					  else{
						$deliveryAddress = new Deliveryaddress;
						$deliveryAddress->userid = $user->userid;
						$deliveryAddress->firstname = $request['firstname'];						
						$deliveryAddress->addresstype = "profileaddress";						
						$deliveryAddress->addressline1 = $request['addressline1'];
						$deliveryAddress->addressline2 = $request['addressline2'];
						$deliveryAddress->suburb = $request['suburb'];
						$deliveryAddress->state = $request['state'];
						$deliveryAddress->postcode = $request['postcode'];				
						//$deliveryAddress->mobile = $request->mobile;
						$deliveryAddress->lattitude = "";
						$deliveryAddress->longitude = "";
						$addressverify = $this->checkaddress($request['addressline1'],$request['addressline2'],$request['suburb'],$request['postcode'],$request['state']);
						//echo $addressverify;exit;
						if($addressverify == "OK")
						{
								$deliveryAddress->save();
						}
						else
						{
							$response->msg 		= "Selected Address is not valid.";
							$response->status 		= $this->successStatus;
						}
					  }
					 
				 
				 if ( Healthprofile::where('userid',  $user->userid)->where('parentuser_flag' , '1')->exists()) {
					$healthprofile = Healthprofile::where('userid',  $user->userid)->where('parentuser_flag' , '1')->first();
					
				  $healthprofile->gpname = $request['gpname'];
				  $healthprofile->gpclinicname = $request['gpclinicname'];
				  $healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				  $healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				  $healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				  $healthprofile->gpclinicstate = $request['gpclinicstate'];
				  $healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				  $healthprofile->gpclinicphone = $request['gpclinicphone'];
				  $healthprofile->gplink = $request['gplink'];
				  
				  
				  $healthprofile->ismedicare = $request['ismedicare'];
				  $healthprofile->medicarenumber = $request['medicarenumber'];
				  $healthprofile->medicarenumberpos = $request['medicarenumberpos'];
				 $healthprofile->medicare_exp = $request['medicare_exp'];
				 $healthprofile->heathcarecard = $request['heathcarecard'];
				 
				 $healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				 $healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				 $healthprofile->healthfundname = $request['healthfundname'];
				 $healthprofile->healthfundmembershipnumber = $request['healthfundmembershipnumber'];
				 $healthprofile->healthfundmembershipreferencenumber = $request['healthfundmembershipreferencenumber'];
				 
				 $healthprofile->safetynet = $request['safetynet'];
				 $healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				 $healthprofile->veteranaffairsexp = $request['veteranaffairsexp'];
				 $healthprofile->ihi = $request['ihi'];
				 $healthprofile->ihi_exp = $request['ihi_exp'];
				 $healthprofile->dvano = $request['dvano'];				 
				 $healthprofile->dvanoexp = $request['dvanoexp'];
				 
				 $healthprofile->smoking = $request['smoking'];
				 
				 $healthprofile->pregnency = $request['pregnency'];
				 $healthprofile->expectedduedate = $request['expectedduedate'];
				 
				 $healthprofile->lactation = $request['lactation'];
				 $healthprofile->anyallergies = $request['anyallergies'];
				 $healthprofile->allergies = $request['allergies'];
				 $healthprofile->medicalconditions = $request['medicalconditions'];
				 
				 $healthprofile->oncontraception = $request['oncontraception'];
				 $healthprofile->nameofcontraception = $request['nameofcontraception'];
				 
				 $healthprofile->weight = $request['weight'];
				 $healthprofile->medicalprocedure = $request['medicalprocedure'];
				 $healthprofile->descriptionforprocedure = $request['descriptionforprocedure'];
				 $healthprofile->procedureyear = $request['procedureyear'];
				 
				 $healthprofile->parentuser_flag = '1';
				 $healthprofile->profile_relation = 'self';
				 $healthprofile->save();
				 //$healthprofile->save();
				}
				else{
					$healthprofile = new Healthprofile;
					$healthprofile->userid =  $user->userid;
					$healthprofile->gpname = $request['gpname'];
				    $healthprofile->gpclinicname = $request['gpclinicname'];
				    $healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				    $healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				    $healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				    $healthprofile->gpclinicstate = $request['gpclinicstate'];
				    $healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				    $healthprofile->gpclinicphone = $request['gpclinicphone'];
				  
				  
				 $healthprofile->ismedicare = $request['ismedicare'];
				 $healthprofile->medicarenumber = $request['medicarenumber'];
				 $healthprofile->medicare_exp = $request['medicare_exp'];
				 $healthprofile->heathcarecard = $request['heathcarecard'];
				 
				 $healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				 $healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				 $healthprofile->healthfundname = $request['healthfundname'];
				 $healthprofile->healthfundmembershipnumber = $request['healthfundmembershipnumber'];
				 $healthprofile->healthfundmembershipreferencenumber = $request['healthfundmembershipreferencenumber'];
				 
				 $healthprofile->safetynet = $request['safetynet'];
				 $healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				 $healthprofile->veteranaffairsexp = $request['veteranaffairsexp'];
				 $healthprofile->ihi = $request['ihi'];
				 $healthprofile->ihi_exp = $request['ihi_exp'];
				 $healthprofile->dvano = $request['dvano'];				 
				 $healthprofile->dvanoexp = $request['dvanoexp'];
				 
				 $healthprofile->smoking = $request['smoking'];
				 $healthprofile->allergies = $request['allergies'];
				 $healthprofile->medicalconditions = $request['medicalconditions'];
				 
				 $healthprofile->pregnency = $request['pregnency'];
				 $healthprofile->expectedduedate = $request['expectedduedate'];
				 
				 $healthprofile->lactation = $request['lactation'];
				 
				 $healthprofile->oncontraception = $request['oncontraception'];
				 $healthprofile->nameofcontraception = $request['nameofcontraception'];
					 $healthprofile->parentuser_flag = '1';
					 $healthprofile->profile_relation = 'self';
					 $healthprofile->save();
				}
				 
				 if($healthprofile->medicarenumber!=null || $healthprofile->medicarenumber!=''){
					 $date=date("Y-m-d");
		//$healthprofile->medicarestatus=$this->common->medicareverify($user->userid,$user->firstname,$user->lastname,$user->dob,$healthprofile->medicarenumber,$date,1);
		//$healthprofile->concessionstatus=$this->common->medicareverify($user->userid,$user->firstname,$user->lastname,$user->dob,$healthprofile->medicarenumber,$date,2);
		//$healthprofile->medicarestatus=1;
		//$healthprofile->concessionstatus=1;}else{
		$healthprofile->medicarestatus=2;
		$healthprofile->concessionstatus=2;
		}
				$response->userDetails 	= $user;
				$response->healthprofile 	= $healthprofile;
				$response->AddressDetails 	= $deliveryAddress;
				$response->msg 		= trans('messages.update_profile');
				$response->status 		= $this->successStatus;
			
		 return json_encode($response);
	}
	/**
      @OA\Post(
          path="/v3/updateDeviceDetails",
          tags={"Device Details"},
          summary="update Device Details",
          operationId="updateDeviceDetails",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="deviceid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="devicetoken",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="devicetype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	public function updateDeviceDetails(Request $request)
	{
		$response=(object)array();
		$devicedetails=(object)array();
		 $user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
		// print_r($request);exit;
		 if($request->devicetoken!="" || $request->devicetype!="" || $user->userid!="")
		 {
			//$user->userid = $user->userid;
			$user->deviceid = $request->deviceid;
			$user->devicetoken = $request->devicetoken;
			$user->devicetype = $request->devicetype;
			$user->save();			
			//$response->deviceDetails 	= $devicedetails;
			$response->msg 		= "Device Details added successfully";
			$response->status 		= $this->successStatus;				
		}
		else
		{
			$response->msg 		= trans('messages.general_messgae');
			$response->status 		= $this->successStatus;
		}
		return json_encode($response);
	}
/**
      @OA\post(
          path="/v3/logout",
          tags={"User"},
          summary="User Logout",
          operationId="User Logout",
    security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function logout(Request $request)
		{ 
			$request->user()->token()->revoke();
			return response()->json([
				'message' => trans('messages.logout_messgae')
			]);
        
		}
		
	/**
      @OA\post(
          path="/v3/gplocations",
          tags={"GeneralPractioner"},
          summary="gplocations",
          operationId="gplocations",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="gpname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function gplocations(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$gpLocations = Gplocations :: where('gpname', 'like', '%'.$request->gpname.'%')->get();
			return response()->json(['gpLocations' => $gpLocations], $this->successStatus);
        
		}	
		
		public function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
    
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $base64_string ) );

        $file = public_path('profilepic/'). $output_file;
        file_put_contents($file, $ifp);
        rename($output_file, 'profilepic/'.$output_file);
        // clean up the file resource
        fclose( $ifp ); 
        return $output_file; 
    }
	
	/**
	 @OA\Post(
          path="/v3/addFamilyMember",
          tags={"User"},
          summary="Add FamilyMember",
          operationId="Add FamilyMember",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="middlename",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="knownas",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profilepic",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			 			 
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="sex",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="permissionforsms",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="occupation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ), @OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			  
			
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ismedicare",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipreferencenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="ihi",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ihi_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvano",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvanoexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="expectedduedate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="procedureyear",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profile_relation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
		 @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 
	public function addFamilyMember(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 //$user_details = User::findOrFail($user->userid);
		
			$familyMember = new User;
				
		 
				$familyMember->username = $request->username;
				$familyMember->password = $request->password;
				$familyMember->firstname = $request->firstname;
				$familyMember->middlename = $request->middlename;
				$familyMember->lastname = $request->lastname;
				$familyMember->knownas = $request->knownas;
				$familyMember->mobile = $request->mobile;
				$familyMember->permissionforsms = $request->permissionforsms;
				$familyMember->dob = $request->dob;
				 $today = date('Y-m-d');
        		 $age =  date_diff(date_create($request['dob']), date_create($today))->y;
				if($request->profilepic!= "")
				{					
					$profilepic=$this->base64_to_jpeg($request->profilepic,time()."-familipic.jpeg");				
				}
				else
				{
					$profilepic = '';
				}
				$familyMember->profilepic = $profilepic;
				$familyMember->sex = $request->sex;
				$familyMember->defaultaddress = $request->defaultaddress;
				$familyMember->homephone = $request->homephone;
				$familyMember->occupation = $request->occupation;
				$familyMember->emergencycontactname = $request->emergencycontactname;
				$familyMember->emergencycontactnumber = $request->emergencycontactnumber;
				$familyMember->occupation = $request->occupation;
				$familyMember->profiletypeid = "6";
				$familyMember->userstatus = "1";
				$familyMember->emailverified = "0";
				$familyMember->hashcode = $request->hashcode;
				$familyMember->parentuserid = $user->userid;
				
				$familyMember->save();
				$familyMember['age'] = $age;
		
		$familyuserid = $familyMember->userid;
		if($familyuserid!="")
		{
			$familyhealthprofile = new Healthprofile;
			$familyhealthprofile->userid = $familyuserid;
			$familyhealthprofile->gpname = $request->gpname;
			$familyhealthprofile->gpclinicname = $request->gpclinicname;
			$familyhealthprofile->gplink = $request->gplink;
			$familyhealthprofile->gpclinicaddressline1 = $request->gpclinicaddressline1;
			$familyhealthprofile->gpclinicaddressline2 = $request->gpclinicaddressline2;
			$familyhealthprofile->gpclinicsuburb = $request->gpclinicsuburb;
			$familyhealthprofile->gpclinicstate = $request->gpclinicstate;
			$familyhealthprofile->gpclinicpostcode = $request->gpclinicpostcode;
			$familyhealthprofile->gpclinicphone = $request->gpclinicphone;
			$familyhealthprofile->medicarenumber = $request->medicarenumber;
			$familyhealthprofile->ismedicare = $request->ismedicare;
			$familyhealthprofile->medicare_exp = $request->medicare_exp;
			$familyhealthprofile->heathcarecard = $request->heathcarecard;
			$familyhealthprofile->pensionerconcessioncard = $request->pensionerconcessioncard;
			$familyhealthprofile->concessionsafetynetcard = $request->concessionsafetynetcard;
			$familyhealthprofile->healthfundname = $request->healthfundname;
			$familyhealthprofile->healthfundmembershipnumber = $request->healthfundmembershipnumber;
			$familyhealthprofile->healthfundmembershipreferencenumber = $request->healthfundmembershipreferencenumber;
			$familyhealthprofile->ihi = $request->ihi;
			$familyhealthprofile->ihi_exp = $request->ihi_exp;
			$familyhealthprofile->dvano = $request->dvano;
			$familyhealthprofile->dvanoexp = $request->dvanoexp;
			$familyhealthprofile->veteranaffairsnumber = $request->veteranaffairsnumber;
			$familyhealthprofile->veteranaffairsexp = $request->veteranaffairsexp;
			$familyhealthprofile->safetynet = $request->safetynet;
			$familyhealthprofile->anyallergies = $request->anyallergies;
			$familyhealthprofile->allergies = $request->allergies;
			$familyhealthprofile->medicalconditions = $request->medicalconditions;
			$familyhealthprofile->pregnency = $request->pregnency;
			$familyhealthprofile->expectedduedate = $request->expectedduedate;
			$familyhealthprofile->lactation = $request->lactation;
			$familyhealthprofile->smoking = $request->smoking;
			$familyhealthprofile->oncontraception = $request->oncontraception;
			$familyhealthprofile->nameofcontraception = $request->nameofcontraception;
			$familyhealthprofile->weight = $request->weight;
			$familyhealthprofile->medicalprocedure = $request->medicalprocedure;
			$familyhealthprofile->descriptionforprocedure = $request->descriptionforprocedure;
			$familyhealthprofile->procedureyear = $request->procedureyear;
			$familyhealthprofile->profile_relation = $request->profile_relation;
			$familyhealthprofile->parentuser_flag = "0";
			$familyhealthprofile->save();
		}
		
			$familyprofile_completeness=0;
			 if(isset($familyMember->firstname)){$familyprofile_completeness=$familyprofile_completeness+20;}
			 if(isset($familyMember->lastname)){$familyprofile_completeness=$familyprofile_completeness+10;}
			 if(isset($familyMember->dob)){$familyprofile_completeness=$familyprofile_completeness+10;}
			 if(isset($familyMember->mobile)){$familyprofile_completeness=$familyprofile_completeness+10;}
			 if(isset($familyMember->profilepic) && $familyMember->profilepic!='')
			 {$familyprofile_completeness=$familyprofile_completeness+30;}
			 if(isset($familyhealthprofile->medicarenumber) || isset($familyhealthprofile->heathcarecard) || isset($familyhealthprofile->veteransaffairscard) || isset($familyhealthprofile->safetynet))
			 {$familyprofile_completeness=$familyprofile_completeness+20;}
		
			
		$response->familyMember 	= $familyMember;
		$response->familyhealthprofile 	= $familyhealthprofile;
		$response->familyprofile_completeness 	= $familyprofile_completeness;
		
		return json_encode($response);
		
	}
	
	/**
	 @OA\Post(
          path="/v3/updateFamilyMember",
          tags={"User"},
          summary="Update FamilyMember",
          operationId="Update FamilyMember",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="middlename",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lastname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="knownas",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profilepic",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			 			 
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="sex",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="permissionforsms",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="occupation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="emergencycontactnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),			  
			
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="ismedicare",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="healthfundmembershipreferencenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="ihi",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ihi_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvano",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dvanoexp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="expectedduedate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="procedureyear",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="profile_relation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
		 @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 
	public function updateFamilyMember(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 //$user_details = User::findOrFail($user->userid);
		
		if($request->userid !="" && $request->profileid !="")
		{			
				
		 if ( User::where('userid',  $request->userid)->where('parentuserid' , $user->userid)->where('userstatus' , "1")->exists()) {
				$familyMember = User::where('userid',  $request->userid)->where('parentuserid' , $user->userid)->first();
			$familyMember->username = $request->username;
			$familyMember->password = $request->password;
			$familyMember->firstname = $request->firstname;
			$familyMember->middlename = $request->middlename;
			$familyMember->lastname = $request->lastname;
			$familyMember->knownas = $request->knownas;
			$familyMember->mobile = $request->mobile;
			$familyMember->permissionforsms = $request->permissionforsms;
			$familyMember->dob = $request->dob;
			$today = date('Y-m-d');
        	$age =  date_diff(date_create($request['dob']), date_create($today))->y;
			if($familyMember->profilepic == "" && $request['profilepic'] !="")
			{
			
				$familyMember->profilepic=$this->base64_to_jpeg($request['profilepic'],time()."-familipic.jpeg");
				
			}
			 elseif($familyMember->profilepic !="" || !empty($familyMember->profilepic))
				 {
					 
					 $supported_image = array(						
						'jpeg'
					);

					$src_file_name = $request['profilepic'];
					$ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
					if (in_array($ext, $supported_image))
					{	
						//echo "12fdsfds3";exit;
					 $familyMember->profilepic = $familyMember->profilepic;
					}
					else
					{
						//echo "123";exit;
					$familyMember->profilepic=$this->base64_to_jpeg($request['profilepic'],time()."-familipic.jpeg");
					}
				 }
			elseif($request['profilepic'] =="")
			{
				$familyMember->profilepic = "";
			}
			
			$familyMember->sex = $request->sex;
			$familyMember->defaultaddress = $request->defaultaddress;
			$familyMember->homephone = $request->homephone;
			$familyMember->occupation = $request->occupation;
			$familyMember->emergencycontactname = $request->emergencycontactname;
			$familyMember->emergencycontactnumber = $request->emergencycontactnumber;
			$familyMember->occupation = $request->occupation;
			$familyMember->profiletypeid = "6";
			$familyMember->userstatus = "1";
			$familyMember->emailverified = "0";
			$familyMember->hashcode = $request->hashcode;
			$familyMember->parentuserid = $user->userid;
			
			$familyMember->save();
			
        	$familyMember['age'] =	 $age;
		
		$familyuserid = $familyMember->userid;
		
		if ( Healthprofile::where('userid',  $request->userid)->where('parentuser_flag' , '0')->exists()) {
				$healthprofile = Healthprofile::where('userid',  $request->userid)->where('parentuser_flag' , '0')->first();
					
				$healthprofile->gpname = $request['gpname'];
				$healthprofile->gpclinicname = $request['gpclinicname'];
				$healthprofile->gplink = $request['gplink'];
				$healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				$healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				$healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				$healthprofile->gpclinicstate = $request['gpclinicstate'];
				$healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				$healthprofile->gpclinicphone = $request['gpclinicphone'];


				$healthprofile->ismedicare = $request['ismedicare'];
				$healthprofile->medicarenumber = $request['medicarenumber'];
				$healthprofile->medicare_exp = $request['medicare_exp'];
				$healthprofile->heathcarecard = $request['heathcarecard'];

				$healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				$healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				$healthprofile->healthfundname = $request['healthfundname'];
				$healthprofile->healthfundmembershipnumber = $request['healthfundmembershipnumber'];
				$healthprofile->healthfundmembershipreferencenumber = $request['healthfundmembershipreferencenumber'];

				$healthprofile->safetynet = $request['safetynet'];
				$healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				$healthprofile->veteranaffairsexp = $request['veteranaffairsexp'];
				$healthprofile->ihi = $request['ihi'];
				$healthprofile->ihi_exp = $request['ihi_exp'];
				$healthprofile->dvano = $request['dvano'];				 
				$healthprofile->dvanoexp = $request['dvanoexp'];

				$healthprofile->smoking = $request['smoking'];

				$healthprofile->pregnency = $request['pregnency'];
				$healthprofile->expectedduedate = $request['expectedduedate'];

				$healthprofile->lactation = $request['lactation'];
				$healthprofile->anyallergies = $request['anyallergies'];
				$healthprofile->allergies = $request['allergies'];
				$healthprofile->medicalconditions = $request['medicalconditions'];
				$healthprofile->oncontraception = $request['oncontraception'];
				$healthprofile->nameofcontraception = $request['nameofcontraception'];

				$healthprofile->weight = $request['weight'];
				$healthprofile->medicalprocedure = $request['medicalprocedure'];
				$healthprofile->descriptionforprocedure = $request['descriptionforprocedure'];
				$healthprofile->procedureyear = $request['procedureyear'];

				//$healthprofile->parentuser_flag = '0';
				$healthprofile->profile_relation = $request['profile_relation'];
				$healthprofile->save();
				}
				
						
		 
			$response->familyMember 	= $familyMember;
			$response->familyhealthprofile 	= $healthprofile;
			$response->msg 		= trans('messages.update_profile');
			$response->status 		= $this->successStatus;
		}
		else
		 {
			 $response->msg 		= trans('messages.general_messgae');
			$response->status 		= $this->successStatus;
		 }
			
			return json_encode($response);
		}
	}
	
	/**
      @OA\Get(
          path="/v3/getAllProfiles",
          tags={"User"},
          summary="All Profiles",
          operationId="All Profiles",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getAllProfiles() {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		
		$profile = User :: leftJoin('healthprofile','healthprofile.userid', '=' ,'users.userid')
							 ->where('users.parentuserid', '=',  $user->userid);
							//->union($userProfile);
							 //->get();
		
		$profileDetails = User :: leftJoin('healthprofile','healthprofile.userid', '=' ,'users.userid')
							 ->where('users.userid', '=',  $user->userid)
							 ->union($profile)
							 ->get();
		$numrows = $profileDetails->count();
		for($i=0;$i<$numrows;$i++)
		{
			$today = date('Y-m-d');
			$age[$i] =  date_diff(date_create($profileDetails[$i]['dob']), date_create($today))->y;
			$profileDetails[$i]['age'] =$age[$i];
		}
		$response->profileDetails 	=  $profileDetails;		
		$response->status 		= $this->successStatus;
		
		
		return json_encode($response); 
	}
	
	/**
      @OA\Post(
          path="/v3/updatehealthprofile",
          tags={"User"},
          summary="Update Healthprofile",
          operationId="Update Healthprofile",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			@OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="heathcarecard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="weight",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="smoking",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pregnency",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="oncontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="nameofcontraception",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="descriptionforprocedure",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="lactation",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="anyallergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="allergies",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicalconditions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="dob",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			  			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	public function updatehealthprofile(Request $request)
	{
		if ( Healthprofile::where('profileid',  $request->profileid)->exists()) {
					$healthprofile = Healthprofile::where('profileid',  $request->profileid)->first();
				
				$userdetails = User::where('userid',  $healthprofile->userid)->get();
				$bday = $userdetails[0]['dob'];
				$today = date('Y-m-d');
        		$age =  date_diff(date_create($bday), date_create($today))->y;	
				$healthprofile->ismedicare = $request['ismedicare'];
				$healthprofile->medicarenumber = $request['medicarenumber'];
				$healthprofile->medicare_exp = $request['medicare_exp'];
				$healthprofile->heathcarecard = $request['heathcarecard'];
				$healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				$healthprofile->safetynet = $request['safetynet'];
				$healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				
				$healthprofile->weight = $request['weight'];
				$healthprofile->smoking = $request['smoking'];
				$healthprofile->pregnency = $request['pregnency'];
				$healthprofile->oncontraception = $request['oncontraception'];
				$healthprofile->nameofcontraception = $request['nameofcontraception'];
				$healthprofile->descriptionforprocedure = $request['descriptionforprocedure'];
				$healthprofile->lactation = $request['lactation'];
				$healthprofile->anyallergies = $request['anyallergies'];
				$healthprofile->allergies = $request['allergies'];
				$healthprofile->medicalconditions = $request['medicalconditions'];
				$healthprofile->save();
				$healthprofile['age'] = $age ;
				
				return response()->json(['healthprofile' => $healthprofile], $this->successStatus);
		}
	}
	
	/**
      @OA\post(
          path="/v3/updategpinfo",
          tags={"User"},
          summary="Update gplocations Info",
          operationId="Update gplocations Info",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="gpname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ), @OA\Parameter(
              name="gplink",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline1",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicaddressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicsuburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicstate",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicpostcode",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpclinicphone",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function updategpinfo(Request $request)
		{ 
			if ( Healthprofile::where('profileid',  $request->profileid)->exists()) {
					$healthprofile = Healthprofile::where('profileid',  $request->profileid)->first();
				
				  $userdetails = User::where('userid',  $healthprofile->userid)->get();	
				  
				  $healthprofile->gpclinicname = $request['gpclinicname'];
				  $healthprofile->gpclinicaddressline1 = $request['gpclinicaddressline1'];
				  $healthprofile->gpclinicaddressline2 = $request['gpclinicaddressline2'];
				  $healthprofile->gpclinicsuburb = $request['gpclinicsuburb'];
				  $healthprofile->gpclinicstate = $request['gpclinicstate'];
				  $healthprofile->gpclinicpostcode = $request['gpclinicpostcode'];
				  $healthprofile->gpclinicphone = $request['gpclinicphone'];
				  $healthprofile->gplink = $request['gplink'];
				  $healthprofile->save();
				
				return response()->json(['healthprofile' => $healthprofile], $this->successStatus);
			}
        
		}
		
		/**
      @OA\post(
          path="/v3/medicalconditions",
          tags={"User"},
          summary="Medical Conditions",
          operationId="Medical Conditions",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="name",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function medicalconditions(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$medicalconditions = Medicalconditions :: where('name', 'like', '%'.$request->name.'%')->get();
			return response()->json(['medicalconditions' => $medicalconditions], $this->successStatus);
        
		}
		
		/**
      @OA\post(
          path="/v3/allergies",
          tags={"User"},
          summary="Allergies",
          operationId="Allergies",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="allergyname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function allergies(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$allergies = Allergies :: where('allergyname', 'like', '%'.$request->allergyname.'%')->get();
			return response()->json(['allergies' => $allergies], $this->successStatus);
        
		}
		
		
		/**
      @OA\post(
          path="/v3/vaccination",
          tags={"User"},
          summary="Vaccin Information",
          operationId="Vaccin Information",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="vaccinename",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function vaccination(Request $request)
		{ 
			$user = Auth::user();
			$user_details = User::findOrFail($user->userid);
			$vaccination = Immunisation :: where('vaccinename', 'like', '%'.$request->vaccinename.'%')->get();
			return response()->json(['vaccination' => $vaccination], $this->successStatus);
        
		}
		
		
		/**
      @OA\post(
          path="/v3/forgotpassword",
          tags={"User"},
          summary="Forgotpassword",
          operationId="Forgotpassword",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function forgotpassword(Request $request)
		{ 
		$response=(object)array();
		
			$users_existance=User :: where('username',$request->username)
									->where('userstatus','=',1)
									->where('profiletypeid','=',1)
									->get();
			if($users_existance->count() > 0)
			{				
			if($users_existance[0]['username'] != $request->username)
			{
				$response->msg 		= trans('messages.email_not_exist');
				$response->status 		= $this->successStatus; 
			}
			else
			{
				$link=url('resetpass/'.$users_existance[0]['hashcode']);
								
				$subject=env('ENV')." Medmate – Forgot your password";
				
				//$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Reset Password</button></a>";
				//$body_message = trans('messages.Forgot_password_email');
				//$body_message 	= 	str_replace('Reset_password_button',$content,$body_message);
				$body_message 	= 	'
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>password reset</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Password Reset</h1>  
                                                <p>Forgot your password? No problem. Click the link below to create a new one. </p>
                                                <center>
                                                <a href="'.$link.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/reset-password-btn.png" alt="View Order" />
                                                </a>
                                                </center>
                                                <p>Didn’t ask to reset your password? No worries. Ignore this email and we’ll keep it the same.</p>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
				';
				$message['to']           = $users_existance[0]['username'];
                $message['subject']      = $subject;
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			}
			}
			else
			{
				$response->msg 		= trans('messages.email_sent');
				$response->status 		= $this->successStatus; 
			}
			
			return json_encode($response); 
		}
		
		public function testPush(Request $request){
            $this->common = new CommonController($request);
            $response=(object)array();
             if($request->devicetype==1){
            //$registration_ids[0]=  'fyWvqFdv3DI:APA91bEJ7RSmmoZTXuLREnGeeAKHMoJ_3PCo5WkfobO1k6xWcpZP15fp4qkxh274WkAfFMjzFkHMvEDP3K4KnrgBNNFfQ_zyghsJ2Bqrckp6KgzAUqnkqYBjkHxGCRZD6g6tUQVe9K5R';
            $registration_ids[0]=  $request->device_token;
                  // $data=array('body' => 'Hello world');
                    $data= (object) array(
                        'google.delivered_priority'           => 'high',
                         'google.original_priority'      => "high",
                         'orderid'        => "12",
						 'usertoken' =>"e1Armo1o9kJjq6nOU4i_Lb:APA91bHH6xYPFEOXarHG3ho_4Za50T6UTxlMqxaamw4qukgOw_2SlHtOzmftTbYWhnfWFPua193ZXFSrM3y0y-3inzrQTAaBzT86keX3YBIgFrnKKk0foumlb4VNz74Hum_oy0F4B8TV"
                          );
						  
   
                   $fields = array('registration_ids' => $registration_ids,'body'=>'helloworld','data' => $data,"notificationType" => "1", "title"=> "test",'priority' => 'high', 'notification' => array(
        'title' => 'This is title',
        'body' => 'This is body'
    ));
             $msgEn = 'hello ';
            // $msgAr = 'h12';
			print_r(json_encode($fields)); 
             $re=$this->common->sendPushNotification($fields);
			// print_r($re);exit;
			 }elseif($request->devicetype==2){
            // print_r($fields); die('end');
            //$this->common->notification('en',38,2,$msgEn,$msgAr,100,1);
            $this->common->iPhonePushNotification('hello world',$request->devicetoken,'C','test','1');
            
             }
             $response->msg 		= "success";
             $response->status 		= $this->successStatus; 
             return json_encode($response);    
        }
		
	/**
      @OA\Get(
          path="/v3/getNotifications",
          tags={"Notifications"},
          summary="Notifications",
          operationId="Notifications",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getNotifications() {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);

		$notifications = Notifications :: where('userid', '=', $user->userid)->where('status','=',1)->orderBy('notificationid', 'desc')->get();
			return response()->json(['notifications' => $notifications], $this->successStatus);
		
		
	}
	
	/**
      @OA\Get(
          path="/v3/getNotificationsCount",
          tags={"Notifications"},
          summary="Notifications Count",
          operationId="Notifications Count",
   security={

      {"bearerAuth": {}}
    },
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getNotificationsCount() {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);

		$notifications = Notifications :: where('userid', '=', $user->userid)->where('isopen','=',1)->get();
		$notificicationscount = $notifications->count();
			return response()->json(['notificicationscount' => $notificicationscount], $this->successStatus);
		
		
	}
	
	/**
      @OA\post(
          path="/v3/updatenotification",
          tags={"Notifications"},
          summary="Update Notifications",
          operationId="Update Notifications",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="notificationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function updatenotification(Request $request)
		{ 
			$notifications = Notifications :: where('notificationid', '=', $request->notificationid)
												->update(['notifications.isopen' => '0']);
			
			
			return response()->json(['notifications' => $notifications], $this->successStatus);
		}
	/**
      @OA\Get(
          path="/v3/getFamilyMember",
          tags={"User"},
          summary="FamilyUserProfile",
          operationId="FamilyUser Profile",
   security={

      {"bearerAuth": {}}
    },
	
	@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     @OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */

	public function getFamilyMember(Request $request) {
		$response=(object)array();
		$user = Auth::user();
		$userdetails = User :: where('userid', $request->userid)
								->where('userstatus' , 1)
								->where('parentuserid' , $user->userid)
								->get();
		$healthprofile = Healthprofile::where('userid', $request->userid)->get();
		
		$today = date('Y-m-d');
        $age =  date_diff(date_create($userdetails[0]['dob']), date_create($today))->y;
		if(!empty($userdetails[0]['profilepic']))
		{
			$userdetails[0]['profilepic'] = url('/profilepic/'.$userdetails[0]['profilepic']);
		}
		elseif($userdetails[0]['profilepic'] == "")
		{
			$userdetails[0]['profilepic'] ="";
		}
		
		$family_profile_completeness=0;
		if(isset($userdetails[0]['firstname'])){$family_profile_completeness=$family_profile_completeness+20;}
		if(isset($userdetails[0]['lastname'])){$family_profile_completeness=$family_profile_completeness+10;}
		if(isset($userdetails[0]['dob'])){$family_profile_completeness=$family_profile_completeness+10;}
		if(isset($userdetails[0]['mobile'])){$family_profile_completeness=$family_profile_completeness+10;}
		if(isset($userdetails[0]['profilepic'])){$family_profile_completeness=$family_profile_completeness+30;}
		if(isset($healthprofile[0]['medicarenumber']) || isset($healthprofile[0]['heathcarecard']) || isset($healthprofile[0]['veteransaffairscard']) || isset($healthprofile[0]['safetynet']) || isset($healthprofile[0]['pensionerconcessioncard']) || isset($healthprofile[0]['concessionsafetynetcard']))
		{$family_profile_completeness=$family_profile_completeness+20;}
		
		
		$response->family_profile_completeness 	=  $family_profile_completeness;
		$response->userdetails 	=  $userdetails;
		$response->healthprofile 	=  $healthprofile;
		$response->age 	=  $age;
		
		$response->msg 		= "success";
		$response->status 		= $this->successStatus; 
		return json_encode($response);
	}
	/**
      @OA\Post(
          path="/v3/emailverification",
          tags={"User"},
          summary="Email Verification",
          operationId="Email Verification",
   security={

      {"bearerAuth": {}}
    },
	
	@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
 
		  
      )
     */
	public function emailverification(Request $request)
	{
		$response=(object)array();
		
		$user = User :: where('userid',$request->userid)
						->where('profiletypeid',1)
						->where('userstatus',1)
						->get();
						
		if($user->count() > 0){
				$link=url('verifyaccount/'.base64_encode($request->userid));
				
				$subject=env('ENV')." Medmate – Complete your registration";
				
				//$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Verify Email</button></a>";
						  // $body_message = trans('messages.Verify_Email_Template');
                   
              // $body_message 	= 	str_replace('verify_button',$content,$body_message);
				$body_message ='
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Subject</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
			#bodyCell{padding:20px 0;}
			#templateContainer{width:500px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: center;
			}
			.bodyContent {
				text-align: center;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 14px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:500px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 600px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
					max-width:500px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                			<td width="100%" height="24" style="height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                        </tr> 
                                    	<tr>
                                			<td width="100%" height="20" style="height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                    	<tr>
                                			<td width="50" height="140" style="height:140px;width:50px;">&nbsp;</td>
                                			<td height="140" style="height:140px;">&nbsp;</td>
                                			<td width="50" height="140" style="height:140px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent">
                                                <h1 style="margin-bottom: 27px;">Hi '.$user[0]['firstname'].',</h1>
												<h2 style="font-size: 20px;font-weight: 500;font-style: normal;letter-spacing: normal;line-height: 30px;margin-bottom: 12px;">To complete your Medmate registration please verify your email.</h2>
												<div class="line">&nbsp;</div>
                                                <p style="margin: 0 0 18px;">&nbsp;</p>
                                                <a href="'.$link.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/verify-button.png" alt="Verify Email" />
                                                </a>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="108" style="height:108px;width:50px;">&nbsp;</td>
                                			<td height="140" style="height:108px;">&nbsp;</td>
                                			<td width="50" height="108" style="height:108px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td>
                                	<!-- BEGIN PREFOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreFooter">
                                    	<tr>
                                			<td width="100%" height="22" style="height:22px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td valign="top" class="preFooter">
                            					Have a question? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a>
                                            </td>
                                        </tr>
                                    	<tr>
                                			<td width="100%" height="22" style="height:22px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END PREFOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->


                    	<!-- BEGIN FOOTER // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                        	<tr>
                    			<td width="100%" height="25" style="height:25px;">&nbsp;</td>
                        	</tr>
                            <tr>
                            	<td align="center" class="footerContent">
                            		© 2020 Medmate Pty Ltd
                            	</td>
                            </tr>
                        	<tr>
                    			<td width="100%" height="25" style="height:25px;">&nbsp;</td>
                        	</tr>
                        </table>
                        <!-- // END FOOTER -->
                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
				';
               $message['to']           = $user[0]['username'];
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   
				$response->msg 		= "success";
				$response->status 	= $this->successStatus;                
			}
			else
			{
				$response->msg 		= "Userid is not Valid";
				$response->status 	= $this->successStatus;
			}
			return json_encode($response);
	}
	
	/**
      @OA\post(
          path="/v3/updatemedicareinfo",
          tags={"User"},
          summary="Update Medicare Info",
          operationId="Update Medicare Info",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="ismedicare",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="profileid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
		@OA\Parameter(
              name="medicarenumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medicare_exp",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
 	    @OA\Parameter(
              name="heathcarecard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pensionerconcessioncard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="concessionsafetynetcard",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="veteranaffairsnumber",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="safetynet",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function updatemedicareinfo(Request $request)
		{ 
			if ( Healthprofile::where('profileid',  $request['profileid'])->exists()) {
					$healthprofile = Healthprofile::where('profileid',  $request['profileid'])->first();
				
				  $userdetails = User::where('userid',  $request['userid'])->get();	
				  
				$healthprofile->ismedicare = $request['ismedicare'];
				$healthprofile->medicarenumber = $request['medicarenumber'];
				$healthprofile->medicare_exp = $request['medicare_exp'];
				$healthprofile->heathcarecard = $request['heathcarecard'];
				$healthprofile->pensionerconcessioncard = $request['pensionerconcessioncard'];
				$healthprofile->safetynet = $request['safetynet'];
				$healthprofile->concessionsafetynetcard = $request['concessionsafetynetcard'];
				$healthprofile->veteranaffairsnumber = $request['veteranaffairsnumber'];
				$healthprofile->save();
				
				return response()->json(['healthprofile' => $healthprofile], $this->successStatus);
			}
        
		}
		
		private function checkaddress($addline1,$addline2,$suburb,$postcode,$state)
  {
	$addline1 = str_replace(" ","",$addline1);
	$addline2 = str_replace(" ","",$addline2);
	$suburb = str_replace(" ","",$suburb);
	$state = str_replace(" ","",$state);
	$address = $addline1.'+'.$suburb.'+'.$postcode.'+'.$state;
	$curl = curl_init();
	$url="https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyCvYESmh7woXSFQnOLOr9WhN-A_GYUEKhE";
	curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
));

$response = curl_exec($curl);

curl_close($curl);
$response=json_decode($response);
// print_r($response);exit;
return $response->status;
  }
  
/**
      @OA\Post(
          path="/v3/adduserlocation",
          tags={"adduserlocation"},
          summary="update userlocation",
          operationId="update userlocation",
		   security={{"bearerAuth": {}}},
   		  
			  @OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	public function adduserlocation(Request $request)
	{
		$response=(object)array();
		
		 $user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
		
			$user->currentlocationid = $request->locationid;			
			$user->save();			
			
			$response->msg 		= "User Current Location added successfully.";
			$response->status 		= $this->successStatus;				
		
		return json_encode($response);
	} 
	/**
      @OA\get(
          path="/v3/getusercurrentlocation",
          tags={"getusercurrentlocation"},
          summary="get user current location",
          operationId="get user current location",
		   security={{"bearerAuth": {}}},			 
			  			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	public function getusercurrentlocation(Request $request)
	{
		//$response=(object)array();
		
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		$usercurrentlocation = User :: select('currentlocationid')->where('userid' , $user->userid)->where('userstatus' , 1)->get();			
		$location = Locations :: where('locationid',$usercurrentlocation[0]['currentlocationid'])->get();				
		$lastselectaddress = Deliveryaddress:: where('deliveryaddresses.userid', '=', $user->userid)->where('currentselectaddress',1)->where('deliveryaddresssts',1)->get();
		return response()->json(['usercurrentlocation' => $usercurrentlocation,'locationdetails' => $location,'lastselectaddress' => $lastselectaddress], $this->successStatus);
	} 
	/**
      @OA\get(
          path="/v3/changecurrentlocation",
          tags={"changecurrentlocation"},
          summary="change user current location",
          operationId="change user current location",
		   security={{"bearerAuth": {}}},			 
			  			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	public function changecurrentlocation(Request $request)
	{
		$response=(object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		//$usercurrentlocation = User :: select('currentlocationid')->where('userid' , $user->userid)->where('userstatus' , 1)->get();			
		//$location = Locations :: where('locationid',$usercurrentlocation[0]['currentlocationid'])->get();				
		//$lastselectaddress = Deliveryaddress:: where('deliveryaddresses.userid', '=', $user->userid)->where('currentselectaddress',1)->where('deliveryaddresssts',1)->get();
		$lastselectaddress = Deliveryaddress:: where('deliveryaddresses.userid', '=', $user->userid)->where('currentselectaddress',1)->where('deliveryaddresssts',1)->update(['currentselectaddress'=>0]);
		$usercurrentlocation = User :: where('userid' , $user->userid)->where('userstatus' , 1)->update(['currentlocationid'=>null]);				
		$response->msg 		= "User Current Location added successfully.";
		$response->status 		= $this->successStatus;				
		return json_encode($response);
		
	}
}
