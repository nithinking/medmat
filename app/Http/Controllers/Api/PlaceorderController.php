<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Orderdetails; 
use App\models\Basket; 
use App\models\Cart; 
use App\models\Ordertracking; 
use App\models\Orderinvoicelist; 
use App\models\Invoice; 
use App\models\Orderbasketlist; 
use App\models\Basketitemlist; 
use App\models\Prescriptiondetails; 
use App\models\Prescriptionitemdetails; 
use App\models\Tokenswallet; 
use App\models\Orderprescriptionitemdetails; 
use App\models\Healthprofile; 
use App\models\Deliveryaddress; 
use App\models\Locations; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class PlaceorderController extends Controller
{
    public $successStatus = 200;
	/**
      @OA\Post(
          path="/v2/placeOrder",
          tags={"place Order"},
          summary="place Order",
          operationId="place Order",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="prescriptionid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="ordertype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="userpreffereddate",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="userprefferedtime",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="orderinstructions",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ), 
		  @OA\Parameter(
              name="orderTotal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="deliveryfee",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="isGeneric",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),
		 @OA\Parameter(
              name="speakwithpharmacist",
              in="query",
              required=true,
              @OA\Schema(
                  type="integer"
              )
          ),
		  
		  @OA\Parameter(
              name="deliveryaddressid",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),
		  	
		  	  @OA\Parameter(
              name="itemimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),
		  
		  	@OA\Parameter(
              name="usermobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="integer"
              )
          ),  
         
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
    public function placeOrder(Request $request)
	{
		
		
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get();
		//$cart = Cart :: where ('enduserid', $user->userid)->where('cartstatus', '1')->get();
		//$noofrows = $cart->count();
		
		$cart =$request->cartitems;
        $locationname = Locations :: where('locationid','=',$request->locationid)->get();
		date_default_timezone_set($locationname[0]['timezone']);
		$orderDetails = new Orderdetails;
						
			$orderDetails->uniqueorderid = mt_rand(100000, 999999).$user->userid;
			$orderDetails->enduserid = $user->userid;
			$orderDetails->locationid = $request->locationid;
			$orderDetails->deliveryaddressid = $request->deliveryaddressid;
			$orderDetails->orderinstructions = $request->orderinstructions;
			$orderDetails->isGeneric = $request->isGeneric;
			$orderDetails->orderstatus = "Waiting For Review";
			$orderDetails->orderTotal = $request->orderTotal;
			$orderDetails->speakwithpharmacist = $request->speakwithpharmacist;
			$orderDetails->orderDate = date('Y-m-d H:i:s');			
			$orderDetails->ordertype = $request->ordertype;
			
			
			$orderDetails->deliveryfee = $request->deliveryfee;
			
			$orderDetails->userpreffereddate = $request->userpreffereddate;
			$orderDetails->userprefferedtime = $request->userprefferedtime;
			$orderDetails->isnew = 1;
			$orderDetails->save();
			
			$orderid = $orderDetails->id;
			
			if(isset($request->usermobile) &&  $request->usermobile!= "")
            {
                $usermobileupdate = User :: where('users.userid', '=', $user->userid)
                                            ->update(['users.mobile' => $request->usermobile]);
            }
            
			
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $orderid;
			$orderTracking->orderstatus = "Waiting For Review";
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	
		if($orderid!="")
			{	
		for($i=0;$i<count($cart);$i++){
		//print_r($orderid);exit;
			
			$basket = new Basket;
			$basket->profileid = $cart[$i]['profileid'];
			$basket->orderid = $orderid;
			$basket->createddate = date('Y-m-d');;
			$basket->scriptid  = $cart[$i]['prescriptionid'];
			if(!empty($cart[$i]['referrencetable']))
			$basket->refferencetable  = $cart[$i]['referrencetable'];
		else
			$basket->refferencetable  ='';
			//$basket->catalogueid  = $cart[$i]['prescriptionid'];
			$basket->medlistid  = $cart[$i]['medlistid'];
			$basket->basketstatus  = 1;
			$basket->save();
			//print_r($basket);exit;
			$basketid = $basket->id;
			
			$basketitemlist = new Basketitemlist;
			$basketitemlist->basketid = $basketid;
			$basketitemlist->itemstatus = '1';
			$basketitemlist->save();			
			
			$invoice = new Invoice;
			$invoice->orderid = $orderid;
			$invoice->userid = $cart[$i]['userid'];
			$invoice->profileid = $cart[$i]['profileid'];			
			$invoice->invoicestatus = "1";			
			$invoice->save();				
			$invoiceid = $invoice->id;
												 
			$orderbasketlist = new Orderbasketlist;
			$orderbasketlist->orderid = $orderid;
			$orderbasketlist->userid = $cart[$i]['userid']; 
			$orderbasketlist->profileid = $cart[$i]['profileid'];
			$orderbasketlist->basketid  = $basketid;
			$orderbasketlist->locationid  = $request->locationid;
			$orderbasketlist->deliveryaddressid  = $request->deliveryaddressid;
			$orderbasketlist->prescriptionid   = $cart[$i]['prescriptionid'];
			$orderbasketlist->medlistid   = $cart[$i]['medlistid'];
			$orderbasketlist->drugname   = $cart[$i]['name'];
			$orderbasketlist->drugquantity   = $cart[$i]['quantity'];
			$orderbasketlist->drugpack   = $cart[$i]['drugpack'];
			$orderbasketlist->prescriptiontype   = $cart[$i]['prescriptiontype'];
			$orderbasketlist->basketliststatus   = 1;
			$orderbasketlist->isscript   = $cart[$i]['scriptavailable'];
			$orderbasketlist->itemimage   = $cart[$i]['image'];
			if($cart[$i]['price'] == 'NA')
			{
			$orderbasketlist->originalprice   = '0.00';
			}
			else
			{
			$orderbasketlist->originalprice   = $cart[$i]['price'];
			}
			$orderbasketlist->save();
				
			$orderinvoicelist = new Orderinvoicelist;
			$orderinvoicelist->invoiceid = $invoiceid;
			$orderinvoicelist->orderid  = $orderid;
			$orderinvoicelist->orderinvoicestatus = "1";
			$orderinvoicelist->save();
			
			if($cart[$i]['prescriptionid'] !="")
			{
			
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cart[$i]['prescriptionid'].'"'))
															  ->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
																->update(['prescriptionitemdetails.medstatus' => 'Ordered']);
			
			//To Check if uploaded script is related Medlist Medication script
			$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$cart[$i]['prescriptionid'].'"'))
													->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
													->get();
			$count = $getmedlistid->count();
			if($count > 0)
			{
				if($count == 1)
				$medlistdetails = $getmedlistid[0]['medilistid'];
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
												->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
												->update(['prescriptionitemdetails.medstatus' => 'Ordered']);
			}
		
			}
			else
			{
				
			$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $cart[$i]['medlistid'])
												->where('prescriptionitemdetails.profileid', '=', $cart[$i]['profileid'])
												->update(['prescriptionitemdetails.medstatus' => 'Ordered']);
			}
			
		}
			$locationname = Locations :: where('locationid','=',$request->locationid)->get();
			$basketdetails = Basket :: where('orderid' , $orderid)->get();
			$invoicedetails = Invoice :: where('orderid' , $orderid)->get();
			$orderbasketlistdetails = Orderbasketlist :: where('orderid' , $orderid)->get();
			$getuniqueid = Orderdetails :: where('orderid' , $orderid)->get();
			$orderinvoicelistdetails = Orderinvoicelist :: where('orderid' , $orderid)->get();
			$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$request->deliveryaddressid)->get();
			
			$removeCart = Cart :: where('enduserid', $user->userid)->delete();
			
			$msg = "Great work, your order has been sent to '".$locationname[0]['locationname']."'. The Pharmacy will review & confirm your order and update the availability of items and price if required.";
			
			
			$response->orderDetails 	= $orderDetails;
			$response->orderTracking 	= $orderTracking;
			$response->basket 	= $basketdetails;
			$response->orderbasketlist = $orderbasketlistdetails;			
			$response->msg 		= $msg;
			$response->status 		= $this->successStatus;
			
			$this->placeorderemails($request->locationid,$orderid,$request->deliveryaddressid,$user->userid,$request->userpreffereddate,$request->userprefferedtime,$request->deliveryfee,$request->orderTotal);
			 
		
		
			
		}
		else
		{
			$response->msg 		= trans('messages.required_message');
			$response->status 		= $this->successStatus;
		}
		
			
		return json_encode($response);
	}
	
	public function placeorderemails($locationid, $orderid, $deliveryaddressid,$userid,$userpreffereddate,$userprefferedtime,$deliveryfee,$ordertotal)
	{
		$user = Auth::user();
		//$user_details = User::findOrFail($user->userid);
		$user_details = User :: where('userid', $userid)->get();
		
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$locationname = Locations :: where('locationid','=',$locationid)->get();
		$basketdetails = Basket :: where('orderid' , $orderid)->get();
		$invoicedetails = Invoice :: where('orderid' , $orderid)->get();
		$orderbasketlistdetails = Orderbasketlist :: where('orderid' , $orderid)->get();
		$getuniqueid = Orderdetails :: where('orderid' , $orderid)->get();
		$orderinvoicelistdetails = Orderinvoicelist :: where('orderid' , $orderid)->get();
		$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$deliveryaddressid)->get();
		
		//Pharmacy Email
		
			$orderday = $userpreffereddate;
			$orderdate = date('d F Y', strtotime($userpreffereddate));
			$placeddate = date('d F Y', strtotime($getuniqueid[0]['orderDate']));
			$link = "https://pharmacy.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			$subject="Medmate – New Order Request #".$getuniqueid[0]['uniqueorderid'];				
				
			$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><span>New Order Received</span> <br> <span style="color: black;font-size:large">Order : '.$getuniqueid[0]['uniqueorderid'].'</span></p>
       
		</div>
		<div>
        <p>Hello '.$locationname[0]['locationname'].',</p>
        <p>A New order has been placed with your Pharmacy. Use the below link to review the order.</p>
        <p>
          <span style="color: #1d9bd8;"> Order Details</span>   <br>
		'.$getuniqueid[0]['ordertype'].' <br>
		'.$orderdate.', '.$userprefferedtime.'
        </p>
        <p style="height: 150px;width:700px;background-color: #f2f2f2;"><a href="'.$link.'"><button class="one">Click to Review</button></a></p>
        <p>If you have any questions, please email <a href="">enquires@medmate.com.au</a> </p>
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = $locationname[0]['email'];
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('orders@medmate.com.au', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			//}   
			   //Email to Customer
			$prescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->get();
											  
					$nqty=0;$nprice=0;	
					if($prescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$prescriptionitems->count();$np++)
						{
							$nqty = $nqty+$prescriptionitems[$np]['drugpack'];
							$priceval = $prescriptionitems[$np]['drugpack']*$prescriptionitems[$np]['originalprice'];
							$nprice = $priceval+$nprice;
						}
					}
			$tbc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->where('orderbasketlist.originalprice' , '=' , '0.00')
                                             ->get();
			$tbccount = $tbc->count();
			$tbctext='';
			if($tbccount > 0)
			{
				$tbctext=  "(".$tbccount ."scripts with pricing to be confirmed by pharmacy)"; 
			}
			else
				$tbctext='';
			$nonprescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 0)
                                              ->get();
					$qty=0; $npriceval=0;
					if($nonprescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$nonprescriptionitems->count();$np++)
						{
							$qty = $qty+$nonprescriptionitems[$np]['drugpack'];
							$npval=$nonprescriptionitems[$np]['drugpack']*$nonprescriptionitems[$np]['originalprice'];
							$npriceval = $npriceval+$npval;
						}
					} 
			
				$subtotal = $npriceval+$nprice;
			$gst=0;
			$gstcalc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->get();
			for($i=0;$i<$gstcalc->count();$i++)
			{
				$gstcalcval = $this->compute_price($locationid,$gstcalc[$i]['prescriptionid'],$user->userid);
				$gst = $gst+$gstcalcval;
			}				
			
			$body_message1 = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
					</head>
			<style>
			 /* CLIENT-SPECIFIC STYLES */
    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

				body  {
					font-family: "Nunito";font-size: 18px;
				}
				.top {
					display: flex;flex-direction: row;width: 700px !important;justify-content: space-between;
				}
				.heading {
				   color: #1d9bd8;font-size: xx-large;font-weight: 600;
				}
				.total {
					width: 700px !important;  margin: auto;
				}
				.bottom {
					text-align: center;color :#7e7e7e;
				}
				.one {
					background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
				}
				.a1 {
					color: #1d9bd8;
				}
				table {
			  border-collapse: collapse; width: 100%;
			}

			table, td, th {
			  border: 1px solid black;padding: auto;
			}
			.s1 {
				padding-left: 60px;
			}
			.s2 {
				padding-left: 30px;
			}
			</style>
			<body>
		<div class="total">
			<div class="top" style="width: 700px !important;">
				<p style="padding-top: 15px;width: 400px !important;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
				<p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><span>New Order Placed</span> <br> <span style="color: black;font-size:large">Order:'.$getuniqueid[0]['uniqueorderid'].'</span></p>
			</div>
			<div>
				<p>Hello '.$user_details[0]['firstname'].',</p>
				<p>Your order has been placed with the selected Pharmacy. The Pharmacy will review your order and update the availability of items and price. You can view the status of your order within the medmate app.</p>
				<p>The Pharmacy will contact you if there are any issues.</p>
				<div style="height:auto;width:700px !important;background-color: #f2f2f2;display: flex;flex-direction: row !important;justify-content: space-around !important;">
				  <p style="width: 250px ;margin-left : 10px ">
					  <span class="a1" style="color: #1d9bd8;">Your Details:</span> <br>
					  <span>'.$user_details[0]['firstname'].' '.$user_details[0]['lastname'].' <br> '.$user_details[0]['mobile'].' <br>   
					  ';
					  if($deliveryaddress->count() > 0)
					  {
					$body_message1 .=' '.$deliveryaddress[0]['addressline1'].' '.$deliveryaddress[0]['addressline2'].' <br>'.$deliveryaddress[0]['suburb'].', '.$deliveryaddress[0]['state'].', '.$deliveryaddress[0]['postcode'].' <br> Australia
					  ';
					  }
					$body_message1 .='</span>
					
				  </p>
				  <p style="width: 250px !important;">
					<span class="a1" style="color: #1d9bd8;">Pharmacy Details:</span> <br>					
					<span>'.$locationname[0]['locationname'].' <br>'.$locationname[0]['phone'].' <br> '.$locationname[0]['address'].' <br> '.$locationname[0]['suburb'].', '.$locationname[0]['suburb'].', '.$locationname[0]['state'].' <br> Australia</span>
				  </p>
				  <p style="width: 200px !important; margin-left : 10px !important;">
					  <span><img src="https://pharmacy.medmate.com.au/pharmacy/'.$locationname[0]['logo'].'" heigt="150px" width="150px"></span>
				  </p>
				</div>
				<div>
					<p class="a1" style="color: #1d9bd8;">Order Details</p>
					<p>Order: '.$getuniqueid[0]['uniqueorderid'].' <br> Placed on '.$placeddate.'</p>
					 <p>
					<table border="1px" cellpadding="0" cellspacing="0" width="100%" style="max-width: 750px;border: 1px solid black;padding: auto;" class="wrapper">
						<tr><th>Item Type</th> <th>Qty</th> <th>Price(Inc.GST)</th></tr>
						<tr><td class="s1">Prescription Items<br>'.$tbctext.'</td> <td class="s1" style="text-align: center;">'.$nqty.'</td> <td class="s1" style="text-align: center;">'.$nprice.'</td></tr>
						<tr><td class="s1">Non-Prescription Items</td> <td class="s1" style="text-align: center;">'.$qty.'</td> <td class="s1" style="text-align: center;">'.$npriceval.'</td></tr>
					</table>
					 </p>
					 <p >
						<table border="1px" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;margin-left:200px" class="wrapper">
							<tr><th>Subtotal</th> <th>'.$subtotal.'</th> </tr>
						';
						if($getuniqueid[0]['ordertype'] == "Delivery")
						{
						$body_message1 .=	'<tr style="text-align: center;"><td class="s2" >Delivery Fee</td> <td class="s2">'.$deliveryfee.'</td> </tr>';
						}
						$body_message1 .='	<tr style="text-align: center;"><td class="s2">Order Total</td> <td class="s2">'.$ordertotal.'</td> </tr>
							<tr style="text-align: center;"><td class="s2">GST</td><td class="s2">'.$gst.'</td></tr>
						</table>
					 </p>
				</div>
			</div>
			<div class="bottom">
				<p>
					Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
									medmate.com.au
				</p>
			</div>
		</div>
		</body>
		</html>
			';
			   $message1['to']           = $user_details[0]['username'];
               $message1['subject']      = "Medmate – Placed Order details #".$getuniqueid[0]['uniqueorderid'];
               $message1['body_message'] = $body_message1;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message1, function ($m1) use ($message1)  {
               $m1->to($message1['to'])
                  ->subject($message1['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message1['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
	}
	
	public function compute_price($location,$drug,$profile){
$price="NA";
$gst=0;
$price_details = DB::table('catalouge_master as C')
						->select('CP.*','C.ShortDescription','C.GST','C.ItemCommodityClass')
						->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
						->where([
							['C.MedmateItemCode',$drug],
							['CP.location_id',$location]
							])
                        ->get();
                      
                        $healthprofile = DB::table('healthprofile')->where('profileid',$profile)->get();
						if(count($price_details)>0){
						if($price_details[0]->ItemCommodityClass=='OTC')
						{
							$p=$price_details[0]->StandardPrice;
							$gst =(($price_details[0]->GST)*$p)/100;
							$price =$p+ $gst;
							
						}
						else
						{
							if(count($healthprofile)>0){
							
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null || $healthprofile[0]->veteranaffairsnumber){
                                    $p=$price_details[0]->Con_Pens_Vet_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->StandardPrice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }

                            }
							
						}
                        }
						else{
                            $price="NA";
                        }
                        return round($gst,2);
                        

    } 
	/**
      @OA\Get(
          path="/v2/myOrders",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
      
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrders()
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get(); 
		
		$orders = Orderdetails :: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')								 							
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')									
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								->where('orderdetails.enduserid','=',$user->userid)
								->distinct()
								->select('locations.*','orderdetails.*','ordertracking.orderstatus as status')
								->where('ordertracking.trackingstatus','=',1)
								->orderBy('orderdetails.created_at', 'DESC')
								->get();
		$nooforders = $orders->count();
		
		for($i=0;$i<$nooforders;$i++)
		{			
			$orderList[$i] = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->distinct()
									->select('orderbasketlist.*')
									->where('ordertracking.orderid' , '=' , $orders[$i]['orderid'])
									->where('basket.orderid' , '=' , $orders[$i]['orderid'])
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orders[$i]['orderid'])
									->get();
			// if(!empty($orderList[$i]))
			// {
			// for($p=0; $p< $orderList[$i]->count();$p++)
			// {
				// $prescriptionDetails[$p] = Prescriptiondetails :: where('prescriptiondetails.prescriptionid','=',$orderList[$i][$p]['prescriptionid'])->get();
				// $prescriptionItemDetails[$p] = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid','=',$orderList[$i][$p]['prescriptionid'])
																			// ->where('prescriptionitemdetails.profileid','=',$orderList[$i][$p]['profileid'])
																			// ->get();
				
				//$orderList[$i][$p]['prescriptionDetails']	= 	$prescriptionDetails[$p];
				//$orderList[$i][$p]['prescriptionItemDetails']	= 	$prescriptionItemDetails[$p];
			// }
			$orders[$i]['orderItems']	= 	$orderList[$i];
			}
			
			
		
		
		
		 return response()->json(['myOrders' => $orders], $this->successStatus); 
     }
	 
	 /**
      @OA\Get(
          path="/v2/myOrderSummary",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ), 
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrderSummary(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		$healthprofile = Healthprofile::where('userid', $user->userid)->get(); 
		
		$orders = Orderdetails :: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')									
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								->where('enduserid','=',$user->userid)
								->where('ordertracking.trackingstatus','=',1)
								->where('orderdetails.orderid','=',$request->orderid)
								->distinct()
								->select('locations.*','orderdetails.*','orderdetails.deliveryfee as deliveryfee','ordertracking.orderstatus as status')
								->orderBy('orderdetails.created_at', 'DESC')
								->get();
		$nooforders = $orders->count();
		
		for($i=0;$i<$nooforders;$i++)
		{			
			$orderList[$i] = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->distinct()
									->select('orderbasketlist.*')
									->where('ordertracking.orderid' , '=' , $orders[$i]['orderid'])
									->where('basket.orderid' , '=' , $orders[$i]['orderid'])
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orders[$i]['orderid'])
									->get();
			
			
			
			$orders[$i]['orderItems']	= 	$orderList[$i];
			
			
				
		}
		
		
		 return response()->json(['myOrders' => $orders], $this->successStatus); 
     }
	 
	 /**
      @OA\post(
          path="/v2/myOrderItemUpdate",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderbasketid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="orderTotal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="drugpack",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
        ),		  
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrderItemUpdate(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.orderbasketid','=',$request->orderbasketid)
										->where('orderbasketlist.userid','=',$user->userid)
										->where('orderbasketlist.basketliststatus', '=',1)
										->update(['orderbasketlist.drugpack' => $request->drugpack]);
										
		if($request->orderTotal!='')
		{			
		$ordertotalUpdate = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
											->update(['orderdetails.orderTotal' => $request->orderTotal]);
		}
		return response()->json(['itemupdate' => $orderBasket], $this->successStatus); 
    }
	
	/**
      @OA\post(
          path="/v2/myOrderItemRemove",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderbasketid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderTotal",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
			
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function myOrderItemRemove(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.orderbasketid','=',$request->orderbasketid)
										->where('orderbasketlist.userid','=',$user->userid)
										->update(['orderbasketlist.basketliststatus' => 0]);
										
		if($request->orderTotal!='')
		{			
		$ordertotalUpdate = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
											->update(['orderdetails.orderTotal' => $request->orderTotal]);
		}
		return response()->json(['itemremoved' => $orderBasket], $this->successStatus); 
     }
	 
	 
	/**
      @OA\post(
          path="/v2/cancelOrder",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="orderremarks",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function cancelOrder(Request $request)
	 {
		$response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		
		
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
										->where('orderdetails.enduserid','=',$user->userid)
										->update(['orderdetails.orderstatus' => 'Cancelled']);
		$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)->where('ordertracking.trackingstatus','=',1)->get();
		
		$noofrows = $ordertrack->count();
		
		for($i=0;$i<$noofrows;$i++)	
		{
			$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)
											  ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
											->update(['ordertracking.trackingstatus' => '0']);
											
		}								
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $request->orderid;
			$orderTracking->orderstatus = "Cancelled";
			$orderTracking->isnew = '1';
			$orderTracking->orderremarks = $request->orderremarks;
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	

		//Medlist Status Change After Cancellation
		
			$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.userid','=',$user->userid)->get();
			
			$noofItems = $orderBasket->count();
			for($b=0;$b<$noofItems;$b++)
			{
										
				if($orderBasket[$b]['prescriptionid'] !="")
				{
				
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
																  ->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
																  ->where('prescriptionitemdetails.isscript', '=', 1)															  
																	->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				
				//To Check if uploaded script is related Medlist Medication script
				$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
															->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
															->get();
				$count = $getmedlistid->count();
				if($count > 0)
				{
					if($count == 1)
					$medlistdetails = $getmedlistid[0]['medilistid'];
					$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
													->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				}
		
				}
				else
				{
					
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $orderBasket[$b]['medlistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$i]['profileid'])
													->where('prescriptionitemdetails.isscript', '=', 0)
													->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				}
			}
		
		return response()->json(['cancelledItem' => $orderTracking], $this->successStatus); 
     }
	 
	/**
      @OA\post(
          path="/v2/completeOrder",
          tags={"My Orders"},
          summary="My Orders",
          operationId="My Orders",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="orderid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			
			
         @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function completeOrder(Request $request)
	 {
		 $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
		
		
		
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
										->where('orderdetails.enduserid','=',$user->userid)
										->update(['orderdetails.orderstatus' => 'Completed']);
		$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)->where('ordertracking.trackingstatus','=',1)->get();
		
		$noofrows = $ordertrack->count();
		
		for($i=0;$i<$noofrows;$i++)	
		{
			$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)
											  ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
											->update(['ordertracking.trackingstatus' => '0']);
											
		}								
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $request->orderid;
			$orderTracking->orderstatus = "Completed";
			$orderTracking->isnew = '1';
			$orderTracking->orderremarks = '';
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	
	 //Medlist Status Change After Completetion
		
			$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.userid','=',$user->userid)->get();
			
			$noofItems = $orderBasket->count();
			for($b=0;$b<$noofItems;$b++)
			{
										
				if($orderBasket[$b]['prescriptionid'] !="")
				{
				
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
																  ->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
																  ->where('prescriptionitemdetails.isscript', '=', 1)															  
																	->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				
				//To Check if uploaded script is related Medlist Medication script
				$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
															->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
															->get();
				$count = $getmedlistid->count();
				if($count > 0)
				{
					if($count == 1)
					$medlistdetails = $getmedlistid[0]['medilistid'];
					$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
													->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				}
		
				}
				else
				{
					
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $orderBasket[$b]['medlistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$i]['profileid'])
													->where('prescriptionitemdetails.isscript', '=', 0)
													->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				}
			}
		
		return response()->json(['completedOrder' => $orderTracking], $this->successStatus); 
     }
	 
}
