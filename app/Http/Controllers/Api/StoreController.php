<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Prescriptionitemdetails; 
use App\models\Healthprofile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class StoreController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/storeitems",
    *      tags={"Store Items"},
    *      summary="Store Items",
    *      operationId="storeitems",
    *	  security={{"bearerAuth": {}} },
    *     @OA\Parameter(
    *          name="category",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
	*	  @OA\Parameter(
    *          name="limit",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	  		      
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function storeitems(Request $request)
	{
        $response = (object)array();
		$user = Auth::user();
		$user_details = User::findOrFail($user->userid);
        $healthprofile = Healthprofile::where('userid', $user->userid)->get();
        $category='';
        //$limit='10';
		//and C.ViewOrder is Not Null
		$limit=" and C.ViewOrder is Not Null order by C.ViewOrder asc limit 10";
        if(isset($request->category)){
            $category=" and SC.categoryid='".$request->category."'"; 
			$limit="";

        }
             /*$catalouge_data = DB::table('catalouge_master as C')
                        ->select('C.itemNameDisplay as drug','C.MedmateItemCode as drugcode','C.itemCommodityClass as script','C.PhotoID','C.MIMsCMI as cmi')
                        ->where('C.itemNameDisplay','LIKE','%'.$request->medication.'%')
                        ->get();*/
       // $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,PI.image,F.form,C.active,C.active_units,C.units_per_pack as pack from catalouge_master as C left join packitemimages as PI on C.MIMsProductCode=PI.prodcode  left join formdat as F on C.MimsFormCode=F.formcode where C.MIMsProdCode=F.prodcode and C.Mimsformcode=F.formcode and   C.itemNameDisplay LIKE '%".$request->medication."%'");                
       $drug_details=array();
	   //$drug_details = new \stdClass();
       $category_data=array();
      // $categories=DB::select("select DISTINCT SC.categoryid,SC.CategoryLv1,SC.vieworder from catalouge_category_master as SC left join catalouge_master as C on SC.categoryid=C.CategoryLv1 where C.ItemCommodityClass='OTC'".$category." order by SC.vieworder");
      $categories=DB::select("select DISTINCT SC.categoryid,SC.CategoryLv1,SC.vieworder from catalouge_category_master as SC where SC.CategoryLv1!='Prescriptions'".$category." order by SC.vieworder");
	  for($i=0;$i<count($categories);$i++){
		  if($categories[$i]->categoryid=='10'){
			  $catalouge_data[$categories[$i]->CategoryLv1] = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.ShortDescription,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.ShortDescription,C.standard_price as price,C.GST,C.ViewOrder,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.itemCommodityClass ='OTC' and C.ItemStatus=1 and C.CategoryLv1 like '%".$categories[$i]->categoryid."'".$limit); 
		  }else{
          $catalouge_data[$categories[$i]->CategoryLv1] = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.ShortDescription,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.ShortDescription,C.standard_price as price,C.GST,C.ViewOrder,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.itemCommodityClass ='OTC' and C.ItemStatus=1 and C.CategoryLv1 = '".$categories[$i]->categoryid."'".$limit); }
                 for($j=0;$j<count($catalouge_data[$categories[$i]->CategoryLv1]);$j++){
            $catalouge_data[$categories[$i]->CategoryLv1][$j]->reference="Catalouge";
            $p=(float)$catalouge_data[$categories[$i]->CategoryLv1][$j]->price;
							$gst =(float)(($catalouge_data[$categories[$i]->CategoryLv1][$j]->GST)*$p)/100;
							$catalouge_data[$categories[$i]->CategoryLv1][$j]->price =number_format(($p+ $gst), 2, '.', '');
			if(isset($catalouge_data[$categories[$i]->CategoryLv1][$j]->PhotoID)){
				$catalouge_data[$categories[$i]->CategoryLv1][$j]->PhotoID="https://pharmacy.medmate.com.au/catalouge/".$catalouge_data[$categories[$i]->CategoryLv1][$j]->PhotoID.".jpg";
			}else if(isset($catalouge_data[$categories[$i]->CategoryLv1][$j]->image)){
				$catalouge_data[$categories[$i]->CategoryLv1][$j]->image="https://pharmacy.medmate.com.au/mimsimages/".$catalouge_data[$categories[$i]->CategoryLv1][$j]->image.".jpg";
			}
           // $catalouge_data[$i]->strength=$catalouge_data[$i]->active.$catalouge_data[$i]->active_units;
            if($catalouge_data[$categories[$i]->CategoryLv1][$j]->script=='Prescription'){$catalouge_data[$categories[$i]->CategoryLv1][$j]->script="Yes";}else{$catalouge_data[$categories[$i]->CategoryLv1][$j]->script="No";}
            }
			$drug_details[$i] = new \stdClass(); 
			$drug_details[$i]->categoryid=$categories[$i]->categoryid;
			$drug_details[$i]->name=$categories[$i]->CategoryLv1;
			$drug_details[$i]->values=$catalouge_data[$categories[$i]->CategoryLv1];
           // $drug_details[$categories[$i]->CategoryLv1]=$catalouge_data[$categories[$i]->CategoryLv1];
    }
    /*    for($i=0;$i<count($mims_data);$i++){
           // $mims_data[$i]->strength=$mims_data[$i]->active.$mims_data[$i]->active_units;
            $mims_data[$i]->reference="MIMS";
            $drug_details[]=$mims_data[$i];
        }*/
        if(!empty($drug_details)){
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.succ_message');
        }else{
            $response->drug_details 	= $drug_details;
            $response->msg 		=trans('messages.no_data');  
        }
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        
   }
   /**
    *  @OA\Post(
    *     path="/v2/storesearch",
    *      tags={"Store Items"},
    *      summary="Store Items",
    *      operationId="storesearch",
    *	  security={{"bearerAuth": {}} },
    *     @OA\Parameter(
    *          name="medication",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*
	*	  		      
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
   public function storesearch(Request $request){   
    $response = (object)array();
    $user = Auth::user();
    $user_details = User::findOrFail($user->userid);
    $healthprofile = Healthprofile::where('userid', $user->userid)->get();
   
   $catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,C.standard_price as price,C.GST,C.ShortDescription,(select image from packitemimages where prodcode=C.MIMsProductCode and  formcode=C.MimsFormCode and packcode=C.MimsPackCode  ) as image from catalouge_master as C  where C.ItemCommodityClass='OTC' and C.ItemStatus=1 and C.itemNameDisplay LIKE '%".$request->medication."%'"); 
   //$mims_data=DB::select("select `P`.`prodcode` as `drugcode`, `P`.`ActualProduct` as `drug`, `F`.`rx` as `script`, `F`.`rx_text`, `F`.`CMIcode` as `cmi`,PI.image,F.form,PA.active,PA.active_units,PA.units_per_pack as pack from `packname` as `P` left join `formdat` as `F` on `P`.`formcode` = `F`.`formcode`  left join packitemimages as PI on P.prodcode=PI.prodcode left join packdat as PA on P.packcode=PA.packcode where  P.prodcode=F.prodcode and P.formcode=PI.formcode and P.packcode=PI.packcode and F.prodcode=PI.prodcode and F.formcode=PI.formcode  and `P`.`ActualProduct` LIKE '%".$request->medication."%'");
  
   $drug_details=array();
    
    for($i=0;$i<count($catalouge_data);$i++){
        $catalouge_data[$i]->reference="Catalouge";
        $p=(float)$catalouge_data[$i]->price;
							$gst =(float)(($catalouge_data[$i]->GST)*$p)/100;
							$catalouge_data[$i]->price =number_format(($p+ $gst), 2, '.', '');
			
		if(isset($catalouge_data[$i]->PhotoID)){
				$catalouge_data[$i]->PhotoID="https://pharmacy.medmate.com.au/catalouge/".$catalouge_data[$i]->PhotoID.".jpg";
			}else if(isset($catalouge_data[$i]->image)){
				$catalouge_data[$i]->image="https://pharmacy.medmate.com.au/mimsimages/".$catalouge_data[$i]->image.".jpg";
			}
       // $catalouge_data[$i]->strength=$catalouge_data[$i]->active.$catalouge_data[$i]->active_units;
        if($catalouge_data[$i]->script=='Prescription'){$catalouge_data[$i]->script="Yes";}else{$catalouge_data[$i]->script="No";}
        $drug_details[]=$catalouge_data[$i];
    }
    if(!empty($drug_details)){
    $response->drug_details 	= $drug_details;
    $response->msg 		= trans('messages.succ_message');
    }else{
        $response->drug_details 	= $drug_details;
        $response->msg 		= trans('messages.no_data');  
    }
    $response->status 		= $this->successStatus;
    return json_encode($response);	
    }

   
}