<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Orderdetails;
use App\models\Ordertracking;
use App\models\Orderbasketlist;
use App\models\Deliveryaddress;
use App\models\Adminprofiletype; 
use App\models\Healthprofile; 
use App\models\Basket; 
use App\models\Basketitemlist; 
use App\models\Invoice; 
use App\models\Orderinvoicelist; 
use App\models\Locations;  
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class PaymentController extends Controller
{
    public $successStatus = 200;
    /**
      @OA\Post(
          path="/v2/payment",
          tags={"Payment"},
          summary="Payment",
          operationId="payment",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="order_id",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="amount",
              in="query",
              required=true,
              @OA\Schema(
                  type="float"
              )
              ),
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
    public function createinvoice(Request $request){
        $response = (object)array();
        $invoiceReference=$request->order_id;
        $order_id=$request->order_id;
        $vars=array (
            'invoiceReference' => $invoiceReference,
            'providerNumber' => 'MEDMATE1',
            'nonClaimableItems' => 
            array (
              0 => 
              array (
                'reference' => '01',
                'description' => $order_id,
                'chargeAmount' => $request->amount,
              ),
            ),
            'callbackUrls' => 
            array (
              0 => 
              array (
                'url' => 'https://pharmacy.medmate.com.au/success',
                'event' => 'success',
              ),
              1 => 
              array (
                'url' => 'https://pharmacy.medmate.com.au/failure',
                'event' => 'failure',
              ),
              2 => 
              array (
                'event' => 'cancel',
                'url' => 'https://pharmacy.medmate.com.au/cancel',
              ),
            ),
            'webhooks' => 
            array (
              0 => 
              array (
                'method' => 'get',
                'url' => 'https://pharmacy.medmate.com.au/orders/6EB309C5-07A4-40DD-BD4A-FD78302A9570',
                'event' => 'invoiceBalancePaid',
              ),
            ),
        );
		//print_r(json_encode($vars));exit;
        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api-au.medipass.io/v3/transactions/invoices",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
 // CURLOPT_POSTFIELDS =>"{\n  \"invoiceReference\" : \"abcd54324\",\n  \"providerNumber\" : \"2429581T\",\n  \"nonClaimableItems\": [\n    {\n        \"reference\": \"01\",\n        \"description\": \"Order 54989\",\n        \"chargeAmount\": \"41.23\"\n        \n    }\n  ],\n  \"callbackUrls\": [\n    {\n      \"url\": \"https://pharmacy.medmate.com.au/success\",\n      \"event\": \"success\"\n    },\n    {\n      \"url\": \"https://pharmacy.medmate.com.au/failure\",\n      \"event\": \"failure\"\n    },\n    {\n      \"event\": \"cancel\",\n      \"url\": \"https://pharmacy.medmate.com.au/cancel\"\n    }\n  ],\n  \"webhooks\": [\n    {\n      \"method\": \"get\",\n      \"url\": \"https://pharmacy.medmate.com.au/orders/6EB309C5-07A4-40DD-BD4A-FD78302A9570\",\n      \"event\": \"invoiceBalancePaid\"\n    }\n  ]\n}",
 CURLOPT_POSTFIELDS=>json_encode($vars), 
 CURLOPT_HTTPHEADER => array(
    "Authorization: Bearer 5ee6d6701aaee7004b74c711:WUI-V9OznrNOe63cf-4bicDLFc56PFJAvCfYlvHbmLc",
    "Content-Type: application/json",
    "x-appid: medmate-web"
  ),
));
$server_output = curl_exec($curl);

//print_r($server_output);exit;
curl_close($curl);
if(isset($server_output)){
$server_output=json_decode($server_output);
DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['transactionId' => $server_output->transactionId,'invoice_id' => $server_output->_id]);
$response->invoice_details 	= $server_output->paymentRequestUrl;
$response->msg 		= trans('messages.succ_message');
$response->status 		= $this->successStatus;
}
return json_encode($response);	

    }
/**
      @OA\Post(
          path="/v2/paymentstatus",
          tags={"Payment"},
          summary="PaymentStatus",
          operationId="paymentstatus",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="order_id",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="payment_status",
              in="query",
              required=true,
              @OA\Schema(
                  type="integer"
              )
              ),
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */    
public function paymentstatus(Request $request){
  $response = (object)array();
  DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['paymentstatus' => $request->payment_status]);
if($request->payment_status==1){
$response->msg 		= "Success";
DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['isnew' => 0]);
$orderid = Orderdetails :: where('uniqueorderid',  $request->order_id)->get();
$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$orderid[0]->orderid)->where('ordertracking.trackingstatus','=',1)->get();
		
$noofrows = $ordertrack->count();

for($i=0;$i<$noofrows;$i++)	
{
	$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$orderid[0]->orderid)
										 ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
									     ->update(['ordertracking.trackingstatus' => '0']);
	
									
}

if($orderstsChange)
{
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $orderid[0]->orderid;
			$orderTracking->orderstatus = "Process";
			$orderTracking->orderremarks = '';
			$orderTracking->isnew = '1';
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	
			$this->paymentemail($orderid[0]->orderid,$request->order_id);
}
//$orderid = $orderid[0]->orderid;

			
}
else if($request->payment_status==0)
{$response->msg = trans('messages.failure_message');}
else{$response->msg="Cancel";}


$response->status 		= $this->successStatus;
return json_encode($response);
}
  
 public function paymentemail($orderid,$uniqueorderid)
 {
	 
	 $orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')								
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')								 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','locations.*','orderdetails.deliveryfee as orderdeliveryfee','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.locationid','locations.email','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "Process")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
		
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->distinct()
									->select('orderbasketlist.*')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Process")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$deliverydate = $orderdate = date('d F Y', strtotime($orderData[0]['userpreffereddate']));
		$placeddate = date('d F Y', strtotime($orderData[0]['orderDate']));
		$deliveryfee=0;
			$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			 /* CLIENT-SPECIFIC STYLES */
    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

				body  {
					font-family: "Nunito";font-size: 18px;
				}
				.top {
					display: flex;flex-direction: row;width: 700px !important;justify-content: space-between !important;
				}
				.heading {
				   color: #1d9bd8;font-size: xx-large;font-weight: 600;
				}
				.total {
					width: 700px !important;  margin: auto;
				}
				.bottom {
					text-align: center;color :#7e7e7e;
				}
				.one {
					background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px !important;margin-top: 50px;
				}
				.a1 {
					color: #1d9bd8;
				}
				table {
			  border-collapse: collapse; width: 100%;
			}

			table, td, th {
			  border: 1px solid black;padding: auto;
			}
			.s1 {
				padding-left: 60px;
			}
			.s2 {
				padding-left: 30px;
			}
			</style>
			<body>
			<div class="total">
			<div class="top" style="width: 700px !important;">
				<p style="padding-top: 15px;width: 400px !important;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px"></p>
				<p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><span>Order Confirmed</span> <br> <span style="color: black;font-size:large">Order:'.$uniqueorderid.'</span></p>
			</div>
			<div>
			<p>Hello '.$orderData[0]['fname'].',</p>
			<p>Your order has been confirmed and is with the Pharmacy for processing. We’ll let you know once the item(s) dispatched. You can view the status of the order within the medmate app. </p>
			<p>The Pharmacy will contact you if there are any issues.</p>
			<div style="height:auto;width:700px !important;background-color: #f2f2f2;display: flex;flex-direction: row !important;justify-content: space-around !important;">';
			  if($orderData[0]['ordertype'] == "Delivery")
			{	
			$body_message.='  <p style="width: 250px !important;margin-left : 10px !important; ">
				  <span class="a1" style="color: #1d9bd8;">Your Details:</span> <br>
				  <span>'.$orderData[0]['fname'].' '.$orderData[0]['lname'].' <br> '.$orderData[0]['mobilenumber'].' <br>  
				  
				  '.$orderData[0]['addr1'].', '.$orderData[0]['addr2'].' <br> '.$orderData[0]['addsuburb'].', '.$orderData[0]['addstate'].', '.$orderData[0]['addpostcode'].' <br> Australia </span>
			  </p>';
			}
			else
			{
			$body_message.='	<p style="width: 250px !important;margin-left : 10px !important; ">
              <span> <span class="a1"> Pickup Time:</span> <br>
                '.$orderdate.' , '.$orderData[0]['userprefferedtime'].'.
                <br> <br>
               <span class="a1"> Your Details to: </span><br>
                '.$orderData[0]['fname'].' '.$orderData[0]['lname'].'</span>
          </p>';
			}
			$body_message.='
			  <p style="width: 250px !important;">
				<span class="a1" style="color: #1d9bd8;">Pharmacy Details:</span> <br>
				
				<span>'.$orderData[0]['locationname'].' <br>'.$orderData[0]['phone'].' <br>  '.$orderData[0]['address'].'<br> '.$orderData[0]['suburb'].', '.$orderData[0]['state'].', '.$orderData[0]['postcode'].' <br> Australia</span>
			  </p>
			  <p style="width: 200px !important; margin-left : 10px !important;">
				  <span><img src="https://pharmacy.medmate.com.au/pharmacy/'.$orderData[0]['logo'].'" heigt="150px" width="150px"></span>
			  </p>
			</div>
			<div>
            <p class="a1" style="color: #1d9bd8;">Order Details</p>
            <p>Order: '.$uniqueorderid.' <br> Placed on '.$placeddate.'</p>
			';
			$prescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->get();
        $nqty=0;$nprice=0;
					if($prescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$prescriptionitems->count();$np++)
						{
							$nqty = $nqty+$prescriptionitems[$np]['drugpack'];
							$priceval = $prescriptionitems[$np]['drugpack']*$prescriptionitems[$np]['originalprice'];
							$nprice = $priceval+$nprice;
						}
					} 
			$nonprescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 0)
                                              ->get();
        $qty=0;$npriceval =0;
					if($nonprescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$nonprescriptionitems->count();$np++)
						{
							$qty = $qty+$nonprescriptionitems[$np]['drugpack'];
							$npval=$nonprescriptionitems[$np]['drugpack']*$nonprescriptionitems[$np]['originalprice'];
							$npriceval = $npriceval+$npval;
						}
					} 
			$subtotal = $npriceval+$nprice;
			$gst=0;
			$gstcalc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->get();
			for($i=0;$i<$gstcalc->count();$i++)
			{
				$gstcalcval = $this->compute_price($orderData[0]['locationid'],$gstcalc[$i]['prescriptionid'],$orderData[0]['myuserid']);
				$gst = $gst+$gstcalcval;
			}
			$body_message.='
             <p>
            <table border="1px" cellpadding="0" cellspacing="0" width="100%" style="max-width: 750px;border: 1px solid black;padding: auto;" class="wrapper">
                <tr><th>Item Type</th> <th>Qty</th> <th>Price(Inc.GST)</th></tr>
                <tr><td class="s1">Prescription Items</td> <td class="s1" style="text-align: center;">'.$nqty.'</td> <td class="s1" style="text-align: center;">'.$nprice.'</td></tr>
                <tr><td class="s1">Non-Prescription Items</td> <td class="s1" style="text-align: center;">'.$qty.'</td> <td class="s1" style="text-align: center;">'.$npriceval.'</td></tr>
            </table>
             </p>
             <p >
                <table border="1px" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;margin-left:200px" class="wrapper">
                    <tr><th>Subtotal</th> <th>'.$subtotal.'</th> </tr>';
					 if($orderData[0]['ordertype'] == "Delivery")
			{	
			$body_message.=' 
                    <tr style="text-align: center;"><td class="s2">Delivery Fee</td> <td class="s2">'.$orderData[0]['orderdeliveryfee'].'</td> </tr>';
			}
              $body_message.='       <tr style="text-align: center;"><td class="s2">Order Total</td> <td class="s2">'.$orderData[0]['orderTotal'].'</td> </tr>
                    <tr style="text-align: center;"><td class="s2">GST</td><td class="s2">'.$gst.'</td></tr>
                </table>
             </p>
        </div><br/>
		 <p>        <b>
            Due to COVID-19, your delivery may be delayed. 
        </b></p>
        <p>        <p>
            Please review the order details in the medmate app. <br>
            We hope to see you again soon. 
        </p></p>
			</div>
			<div class="bottom">
				<p>
					Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
					               medmate.com.au
				</p>
			</div>
		</div>
		</body>
		</html>
			';
	
			   $message['to']           = $orderData[0]['emailaddress'];
               $message['subject']      = "Medmate – Order #".$uniqueorderid." has been confirmed";
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			//Email to pharmacy
			
			$link = "https://pharmacy.medmate.com.au/pharmacy/viewprocessorder/".$orderid;
			$body_message1='
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;width: 120px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
			<p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px"></p>
			<p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><span>Ready to Process</span> <br> <span style="color: black;font-size:large">Order:'.$uniqueorderid.'</span></p>
		</div>
		<div>
        <p>Hello '.$orderData[0]['locationname'].',</p>
        <p>Order has been confirmed and finalised by the customer. Use the below link to review and process the order.</p>
        <p>
          <span style="color: #1d9bd8;"> Order Details</span>   <br>
			'.$orderData[0]['ordertype'].' <br>
			'.$deliverydate.', '.$orderData[0]['userprefferedtime'].'
        </p>
        <p style="height: 150px;width:700px;background-color: #f2f2f2;">
		<a href="'.$link.'"><button class="one" style="background-color: #1d9bd8 !important;color: white !important;border: 0px solid !important;border-radius: 10px !important;width: 120px !important;height: 35px !important;margin-left: 300px !important;margin-top: 50px !important;">Click to Review</button></a></p>
        <p>If you have any questions, please email <a href="">enquires@medmate.com.au</a> </p>
		</div>
		<div class="bottom">
			<p>
				Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
				              medmate.com.au
			</p>
		</div>
		</div>
		</body>
		</html>';
			   $message1['to']           = $orderData[0]['email'];
               $message1['subject']      = "Medmate – Order #".$uniqueorderid." has been confirmed";
               $message1['body_message'] = $body_message1;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message1, function ($m1) use ($message1)  {
               $m1->to($message1['to'])
                  ->subject($message1['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message1['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
 } 
 
 
public function compute_price($location,$drug,$profile){
$price="NA";
$gst=0;
$price_details = DB::table('catalouge_master as C')
						->select('CP.*','C.ShortDescription','C.GST','C.ItemCommodityClass')
						->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
						->where([
							['C.MedmateItemCode',$drug],
							['CP.location_id',$location]
							])
                        ->get();
                      
                        $healthprofile = DB::table('healthprofile')->where('userid',$profile)->get();
						if(count($price_details)>0){
						if($price_details[0]->ItemCommodityClass=='OTC')
						{
							$p=$price_details[0]->StandardPrice;
							$gst =(($price_details[0]->GST)*$p)/100;
							$price =$p+ $gst;
							
						}
						else
						{
							if(count($healthprofile)>0){
							
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null || $healthprofile[0]->veteranaffairsnumber){
                                    $p=$price_details[0]->Con_Pens_Vet_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->StandardPrice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }

                            }
							
						}
                        }
						else{
                            $price="NA";
                        }
                        return round($gst,2);
                        

    }  
}