<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Prescriptionitemdetails; 
use App\models\Healthprofile; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
class SpecialProductsController extends Controller
{
    public $successStatus = 200;
	/**
    *  @OA\Post(
    *     path="/v2/specialproducts",
    *      tags={"Special Products"},
    *      summary="Special Products",
    *      operationId="Special Products",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="locationid",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),	  		      
    *     
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function SpecialProducts(Request $request)
	{
		$response = (object)array();
		$user = Auth::user();
        $user_details = User::findOrFail($user->userid);
        $special_items = DB::table('catalouge_master as C')
						->select('C.itemNameDisplay','C.MedmateItemCode','C.itemCommodityClass','C.PhotoID','CP.SalePrice as price','CP.SalePrice as StandardPrice','C.GST')
						->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
						->where([
							['CP.SalePrice','!=',0],
                            ['CP.location_id',$request->locationid],
                            ['CP.Availability',1],
                            ['C.itemCommodityClass','OTC'],
							['C.ItemStatus',1]
							])
                        ->get();
                        for($i=0;$i<count($special_items);$i++){
                            $p=(float)$special_items[$i]->price;
							$gst =(float)(($special_items[$i]->GST)*$p)/100;
							$special_items[$i]->price =number_format(($p+ $gst), 2, '.', '');
                        }
                      
        $response->specialproducts 	= $special_items;
        $response->msg 		= trans('messages.succ_message');
		$response->status 		= $this->successStatus;
		return json_encode($response);	
        }
    		
}
