<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User; 
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function verifyNewAccount(Request $request)
	{
		$token = '';
		
		if( $request->token ) {
			$token = $request->token;
        }
        $id=base64_decode($token);
//echo $token;die;
		
		if($token == '') {
			return redirect()->route('login',$this->lang)->with('error' , "Token empty.");
		}else {
			$checkToken = User::findOrFail($id);
			if($checkToken) {
				if($checkToken->emailverified == 1){
					return redirect()->route('emailverified')->with('success' , "Your account is already verified. Please continue login.");
				}
				User::WHERE('userid' , $id)->update(['emailverified' => 1]);
				return redirect()->route('emailverified')->with('success' , "User verified successfully.");
			}
		}
	}
		
		
}
