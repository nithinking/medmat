<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use App\User; 



use Config;
use Validator;
use DB;
use Mail;
use App;


use App\Http\Controllers\Controller;

class CommonController extends Controller
{
    //
	/*public function __construct( Request $request )
    {
      //
		parent::__construct( $request );
		
	}*/ 

	/*===============Push Notification=======================*/

	public function sendPushNotification($fields) {
	    // Set POST variables
	    $url = 'https://fcm.googleapis.com/fcm/send';
	    /*$firebase_api_key='AAAAoKLps8s:APA91bG3e-YuIIEjv9it8zCSI7gNwUfaPSVlRDTASyV2GM58IeteXmjBrmY_7ORX5ieamw48n_jMJZKBVS6IuDQ8NMYPHKzz_UmGZ8ZhVMRuzgFY1RsafyadWBuCm37uJ43B8Ul8jWV9';*/
	    $firebase_api_key='AAAANPfdBQo:APA91bHgC8O8rw2jQNUECU6zSNqhGMm8iJtllHF_KP9XFMoDaIv7thJHIdMgl1cGvX3zcwIcstwMYrRi1Qvjm7El2tLaOlJguz9p4fWAzgg5uVATbkD-gnpm1qpBfNs6cyygjBGhA5JN';
	    // $firebase_api_key='AIzaSyAaHkJ8dq77PSoQp6C_rACDtqOVl8vO7vs';

	    $headers = array(
	      'Authorization: key='.$firebase_api_key,
	      'Content-Type: application/json'
	      );
	          // Open connection
	    $ch = curl_init();
	          // Set the url, number of POST vars, POST data
	    curl_setopt($ch, CURLOPT_URL, $url);

	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	          // Disabling SSL Certificate support temporarly
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	          // Execute post
	    $result = curl_exec($ch);
	    
	    
	    if ($result === FALSE) {
	              die('Curl failed: ' . curl_error($ch));
	    }
	    //  echo "<pre>";
	    // print_r($result); die('f');
	          // Close connection
	    curl_close($ch);
	    //return true;
	    return $result;
    }

	public function iPhonePushNotification($msg=Null,$deviceToken,$message_type='',$link_value='',$notification_type=0,$soundType=0){
	    $passphrase='123456';
	    $ctx = stream_context_create();
	    stream_context_set_option($ctx, 'ssl', 'local_cert', ('medmate-push.pem'));
	    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
	    // echo $ctx; die;
	    // Open a connection to the APNS server
	    $fp = stream_socket_client(
	      'ssl://gateway.push.apple.com:2195', $err,
	      $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	    if (!$fp)
	      exit("Failed to connect: $err $errstr" . PHP_EOL);
	    // $type="notification";
	    // echo 'Connected to APNS' . PHP_EOL;
	    // Create the payload body
		 if($soundType == 1){
			$sound  = 'water.caf';
		}else{
			$sound  = 'default';
		}
	    $body['aps'] = array(
	      'alert'             => $msg,
	      'message_type' 	  => $message_type,
          'link_value'        => $link_value,
          'notification_type' => $notification_type,
	      'sound'             => $sound
	      );

	    // Encode the payload as JSON
	    $payload = json_encode($body);
		try{
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		}catch(Exception $e){}
    	// Send it to the server
    	$result = fwrite($fp, $msg, strlen($msg));
		
	    // Build the binary notification
	    

	    // if (!$result)
	    //   echo 'Message not delivered' . PHP_EOL;
	    // else
	    //   echo 'Message successfully delivered' . PHP_EOL;
	    // Close the connection to the server
	    fclose($fp);
		return $result;
	}

	/*
	  * Send push notification for ios and android phones
	  * @param string  $language En/Ar
	  * @param integer $toId Customer id who get notification
	  * @param string  $msgEn , $msgAr message  
	  * @param integer $formCustomer Customer id who send notification
	  * @param integer $related_id which region notification send 
	  * return true
	*/
	//$request->user_id,'C',$msgEn,$msgAr,$id,1
    public function notification($user_id,$message_type,$msgEn,$link_value,$notification_type=0,$soundType=0){
		
		//echo $notification_type;die;
	        
	 $list=DB::select("select * from users where userid=".$user_id);
	    	$msg = $msgEn;
	        if(strtolower($list[0]->devicetype)==1){

	          $registration_ids[0]=  $list[0]->devicetoken;
	          $data['message']= (object) array(
	          	'message'           => $msg,
	          	'message_type'      => $message_type,
	          	'link_value'        => $link_value,
	          	'notification_type' => $notification_type,
				'soundType' => $soundType
	          );
	          // $data=  array('body'=> $msg);
	          $fields = array('registration_ids' => $registration_ids,'data' => $data);
	          try{
	            $this->sendPushNotification($fields);
	          }catch(Exception $e){
	                       // echo 'Error:'.$e->getMessage();
	                      // return;
	          }
	        }else if(strtolower($list[0]->devicetype)==2){
	          try{
				if(isset($list[0]->devicetoken) && $list[0]->devicetoken != null && $list[0]->devicetoken != '') {
					$this->iPhonePushNotification($msg,$list[0]->devicetoken,$message_type,$link_value,$notification_type,$soundType);
				}
	          }catch(Exception $e){
	                      //  echo 'Error:'.$e->getMessage();
	                      // return;
	          }
        }
	    return true;
    }
    public function testPush(Request $request){
		/*$list['user_id']      = 36;
		$list['message_type'] = "C";
		$request              =$list;
		$phone = '94027734,96429683,92634504';
		$message="test api from production webservice controller"; 
		$response = $this->ServiceController->send_sms($phone, $message) ;*/

		/*$response = $this->common->iPhonePushNotification($message,'5004001EF3DA394218A636D5361A25B84281F140E4E109222EEF26EB86C42FF1');
		// print_r($response); die;
		// // $this->common->pubnubNotification($request);
		
		// $data = $this->common->smtpmail(); die($data);
		 
		/*$url = url('/'.$request->language.'/verifynewaccount/12121');
		
		$response= $this->common->verifyNewAccountSms('92634504','Please click the given link',$url);
		dd($response);
     echo $this->date; die;*/
     if($request->devicetype==1){
        //$registration_ids[0]=  'fyWvqFdv3DI:APA91bEJ7RSmmoZTXuLREnGeeAKHMoJ_3PCo5WkfobO1k6xWcpZP15fp4qkxh274WkAfFMjzFkHMvEDP3K4KnrgBNNFfQ_zyghsJ2Bqrckp6KgzAUqnkqYBjkHxGCRZD6g6tUQVe9K5R';
        $registration_ids[0]=  $request->device_token;
	           $data=array('body' => 'Hello world');
	            $data['message']= (object) array(
                    'message'           => 'hjjgj',
                     'message_type'      => "C",
                     'link_value'        => "test",
                     'notification_type' => "1",
                      "priority"=> "high"
                   );
	           $fields = array('registration_ids' => $registration_ids,'data' => $data);
	     $msgEn = 'hello ';
		// $msgAr = 'h12';

		 $this->common->sendPushNotification($fields);}elseif($request->devicetype==2){
		// print_r($fields); die('end');
		//$this->common->notification('en',38,2,$msgEn,$msgAr,100,1);
		$this->common->iPhonePushNotification('hello world',$request->devicetoken,'C','test','1');
        
         }die('done');
	}

	
	function shortUrl($url, $shorten = true) {
		$ch = curl_init();  
		curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$data = curl_exec($ch);  
		curl_close($ch);
		return $data;
	}


	function verifyNewAccountSms($phone,$msg,$url){
		$response=[];
		$urlData['id']="";
		$urlData = $this->shortUrl($url);
		if(isset($urlData)){
			$message = $msg.":- ".$urlData;
			$response = $this->ServiceController->send_sms($phone, $message) ;
		}
		return ($response);
	}
    /*****Google short url end********/
   /*
	public function pubNubNotification($user_id,$message_type){
		if(!empty($user_id) && !empty($message_type))
		{
			$curl = curl_init();
	
			$data = [
						'channel' => Config::get("constants.pubnub.notificationChannel"),
						'subscribeKey' => Config::get("constants.pubnub.subscribeKey"),
						'publishKey' => Config::get("constants.pubnub.publishKey"),
						'message' => urlencode('{"user_id":"'.$user_id.'","message_type":"'.$message_type.'"}')
					];
				
			$url = 'https://ps.pndsn.com/publish/'.$data['publishKey'].'/'.$data['subscribeKey'].'/0/'.$data['channel'].'/myCallback/'.$data['message'].'?store=1&uuid=958B6A0D-14B5-48A3-A963-5CC423E115CC';

			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30000,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				
				CURLOPT_HTTPHEADER => array(
					"accept: *//*",
					"accept-language: en-US,en;q=0.8",
					"content-type: application/json",
				),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
		}
		
		return response()->json([
				'response'      => [],
				'message'       => 'successfully send',
				'response_code' => 200
		]);
	}*/

	static public function notification_count($user_id,$type='V'){
		return $data = DB::table('infoline_push_notifications')
		->where([
			['user_id',$user_id],
			['message_type',$type],
			['is_read',0],
		])
		->count();
	}

	static public function notification_read($user_id,$type='V'){
		return $data = DB::table('infoline_push_notifications')
		->where([
			['user_id',$user_id],
			['message_type',$type],
			['is_read',0],
		])
		->update(['is_read'=>1]);
	}
	public function gettimezone()
	{
$ip = $_SERVER['REMOTE_ADDR'];
$ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
$ipInfo = json_decode($ipInfo);
$timezone = $ipInfo->timezone;
return $timezone;
//date_default_timezone_set($timezone);
//echo date_default_timezone_get();

	}
}
