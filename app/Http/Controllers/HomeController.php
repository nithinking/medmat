<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use DB;
use Validator;
use App\User;
use Illuminate\Support\Facades\Auth; 

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		//return View::make('hello');
	}
	
	public function aboutUs()
	{
		//return View::make('aboutus');
		return view('aboutus');
	}
	public function privacy()
	{
		//return View::make('aboutus');
		return view('privacy');
	}
	public function termsandconditions()
	{
		//return View::make('aboutus');
		return view('terms');
	}
	public function basic_email() {
		$data = array('name'=>"Virat");
	 
		Mail::send([], $data, function($message) {
		   $message->to('vijaya.talluri@roundsqr.net', 'Smple Mail')->subject
			  ('Laravel Basic Testing Mail');
		   $message->from('medmate-app@medmate.com.au','Medmate');
		});
		echo "Basic Email Sent. Check your inbox.";
	 }
	public function emailVerified()
	{
		//return View::make('aboutus');
		return view('emailverified');
	}
	public function resetpass(Request $req)
	{
		
			$tempPass = $req->token;
		
			return view('reset',compact('tempPass'));
		
	}
	public function rejectorder($orderid){
		$orderData = DB::table('orderdetails')->where('orderid',$orderid)->get();
		$locationname = DB::table('locations')->where('locationid','=',$orderData[0]->locationid)->get();
		$subject=env('ENV')." An order has been rejected".$orderData[0]->uniqueorderid." for  location <".$orderData[0]->locationid.">";
		$body_message = '
			<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
				<title>Document</title>
			</head>
			<style>
			body  {
				font-family: "Nunito";font-size: 18px;
			}
			.top {
				display: flex;flex-direction: row;width: 700px;justify-content: space-between;
			}
			.heading {
			   color: #1d9bd8;font-size: xx-large;font-weight: 600;
			}
			.total {
				width: 700px;  margin: auto;
			}
			.bottom {
				text-align: center;color :#7e7e7e;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px;
			}
		</style>
		<body>
		<div class="total">
		<div class="top">
        <p style="padding-top: 15px;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px" ></p>
        <p class="heading" style="width:300px !important;color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;"><br> <span style="color: black;font-size:large"></span></p>
       
		</div>
		<div>
        <p>Hello Admin,</p>
        <p>Order '.$orderData[0]->uniqueorderid.' for '.$locationname[0]->locationname.' and location <'.$orderData[0]->locationid.'> has been rejected.</p>
             
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
              medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
					';

			   $message['to']           = "admin@medmate.com.au";
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('vijya.talluri@roundsqr.net', 'Medmate')
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
			   header('Location: https://medmate.com.au/reject-order/');     exit();
               }catch(\Exception $e){
                  echo 'Error:'.$e->getMessage();
                 // return;
               }

	}

	public function resetPassword(Request $req)
	{
		
		$v = Validator::make($req->all(), [
			'pass' => 'required|min:6',  
			'repass' => 'required|same:pass|min:6', 
		]);
		if ($v->fails()) {
			return redirect()->back()->withErrors($v->errors())->withError('Please enter password and confirm password');
		}
		else
		{
			
		if(!isset($req->token))
		{
			return redirect()->back()->withErrors('Verification code does not exist');
		}
		else if(!DB::table('users')->where('hashcode',$req->token)->exists())
		{
			return redirect()->back()->withErrors('Verification code does not match');
		}
		$user = User::where('hashcode',$req->token)->first();
		
		$user->password = bcrypt($req->pass);
		$user->save();
		return back()->with('success_message','Password updated successfully.');
		}
	}
}
