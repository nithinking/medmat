<?php
namespace App\Http\Controllers;
require_once '/var/www/html/medmate-backend/vendor/stripe/stripe-php/init.php';
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Stripe;
use DB;
   
class StripePaymentController extends Controller
{
	
	public function __construct(){
		\Stripe\Stripe::setApiKey(env('STRIPE_KEY'));
	}
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe($amount,$order_id)
    {
		$amount=round($amount,2);
		   $data=array('amount'=>$amount,
            'order_id'=>$order_id
            );
        return view('stripe')->with($data);
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
		if(env('ENV') == "TEST")	
		$desc="Test-Payment for order ID ".$request->order_id;
		else if(env('ENV') == "DEV")	
		$desc="Dev-Payment for order ID ".$request->order_id;
		else
		$desc="Payment for order ID ".$request->order_id;
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
       $response= Stripe\Charge::create ([
                "amount" => $request->amount * 100, 
                "currency" => "aud",
                "source" => $request->stripeToken,
                "description" => $desc ,
				"metadata" => ["order_id" => $request->order_id]
        ]);
		if($response->status=='succeeded'){
			DB::table('orderdetails')->where('uniqueorderid',  $request->order_id)->update(['transactionId' => $response->id,'invoice_id' => $response->balance_transaction]);
			return redirect('success');
		}else{
			return redirect('failure');
		}
		
        Session::flash('success', 'Payment successful!');
          
        return back();
    }
}
