<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Business;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class BusinessController extends Controller
{
    //
	 public $successStatus = 200;
	 /**
      @OA\POST(
          path="/v2/addBusiness",
          tags={"Businesses"},
          summary="Businesses Information",
          operationId="Businesses Information",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="businessname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="website",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="parentbusinessid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addBusiness(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$availBusiness = Business ::  where('businessname', $request->businessname)->get();
		 if($availBusiness->count() > 0  )
		 {		 
				
			$response->msg 		= "Business Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$Business = new Business;		
			
			$Business->businessname = $request->businessname;
			$Business->website = $request->website;
			$Business->grouptypeid  = 2;
			$Business->parentbusinessid  = 0;				 
			$Business->save();
			$response->business 	= $Business;
			$response->msg 		= "Business Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 
	 /**
      @OA\Get(
          path="/v2/getAllBusinesses",
          tags={"Businesses"},
          summary="Businesses Information",
          operationId="Businesses Information",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllBusinesses(){
		 $user = Auth::user();
		
         $Businesses = Business::get();
		
        return response()->json(['businesses' => $Businesses], $this->successStatus); 
      
  }
  
   /**
      @OA\POST(
          path="/v2/deleteBusiness",
          tags={"Businesses"},
          summary="Businesses Information",
          operationId="Businesses Information",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="businessid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deleteBusiness(Request $request){
		 $user = Auth::user();
		
        $business = Business :: where('businessid','=',$request->businessid)->delete();
		//$business = Business :: where('businessid','=',$request->businessid)->update(['business.businessstatus' => '0']);;
		 
		$success['message'] = "Business deleted";
        return response()->json(['business' => $business], $this->successStatus); 
      
  }
  /**
      @OA\Post(
          path="/v2/updateBusiness",
          tags={"Businesses"},
          summary="Businesses Information",
          operationId="Businesses Information",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="businessid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="businessname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),  
			  @OA\Parameter(
              name="website",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="parentbusinessid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateBusiness(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $businessinfo = Business ::  where('businessid', $request->businessid)
							->update(['business.businessname' => $request->businessname,'business.website' => $request->website,'business.parentbusinessid' => $request->parentbusinessid]);
		$success['message'] = "Business Details Updated";
        return response()->json(['businessinfo' => $businessinfo], $this->successStatus);
	}
}
