<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Pathologylocations;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class PathologylocationsController extends Controller
{
    public $successStatus = 200;
	 /**
      @OA\POST(
          path="/v2/addPathologylocation",
          tags={"Pathologylocation"},
          summary="Pathologylocation Info",
          operationId="Pathologylocation Info",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="locationname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="locationaddress",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addPathologylocation(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$availlocation = Pathologylocations ::  where('locationname', $request->locationname)->get();
		 if($availvaccine->count() > 0  )
		 {		 
				
			$response->msg 		= "Pathologylocation Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$pathologylocation = new Pathologylocations;		
			
			$pathologylocation->locationname = $request->locationname;
			$pathologylocation->locationaddress = $request->locationaddress;			 
			$pathologylocation->status = $request->status;			 
			$pathologylocation->save();
			$response->pathologylocation 	= $pathologylocation;
			$response->msg 		= "Pathologylocation Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 /**
      @OA\Get(
          path="/v2/getAllPathologylocation",
          tags={"Pathologylocation"},
          summary="GET Pathologylocation",
          operationId="GET Pathologylocation",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllPathologylocation(){
		 $user = Auth::user();
		
         $locations = Pathologylocations::get();
		
        return response()->json(['locations' => $locations], $this->successStatus); 
      
  }
  /**
      @OA\POST(
          path="/v2/deletePathologylocation",
          tags={"Pathologylocation"},
          summary="Delete Pathologylocation Info",
          operationId="Delete Pathologylocation Info",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="plocationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deletePathologylocation(Request $request){
		 $user = Auth::user();
		
        $locations = Pathologylocations :: where('id','=',$request->plocationid)->->update(['pathology_locations.status' => '0']);
		
		 
		$success['message'] = "Pathologylocation deleted";
        return response()->json(['locations' => $locations], $this->successStatus); 
      
  }
  
  /**
      @OA\Post(
          path="/v2/updatePathologylocation",
          tags={"Pathologylocation"},
          summary="Pathologylocation Info",
          operationId="Pathologylocation Info",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="plocationid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="locationname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="locationaddress",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="status",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updatePathologylocation(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $locations = Pathologylocations ::  where('id', $request->plocationid)
							->update(['pathology_locations.locationname' => $request->locationname,'pathology_locations.locationaddress' => $request->locationaddress,'pathology_locations.status' => $request->status]);
		$success['message'] = "Pathologylocations Details Updated";
        return response()->json(['locations' => $locations], $this->successStatus);
	}
}
