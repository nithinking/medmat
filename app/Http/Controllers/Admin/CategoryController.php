<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Categorymaster;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class CategoryController extends Controller
{
    //
	public $successStatus = 200;
	/**
      @OA\POST(
          path="/v2/addCategory",
          tags={"Category"},
          summary="Category Add",
          operationId="Category Add",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="CategoryLv1",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="vieworder",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addCategory(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$availCategory = Gplocations ::  where('CategoryLv1', $request->CategoryLv1)->get();
		 if($availCategory->count() > 0  )
		 {		 
				
			$response->msg 		= "Category Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$category = new Categorymaster;		
			
			$category->CategoryLv1 = $request->gpname;
			$category->categorystatus = 1;
			$category->vieworder  = $request->vieworder;			 
			$category->save();
			$response->category 	= $category;
			$response->msg 		= "Category Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 
	 /**
      @OA\Get(
          path="/v2/getAllCategory",
          tags={"All Category"},
          summary="Get Category",
          operationId="Get Category",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllCategory(){
		 $user = Auth::user();
		
         $category = Categorymaster::get();
		
        return response()->json(['category' => $category], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/deleteCategory",
          tags={"Delete Category"},
          summary="Delete Category",
          operationId="Delete Category",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="categoryid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deleteCategory(Request $request){
		 $user = Auth::user();
		
        $category = Categorymaster :: where('categoryid','=',$request->categoryid)->update(['catalouge_category_master.categorystatus' => '0']);
		
		 
		$success['message'] = "Category deleted";
        return response()->json(['category' => $category], $this->successStatus); 
      
  }
  
  /**
      @OA\Post(
          path="/v2/updateCategory",
          tags={"update Category"},
          summary="update Category",
          operationId="updateCategory",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="categoryid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="categorystatus",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="CategoryLv1",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="vieworder",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			 
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateCategory(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $category = Categorymaster ::  where('categoryid', $request->categoryid)
							->update(['catalouge_category_master.CategoryLv1' => $request->CategoryLv1,'catalouge_category_master.categorystatus' => $request->categorystatus,'catalouge_category_master.vieworder' => $request->vieworder]);
		$success['message'] = "Category Details Updated";
        return response()->json(['category' => $category], $this->successStatus);
	}
}
