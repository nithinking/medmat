<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
use Helper;
use Session;
use Config;
use App;
use DateTime;
use Response;
class PathologyUserController extends Controller
{
    public $successStatus = 200;
    /**
      @OA\Post(
          path="/v2/addPathologyUser",
          tags={"PathologyUser"},
          summary="Add Pathology User",
          operationId="PathologyUser",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
          
		   
		   @OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="password",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  
			
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function addPathologyUser(Request $request) {
		
		 $response 	   = (object)array();
		 $user = Auth::user();
		 //$user_details = User::findOrFail($user->userid);
	
		$availUser = User ::  where('username', $request->username)->where('profiletypeid',4)->where('userstatus',1)->get();
		 if($availUser->count() > 0  )
		 {		 
				
			$response->msg 		= "User Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
			{
				$user = new User;		
				
				$user->firstname = $request->firstname;
				$user->username = $request->username;
				$user->password = bcrypt($request->password);
				$user->profiletypeid = 4;
				$user->userstatus = 1;
				$user->save();
				$response->user 	= $user;
				$response->msg 		= "Pathology User Added Successfully.";
				$response->status 		= $this->successStatus;
			}
			return json_encode($response);
	}
	
	/**
      @OA\Get(
          path="/v2/getPathologyUsers",
          tags={"PathologyUser"},
          summary="Get Pathology Address",
          operationId="Pathology Address",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getPathologyUsers(){
		 $user = Auth::user();
		
         $users = User::where('profiletypeid', 4)->get();
		
        return response()->json(['users' => $users], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/deletePathologyUser",
          tags={"PathologyUser"},
          summary="Remove Pathology User",
          operationId="Pathology User",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deletePathologyUser(Request $request){
		 $user = Auth::user();
		
         $users = User:: where('users.userid', '=', $request->userid)
													->where('profiletypeid', '=', 4 )
													->update(['users.userstatus' => '0']);
		$success['message'] = "User deleted";
        return response()->json(['users' => $users], $this->successStatus); 
      
  }
  /**
      @OA\Post(
          path="/v2/updatePathologyUser",
          tags={"PathologyUser"},
          summary="update Pathology User",
          operationId="update Pathology User",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="username",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),  
			  @OA\Parameter(
              name="userstatus",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updatePathologyUser(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $userinfo = User ::  where('userid', $request->userid)
							->update(['users.userstatus' => $request->userstatus,'users.firstname' => $request->firstname]);
		$success['message'] = "User Details Updated";
        return response()->json(['userinfo' => $userinfo], $this->successStatus);
	}
}
