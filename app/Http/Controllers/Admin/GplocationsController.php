<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Gplocations;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class GplocationsController extends Controller
{
    //
	 public $successStatus = 200;
	 /**
      @OA\POST(
          path="/v2/addGplocations",
          tags={"Gplocations"},
          summary="Gplocations Information",
          operationId="Gplocations Information",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="gpname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gplink",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="gpaddress1",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpaddress2",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="gpaddress3",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpsuburb",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpstate",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gppostcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpcountry",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpphonenumber",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpwebsitelink",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addGplocations(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$availgplocations = Gplocations ::  where('gpname', $request->gpname)->get();
		 if($availgplocations->count() > 0  )
		 {		 
				
			$response->msg 		= "gpname Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$gplocations = new Gplocations;		
			
			$gplocations->gpname = $request->gpname;
			$gplocations->gplink = $request->gplink;
			$gplocations->gpaddress1  = $request->gpaddress1;
			$gplocations->gpaddress2  = $request->gpaddress2;
			$gplocations->gpaddress3  = $request->gpaddress3;
			$gplocations->gpsuburb    = $request->gpsuburb;				 
			$gplocations->gpstate     = $request->gpstate;				 
			$gplocations->gppostcode  = $request->gppostcode;				 
			$gplocations->gpcountry   = $request->gpcountry;				 
			$gplocations->gpphonenumber = $request->gpphonenumber;				 
			$gplocations->gpwebsitelink = $request->gpwebsitelink;				 
			$gplocations->save();
			$response->business 	= $Business;
			$response->msg 		= "Gplocation Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 
	 /**
      @OA\Get(
          path="/v2/getAllGplocations",
          tags={"Gplocations"},
          summary="Gplocations Information",
          operationId="Gplocations Information",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllGplocations(){
		 $user = Auth::user();
		
         $gplocations = Gplocations::get();
		
        return response()->json(['gplocations' => $gplocations], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/deleteGplocations",
          tags={"Gplocations"},
          summary="Gplocations Information",
          operationId="Gplocations Information",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="gplocationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deleteGplocations(Request $request){
		 $user = Auth::user();
		
        $gplocations = Gplocations :: where('gplocationid','=',$request->gplocationid)->delete();
		
		 
		$success['message'] = "Business deleted";
        return response()->json(['gplocations' => $gplocations], $this->successStatus); 
      
  }
  
  /**
      @OA\Post(
          path="/v2/updateGplocations",
          tags={"Gplocations"},
          summary="Gplocations Information",
          operationId="Gplocations Information",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="gplocationid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="gplink",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="gpaddress1",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpaddress2",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="gpaddress3",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpsuburb",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpstate",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gppostcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpcountry",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpphonenumber",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="gpwebsitelink",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateGplocations(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $gplocationinfo = Gplocations ::  where('gplocationid', $request->gplocationid)
							->update(['gplocations.gpname' => $request->gpname,'gplocations.gplink' => $request->gplink,'gplocations.gpaddress1' => $request->gpaddress1,'gplocations.gpaddress2' => $request->gpaddress2,'gplocations.gpaddress3' => $request->gpaddress3,'gplocations.gpsuburb' => $request->gpsuburb,'gplocations.gpstate' => $request->gpstate,'gplocations.gppostcode' => $request->gppostcode,'gplocations.gpcountry' => $request->gpcountry,'gplocations.gpphonenumber' => $request->gpphonenumber,'gplocations.gpwebsitelink' => $request->gpwebsitelink]);
		$success['message'] = "Gplocation Details Updated";
        return response()->json(['gplocationinfo' => $gplocationinfo], $this->successStatus);
	}
}
