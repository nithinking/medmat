<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Medicalconditions;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class MedicalconditionsController extends Controller
{
    //
	public $successStatus = 200;
	 /**
      @OA\POST(
          path="/v2/addMedicalconditions",
          tags={"Medicalconditions"},
          summary="Medicalconditions Info",
          operationId="Medicalconditions Info",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="bpterm",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="name",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="isactive",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="isprocedure",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="snomed",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="icpc",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addMedicalconditions(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$availCondiitons = Medicalconditions ::  where('name', $request->name)->get();
		 if($availCondiitons->count() > 0  )
		 {		 
				
			$response->msg 		= "MedicalCondiiton Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$conditions = new Medicalconditions;		
			
			$conditions->bpterm   = $request->bpterm;
			$conditions->name 	 = $request->name;			 
			$conditions->isactive = $request->isactive;			 
			$conditions->isprocedure = $request->isprocedure;			 
			$conditions->snomed   = $request->snomed;			 
			$conditions->icpc     = $request->icpc;			 
			$conditions->save();
			$response->conditions 	= $conditions;
			$response->msg 		= "MedicalCondiiton Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 
	 /**
      @OA\Get(
          path="/v2/getAllMedicalconditions",
          tags={"Medicalconditions"},
          summary="Medicalconditions Info",
          operationId="Medicalconditions Info",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllMedicalconditions(){
		 $user = Auth::user();
		
         $conditions = Medicalconditions::get();
		
        return response()->json(['conditions' => $conditions], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/deleteMedicalconditions",
          tags={"Medicalconditions"},
          summary="Medicalconditions Info",
          operationId="Medicalconditions Info",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="medicalid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deleteMedicalconditions(Request $request){
		 $user = Auth::user();
		
        $conditions = Medicalconditions :: where('medicalid','=',$request->medicalid)->update(['medicalconditions.isactive' => 0]);
		
		 
		$success['message'] = "MedicalCondiiton deleted";
        return response()->json(['conditions' => $conditions], $this->successStatus); 
      
  }
  
  /**
      @OA\Post(
          path="/v2/updateMedicalconditions",
          tags={"Medicalconditions"},
          summary="Medicalconditions Info",
          operationId="Medicalconditions Info",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="medicalid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="bpterm",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="name",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="isprocedure",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="isactive",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="snomed",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="icpc",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateMedicalconditions(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $conditions = Medicalconditions ::  where('medicalid', $request->medicalid)
							->update(['medicalconditions.bpterm' => $request->bpterm,'medicalconditions.isactive' => $request->isactive,'medicalconditions.name' => $request->name,'medicalconditions.isprocedure' => $request->isprocedure,'medicalconditions.snomed' => $request->snomed,'medicalconditions.icpc' => $request->icpc]);
		$success['message'] = "Medicalconditions Details Updated";
        return response()->json(['conditions' => $conditions], $this->successStatus);
	}
}
