<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Business;
use App\models\Locations;
use App\models\Deliverypartner;
use App\models\Locationdeliverytimings;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class LocationsController extends Controller
{
    //
	 public $successStatus = 200;
	
	/**
      @OA\Get(
          path="/v1/getAllLocations",
          tags={"Locations"},
          summary="Locations Information",
          operationId="Locations Information",
		  security={{"bearerAuth": {}} },
		  
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllLocations(Request $request){
		 $user = Auth::user();
		if(isset($request->location_id)){
		$locations = Locations::where('locationid',$request->location_id)->get();}
		elseif(isset($request->status)){
		$locations = Locations::where('locationstatus',$request->status)->get();
		}else{
			$locations = Locations::get();
		}
		$Locations = $locations->map(function ($location, $key) {
        $location->logo = url('pharmacy/'. $location->logo);
        return $location; 
    });
    return response()->json(['locations' => $Locations], $this->successStatus);
      
  }
  /**
      @OA\Get(
          path="/v1/getlocdeliverypartners",
          tags={"Locations"},
          summary="Locations Delivery partner Information",
          operationId="Locations Delivery partner Information",
		  security={{"bearerAuth": {}} },
		  
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getLocdeliverypartners(Request $request){
		 $user = Auth::user();
		if(isset($request->partner_id)){
         $locations = DB::table('deliverypartner')->where('locationid',$request->location_id)->where('deliverypartnerid',$request->partner_id)->where('partnerstatus',1)->get();
		}else{
		 $locations = DB::table('deliverypartner')->where('locationid',$request->location_id)->where('partnerstatus',1)->get();
		}
        return response()->json(['deliverypartners' => $locations], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v1/deleteLocations",
          tags={"Locations"},
          summary="Locations Information",
          operationId="Locations Information",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deleteLocations(Request $request){
		 $user = Auth::user();
		
        
		$locations = Locations :: where('locationid','=',$request->locationid)
								->update(['locations.locationstatus' => '0']);
		 
		$success['message'] = "Location deleted";
        return response()->json(['locations' => $locations], $this->successStatus); 
      
  }
  /**
      @OA\POST(
          path="/v1/addLocations",
          tags={"Locations"},
          summary="Locations Information",
          operationId="Locations Information",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="brandname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="Website",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),@OA\Parameter(
              name="businessid",
              in="query",
              required=flase,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="locationname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="address",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="streetaddress",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="suburb",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="state",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="phone",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="website",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="rating",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="email",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="displaypostcodes",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="logo",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="photo",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="shortdesc",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="detaileddesc",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="deliverytext",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="businesstype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="subscriptiontype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="isfeatured",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="services",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="HealthProfessionals",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="AboutHealthProfessionals",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  
			  @OA\Parameter(
              name="timings",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="promomessage",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="latitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="longitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="timezone",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="featured_postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="pickupinstruction",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="medviewFlowStoreID",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="OTCPricingTier",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ScriptPricingTier",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addLocations(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		 
		 if($request->businessname!="" && $request->website!="")
		 {
			$Business = new Business;		
			
			$Business->businessname = $request->businessname;
			$Business->website = $request->website;
			$Business->grouptypeid  = 2;
			$Business->parentbusinessid  = 0;				 
			$Business->save();
			$businessid = $Business->id;
		 }
		 else
			$businessid = $request->businessid;
		
		$availLocations = Locations ::  where('locationname', $request->locationname)->get();
		 if($availLocations->count() > 0  )
		 {		 
				
			$response->msg 		= "Location Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$locations = new Locations;		
			
			$locations->businessid = $businessid;
			$locations->locationname = $request->locationname;
			$locations->address  = $request->address;
			$locations->streetaddress  = $request->streetaddress;
			$locations->suburb  = $request->suburb;				 
			$locations->state  = $request->state;				 
			$locations->phone  = $request->phone;				 
			$locations->website  = $request->website;				 
			$locations->rating  = $request->rating;				 
			$locations->email  = $request->email;				 
			$locations->postcode  = $request->postcode;				 
			$locations->displaypostcodes  = $request->displaypostcodes;	
			if($request->logo!="")
			{
				$extension = explode('/', mime_content_type($request->logo))[1] ?? "jpg" ;
			  	$locations->logo  = $this->base64_to_jpeg($request->logo,time()."-".$request->locationname.'.'.$extension);
			}
			$locations->photo  = $request->photo;				 
			$locations->shortdesc  = $request->shortdesc;				 
			$locations->detaileddesc  = $request->detaileddesc;				 
			$locations->deliverytext  = $request->deliverytext;				 
			$locations->businesstype  = $request->businesstype;				 
			$locations->subscriptiontype  = $request->subscriptiontype;				 
			$locations->isfeatured  = $request->isfeatured;						 
			$locations->services  = $request->services;						 
			$locations->HealthProfessionals  = $request->HealthProfessionals;						 
			$locations->AboutHealthProfessionals  = $request->AboutHealthProfessionals;							 
			$locations->timings  = $request->timings;						 
			$locations->promomessage  = $request->promomessage;						 
			$locations->latitude  = $request->latitude;						 
			$locations->longitude  = $request->longitude;						 
			$locations->locationstatus  = 1;						 
			$locations->timezone  = $request->timezone;						 
			$locations->featured_postcode  = $request->featured_postcode;						 
			$locations->pickupinstruction  = $request->pickupinstruction;						 
			$locations->medviewFlowStoreID  = $request->medviewFlowStoreID;						 
			$locations->OTCPricingTier  = $request->OTCPricingTier;						 
			$locations->ScriptPricingTier  = $request->ScriptPricingTier;	
      $locations->OTCCatalogueBusinessID  = $request->OTCCatalogueBusinessID; 
      $locations->OTCCatalogueLocationID  = $request->OTCCatalogueLocationID; 
      $locations->ScriptCatalogueBusinessID  = $request->ScriptCatalogueBusinessID; 
      $locations->ScriptCatalogueLocationID  = $request->ScriptCatalogueLocationID; 					 
			$locations->save();
			
			
			$response->locations 	= $locations;
			$response->msg 		= "Location Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 
	 /**
      @OA\POST(
          path="/v1/updateLocations",
          tags={"Locations"},
          summary="Locations Information",
          operationId="Locations Information",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="businessid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="locationname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="address",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="suburb",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="state",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="phone",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="website",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="rating",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="email",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="displaypostcodes",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="logo",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="photo",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="shortdesc",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="detaileddesc",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="deliverytext",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="businesstype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="subscriptiontype",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="isfeatured",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="services",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="HealthProfessionals",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="AboutHealthProfessionals",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="deliveryfee",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="ordervaluequlifiing",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="timings",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="promomessage",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="latitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="longitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="timezone",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function updateLocations(Request $request)
   {
     $response     = (object)array();
     $user = Auth::user();
    
    if ( Locations::where('locationid',  $request->locationid)->exists()) {
    $locations = Locations::where('locationid',  $request->locationid)->first();
    //{
      //$locations = new Locations;     
    if(!empty($request->logo))
	{
        if ($this->is_base64($request->logo)) {
          $extension = explode('/', mime_content_type($request->logo))[1] ?? "jpg" ;
		  $logo = $this->base64_to_jpeg($request->logo,time()."-".$request->locationname.'.'.$extension);
        }
        else{
          $logo = $locations->logo;
        }
	}
    else{
        $logo = $locations->logo;
    }
      $saveData = [
        'businessid' => $request->businessid,
        'locationname' => $request->locationname,
        'address'  => $request->address,
        'suburb'  => $request->suburb,       
        'state'  => $request->state,       
        'phone'  => $request->phone,     
        'website'  => $request->website,         
        'rating'  => $request->rating,
        'email'  => $request->email,     
        'postcode'  => $request->postcode,         
        'displaypostcodes'  => $request->displaypostcodes,
        'logo'  => $logo ?? $locations->logo,      
        'photo'  => $request->photo,     
        'shortdesc' => $request->shortdesc,    
        'detaileddesc'  => $request->detaileddesc,         
        'deliverytext'  => $request->deliverytext,         
        'businesstype'  => $request->businesstype,         
        // 'subscriptiontype'  => $request->subscriptiontype,         
        'isfeatured'  => $request->isfeatured,
        'services'  => $request->services, 
        'HealthProfessionals'  => $request->HealthProfessionals,       
        'AboutHealthProfessionals'  => $request->AboutHealthProfessionals,
        'deliveryfee'  => $request->deliveryfee,   
        'ordervaluequlifiing'  => $request->ordervaluequlifiing,
        'timings'  => $request->timings,       
        'promomessage'  => $request->promomessage,
        'latitude'  => $request->latitude,           
        'longitude'  => $request->longitude,           
        'locationstatus'  => 1,      
        'medviewFlowStoreID'  => $request->medviewFlowStoreID,
        'timezone'  => $request->timezone,
        'pickupinstruction'  => $request->pickupinstruction,    
        'OTCCatalogueBusinessID'  => $request->OTCCatalogueBusinessID,
        'OTCCatalogueLocationID'  => $request->OTCCatalogueLocationID,
        'ScriptCatalogueBusinessID'  => $request->ScriptCatalogueBusinessID,
        'ScriptCatalogueLocationID'  => $request->ScriptCatalogueLocationID,
      ];
      Locations::where('locationid',  $request->locationid)->update($saveData);
      $response->locations  = $locations;
      $response->msg    = "Location Updated Successfully.";
      $response->status     = $this->successStatus;
    }
    return json_encode($response); 
       
   }
	 
	 /**
      @OA\POST(
          path="/v1/addDeliveryPartners",
          tags={"DeliveryPartners"},
          summary="DeliveryPartners Information",
          operationId="DeliveryPartners Information",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="partnername",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="deliverycharge",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="deliverylimit",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="deliverytext",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="scriptval",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="basekms",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="extrakmcharge",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="maxkmallowed",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="starttime",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="endtime",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="partnerimage",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addDeliveryPartners(Request $request)
   {
     $response     = (object)array();
     $user = Auth::user();

      $deliverypartner = new Deliverypartner;   
      
      $deliverypartner->locationid = $request->locationid;
      $deliverypartner->partnername = $request->partnername;
      $deliverypartner->deliverycharge = $request->deliverycharge;
      $deliverypartner->deliverytext = $request->deliverytext;
      $deliverypartner->deliverylimit = $request->deliverylimit;
      $deliverypartner->scriptval = 2;
      $deliverypartner->basekms = $request->basekms;
      $deliverypartner->extrakmcharge = $request->extrakmcharge;
      $deliverypartner->maxkmallowed = $request->maxkmallowed;
      $deliverypartner->starttime = $request->starttime;
      $deliverypartner->endtime = $request->endtime;
      $deliverypartner->partnerimage = $request->partnerimage;
      $deliverypartner->partnerstatus = $request->partnerstatus ?? 1;
                 
      $deliverypartner->save();

      $deliverypartner = new Deliverypartner; 
      $deliverypartner->locationid = $request->locationid;
      $deliverypartner->partnername = $request->partnername;
      $deliverypartner->deliverycharge = $request->deliverycharge;
      $deliverypartner->deliverytext = $request->deliverytext;
      $deliverypartner->deliverylimit = $request->scriptval;
      $deliverypartner->scriptval = 1;
      $deliverypartner->basekms = $request->basekms;
      $deliverypartner->extrakmcharge = $request->extrakmcharge;
      $deliverypartner->maxkmallowed = $request->maxkmallowed;
      $deliverypartner->starttime = $request->starttime;
      $deliverypartner->endtime = $request->endtime;
      $deliverypartner->partnerimage = $request->partnerimage;
      $deliverypartner->partnerstatus = $request->partnerstatus ?? 1;
                 
      $deliverypartner->save();
      $response->deliverypartner  = $deliverypartner;
      $response->msg    = "DeliveryPartners Added Successfully.";
      $response->status     = $this->successStatus;
    
    return json_encode($response); 
       
   }
	 /**
      @OA\POST(
          path="/v1/addDeliveryTiming",
          tags={"Locationtimings"},
          summary="Locationtimings Information",
          operationId="Locationtimings Information",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="deliveryTime",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  
			 
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addDeliveryTiming(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$dflag=false;
		$business = Locations :: where('locationid',$request->locationId)->get();
		$businessid = $business[0]->businessid;
		//print_r($request->deliveryTimings);exit;
		$deliverytimings=$request->deliveryTimings;
		 //count($deliverytimings)
		$deliveryslot = array("sunShift1Checked"=>"6AM-8AM","sunShift2Checked"=>"8AM-10AM","sunShift3Checked"=>"10AM-12PM","sunShift4Checked"=>"12PM-2PM","sunShift5Checked"=>"2PM-6PM","sunShift6Checked"=>"2PM-4PM","sunShift7Checked"=>"4PM-6PM","sunShift8Checked"=>"6PM-8PM","sunShift9Checked"=>"8PM-10PM",
"monShift1Checked"=>"6AM-8AM","monShift2Checked"=>"8AM-10AM","monShift3Checked"=>"10AM-12PM","monShift4Checked"=>"12PM-2PM","monShift5Checked"=>"2PM-6PM","monShift6Checked"=>"2PM-4PM","monShift7Checked"=>"4PM-6PM","monShift8Checked"=>"6PM-8PM","monShift9Checked"=>"8PM-10PM",
"tueShift1Checked"=>"6AM-8AM","tueShift2Checked"=>"8AM-10AM","tueShift3Checked"=>"10AM-12PM","tueShift4Checked"=>"12PM-2PM","tueShift5Checked"=>"2PM-6PM","tueShift6Checked"=>"2PM-4PM","tueShift7Checked"=>"4PM-6PM","tueShift8Checked"=>"6PM-8PM","tueShift9Checked"=>"8PM-10PM",
"wedShift1Checked"=>"6AM-8AM","wedShift2Checked"=>"8AM-10AM","wedShift3Checked"=>"10AM-12PM","wedShift4Checked"=>"12PM-2PM","wedShift5Checked"=>"2PM-6PM","wedShift6Checked"=>"2PM-4PM","wedShift7Checked"=>"4PM-6PM","wedShift8Checked"=>"6PM-8PM","wedShift9Checked"=>"8PM-10PM",
"thuShift1Checked"=>"6AM-8AM","thuShift2Checked"=>"8AM-10AM","thuShift3Checked"=>"10AM-12PM","thuShift4Checked"=>"12PM-2PM","thuShift5Checked"=>"2PM-6PM","thuShift6Checked"=>"2PM-4PM","thuShift7Checked"=>"4PM-6PM","thuShift8Checked"=>"6PM-8PM","thuShift9Checked"=>"8PM-10PM",
"friShift1Checked"=>"6AM-8AM","friShift2Checked"=>"8AM-10AM","friShift3Checked"=>"10AM-12PM","friShift4Checked"=>"12PM-2PM","friShift5Checked"=>"2PM-6PM","friShift6Checked"=>"2PM-4PM","friShift7Checked"=>"4PM-6PM","friShift8Checked"=>"6PM-8PM","friShift9Checked"=>"8PM-10PM",
"satShift1Checked"=>"6AM-8AM","satShift2Checked"=>"8AM-10AM","satShift3Checked"=>"10AM-12PM","satShift4Checked"=>"12PM-2PM","satShift5Checked"=>"2PM-6PM","satShift6Checked"=>"2PM-4PM","satShift7Checked"=>"4PM-6PM","satShift8Checked"=>"6PM-8PM","satShift9Checked"=>"8PM-10PM"
);
		$deliverywindow=array("6AM-8AM"=>6,"8AM-10AM"=>8,"10AM-12PM"=>10,"12PM-2PM"=>12,"2PM-6PM"=>14,"2PM-4PM"=>14,"4PM-6PM"=>16,"6PM-8PM"=>18,"8PM-10PM"=>20);
		for($i=0;$i<count($deliverytimings);$i++){
			//print_r($deliverytimings[$i]['day']);exit;
			$Locationdeliverytimings = new Locationdeliverytimings;				
			$Locationdeliverytimings->locationid = $request->locationId;
			$Locationdeliverytimings->businessid = $businessid;
			$Locationdeliverytimings->dayoftheweek = $deliverytimings[$i]['day'];
			$Locationdeliverytimings->deliverytype = $request->deliveryType;
			$window=$deliverywindow[$deliveryslot[$deliverytimings[$i]['time']]];
			$Locationdeliverytimings->deliverywindow = $deliveryslot[$deliverytimings[$i]['time']];
			$Locationdeliverytimings->timings =$window;									 
			$Locationdeliverytimings->save();
			$dflag=true;
		}
			//$response->Locationdeliverytimings 	= $Locationdeliverytimings;
			if($dflag==true){
			$response->msg 		= "Locationdeliverytimings Added Successfully.";
			$response->status 		= $this->successStatus;}else{
			$response->msg 		= "Try Again";
			$response->status 		= 401;
			}
		
		return json_encode($response); 
       
	 }
	 
	/**
      @OA\Get(
          path="/v1/getAllDeliverypartners",
          tags={"Deliverypartners"},
          summary="Deliverypartners Information",
          operationId="Deliverypartners Information",
		  security={{"bearerAuth": {}} },
		  
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllDeliverypartners(Request $request){
		 $user = Auth::user();
		
      if(isset($request->partner_id)){
        $deliverypartner = Deliverypartner::where('deliverypartnerid',$request->partner_id)->get();
      }
      else{
        $deliverypartner = Deliverypartner::get();
      }
		
      return response()->json(['deliverypartner' => $deliverypartner], $this->successStatus); 
      
  }

  public function updateDeliveryPartners(Request $request)
   {
     $response     = (object)array();
     $user = Auth::user();
    if (Deliverypartner::where('deliverypartnerid',  $request->deliverypartnerid)->exists()) {

      $reqData = [
        'locationid' => $request->locationid,
        'partnername' => $request->partnername,
        'deliverycharge'  => $request->deliverycharge,               
        'deliverytext'  => $request->deliverytext,        
        'basekms'  => $request->basekms,         
        'extrakmcharge'  => $request->extrakmcharge,
        'starttime'  => $request->starttime,
        'endtime'  => $request->endtime,
        'partnerimage'  => $request->partnerimage,
        'partnerstatus'  => $request->partnerstatus,
      ];
      /*record one*/
      $data = [
        'deliverylimit'  => $request->deliverylimit,
        'scriptval'  =>  2
      ];
      $saveData = array_merge($reqData,$data);
       $deliverypartner = Deliverypartner::where('deliverypartnerid',  $request->deliverypartnerid)->update($saveData);
      /*record two*/
      $nxt = [
      'deliverylimit' => $request->scriptval,
      'scriptval'  =>  1
      ];
      $nxtData = array_merge($reqData,$nxt);
      Deliverypartner::where('deliverypartnerid', '!=',  $request->deliverypartnerid)
      ->where('partnername', $request->partnername)
      ->where('locationid', $request->locationid)      
      ->update($nxtData);

      $response->deliverypartner  = $deliverypartner;
      $response->msg    = "DeliveryPartners Updated Successfully.";
      $response->status     = $this->successStatus;
    }
    return json_encode($response); 
       
   }
  
  public function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 
    
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );
    
        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[1] ) );

        $file = public_path('pharmacy/'). $output_file;
        file_put_contents($file, $ifp);
        rename($output_file, 'pharmacy/'.$output_file);
        // clean up the file resource
        fclose( $ifp ); 
        return $output_file; 
    }

    protected function is_base64($logo){
      // Check if there are valid base64 characters
      if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $logo)) return false;

      // Decode the string in strict mode and check the results
      $decoded = base64_decode($logo, true);
      if(false === $decoded) return false;

      // Encode the string again
      if(base64_encode($decoded) != $logo) return false;

      return true;
  }
}
