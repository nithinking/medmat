<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Allergies;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class AllergiesController extends Controller
{
    public $successStatus = 200;
	 /**
      @OA\POST(
          path="/v2/addAllergies",
          tags={"Allergies"},
          summary="Allergies Info",
          operationId="Allergies Info",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="allergyclass",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="allergyname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addAllergies(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$availallergies = Allergies ::  where('allergyname', $request->allergyname)->get();
		 if($availallergies->count() > 0  )
		 {		 
				
			$response->msg 		= "Allergy Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$allergies = new Allergies;		
			
			$allergies->allergyclass = $request->allergyclass;
			$allergies->allergyname = $request->allergyname;			 
			$allergies->save();
			$response->allergies 	= $allergies;
			$response->msg 		= "Allergies Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 
	 /**
      @OA\Get(
          path="/v2/getAllAllergies",
          tags={"Allergies"},
          summary="Allergies Info",
          operationId="Allergies Info",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllAllergies(){
		 $user = Auth::user();
		
         $allergies = Allergies::get();
		
        return response()->json(['allergies' => $allergies], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/deleteAllergies",
          tags={"Allergies"},
          summary="Allergies Info",
          operationId="Allergies Info",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="allergyid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deleteAllergies(Request $request){
		 $user = Auth::user();
		
        $allergies = Allergies :: where('allergyid','=',$request->allergyid)->delete();
		
		 
		$success['message'] = "Allergies deleted";
        return response()->json(['allergies' => $allergies], $this->successStatus); 
      
  }
  
  /**
      @OA\Post(
          path="/v2/updateAllergies",
          tags={"Allergies"},
          summary="Allergies Info",
          operationId="Allergies Info",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="allergyid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="allergyclass",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="allergyname",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateAllergies(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $allergies = Allergies ::  where('allergyid', $request->allergyid)
							->update(['allergies.allergyclass' => $request->allergyclass,'allergies.allergyname' => $request->allergyname]);
		$success['message'] = "Allergies Details Updated";
        return response()->json(['allergies' => $allergies], $this->successStatus);
	}
}
