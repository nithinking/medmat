<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User;
use App\models\Immunisation;
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use Session;
use Config;
use App;
use DB;
class ImmunisatoinController extends Controller
{
    public $successStatus = 200;
	 /**
      @OA\POST(
          path="/v2/addImmunisation",
          tags={"Immunisation"},
          summary="Immunisation Info",
          operationId="Immunisation Info",
		  security={{"bearerAuth": {}} },
			@OA\Parameter(
              name="vaccinename",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="acircode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function addImmunisation(Request $request)
	 {
		 $response 	   = (object)array();
		 $user = Auth::user();
		
		$availvaccine = Immunisation ::  where('vaccinename', $request->vaccinename)->get();
		 if($availvaccine->count() > 0  )
		 {		 
				
			$response->msg 		= "Vaccine Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
		{
			$vaccine = new Immunisation;		
			
			$vaccine->vaccinename = $request->vaccinename;
			$vaccine->acircode = $request->acircode;			 
			$vaccine->save();
			$response->vaccine 	= $vaccine;
			$response->msg 		= "Vaccine Added Successfully.";
			$response->status 		= $this->successStatus;
		}
		return json_encode($response); 
       
	 }
	 
	 /**
      @OA\Get(
          path="/v2/getAllImmunisation",
          tags={"Immunisation"},
          summary="GET Immunisation",
          operationId="GET Immunisation",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getAllImmunisation(){
		 $user = Auth::user();
		
         $vaccine = Immunisation::get();
		
        return response()->json(['vaccine' => $vaccine], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/deleteImmunisation",
          tags={"Immunisation"},
          summary="Delete Immunisation Info",
          operationId="Delete Immunisation Info",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="vaccineid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deleteAllergies(Request $request){
		 $user = Auth::user();
		
        $vaccine = Immunisation :: where('vaccineid','=',$request->vaccineid)->delete();
		
		 
		$success['message'] = "Vaccine deleted";
        return response()->json(['vaccine' => $vaccine], $this->successStatus); 
      
  }
  
  /**
      @OA\Post(
          path="/v2/updateImmunisation",
          tags={"Immunisation"},
          summary="Immunisation Info",
          operationId="Immunisation Info",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="vaccineid",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="vaccinename",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),			  
			  @OA\Parameter(
              name="acircode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
			  
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updateImmunisation(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $vaccine = Immunisation ::  where('vaccineid', $request->vaccineid)
							->update(['immunisation.vaccinename' => $request->vaccinename,'immunisation.acircode' => $request->acircode]);
		$success['message'] = "Vaccine Details Updated";
        return response()->json(['vaccine' => $vaccine], $this->successStatus);
	}
  
  
}
