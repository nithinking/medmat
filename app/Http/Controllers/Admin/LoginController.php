<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use DB;
use Mail;
use Helper;
use Session;
use Config;
use App;
use DateTime;
use Response; 

/**
  @OA\Info(
      description="",
      version="1.0.0",
      title="Medmate Admin API",
 )
 
* @OA\SecurityScheme(
*      securityScheme="bearerAuth",
*      in="header",
*      name="bearerAuth",
*      type="http",
*      scheme="bearer",
*      bearerFormat="JWT",
* ),
 */
class LoginController extends Controller
{
	public $successStatus = 200;
    /**
      @OA\Post(
          path="/v1/login",
          tags={"User"},
          summary="Login",
          operationId="login",
      
          @OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Parameter(
              name="password",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),    
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	public function login(){ 
		if(Auth::attempt(['username' => request('username'), 'password' => request('password') ,'profiletypeid' => '8','userstatus' => '1'])){ 
		   $user = Auth::user(); 
		   $success['token'] =  $user->createToken('AppName')-> accessToken; 
		   $success['user_details']=$user;
		  
			return response()->json(['success' => $success], $this-> successStatus); 
		  } else{ 
		   return response()->json(['error'=>'Unauthorised'], 401); 
		   } 
	}
	
	/**
      @OA\post(
          path="/v1/forgotpassword",
          tags={"User"},
          summary="Forgotpassword",
          operationId="Forgotpassword",
    security={{"bearerAuth": {}} },
		
		@OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string")
              ),
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function forgotpassword(Request $request)
		{ 
		$response=(object)array();
		
			$users_existance=User :: where('username',$request->username)
									->where('userstatus','=',1)
									->where('profiletypeid','=',1)
									->get();
			if($users_existance->count() > 0)
			{				
			if($users_existance[0]['username'] != $request->username)
			{
				$response->msg 		= trans('messages.email_sent');
				$response->status 		= $this->successStatus; 
			}
			else
			{
				$link=url('resetpass/'.$users_existance[0]['hashcode']);
				$subject="Medmate – Forgot your password";
				//$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Reset Password</button></a>";
				//$body_message = trans('messages.Forgot_password_email');
				//$body_message 	= 	str_replace('Reset_password_button',$content,$body_message);
				$body_message 	= 	'
				<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <title>Document</title>
</head>
<style>
    body  {
        font-family: "Nunito";font-size: 18px;
    }
    .top {
        display: flex;flex-direction: row;width: 700px;justify-content: space-between;
    }
    .heading {
       color: #1d9bd8;font-size: xx-large;font-weight: 600;
    }
    .total {
        width: 700px;  margin: auto;
    }
    .bottom {
        text-align: center;color :#7e7e7e;
    }
    .one {
        background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;width: 120px;height: 35px;margin-left: 300px;margin-top: 50px;
    }
</style>
<body>
<div class="total">
    <div class="top">
        <p style="padding-top: 15px;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAd0AAABqCAMAAADOW3slAAAAn1BMVEX///8dmtZYWFpBqNxRUVMAldQAltRQUFJ/vONquOJUVFZLS01HR0r09PSwsLG73fGGxefX19diYmOnp6jLy8za7vh4v+QAktOv2O+GhoehoaLj8voon9jE4/NXsN/39/d7e3zu9fvm5uZeXmBra23V1dXg4OCf0OvCwsN/f4CWy+rS6/bp9fuSkpOampv1/P44ODu9vb1itOBMrd1ApNpRVRAqAAAOnklEQVR4nO2deXuyOhPGFVmO4L7UDat1rVrrW+v3/2wvi9JkZgJBeUyL3n+c6zySBDo/EkIyMxQKrHb9Rvm7lAcdy9vxqPBUpG7DNQyzmBcZpnnqH1Qb9Zfoo1w0VAPJXIa7Vm3XX6F+Dtn6Ml67qk2rXo18svVkunXVxlWtl9zC9WQ+ON5cw/VG57FqA6vUOt9wvd77odrE6tTNz1uQQOa3ahur03fu6RZ7fdVGVqVx/uEWiyfVVlalV9WWv4d6Dzpv/niErls0y6rtrEb1h6BbLD7mivPLY9Dt7VQbWom+Vdv9PjIecztBtdnvJKOh2tBKpNrsd5L5n2pDK5Fqs2clo8cJHn7S/ctyx11WoyM4/qT7l+UCJ6oyeBV40v3LetIlpQZG5nrSJaUGRubKC91Zk9PyxubUwMhceaHbrjis/ndjc2pgZK680K1ZGqvKjc2pgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJqYGRuZ50SamBkbmedEmpgZG5nnRJpbCgaQQOEOL0C8bZQcKQDjvzmgxkpknpEFVifsuM7qy5XC6bs4QCy9VEsrnle9tXZ1mVKp+ObrXZqbUHg3271mmS7WNEvAvLxUqmcfqvv/soFEbj9X8uAcMwXxv93ahweNvVt68ytIyeW26s+3VP68bxJHdPmL1Tebs+1ylGl5eOLr8RExlm0tm3NFv3pM0H74S5VrXp/FJg0W4mkJosveZ0x/Ll6Nqidrlnvqa8wtK+Cm1IdxIJNt8cLjTba9q2vf86ttf+KpGu0X/jtA7MZJrlHePvfRgfgfWM7zUXRvmxdeP5GuZxPWJdyA+jdeI9YRrf2xF3HS/njBCp6FYrNqtKJ/y1trD0yK62Mx8Cvp2pZtk/BSx72iGpFojmggraPrR/2+HO7/i/reYtT4u5xmvROmte41v/WujMxYguCBkQRNz0fesZR5R9YvfNdDXjVEe+/m+NOExug8o3tCubcXzN1zE6zUejaKany5nFCWzSWfDdRtOs1jtTabUB1vTtORCOt+8tVFzTdG3od0HQQfWg+QoqzV9Mm2l8Uls4uHX/gqb8gCJFlwqQPDQiA5oNMo5jfBKgMoskW1+7o3B8Nkp0qFf31ciA7idhLtsaRuNhTdPRcf8GwKNhcIaBI4A0nQnoCipQdJtTkm1wyfqQHcIl6LqCtDGXmH1h3pERjdcoxeWhWYvgvghDgbbmrXSrU9q4zj6sMdkLjG/blPvEqgXHgZ8KWvNWujVbxDYoOGWmhIl0eyehVQO85knManSiMCWEBXTJeyI2VKR+I90JGpWjY59+hclUTGuO59fNWPNXVrWb6IqGheiK9J8rSqRrxkTPlf0xMS4pRRePs0ZiHPyhhGv14jObjIs30LXep9SoG8r58iqI4XrGXMDZbDxcTZvv+eOp6IpvxJ8r+sGbSDc2m5dbLMZn++rDqABTJhivBHtvAlwP7w10tXkcDXtWGMQa1Bnyp16Jb5VLk/w/U9FdJLbO4k2im2DUxKB2GGMoF2npgv6eOoIvFd1Y6fuvpKGQm1lNWvE9lziDX02S7iax5wZXdBlPbqNbSBxmd1znTR6WQ404OEb6yPns6MKehqXv2TMPJDoXqO9Xk6PbTigVlT5f0o10k1ViMUnHWfbZR694XidUhnQlxEysOlKdi5M83aV04+E7/L+n22fs/C2PiVkMu+aS7krXYtaRUo/LKehOFtKNn8fmf073jcGUIr1B96fW8Yqz3pWuvYiafpccOllJ061RZWxbp+bowVz/39P9mVcZL2mqRckr09wTka6gG6wKE8uHP3b0x0WdGhytaEGyJajpOI6waVm6Vdy4bc0Xm8/NQkNXFXZeObqj9X/l8jbOzPVGudwgc5lvo20mcml5vd1uqdeu0S1dNz1dXWsvZ7PVu3BSaluDzmo2a7aJJUnrsnq/pBhZ2qDW6XRqU5tu+7zO7G896fAO0C9y2kTXtTad8L6afc3hVTn+IrkM3UO5Z5ieeiUR37VpeCUMw9jiY+NzJzRx1z1s3V6wVdsj1pDPgOh3rtH66HoqrwVrKWnphmtSvlbITqGZF5f3nsknqh49ePcE+Xm0F1GtWVTbAd3Z59DXFCx0DC/67KBxwdbYLSF4VcHjQoLu6GfjRpBw8Ri99hh44nTphD3UdcdMQvfeK6y4Cw9SE+bR63lj1zR6ZZJvSrrsvKhArVzpU2ZFqgbr24PwwAR33Qq7t1OobojOrTMFYnbvO6CqDhbJ4FX5r+HJdA/sYYPqSWx+Z/xuejgvTaBEq1tuwdEsvYF6wQObeonqs/uERpFax0pH1/pk61bhJqvfEbitvk8dHg5/hwAukxu2KsYrSXcAtnMXcPcRbE74d2wyXT5f2ekNHo+G3rO1Ufd26d/7YDUZ5dkN0vkTa5Awn7RJ4E1HFxjqHXVeC2yLI/7hz0NY0eL33H3t0cNXju4EnNPBuxf8sO7fcol0R7wt8ZP1ABYb0VAaHu+BEXSEdoJg0+EyFxqYce5DYladii5iAJ5+mj0FBb4AopDCBNbTB+jCChM085Wju+TvHG43/6wmX0SvJtOFH8RA8HagABq8wyyyLvgV2t8/N9/0m9/p0Xh+cFG9YgndAul2EWA3ADbG42sVAKqQv2oatbePhm85uuCITbnt8XeXtUykeyjBArCjNKAh4dw4sDT8ldgbRJsFR9/tA/4FW3xXEANKGrqoZ6JtHgc52IBeWgkKzMBdwy9AR4IrTnJ0+ceu/omaLcBlUOsrke4bpICenyjBM5w2Bw9uE1Qjv5HjojJ4d4h093DhbCANXWyp6oK3P9rChU/YkC7slQ7tVfcORgY5uvyAbk8HhDbw70qiu0MbtKATos6N9vMDuvDDIqMuITAsePMuNMzXSb8rVCzV7j2e+oD5CX58ghXHkC58Grdon7oZGBnk6MKpACm+yCaRLrbmK1/gAz8HwYtt2Hev+KaXNxnvwWr0d7Hop4EkXaKL8cOgjUfYJUUXvJLgEf8sMPuVoltNvzuhLeR8IuPojuBxmm7xiq/GjHqYriDjcAkUy5SuPkQFOjJ0iRlzILggxRwS0p1dQVe7nW4XHhf03SvoHjy68DdixuwLzuR/A12iWijKr+osId1kf55/Qle2716Ruv5gSNOFz/pfQZec1xaedEN5r7Z/ue9mOjJf89y9I91rRmYXrXAV0Az9F9OdCv4uwCAHdK967rp4VkUscREXpYYufCMiHNl9ZfJG9MvoXuFh4c2q0Pc36U9SosUqJXQlVzPgOqccXbDAMm9J6G504XrDbpws710brYyNUFL8QEld/C504UqkvUH1fMENpqtWIolNBEL3omuCJcVtz0iWVw2tIFMvvNg7RwlduEmn6VQIGXJ+v2oXQUftUroX3SIA8EZAkuIGt6SCUmghTAndwgYuFy5QxUIVbd/L0W3ybVM7gFh3o2uAE2/JBygS3Dik1iKJQBQ1dMGkmdrgrWKf5Ot27yt05DCvu9FF32qT++4vmjQXDiirA+FJq4ZuE3stgqpVwmlL0vMGeuQJZuS+ZhdHvbvRRdPaD2FmDeNU/vl/3C8PfHx+j3KTVkMXe+Ro1oaFsKRC+GS95sDAYAvi/gurvXaZzt2NLh5j34g4XT/twnf/wLhqwTfZgu8oW4zgGS7ppqmILnSn04JcGbNwe7ja3JORvZJ0UZyJrdWwg8bsa2rp0QPhfnQJx2SG0plV8bT1h2Jm39GgfNy7jZOftsr0itOrJIroNqkoB8ta7Ift4X7h0GvFknTRe7LX8qLGbiFPmu2p7afUsO9Pl+qFo61HyfQd3YN8ZK/r88OZoYt9b0LtxvX6WLhprIgu8qoJZeu6hcIMUtMlI02s6acf6NCptfce2XM0iwq69Ddwu/V14+Wlsa2z4fOszwDyqpGRKrrYozlZsnTxm3JAUveDlByLvX1U0EVb7GKxdK/6dK4quoLOmw1d5IkrkhK68pw4B/jYvCsCKaObFMt3E92VZHS2ErpJKVR+tGPhXJFYQR3dwjB18L08XXps/jV0sVs5LX658YqYYnV0ySCzrOhSYUgU3fu/7/oyJINxwWIyzDWWLIV0Z4nJFa7yVj9LCq90zpts6crG3wO6ZuJ87PAbfDPOasanybFbn1d43kTCUWa/h65k74UbQSg+EOhQBk90lXQLzbj0ZrY9i9nMk8i+3Y7L/qCYrtcPJabAO7gEbaLgbVaH4y2ZBDOnW5iJ0/3p8xWZBfQsmdzqHTo7wK+gWzTJcGpeXRz+eRI/ew+vxu35mTndSNcbPyt0B3OmVTqD71lSmfOrotbPsvRLzOL96RaLvWPCLGlMeT4K35bHxZsz+ELdTLewbBE5lC07MPutdL3BYVMR9V+9Mv+JR1VBt2gaLzE9sU/uHfl5nalO/+Enmk2kyyVouAddP9F+hXtC6o59zsXPZ87XHaaS9FcvZsM5ypFk61Zl/slmV4duSjBNwT+h6w/Pxzr1/B2Ny67YacP8hhn6u40ws0pSLALX8av/4z9QjeluuE9YV3BQQQc0QUb7zdqbeeX81YtKa/9+2a97B4GZTJVaxWIV++3sZXs6d5xLUadiT/c1sKOPTH9scDoiE5/4Ai8YwgtfgnYwNw33uN5F37A4vI269UbJjf0wglfLPfa7I//953AY7bbf0RZiwjn5HYwl/3F5jGbFF8BeEFXQhIhAddb5qrVrX8uZ3CdulsM2K1Ek0k/zS695T1/eGar4FDgLBXRLROILUB0zsYmoJfdUeg1UOrmm1DdrTMMMKnkV2HMnnfOKQJcc6EPKu+mvy7xiqToXQokR8qjEJOF51fgROu9JtZWVSc7z9E9LkADxEYTXhfKmpFXqXIsOq8uRrokuzY9e8o0XRYk+mHKNN/ts8X9NdBaoPMgUfqXwgdQv5pOv8XpFFrT86aOcQ76Ge40vdC7VbbjiT9r/PRmmeSK/wvGw2vUb5e9SHnQsb+GHHx9Q/wcKM58K8oW5AQAAAABJRU5ErkJggg==" width="180" height="40"></p>
        <p class="heading">Password Reset </p>
    </div>
    <div>
        <p>Forgot your password?</p>
        <p>No problem. Click the below link and create a new password.</p>
        <p style="height: 150px;width:700px;background-color: #f2f2f2;"><a href="'.$link.'"><button class="one">Reset Password </button></a></p>
        <p>Didn’t ask to reset your password? No worries. <br> Ignore this email and we’ll keep it the same.</p>
        <p>If you have any questions, please email <a href="">enquires@medmate.com.au</a> </p>
    </div>
    <div class="bottom">
        <p>
            Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
            Empowering Healthcare Consumers with Choice, Value, & Convenience <br> medmate.com.au
        </p>
    </div>
</div>
</body>
</html>
				';
				$message['to']           = $users_existance[0]['username'];
                $message['subject']      = $subject;
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			}
			}
			else
			{
				$response->msg 		= trans('messages.email_sent');
				$response->status 		= $this->successStatus; 
			}
			
			return json_encode($response); 
		}
	
	/**
      @OA\post(
          path="/v1/logout",
          tags={"User"},
          summary="User Logout",
          operationId="User Logout",
    security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
		public function logout(Request $request)
		{ 
			$request->user()->token()->revoke();
			return response()->json([
				'message' => trans('messages.logout_messgae')
			]);
        
		}
}
