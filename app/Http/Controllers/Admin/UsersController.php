<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\models\Adminprofiletype;
use App\models\Locations;
use App\User;
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;
use Helper;
use Session;
use Config;
use App;
use DateTime;
use Response;
class UsersController extends Controller
{
	 public $successStatus = 200;
    /**
      @OA\Post(
          path="/v2/addPharmacyUser",
          tags={"PharmacyUser"},
          summary="Add Pharmacy Address",
          operationId="PharmacyUser",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
          
		   
		   @OA\Parameter(
              name="username",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),@OA\Parameter(
              name="locationid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="password",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  
			
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function addPharmacyUser(Request $request) {
		
		 $response 	   = (object)array();
		 $user = Auth::user();
		 //$user_details = User::findOrFail($user->userid);
	
		$availUser = User ::  where('username', $request->username)->where('profiletypeid',3)->where('userstatus',1)->get();
		 if($availUser->count() > 0  )
		 {		 
				
			$response->msg 		= "User Already Exists. ";
			$response->status 		= $this->successStatus;
		 }
		 else
			{
				$user = new User;		
				
				$user->firstname = $request->firstname;
				$user->username = $request->username;
				$user->password = bcrypt($request->password);
				$user->profiletypeid = 3;
				$user->userstatus = 1;
				$user->save();
				
				$userid = $user->userid;
				if($userid)
				{
					$business = Locations :: where('locationid', $request->locationId)->get();
					$adminprofile = new Adminprofiletype;
					$adminprofile->userid = $userid;
					$adminprofile->locationid = $request->locationId;
					$adminprofile->profiletypeid = 3;
					$adminprofile->businessid = $business[0]->businessid;
					$adminprofile->profilestatus = 1;
					$adminprofile->save();
					
					$response->msg 		= "Pharmacy User Added Successfully.";
					$response->status 		= $this->successStatus;
					
				}else
				{
					$response->msg 		= "Please check the details provided";
					$response->status 		= 402;
				}
					
					
			}
			return json_encode($response);
	}
	
	/**
      @OA\Get(
          path="/v2/getPharmacyUsers",
          tags={"PharmacyUser"},
          summary="Get Pharmacy Address",
          operationId="Pharmacy Address",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getPharmacyUsers(){
		 $user = Auth::user();
		
         $users = User::where('profiletypeid', 3)->get();
		
        return response()->json(['users' => $users], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/deletePharmacyUser",
          tags={"PharmacyUser"},
          summary="Remove Pharmacy User",
          operationId="Pharmacy User",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function deletePharmacyUser(Request $request){
		 $user = Auth::user();
		
         $users = User:: where('users.userid', '=', $request->userid)
													->where('profiletypeid', '=', 3 )
													->update(['users.userstatus' => '0']);
		$success['message'] = "User deleted";
        return response()->json(['users' => $users], $this->successStatus); 
      
  }
  
  /**
      @OA\Post(
          path="/v2/updatePharmacyUser",
          tags={"PharmacyUser"},
          summary="update Pharmacy User",
          operationId="update Pharmacy User",
		   security={{"bearerAuth": {}}},
   		  @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			 @OA\Parameter(
              name="username",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),  
			  @OA\Parameter(
              name="userstatus",
              in="query",
              required=false,
              @OA\Schema(
                  type="string")
              ),
			  @OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
 	public function updatePharmacyUser(Request $request)
    {
		$response=(object)array();
		
		 $user = Auth::user();
		 
		 $userinfo = User ::  where('userid', $request->userid)
							->update(['users.userstatus' => $request->userstatus,'users.firstname' => $request->firstname]);
		$success['message'] = "User Details Updated";
        return response()->json(['userinfo' => $userinfo], $this->successStatus);
	}
}
