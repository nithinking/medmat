<?php

namespace App\Http\Controllers\Pharmacy;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use App\User; 
use App\models\Orderdetails;
use App\models\Ordertracking;
use App\models\Orderbasketlist;
use App\models\Deliveryaddress;
use App\models\Adminprofiletype; 
use App\models\Healthprofile; 
use App\models\Basket; 
use App\models\Basketitemlist; 
use App\models\Invoice; 
use App\models\Orderinvoicelist; 
use App\models\Locations; 
use App\models\Notifications; 
use App\models\Prescriptionitemdetails;
use App\models\Doordashlogs;
use Illuminate\Support\Facades\Auth; 
use Validator;
use DataTables;
use DB;
use Mail;

class OrdersController extends Controller
{
	public function getsearchorders(Request $request)
	{
				
              return view('getsearchorders');
	}
	public function searchorders(Request $request)
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
		$profileid=$phamacy_details->profiletypeid;
		if($phamacy_details->profiletypeid == 3)
		{
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 //->where('ordertracking.orderstatus' , '=' , 'Waiting For Review')
								 ->where('ordertracking.trackingstatus' , '=' , '1')	
								->where('ordertracking.orderstatus','!=','Pending Payment')
								// ->where('orderdetails.orderstatus','!=','Pending Payment')
								->where('orderdetails.orderstatus','!=',99)
								 ->where('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 //->orWhereRaw("find_in_set('orderdetails.locationid','adminprofiletype.locationid')")
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' ,DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 
								 ->get();
								
		}
		else
		{
			$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 //->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 //->where('ordertracking.orderstatus' , '=' , 'Waiting For Review')
								 ->where('ordertracking.trackingstatus' , '=' , '1')
								 ->where('ordertracking.orderstatus','!=','Pending Payment')
								// ->where('orderdetails.orderstatus','!=','Pending Payment')
								->where('orderdetails.orderstatus','!=',99)
								// ->where('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 //->orWhereRaw("find_in_set('orderdetails.locationid','adminprofiletype.locationid')")
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 
								 ->get();
		}
		$orderArray = [];
		date_default_timezone_set($timezone);
		foreach ($orders as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$order->profileid=$profileid;
			$customArray[$order->orderid] = $order;		
		}
		
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('action', function($row){
                    if($row['profileid'] == 3)
		{
                    $btn = '<a href="'. url('pharmacy/viewglobalsearchorders', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
		}else if($row['profileid']==7){
			$btn = '<a href="'. url('pharmacy/viewglobalsearchorders', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a> &nbsp;&nbsp;&nbsp; <a href="'. url('pharmacy/changepharmacy', ['orderid' => $row['orderid']]) .'"  class=" btn btn-primary btn-sm"> Change Pharmacy </a>';
		}			
					return $btn;
                })
				->addColumn('ordertype', function($row){
					  if($row->ordertype=='Pickup'){
						$valtype = $row->ordertype;
                      }else{
						if($row->deliverypartnername == "DoorDash"){
                      $valtype = '<span  style="color:red"> DoorDash </span>';
					  }else{
                      $valtype = $row->deliverypartnername ? $row->deliverypartnername : $row->deliverypartnername;
					  }}
				  return $valtype;
                })
                ->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })				
                ->rawColumns(['action','ordertype','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	
	public function getinstoreorders(Request $request)
	{
				
              return view('getinstoreorders');
	}
	public function instoreorders(Request $request)
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
		$profileid=$phamacy_details->profiletypeid;
		if($phamacy_details->profiletypeid == 3)
		{
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where('ordertracking.orderstatus' , '=' , 'Widget Order')
								 ->where('ordertracking.trackingstatus' , '=' , '1')	
								->where('ordertracking.orderstatus','!=','Pending Payment')
								// ->where('orderdetails.orderstatus','!=','Pending Payment')
								->where('orderdetails.orderstatus','!=',99)
								 ->where('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 //->orWhereRaw("find_in_set('orderdetails.locationid','adminprofiletype.locationid')")
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 
								 ->get();
								
		}
		else
		{
			$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 //->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where('ordertracking.orderstatus' , '=' , 'Widget Order')
								 ->where('ordertracking.trackingstatus' , '=' , '1')
								 ->where('ordertracking.orderstatus','!=','Pending Payment')
								// ->where('orderdetails.orderstatus','!=','Pending Payment')
								->where('orderdetails.orderstatus','!=',99)
								// ->where('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 //->orWhereRaw("find_in_set('orderdetails.locationid','adminprofiletype.locationid')")
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 
								 ->get();
		}
		$orderArray = [];
		date_default_timezone_set($timezone);
		foreach ($orders as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$order->profileid=$profileid;
			$customArray[$order->orderid] = $order;			
		}
		
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('action', function($row){
                      if($row['profileid'] == 3)
		{
                    $btn = '<a href="'. url('pharmacy/viewinstoreorders', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
		}else if($row['profileid']==7){

			$btn = '<a href="'. url('pharmacy/viewglobalsearchorders', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a> &nbsp;&nbsp;&nbsp; <a href="'. url('pharmacy/changepharmacy', ['orderid' => $row['orderid']]) .'"  class=" btn btn-primary btn-sm"> Change Pharmacy </a>';
		}			
                    
                   
					return $btn;
                })
				->addColumn('ordertype', function($row){
                      /*if($row->deliverypartnername == "DoorDash")
                      $valtype = '<span  style="color:red"> DoorDash </span>';
				    else
                      $valtype = $row->deliverypartnername ? $row->deliverypartnername : $row->deliverypartnername;
				  return $valtype;*/
				  return $row->ordertype;
                })
                ->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })				
                ->rawColumns(['action','ordertype','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	
    public function getallorders(Request $request)
	{
				
              return view('getallorders');
	}
	public function allorders(Request $request)
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		$profileid=$phamacy_details->profiletypeid;
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
					
		if($phamacy_details->profiletypeid == 3)
		{
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where('ordertracking.orderstatus' , '=' , 'Waiting For Review')
								 ->where('orderdetails.orderstatus','!=',99)
								 ->where('ordertracking.trackingstatus' , '=' , '1')							 
								 ->where('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 //->orWhereRaw("find_in_set('orderdetails.locationid','adminprofiletype.locationid')")
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname',DB::raw('(CASE 
                        WHEN orderdetails.transactionId != "" AND orderdetails.paymentstatus IS NULL THEN "Payment Failure" 
                        ELSE "Waiting For Review" 
                        END) AS currentorderstatus'))
								// ->orderBy('orderdetails.userpreffereddate', 'DESC')
								 ->get();
		}
	 if($phamacy_details->profiletypeid == 7)
	{
		//echo "Madhoo";exit;
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 //->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								 //->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where('ordertracking.orderstatus' , '=' , 'Waiting For Review')
								 ->where('orderdetails.orderstatus','!=',99)
								 ->where('ordertracking.trackingstatus' , '=' , '1')							 
								 //->where('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 //->orWhereRaw("find_in_set('orderdetails.locationid','adminprofiletype.locationid')")
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname',DB::raw('(CASE 
                        WHEN orderdetails.transactionId != "" AND orderdetails.paymentstatus IS NULL THEN "Payment Failure" 
                        ELSE "Waiting For Review" 
                        END) AS currentorderstatus'))
								// ->orderBy('orderdetails.userpreffereddate', 'DESC')
								 ->get();
	}		
		$orderArray = [];
		date_default_timezone_set($timezone);
		foreach ($orders as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->profileid=$profileid;
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('action', function($row){
                    
                    
                   if($row['profileid'] == 3)
		{
                   $btn = '<a href="'. url('pharmacy/viewinboxorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
		}else if($row['profileid']==7){
			$btn = '<a href="'. url('pharmacy/viewinboxorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a> &nbsp;&nbsp;&nbsp; <a href="'. url('pharmacy/changepharmacy', ['orderid' => $row['orderid']]) .'"  class=" btn btn-primary btn-sm"> Change Pharmacy </a>';
		}			
					return $btn;
                })
				->addColumn('ordertype', function($row){					
					 if($row->type==2){
						$valtype = $row->ordertype;
                      }else{
						if($row->deliverypartnername == "DoorDash"){
                      $valtype = '<span  style="color:red"> DoorDash </span>';
					  }else{
                      $valtype = $row->deliverypartnername ? $row->deliverypartnername : $row->deliverypartnername;
					  }}
					  return $valtype;
                
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
				
                ->rawColumns(['action','ordertype','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	
	 public function awaitingauthorisation(Request $request)
	{
				
              return view('awaitingauthorisation');
	}
	
	 public function getauthorisation(Request $request)
	{
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
		
				$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{
        $details = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Authorisation')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),DB::raw("TIMESTAMPDIFF(minute, ordertracking.statuscreatedat, UTC_TIMESTAMP) AS awatingtime"),'orderdetails.uniqueorderid',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname',DB::raw('(CASE 
                        WHEN orderdetails.transactionId != "" AND orderdetails.paymentstatus IS NULL THEN "Payment Failure" 
                        ELSE "Awaiting Authorisation" 
                        END) AS currentorderstatus'))
								 ->orderBy('orderdetails.userpreffereddate', 'DESC')
								 ->get();
								//->select('ordertracking.statuscreatedat',DB::raw("CONCAT(TIMESTAMPDIFF(minute, ordertracking.statuscreatedat, CURRENT_TIMESTAMP),' Minutes') AS awatingtime"),'orderdetails.orderid', 'orderdetails.uniqueorderid',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
		}
		else
		{
			$details = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 //->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Authorisation')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),DB::raw("TIMESTAMPDIFF(minute, ordertracking.statuscreatedat, UTC_TIMESTAMP) AS awatingtime"),'orderdetails.uniqueorderid',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname',DB::raw('(CASE 
                        WHEN orderdetails.transactionId != "" AND orderdetails.paymentstatus IS NULL THEN "Payment Failure" 
                        ELSE "Awaiting Authorisation" 
                        END) AS currentorderstatus'))
								 ->orderBy('orderdetails.userpreffereddate', 'DESC')
								 ->get();
		}
		$orderArray = [];
		date_default_timezone_set($timezone);
		foreach ($details as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		//return Datatables::of($customArray)->make(true);
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('awatingtime', function($row){
					$val = $this->days($row['awatingtime']);
					//$awatingtime = '<font color="red"> '+ $val +'</font>';
                    return  $val;
                })
				->addColumn('action', function($row){
                     $btn = '<a href="'. url('pharmacy/viewauthenticatoinorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
                   
					return $btn;
                })
				->addColumn('ordertype', function($row){
					if($row->deliverypartnername == "DoorDash")
                      $valtype = '<span  style="color:red"> DoorDash </span>';
				    else
                      $valtype = $row->deliverypartnername ? $row->deliverypartnername : $row->deliverypartnername;
				  return $valtype;
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
                
                
                ->rawColumns(['awatingtime','action','ordertype','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	
	public function readytoprocess(Request $request)
	{
				
              return view('readytoprocess');
	}
	public function getallprocess(Request $request)
	{
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		$profileid=$phamacy_details->profiletypeid;
		if($phamacy_details->profiletypeid == 3)
		{		
              $process = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 
								 ->where ('ordertracking.orderstatus' , '=' , 'Process')
								->where ('ordertracking.trackingstatus' , '=' , '1')
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname',DB::raw("TIMESTAMPDIFF(minute, ordertracking.statuscreatedat, UTC_TIMESTAMP) AS awatingtime"))
								 ->orderBy('orderdetails.userpreffereddate', 'DESC')
								 ->get();
	
		}
		else
		{
			$process = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								//->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 
								 ->where ('ordertracking.orderstatus' , '=' , 'Process')
								->where ('ordertracking.trackingstatus' , '=' , '1')
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname',DB::raw("TIMESTAMPDIFF(minute, ordertracking.statuscreatedat, UTC_TIMESTAMP) AS awatingtime"))
								 ->orderBy('orderdetails.userpreffereddate', 'DESC')
								 ->get();
		}
		  $orderArray = [];
		date_default_timezone_set($timezone);
		foreach ($process as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$order->profileid=$profileid;
			$customArray[$order->orderid] = $order;			
		}
		//return Datatables::of($customArray)->make(true);
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('action', function($row){
                     
					 if($row['profileid'] == 3)
		{
                    $btn = '<a href="'. url('pharmacy/viewprocessorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
		}else if($row['profileid']==7){
			$btn = '<a href="'. url('pharmacy/viewprocessorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a> &nbsp;&nbsp;&nbsp; <a href="'. url('pharmacy/changepharmacy', ['orderid' => $row['orderid']]) .'"  class=" btn btn-primary btn-sm"> Change Pharmacy </a>';
		}			
                   
					return $btn;
                })
				->addColumn('ordertype', function($row){					
					 if($row->deliverypartnername == "DoorDash")
                      $valtype = '<span  style="color:red"> DoorDash </span>';
				    else
                      $valtype = $row->deliverypartnername ? $row->deliverypartnername : $row->deliverypartnername;
				  return $valtype;
                
                })
				->addColumn('awatingtime', function($row){
					$val = $this->days($row['awatingtime']);
					//$awatingtime = '<font color="red"> '+ $val +'</font>';
                    return  $val;
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
                ->rawColumns(['action','ordertype','awatingtime','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	
	public function deliverylist(Request $request)
	{
				
              return view('deliverylist');
	}
	public function getdeliverylist(Request $request)
	{
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
				$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{
              $deliverylist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Delivery')
									->where ('ordertracking.trackingstatus' , '=' , '1')	
									->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','orderdetails.deliverypartnername','orderdetails.partner_deliveryid','orderdetails.delivery_tracking','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		else
		{
			$deliverylist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Delivery')
									->where ('ordertracking.trackingstatus' , '=' , '1')	
								//	->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								 ->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','orderdetails.deliverypartnername','orderdetails.partner_deliveryid','orderdetails.delivery_tracking','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
	  $orderArray = [];
	  date_default_timezone_set($timezone);
		  foreach ($deliverylist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		//return Datatables::of($customArray)->make(true);
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('action', function($row){
                     $btn = '<a href="'. url('pharmacy/viewdeliveryorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
                   
					return $btn;
                })
				->addColumn('ordertype', function($row){
					if($row->deliverypartnername == "DoorDash")
                      $valname = '<a href="'.$row->delivery_tracking.'" style="color:red" target="_blank"> DoorDash - '.$row->partner_deliveryid.' </a>';
					else
					  $valname = $row->deliverypartnername ? $row->deliverypartnername : 'Pickup';
				  return $valname;
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
                ->rawColumns(['action','ordertype','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	public function pickuplist(Request $request)
	{
				
              return view('pickuplist');
	}
	public function getpickuplist(Request $request)
	{
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
			$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);	
		if($phamacy_details->profiletypeid == 3)
		{
             $pickuplist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Pickup')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		else
		{
			$pickuplist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Pickup')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
	 
		  $orderArray = [];
		   date_default_timezone_set($timezone);
		  foreach ($pickuplist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		//return Datatables::of($customArray)->make(true);
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('action', function($row){
                     $btn = '<a href="'. url('pharmacy/viewpickuporder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
                   
					return $btn;
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
                ->rawColumns(['action','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	public function transitlist(Request $request)
	{
				
              return view('intransit');
	}
	public function gettransitlist(Request $request)
	{
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
			$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{
             $pickuplist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'In Transit')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.partner_deliveryid','orderdetails.delivery_tracking','orderdetails.deliverypartnername','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		else
		{
			$pickuplist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'In Transit')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.partner_deliveryid','orderdetails.delivery_tracking','orderdetails.orderid','orderdetails.userpreffereddate' , DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
	 
		  $orderArray = [];
		  date_default_timezone_set($timezone);
		  foreach ($pickuplist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		//return Datatables::of($customArray)->make(true);
		if(!empty($customArray))
        {
		return Datatables::of($customArray)->addIndexColumn()
                ->addColumn('action', function($row){
                     $btn = '<a href="'. url('pharmacy/viewtransitorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
                   
					return $btn;
                })
				->addColumn('ordertype', function($row){
					if($row->deliverypartnername == "DoorDash")
                      $valname = '<a href="'.$row->delivery_tracking.'" style="color:red" target="_blank"> DoorDash - '.$row->partner_deliveryid.' </a>';
					else
					  $valname = $row->deliverypartnername ? $row->deliverypartnername : 'Pickup';
				  return $valname;
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
				
                ->rawColumns(['action','ordertype','createddate'])
		->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	public function editorder(Request $request,$orderid=NULL)
	{
		
				
              return view('editorder');
	}
	
	public function completedorders(Request $request)
	{
				
              return view('completedorders');
	}
	public function getcompletedorders(Request $request)
	{
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);	
		if($phamacy_details->profiletypeid == 3)
		{
        $completedlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 
								 ->where ('ordertracking.orderstatus' , '=' , 'Completed')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)									 
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		else
		{
			$completedlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								//->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 
								 ->where ('ordertracking.orderstatus' , '=' , 'Completed')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)									 
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
	 
		  $orderArray = [];
		  date_default_timezone_set($timezone);
		  foreach ($completedlist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$completedArray[$order->orderid] = $order;			
		}
		//return Datatables::of($customArray)->make(true);
		if(!empty($completedArray))
        {
				return Datatables::of($completedArray)->addIndexColumn()
                ->addColumn('action', function($row){
                     $btn = '<a href="'. url('pharmacy/viewcompletedorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
                   
					return $btn;
                })
				->addColumn('ordertype', function($row){
                      return $row->deliverypartnername ? $row->deliverypartnername : 'Pickup';
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
                ->rawColumns(['action','createddate'])
				->make(true);
		}
		else
		{
			echo json_encode(["data"=>[]]);
		}
	}
	public function cancelledorders(Request $request)
	{
				
              return view('cancelledorders');
	}
	public function getcancelledorders(Request $request)
	{
		$this->common = new CommonController();
		$timezone = $this->common->gettimezone();
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{
        $cancelledlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Cancelled')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)	
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
		}
		else
		{
			 $cancelledlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Cancelled')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)	
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','orderdetails.ordertype','orderdetails.deliverypartnername','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
		}
	 
		  $orderArray = [];
		  date_default_timezone_set($timezone);
		  foreach ($cancelledlist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$cancelledArray[$order->orderid] = $order;			
		}
		//return Datatables::of($cancelledArray)->make(true);
		if(!empty($cancelledArray))
        {
				return Datatables::of($cancelledArray)->addIndexColumn()
                ->addColumn('action', function($row){
                     $btn = '<a href="'. url('pharmacy/viewcanelledorder', ['orderid' => $row['orderid']]) .'" class="edit btn btn-primary btn-sm"> View </a>';
                   
					return $btn;
                })
				->addColumn('ordertype', function($row){
                      return $row->deliverypartnername ? $row->deliverypartnername : 'Pickup';
                })
				->addColumn('createddate', function($row){                    
                    
					$utc_ts = strtotime($row->statuscreatedat." UTC");                  
					return  date('Y-m-d H:i', $utc_ts);
                })
                ->rawColumns(['action','createddate'])
				->make(true);
		}
		else
		{
			
			echo json_encode(["data"=>[]]);
			
		}
	}
	
	private function days($minutes){
    $d = floor ($minutes / 1440);
    $h = floor (($minutes - $d * 1440) / 60);
    $m = $minutes - ($d * 1440) - ($h * 60);
	if($d >0)
    return $d." days".$h." hours ".$m." mins";
	else
    return $h." hours ".$m." mins";
   	} 
	public function changepharmacy(Request $request,$orderid=null){
		$current_location=Orderdetails :: where('orderid',$orderid)->get(); 
		$orderid=$orderid;
		$prevlocationid=$current_location[0]->locationid;
		$locationdata=Locations :: leftJoin('deliverypartner','deliverypartner.locationid','=','locations.locationid')->where('deliverypartner.partnername','=',$current_location[0]->deliverypartnername)->where('deliverypartner.partnerstatus',1)->where('locations.locationstatus',1)->select('locations.locationid','locations.locationname')->get();
		$locations = array();
foreach ($locationdata as $key => $value){
  if(!in_array($value, $locations))
    $locations[$key]=$value;
}
		//$locations=array_unique($locationdata);
		//print_r($locations);exit;
		return view('changepharmacy', compact('orderid','locations','prevlocationid'));
	}
	public function updatepharmacy(Request $request){
		//print_r($request->orderid);exit;
	$prevlocdetails=Locations :: where('locationid',$request->prevlocation)->get();
	$currentlocdetails=Locations :: where('locationid',$request->locationid)->get();
	DB::table('orderdetails')->where('orderid',$request->orderid)->update(['locationid'=>$request->locationid,'previous_locationid'=>$request->prevlocation]);
	DB::table('orderbasketlist')->where('orderid',$request->orderid)->update(['locationid'=>$request->locationid,'previous_locationid'=>$request->prevlocation]);
	$locationname = Locations :: where('locationid','=',$request->locationid)->get();
		//$user_details = User::findOrFail($user->userid);
		$orderData = Orderdetails::where('orderid',$request->orderid)->get();
		//print_r($orderData);exit;
		$userid=$orderData[0]->enduserid;
		$user_details = User :: where('userid', $orderData[0]->enduserid)->get();
		//print_r($user_details);exit;
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$locationname = Locations :: where('locationid','=',$request->locationid)->get();
		$basketdetails = Basket :: where('orderid' , $request->orderid)->get();
		$invoicedetails = Invoice :: where('orderid' , $request->orderid)->get();
		$orderbasketlistdetails = Orderbasketlist :: where('orderid' , $request->orderid)->get();
		$getuniqueid = Orderdetails :: where('orderid' , $request->orderid)->get();
		$orderinvoicelistdetails = Orderinvoicelist :: where('orderid' , $request->orderid)->get();
		$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$orderData[0]->deliveryaddressid)->get();
		$orderid=$request->orderid;
		$deliveryfee=$orderData[0]->deliveryfee;
		//Pharmacy Email
		
			$orderday = $orderData[0]->userpreffereddate;
			$orderdate = date('d F Y', strtotime($orderData[0]->userpreffereddate));
			$placeddate = date('d F Y', strtotime($getuniqueid[0]['orderDate']));
			// $link = "https://pharmacy.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			// $subject="Medmate – New Order Request #".$getuniqueid[0]['uniqueorderid'];
			if(env('ENV') == "TEST")			
			$link = "https://test-app.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			else if(env('ENV') == "DEV")
			$link = "https://dev-app.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			else
			$link = "https://pharmacy.medmate.com.au/pharmacy/viewinboxorder/".$orderid;
			if($locationname[0]->locationstatus==1){			
			$subject=env('ENV')." Medmate – New Order Request #".$getuniqueid[0]['uniqueorderid']; 
			}else{
				$subject = env('ENV')." Medmate – New Order Request #".$getuniqueid[0]['uniqueorderid']; 
			}
			$uniqueorderid = $getuniqueid[0]['uniqueorderid'];
			$prescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->get();
											  
					$nqty=0;$nprice=0;	
					if($prescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$prescriptionitems->count();$np++)
						{
							$nqty = $nqty+$prescriptionitems[$np]['drugpack'];
							$priceval = $prescriptionitems[$np]['drugpack']*$prescriptionitems[$np]['originalprice'];
							$nprice = $priceval+$nprice;
						}
					}
			$tbc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->where('orderbasketlist.originalprice' , '=' , '0.00')
                                             ->get();
			$tbccount = $tbc->count();
			$tbctext='';
			if($tbccount > 0)
			{
				$tbctext=  "(".$tbccount ."scripts with pricing to be confirmed by pharmacy)"; 
			}
			else
				$tbctext='';
			$nonprescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 0)
                                              ->get();
					$qty=0; $npriceval=0;
					if($nonprescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$nonprescriptionitems->count();$np++)
						{
							$qty = $qty+$nonprescriptionitems[$np]['drugpack'];
							$npval=$nonprescriptionitems[$np]['drugpack']*$nonprescriptionitems[$np]['originalprice'];
							$npriceval = $npriceval+$npval;
						}
					} 
			
				$subtotal = $npriceval+$nprice;
			$gst=0;
			$gstcalc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->get();
			for($i=0;$i<$gstcalc->count();$i++)
			{
				$gstcalcval = $this->compute_price($request->locationid,$gstcalc[$i]['prescriptionid'],$user_details[0]->userid);
				$gst = $gst+$gstcalcval;
			}
			$deliverydate = $orderdate = date('d F Y', strtotime($orderData[0]['userpreffereddate']));
			$body_message = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order to pharmacy</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {       
                color: #000000 !important;         
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">You have a new quote request.</h1>  
                                                <p>Hello '.$locationname[0]['locationname'].',<br>
                                                A new quote has been requested by a customer. <br>
                                                Use the below link to review the order.<p>
                                                <h3>Order Details</h3>
                                                <p>'.$orderData[0]['ordertype'].'<br>
                                                '.$orderData[0]->userpreffereddate.', '.$orderData[0]->userprefferedtime.'</p>

                                                <center>
                                                <a href="'.$link.'" target="_blank">
                                                    <img src="https://pharmacy.medmate.com.au/images/emailtemp/view-order-btn.png" alt="View Order" />
                                                </a>
                                                </center>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="mailto:customerservice@medmate.com.au">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
			';
			$message['to']           = $locationname[0]['email'];
               $message['subject']      = $subject;
               $message['body_message'] = $body_message;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('orders@medmate.com.au', 'Medmate')
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
//customer mail	
$body_message1='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$getuniqueid[0]['uniqueorderid'].'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Order Update</h1>  
                                                <p>Hi '.$user_details[0]['firstname'].',<br>
                                                Thank you for your recent purchase. The pharmacy you had chosen was unable to fulfil the order. Your order has been redirectied to '.$locationname[0]['locationname'].'</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Your Order Details</h3>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetails"> 
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td class="orderDetailsTop">
                                                            Order: '.$getuniqueid[0]['uniqueorderid'].'<br>
                                                            Placed on '.$placeddate.'
                                                        </td>        
                                                        <td width="20" style="width:20px;">&nbsp;</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetailsPurchases"> 
                                                                <thead>
                                                                    <tr>
                                                                        <th>Items</th>
                                                                        <th>Qty</th>
                                                                        <th>Price</th>
                                                                    </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Prescription Items<br>
                                                                            '.$tbctext.'
                                                                        </td>
                                                                        <td>'.$nqty.'</td>
                                                                        <td>'.$nprice.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Non-Prescription Items</td>
                                                                        <td>'.$qty.'</td>
                                                                        <td>'.$npriceval.'</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                        <td height="40" style="height:40px;">&nbsp;</td>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Subtotal</td>
                                                        <td>'.$subtotal.'</td>
                                                    </tr>';
                                                    if($getuniqueid[0]['ordertype'] == "Delivery")
                                                    {
                                                    $body_message1 .=   '<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Delivery&nbsp;Fee</td>
                                                        <td>'.$deliveryfee.'</td>
                                                    </tr>';
                                                    }
                                                    $body_message1 .='<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Order&nbsp;Total</td> 
                                                        <td>'.$orderData[0]->orderTotal.'</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>GST</td>
                                                        <td>'.$gst.'</td>
                                                    </tr> 
                                                </table>


                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4>Your Details:</h4>
                                                            '.$user_details[0]['firstname'].' '.$user_details[0]['lastname'].' <br> '.$user_details[0]['mobile'].' <br>   
                                                              ';
                                                              if($deliveryaddress->count() > 0)
                                                              {
                                                                $body_message1 .=' '.$deliveryaddress[0]['addressline1'].' '.$deliveryaddress[0]['addressline2'].' <br>'.$deliveryaddress[0]['suburb'].', '.$deliveryaddress[0]['state'].', '.$deliveryaddress[0]['postcode'].' <br> Australia
                                                              ';
                                                              }
                                                            $body_message1 .='
                                                        </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$locationname[0]['locationname'].' <br>'.$locationname[0]['phone'].' <br> '.$locationname[0]['address'].' <br> '.$locationname[0]['suburb'].', '.$locationname[0]['suburb'].', '.$locationname[0]['state'].' <br> Australia
                                                        </td>                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="mailto:customerservice@medmate.com.au">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>';
$subject1=env('ENV')."Medmate-OrderUpdate(".$orderData[0]->uniqueorderid.")";
$message['to']           = $user_details[0]['username'];
               $message['subject']      = $subject1;
               $message['body_message'] = $body_message1;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('admin@medmaate.com.au','Medmate')
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   //Previous Pharmacy mail
$body_message2='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order to pharmacy</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {       
                color: #000000 !important;         
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">An order has been re-directed</h1>  
                                                <p>Hello '.$prevlocdetails[0]['locationname'].',<br>
                                                An order that had been sent to you previously has been redirected to another pharmacy.<p>
                                                

                                                
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="mailto:customerservice@medmate.com.au">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2021 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>';
$subject2=env('ENV')."Order Re-directed";
$message['to']           = $prevlocdetails[0]['email'];
               $message['subject']      = $subject2;
               $message['body_message'] = $body_message2;
               // echo ($message['body_message']); die;
               try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
				  ->bcc('admin@medmaate.com.au','Medmate')
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   return "success";

	}
	
	public function viewglobalsearchorders(Request $request,$orderid=null)
	{
	
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.locationid','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								 // ->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
								  $order=$orderData[0];
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									//->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									//->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	
	public function viewinstoreorders(Request $request,$orderid=null)
	{
	
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.locationid','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								 // ->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
								  $order=$orderData[0];
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									//->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									//->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	
	public function viewinboxorder(Request $request,$orderid=null)
	{
		$orderstatus="Waiting For Review";
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$orderid)->update(['orderdetails.isnew' => 0]);	
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , $orderstatus)
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
								  $order=$orderData[0];
		//print_r($orderid);
		//print_r(count($orderData));exit;
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Waiting For Review")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Waiting For Review")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$storelist = Orderbasketlist :: join ('basket','basket.orderid', '=' , 'orderbasketlist.orderid')
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')
									->distinct()
									->select('orderbasketlist.*')						
									// ->where('basket.refferencetable' , '=' , 'MIMS')							
									 ->where('basket.refferencetable' , '=' , 'Catalouge')
								  ->where('ordertracking.orderstatus' , '=' , "Waiting For Review")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.enduserid' , '=' , $orderData[0]['myuserid'])
								  ->where('orderbasketlist.orderid' , '=' , $orderid)
								  ->where('orderbasketlist.basketliststatus' , '=' , 1)
								 ->get();
		
		$originalstorelist = Orderbasketlist :: leftJoin ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')
										->leftJoin ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
										->distinct()
										->select('orderbasketlist.*')						
										// ->where('basket.refferencetable' , '=' , 'MIMS')							
										 ->where('basket.refferencetable' , '=' , 'Catalouge')
										->where('ordertracking.orderstatus' , '=' , "Waiting For Review")
										->where('ordertracking.trackingstatus' , '=' , 1)
										->where('basket.basketstatus' , '=' , 1)
										->where('orderbasketlist.orderid' , '=' , $orderid)
										->where('orderbasketlist.basketliststatus' , '=' , 0)										
										->get();
		$locationinfo = Locations :: where('locationid', '=',$orderData[0]['locationid'])->get();
		return view('editorder', compact('order','useraddress','healthprofile','scriptslist','storelist','originalscriptslist','originalstorelist','locationinfo'));
	}
	
	public function viewauthenticatoinorder(Request $request,$orderid=null)
	{
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.locationid','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
		$order=$orderData[0];
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									//->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Awaiting Authorisation")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	
	public function viewprocessorder(Request $request,$orderid=null)
	{
		$orderdetails = Ordertracking :: where('ordertracking.orderid','=',$orderid)
										->where('ordertracking.trackingstatus','=',1)
										->where('ordertracking.orderstatus','=',"Process")
										->update(['ordertracking.isnew' => 0]);	
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','orderdetails.deliverypartnername as deliverypartner','ordertracking.statuscreatedat as statustime','locations.locationname','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "Process")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
								  $order=$orderData[0];
		
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.*','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Process")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									//->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.*','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Process")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									//->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
								 
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	public function viewdeliveryorder(Request $request,$orderid=null)
	{
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "Awaiting Delivery")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
								  $order=$orderData[0];
		
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Awaiting Delivery")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Awaiting Delivery")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	
	public function viewpickuporder(Request $request,$orderid=null)
	{
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "Awaiting Pickup")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
		$order=$orderData[0];
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Awaiting Pickup")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Awaiting Pickup")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	public function viewtransitorder(Request $request,$orderid=null)
	{
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "In Transit")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
		$order=$orderData[0];
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "In Transit")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "In Transit")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	public function viewcompletedorder(Request $request,$orderid=null)
	{
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "Completed")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
		$order=$orderData[0];
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
											->get();
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Completed")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Completed")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	
	public function viewcanelledorder(Request $request,$orderid=null)
	{
		$orderdetails = Ordertracking :: where('ordertracking.orderid','=',$orderid)
										->where('ordertracking.trackingstatus','=',1)
										->where('ordertracking.orderstatus','=','Cancelled')
										->update(['ordertracking.isnew' => 0]);
										
		$orderData = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								// ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')		
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 //->join ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 	
								  ->select('users.userid as myuserid','users.firstname as fname','users.lastname as lname','users.username as emailaddress','users.sex as gender','users.mobile as mobilenumber','users.dob as dateofbirth',
								  'orderdetails.*','ordertracking.orderstatus as myorderstatus','ordertracking.statuscreatedat as statustime','locations.locationname','locations.address','locations.postcode','locations.logo','locations.phone',
								  'deliveryaddresses.addressline1 as addr1','deliveryaddresses.mobile as deliverymobile','deliveryaddresses.addressline2 as addr2','deliveryaddresses.suburb as addsuburb','deliveryaddresses.state as addstate','deliveryaddresses.postcode as addpostcode')
								  ->where('ordertracking.orderid' , '=' , $orderid)
								  ->where('ordertracking.orderstatus' , '=' , "Cancelled")
								  ->where('ordertracking.trackingstatus' , '=' , 1)
								  ->where('orderdetails.orderid' , '=' , $orderid)
								  ->get();
		$order=$orderData[0];
        //echo   $orderData[0]['myuserid'];
		$useraddress = Deliveryaddress :: where('userid', '=' ,$orderData[0]['myuserid'])
										->where('addresstype' , '=' , 'profileaddress')
										->get();
		
		  $healthprofile =  Healthprofile :: where('userid', '=' ,$orderData[0]['myuserid'])
											->get();
		
		$scriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
									->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Cancelled")
									->where('orderbasketlist.basketliststatus' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		$originalscriptslist = Orderbasketlist :: join ('orderdetails','orderdetails.orderid', '=' , 'orderbasketlist.orderid')	
									->join ('ordertracking','ordertracking.orderid', '=' , 'orderbasketlist.orderid')
									->join ('basket','basket.scriptid', '=' , 'orderbasketlist.prescriptionid')									
									->leftJoin ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')									 
								   ->leftJoin ('prescriptionitemdetails','prescriptionitemdetails.prescriptionid', '=' , 'orderbasketlist.prescriptionid')
									->distinct()
									->select('prescriptiondetails.*','prescriptionitemdetails.prescriptionissuer','orderbasketlist.*','orderdetails.type')
									//->where('basket.refferencetable' , '!=' , 'Catalouge')
									->where('ordertracking.orderid' , '=' , $orderid)
									->where('basket.orderid' , '=' , $orderid)
									->where('ordertracking.orderstatus' , '=' , "Cancelled")
									->where('orderbasketlist.basketliststatus' , '=' , 0)
									->where('orderbasketlist.isscript' , '=' , 1)
									->where('ordertracking.trackingstatus' , '=' , 1)
									->where('orderdetails.orderid' , '=' , $orderid)
									->get();
		
		return view('vieworder', compact('order','useraddress','healthprofile','scriptslist','originalscriptslist'));
	}
	
	public function changestatus($status,$orderid,$userid)
	{
		
		if($status == 'Delivery'){
			$newstatus = "Awaiting Delivery";
		$redirect=redirect()->route('deliverylist')->with('success','Order processed. Awaiting delivery');
		}else if($status == 'Pickup' || $status == 'Pick Up' || strtolower($status) == 'pickup'){
			$newstatus = "Awaiting Pickup";
		$redirect=redirect()->route('pickuplist')->with('success','Order processed. Awaiting pick up');
		}else if($status == 'Widget Order' || $status == 'Pick Up' || strtolower($status) == 'pickup'){
			$newstatus = "Awaiting Pickup";
		$redirect=redirect()->route('pickuplist')->with('success','Order processed. Awaiting pick up');
		}else if($status == 'Completed'){
			$newstatus = "Completed";
	$redirect=redirect()->route('completedorders')->with('success','Order Completed Successfully!');
		}else if($status == 'In Transit'){
			$newstatus = "In Transit";
		$redirect=redirect()->route('transitlist')->with('success','Order Out for Delivery!');
		}else if($status == 'Awaiting Authorisation'){
			$newstatus = "Awaiting Authorisation";
		$redirect=redirect()->route('awaitingauthorisation')->with('success','Order successfully sent to customer');}
		
		/*$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$orderid)
										->where('orderdetails.enduserid','=',$userid)
										->update(['orderdetails.orderstatus' => $newstatus]);*/
							DB::table('orderdetails')
										->where('orderid','=',$orderid)
										->where('enduserid','=',$userid)
										->update(['orderstatus' => $newstatus]);
		//$orderdetails=DB::select("update orderdetails set orderstatus='".$newstatus."' where orderid='".$orderid."' and enduserid='".$userid."'");										
		$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$orderid)->where('ordertracking.trackingstatus','=',1)->get();
		$noofrows = $ordertrack->count();
		for($i=0;$i<$noofrows;$i++)	
		{
			$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$orderid)
											  ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
											->update(['ordertracking.trackingstatus' => '0']);
											
		}	date_default_timezone_set('UTC');							
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $orderid;
			$orderTracking->orderstatus = $newstatus;
			$orderTracking->orderremarks = '';
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	
			$this->send_notification($newstatus,$orderid,$userid);
			$orderinfo = Orderdetails :: where('orderdetails.orderid','=',$orderid)->get();
		
		if($newstatus == "Completed")
		{
			//Medlist Status Change After Completed
		
			$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
										->where('orderbasketlist.userid','=',$userid)->get();
			
			$noofItems = $orderBasket->count();
			for($b=0;$b<$noofItems;$b++)
			{
										
				if($orderBasket[$b]['prescriptionid'] !="")
				{
				
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
																  ->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
																  ->where('prescriptionitemdetails.isscript', '=', 1)															  
																  ->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				//To Check if uploaded script is related Medlist Medication script
				$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
															->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
															->get();
				$count = $getmedlistid->count();
				if($count > 0)
				{
					if($count == 1)
					$medlistdetails = $getmedlistid[0]['medilistid'];
					$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
													->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				}
				}
				else
				{
					
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $orderBasket[$b]['medlistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
													->where('prescriptionitemdetails.isscript', '=', 0)
													->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				}
			}
		}
		if($newstatus == "Awaiting Authorisation")
		{
			$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
			$this->sendawaitingemailtocustomer($orderdetails[0]['locationid'],$orderdetails[0]['deliveryaddressid'],$userid,$orderid);
		}
		if($newstatus == "In Transit")
		{
			$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
			$this->sendintransitemailtocustomer($orderdetails[0]['locationid'],$orderdetails[0]['deliveryaddressid'],$userid,$orderid);
		}
		if($newstatus == "Awaiting Pickup")
		{
			$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
			$this->sendpickupemailtocustomer($orderdetails[0]['locationid'],$orderdetails[0]['deliveryaddressid'],$userid,$orderid);
		}
		
		return $redirect;
		//return view('getallorders');
	}
	public function send_notification($status,$orderid,$userid){
		$this->common = new CommonController();
		$order_details=DB::select("select * from orderdetails where orderid=".$orderid);
		$user_details=DB::select("select * from users where userid=".$userid);
		$message_title= "Order id ".$order_details[0]->uniqueorderid." Status Updated ";
		$trakingurl = $order_details[0]->delivery_tracking;
		if($status == 'Awaiting Delivery')
			$message_body="Your order is Processed";
		else if($status == 'Awaiting Pickup')
			$message_body="Your order is processed and ready for pickup";
		else if($status == 'Completed')
			$message_body="Your order is Completed";
		else if($status == 'In Transit')		
		{			
			if($order_details[0]->delivery_tracking != "" && $order_details[0]->deliverypartnername == "DoorDash" )
			{
				$message_body="Order ID: ".$order_details[0]->uniqueorderid.". Your order is on its way. Click <a href='".$order_details[0]->delivery_tracking."'> here </a> to track.";
				$trakingurl = $order_details[0]->delivery_tracking;
			}
			else
			{
				$message_body="Your order is out for delivery!";
			}
		}
		else if($status == 'Awaiting Authorisation')
			$message_body="Your order is approved proceed to payment";
		
		else if($status=='Cancelled')
			$message_body="Your order,has been cancelled by the pharmacy,for more details,contact the pharmacy";
		$data= (object) array(
						'body'      => "1",
						'title' => "2",
                         'orderid'        => $orderid,
						 'uniqueorderid'=> $order_details[0]->uniqueorderid,
						 'status' =>$status
						 );
						  $notification=(object)array("body"=>$message_body,"title"=>$message_title);
						
                   $fields = array('to' => $user_details[0]->devicetoken,'data' => $data, 'notification' => $notification);
				if(isset($user_details[0]->devicetoken)){		
				 $re=$this->common->sendPushNotification($fields);
				}
				$notificationmessage = $message_title.'<br/>'.$message_body;
		$Notifications = new Notifications;
		$Notifications->notificationtype = 1; //1.APP Notifications 2.Promotional notifications		
		$Notifications->notificationmessage = $notificationmessage;
		$Notifications->userid = $userid;
		$Notifications->orderid = $orderid;
		$Notifications->trackingurl = $trakingurl;
		$Notifications->status = 1;
		$Notifications->isopen = 1;
		$Notifications->save();
		
		return true;
	}
	//DoorDash API
	public function calldoordashdelivery(Request $request)
	{
		$orderinfo = Orderdetails :: where('orderdetails.orderid','=',$request->orderid)->get(); 
		$newstatus = "Awaiting Delivery";
		//Test DD API KEY
		//"Authorization: Bearer 22590509529bb2aa58e00387ef617843179e8eee",
		// if($newstatus == "Awaiting Delivery" && $orderinfo[0]['deliverypartnername'] == "DoorDash")
		// {
			$pickupaddress = Locations :: where('locationid',$orderinfo[0]['locationid'])->get();
					$pickupcity = 	$pickupaddress[0]['suburb'];	
					$pickupstate =  $pickupaddress[0]['state'];
					$pickupstreet =  $pickupaddress[0]['streetaddress'];
					$pickuppostcode =  $pickupaddress[0]['postcode'];
					$pickuplat =  $pickupaddress[0]['latitude'];
					$pickuplong =  $pickupaddress[0]['longitude'];
					//$mobile =  '+61'.$pickupaddress[0]['phone'];
					$mobile = '+61'.substr(str_replace(" ","",$pickupaddress[0]['phone']),1);
					$deleiveryaddress = Deliveryaddress :: where('deliveryaddressid',  $orderinfo[0]['deliveryaddressid'])->get();
					$deleiverycity = $deleiveryaddress[0]['suburb'];
					$deleiverystate = $deleiveryaddress[0]['state'];
					$deleiverystreet = $deleiveryaddress[0]['addressline1'];
					$deleiverypostcode = $deleiveryaddress[0]['postcode'];
					
					$orderamount = str_replace(".","",$orderinfo[0]['orderTotal']);
					$healthprofile = Healthprofile::where('userid', $orderinfo[0]['enduserid'])->get();
					$user = User :: where('userid', $orderinfo[0]['enduserid'])->get();
					$usermobile = '+61'.substr($user[0]['mobile'],1);
					if($user[0]['lastname']!="")
					{
					$lastname = $user[0]['lastname'];
					}
					else
					$lastname = $user[0]['firstname'];	
					
					//date_default_timezone_set($pickupaddress[0]['timezone']);
					$todaydate = gmdate("Y-m-d\TH:i:s\Z");
					$pickup_time = $todaydate;
					
					$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$request->orderid)
										->where('orderbasketlist.userid','=',$orderinfo[0]['enduserid'])
										->where('orderbasketlist.basketliststatus','=',1)->get();
			
					$noofItems = $orderBasket->count();
					
					if(strpos($orderinfo[0]['deliveryinstructions'], 'Cannot be left unattended') !== false)
					{
						$scheduledVal = "false";
						$signrequired = "false";
						$containalcohol = "false";
					}
					else if(strpos($orderinfo[0]['deliveryinstructions'], 'Attempt to hand deliver') !== false)
					{
						$scheduledVal = "true";
						$signrequired = "true";
						$containalcohol = "true";
					}
					
			$curl = curl_init();
			$vars='{
			  "pickup_address": {
				"city": "'.$pickupcity.'",
				"state": "'.$pickupstate.'",
				"street": "'.$pickupstreet.'",
				"unit": "",
				"zip_code": "'.$pickuppostcode.'",
				"location": {
				  "lng": '.$pickuplong.',
				  "lat": '.$pickuplat.'
				}
			  },
			  "pickup_phone_number": "'.$mobile.'",
			  "dropoff_address": {
				"city": "'.$deleiverycity.'",
			"state": "'.$deleiverystate.'",
			"street": "'.$deleiverystreet.'",
			"unit": "",
			"zip_code": "'.$deleiverypostcode.'"
			},
			  "customer": {
				"phone_number": "'.$usermobile.'",
				"business_name": "NONE",
				"first_name": "'.$user[0]['firstname'].'",
				"last_name": "'.$lastname.'",
				"email": "'.$user[0]['username'].'",
				"should_send_notifications": false
			  },
			  "order_value": '.$orderamount.',
			  "pickup_time": "'.$pickup_time.'",
			 
			  "team_lift_required": false,
			  "barcode_scanning_required": false,
			  "pickup_business_name": "'.$pickupaddress[0]['locationname'].'",
			  "pickup_instructions": "'.$pickupaddress[0]['pickupinstruction'].'",
			  "dropoff_instructions": "'.$orderinfo[0]['deliveryinstructions'].'",
			  "order_volume": 0,
			  "tip": 0,
			  "external_delivery_id": "'.$orderinfo[0]['uniqueorderid'].'",
			  "driver_reference_tag": "'.$orderinfo[0]['uniqueorderid'].'",
			  "external_business_name": "Medmate",
			  "external_store_id": "'.$pickupaddress[0]['locationid'].'",
			  "contains_alcohol": '.$containalcohol.',
			  "requires_catering_setup": false,
			  "num_items": '.$noofItems.',
			  "signature_required": '.$signrequired.',
			  "allow_unattended_delivery": '.$scheduledVal.',
			  "delivery_metadata": {
				"foo": "bar"
			  },
			  "allowed_vehicles": [
				"car",
				"bicycle"
			  ],
			  "is_contactless_delivery": false
			}';
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => env('DOORDASH_DELIVERIES_URL'),
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>$vars,
			  CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".env('DOORDASH_KEY'),
				"Content-Type: application/json",   
			  ),
			));

			$res = curl_exec($curl);
			//print_r($vars);
			curl_close($curl);			
			$resval = json_decode($res);
			//print_r($resval);exit;
			//$input=array();
			$Doordashlogs=new Doordashlogs;
			$Doordashlogs->doordashapiurl=env('DOORDASH_DELIVERIES_URL');
			$Doordashlogs->urltype="Deliveries";
			$Doordashlogs->uniqueorderid=$orderinfo[0]['uniqueorderid'];
			$Doordashlogs->orderid=$request->orderid;
			$Doordashlogs->pickupcity=$pickupcity;
			$Doordashlogs->pickupstate=$pickupstate;
			$Doordashlogs->pickupstreet=$pickupstreet;
			$Doordashlogs->pickupunit="";
			$Doordashlogs->pickupzip_code=$pickuppostcode;
			$Doordashlogs->pickup_phone_number=$mobile;
			$Doordashlogs->dropoffcity=$deleiverycity;
			$Doordashlogs->dropoffstate=$deleiverystate;
			$Doordashlogs->dropoffstreet=$deleiverystreet;
			$Doordashlogs->dropoffunit="";
			$Doordashlogs->dropoffzip_code=$deleiverypostcode;
			$Doordashlogs->customerphone_number=$usermobile;
			$Doordashlogs->customerbusiness_name="";
			$Doordashlogs->customerfirst_name=$user[0]['firstname'];
			$Doordashlogs->customerlast_name=$lastname;
			$Doordashlogs->customeremail=$user[0]['username'];
			$Doordashlogs->send_notifications="false";
			$Doordashlogs->order_value=$orderamount;
			$Doordashlogs->pickup_time=$pickup_time;
			$Doordashlogs->pickup_business_name=$pickupaddress[0]['locationname'];
			$Doordashlogs->pickup_instructions=$pickupaddress[0]['pickupinstruction'];
			$Doordashlogs->dropoff_instructions=$orderinfo[0]['deliveryinstructions'];
			$Doordashlogs->external_delivery_id=$orderinfo[0]['uniqueorderid'];
			$Doordashlogs->driver_reference_tag=$orderinfo[0]['uniqueorderid'];
			$Doordashlogs->external_store_id=$pickupaddress[0]['locationid'];
			$Doordashlogs->contains_alcohol=$containalcohol;
			$Doordashlogs->num_items=$noofItems;
			$Doordashlogs->signature_required=$signrequired;
			$Doordashlogs->allow_unattended_delivery=$scheduledVal;	
			$Doordashlogs->jsonrequest=$vars;	
			//$Doordashlogs->jsonresponse=$resval;
			$Doordashlogs->save();
			if(isset($resval->id) && isset($resval->delivery_tracking_url) && $resval->id != "" && $resval->delivery_tracking_url != "" )
			{
				date_default_timezone_set('UTC');
				$orderdetailsinfo = Orderdetails ::  where('orderdetails.orderid', '=', $request->orderid)
								->update(['orderdetails.partner_deliveryid' => $resval->id , 'orderdetails.delivery_tracking' => $resval->delivery_tracking_url]);
				$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)->where('ordertracking.trackingstatus','=',1)->get();
				$noofrows = $ordertrack->count();
				for($i=0;$i<$noofrows;$i++)	
				{
					$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$request->orderid)
													  ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
													->update(['ordertracking.trackingstatus' => '0']);
													
				}								
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $request->orderid;
			$orderTracking->orderstatus = $newstatus;
			$orderTracking->orderremarks = '';
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	
			//$this->send_notification($newstatus,$request->orderid,$userid);
			
				$this->send_notification($newstatus,$request->orderid,$orderinfo[0]['enduserid']);	
				$this->senddoordashurltocustomer($orderinfo[0]['enduserid'],$request->orderid);
				return "Your order has been successfully allocated to a driver.";
			}
			elseif(isset($resval->field_errors) && $resval->field_errors != "")
			{
				
				return "Your order has NOT been allocated because ".$resval->field_errors[0]->error;
			}
			

		//}
	}
	public function updateeditorder(Request $request)
	{
		//print_r($request);exit;
		$orderid = $request->orderVal;
		$userid = $request->userinfo;
		
		$healthprofile =  Healthprofile :: where('userid', '=' ,$userid)->get();		
		$profileid = $healthprofile[0]['profileid'];
		
		$orderdetails = Orderdetails :: where('orderid' , '=', $orderid)->get();
		$locationid = $orderdetails[0]['locationid'];
		$deliveryaddressid = $orderdetails[0]['deliveryaddressid'];
		$delivery_fee=$orderdetails[0]['deliveryfee'];
		$basketVal = Basket :: where('orderid', '=', $orderid)->get();
		
		$basketstatus = Basket :: where('basket.orderid', '=', $orderid)
								->update(['basket.basketstatus' => '0']);
								
		$numvalBasket = $basketVal->count();
		for($b=0;$b<$numvalBasket;$b++)
		{			
		$basketitemliststatus = Basketitemlist :: where('basketitemlist.basketid', '=', $basketVal[$b]['basketid'])
												->update(['basketitemlist.itemstatus' => '0']);
		
		}
		$orderbasket = Orderbasketlist :: where('orderid', '=', $orderid)->where('basketliststatus','=',1)->get();
		for($r=0;$r<$orderbasket->count();$r++)
		{
			$orderbasketlist = Orderbasketlist :: where('orderbasketlist.orderbasketid', '=',$orderbasket[$r]['orderbasketid'])
												-> where('orderbasketlist.orderid', '=', $orderid)
												->update(['orderbasketlist.basketliststatus' => '0']);
		}
		$invoice = Invoice ::  where('invoice.orderid', '=', $orderid)
								->update(['invoice.invoicestatus' => '0']);
		
		$orderinvoice = Orderinvoicelist ::  where('orderinvoicelist.orderid', '=', $orderid)
								->update(['orderinvoicelist.orderinvoicestatus' => '0']);
		
		
		
		
		$scriptcart = $request->scriptdata;
		if(!empty($scriptcart))
		{
		for($i=0;$i<count($scriptcart);$i++){
		//print_r($orderid);exit;
			if($orderid!="")
			{					
				//print_r($cart);exit;
			$basket = new Basket;
			$basket->profileid = $profileid;
			$basket->orderid = $orderid;
			$basket->createddate = date('Y-m-d');;
			$basket->scriptid  = $scriptcart[$i]['itemid'];
			$basket->refferencetable  = '';			
			$basket->basketstatus  = 1;
			$basket->save();
			//print_r($basket);exit;
			$basketid = $basket->id;
			
			$basketitemlist = new Basketitemlist;
			$basketitemlist->basketid = $basketid;
			$basketitemlist->itemstatus = '1';
			$basketitemlist->save();			
			
			$invoice = new Invoice;
			$invoice->orderid = $orderid;
			$invoice->userid = $userid;
			$invoice->profileid = $profileid;			
			$invoice->invoicestatus = "1";			
			$invoice->save();				
			$invoiceid = $invoice->id;
												 
			$orderbasketlist = new Orderbasketlist;
			$orderbasketlist->orderid = $orderid;
			$orderbasketlist->userid = $userid; 
			$orderbasketlist->profileid = $profileid;
			$orderbasketlist->basketid  = $basketid;
			$orderbasketlist->locationid  = $locationid;
			$orderbasketlist->deliveryaddressid  = $deliveryaddressid;
			$orderbasketlist->prescriptionid   = $scriptcart[$i]['itemid'];
			$orderbasketlist->drugname   = $scriptcart[$i]['itemdesc'];
			//$orderbasketlist->drugquantity   = $scriptcart[$i]['quantity'];
			$orderbasketlist->drugpack   = $scriptcart[$i]['drugpack'];
			$orderbasketlist->isscript   = $scriptcart[$i]['isscript'];
			$orderbasketlist->itemimage   = $scriptcart[$i]['itemimage'];
			$orderbasketlist->basketliststatus   = 1;
			if($scriptcart[$i]['price'] == 'NA')
			{
			$orderbasketlist->originalprice   = '0.00';
			}
			else
			{
			$orderbasketlist->originalprice   = $scriptcart[$i]['originalprice'];
			}
			$orderbasketlist->save();
				
			$orderinvoicelist = new Orderinvoicelist;
			$orderinvoicelist->invoiceid = $invoiceid;
			$orderinvoicelist->orderid  = $orderid;
			$orderinvoicelist->orderinvoicestatus = "1";
			$orderinvoicelist->save();
		}
		}
		}
		$storecart = $request->storedata;
		if(!empty($storecart))
		{
		for($i=0;$i<count($storecart);$i++){
		//print_r($orderid);exit;
			if($orderid!="")
			{
				
					
				//print_r($cart);exit;
			$basket = new Basket;
			$basket->profileid = $profileid;
			$basket->orderid = $orderid;
			$basket->createddate = date('Y-m-d');;
			$basket->scriptid  = $storecart[$i]['itemid'];
			$basket->refferencetable  = 'Catalouge';			
			$basket->basketstatus  = 1;
			$basket->save();
			//print_r($basket);exit;
			$basketid = $basket->id;
			
			$basketitemlist = new Basketitemlist;
			$basketitemlist->basketid = $basketid;
			$basketitemlist->itemstatus = '1';
			$basketitemlist->save();			
			
			$invoice = new Invoice;
			$invoice->orderid = $orderid;
			$invoice->userid = $userid;
			$invoice->profileid = $profileid;			
			$invoice->invoicestatus = "1";			
			$invoice->save();				
			$invoiceid = $invoice->id;
												 
			$orderbasketlist = new Orderbasketlist;
			$orderbasketlist->orderid = $orderid;
			$orderbasketlist->userid = $userid; 
			$orderbasketlist->profileid = $profileid;
			$orderbasketlist->basketid  = $basketid;
			$orderbasketlist->locationid  = $locationid;
			$orderbasketlist->deliveryaddressid  = $deliveryaddressid;
			$orderbasketlist->prescriptionid   = $storecart[$i]['itemid'];
			$orderbasketlist->drugname   = $storecart[$i]['itemdesc'];
			$orderbasketlist->drugquantity   = $storecart[$i]['quantity'];
			$orderbasketlist->drugpack   = $storecart[$i]['drugpack'];
			$orderbasketlist->basketliststatus   = 1;
			if($storecart[$i]['price'] == 'NA')
			{
			$orderbasketlist->originalprice   = '0.00';
			}
			else
			{
			$orderbasketlist->originalprice   = $storecart[$i]['originalprice'];
			}
			$orderbasketlist->save();
				
			$orderinvoicelist = new Orderinvoicelist;
			$orderinvoicelist->invoiceid = $invoiceid;
			$orderinvoicelist->orderid  = $orderid;
			$orderinvoicelist->orderinvoicestatus = "1";
			$orderinvoicelist->save();
		}
			
		}			
		}	
		/*$orderval = orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
									  ->where('orderbasketlist.basketliststatus','=',1)
									  ->select('orderbasketlist.originalprice','orderbasketlist.drugquantity')
									  ->get();
								
		for($t=0;$t<$orderval->count();$t++)
		{
			$ordertotal =0;
			$eachprice[$t] = $orderval[$t]['drugquantity'] * $orderval[$t]['originalprice'];
			$ordertotal = $ordertotal+$eachprice[$t];
			
		}
		$ordertotal = $ordertotal;*/
		$ordertotal=$request->ordertotal;
		$deliveryfee = $request->deliveryfee;
		if($delivery_fee!=$deliveryfee){
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$orderid)
                                        ->where('orderdetails.enduserid','=',$userid)
                                        ->update(['orderdetails.orderstatus' => 'Awaiting Authorisation','orderdetails.orderTotal' => $ordertotal,'orderdetails.deliveryfee' => $deliveryfee,'orderdetails.isdeliveryapply'=>0]);
        }else{
			$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$orderid)
                                        ->where('orderdetails.enduserid','=',$userid)
                                        ->update(['orderdetails.orderstatus' => 'Awaiting Authorisation','orderdetails.orderTotal' => $ordertotal,'orderdetails.deliveryfee' => $deliveryfee]);
		}
		$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$orderid)->where('ordertracking.trackingstatus','=',1)->get();
        $noofrows = $ordertrack->count();
        for($i=0;$i<$noofrows;$i++)
        {
            $orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$orderid)
                                              ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
                                            ->update(['ordertracking.trackingstatus' => '0']);

        }
            $orderTracking = new Ordertracking;
            $orderTracking->orderid = $orderid;
            $orderTracking->orderstatus = 'Awaiting Authorisation';
            $orderTracking->orderremarks = '';
            $orderTracking->statusCreatedat = date('Y-m-d H:i:s');
            $orderTracking->statusupdatedat = date('Y-m-d H:i:s');
            $orderTracking->save();
			$this->send_notification("Awaiting Authorisation",$orderid,$userid);
			$this->sendemailtocustomer($locationid,$deliveryaddressid,$userid,$orderid);
			return redirect()->route('awaitingauthorisation')->with('success','Order successfully sent to customer');
			
		// return view('awaitingauthorisation');
		
		
	}
	public function senddoordashurltocustomer($userid,$orderid)
	{
		$user = User :: where('userid', $userid)->get();
		
		$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
		$body_message=' 
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
			<title>Document</title>
		</head>
		<style>
			body  {
				font-family: "Nunito" !important;font-size: 18px !important;
			}
			.top {
				display: flex;flex-direction: row;width: 700px !important;justify-content: space-between !important;
			}
			.heading {
			   color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;
			}
			.total {
				width: 700px !important;  margin: auto !important;
			}
			.bottom {
				text-align: center;color :#7e7e7e !important;
			}
			.one {
				background-color: #1d9bd8;color: white;border: 0px solid;border-radius: 10px;height: 35px;margin-left: 300px;margin-top: 50px !important;
			}
			.a1 {
				color: #1d9bd8 !important;
			}
		</style>
		<body>
		<div class="total">
			<div class="top" style="width: 700px !important;">
				<p style="padding-top: 15px;width: 400px !important;"><img src="https://pharmacy.medmate.com.au/log.jpg" width="200px" height="50px"></p>
				<p class="heading" style="color: #1d9bd8 !important;font-size: xx-large !important;font-weight: 600 !important;width:300px !important;"><span>Tracking Url</span> <br> <span style="color: black;font-size:large">Order:'.$orderdetails[0]['uniqueorderid'].'</span></p>
			</div>
			<div>
			<p>
				Your order has been picked up and is on its way. <a href="'.$orderdetails[0]['delivery_tracking'].'">Click here</a> to track your delivery.
			

			</p>	
			</div>
			</div>
			<div class="bottom">
				<p>
					Medmate Australia Pty Ltd. ABN: 88 628 471 509 <br>
									medmate.com.au
				</p>
			</div>
		</div>
		</body>
		</html>
		';
				$message['to']           = $user[0]['username'];
               // $message['subject']      = "Complete Order";
                $message['subject']      = env('ENV')." Doordash Message";
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
	}
	public function sendemailtocustomer($locationid,$deliveryaddressid,$userid,$orderid)
	{
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$user = User :: where('userid', $userid)->get();
		$location = Locations :: where('locationid','=',$locationid)->get();
		$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
		$orderData=$orderdetails;
		$uniqueorderid=$orderdetails[0]->uniqueorderid;
		//$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$deliveryaddressid)->get();
		$deliveryaddress = Deliveryaddress :: where('addresstype','profileaddress')->where('userid',$userid)->get();
		$body_message1='';
		$body_message='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order processing</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Your order is being processed.</h1>  
                                                <p>Hello '.$orderData[0]['fname'].',<br>
                                                Your order has been confirmed and is currently being processed by the pharmacy. We’ll let you know when it is on its way. </p>
                                                <p>You can view the status of the order within the Medmate app. </p>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
		';
				$message['to']           = $user[0]['username'];
                //$message['subject']      = "Complete Order";
                $message['subject']      = env('ENV')." Complete Order";
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
	}
	
	public function ajaxGetItemsfromCatalogue(Request $request)
	{
		$business = Locations :: where('locationid', $request->location)->get();
		$healthprofile = Healthprofile::where('userid', $request->userid)->get();
		//$catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,PI.image from catalouge_master as C left join packitemimages as PI on C.MIMsProductCode=PI.prodcode where C.MimsFormCode=PI.formcode and C.MimsPackCode=PI.packcode and C.itemNameDisplay LIKE '%".$request->medication."%'"); 
		$catalouge_data = DB::select("select mastercatid,itemNameDisplay as drug,MedmateItemCode as drugcode from catalouge_master where itemNameDisplay LIKE '%".$request->medication."%'"); 
		return $catalouge_data;
		 $drug_details=array();
        for($i=0;$i<count($catalouge_data);$i++){
            $catalouge_data[$i]->reference="Catalouge";
			// $price_details = DB::table('catalouge_master as C')
						// ->select('CP.*','C.ShortDescription','C.GST')
						// ->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
						// ->where([
							// ['C.itemNameDisplay',$catalouge_data[$i]->drug],
							// ['CP.location_id',$request->location]
							// ])
                        // ->get();
           $price_details = DB::table('catalouge_master as C')
				->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST','C.ItemCommodityClass')
				->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
				->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
				->where([
					['C.itemNameDisplay',$catalouge_data[$i]->drug],
					
					])
				->orwhere([ ['CO.location_id',$request->location],
							['CO.businessid',$business[0]['businessid']],							
							['CO.businessid',0],       
							['CO.Pricing_Tier',1],
					])
				->orwhere([ ['CS.location_id',$request->location],
							['CS.businessid',$business[0]['businessid']],							                        
							['CS.businessid',0],
							['CS.Pricing_Tier',1],
					])
				->get();
				//echo "Madhoo";
			//print_r($price_details);exit;
//=======
         //  $price_details = DB::select("select  `CO`.*, `CS`.*, CS.medicare,CS.Con_Pens_ConSafty,CS.SafetyNet,CS.Veterans,`CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode` where (`CO`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$catalouge_data[$i]->drug."%' and `CO`.`Pricing_Tier` = 1) or (`CS`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$catalouge_data[$i]->drug."%' and `CS`.`Pricing_Tier` = 1 and `CS`.`status` = 1)");
				
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
            if($catalouge_data[$i]->script=='Prescription')
			{
				$catalouge_data[$i]->script="Yes";
				if(count($price_details)>0){ 
                        if(count($healthprofile)>0){
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                    $p=$price_details[0]->Con_Pens_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->veteranaffairsnumber!=null){
                                    $p=$price_details[0]->Veterans;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->ScriptStandardprice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                            }
                        }else{
                            $price="NA";
                        }
			}
			else
			{
				$catalouge_data[$i]->script="No";
				$p=$price_details[0]->OTCStandardprice;
				$gst =(($price_details[0]->GST)*$p)/100;
				$price =$p+ $gst;
			}
			$catalouge_data[$i]->price=round($price,2);
            $drug_details[]=$catalouge_data[$i];
            //$catalouge_data['price']=round($price,2);
        }
		
		return $drug_details;
		
	}
	public function ajaxGetItemsfromCataloguedata(Request $request)
	{
		$business = Locations :: where('locationid', $request->location)->get();
		$healthprofile = Healthprofile::where('userid', $request->userid)->get();
		$catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,(select image from packitemimages as PI where PI.prodcode=C.MIMsProductCode and PI.formcode=C.MimsFormCode and PI.packcode=C.MimsPackCode) as image from catalouge_master as C where C.mastercatid='".$request->medication."'"); 
		//return $catalouge_data;
		$drug_details=array();
        for($i=0;$i<count($catalouge_data);$i++){
			$catalouge_data[$i]->reference="Catalouge";
			if(!empty($catalouge_data[$i]->PhotoID)){
				$catalouge_data[$i]->itemimage = 'https://pharmacy.medmate.com.au/catalouge/'.$catalouge_data[$i]->PhotoID.'.jpg';}
				
				elseif(!empty($catalouge_data[$i]->image))
					$catalouge_data[$i]->itemimage = 'https://pharmacy.medmate.com.au/mimsimages/'.$catalouge_data[$i]->image.'.jpg';
				
				else
					$catalouge_data[$i]->itemimage = '';
				
			// $price_details = DB::table('catalouge_master as C')
//<<<<<<< HEAD
						// ->select('CP.*','C.ShortDescription','C.GST')
						// ->leftjoin('catalouge_pricing as CP', 'C.MedmateItemCode', '=', 'CP.medmateItemcode')
						// ->where([
							// ['CP.medmateItemcode',$catalouge_data[$i]->drugcode],
							// ['CP.location_id',$request->location]
							// ])
                        // ->get();
           //print_r($price_details);exit;
		   $price_details = DB::table('catalouge_master as C')
				->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST','C.ItemCommodityClass')
				->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
				->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
				->where([
					['C.itemNameDisplay',$catalouge_data[$i]->drugcode],
					])
				->orwhere([ ['CO.location_id',$request->location],
							['CO.businessid',$business[0]['businessid']],							
							['CO.businessid',0],                           
							['CO.Pricing_Tier',1],
					])
				->orwhere([
							['CS.location_id',$request->location],
							['CS.businessid',$business[0]['businessid']],
							['CS.businessid',0],
							['CS.Pricing_Tier',1],
					])
				->get();
//=======
						// ->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST')
						// ->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						// ->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
						// ->where([
							// ['CO.medmateItemcode',$catalouge_data[$i]->drugcode],
							// ['CO.businessid',0],
							// ['CO.Pricing_Tier',1]
							// ])
						// ->orwhere([
							// ['CS.medmateItemcode',$catalouge_data[$i]->drugcode],
							// ['CS.businessid',0],
							// ['CS.Pricing_Tier',1]
							// ])
                        // ->get();
         
		   //$price_details = DB::select("select  CO.*, CS.*,CO.StandardPrice as OTCStandardprice, CS.StandardPrice as ScriptStandardprice,  C.itemNameDisplay,C.ShortDescription, C.GST, C.ItemCommodityClass from catalouge_master as C left join catalogueotcpricing as CO on C.MedmateItemCode = CO.medmateItemcode left join cataloguescriptpricing as CS on C.MedmateItemCode = CS.medmateItemcode where (CO.businessid = 0 and CO.medmateItemcode = '".$catalouge_data[$i]->drugcode."' and CO.Pricing_Tier = 1 and CO.status = 1) or (CS.businessid = 0 and CS.medmateItemcode = '".$catalouge_data[$i]->drugcode."' and CS.Pricing_Tier = 1)");
				 
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
            if($catalouge_data[$i]->script=='Prescription')
			{
				$catalouge_data[$i]->script="1";
				$catalouge_data[$i]->isscript = $catalouge_data[$i]->script;
				$price_details = DB::table('catalouge_master as C')
						->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST')
						->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
						
						->where([
							['CS.medmateItemcode',$catalouge_data[$i]->drugcode],
							['CS.businessid',0],
							['CS.Pricing_Tier',1]
							])
                        ->get();
				if(count($price_details)>0){ 
				
                        if(count($healthprofile)>0){
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
//<<<<<<< HEAD
//=======
									//echo "Con_Pens_ConSafty";exit;
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
                                    $p=$price_details[0]->Con_Pens_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								if($healthprofile[0]->medicarenumber!=null){
//<<<<<<< HEAD
//=======
									//echo "medicare";exit;
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->veteranaffairsnumber!=null){
//<<<<<<< HEAD
//=======
									//echo "Veterans";exit;
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
                                    $p=$price_details[0]->Veterans;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
//<<<<<<< HEAD
//=======
									//echo "ScriptStandardprice";exit;
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
                                    $p=$price_details[0]->ScriptStandardprice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                            }
                        }else{
                            $price="NA";
                        }
			}
			else
			{
				$price_details = DB::table('catalouge_master as C')
						->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST')
						->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
						->where([
							['CO.medmateItemcode',$catalouge_data[$i]->drugcode],
							['CO.businessid',0],
							['CO.Pricing_Tier',1]
							])
						
                        ->get();
				$catalouge_data[$i]->script="0";
				$catalouge_data[$i]->isscript = $catalouge_data[$i]->script;
				$p=$price_details[0]->OTCStandardprice;
				$gst =(($price_details[0]->GST)*$p)/100;
				$price =$p+ $gst;
			}
			$catalouge_data[$i]->price=round($price,2);
            $drug_details[]=$catalouge_data[$i];
            //$catalouge_data['price']=round($price,2);
        }
		
		return $drug_details;
		
	}
	
	public function ajaxGetItemsfromCatalogueOTC(Request $request)
	{
		$business = Locations :: where('locationid', $request->location)->get();
		$healthprofile = Healthprofile::where('userid', $request->userid)->get();
		//$catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,PI.image from catalouge_master as C left join packitemimages as PI on C.MIMsProductCode=PI.prodcode where C.MimsFormCode=PI.formcode and C.MimsPackCode=PI.packcode and C.itemNameDisplay LIKE '%".$request->medication."%'"); 
		$catalouge_data = DB::select("select mastercatid,itemNameDisplay as drug,MedmateItemCode as drugcode from catalouge_master where itemCommodityClass='OTC' and itemNameDisplay LIKE '%".$request->medication."%'"); 
		return $catalouge_data;
		 $drug_details=array();
        for($i=0;$i<count($catalouge_data);$i++){
            $catalouge_data[$i]->reference="Catalouge";
//<<<<<<< HEAD
			$price_details = DB::table('catalouge_master as C')
				->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST','C.ItemCommodityClass')
				->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
				->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
				->where([
					['C.itemNameDisplay',$catalouge_data[$i]->drugcode],
					
					])
				->orwhere([ ['CO.location_id',$request->location],
							['CO.businessid',$business[0]['businessid']],
							['CO.businessid',0],                           
							['CO.Pricing_Tier',1],
					])
				->orwhere([ 
							['CS.location_id',$request->location],
							['CS.businessid',$business[0]['businessid']],                         
							['CS.businessid',0], 
							['CS.Pricing_Tier',1],
					])
				->get();
//=======
//			$price_details = DB::select("select  `CO`.*, `CS`.*, `CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode` where (`CO`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$catalouge_data[$i]->drugcode."%' and `CO`.`Pricing_Tier` = 1 ) or (`CS`.`businessid` = 0 and `C`.`itemNameDisplay` LIKE '%".$catalouge_data[$i]->drugcode."%' and `CS`.`Pricing_Tier` = 1 and `CS`.`status` = 1)");
				
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
           
            if($catalouge_data[$i]->script=='Prescription')
			{
				
				$catalouge_data[$i]->script="Yes";
				if(count($price_details)>0){ 
                        if(count($healthprofile)>0){
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                    $p=$price_details[0]->Con_Pens_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								if($healthprofile[0]->veteranaffairsnumber!=null){
                                    $p=$price_details[0]->Veterans;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->ScriptStandardprice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                            }
                        }else{
                            $price="NA";
                        }
			}
			else
			{
				$catalouge_data[$i]->script="No";
				$p=$price_details[0]->OTCStandardprice;
				$gst =(($price_details[0]->GST)*$p)/100;
				$price =$p+ $gst;
			}
			$catalouge_data[$i]->price=round($price,2);
            $drug_details[]=$catalouge_data[$i];
            //$catalouge_data['price']=round($price,2);
        }
		
		return $drug_details;
		
	}
	public function ajaxGetItemsfromCatalogueOTCdata(Request $request)
	{
		$business = Locations :: where('locationid', $request->location)->get();
		$healthprofile = Healthprofile::where('userid', $request->userid)->get();
		$catalouge_data = DB::select("select C.itemNameDisplay as drug,C.MedmateItemCode as drugcode,C.itemCommodityClass as script,C.PhotoID,C.MIMsCMI as cmi,(select image from packitemimages as PI where PI.prodcode=C.MIMsProductCode and PI.formcode=C.MimsFormCode and PI.packcode=C.MimsPackCode) as image from catalouge_master as C where C.mastercatid='".$request->medication."'"); 
		//return $catalouge_data;	
		$drug_details=array();
        for($i=0;$i<count($catalouge_data);$i++){
            $catalouge_data[$i]->reference="Catalouge";
//<<<<<<< HEAD
			$price_details = DB::table('catalouge_master as C')
				->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST','C.ItemCommodityClass')
				->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
				->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
				->where([
					['C.itemNameDisplay',$catalouge_data[$i]->drugcode],
					])
				->orwhere([ ['CO.location_id',$request->location],
							['CO.businessid',$business[0]['businessid']],
							['CO.businessid',0],                           
							['CO.Pricing_Tier',1],
					])
				->orwhere([ 
							['CS.location_id',$request->location],
							['CS.businessid',$business[0]['businessid']],                         
							['CS.businessid',0], 
							['CS.Pricing_Tier',1],
					])
				->get();
//=======
		//	$price_details = DB::select("select  `CO`.*, `CS`.*, `CO`.`StandardPrice` as `OTCStandardprice`, `CS`.`StandardPrice` as `ScriptStandardprice`,  `C`.`itemNameDisplay`,`C`.`ShortDescription`, `C`.`GST`, `C`.`ItemCommodityClass` from `catalouge_master` as `C` left join `catalogueotcpricing` as `CO` on `C`.`MedmateItemCode` = `CO`.`medmateItemcode` left join `cataloguescriptpricing` as `CS` on `C`.`MedmateItemCode` = `CS`.`medmateItemcode` where (`CO`.`businessid` = 0 and `CO`.`medmateItemcode` ='".$catalouge_data[$i]->drugcode."' and `CO`.`Pricing_Tier` = 1 and `CO`.`status` = 1) or (`CS`.`businessid` = 0 and `CS`.`medmateItemcode` ='".$catalouge_data[$i]->drugcode."' and `CS`.`Pricing_Tier` = 1)");
				
//>>>>>>> d7d398635ac8dfd84b3be196ba7dcdd98eca6d00
		
           //print_r($price_details);exit;
            if($catalouge_data[$i]->script=='Prescription')
			{
				$price_details = DB::table('catalouge_master as C')
						->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST')
						->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
						
						->where([
							['CS.medmateItemcode',$catalouge_data[$i]->drugcode],
							['CS.businessid',0],
							['CS.Pricing_Tier',1]
							])
                        ->get();
				$catalouge_data[$i]->script="Yes";
				if(count($price_details)>0){ 
                        if(count($healthprofile)>0){
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                    $p=$price_details[0]->Con_Pens_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->veteranaffairsnumber!=null){
                                    $p=$price_details[0]->Veterans;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->ScriptStandardprice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                            }
                        }else{
                            $price="NA";
                        }
			}
			else
			{
				$price_details = DB::table('catalouge_master as C')
						->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST')
						->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
						->where([
							['CO.medmateItemcode',$catalouge_data[$i]->drugcode],
							['CO.businessid',0],
							['CO.Pricing_Tier',1]
							])
						
                        ->get();
				$catalouge_data[$i]->script="No";
				$p=$price_details[0]->OTCStandardprice;
				$gst =(($price_details[0]->GST)*$p)/100;
				$price =$p+ $gst;
			}
			$catalouge_data[$i]->price=round($price,2);
            $drug_details[]=$catalouge_data[$i];
            //$catalouge_data['price']=round($price,2);
        }
		
		return $drug_details;
		
	}

	
	public function totalorderscount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 7)
		{
		$orderscount = Orderdetails :: where('orderdetails.isnew','=',1)->where('orderdetails.orderstatus','!=','Pending Payment')->where('orderdetails.orderstatus','!=','99')->get();
		
		
		$payment = Ordertracking ::where('ordertracking.orderstatus','=','Process')
								  ->where('ordertracking.isnew','=',1)
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();
		
		
		$cancelledorders = Ordertracking ::where('ordertracking.orderstatus','=','Cancelled')
								  ->where('ordertracking.isnew','=',1)
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();
								  
		
		}
		else if($phamacy_details->profiletypeid == 3)
		{
			$locationid = Adminprofiletype :: where('adminprofiletype.userid' ,'=', $pharmacist->userid)
											  ->get();
			$orderscount = Orderdetails :: where('orderdetails.isnew','=',1)
											->where('orderdetails.locationid','=',$locationid[0]['locationid'])
											->where('orderdetails.orderstatus','!=','Pending Payment')
											->where('orderdetails.orderstatus','!=','99')
											->get();
		
		
		$payment = Ordertracking :: join('orderdetails','orderdetails.orderid', '=', 'ordertracking.orderid')
									->where('ordertracking.orderstatus','=','Process')
								  ->where('ordertracking.isnew','=',1)
								  ->where('orderdetails.locationid','=',$locationid[0]['locationid'])
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();
		
		
		$cancelledorders = Ordertracking ::join('orderdetails','orderdetails.orderid','=','ordertracking.orderid')
									->where('ordertracking.orderstatus','=','Cancelled')
								  ->where('ordertracking.isnew','=',1)
								  ->where('orderdetails.locationid','=',$locationid[0]['locationid'])
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();
		
		}
		
		$neworderscount = $orderscount->count();	
		$paymentorderscount = $payment->count();		
		$cancelledorderscount = $cancelledorders->count();
		$totalcount = $neworderscount+$paymentorderscount;
								  
		return $totalcount;
	}
	public function updatestatus($status,$orderid,$userid,$ordertotal)
	{
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$orderid)
                                        ->where('orderdetails.enduserid','=',$userid)
                                        ->update(['orderdetails.orderstatus' => 'Awaiting Authorisation','orderdetails.orderTotal' => $ordertotal]);
        $ordertrack = Ordertracking :: where('ordertracking.orderid','=',$orderid)->where('ordertracking.trackingstatus','=',1)->get();
        $noofrows = $ordertrack->count();
        for($i=0;$i<$noofrows;$i++)
        {
            $orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$orderid)
                                              ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
                                            ->update(['ordertracking.trackingstatus' => '0']);

        }
            $orderTracking = new Ordertracking;
            $orderTracking->orderid = $orderid;
            $orderTracking->orderstatus = 'Awaiting Authorisation';
            $orderTracking->orderremarks = '';
            $orderTracking->statusCreatedat = date('Y-m-d H:i:s');
            $orderTracking->statusupdatedat = date('Y-m-d H:i:s');
            $orderTracking->save();
			
		 return view('awaitingauthorisation');
	}
	
	
	
	public function neworderscount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 7)
		{
		$orderscount = Orderdetails :: where('orderdetails.isnew','=',1)->get();
		
		}
		else
		{
			$locationid = Adminprofiletype :: where('adminprofiletype.userid' ,'=', $pharmacist->userid)
											  ->get();
			$orderscount = Orderdetails :: where('orderdetails.isnew','=',1)
											->where('orderdetails.locationid','=',$locationid[0]['locationid'])
											->get();
		}
		$neworderscount = $orderscount->count();
		
								  
		return $neworderscount;
	}
	public function paymentorderscount()
	{		
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 7)
		{
		$payment = Ordertracking ::where('ordertracking.orderstatus','=','Process')
								  ->where('ordertracking.isnew','=',1)
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();
		}
		else
		{
			$locationid = Adminprofiletype :: where('adminprofiletype.userid' ,'=', $pharmacist->userid)
											  ->get();
		$payment = Ordertracking :: join('orderdetails','orderdetails.orderid', '=', 'ordertracking.orderid')
									->where('ordertracking.orderstatus','=','Process')
								  ->where('ordertracking.isnew','=',1)
								  ->where('orderdetails.locationid','=',$locationid[0]['locationid'])
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();	
		}
		$paymentorderscount = $payment->count();
								  
		return $paymentorderscount;
	}
	public function cancelledorderscount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 7)
		{
		$cancelledorders = Ordertracking ::where('ordertracking.orderstatus','=','Cancelled')
								  ->where('ordertracking.isnew','=',1)
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();
		}
		else
		{
			$locationid = Adminprofiletype :: where('adminprofiletype.userid' ,'=', $pharmacist->userid)
											  ->get();
		$cancelledorders = Ordertracking ::join('orderdetails','orderdetails.orderid','=','ordertracking.orderid')
									->where('ordertracking.orderstatus','=','Cancelled')
								  ->where('ordertracking.isnew','=',1)
								  ->where('orderdetails.locationid','=',$locationid[0]['locationid'])
								  ->where('ordertracking.trackingstatus','=',1)
								  ->get();	
		}
								  
		$cancelledorderscount = $cancelledorders->count();	
								  
		return $cancelledorderscount;
	}
	
	public function cancelOrder($orderid,$userid)
	 {
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		
		$orderdetails = Orderdetails :: where('orderdetails.orderid','=',$orderid)
										->where('orderdetails.enduserid','=',$userid)
										->update(['orderdetails.orderstatus' => 'Cancelled']);
		$ordertrack = Ordertracking :: where('ordertracking.orderid','=',$orderid)->where('ordertracking.trackingstatus','=',1)->get();
		
		$noofrows = $ordertrack->count();
		
		for($i=0;$i<$noofrows;$i++)	
		{
			$orderstsChange[$i] = Ordertracking :: where('ordertracking.orderid','=',$orderid)
											  ->where('ordertracking.ordertrackingid','=',$ordertrack[$i]['ordertrackingid'])
											->update(['ordertracking.trackingstatus' => '0']);
											
		}								
			$orderTracking = new Ordertracking;
			$orderTracking->orderid = $orderid;
			$orderTracking->orderstatus = "Cancelled";
			$orderTracking->isnew = '1';
			//$orderTracking->orderremarks = $request->orderremarks;
			$orderTracking->statusCreatedat = date('Y-m-d H:i:s');
			$orderTracking->statusupdatedat = date('Y-m-d H:i:s');
			$orderTracking->save();	

			$this->send_notification("Cancelled",$orderid,$userid);
		//Medlist Status Change After Cancellation
		
			$orderBasket = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
										->where('orderbasketlist.userid','=',$userid)->get();
			
			$noofItems = $orderBasket->count();
			for($b=0;$b<$noofItems;$b++)
			{
										
				if($orderBasket[$b]['prescriptionid'] !="")
				{
				
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
																  ->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
																  ->where('prescriptionitemdetails.isscript', '=', 1)															  
																	->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				
				//To Check if uploaded script is related Medlist Medication script
				$getmedlistid = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionid', '=', \DB::raw('"'.$orderBasket[$b]['prescriptionid'].'"'))
															->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
															->get();
				$count = $getmedlistid->count();
				if($count > 0)
				{
					if($count == 1)
					$medlistdetails = $getmedlistid[0]['medilistid'];
					$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $getmedlistid[0]['medilistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$b]['profileid'])
													->update(['prescriptionitemdetails.medstatus' => 'Add Your Script']);
				}
		
				}
				else
				{
					
				$medsts = Prescriptionitemdetails :: where('prescriptionitemdetails.prescriptionitemid', '=', $orderBasket[$b]['medlistid'])
													->where('prescriptionitemdetails.profileid', '=', $orderBasket[$i]['profileid'])
													->where('prescriptionitemdetails.isscript', '=', 0)
													->update(['prescriptionitemdetails.medstatus' => 'medlist']);
				}
			}
		
		 //return view('cancelledorders'); 
		 return \Redirect::route('cancelledorders');
     }
	 
	 public function updatedeliveryaddress(Request $request)
	{
		
		$v = Validator::make($request->all(), [
			'addr1' => 'required|string',
			'addsuburb' => 'required|string',
			'addstate' => 'required|string',
			'addpostcode' => 'required|string'
		]);
		if ($v->fails()) {
			
			return redirect()->back()->withErrors($v->errors())->withError('check Your Inputs');
		}
		else
		{
			
		$deliveryAddress = new Deliveryaddress;
		$deliveryAddress->userid = $request->myuserid;						
		$deliveryAddress->addressline1 = $request->addr1;
		$deliveryAddress->addressline2 = $request->addr2;
		$deliveryAddress->suburb = $request->addsuburb;
		$deliveryAddress->state = $request->addstate;
		$deliveryAddress->postcode = $request->addpostcode;				
		$deliveryAddress->mobile = $request->mobile;
		$deliveryAddress->lattitude = "";
		$deliveryAddress->longitude = "";
		$deliveryAddress->save();

		$deliveryaddressid = $deliveryAddress->id;
		
		$orderdetails =  Orderdetails :: where('orderdetails.orderid','=',$request->orderid)
										->where('orderdetails.enduserid','=',$request->myuserid)
										->update(['orderdetails.deliveryaddressid' => $deliveryaddressid]);
							  
		return redirect()->back();
		}
	}
	
	public function inboxorderscount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		
		if($phamacy_details->profiletypeid == 3)
		{
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where ('ordertracking.orderstatus' , '=' , 'Waiting For Review')
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								 ->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->get();
		}
		else
		{
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where ('ordertracking.orderstatus' , '=' , 'Waiting For Review')
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								// ->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->get();	
		}
		$orderArray = [];
		
		foreach ($orders as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		return count($orderArray);
	}
	public function instoreorderscount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		
		if($phamacy_details->profiletypeid == 3)
		{
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where ('ordertracking.orderstatus' , '=' , 'Widget Order')
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								 ->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->get();
		}
		else
		{
		$orders = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')															
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')															
								 ->where ('ordertracking.orderstatus' , '=' , 'Widget Order')
								 ->where ('ordertracking.trackingstatus' , '=' , '1')
								// ->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->get();	
		}
		$orderArray = [];
		
		foreach ($orders as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		return count($orderArray);
	}
	public function authoraisecount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{
        $details = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Authorisation')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								->get();
		}
		else
		{
			$details = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 //->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 //->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Authorisation')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								->get();
		}
		$orderArray = [];
		
		foreach ($details as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		
		return count($orderArray);
	}
	
	public function processcount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{		
              $process = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 
								 ->where ('ordertracking.orderstatus' , '=' , 'Process')
								->where ('ordertracking.trackingstatus' , '=' , '1')
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)								
								 ->get();
		}
		else
		{
			$process = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')
								//->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')								 
								 ->where ('ordertracking.orderstatus' , '=' , 'Process')
								->where ('ordertracking.trackingstatus' , '=' , '1')
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)								
								 ->get();
		}
		$orderArray = [];
		
		foreach ($process as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		
		return count($orderArray);						 
	}
	public function deliverycount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{		
              $deliverylist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Delivery')
									->where ('ordertracking.trackingstatus' , '=' , '1')	
									->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)								
								 ->get();
		}
		else
		{
			$deliverylist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Delivery')
									->where ('ordertracking.trackingstatus' , '=' , '1')	
								//	->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)								
								 ->get();
		}
		$orderArray = [];
		  foreach ($deliverylist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		
		return count($orderArray);						 
	}
	public function pickupcount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{		
              $pickuplist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Pickup')
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								 ->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->get();
		}
		else
		{
			$pickuplist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Awaiting Pickup')
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								// ->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								 ->get();
		}
		$orderArray = [];
		  foreach ($pickuplist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		return count($orderArray);	
						
	}
	public function transitcount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{		
              $transitlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'In Transit')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		else
		{
			 $transitlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'In Transit')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		$orderArray = [];
		  foreach ($transitlist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		return count($orderArray);							 
	}
	public function completedcount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{		
              $completedlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Completed')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		else{
			$completedlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Completed')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		$orderArray = [];
		  foreach ($completedlist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		return count($orderArray);						 
	}
	public function cancelledcount()
	{
		$pharmacist = Auth::user();
		$phamacy_details = User::findOrFail($pharmacist->userid);
		if($phamacy_details->profiletypeid == 3)
		{		
              $cancelledlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								 ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								 ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Cancelled')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		else
		{
			$cancelledlist = Orderdetails:: join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')	
								 ->join ('users','users.userid', '=' , 'orderdetails.enduserid')	
								 ->join ('orderbasketlist','orderbasketlist.orderid', '=' , 'orderdetails.orderid')	
								 ->leftJoin ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								// ->join ('locations','locations.locationid', '=' , 'orderdetails.locationid')	
								// ->join ('adminprofiletype','adminprofiletype.locationid', '=' , 'orderdetails.locationid')
								 ->where ('ordertracking.orderstatus' , '=' , 'Cancelled')	
								 ->where ('ordertracking.trackingstatus' , '=' , '1')	
								//->where ('adminprofiletype.userid' , '=' , $pharmacist->userid)
								//->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.orderid', 'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')->get();
								->select(DB::raw("DATE_FORMAT(ordertracking.statuscreatedat, '%d-%m-%y %H:%i') as createddate"),'ordertracking.statuscreatedat','orderdetails.ordertype','orderdetails.orderid', DB::raw("CONCAT(DATE_FORMAT(orderdetails.userpreffereddate, '%d-%m-%y'),' ',orderdetails.userprefferedtime) as deliverydatetime"),'orderdetails.uniqueorderid','ordertracking.orderstatus',DB::raw("CONCAT(Ifnull(users.firstname,' ') ,' ', Ifnull(users.lastname,' ')) as customername"),'deliveryaddresses.suburb','orderbasketlist.drugname')
								 ->orderBy('ordertracking.statuscreatedat', 'DESC')
								 ->get();
		}
		$orderArray = [];
		  foreach ($cancelledlist as $order){
			if(array_key_exists($order->orderid, $orderArray)) {
				$orderArray[$order->orderid] =   $orderArray[$order->orderid]. " , ". $order->drugname;					
			} else {
				$orderArray[$order->orderid] = $order->drugname;
			}
			$order->drugname = $orderArray[$order->orderid];
			$customArray[$order->orderid] = $order;			
		}
		
		return count($orderArray);						 
	}
	
	public function updatedriverinstruction(Request $request)
	{		
		$updatedetails = DB::table('orderdetails')->where('orderid','=',$request->orderid)
										->where('enduserid','=',$request->userid)
										->update(['driverinstructions' => $request->instruction]);										
										
		return 	$updatedetails;						
	}
	
	public function deliveryslot(Request $request)
	{
		//print_r($request->selecteddate);
		//print_r($request->locationid);
		//print_r($request);exit;
		
		$dayofweek = date('w', strtotime($request->selecteddate));
		if($dayofweek == 1)
			$day = "Monday";
		elseif($dayofweek == 2)
			$day = "Tuesday";
		elseif($dayofweek == 3)
			$day = "Wednesday";
		elseif($dayofweek == 4)
			$day = "Thursday";	
		elseif($dayofweek == 5)
			$day = "Friday";
		elseif($dayofweek == 6)
			$day = "Saturday";
		elseif($dayofweek == 0)
			$day = "Sunday";
		$slotArray = [];
		//echo $request->selecteddate."selected";
		$locationname = Locations :: where('locationid','=',$request->locationid)->get();
		date_default_timezone_set($locationname[0]['timezone']);
		//if($request->selecteddate = date("Y-m-d")){
		$time = date("H")+2; //}
		//else{$time=2;}
		
		//echo $request->selecteddate;
		//echo date("Y-m-d");
		$slot = DB::table('locationdeliverytimings')->where('locationid','=',$request->locationid)
										->where('dayoftheweek','=',$day)
										->where('deliverytype','=', 'Delivery')
										//->where('timings','>=' ,$time)
										->select('deliverywindow')
										->get();
		
		
		$slotArray =	$slot;							
		return $slotArray;
	}
	
	public function updatedeliveryslot(Request $request)
	{
		// print_r($request->orderid);
		// print_r($request->datepicker);
		// print_r($request->select_slot); exit;
		$updatedetails = DB::table('orderdetails')->where('orderid','=',$request->orderid)										
										->update(['userpreffereddate' => $request->datepicker ,'userprefferedtime' => $request->select_slot]);	
		return redirect()->back();
	}
	public function sendtomedview(Request $request) {
		$data = array(
			"location" => $request->locationid,
			"barcode" => $request->barcode,
			"mobilenumber" => $request->mobilenumber,
			"issent" => $request->issent,
			"prescriptionitemid"=>$request->prescriptionitemid,
			"order_id"=>$request->order_id,
		);
		//return $data;
			$location=DB::table('locations')->where('locationid',$request->locationid)->get();
			$orderdata=DB::table('orderdetails')->where('uniqueorderid',$request->order_id)->get();
		//	print_r($orderdata);exit;
			$isgeneric=false;
			if($orderdata[0]->isGeneric!=null){
				$isgeneric=true;
			}
			$notes=$request->order_id;
			date_default_timezone_set("UTC");
			$date= date('Y-m-d\TH:i:s');
			$date=$date."+00:00";
			$locationID = $location[0]->medviewFlowStoreID;
			$mobile = $request->mobilenumber;
			$barcode = $request->barcode;
			/*$locationID="100100300";
			$mobile="9989542822";
			$barcode="21KR32BQG5KYXJ7Q71";*/
			$curl = curl_init();
			curl_setopt_array($curl, array(
  CURLOPT_URL => env('MEDVIEW_URL'),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>"<Task xmlns=\"http://hl7.org/fhir\">\r\n  <meta>\r\n    <security>\r\n      <system value=\"http://fred.com.au/security-context\" />\r\n      <code value=\"$locationID\" />\r\n    </security> \r\n  </meta>\r\n  <extension url=\"http://fhir.fred.com.au/mvf/StructureDefinition/delivery\">\r\n    <valueBoolean value=\"true\" />\r\n  </extension>\r\n  <extension url=\"http://fhir.fred.com.au/mvf/StructureDefinition/preferred-notification-type\">\r\n    <valueCoding>\r\n      <system value=\"http://fhir.fred.com.au/mvf/CodeSystem/notification-type\" />\r\n      <code value=\"3\" />\r\n      <display value=\"Sms\" />\r\n    </valueCoding>\r\n  </extension>\r\n  <extension url=\"http://fhir.fred.com.au/mvf/StructureDefinition/notification\">\r\n    <valueString value=\"$mobile\" />\r\n  </extension>\r\n  <extension url=\"http://fhir.fred.com.au/mvf/StructureDefinition/accept-generics\">\r\n    <valueBoolean value=\"$isgeneric\" />\r\n  </extension>\r\n  <extension url=\"http://fhir.fred.com.au/mvf/StructureDefinition/accept-generics-notes\">\r\n    <valueString value=\"generic brand notes\" />\r\n  </extension>\r\n  <intent value=\"plan\" />\r\n  <note>\r\n    <text value=\"$request->oreder_id\" />\r\n  </note>\r\n  <restriction>\r\n    <period>\r\n      <start value=\"$date\" />\r\n    </period>\r\n  </restriction>\r\n  <input>\r\n    <type>\r\n      <text value=\"Dispense Prescription\" />\r\n    </type>\r\n    <valueReference>\r\n      <identifier>\r\n        <type>\r\n          <text value=\"ETP Identifier\" />\r\n        </type>\r\n        <system value=\"http://fhir.erx.com.au/NamingSystem/identifiers#pesscriptid\" />\r\n        <value value=\"$barcode\" />\r\n      </identifier>\r\n    </valueReference>\r\n  </input>\r\n</Task>",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/fhir+xml",
    "Accept-Charset: utf-8",
    "Ocp-Apim-Subscription-Key: ".env('MEDVIEW_SUBSCRIPTION_KEY')
  ),
));

			$response = curl_exec($curl);
			$xml = simplexml_load_string($response);
			$json = json_encode($xml);
			$arr = json_decode($json,true);
			//print_r($arr);exit;
			$taskid=$arr['identifier']['value']['@attributes']['value'];
			$err = curl_error($curl);
			$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			if ($err) {
			  return $err;
			} else {
				if( $http_code == 201 ) {
					DB::table('prescriptionitemdetails')->where('prescriptionitemid',$request->prescriptionitemid)->update(array('issent'=>1,'medviewtaskid'=>$taskid));
					return "success";
				} 
			}
	}
	
	public function sendintransitemailtocustomer($locationid,$deliveryaddressid,$userid,$orderid)
	{
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$user = User :: where('userid', $userid)->get();
		$location = Locations :: where('locationid','=',$locationid)->get();
		$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
		$orderData=$orderdetails;
		$uniqueorderid=$orderdetails[0]->uniqueorderid;
		//$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$deliveryaddressid)->get();
		$deliveryaddress = Deliveryaddress :: where('addresstype','profileaddress')->where('userid',$userid)->get();
		$body_message1='';
		$body_message=' 
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Order Dispatched</h1>  
                                                <p>Hi '.$orderData[0]['fname'].',<br>
                                                Your order has been dispatched by the Pharmacy for delivery. You can view the status of your order within the Medmate app. The Pharmacy will contact you if there are any issues.</p>
                                                
												<div class="line">&nbsp;</div>
												<br>
                                                
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4>Your Details:</h4>
                                                            '.$user[0]['firstname'].' '.$user[0]['lastname'].' <br> '.$user[0]['mobile'].' <br>   
                                                              ';
                                                              if($deliveryaddress->count() > 0)
                                                              {
                                                                $body_message .=' '.$deliveryaddress[0]['addressline1'].' '.$deliveryaddress[0]['addressline2'].' <br>'.$deliveryaddress[0]['suburb'].', '.$deliveryaddress[0]['state'].', '.$deliveryaddress[0]['postcode'].' <br> Australia
                                                              ';
                                                              }
                                                            $body_message .='
                                                        </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$location[0]['locationname'].' <br>'.$location[0]['phone'].' <br> '.$location[0]['address'].' <br> '.$location[0]['suburb'].', '.$location[0]['suburb'].', '.$location[0]['state'].' <br> Australia
                                                        </td>                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
		';
				$message['to']           = $user[0]['username'];
                //$message['subject']      = "Complete Order";
                $message['subject']      = env('ENV')." Order Dispatched";
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
	}
	
	public function sendpickupemailtocustomer($locationid,$deliveryaddressid,$userid,$orderid)
	{
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$user = User :: where('userid', $userid)->get();
		$location = Locations :: where('locationid','=',$locationid)->get();
		$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
		$orderData=$orderdetails;
		$uniqueorderid=$orderdetails[0]->uniqueorderid;
		//$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$deliveryaddressid)->get();
		$deliveryaddress = Deliveryaddress :: where('addresstype','profileaddress')->where('userid',$userid)->get();
		if($user[0]->mobile!=null || $user[0]->mobile!=''){
			$orgphone=$user[0]->mobile;
			$phonelength = strlen($orgphone);
		if($phonelength == 10)
		{			
			if(substr($orgphone,0,2) == '04')
				$finalphone = $orgphone;			
		}
		else if($phonelength > 10)
		{
			
			if(substr($orgphone,0,4) == '+614')
			{				
				$phoneVal = str_replace("+614", "",$orgphone);
				$finalphone='04'.$phoneVal;
			}
			elseif(substr($orgphone,0,5) == '+6104')
			{				
				$phoneVal = str_replace("+6104", "",$orgphone);
				$finalphone='04'.$phoneVal;
			}
			elseif(substr($orgphone,0,5) == '00614')
			{				
				$phoneVal = str_replace("00614", "",$orgphone);
				$finalphone='04'.$phoneVal;
			}
			elseif(substr($orgphone,0,6) == '006104')
			{				
				$phoneVal = str_replace("006104", "",$orgphone);
				$finalphone='04'.$phoneVal;
			}
		}	
		if(isset($finalphone) && $finalphone!='' && $finalphone!=0)
		{
			$email=$finalphone.'@e2s.smsbroadcast.com.au';
			$subject=$finalphone;
		
		$body_message1='Hello, your Medmate Order '.$uniqueorderid.' is ready for Pick Up at '.$location[0]->locationname.'. Thank you.';
		$message['to']           = $email;
                //$message['subject']      = "Complete Order";
                $message['subject']      = $subject;
                $message['body_message'] = $body_message1;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
		}
		}
		$body_message=' 
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Ready for Pick Up</h1>  
                                                <p>Hi '.$orderData[0]['fname'].',<br>
                                                Your order is ready for pick up. Please come to Pharmacy to collect your order at your convenience. Your order has been pre-purchaed and you do not need to pay again.</p>
                                                
												<div class="line">&nbsp;</div>
												<br>
                                                
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4>Your Details:</h4>
                                                            '.$user[0]['firstname'].' '.$user[0]['lastname'].' <br> '.$user[0]['mobile'].' <br>   
                                                              ';
                                                              
                                                            $body_message .='
                                                        </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$location[0]['locationname'].' <br>'.$location[0]['phone'].' <br> '.$location[0]['address'].' <br> '.$location[0]['suburb'].', '.$location[0]['suburb'].', '.$location[0]['state'].' <br> Australia
                                                        </td>                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
		';
				$message['to']           = $user[0]['username'];
                //$message['subject']      = "Complete Order";
                $message['subject']      = env('ENV')." Complete Order";
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
	}
	public function sendawaitingemailtocustomer($locationid,$deliveryaddressid,$userid,$orderid)
	{
		$healthprofile = Healthprofile::where('userid', $userid)->get();
		$user = User :: where('userid', $userid)->get();
		$location = Locations :: where('locationid','=',$locationid)->get();
		$orderdetails = Orderdetails :: where('orderid' , $orderid)->get();
		$orderData=$orderdetails;
		$uniqueorderid=$orderdetails[0]->uniqueorderid;
		$placeddate = date('d F Y', strtotime($orderData[0]['created_at']));
		//$deliveryaddress = Deliveryaddress :: where('deliveryaddressid',$deliveryaddressid)->get();
		$deliveryaddress = Deliveryaddress :: where('addresstype','profileaddress')->where('userid',$userid)->get();
		$prescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 1)
                                              ->get();
        $nqty=0;$nprice=0;
					if($prescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$prescriptionitems->count();$np++)
						{
							$nqty = $nqty+$prescriptionitems[$np]['drugpack'];
							$priceval = $prescriptionitems[$np]['drugpack']*$prescriptionitems[$np]['originalprice'];
							$nprice = $priceval+$nprice;
						}
					} 
			$nonprescriptionitems = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->where('orderbasketlist.isscript' , '=' , 0)
                                              ->get();
        $qty=0;$npriceval =0;
		//'.$tbctext.'
					if($nonprescriptionitems->count() > 0)
					{
						
						for($np=0;$np<$nonprescriptionitems->count();$np++)
						{
							$qty = $qty+$nonprescriptionitems[$np]['drugpack'];
							$npval=$nonprescriptionitems[$np]['drugpack']*$nonprescriptionitems[$np]['originalprice'];
							$npriceval = $npriceval+$npval;
						}
					} 
			$subtotal = $npriceval+$nprice;
			$gst=0;
			$gstcalc = Orderbasketlist :: where('orderbasketlist.orderid','=',$orderid)
                                              ->where('orderbasketlist.basketliststatus' , '=' , 1)
                                              ->get();
			for($i=0;$i<$gstcalc->count();$i++)
			{
				$gstcalcval = $this->compute_price($orderData[0]['locationid'],$gstcalc[$i]['prescriptionid'],$orderData[0]['myuserid']);
				$gst = $gst+$gstcalcval;
			}
		$body_message='';
		$body_message=' 
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>order confirmation</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
            h3 {
                color: #1998d5 !important;
                font-size: 18px;
                font-style:normal;
                font-weight:500;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            h4 {
                color: #303030 !important;
                font-size: 16px;
                font-style:normal;
                font-weight:700;
                line-height:100%;
                letter-spacing:normal;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
                margin-top: 0;
                margin-bottom: 0;
            }
            p {                
                font-size: 16px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 30px;
                margin: 0 0 30px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;
            }
			#bodyCell{padding:20px 0;}
			#templateContainer{width:600px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: left;
			}
            .topContent {
                text-align: right;
            }
			.bodyContent {
				text-align: left;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
            #orderDetails {
                background-color: #fafafa;
                width: 100%;
                margin: 0 0 20px;
            }
            .orderDetailsTop {
                text-align: right;
                font-size: 12px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 24px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
            }
            #orderDetailsPurchases th {
                border-bottom: 1px solid #1f1f1f;                                
                font-size: 14px;
                font-weight: 500;
                padding: 7px 8px;
            }
            #orderDetailsPurchases td {
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 28px;
                padding: 6px 8px;
            }
            #orderTotals {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 400;
                font-style: normal;
                letter-spacing: normal;
                line-height: 19px;
                margin: 0 0 40px;
            }
            #detailsTable h4 {
                font-family: "Roboto", Arial, Helvetica, sans-serif;  
                font-size: 14px;
                font-weight: 500;
                line-height: 100%;
                margin: 0 0 4px;
            }
            #detailsTable {
                font-size: 12px;
                font-family: "Roboto", Arial, Helvetica, sans-serif;    
                line-height: 21px;
            }
			.preFooter {
				text-align: center;			
				font-size: 16px;
				line-height:100%;
				font-family: "Roboto", Arial, Helvetica, sans-serif;	
			}
            .preFooter .line {
                border-bottom: 1px solid #979797;
            }
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:600px;}
			.footerContent {
				text-align:center;
				font-family: "Roboto", Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 640px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
                    max-width:600px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                			<td height="24" style="height:24px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="headerContent">
                                            	<img src="https://pharmacy.medmate.com.au/images/emailtemp/medmate-logo.png" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr> 
                                    	<tr>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                            <td width="50" style="width:50px;height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                        <tr>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                            <td height="23" style="height:23px;">&nbsp;</td>
                                            <td width="50" height="23" style="height:23px;width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50"></td>
                                            <td valign="top" class="topContent">
                                                <p style="text-aling: right;margin-bottom: 0;">Order:'.$uniqueorderid.'</p>
                                            </td>
                                            <td width="50"></td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                			<td height="20" style="height:20px;">&nbsp;</td>
                                			<td width="50" height="20" style="height:20px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent"> 
                                                <h1 style="margin-bottom: 22px;">Review Price & Checkout</h1>  
                                                <p>Hi '.$user[0]['firstname'].',<br>
                                                Thank you for your recent request for a price. The Pharmacy has provided a quote. Please open the order within the Medmate app to review and checkout.</p>
                                                <h3 style="font-size: 20px;margin-top: 40px;margin-bottom: 18px;">Your Order Details</h3>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetails"> 
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td class="orderDetailsTop">
                                                            Order: '.$uniqueorderid.'<br>
                                                            Placed on '.$placeddate.'
                                                        </td>        
                                                        <td width="20" style="width:20px;">&nbsp;</td>                                                
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                        <td height="23" style="height:23px;">&nbsp;</td>
                                                        <td width="20" height="23" style="height:23px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderDetailsPurchases"> 
                                                                <thead>
                                                                    <tr>
                                                                        <th>Items</th>
                                                                        <th>Qty</th>
                                                                        <th>Price</th>
                                                                    </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Prescription Items<br>
                                                                            
                                                                        </td>
                                                                        <td>'.$nqty.'</td>
                                                                        <td>'.$nprice.'</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Non-Prescription Items</td>
                                                                        <td>'.$qty.'</td>
                                                                        <td>'.$npriceval.'</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td width="20" style="width:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                        <td height="40" style="height:40px;">&nbsp;</td>
                                                        <td width="20" height="40" style="height:40px;width:20px;">&nbsp;</td>
                                                    </tr>
                                                </table>

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="orderTotals"> 
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Subtotal</td>
                                                        <td>'.$subtotal.'</td>
                                                    </tr>';
                                                    if($orderData[0]['ordertype'] == "Delivery")
                                                    {
                                                    $body_message .=   '<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Delivery&nbsp;Fee</td>
                                                        <td>'.$orderData[0]['deliveryfee'].'</td>
                                                    </tr>';
                                                    }
                                                    $body_message .='<tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>Order&nbsp;Total</td> 
                                                        <td>'.$orderData[0]['ordertotal'].'</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">&nbsp;</td>
                                                        <td>GST</td>
                                                        <td>'.$gst.'</td>
                                                    </tr> 
                                                </table>


                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detailsTable"> 
                                                    <tr>
                                                        <td valign="top">
                                                            <h4>Your Details:</h4>
                                                            '.$user[0]['firstname'].' '.$user[0]['lastname'].' <br> '.$user[0]['mobile'].' <br>   
                                                              ';
                                                              if($deliveryaddress->count() > 0)
                                                              {
                                                                $body_message .=' '.$deliveryaddress[0]['addressline1'].' '.$deliveryaddress[0]['addressline2'].' <br>'.$deliveryaddress[0]['suburb'].', '.$deliveryaddress[0]['state'].', '.$deliveryaddress[0]['postcode'].' <br> Australia
                                                              ';
                                                              }
                                                            $body_message .='
                                                        </td>
                                                        <td valign="top">
                                                            <h4>Pharmacy Details:</h4>
                                                            '.$location[0]['locationname'].' <br>'.$location[0]['phone'].' <br> '.$location[0]['address'].' <br> '.$location[0]['suburb'].', '.$location[0]['suburb'].', '.$location[0]['state'].' <br> Australia
                                                        </td>                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                			<td height="45" style="height:45px;">&nbsp;</td>
                                			<td width="50" height="45" style="height:45px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td> 
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter">
                                                Have a question or feedback? <a href="https://medmate.com.au/faq/" target="_blank">Contact Us</a> 
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;"></td>
                                            <td height="20" style="height:15px;line-height:15px;font-size: 10px;">&nbsp;</td>
                                            <td width="50" style="width:50px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="preFooter"> 
                                                <div class="line"></div>
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td align="center" class="footerContent">
                                                © 2020 Medmate Pty Ltd
                                            </td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                            <td height="25" style="height:25px;">&nbsp;</td>
                                            <td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->

                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>
		';
		$message['to']           = $user[0]['username'];
                //$message['subject']      = "Complete Order";
                $message['subject']      = env('ENV')." Complete Order";
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                 // ->from('medmate-app@medmate.com.au', 'Medmate')
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
	}
public function compute_price($location,$drug,$profile){
$price="NA";
$gst=0;
$business = Locations :: where('locationid', $location)->get();

$price_details = DB::table('catalouge_master as C')
						->select('CO.*','CS.*','CO.StandardPrice as OTCStandardprice','CS.StandardPrice as ScriptStandardprice','C.ShortDescription','C.GST','C.ItemCommodityClass')
						->leftjoin('catalogueotcpricing as CO', 'C.MedmateItemCode', '=', 'CO.medmateItemcode')
						->leftjoin('cataloguescriptpricing as CS', 'C.MedmateItemCode', '=', 'CS.medmateItemcode')
						// ->where([
							// ['C.itemNameDisplay',$drug],
							// ])
						->where([ //['CO.location_id',$location],
									['CO.businessid',0],
									['CO.Pricing_Tier',1],
									['C.itemNameDisplay','LIKE','"%'.$drug.'%"'],
									//['CO.businessid',$business[0]['businessid']],
							])
						->orwhere([ //['CS.location_id',$location],
									['CS.businessid',0],
									['CS.Pricing_Tier',1],
									['C.itemNameDisplay','LIKE','"%'.$drug.'%"'],	
									//['CS.businessid',$business[0]['businessid']],
							])
                        ->get();                        
                        $healthprofile = DB::table('healthprofile')->where('userid',$profile)->get();
						if(count($price_details)>0){
						if($price_details[0]->ItemCommodityClass=='OTC')
						{
							$p=$price_details[0]->OTCStandardprice;
							$gst =(($price_details[0]->GST)*$p)/100;
							$price =$p+ $gst;
							
						}
						else
						{
							if(count($healthprofile)>0){
							
                                if($healthprofile[0]->safetynet!=null){
                                    $p=$price_details[0]->SafetyNet;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
									
                                }
                                if($healthprofile[0]->pensionerconcessioncard !=null || $healthprofile[0]->concessionsafetynetcard !=null){
                                    $p=$price_details[0]->Con_Pens_ConSafty;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
								if($healthprofile[0]->veteranaffairsnumber !=null){
                                    $p=$price_details[0]->Veterans;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }
                                if($healthprofile[0]->medicarenumber!=null){
                                    $p=$price_details[0]->medicare;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }else{
                                    $p=$price_details[0]->ScriptStandardprice;
									$gst =(($price_details[0]->GST)*$p)/100;
									$price =$p+ $gst;
                                }

                            }
							
						}
                        }
						else{
                            $price="NA";
                        }
                        return round($gst,2);
                        

    } 
//}
}
