<?php

namespace App\Http\Controllers\Pharmacy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Orderdetails; 
use App\models\Basket; 
use App\models\Ordertracking; 
use App\models\Prescriptiondetails; 
use App\models\Locations; 
use App\models\Orderbasketlist; 
use Illuminate\Support\Facades\Auth; 
use Validator;
class OrderDetailstoPharmacyController extends Controller
{
    public function getorderdetails(Request $request,$orderid=null)
	{
		//$orderid = base64_decode($orderid);
		$orderData = Orderdetails:: join ('basket','basket.orderid', '=', 'orderdetails.orderid')
								  ->join ('orderbasketlist','orderbasketlist.orderid' , '=' , 'orderdetails.orderid')
								  ->join ('ordertracking','ordertracking.orderid', '=' , 'orderdetails.orderid')								  
								  ->join ('prescriptiondetails','prescriptiondetails.prescriptionid', '=' , 'orderdetails.prescriptionid')								  
								  ->join ('tokenswallet','tokenswallet.prescriptionid', '=' , 'orderdetails.prescriptionid')								  
								  ->join ('deliveryaddresses','deliveryaddresses.deliveryaddressid', '=' , 'orderdetails.deliveryaddressid')	
								  ->join ('localtions','localtions.localtionid', '=' , 'orderdetails.localtionid')
								  ->join ('users','users.userid', '=' , 'orderdetails.createdBy')	
								  ->select('orderdetails.*','ordertracking.*','locations.locationname','locations.address','locations.postcode','pharmacy.pharmacyLogo','locations.phone',
								  'prescriptiondetails.userid','prescriptiondetails.prescriptiontype','prescriptiondetails.prescriptiontitle','prescriptiondetails.prescriptionimage'
								  ,'prescriptiondetails.prescriptionbackimage','prescriptiondetails.barcode','tokenswallet.*','deliveryaddresses.*','users.address as defaultaddress','users.mobile as mobilenumber','users.profileImage as myImage','users.username as emailaddress')
								  ->where('orderdetails.orderid' , '=' , $orderid)->get();
		
            
			
		return view('orderdetails',compact('orderData')); 
	}
	
	
	
}
