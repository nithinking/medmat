<?php

namespace App\Http\Controllers\Pharmacy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\models\Healthprofile; 
use App\models\Deliveryaddress; 
use App\models\Gplocations; 
use App\models\Adminprofiletype;
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Mail;

class LoginController extends Controller
{
    public function index(){
    	return view('login');
		
    } 
	public function terms()
	{
		//return View::make('aboutus');
		return view('terms');
	}
	
	public function settings()
	{
		$user = Auth::user();
		$locationid = Adminprofiletype::where('userid', $user->userid)->value('locationid');
		return view('settings',  compact('locationid'));
	}
	public function login(Request $request)
	{		
		$v = Validator::make($request->all(), [
			'username' => 'required|string',
			'password' => 'required|string',
		]);
		if ($v->fails()) {
			return redirect()->back()->withErrors($v->errors())->withError('check Your Inputs');
		}
		else
		{
			// if(!User::where('email', '=', $req->email)->orWhere('userName',$req->email)->exists())
			// {
				// return redirect()->back()->withError('username does not exist ');
			// }
			// else
			// {
			if(Auth::attempt(['username' => request('username'), 'password' => request('password') ,'profiletypeid' => array('3', '7'),'userstatus' => '1'])){ 
			   $user = Auth::user();
			   $success['token'] =  $user->createToken('AppName')-> accessToken; 
			   $success['user_details']=$user;
			  // print_r($user->firstname);exit;
			   return view('getallorders');
			   return response()->json(['usersinfo' => $user], $this-> successStatus); 
			  } else{ 
			  //return view('index');
			  return redirect()->back()->withError('Incorrect Username/Password ');
			  }	
			//}			  
		}
		   
		
	}
	
	public function logout(Request $request) {
		  Auth::logout();
		  return redirect('/');
		}
		
	public function forgot(Request $req)
	{
		return view('forgot');
	}
	public function forgetPassword(Request $req)
	{	
		
		/*$users_existance= User :: where('username','=',$req->username)																						
									->where('userstatus','=',1)
									->where('profiletypeid','=',3)
									->orWhere('profiletypeid','=',7)
									->get();*/
		$users_existance=DB::select("select * from users where username='".$req->username."' and userstatus=1 and profiletypeid in(3,7)");							
	//print_r($req->username);
	//print_r($users_existance);exit;
			if(count($users_existance) > 0)
			{				
			if($users_existance[0]->username != $req->username)
			{
				return redirect()->back()->withError('Email does not exist');
			}
			else
			{
				$link=url('resetpass/'.$users_existance[0]->hashcode);
				$subject="Medmate – Forgot your password";
				$content = " <a href='".$link."'><button style='width: 160px;height: 40px;background-color: #3ea6bf;border: 0px;color: white;font-size: medium;margin-left:280px;border-radius: 4px;margin-top: 30px;'>Reset Password</button></a>";
				$body_message = trans('messages.Forgot_password_email');
				$body_message 	= 	str_replace('Reset_password_button',$content,$body_message);
				$message['to']           = $users_existance[0]->username;
                $message['subject']      = $subject;
                $message['body_message'] = $body_message;
				try{
				Mail::send([], $message, function ($m) use ($message)  {
               $m->to($message['to'])
                  ->subject($message['subject'])
                  ->from('medmate-app@medmate.com.au', 'Medmate')
               ->setBody($message['body_message'], 'text/html');
               }); 
               }catch(\Exception $e){
                 // echo 'Error:'.$e->getMessage();
                 // return;
               }
			   return back()->with('success_message','Success! Email Sent Successfully.');
			  
		}
		}
			else
			{
				return redirect()->back()->withError('Email does not exist');
			}
		
	}
}
