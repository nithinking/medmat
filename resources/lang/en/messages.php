<?php

return [
    /* Email Template */
	 

'Welcome_Email' => '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5</title>
</head>
<style>
.one {
 width: 100%;background-color: #e7f5f8;height: 100%;min-height:600px;padding-top: 60px;
}
.two {
  width: 700px;height:300px;background-color: white;margin-left: 30%;margin-top: 0px;
}    

</style>
<body>
    <div class="one">
       <div class="two">
	   
          <img src="https://pharmacy.medmate.com.au//logo.jpg" alt="" width="240px" height="50px" style="margin-left: 240px;">
           <p style="font-size: x-large;color: #3ea6bf;margin-left: 220px;">  Welcome to Medmate</p>
           <hr width="500px">
           <div style="width: 500px;margin-left: 100px;height: 60px;">
               <p style="text-align: center;">
					Instructions on how to use

               </p>
           </div>
          
       </div>
	    <div class="two">
		<p>If you have any questions, please email enquries@medmate.com.au </p>
		</div>
    </div>
</body>
</html>', 

'Order_Placed_Template' =>'<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>4</title>
</head>
<style>
.one {
 width: 100%;background-color: #e7f5f8;height: 100%;padding-top: 60px;
}
.two {
  width: 450px;height:400px;background-color: white;margin-left: 35%;margin-top: 0px;
}    
.three {
    width: 450px;height:250px;margin-left: 35%;margin-top: 0px;padding-left: 30px;font-size: large;color: #777777;
}
</style>
<body>
    <div class="one">
       <div class="two">
          <img src="logo.png" alt="" width="240px" height="50px" style="margin-left: 120px;">
          <p style="color: #49aed1;font-size: x-large;margin-top: 0px;margin-left: 150px;">New order placed</p>
          <p style="margin-left: 100px;font-size: x-large;color: #777777;margin-top: 30px;">Hi pharmacy.</p>
          <p style="font-size: x-large;color: #777777;margin-top: 30px;margin-left: 30px;">New order placed with order no #orderid </p>
          <p style="font-size: x-large;color: #777777;margin-left: 90px;margin-bottom: 40px;">Use the below link to review</p>
          button 
       </div>
       <div class="three">
        <p style="margin-bottom: 0px;margin-top: 20px;">Pocket Health Pty Ltd</p>
        <p>All Rights Reserved 2018</p>
        <p></p>
        <p>You are receiving this email because you registered <br>
        with the <span> myMedkit APP  in itunes or Google Play</p>
        with the <span style="background-color: #fef300;"> myMedkit </span> APP  in itunes or Google Play</p>
        <p>Store</p>
       </div>
    </div>
</body>
</html>',


'Forgot_password_email' => '<table border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td width="601" valign="top">
                <p>
                    <img 
                        width="576"
                        height="101"
                        src="https://pharmacy.medmate.com.au//logo.jpg" alt="Medmate"   />
                </p>
            </td>
        </tr>
        <tr>
            <td width="601">
                <p align="center">
                    <strong>Forgot your password?</strong>
                </p>
                <p align="center">
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="601" valign="top">
                <p>
                    Forgot your password?
                </p>
                <p>
                    No problem, click the link below and create a new password.
                </p>
                <p>
                    <strong><u></u></strong>
                </p>
                <table cellpadding="0" cellspacing="0" align="left">
                    <tbody>
                        <tr>
                            <td width="224" height="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td> Reset_password_button
                            </td>
                        </tr>
                    </tbody>
                </table>
                <u></u>
                <p>
                    <u></u>
                </p>
                <br clear="ALL"/>
                <p>
                    Didn’t ask to reset your password? No worries, ignore this
                    email and we’ll keep it the same
                </p>
                <p>
                    If you have any questions, please email
                    <a href="mailto:enquries@medmate.com.au">
                        enquries@medmate.com.au
                    </a>
                </p>
            </td>
        </tr>
        <tr>
            <td width="601" valign="top">
                <p align="center">
                    Medmate Australia Pty Ltd. ABN: 88 628 471 509
                </p>
                <p align="center">
                    Empowering Healthcare Consumers with Choice, Value, &amp;
                    Convenience
                </p>
                <p align="center">
                    medmate.com.au
                </p>
            </td>
        </tr>
    </tbody>
</table>',

'Pharmacy_neworder' => '',

'update_profile' =>  'User profile Information added successfully',
'general_messgae' =>  'Please fill all required fields',
'logout_messgae' =>  'Successfully logged out',
'user_feature_messgae' =>  'This user doesnt have an access to this feature.',
'Remove_delivery_address' =>  'Address Removed.',
'succ_message' =>  'Success',
'failure_message' =>  'Failure',
'no_data' =>  'No data found',
'required_message' =>  'Required data missing',
'price_computed' =>  'Price computed successfully',
'prescription_Add' =>  'Prescription details added',
'prescription_err' =>  'Please fill  fields based on the selected script type',
'remove_prescription' =>  'Script Removed from list',
'add_medlist' =>  'Medication added to your list',
'remove_medlist' =>  'Medication Removed from list',
'repeats_edit_medlist' =>  'Repeats Updated in Medlist',
'email_not_exist' =>  'Email does not exist.',
'email_sent' =>  'Success! Email Sent Successfully.',
'doordasherrmsg' =>  'Issue encountered, please retry. If issue persists contact support. ',


]
?>