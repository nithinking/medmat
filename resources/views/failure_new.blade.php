<!DOCTYPE html>
<html lang="en">
<head>
  <title>Medmate</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
  .container {
  font-family: "Sfpro" !important;
  font-size:15px !important;
  }
  .container p{
      color:#323643 !important;
  }
  </style>
<body>

<div class="container">
<div class="page-header" style="background:#1c9ad5;text-align:center;padding: 5px;color: #ffffff;margin: 11px 0 20px !important;">
  <h2>Payment Status</h2></div>
  
  
  <!-- Left-aligned media object -->
    
  <!-- Right-aligned media object -->
  <div class="media">
    <div class="media-body">
      <h4 class="media-heading" style="color:#c81c1a;">Payment Rejection</h4>
      <p>We are sorry but the payment did not go through and will not be able to proceed with the order .</p>
      <p>Please do reach out to the pharmacy directly.</p>
      <p>Regert the inconvenience</p>
    </div>
    </div>
</div>

</body>
</html>
