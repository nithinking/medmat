<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <style>
        body {
            font-family: 'Roboto';
        }
        .center {
  margin: auto;
  width: 40%;

  padding: 10px;
}
    </style>
</head>
<body>
    <div class="center" style="margin-top: 10%;">
        <p><img src="{{ asset('lgo-pharmacy.png') }}" alt="" width="280px" height="60px"></p> <br>
        <p style="font-size: larger;"> <b> Enter your new password.</b></p>
		<form id="forgotform" action="{{route ("resetPassword")}}" class="form-vertical forgot-form" action="" method="post" onsubmit="return ValidateResetForm();">
		@csrf
        <input id="first" name="pass" type="password" style="width: 400px;height:37px;border: 1px solid #999999;border-radius: 3px;" placeholder="  Enter your new password"> <br> <br>
        <input id="second" name="repass" type="password" style="width: 400px;height: 37px;border: 1px solid #999999;border-radius: 3px;" placeholder="  Please confirm your password"> <br> <br>
        <input type="hidden" name="token" value="{{$tempPass}}">
		<button style="width: 410px; height: 37px;border-radius: 5px;border: 0px solid;"  onclick="myFunction()">Change Password</button>
        <br>
        <p id="demo" style="color: #07ad07;margin-left: 60px;margin-top: 30px;"></p>
        <p id="emo" style="color: #FF9494;margin-left: 130px;margin-top: 30px;"></p>
      </div>
     
      <script>
        function myFunction() {
            var firstInput = document.getElementById("first").value;
            var secondInput = document.getElementById("second").value; 
            if (firstInput === secondInput && firstInput != '') {
                document.getElementById("demo").innerHTML = "Your password has changed Successfully";
            }           
            else {
                document.getElementById("emo").innerHTML = "Something went wrong";
            }
        }
        </script>
</body>
</html>