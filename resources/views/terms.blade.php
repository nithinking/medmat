<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>Medmate :: Privacy & Security</title>
</head>
<style>
.header {
    width: 100%;
    text-align: center;
}
.content {
    margin-left: 70px;margin-top: 30px;align-content: space-around;width: 1200px;
}
.footer {
    background-color: #fff;
    height: 120px;
}

</style>

<body>
<div class="container">
    <nav class="navbar navbar-light  d-flex justify-content-center align-items-center" style="width: 100%;background-color: white;">
       <img height="100" width="300" src="https://medmate.com.au/wp-content/uploads/2018/12/medmate_logo_main-01-300x67.png" alt="Medmate" title="" style="max-height: 88px;">
      </nav>
    <div class="col-xs-12 col-sm-6 col-md-8" style="margin-left: 80px;margin-top: 30px;">
    <h2>Terms & Conditions.</h2>

		<div>
			<p><h3>MEDADVISOR END USER LICENCE AGREEMENT</h3></p>
			<p>By clicking on the 'Accept' button or otherwise downloading, accessing or using the Software or continuing to receive the MedAdvisor Service, you agree with MedAdvisor to be bound by this Agreement.</p>
			<p>Please read the following MedAdvisor End User Licence Agreement carefully before continuing. This is a legal contract between MedAdvisor International Pty Ltd (<b>'MedAdvisor'</b> or <b>'we'</b>) and you.</p>
			<p>In this Agreement, <b>'Patient'</b> means the person receiving the benefit of the Software and/or the MedAdvisor Services and <b>'you'</b> means all person(s) downloading, accessing, using and/or receiving the benefit of the Software and the MedAdvisor Services, including but not limited to the Patient and persons who use the Software and the MedAdvisor Services for the benefit of the Patient.</p>
			<p>If you are using the Software and/or the MedAdvisor Service for the benefit of a Patient, you must ensure that the Patient has expressly authorised such usage and has also agreed to be bound by this Agreement. Conversely, if you are a Patient who has authorised for a user to use the Software and/or the MedAdvisor Service for your benefit, you must ensure that the user has also agreed to be bound by this Agreement. </p>
			<p>The terms in this Agreement are subject to, and will not apply to the extent that they limit or exclude any rights and remedies you have under the Australian Consumer Law (as defined in clause 7) which cannot be lawfully excluded, restricted or modified (<b>'Non-Excludable Rights'</b>).</p>
			<p>MedAdvisor is willing to license the software provided by it to you, whether by download, via the Internet, as part of a device or piece of equipment, or on software media, including all databases, data, and documentation contained therein or provided therewith, including any subsequent updates (the <b>'Software'</b>) to you only upon the condition that you accept all of the terms and conditions contained in this Agreement.</p>
			<p>To the extent permitted by law, this Agreement, together with the MedAdvisor Privacy Policy (<b>'Privacy Policy'</b>) constitute the entire agreement between you and MedAdvisor in relation to the Software and the related services provided via or in connection with the Software (<b>'MedAdvisor Service'</b>). Our Privacy Policy details the types of personal information we collect, who can access the information and the purposes for which it may be used. MedAdvisor takes the privacy of patient and pharmacy data very seriously. A copy of the latest version of our Privacy Policy can be found at:<a href="www.medadvisor.com.au/privacy" target="_blank"> www.medadvisor.com.au/privacy. </a></p>
			<p>By clicking on the 'Accept' button, or otherwise downloading, accessing or using the Software or continuing to receive the MedAdvisor Service (such as SMS reminders), you accept all of the terms and conditions of this Agreement and the Privacy Policy and agree to be bound by their terms. If you do not accept the terms of this Agreement, or the Privacy Policy, you are not permitted to use the Software or MedAdvisor Service. Please click the 'Cancel' button or opt-out (where applicable) and do not download, access or use the Software.</p>
			<p>You are also deemed to have accepted the terms of this Agreement if:</p>
			<p>a copy of the terms of this Agreement are made available to you if requested by you at a participating MedAdvisor pharmacy and you consent to your pharmacist signing you up to the MedAdvisor Service; or
</p>
			<p>we provide you with a copy of the Agreement via email or a hyperlink to the terms of this Agreement via SMS and you either notify MedAdvisor that you accept the terms of this Agreement or continue receiving and/or having the benefit of the MedAdvisor Service after receiving such notice.
</p>
			<p>MedAdvisor reserves the right to offer you optional additional services via the MedAdvisor Service relevant to the MedAdvisor Service and other health services, including:</p>
			<p>additional information and training about your medication and health condition;</p>
			<p>services offered by your chosen pharmacy, doctor or other health care provider; and
information regarding joining a clinical research program.
</p>
			<p>You acknowledge that the information you may receive in relation to the above examples will be matched and targeted to your condition or drug profile. MedAdvisor will not disclose any identifiable personal information about you to any third party providers of additional services unless you have provided express consent for us to do so. You may also opt out of the additional services at any time. Please contact us at the contact details provided in our Privacy Policy at <a href="www.medadvisor.com.au/privacy" target="_blank">www.medadvisor.com.au/privacy</a> to opt out.</p>
			<p><h3>1. Licence to use the Software</h3></p>
			<p>MedAdvisor grants, and you hereby accept, a non-exclusive, non-transferable, revocable licence to use the Software on the terms and conditions set forth in this Agreement. The terms of this Agreement apply to this or any subsequent version of the Software. If the Software is provided by remote download or any other means, you may make one copy of the Software in machine-readable form for backup purposes only. The backup copy must include all copyright information contained on the original. If the Software is provided via the Internet, then the licence herein includes a licence to access servers controlled by MedAdvisor ('Servers') only for the purpose of accessing the Software.</p>
			<p>You may not do any of the following yourself, or through any third party, and you may not permit any third party with whom you have a business or personal relationship to do any of the following: (A) copy the Software, except as expressly set forth above; (B) modify or create derivative works based upon the Software; (C) decompile, disassemble, or reverse engineer the Software in whole or in part; (D) defeat, disable, or circumvent any protection mechanism related to the Software; (E) sell, license, sublicense, lease, rent, distribute, disclose, permit access to, or transfer to any third party, whether for profit or without charge, any portion of the Software, or, in particular, without limiting the generality of the foregoing, distribute the Software on any media; make the Software accessible to the public or third parties, whether over networks, electronic bulletin boards, websites, or otherwise; or allow any third party to use the Software; (F) export, re-export, download, or otherwise use the Software in violation of any laws or regulations; or (G) solely rely on the Software in connection with any critical application, which includes any medical application, or any application where failure or malfunction could lead to injury to persons or loss of life or catastrophic property damage. MedAdvisor is a technology tool and a communications platform which is offered subject to the disclaimers in this Agreement, accordingly, you should always have contingencies in place rather than rely solely on the Software. Subject always to your Non-Excludable Rights, should you decide to solely rely on the Software for any critical application, MedAdvisor disclaims any and all liability arising out of the use of the Software and you assume full responsibility for such use.</p>
			<p><h3>2. GP Link</h3></p>
			<p><u>Definitions</u></p>
			<p>In this clause, capitalised terms have the following meanings:</p>
			<p><b>"Accepted Script Request"</b> means a Script Request to which your Nominated Practitioner confirms they will make a Script available to you.</p>
			<p><b>"Collection Address"</b> means the address nominated by you in a Script Request for collection of the requested Script being either the residential address nominated by you in the MedAdvisor platform, the Nominated Address of your Nominated Practitioner or the Nominated Address of your Nominated Pharmacy.</p>
			<p><b>"GP Link Services"</b> means the use of and access to the GP Link System, the Script Delivery Service, and such other services as are subsequently specified by MedAdvisor in writing to be 'GP Link Services'.</p>
			<p><b>"GP Link System"</b> means the Software enabling interaction between you and your Nominated Practitioner and the delivery of the GP Link Services.</p>
			<p><b>"Nominated Address"</b> means the business address specified by a Nominated Pharmacy or a Nominated Practitioner (as applicable).</p>
			<p><b>"Nominated Pharmacy"</b> means the pharmacy nominated by you in the MedAdvisor platform.</p>
			<p><b>"Nominated Practitioner"</b> means the general medical practitioner nominated by you as having an existing doctor-patient relationship with you, or a medical practitioner that you nominate to provide the Script Delivery Service.</p>
			<p><b>"Script"</b> means a script provided by your Nominated Practitioner (which references the Nominated Practitioner's Medicare provider number), authorising you to purchase specified quantities of specified pharmaceutical products from a pharmacy.</p>
			<p><b>"Script Delivery</b> Service" means the service by which your Nominated Practitioner provides you with a Script in return for the Script Fee.</p>
			<p><b>"Script Fee"</b> means the fee payable by you in consideration for the provision of a Script in accordance with a Script Request.</p>
			<p><b>"Script Request"</b> means a request submitted via the GP Link System by you to your Nominated Practitioner to make a Script available.</p>
			<p><b>"Service Fee"</b> means the fee to be retained by MedAdvisor in respect of any Accepted Script Request.</p>
			<p><u>GP Link Services</u></p>
			<p>The terms and conditions in this clause apply to the GP Link Services, including the Script Delivery Service.</p>
			<p>Before you finalise your Script Request, MedAdvisor will notify you of the amount of the Script Fee.</p>
			<p>If you submit a Script Request, MedAdvisor will transmit that Script Request to your Nominated Practitioner via the GP Link System. Upon receipt of a Script Request, your Nominated Practitioner may elect to either decline or agree to fulfil the Script Request.</p>
			<p>If your Nominated Practitioner agrees to fulfil a Script Request:</p>
			<p>you have a direct contractual relationship with your Nominated Practitioner to pay the Script Fee in consideration for the Nominated Practitioner making the requested Script available for collection by you at the Collection Address within 7 days; and</p>
			<p>you acknowledge that:</p>
			<p>MedAdvisor will collect the Script Fee from you on behalf of your Nominated Practitioner using the details provided by you in the payment authorisation completed at the time you elected to first utilise the Script Delivery Service, or such other direct debit or credit card authorisation provided by you at the time of submitting the Script Request;</p>
			<p>MedAdvisor will electronically deliver to you a receipt for payment of the Script Fee;</p>
			<p>MedAdvisor will retain a Service Fee from such Script Fee; and
if MedAdvisor is unable to collect the Script Fee from you, your Nominated Practitioner (and/or the medical practice within which they conduct their business) has a legal right to recover such amount directly from you, and you may not take any action against MedAdvisor, its agents, employees or contractors for any loss or damage incurred by you as a result of such recovery action.
</p>
			<p>If your Nominated Practitioner elects to decline a Script Request, or if your Nominated Practitioner neither declines or agrees to fulfil the Script Request within 4 days of submission of the Script Request, MedAdvisor will notify you via the GP Link System that the Script Request has been declined.</p>
			<p>You acknowledge and agree that the Script Delivery Service is only available, and you may only submit a Script Request if you have:</p>
			<p>completed and submitted to MedAdvisor valid payment authorisation details, and have not revoked such payment authorisation details; and</p>
			<p>consented to the provision of your medical adherence history as recorded in MedAdvisor to your Nominated Practitioner, and have not revoked such consent.</p>
			<p><h3>3. Modification of the MedAdvisor Services or termination of this Agreement</h3></p>
			<p>MedAdvisor reserves the right to modify the MedAdvisor Services and/or product offerings in connection with the Software, at any time without notice to you.</p>
			<p>You may terminate this Agreement at any time by notifying MedAdvisor in writing.</p>
			<p>MedAdvisor may at its sole discretion terminate this Agreement in the event you fail to comply with the terms and conditions of this Agreement, by deactivating the Software or suspending operation of the MedAdvisor program.</p>
			<p>MedAdvisor otherwise reserves the right to terminate this Agreement, any or all of the MedAdvisor Services and/or any product offerings in connection with the Software, at any time by giving reasonable notice to you. </p>
			<p><h3>4. Copyright and Restrictions</h3></p>
			<p>The Software, including all text and other content therein ('Content') is the property of MedAdvisor or its licensors, and is protected by copyright and other intellectual property laws. Except for the rights expressly granted above, this Agreement transfers to you no right, title, or interest in the Software, or the Content or any copyright, patent, trademark, trade secret, or other intellectual property or proprietary right therein.</p>
			<p>MedAdvisor retains sole and exclusive title to all portions of the Software, Content and animation, and any copies thereof, and you hereby assign to MedAdvisor all right, title, and interest in and to any modifications you make to the Software, whether or not such modifications are permitted. None of the Content or animation may be reproduced, transcribed, stored in a retrieval system, translated into any spoken language or computer language, retransmitted in any form or by any means (electronic, mechanical, photocopied, recorded, or otherwise), resold, or redistributed without the prior written consent of MedAdvisor, except that you may reproduce limited excerpts of Content or animation obtained from MedAdvisor sources or databases for personal use only, provided that each such copy contains a copyright notice as follows:</p>
			<p>'© 2014 MedAdvisor International Pty Ltd. All rights reserved.'</p>
			<p>For Content obtained from third-party licensors, you are solely responsible for compliance with any copyright restrictions and is referred to the publication data appearing in bibliographic citations, as well as to the copyright notices appearing in the original publications.</p>
			<p><h3>5. Content</h3></p>
			<p>While great care has been taken in organizing and presenting the Content, MedAdvisor does not warrant or guarantee the Content's correctness, accuracy, or timeliness.</p>
			<p>MedAdvisor does not perform any independent analysis or investigation of any of the Content. MedAdvisor does not assume, and expressly disclaims, any obligation to obtain and include any information other than that provided in the Content. It should be understood that by making this material available, MedAdvisor is not endorsing or advocating the use of any product or procedure described in the Content, nor is MedAdvisor responsible for misuse of a product or procedure due to, or other consequence of, any typographical error or other inaccuracy.</p>
			<p><h3>Use of Professional Judgment</h3></p>
			<p>The editors and authors have conscientiously and carefully tried to present the Content in conformance with the standards of professional practice that prevailed at the time of Content publication. However, such standards and practices may change as new data becomes available, and you should consult a variety of sources. MedAdvisor’s Content is provided for information purposes only. The Content is no substitute for the advice of a qualified health care provider. Always seek and never disregard the advice of your physician or other qualified healthcare providers regarding a medical condition.</p>
			<p>MedAdvisor is not a medical organization and cannot give you or the Patient medical advice or diagnose conditions. If a Patient experiences any pain or exacerbation or has a change in medical condition, Patient should immediately seek appropriate medical attention. If the Patient has a pre-existing medical condition, they must consult with their doctor prior to using MedAdvisor.</p>
			<p>You should never disregard medical advice or delay in seeking it because of information you have received from MedAdvisor.</p>
			<p>All characters or names represented throughout the MedAdvisor Software are fictitious and do not resemble or depict real persons or characters. Any resemblance to real persons, living or dead, is purely coincidental.</p>
			<p>The methods of treatment may change as new information is available, and the information provided by MedAdvisor does not necessarily represent the most current practices or methods in medication use.</p>
			<p><h3>Consumer Medicines Information ('CMI')</h3></p>
			<p>CMIs are sourced from pharmaceutical manufacturers. You acknowledge that MedAdvisor is only a distributor channelling CMIs to you and cannot guarantee the accuracy of the content in the CMI.</p>
			<p><h3>6. Protection and Security</h3></p>
			<p>You must take all reasonable steps to ensure that no unauthorized person shall have access to the Software or their medical information. The Software and Content are the valuable property of MedAdvisor, the unauthorized use or disclosure of which would irreparably harm MedAdvisor. Upon learning of any unauthorized possession or use of or access to the Software, you will notify MedAdvisor, will promptly furnish details of such occurrence, will assist in preventing any recurrence thereof, and will cooperate fully in any litigation or other proceedings undertaken to protect MedAdvisor's rights.</p>
			<p><h3>7. Australian Consumer Law and limitation of liability</h3></p>
			<p>In this Agreement, <b>"Australian Consumer Law"</b> means Schedule 2 of the Competition and Consumer Act 2010 (Cth) and the corresponding provisions of state and territory fair trading legislation and the terms "Consumer" and Consumer Guarantees" have the meaning given to them in the Australian Consumer Law. </p>
			<p><b>The Australian Consumer Law provides Consumers with a number of protections including the Consumer Guarantees that cannot be excluded, restricted or modified. Nothing in this Agreement has the effect of excluding, restricting or modifying a Consumer's rights under the Australian Consumer Law or any other statutory rights which cannot be excluded, restricted or modified.</b></p>
			<p>To the full extent permitted by law and except to the extent that the Consumer Guarantees cannot be excluded as set out in the paragraph below or otherwise, MedAdvisor's liability to you in connection with a breach of or failure to comply with a Consumer Guarantee is limited to: (a) in the case of goods, repairing, replacing or supplying equivalent goods, or paying the cost of any of those remedies to you; or (b) in the case of services, supplying the services again or paying the cost of having the services supplied again.</p>
			<p>MedAdvisor's liability in respect of a breach of or a failure to comply with an applicable Consumer Guarantee will not be limited in the way set out in the above paragraph if:</p>
			<p>the goods or services are 'of a kind ordinarily acquired for personal, domestic or household use or consumption', as that expression is used in section 64A of the Australian Consumer Law;</p>
			<p>it is not 'fair or reasonable' for MedAdvisor to rely on such limitation in accordance with section 64A(3) of the Australian Consumer Law; or</p>
			<p>the relevant Consumer Guarantee is a guarantee pursuant to sections 51, 52 or 53 of the Australian Consumer Law.</p>
			<p><h3>8. Disclaimers and other limitations of liability</h3></p>
			<p>The terms in this clause 8 are subject to, and will not apply to the extent that they limit or exclude any rights and remedies you have under the Australian Consumer Law which cannot be lawfully excluded, restricted or modified (<b>'Non-Excludable Rights'</b>).</p>
			<p>To the extent permitted by law and subject to your Non-Excludable Rights (see clause 7), MedAdvisor will not be liable for any loss or damage incurred by you arising from your use of the Software, MedAdvisor Service or Content.</p>
			<p>MedAdvisor is a technology tool and a communications platform between a pharmacy and a MedAdvisor Software user. MedAdvisor disclaims any responsibility for any third party communications, including communications between you and a pharmacy, misconfiguration of the MedAdvisor Software by a pharmacy and CMIs.</p>
			<p> You expressly acknowledge and agree that use of the Software and the Content are at your sole risk. MedAdvisor does not directly or indirectly practice medicine or dispense medical services and subject to your Non-Excludable Rights, assumes no liability for the Content or any results obtained from the use of such information. The information contained in the Software is not intended in any way to be used as medical advice or to replace medical advice offered to you by health care professionals familiar with the Patient's condition. We recommend that you consult with a qualified health care provider prior to using MedAdvisor. You assume full responsibility for the appropriate use of Content contained in the Software and agrees to hold MedAdvisor, and its third party providers, harmless from any and all claims or actions arising from your use of the Software or the Content.</p>
			<p>TO THE EXTENT PERMITTED BY LAW AND SUBJECT TO YOUR NON-EXCLUDABLE RIGHTS (SEE CLAUSE 7), MEDADVISOR ASSUMES NO RESPONSIBILITY FOR PERSONAL INJURY AND/OR DAMAGE WHICH MAY BE INCURRED DURING OR AFTER USING THE MEDADVISOR SOFTWARE.</p>
			<p>The MedAdvisor Software and the Content are provided 'as is', with all faults and no representations or warranties of any kind, either express or implied.</p>
			<p>MedAdvisor does not warrant:</p>
			<p>against interference with your enjoyment of the MedAdvisorSoftware;</p>
			<p>that the functions contained in, or services performed or provided by, the MedAdvisor Software will meet your requirements;</p>
			<p>that the operation of the Software will be uninterrupted or error-free;</p>
			<p>that any service will continue to be made available;</p>
			<p>that defects in the Software will not prevent you from receiving reminder prescriptions;</p>
			<p>that defects in the Software or services will be corrected; or</p>
			<p>that the Software will be compatible or work with any third party Software, applications or third party services.</p>
			<p>If you choose to store information using our Software, you must keep a copy of the information elsewhere as external factors outside our control (such as technical glitches) could result in content stored on our Software being lost. It is your responsibility to use the Software on a device that is compatible with the Software. MedAdvisor will not be liable for any loss or damage incurred by you arising from your use of the Software on a device that is not compatible with the Software.</p>
			<p>Furthermore, MedAdvisor does not warrant or make any representation regarding the use or the results of the use of the Software (including the related documentation) or the Content in terms of their correctness, accuracy, reliability, or otherwise.</p>
			<p>*TO THE EXTENT PERMITTED BY LAW AND SUBJECT TO YOUR NON-EXCLUDABLE RIGHTS (SEE CLAUSE 7), MEDADVISOR EXPRESSLY DISCLAIMS ANY WARRANTIES, WRITTEN OR ORAL, EXPRESS OR IMPLIED, OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE OR NONINFRINGEMENT WITH RESPECT TO THE SOFTWARE OR THE CONTENT.*</p>
			<p>The Software may be provided with third party plug-ins or other third party software, or the Software may be provided as a plug-in for, or otherwise in association with, third party software. Use of any such third party software will be governed by the applicable licence agreement, if any, with such third party.</p>
			<p>*MEDADVISOR IS NOT RESPONSIBLE FOR ANY THIRD PARTY SOFTWARE AND WILL HAVE NO LIABILITY OF ANY KIND FOR YOUR USE OF SUCH THIRD PARTY SOFTWARE AND MAKES NO WARRANTY OF ANY KIND WITH RESPECT TO SUCH THIRD PARTY SOFTWARE.*</p>
			<p>No salesperson or other representative of any party involved in the distribution of the Software is authorized to make any warranties with respect to the Software or Content beyond those contained in this Agreement. Unauthorized oral statements do not constitute warranties, shall not be relied upon by you and are not a part of this Agreement.</p>
			<p>*TO THE EXTENT PERMITTED BY LAW AND SUBJECT TO YOUR NON-EXCLUDABLE RIGHTS (SEE CLAUSE 7), , MEDADVISOR AND ANY OTHER PARTY INVOLVED IN THE CREATION, PRODUCTION, PROMOTION, OR MARKETING OF THE SOFTWARE, OR THE CONTENT, IS NOT LIABLE (INCLUDING IN NEGLIGENCE) TO ANY OTHER PARTY FOR ANY INCIDENTAL, SPECIAL, INDIRECT, RELIANCE, PUNITIVE OR CONSEQUENTIAL DAMAGES, INCLUDING LOST DATA, BUSINESS INTERRUPTION, LOSS OF USE, LOST REVENUE, OR LOST PROFITS, ARISING OUT OF OR RELATING TO THIS AGREEMENT OR THE SOFTWARE, THE CONTENT, OR THE SERVERS, EVEN IF MEDADVISOR OR SUCH OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.*</p>
			<p><h3>9. Your Favourite Pharmacy</h3></p>
			<p>Your Favourite Pharmacy means your Nominated Pharmacy (as defined in clause 2).</p>
			<p>MedAdvisor may collect personal information about you (such as dispense data) when you deal with any participating MedAdvisor network pharmacies. You consent to MedAdvisor disclosing such personal information, and the information we collect about you via the MedAdvisor Service, to your Favourite Pharmacy. For example, if you have not initiated certain MedAdvisor functionality, such as 'Order', we may notify your Favourite Pharmacy.</p>
			<p>If you elect to change your Favourite Pharmacy in the future, you consent to MedAdvisor disclosing your complete dispense history held by MedAdvisor to that pharmacy.</p>
			<p>You also consent to your Favourite Pharmacy sending you promotional offers via the MedAdvisor Service. You may opt out of receiving such promotional offers at any time. Please contact us at the contact details provided in our Privacy Policy at www.medadvisor.com.au/privacy to opt out. Note that if you want to opt out of receiving promotional offers that you receive from the pharmacy outside of the MedAdvisor Service, you will need to contact the pharmacy directly to unsubscribe from that pharmacy's promotional offers (as this is a matter between you and your pharmacy).</p>
			<p><h3>10. General</h3></p>
			<p>If for any reason any provision of this Agreement is determined to be invalid or unenforceable under any statute or rule of law by a court of competent jurisdiction, such provision shall be interpreted in order to give effect to such provision to the maximum extent permitted by law, and the remaining provisions shall continue in full force and effect.</p>
			<p>This Agreement constitutes the entire agreement between the parties with respect to the subject matter hereof and supersedes all prior agreements, oral or written, and all other communications relating to the subject matter hereof. No amendment or modification of any provision of this Agreement will be effective unless set forth in a document that purports to amend this Agreement and both parties accept that hereto.</p>
			<p>This Agreement is personal to you and you may not assign your rights or obligations to anyone. If you have any questions about MedAdvisor's Policies, please contact us via our website: www.medadvisor.com.au.</p>
			<p><h3>11. Additional terms</h3></p>
			<p>You must comply with any third party terms of agreement applicable to you when using the Software and/or MedAdvisor Services.</p>
			<p>If you obtained the Software from the Apple Inc. (<b>Apple</b>) App Store, you and MedAdvisor acknowledge and agree that (to the full extent permitted by applicable law):</p>
			<p>this Agreement is concluded between you and MedAdvisor only, and not with Apple, and MedAdvisor, not Apple, is solely responsible for the Software and the content thereof;</p>
			<p>the license granted to you for the Software is a non-transferable license to use the Software on any Apple-branded products that you own or control and as permitted by the Usage Rules set forth in the App Store Terms of Service, except that such Software may be accessed and used by other accounts associated with the purchaser via family sharing or volume purchasing;</p>
			<p>Apple has no obligation whatsoever to furnish any maintenance and support services with respect to the Software, either under this Agreement or applicable law;</p>
			<p>in the event of any failure of the Software to conform to any applicable warranty, you may notify Apple, and Apple will refund the purchase price for the Software; and that, to the maximum extent permitted by applicable law, Apple will have no other warranty obligation whatsoever with respect to the Software, and any other claims, losses, liabilities, damages, costs or expenses attributable to any failure to conform to any warranty will be dealt with by MedAdvisor in accordance with this Agreement;</p>
			<p>MedAdvisor, not Apple, is responsible for addressing any claims by you or any third party relating to the Software or your possession and/or use of the Software, including but not limited to:</p>
			<p>product liability claims;</p>
			<p>any claim that the Software fails to conform to any applicable legal or regulatory requirement; and</p>
			<p>claims arising under consumer protection, privacy or similar legislation, including in connection with the Software's use (if applicable) of Apple's HealthKit and HomeKitframeworks;</p>
			<p>in the event of any third party claim that the Software or possession and/or use of the Software by you infringes that third party's intellectual property rights, MedAdvisor, not Apple, will be solely responsible for the investigation, defence, settlement and discharge of any such intellectual property infringement claim; and</p>
			<p>Apple, and Apple's subsidiaries, are third party beneficiaries of this Agreement and, upon your acceptance of this Agreement, Apple will have the right (and will be deemed to have accepted the right) to enforce the Agreement against you as a third party beneficiary thereof.</p>
			<p>You warrant and represent that:</p>
			<p>you are not located in a country that is subject to an embargo by the governments of the United States of America or the Commonwealth of Australia or of a country that has been designated by the United States of America or the Commonwealth of Australia as a "terrorist supporting" country; and</p>
			<p>you are not included on any list of prohibited or restricted parties by the governments of the United States of America or the Commonwealth of Australia.</p>
			<p><h3>12. Changes</h3></p>
			<p>We reserve the right to change this document from time to time. An up-to-date copy of this document is available at http://www.medadvisor.com.au/About/TermsConditions, we will notify you of the updated document and you will be deemed to accept such document in the manner contemplated at the introduction of this Agreement.</p>
			<p>The MedAdvisor End User Licence Agreement was last updated on 3 September 2019.</p>
		</div>
    </div>

<footer class="footer">
<div class="col-xs-12 col-sm-6 col-md-8" style="margin-left: 80px;margin-top: 30px;">
      <p style="float: right;"> &copy; copyrights acquired by medmate </p>
    </div>
</footer>
</div>
</body>
</html>