@extends('header')

@section('title','| Ready To Process')

@section('content')

            <div class="outside">

            <div class="table-responsive">
           <table class="table table-striped table-bordered" id="ordersinfo">
                <thead  class="one">
                 <tr>
                     <th scope="col">Received<!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Ref <!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Customer <!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Suburb<!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Order Type<!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Pick up/Delivery Date & Time</th>
					<th scope="col">AwaitingTime</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                
              </table>
              </div>
            
              
           
            </div>
         </div>
        </div>
        <script src="" async defer></script>
		<script>
        setInterval(function () { window.location.reload(true); }, 60000);
		 
		 // View User Datatable
    $(document).ready(function() {
      //  $.fn.dataTable.ext.errMode = () => showErrorMessage('Something went wrong');
        $('#ordersinfo').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('getallprocess') }}",
            
            "columns":
            [
               // { data: 'createddate', name: 'createddate' },  
			{
            data: 'createddate',
            name: 'createddate',
            type: 'date',
            render: function (data, type, row) { return data ? moment(data).format('DD-MM-YY HH:mm') : ''; }
        },    
				{ data: 'uniqueorderid', name: 'uniqueorderid' },
				{ data: 'customername', name: 'customername' },				
				{ data: 'suburb', name: 'suburb' },
				{ data: 'ordertype', name: 'ordertype' },
				{ data: 'userpreffereddate', name: 'deliverydatetime' },
				{data: 'awatingtime', name: 'awatingtime'},
				{ data: 'orderstatus', name: 'orderstatus' },
				{data: 'action', name: 'action', orderable: false, searchable: false},
				
            ],
			"order":[[0, 'desc']]
        });
    });
         </script>
     @endsection