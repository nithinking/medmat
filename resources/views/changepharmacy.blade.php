@extends('header')
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />-->
@section('title','| ChangePharmacy')

@section('content')


<div style="width: 100%;">
  		        
		  <div class="form-group clearfix">
  <div class="form-group">
    <label for="exampleInputPassword1">Pharmacy</label>
    <select name="pharmacy" id="pharmacy" class="form-control">
	<option value="">Select Location</option>
	@foreach($locations as $location)
	<option value="{{$location->locationid}}">{{$location->locationid}}-{{$location->locationname}}</option>
	@endforeach
	</select>
	<input type="hidden" class="form-control" name="orderid" id="orderid" value="{{$orderid}}">
	<input type="hidden" class="form-control" name="prevlocation" id="prevlocation" value="{{$prevlocationid}}">
  </div>
  <button  class="btn btn-primary" onclick="sendtopharmacy({{$orderid}},{{$prevlocationid}})">Submit</button>
  </div>                
 </div>
  <script src="" async defer></script> 
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.full.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script>
$(document).ready(function () {
      $("#pharmacy").select2();
  });
  function sendtopharmacy(orderid,prevlocation){
	  //alert(orderid);
	  locationid=$("#pharmacy").val();
	  if (confirm('Are you sure you want change pharmacy?')) {
		  $.ajax({
        type: 'post',              
        url: "{{ route('updatepharmacy') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "locationid": locationid,
		  "prevlocation":prevlocation,
          "orderid": orderid
        },    
        success: function(result){
			alert("Order Moved  to New Location Successfully");
            console.log(result);
			window.history.go(-1);
        },
        error: function(result){
			alert(result);
          console.log(result);
        }
      });
	  }
  }
</script>
@endsection
 