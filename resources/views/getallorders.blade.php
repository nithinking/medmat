@extends('header')

@section('title','| Inbox')

@section('content')
	
            <div class="outside">

            <div class="table-responsive" style="padding: 20px;">
            			  
			  <table class="table table-striped table-bordered" id="ordersinfo">
               <thead class="one">
                  <tr>
                     <th scope="col">Received<!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Ref <!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Customer <!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Suburb<!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Order Type<!--<i class="fa fa-chevron-down" style="color: #ffffff;padding-top: 3px;float:right;"></i>--></th>
                    <th scope="col">Pick up / Delivery Date & Time</th>
                    <th scope="col">Status</th>                    
					<th scope="col">Actions</th>
                  </tr>
               </thead>
            </table>
              </div>
            
              
           
            </div>
         </div>
        </div>
        <script src="" async defer></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
		<script src="//cdn.datatables.net/plug-ins/1.10.12/sorting/datetime-moment.js"></script>
		<script>
        
		 setInterval(function () { window.location.reload(true); }, 60000);
		 // View User Datatable
    $(document).ready(function() {
		$.fn.dataTable.moment('DD-MM-YY');
      //  $.fn.dataTable.ext.errMode = () => showErrorMessage('Something went wrong');
        $('#ordersinfo').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('allorders') }}",
            
            "columns":
            [
               // { data: 'createddate',name: 'createddate'}, 
			{
            data: 'createddate',
            name: 'createddate',
            type: 'date',
            render: function (data, type, row) { return data ? moment(data).format('DD-MM-YY HH:mm') : ''; }
			},                 
				{ data: 'uniqueorderid', name: 'uniqueorderid' },
				{ data: 'customername', name: 'customername' },				
				{ data: 'suburb', name: 'suburb' },
				{ data: 'ordertype', name: 'ordertype' },
				{ data: 'userpreffereddate', name: 'deliverydatetime' },
				{ data: 'currentorderstatus', name: 'currentorderstatus' },
				{data: 'action', name: 'action', orderable: false, searchable: false},
				
            ],
			columnDefs: [ { type: 'date', 'targets': [0] } ],
			"order":[[0, 'DESC']],
        });
		
    });
	
         </script>
    @endsection
      
         