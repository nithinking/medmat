<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Subject</title>
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"><!--<![endif]-->
        <style type="text/css">
        	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table{border-collapse:collapse !important;}
			body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

			body, #bodyTable{
				background-color:#f9f9f9;;
			}
			h1 {
				color: #1998d5 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:bold;
				line-height:100%;
				letter-spacing:normal;
				font-family: 'Roboto', Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 15px;
			}
			h2 {
				color: #000000 !important;
				font-size: 28px;
				font-style:normal;
				font-weight:500;
				line-height:100%;
				letter-spacing:normal;
				font-family: 'Roboto', Arial, Helvetica, sans-serif;
				margin-top: 0;
			    margin-bottom: 10px;
			}
			#bodyCell{padding:20px 0;}
			#templateContainer{width:500px;background-color: #ffffff;} 
			#templateHeader{background-color: #1998d5;}
			#headerImage{
				height:auto;
				max-width:100%;
			}
			.headerContent{
				text-align: center;
			}
			.bodyContent {
				text-align: center;
			}
			.bodyContent img {
			    margin: 0 0 25px;
			}
			.bodyContent .line {
				border-bottom: 1px solid #979797;
			}
			.preFooter {
				text-align: center;			
				font-size: 14px;
				line-height:100%;
				font-family: 'Roboto', Arial, Helvetica, sans-serif;	
			}
			.preFooter a {
				color: #1998d5 !important;
				text-decoration: none;
			}
			#templateFooter {width:500px;}
			.footerContent {
				text-align:center;
				font-family: 'Roboto', Arial, Helvetica, sans-serif;
				font-size: 12px;
				line-height: 18px;
			}
            @media only screen and (max-width: 600px){
				body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
				#bodyCell{padding:10px !important;}

				#templateContainer,  #templateFooter{
					max-width:500px !important;
					width:100% !important;
				}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        		<tr>
	                <td align="center" valign="top" id="bodyCell">
	                    <!-- BEGIN TEMPLATE // -->
                    	<table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                	<!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    	<tr>
                                			<td width="100%" height="24" style="height:24px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td valign="top" class="headerContent">
                                            	<img src="{{ asset('images/medmate-logo.png') }}" style="max-width:135px;" id="headerImage" alt="MedMate"/>
                                            </td>
                                        </tr> 
                                    	<tr>
                                			<td width="100%" height="20" style="height:20px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                            	<td>                            		
                                	<!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                                    	<tr>
                                			<td width="50" height="140" style="height:140px;width:50px;">&nbsp;</td>
                                			<td height="140" style="height:140px;">&nbsp;</td>
                                			<td width="50" height="140" style="height:140px;width:50px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                            <td valign="top" class="bodyContent">
                                            	<img src="{{ asset('images/success.png') }}" alt="" />
                                                <h1>Success!</h1>
												<h2>Your email has been verified.</h2>
												<div class="line">&nbsp;</div>
                                            </td>
                                			<td width="50" style="width:50px;">&nbsp;</td>
                                        </tr>
                                    	<tr>
                                			<td width="50" height="140" style="height:140px;width:50px;">&nbsp;</td>
                                			<td height="140" style="height:140px;">&nbsp;</td>
                                			<td width="50" height="140" style="height:140px;width:50px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END BODY -->
                            	</td>
                            </tr>
                            <tr>
                            	<td>
                                	<!-- BEGIN PREFOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreFooter">
                                    	<tr>
                                			<td width="100%" height="22" style="height:22px;">&nbsp;</td>
                                    	</tr>
                                        <tr>
                                            <td valign="top" class="preFooter">
                            					Have a question? <a href="" target="_blank">Contact Us</a>
                                            </td>
                                        </tr>
                                    	<tr>
                                			<td width="100%" height="22" style="height:22px;">&nbsp;</td>
                                    	</tr>
                                    </table>
                                    <!-- // END PREFOOTER -->
                            	</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->


                    	<!-- BEGIN FOOTER // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"> 
                        	<tr>
                    			<td width="100%" height="25" style="height:25px;">&nbsp;</td>
                        	</tr>
                            <tr>
                            	<td align="center" class="footerContent">
                            		© 2020 Medmate Pty Ltd
                            	</td>
                            </tr>
                        	<tr>
                    			<td width="100%" height="25" style="height:25px;">&nbsp;</td>
                        	</tr>
                        </table>
                        <!-- // END FOOTER -->
                    </td>
                </tr>
            </table>
        </center> 
    </body>
</html>