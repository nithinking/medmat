<!DOCTYPE html>
<html class="no-js"> 
    <head>	
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Medmate | @yield('title')</title>
        <meta name="description" content="">
		
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/totop.js') }}"></script>  
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
 		<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
        {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/plug-ins/1.10.21/sorting/date-de.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.22/sorting/datetime-moment.js"></script>
		<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
		<link href='https://fonts.googleapis.com/css?family=Avenir' rel='stylesheet'>
		
		
  
		<script type="text/javascript" src="{{ asset('js/jquery.fulltable.js') }}"></script>
		<link rel="stylesheet" href="{{ asset('css/jquery.fulltable.css') }}" />
		 
    </head>	
    <style>
	
	body {
            font-family: 'Avenir';
        }
    .one {
    background-color: #1c9ad6;
    color: #ffffff;
  }

  .table {
    width: 100%;
    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    font-size: 13px;
    background-color: #ffffff;
  }

  .revieworder {
    padding: 20px;
    background-color: white;
    border: 1px solid #f8f9fa;
  }

  .patientdetails {
    padding: 20px;
    background-color: #EBEBEC;
    border: 1px solid #f8f9fa;
    width: 100%;
  }

  h3,
  h6 {
    color: #079dd4;
  }

  .tot {
    display: flex;
    flex-direction: row;
    width: 100%;
  }

  .topbar {
    width: 100%;
    height: 60px;
    display: flex;
    flex-direction: row;
  }

  .orderbar{
    width: 100%;
    height: 30px;
    display: flex;
    flex-direction: row;
    background-color:#079dd4;
  }

  .titlebar {
    width: 100%;
    height: 80px;
    display: flex;
    flex-direction: row;
    margin-bottom: 8px;
    margin-top: 0px;
    background-color: #EBEBEC;
  }

  .table td {
    font-weight: 500;
    color: #4c4d4e;
  }

  .scriptbar {
    width: 100%;
    height: auto;
    color: #079dd4;
    display: flex;
    padding: .5rem 1rem;
    flex-direction: row;
    margin-bottom: 8px;
    margin-top: 0px;
    background-color: white;
  }

  div.gallery {
    margin: 5px;
    border: 1px solid #ccc;
    float: left;
    width: 210px;
  }

  div.gallery:hover {
    border: 1px solid #777;
  }

  div.gallery img {
    width: 100%;
    height: auto;
  }

  div.desc {
    width: 100%;
    padding: 11px;
    text-align: center;
    display: inline-flex;
    background-color: #1c9ad6;
  }

  #search {
    margin-top: 15px;
    margin-right: 15px;
    margin-left: 50px;
    width: 100%;
    display: flex;
    flex-direction: row;
    /* border-right: 1px solid #999999; */
  }

  .search {
    margin-top: 15px;
    margin-right: 15px;
    margin-left: 50px;
    width: 500px;
    /* border-right: 1px solid #999999; */
  }

  .mail {
    margin-top: 15px;
    margin-left: 20px;
    padding-left: 15px;
    padding-right: 15px;
    border-left: 1px solid #898b8b;
    border-right: 1px solid #898b8b;
  }

  .bell {
    margin-top: 15px;
    padding-left: 15px;
    padding-right: 15px;
    border-right: 1px solid #898b8b;
  }

  aside {
    height: 100%;
    width: 100%;
    font-size: 14px;
    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
  }

  ul {
    padding: 0px;
    margin: 0px;
  }

  ul li {
    list-style-type: none;
    color: #4c4d4e;
    padding-left: 10px;
    padding-top: 10px;
    overflow: auto;
  }

  ul li div {
    float: left;
  }

  .linktitle {
    line-height: 35px;
    padding-left: 10px;
    padding-top: 3px;
  }

  .icons {
    padding: 10px;
  }

  .borderless td,
  .borderless th {
    border: none;
  }

  .mat-5 {
    margin-top: -5px;
  }

  .rWidth {
    font-size: 14px;
    /*width: 20%;*/
  }

  .mr {
    margin-right: 3px;
  }

  .col-white {
    color: white;
  }

  .back-to-top {
    position: fixed;
    bottom: 0;
    right: 2em;
    text-decoration: none;
    padding: 0.5em;
    display: none;
    z-index: 99;
  }
  
   .heading {
    background-color: #1c9ad6;
    color: white;
  }
.my_class {
color: blue;
font-weight:800;
}
.sm {
  list-style: none;
}
.sm {
  position: absolute;
  z-index: 1;
  visibility: hidden;
  background-color: #fff;
  border-width: 1px 1px 1px 1px;
  border-style: solid;
}
.navbar-nav {
  position: relative;
}
.navbar-nav > li > a {
  display: inline-block;
  padding: 8px;
}
.sm a {
  display: block;
  padding: 8px;
}
.navbar-nav > li > i:hover + .sm,
.sm:hover {
  visibility: visible;
}
.badge {
    position: absolute;
    padding: 4px 8px;
    border-radius: 50%;
    background-color: red;
    color: white;
    top: 0;
    width: 86%;
    left: 7%;
    overflow: hidden;
}

div.gallery img:hover{
	margin:-1px -2px -2px -1px;
	width:1400;
	height:800;
}
 
    /*table.dataTable tr.odd { background-color: white !important;  border:1px lightgrey;}
    table.dataTable tr.even{ background-color: white !important; border:1px lightgrey; }*/

    </style>
    <body>
	@php
    $currentRouteName = Route::currentRouteName();
@endphp
@if($currentRouteName =='vieworder')
<div class="totopshow">
    <a href="#" class="back-to-top"><img alt="Back to Top" src="{{ asset('images/gototop0.png') }}" /></a>
  </div>
 @endif
	<div class="tot">
	
         <!-- left -->
         <div class="left-bar">
                <div class="logo" style="padding-left:2%;padding-top:2%">
                <!--<img width="100%" height="60px" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABTAT8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwCpRRRX9EH8mBRRRQAUUUv4UeYCUUxp442w7qh9GIBpPtEX/PWP/voVPNHuUoskoqP7RF/z1j/76FH2iL/nrH/30KOZd0HK+xJRUf2iL/nrH/30KPtEX/PWP/voUcy7oOWXYkoqP7RF/wA9Y/8AvoUfaIv+esf/AH0KOZd0PkkSUVH9oi/56x/99Cj7RF/z1j/76FHMu6Fyy7ElFR/aIv8AnrH/AN9Cj7RF/wA9Y/8AvoUcy7oOWXYkopqzJJ911Y9eGFO//V/9anfS6FtuFFFFMQUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXuH7MfwPsvilqt7q2toZtC011i+zAkfapiN21iP4FUqSB1LL2yD4fX2f+w+P+Lc69j/AKDT/wDpPb185xBiZ4fAylSdm3Y+s4YwtPGZlCFZXSuz26z8B+G9Nt0t7XQdNtoFGFjitI1UfgBVj/hEdD/6BFif+3ZP8K1N2f8APSjdz3z7V+P+2q7uT+8/flhqPSC+5GX/AMIjof8A0B7H/wABk/wo/wCER0P/AKA9j/4Dp/hWtupNwo9tU/mf3j+r0f5F9yMv/hEdD/6A9j/4Dp/hSf8ACI6H/wBAix/8B0/wrVYjvS7vTmj21T+Z/eL6vR/kX3Iyv+ER0P8A6A9j/wCAyf4Un/CJaH/0B7H/AMBk/wAK1g24UjUe2q/zP7x/V6P8i+5GV/wiOh/9Aex/8Bk/wo/4RHQ/+gPY/wDgMn+Fau70GRRuGaPbVP5n94vq9H+RfcjK/wCER0Mf8wex/wDAdP8ACk/4RPQv+gRY/wDgOn+FazEdP0r5p/a4+JninwBqvhiHw7rM2lx3UNy8yxRxvvKtFj7yn+8enrXdgqVfHV40ITs35s83Mq2Fy3DSxNSmmlbZLqz23Xvhj4U8TWMlpqHh7T54H4J+zqrD3VhgqfcHNfCHxy+FZ+EvjqXSoZXuNNuIvtNlLJy3lnIKMe5UgjPcFfw+qP2TvHGvePfA+sXmv6lJqd3BqjQJNIiIVTyYm24VQOrN2715V+3IoXxd4VOOfsNxz/wNK+nySpiMJmbwc53Wt102Pi+IqeEx2TrMKcFF6Wfq7HzVRR+NH+OPx9K/Tj8b8rhRR16cnpgUuDzxz0xRrewCUUZzgd84ooDzCij/AD1ooHoFLR/+vjmvT/gV8Ev+F0Xmtw/2v/ZP9mJC/wDx7ebv8wyD+8uMbD65z7VzYjE0sLSdaq7RW/32OvCYWpja0aFFXlK9vkrnl9Fen/HT4L/8KZ1DRrb+1v7W/tCKaXd9n8ny9hQD+Ns/f7+leYdyPQ4PtRhsRTxdJVqWsWLFYWrg60qFZWlHf8/yCig4VSScD1PFH+OOldXmcgUUUUgCiiigAooooAKKKKACiiigAr7Q/Yd/5Jzr3/Yak/8ASe3r4vr7P/Yd/wCSca//ANhqT/0nt6+R4n/3D5o+34O/5Gi9GWP2lPjx4l+EniLRrDQ4dOeK7tnmc30DyHKsBxtdcdfevPl/bU1xfByK+m2E/iaWaTDpDIttDCANrFd5LsSW4DAYAzjpUX7cX/I6+F/+vCb/ANGCr/7JPwe8O+LNB1XxFr+l2usyfajZwQXkYlijVUVmbaeCxLY5BxtGOprxqOHy+hlVLF4ilzP89T6LE4rNsRndXA4Ws4rz2Ssnc5bw/wDtkeOdP1NZtUg0/VtPLfPbrCYXA7hHBOD1+8D2969E+Jv7ZFrp9pZweCraHUry5gSea6vMmK23YPllVILSAHkZAUjHJyB5Z+1R8M9J+HPjixbRLdbOw1S3ab7KmdsUiMA230Uhlwo4BB7YA7X9kf4Q+HfGGiar4j1/TrfWXjuvsdvb3aCSKNVRGZih4ZiXAyc42jGMmurEUMq+rQzL2Vo/y93/AMA4sLic8+t1MpjWvL+Z9F5epy2g/tjePNP1GOXUU03VbPdmS38gwtt77XU8H6hvpX2T4I8Y6f498Laf4g0x2exvY96hwAyEEhkYAnDKwIPuDXxf+1Z8MdH+HPjLTJdDtlsrLVYJJTaR/wCrjkRlDFR/CCHX5Rxwema6b4V+N73wv+yd42uraZ4ri1v3tLeQHmLzhApZfcGVm+tceYYDC4zC0sVg4cjlJL79PwZ6GU5pjcvxtfB4+fOoRbv6K+nqjrvi5+2BH4d1a50jwfaQancQM0cuo3BY24cHDKiqQXwerZA9M15fYftjfEKzut9wNIvI85aCS0aPj0DB8j8c/SvLvAMXhhfE1p/wlrXMegxjfLFZxs0kxGNqDHKgnqRyMEDBII9o8c+L/gN4i8LXdhpejz6LqIiJtbuz01o2WQD5d+3G9c9QexOMHmvWeXYLBctB4Z1LpXlv/XyPD/tbMcw58V9bVK17RvufRXwX+NmlfGHRZZ7aNrDVbTC3enyHLRk9GU4G5CQcHA6HIFY3x2/aFsPhHFFYW8I1TxFcJ5kdoX2pEmcB5COcZ4AAycdutfJn7Oviebwn8YPDtwJNsd5L9guEU/fWUbQuPQSbG/4CPxwfit4mn8UfEbxPq105dpL6ZFPpFGxSMY9lQD68/Xz4cPUv7RcHrTSv+Ox6lTivESylOP8AGvy39EtT0kftkfET7Z5x/sZos/8AHuLN9n5+Zu/WsT46fGa3+M1v4WuvsTafqNjHcw3duW3JljEVZG4yrYbjgjH4n6R+Gv7LXg7RvCdkuu6XHrWsXEKvdXFw7EByM4jAIChckAgA47184/tHfCOz+EvjK2h0p5P7G1KFpraGVy7QsrAOm5iSyjcpBPPOM8V2YCvlVbGqGGp8so3s+/T/AIJwZphc7oZc54yrzwna66rW6Pdv2H+fhzr4/wCoy/8A6Igrhv25v+Ru8K/9eVx/6MSu5/Yd/wCSd6//ANhl/wD0RBXDftzf8jd4V/68rj/0YleVhf8AkoJfP8j2cX/ySsPl/wClHR/C/wDZX8F+Mvh34f1y/wD7RF9f2UU83k3bKu5l5wOwq94O/Yx8PWt1fXHiO4uNQhe5l+yWUM7RpHb7yIg7jDs+zbnBAyTwT81ep/AH/ki/g054GmQ5/wC+BXyd8YPjv4ym+KOtLpuuXmk2WmXslpa2to+2PEbFCzr0cllJw2RzisaFXMsfiq2Ho1mkm9/U6cTRyjLMHh8TXoJtpbdbrVs9X+KX7HeiSaDPeeChc2eqQIWj0+W4aWG5xk7MyElWPIBzjOMjvTvht+xnodvpMFz4zefUNSljBextp2ighJHK7kIZyPXIHtXtvwv8VzeOPh5oGu3KJHc3tnHLKqDC7yBvx7Zzj2r46+M3x48Y3XxQ1qPS9evNIsNLvJLO2trN9i5icozuOQ5LKThgRjAqcDWzXGueBjVty3u3v6XLzLD5Jlyp5jKjzKdkktu97HrfxL/Y60GbQ7q58HC40/VIYy0dpPcNNDOQMhCXJKk9Ac49RXyl4T8Kal418S2WhaXAZdQupPLCPlRHj7zPx8oUZJ79sE8V+iXwm8V3Pjr4b6Brl4ipd3dqGnCDC+YMqxA7AkE/Q145+zl4XtofjR8WNS8td9rqT20H+wsk0kjgexIT/vmt8DnGJwtHEU675nDa/e9vzOXMshweLxGFqYWPLGo9Uu1r/ka3hH9jnwVpNhF/bf2rX7/b88zzyQRhu+xEYYHsxY+9UfFn7Gfhi8urK58PTXGmolxEbqylneSOaAODIquSXRyucNn8jzVn9rD4w6x8PdO0nR/D8/2LUNUEkkl6qBniiQqMKCMAkv8Ae7bTjkgjx74D/tCeLNO+IGkaXrerXGuaPqlytm63hDyQySMFRkfrjdtBU8YJxk81nh6ebV8PLMIVul7X7bm2Kq5FhsUssnQ10TfZu3zPTvix+zH4D8JfDXxHrGn2F1Hf2NjLcQu97K6h1UkEqWwa7f8AZx+FOi+BfCdrrenSXT3evafaT3SzSh0DCPdhBjgZkat/4/f8kX8ZH/qFz/8AoBryT9jTx1r3io+INM1XUWvLHSrazjsoWjjQQqfNXAKqCeETqT0rz/aYzFZbOpKo3GMtb/L9T03SwGBzqlShSSlKOlu6vf8AA9U+K/wL8PfF6fT7jWpb6GTT0kSH7HOI+H2ls/Kc/dFfLHw4/ZtuviJ401+BbiTTfCukalcWIu2+eaby5GUJHnjIAXLYxyBgnOPVv2vPil4q+Hep+G4vD2sSaZFc29zLOscUT7yjR4++jY+8a9x+HWktovgfSIHZZLloBcXMyqF86eT95LIQABlndm/GtKOLxuW4GNSM/dnpHy11ZniMvy/OM0nSnT96nZz/ALzaVjgLH9kn4aWVqsUuj3N5Jt2mea/nDt7/ACuAD9AK8l+NH7I6eHdIudd8HSzzw2qtJPpU7GVxGOWaJ/vEgZO05J7HIAPT+LvhJ8b/ABV4muNTj8Z2WlR+aWt7Ow1O5hhiQH5VKrEA+BjJbOcntxXvPgq31+38L2MPieWzn1xYylzNYljDIQSAwyo5IwSMYyTioWPxOBlGvDE87e61ZbyvB5jGeGlg3SS2lZL7v+CfmIpDAEcg8gjvRXW/Frw7D4R+J3ifSLZQlvbXzGFFGAqOBIqAdgquFHsBXJV+tUairUo1Y9Un95+HYilLD1ZUpbxbX3OwUUUVsYBRRRQAUUUUAFFFFABX2h+w9/yTjXj/ANRqT/0nt6+L6+wP2H9ctD4U8R6P5gF7FqH2sxk8mN4Y0DD/AIFE4/L1r5TiaLlgG13R9rwhKMc0jzPdM4/9uH/kdvDH/XhN/wCjFr0n9icn/hU96vBH9rXH8kq9+0B+z1qHxh1zSb+x1i300Wdu8DJcQtJu3MDkYIx0rqvgN8Kbr4Q+DZtFu7+LUJZLuS582GMouGC8YJPp618fiMbQnk9PDRl76e33n3uFy3FU+IauNlC1Nrf5I8H/AG5uPE3hEZ/5dLn/ANDjru/2IR/xavU+/wDxOJ+P+ARVsftBfs/6j8ZNV0a8sdXttNWwgliKzwtJv3sp4wRj7tdF8A/hPd/B/wAH3WjXeoRajLNeyXfmwxsigMqDbgk91PfvRiMbQnk9PCxl76e3zY8Pl2KhxFUxkofu2t/kjxD9un/kOeDP+ve7/wDQ4aofBTwfP46/Zj+IGkWiNJevqLS28a9XkiitpEX8WQD8a9g/aE+AeofGa/0K4sdWttMGnRzownhaTf5hQ8YIxjZ+tbXwC+EN58HfCuoaVeajDqUt1fNdiSGJkVQY40xgk8/Jn8a2/tKjTyqlShL95F3t82zneT4ivnletVh+6mrX/wC3Uj4S8CaloOn+J7WTxNpcmr6ISYrm1ilaORcjh1wy/MP7ueRnvivopYv2ZjYrOXxkZ8nzb/zf++M7v0rufi1+ybo3j3U7nWNGvP7B1a4O6dRF5lvO2climQVY5OSDgk5IJzXk8P7Efi9pds2uaOkWceYvnMcf7m0D8N1evUzDA5jGNWdeVOXVJs8CnlOZ5VKVGGGjVi9na/8AXoehfBnwf8IviBrl7f8Ahfw5ewyaFPbzRXV1cTqHcszI6o0h4BjH3hz3FfMvxi8Jz+D/AIl+JNLuYyqG7kuId3R4ZWZ0IPcYOPqrDtX2x8C/gbD8GbDUk/tWTVbzUDGZ5GhEca7N2Aq5J/jOSWOan+MnwK0b4wWMLXTtYatahhbahCoLKD1RwfvJnnHBB6EZNeThc3pYTHyfNKVJq13q/wCr3PdxuQV8dlcFyKFaLvZaJ+X3WPNfht+2F4bXwraW/is3dlrNrEI5JIrd5Y7naMb1KA7SQMlTjBOMnrXgfx6+LrfF/wAYRX0EEtnpNjE1vZwzEeYQWy8jYzgtheOwUcnNeij9h7xP9v2HxHpIsi/+v8iTzcY67Omf+B10/iT9ieKTS9IttA1qO3uYFkN7eXsLO9yx2bQApG1Vw2F56nOSSa9XD1skweJVelK7f3K54uKw/EeY4P6tXp+7G3a77Gz+w+2Ph3r/AP2GX/8ASeCuH/bk+bxd4V/68bj/ANDSvdPgH8JLv4O+FtQ0q91CHUpbq+a7EsMbRqoMcabcEnn5P1r54/bU1y31D4iaRp8EnmS6fYnzsEfI0j5Cn32qrEejL6152X1I4jPJVaWqd/yPVzSjPCcNRoV1aWmnzufS/wAA/wDkivg4H/oFw9P9wV8GfFBcfEzxaO39r3Y6/wDTZ69y+Hf7XWl+B/A+jaBJ4cvruXT7SO3aaKaMKxVcEgE18+eLdZTxH4r1rVo42ijv72e6SNvvIryMwU+pwRzXt5LgcRh8bXqVINJ3s++p87n+ZYTGZfhqVCalKO/lp1Pvr9nT/kh/hAY/5cVH6mvhL4lc/Enxf6/23ff+lMle4/DP9rTTPAHgLRvDsvh2+vJbC3ELTxTRhWwTyATXz94o1hfEHijWdWjjaGPUL+4u1jf7yrJIzhSe5wwqsmwOIw2Or1KsGk72ffUWfZlhcXl2GoUZ3lFK9umiPvz9nE/8WT8L/wDXu3/oxq8r+BPjK20n9on4meH7mRY31S/eS3ZjgPJEzFkHuVfOPSNq5f4Z/taad4A8C6T4fm8O3t7LZRMjTRTRhWJZjxn614d4o8XS6x4+1HxPppn0yee/e9tmVgJYSWLDkdx/njg+bh8mxFatio1YuMZ3s/8At66PWxXEGGo0MFLDz5pU7XXW3LZn25+0L8Df+FyaPYPZ3cdlrWnM5tpJwfKdXA3RvjkAlVIIBxjpXmvwY/ZJ1Twv4ys9e8V3tlImnuJraysXaQPKOVd3ZVwFPIUA8gHPGDj+D/22r+xsYrfxJoH9ozRqAbzT5VjMh9TGwwD7hsewqn4u/bQ1fVryyTRNHXStPjuYpbh5Zt9xcRK6s0YO3EYYBlJ+Y4PGK56OEzujReCgvcd9dPzOrEY7h3EVlmVRv2itpruu6Pov4/N/xZfxnjn/AIlk/wD6Aa8G/YWBGreNT/0xsv8A0Kesvx9+12njjwTrHh//AIRhrQ6jayWxna+DhNy43bfLGcema4z9nv412PwYvNfmvdLutRGpJAqfZCgKeWZCc7iP74/Kt8PlmMp5VXoSp+/Jqy77HPjM5wFbOsNio1Pcind9tGegft0r5mveD1IJBtbsH8Wir334D+Orbx38L9DvY5Q93Dbra3cYI3RzRqFcEdsnBHqGB718gftB/Gew+M2paFc2Om3WnDT4po3W7KHdvZCMbSf7p/SuU+G/xS8QfCvWGv8AQ7kKk2Bc2cw3wzgdCw65HZgQff16pZLVxGVUqMly1I3aT829DihxBRwed1sRB81Kdlp6LVeh9JePvBHx50nXrn/hHPFUmsaPJIz2/wA1tFNGvUI4dADjpuB564B4rK1Lw78cvDfg3W/EWu+PFsItNtJbkWkMcM8khVSdrHywqjjrlqt6X+3LZNar/aXhO7juQORZ3SSIfxYKR+VcZ8UP2uL/AMceHNR0HTNCh06yvoXt5ri5mM0hRhg7VAUKcE8ktXDRwuZylGlPDxSTs3ZbHfisZk3JOtTxU3JptRu97djwnWtav/EmqXOp6pdPe6jdMHnnk2guQoUcKAOAAOnaqVFFfpEYqMeWKsj8mlJyk5Sd2woooqiQooooAKKKKACiiigArY8K+LtX8Ea3Bq+h3r2F/DkB1AKspPKMp+8p9PYdCARj0VE6cakXCaun0NKc50pqcHZrqfRdr+274qjhVLnQNJuJMYMkbyRg/wDASW/nU/8Aw3F4j/6FrTf/AAIk/wAK+baK8T+wsuvf2SPolxJmqVvbM+kv+G4vEf8A0LWm/wDgRJ/hR/w3F4j7+GdM/wDAiT/Cvm2ij+wsu/59If8ArLmv/P5/gfSX/DcXiP8A6FrTf/AiT/CgftxeIx/zLWm/+BEn+FfNtFH9hZd/z6Qf6y5r/wA/mfSX/DcXiPt4a03/AMCJP8KX/huLxH/0LOm/+BEn+FfNlFL+wsu/59IP9Zc1/wCf7PpL/huLxH/0LOmf+BEn+FH/AA3F4j/6FnTf/AiT/Cvm2in/AGFl/wDz6Qf6y5r/AM/mfSX/AA3F4i/6FnTP/AiT/Cj/AIbi8R/9C1pv/gRJ/hXzbRR/YWXf8+kH+sua/wDP5/gfQGtftpeMtQs5ILDTNL0mRhj7QA8zqP8AZBIUHpyQR7V4NqF9c6rfXF7e3Et3eXDmSaeZtzyMTkkn/P8AQQUV34XA4bB39hBK55eMzLF5hb6zUcrC+tJRRXeeYH40UUUbgH40UUUABAPUZoooo8wD0o9OAfrRRQAtJRRTuwsgooopagFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//Z" alt="">-->
				<img width="80%"  src="https://medmate.com.au/wp-content/uploads/2021/02/medmate-blue-logo.png" alt="">
                
				<!--data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABkAT8DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwCpRRRX9EH8mBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRXV/DX4Z638VfEa6PokSb1XzLi6myIraPONzkep4Cjkn2BIzqVIUYOpUdkt2bUaNSvUjSpRvJ6JI5Sivru1/YV0wW6favF188+PnNvaxomfYMWP61N/wwvon/AENeqf8AfiH/AAr53/WPLf8An5+D/wAj6tcJZv8A8+l/4FH/ADPj6ivsH/hhfRP+hr1T/vxD/hR/wwvon/Q16p/34h/wo/1jy3+d/c/8g/1Rzf8A59L/AMCj/mfH1FfYP/DC+if9DXqn/fiH/Cj/AIYX0T/oa9U/78Q/4Uf6x5b/ADv7n/kH+qOb/wDPpf8AgUf8z4+or7B/4YX0T/oa9U/78Q/4Uf8ADC+if9DXqn/fiH/Cj/WPLf539z/yH/qjm/8Az6X/AIFH/M+PqK+wf+GF9E/6GvVP+/EP+FH/AAwvon/Q16p/34h/wo/1jy3+d/c/8hf6o5v/AM+l/wCBR/zPj6ivsH/hhfRP+hr1T/vxD/hR/wAML6J/0Neqf9+If8KP9Y8t/nf3P/IP9Uc3/wCfS/8AAo/5nx9RX1lrX7C8K2MjaP4sma8A+SPULZfLY+hZOV+uD9K+YPE3hnU/B+u3mjaxaNZajaPslibkdAQQehUgggjqDXp4PMsLj7rDzu101T/E8fMMnx2VpPFU7J7PRr8DMooor0zxgooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvs79huxhj+HviG9CYuZdYMDv3KJBCyj8DI5/E18Y19rfsPf8kt1z/sOy/wDpNbV8txK3/Z0vVfmfa8HpPNoekvyPoiiuH+IHxo8JfC+9tLTxHqMllPdRmWJY7WWbKg4JyinHPrVOP9oDwHJ4QbxMddWPSRO1srywSpJJKoDFEjKh2OGB4BHNflkcHiZQjUjTk09nZ2foft0swwcKkqU60VKOrXMrpeavoeiUV4/4d/av+HPiPVk09dVm0+aRgkcmoW7QxMT0+fovUfex1ruPHXxM8NfDXTkvPEWqw6fHKcQx8vLMf9hFBZuo5AwMjNOpgsVTqKlOnJSeys7v0FTzLBVqUq1OtFxju7qy9ex1FFeN6D+1p8ONe1KOzOqXGmvI21JtQtWiiJ7ZfkL9WwK9jVgygg5B5BqK+Fr4VpV4ON+6saYXG4bGpyw1RTS3s07C0Vy3jj4neF/hvapN4i1iDT/MBMUJy80mP7sagsR7gYrgdP8A2vPhpfXQhfVLuzUnAmubGVY/xIBwPc4rSlgcVXh7SlSlJd0mZV8zwOGqeyrVoxl2ckmez0VV0zVLPWtPgvtPuob2yuEDxXFu4dJFPcMOCKreIvE2leEdJm1TWr+DTdPh+/cXDhVHoB6k9gOTXIoScuRLXsdzqQjD2ja5d79LdzTrzD4xfHzSfgzeaVb6npl/qDagkskZsvL+UIVBzuYf3x09Kxl/a++GbXvkf2peCPOPtB06by/r93d+leOftoa5p3iabwJqek3sGo6fcWt4Yri3cOjfNDnkdx3HUV9FluVVJ4ynSxlKSjK+6a2TZ8jnGeUqeX1a+X1oynHl2alvJLb5n0j8Ifi1p3xi8P3mrabZXdhDa3ZtGjvNm8sER8jaxGMOO/Y181/txafBb+PPDl5HGqz3OnSJKwHLBJPlz9N7V6F+w7/yTXX/APsNv/6TwVwv7dH/ACNvhP8A68bj/wBGJXq5dRhhc+dGlpFX/wDSTxM2xNTG8MLEVtZPlf8A5Nb8j5nor1Lw5+zP4/8AFmg2Gs6ZYWU2n30KzwPJeqjMrDIJUjiofB/7OfjvxteahDZabDbQ2N1LZzXl7P5cBljco6oQCzgMpGVUjjrnivvnmGEipN1Y+7vqtD8tjleOk4pUJe9to9fQ8zor0n4ifs9eNvhjpx1HVbGC70tcebe6dMZY4cnA3gqrKOnOMc9aT4ffs9+N/iXYrf6Vp8NrpkgzHfalKYYpP9wAMzD3Ax71X17C+y9v7Vcne6+718if7Nxvt/q3sZc+9rO9u/p57Hm9FemfED9nTxv8NdMk1PU7K3u9Mi5lu9NmMyRe7gqrAe+MeprzOt6OIpYmHPRkpLyOfEYWvhJ+zxEHGXZqwUV7D4P/AGUfiD4usYr42lnoltKu5Bq0zRysD0PlqjMv0YA+1UPF37NPjzwbeWMV3YW13bXlzFaR31jOZIEkkcIvmEqGRcsPmK4/HiuRZlg5VPZKrHm9TtllOYRpe2dCXL3szy2tbw/4S1zxY86aJo99q7W4UzCxtnm8sNnbu2g4zg4z1wfSvR/FP7LXjrwf4c1HW9QGlfYbCBribybxmfYoycDYMn8a9u/Y7+GviHwXDrer6vaR29jrVpZzWMiTrIZExI+SAcrxIvBrixmcUKOFliKE4ya2V93dfpqell+Q4rEY6GFxNOUE7tu2ys7P5tWPkrxB4T1vwnJBHrWkX2kSThmiW+t3hMgXGSu4DOMj86ya+0f2rfg/4s+J+reG7jw3p8V8llBPHP5lykO0u0ZXG48/dNfJNj4K1zVPFU3hux02W+1uGeS3ktbX59rxsVf5hwFBH3icdOa2y3MqeNwyqyklLVtX21e/b5nPm+UVctxksPCMnG6UXb4tE9O+rtoYlFe8WH7F/wAQryzEs1xodjKy58ia8kZwfQlYmX8ia84+IXwj8VfC24jTxBppgt5W2xXsD+bbyN/dDjof9lgDwcDiumjmOExE/Z0qqcu1/wCrnHiMqx+Fp+1r0ZRj3a/Pt8zjqKKK9A8oKKKKACiiigAooooAKKKKACvtb9h7/kluuf8AYdl/9Jravimvtb9h7/kluuf9h2X/ANJravleJf8AkXy9UfbcH/8AI2j6S/I89/bm/wCR28Lf9g+b/wBGCvPvgv8AAXWvjVFdyw6nHpOjac5hFxNG0wMzgMyogZRnG0scjqvXt6D+3N/yO3hb/sHzf+jRXpf7E53fCG7J6/2xcdvRIx/IV50cZVwOQ0qtHSW3pds9aWX0My4orYfEax3a2vaMdLrXfU+Ufit8LdT+EvihtE1SSG6EkQnt7qEEJPESRnB6EEEFecepBBPWfCz4P+J/2hria8utb+z6fpMMWnrfXqGdlCqCkMaArkKuCSSPvA8kmu9/box/wlHhE9/sdz/6HHXoH7EPzfCXUmPX+2pxn6RQgV0YjM68cnhjVb2j0vbbWzfzscmGybDTz+plsr+yWtrvXRNJvfS/qfLXxY+FOq/CHxONI1OWK7jlj8+1vIAQk0eSM7TyrAjBHPbk5r6g+A3xYbRv2aL7W9TZrpvDRmtEDN80oUK0Eef+2iRj6CuK/brA/t7wYcc/Zrsf+Pw1zXgOynvv2PfiItuCWj1mOdwv9xBaO5/BQT+FRWl/amWYepid5Tjf/wACcX96NcPF5LnOLpYS9owlZb/YU162Z5np2n+Kfjl8QvLEv9p+INTcySTTMVjjQckk/wAEaDgAdOAASQD6f45/Y78SeEfC11rFnq9rrj2kZmns4bd4nCAZYpktvIHOOCccZOBXk/w58Mah408W2ui6Vq0Gjahdqywz3NzJAkjAZ8vcgJ3HHA749cV7U37I3xQZSG8VWJBGCDqt3g/+Q69PHYn6rVhCOIjTikvdcb3Xr0XTQ8bLcH9doVKksLOtNt+8pWs/S2r1u73M39j74oXfh3xxF4UnnZ9E1reYY3JIhuQpcMuegYKVI7nb6Guc/aa+JV14++Jmo2a3DHRtGmeytLcH5fMQ7ZZMdyWDAH+6B716D4B/ZL8ZeFvHmharNquhtHpt9b3c8dvcymXy1kBIAMQ6hWAyQDXz147sZ7Lxh4ls7gFLiPUbuN9397zXGazwscFicyliqEk3yrbvff1tZGuOlmODyeGCxUXGPO7X7JKy9L3f/DHq/wAPf2S/Fnjzw1ba3LeWWiW12gltYbsO0siEZDMFGFDDBHJOO1eaePPA2ufDfX5fD+uxeTcQfv4/KcvBIr4HmRkgZDbADwDlcEcV+jPw98UaZ4x8G6TqukSpLZTW6bVQj92wUBo2HZlOQR2Ir5I/bU8UaZrXxA0fTbGRLi60q0kS8kjIIR3dSsRP95QpJHbeK8zK84xmMx8sPWj7uulvht/Vte57Wd8P5fgMrhisPNuXu63vz38vxVuiPTf2Hf8Akmuv/wDYbf8A9J4K4X9uj/kbfCf/AF43H/oxK7r9h3/kmuv/APYbf/0ngrhf26P+Rt8J/wDXjcf+jEriw/8AyUcvn/6SehjP+SRh/wBu/wDpZ9DfAH/kivgvjH/Erh/9BrgPiD+1xoXw/wDGlz4etdEuNVjsZPKvbmCZYxHJ1ZUUg7yM4OSvORnvXf8AwA/5Ir4L5z/xK4f/AEGvgn4pf8lO8X9/+JxeHk/9N3rkyvL6GYY/ELEK6Tem3U785zXE5XleElhXZyUdbJ6KK017n6QWN5pfjjwvBdRCPUNG1W0DhZEys0MidGU9ip5BrxT4hftY6D8M/Fr+GLHQJ9Th03ZBcyW8qwpCQB+7jUjDFRgYyoB4ruv2cyW+B/g0k5P2Bf5mvhD4qMW+KXjQk5/4nt+P/JmSoyfK6GKxlahWu4wvZXt1tfQ0z/OsVg8Bh8Th7RnUs27J6WvZX82fo/o+q6Z458L2t/bbb3SNUtRIokTiSJ1+6yn2OCD7ivkf9nb4P2N38dPFSXkf2rTvCN5JHBHL8weXznWBm9dqxs3P8W019C/s38fBDwmBwBbMB/39euK/Z61CEfGD4z2JYC4OrrOF7ld8qk/gcfmK5cPOeDp46lRei0/8m5fybO3F06eYVstrYhK8nf8A8k5rfekei/Fr4vaL8HtAi1DVVmuZ7hzHa2VsAZZ2HXqcBRkZY9MjqSAeJ+E/7VPh/wCKPiFNAudMuNC1O4DG1S4kWWKfaMlQ4xhsAnBHbrniuM/bc8Garqln4d8Q2dvJdafpwnguxGpbyd5RlkIH8PyEE9B8teF/s8+DNU8afFXw9LpsEj2mmX0N9d3qj93Ckbh9pbpubG0L1+bPQGvSwOU4GtlUsTUfv2et9mtlb7vN30PHzLPcyw2dxwdKPuXikrfEna7vv320Vtep9sfH3/kivjX/ALBU/wD6Ca5r9m34vaZ8QvDMOhWVleW1z4e06zt7iS5CBJTsKZTaxOMxN1A6iul+P3/JFfGv/YLn/wDQTXgn7Cf/ACFvG3/XCy/9Cnry8Ph4VcnrVZbwkmvnyr8j2sZiqtHiHDUYP3akWn8uZr8Ue1/GH4+6N8F7vTLfVdO1C+fUI5JENkI8KEKg7t7r/eFSfA7wPp/h/QbvxDFbGPU/E9zLq88kyjzUjmdpIoTjONiOMgHG4se9eEft4KZNc8IKOrWd4Bn/AHoq+o/AOqQa34G8P39qd1vc6fBInbgxqcVGIoxw+V0atO6dRvm/7dbsv67GuExEsXneIo1rNUVHk01XMlzP9PmeEeL/ANtjStD8Sz2GkeHpNb0+3lMUl+bwQeZg4YxpsbcOuCSM49DmvZbO68OfHH4biUJ9t0LWLdlKSDa6HJBH+y6MDyOhXINeVeI/2yNH8J69e6NqnhHXLTUbSQxyQyeSDx/EMvypHII4IOar2X7a2i6lHcyWfg7xDdR2sbS3EkCROsKDks5D4Ue5xXVWy2vKnCeGwrg1Z83Pe/n0+TRxUM4w0K1SnjMbGpF3XJ7Nq3ddW1bRp39T5D8UeHrjwj4m1XRLs77jTrqS1d8Y37WID47Bhhh7EVl10/xO8XW/j34ga54itLaS0ttRmWWOGYrvUCNVOdpI5Kk8HvXMV+qUXN0ouorSsr+ttT8QxEaca040XeKbs+6vp+AUUUVsYBRRRQAUUUUAFFFFABX2r+w8w/4Vfri5G4a5ISO//Htb18VV61+z38cm+DeuXSXsEl54f1Hb9qjhAMkTrkLKgPXg4K9xjHKgHwc7wtTGYKVOkry0du9j6bhzHUcvzKFau7R1Tfa63PRv22NE1LU/GPhmWy068vY0sJVZrW3eUKTICAdoOK9H/Y00280v4S3MV7aT2czatcOI7iJo22lY8HDAHFdVa/tKfDO8hSVfFtnFuGdk6yROPqrKDU//AA0V8Nf+hw03/vtv8K+Aq1sbUwEcveHkuV72fd9LeZ+p0MNl9LNZ5qsXFuS2vHslvfyPDf229G1HVPEvhR7LT7y9SO0uA7Wtu8oUl0xnaDjoa739i/TbzSvhTqEV9aXFlK2sTuI7iJo2KmOLBwwBxXa/8NFfDX/ocNN/77b/AApP+GivhqeR4x00/wDA2/woqV8ZUy+OA+rysutn3b2t5jpYXL6WbTzX63G8lblvHslvfy7Hin7b2j6hqmteD2stPu71Y7e7Dm1t3l25aLGdoOM4P5V0/wCx34dkb4UeJdO1nTZooLzVJkktryFo/Nia2hVhhgCQfmH516L/AMNFfDX/AKHDTf8Avtv8KP8Ahor4a/8AQ4ab/wB9t/hTliMbLL44BYeSt1s+9+3mKGEy6OazzR4qL5vs3jb4VHe/l2Pkj4tfsz+J/hzq1xcaVZ3WveHt++3urWMyzQjPCyoo3Ajj5wMHGcjoOUt/iZ8RpoV0+DxL4mcAeWLeO5nMv04+f8M19w/8NFfDXv4x03/vtv8ACl/4aK+Gv/Q4ad/323+FetTzjG+zUcThHNrrZ/5PX0seDW4fy1VZTwePVOL3XMn8r8y09bnnH7HXhPxLoNn4p1HxFp1/ZHUpLZoZNSDCacqJdzEMd/8AEvLAZrA/ah/Z11PWdam8Y+FbRr+S4Uf2lp8IzKWUYE0Y/iyAAyjnIBAOTXsv/DRXw17eMdN/77b/AApf+Givhr/0OGm/99t/hXkRxWZQx0sdCi03o1yu1rJW/BfM96WByiplsctqYiLUdVLmjdO7d/xfyPgHTV8T6PfTafpy67p97M2ySzs1uIZJDjoyLgk/UVueJvg34u8I6ZpN5qWjXazamJZFtIIHllhVSvzS7QdpYucKeRjnBOB9xf8ADRXw1/6HDTf++2/wo/4aK+Gv/Q4ab/323+Fe9LPsfzKUcI0uuj1+dtPxPmI8MZZyOM8em+msbLXXTmd/wOC/Yr0280v4ca5He2dxZSPrLuqXMLRsV+zwDIDAHGQefavO/wBudx/wmHhRc/MLCckexkX/AANe363+1H8NdGs5J08RR6lIo+W30+J5ZHPYdNo6dWIHvXxV8WviXe/Fjxrda9dwi1jKLBa2obd5EKklVJ7nLMxPqx7YrPKcPisTmcsfVpuEdd/NWsrmue4rBYPJYZXQrKpLTaz0Tvd2vbta59t/AvxRo1n8HfB0Nxq9hDNHpkKvHJcorKdvQgng18M/E6ZLj4leLJYnWSN9Xu2V1OQw858EHuPeuWaCN2JaNWPqVFPr6bAZVHA16tdTvz9Lba3Pj8zzyeZ4WjhpU+X2fW976W7Kx+gf7PfibR7H4LeEYLnVrG3njslV4pblFZTuPBBPFfEPxMmjuPiZ4xlidZYpNbvmR0OVYG4kIIPcVy7Qxu2WjVj6lRTwAoAAwBTwOVxwOIq11O/P0ttrcnMs7nmWFoYWVPlVNWve99Eu2mx+gP7PPijRrH4M+F4LjVrG3mS3cNHLcorL+8fqCeK+XLr4mXPww/aO8SeJdNIvbZtUuUuYEcbbm3Z+VDdP4VYH1Uds1480Mbtlo1Y+pUU8AKAAMAVjhslpUKtapOXMqt7q3d37nRjOIq2KoYelCHI6NmpJ31Sttb/PsfpL4N+NXgrx5YR3Gm6/ZiRlDPZ3Uqw3EWezRsc/iMj0JrP8WfHnwP4IurHTv7Xsbm/u7qK3FrZTIwh3uqmSVgdsaqG3Etg4HGa/Oh41k+8qt/vDNOACgADAFeSuFMMql/aPl7afn/wD3HxxjHS5VSjz99fy/wCCz7/+OHxC8Lan8IPF9pZ+JdHu7qbTZkihgv4neRivAVQ2ST6CvHP2HtRtNP1Xxobq6gtg0Nnt86RU3YafOMnn/wCvXzH6U140kxvVW/3hmvQpZFClgqmCjUdptO9trW/yPKr8TVK+Y0cxlSV6aatfe6a3t59j6Z/bf1C11DXvB7WtzDcqttdBjDIHx88XXBql+zZ+0lbfD+xTwt4nMg0QOWsr6NS/2TcctG6jkoSSQQCRkjGOnzosax/dUL9BilrsjlFF4GOAqvmS67O927rfucE8+xCzOWaUFySlbTdWslZ7XTtfofpXJrHw++IFpFdTXXhzxBAo+SSZ4LgJ7fNnH0rz745fErwDovwr8S+H7DV9LW9vbGa3t9P0srIfMZcDKxghBkjlsV8JtEjnLIrHpyM06vHo8M06VSM3WbUXdL+v8j38RxlVr0Z01h4qUlZu7e6ttp+bCiiivtD87CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/Z-->
				<!--data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAA7AGcDASIAAhEBAxEB/8QAGwABAQEBAQEBAQAAAAAAAAAAAAgHBgUBBAn/xAA5EAABAwQBAgMFBQUJAAAAAAABAgMEAAUGEQcSIQgTMRQiMkFRGEJVlNMVMzhScRYkJWF1dpG0xP/EABwBAAICAwEBAAAAAAAAAAAAAAAFAwQCBgcBCP/EADIRAAEDAgUDAgMHBQAAAAAAAAECAxEABAUGEiExEyJBFmFCUZEUMlNUceHwFSMzgaH/2gAMAwEAAhEDEQA/APCpWhcUcFZ3zH7c7iiILEW3FKHpc95TbRcV3DaelKipWu57aA1sjY3of2HOX/xrE/zsj9Cu/XGN4faOFl55IUORNfH9llXGcRYTc2tspSDwQNj42qeqVQv2HOX/AMaxP87I/Qp9hzl/8axP87I/QqH1JhX46frVr0RmH8ov6fvU9UqhfsOcv/jWJ/nZH6FPsOcv/jWJ/nZH6FHqTCvx00eiMw/lF/T96nqle3mmG3/AMmnYlk0VLFwgLCXAhXUhaSApK0K+aVJII+ffRAIIHiU4bcQ6gOIMg7gjyK1l5ly3cUy6kpUkkEHkEcg0pSlZ1HSlKUUUpSlFFWz4LLtbLDwpkF4vM9iFBi5DIcfkPrCG20+zRe6iewFbU1y3xi/cbbaWc8si5d4SlUFpMxBMgKJSnp7/AHiCEj5nsN1LfEH8G/JX+oy/+vErjsp4kxmyeGjGOVoC5SchmT0mQ+XldKm1KdSlCU+ieny0EKHf4vqNctu8GtcQv3nH1lJU7oEAHcpkT7fz3rv+HZlv8Hwi2ZtGkrShjqq1EgwF6SBHmOP4Db8PkfA7hlD2FQcutT99YKw5ARJSXgpPdadb7qSAdpHcaOwNGvmUcj4FhUmPCy3L7VaZEodTTUqUltak711aJ2E7BHUe3Y96krOOLMf4oybhbJMXemJuN4uEVdyeefUv2h4OR1FzR+Hq81YIHYjXb1J5gsxuQuSeQrzl3GuZ5jJVcXojBshV/hqQtxCOvSTtSUIQlAV7ukK2FfKmzlq0fAfQ4otaSTsAqQrTtJgDzuZj3NMrnPGIWpVausID+sAbqKQkoDm8AqJggbCJ34FXhPyCxWqzLyO5XiFGtTbSX1TXX0pYDatdKusnWjsaO++xr1rC+Vub5Ksv4rRxbnMSRZ8gvxgXX2MMvpdSH4ifLUVJUps9Lq/TpOlb+hqermzn7vH/AB3xFnjF0x6DKyiSwk3GKtgojkxQhzSwCUtqkyCPl6D7tdryhwzh/EPM/FKMPlSQ1db1D8+LIe81SFtS44Du/X3+s9vTaDr6CxZ4DZ2T6Q+vWpXU0iAUkJChJ9/I5iPnVPEc3Ylilso2jXTbQWAslRCwpakKgDaUx2mYkGeJFfl8W2JZHl/O7VqxSxS7pNGPRpC2YrRWvoDzqSsgfIbSN/5isUuHF/Itqx5rLLlhd2j2h5xLaJS45CepSulOx8QClEAEgAkjROxVrj+MpX+xP/XXj8P855bmkDlafkbEGS1iBVOtjAZCQhAEhSGlEfEEmOghR97ZPf00ysscvLKybQy2lSEIbJkkHvOmB45+nvSTFcqYbiuJvO3Ly0OOuOhMAFI6aQok+eJ2HJ8io9ynjXPsIhRLjl2I3K0xpx6WHZLPSlStb6D/ACq0CelWjoHt2Nfrh8P8oXDHGsthYLdnrQ/0luSlnstKjoLCd9RQd/Hrp133qqYseX3XnXw0T5HIrkaRIayq321UltpLR8tc2Htek6CVBEhaNjXujv3JJ9zlrm3O8D53xDjnG7dFRYpaYLbkUxgVS0vvFpXQr7gQlOkhOgFA9Wx2DD1BiCl/ZktI6qSvVudMICTt5k6vP+/ZL6NwZLYvVvudBaW9EJBXqcKk9w4gFMmOZgHbfEOFPDRdMxzq5YxybZ8gscSBbPai7GLbag+pxAbQpS0LTpSPNOtb9z17Gs5i8SckXHKJOH27Crs7dYqQ67FUz0LaaV8C3CrSUAj0JI38quWzZFcmfFDkOJIU3+z5WKRrk4ko9/z230tpIV9Ol1fb+lc1guYXa9cJXDkibm9jxW/3+5LamXyRCQppgMueQ0joKgnfQ2AnqJALhOiTS5GZL9C1PKSkhQbgbwFKBMwJMbKmJOwjmnTuR8IcbRapUoKbU8VHtBUlBSIkkJndMEwN1E8ColyXFsjw67OWLKbLKtc9oBSmJCOklJ9FA+iknR0QSOxpVD+KjJMLy3AMTmR8+sGTZXapRhzJFsKUF5hbSitwthR6R1tN9t6BWdaB1Stzwm9cv7VLzqdKtwRv4PImDB5E1zHMWFtYPfqtrdetEAgyCYImDpJEg7GDG016Xhjt8XP+A874pt93ixb1OmuvhD2/cZdZYQlzQ7lPU0oHXp236itNyXw95BfPD7ZOHmL/AG9ufa3m3XJa0L8lYStxWgAOr74/4qBNAneqapNdZcfduS+xcaRrCwCgGFARzI29orZ8PztaW9km0urPqENloqDhTKCrVEaTB95r+h/KHCV7zxzjpcC9Qov9jJLb8nzULPnhJYOka9P3J9fqK5TkLwx5m9ndyzrh7kVzGHr8suXKP57zILijta0ra2VBStq6VDsokg6IAhvVNVXt8s3tqEpbuhABEFsEEKOoyCrffcfKrV5nvDL4rU7h51KKTIeIIKU6RBCNu3Y/PzV+ZZ4ZlZfw7ZcBvGXOS8hsTr8ti9PtKX5rrzi1uIcClFRQrrA31b2hCvkUnirH4RM9j5PiuZZHyS1drlZrnGkyxIW88BGYdbW200tfvE+656gAbGh6kxxqmqlZy/iLKFNpvBBKj/jHxcx3bA8wPNQXGcsGuXUPLw46khIH95XwRpJ7NyAIkyY5qtuRudMa498UVwygQ3byxb8fTYn0Q3Ue7ILodUOo9j076VD1CgQe4IrJOMuarVgds5HgTLHMlKzeMWI6mnEARiUyRte/X9+n0/lNZIAANAV9pkxgFo1b9BUntQCZidBkfpvvSW7zjiNxdm6bITCnFJEAx1BpUJI37dv+1qGN8wQLDwPkPEhtUxU+83JE9mc24lLbPSqMoAj4t/3c9x9RWsxPG6wqxQH7zx0zNymCny0zvMQGh1aDjiPd60FSR3QO2/nqpWpWVzl/D7skvIklRVyeSADweCAJHFRWWccYw8BNu7ACAj7oOySVJ5HIKjB53qkLZ4qceY5vncrzMTuSIsrHhZUxGnm1uBwPoc6yT0jp0kjXrvVcrwr4hk8aW+64lk2LtZFi93fVIXDcKOppxWgrQWClaVBKdoOhsbB9QcZpXv8AQLDpqa0dpCRyfhnTBmQRPNBzhi/XTcdQaklavujcuRqBEQQYG0fpWl83cq47yZOtDWJ4QxjNrszLzTMdry0+YXFJJUUNpCU/COw3/WlZpSmNrbN2bQYaHaPcn35O/NJL+/fxO4VdXBBWqJgADYQIAgCAKUpSrFU6UpSiilKUoopSlKKKUpSiilKUoopSlKKK/9k=-->
				<!--data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT8AAACeCAMAAABzT/1mAAAAt1BMVEUemtb///////2azOn///z//v8dm9UAlNMAk9SQxuQdmtf9//6m0esJltgAldIAk9Hs9vu82/DJ4PEAktbe8PZttuFIrN0zodjP5/SOxeb1+/2Qx+S+3/Gf0eyx2OyGxul6vuPl8fs6pNnY7PXk9vkAjtQ+pdhkt9693Oh0vN/L5u4Al814ueKVyOqs1Ophr93r+PVyvdsSlN0cn9NMrtvZ6vhdtthsteSpz+g6pN/T7fFMreOGwObquG8zAAAMaUlEQVR4nO2cC1viSBaGU0WlKlZMJXYgQAKIoKBogzPQu2P3/P/ftacuCeHW7bpGe9jz+qiE3L+cW50EPA9BEARBEARBEARBEARBEARBEARBEARBEARBkH8o8rMPAEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBPl4OOfuj5nYvm3nGU6v6uaWi+ys9ZM1zwapRNOo83ysR8KPUNm41TD9eTfi5yghl+qpTRhtnPy+Kz77ZN8fKdWAUsbC94buTBGf+Yx1zk9AqXrko/Dpw9kJGA/Zh+lHCJudVwiU3ox8mH4+/N6nn33K7wpXQxZ+lH6G7jkZoOTxlH6kfiEbq88+6Xcl9psVjNrtU/gx/1nrrPTjccPyhfl9z5CbacYuzysDN6wfoYu7ABB3iZ2k760f1yWslN7nDLGlaFq/ntFLBg3pB8p1LdG7bve1u//H6xff2g0vgnfd7mtpPH80rJ8XtO2OvnxOXG08/jWsH6/0+5y8/hvoB9F/p7Fl0sEh/OgMfsL+uG7MNR8Sufp8/5Wcg4Lb6bUXHUmmIAavN8ZLnH70y0780xuUR5Z+d5q2P3JcP2lb0trnpIqDxziq3E+kSgWp2JdZBIJzFQR6OSGgIjIvYdFKP6VEuREl0oBLrmLR8GhRVvol7fYNkDAwyN7l+Kk/SOBsw1A3Pr8U43F/eUNrQz1fjyWmo9bF12Uvp5TUBoGwSt67LGBGCMt8O6rfamx4eJRBt9W7vbkZzIXS3inFZvktSdqDh3pJxyNv3EvyMGwvMyXV8mqg6Qu+yrIbO7Lp/cgAvYr0ouyvaR6SMPly7QXN2mClX/8uhjo3nTCymMQK7CPwvoYMTpi1eKBEJJQYG3GtfIw897sxmJCIAvnQZnSrH73pPAZ6lcBrhXSqjuinrlxLOo6GjJqVaTuLwJVnC2IaGowlnSp8SXGR2z2EjA3W6TMxa98HXb1u6GZoI9BOLDY9ZntK8O+5ULJJBav6pW9iuMzItdDhI4IgFP+Ao253VQTxHUKUF6zdtdaD2ksP/MmzAV2pFiXlQJcMIwXLe7A9nnan7eCYfiPd76Y0UT2tiw9XCYx7pdI/GJyz3pJ+69r5sOQ9mOv71F65ZNKmZolB0IX9heZtZjylnXIvGFIWlj25kLLv6ybTSGV/hbETtSpqDTqxIm2+DUSSr7Wrmgs7T3ciS9rxrYFQNq4HcsWHx+xPjKzVJAu2zV805x0WVtOgy0ZfUylVr2bdeq1nu6sro18VTwC4WGJAdpcGC2jQAiv7s/rJ9Z+PW2Hk43KyU1qIsXYuOL7NboqUj+LBngK7juvve+6e26F+VqN68zFkvTysNXPBnMw9TzFi/k6PDaLygX7E6ReMGKV1SWGz3xuMgXv6gYC6mHB1Flx6HpnCyx4AZLTEHNwfQn9Jg7EOpw//c0R9MJn7uLwDL21d59bc0+/K2h/VAtLyRMFBwU3hvzFCiAh0rlPsnLnsBC5MQv3HyQj6sVLAULsx+O+cGXHhF+Kqr5f1fdZq7N4z39ePR5Ap1E4tD288liEkWsJhhj17PBGPIM04gR5VDs7HJs4wecSVDoPuwE/oZ0VkdOvEkJhAE+fV4RUcSNoubYyy8o+dhPhn8ojdMERO2r5L7HyIe3kCCtod5I3deN7Wz04/sKRh+2Yw28ZcVUyT3spFQdGBw6OZnSt48aW9KDy9JpeqgBMYCFezCv7U+tpRZfA84b9wgsXEW/W2bhi2ZutsUCoE+VSuSn8kyfUq6wyq+zVav/Z0mlv98ilwP9ZzdaIZTeI0GNt5hLUaGxzv5Q+oZk09ylblFVMLok+gbLtP4FSnqR4JSPliFqW3NkZKDrazMurxSF3b/DB3BeyJ/OH7m0DqW6iu/vHJUwqT6bL0SvmYjqwxUjLQBbeKN7kLh+C/PAjuyvFbCjVT8M3qRW0WjGaJvjLUv22sjt73X9WCw4WwlDgDFNf21NiLneYhpAjBzXMft9pXIO/epo96VtCjueL2IvSJrrV96v8rteIe+K890aU+T4i2LiPQ+1SnL54mznAmUtw4Z14oc0hRugltltH61fovkLdk1wZKOrrT8Rmy11yvDPY4Ozqkfg/99uxPPJs9MjK3AqopNZGZFmZayjykM5MU0mXlSRdmnmiRRWCzycT6HMTDxCaQU/ljZhyLpwtX280jo7YaOveeiXXurG1iv1doHcUju99D/USnlJ2bR8kgqLSZMeuLpjLInn6yS2wFRofOYV14Ife2DoYEnJv3OR9fl8ztoh2ytH4Sbe/J0+tj4zenH82dWUQtV5iv7bR6spMkE5l1bAqlnatC1IxU/lvrv2j93GZ8/bxIqBN5aA2bNHbXZd/+fjBb5pORPe9u6NLbIi71S0xskbJ6MC1yF7dLWtZo415ZBlN29dP6Wbhs03dXyRaf3Pu3WzoLVm47g7iyoIht/XdHP6gOrN3r4oXqlExdYB00FQD34p/KytphZERRZYVKe6X9sZsTnXIuSd8F0ark8NnR/kulnyssVN+tYAc/UnZK/dTGXYjRdq8qL8cfe/qpZZXH9xg05r8H+tFf6Zdsz0TW4ZKWSWhaHThb/FQ/b1c/6vTztvpl7lWvUkB65IR+YkhPCPjlg+LfK/SjiVWE29GFeTrX/HprMnSF9agcPEHpdax/UOaPX+r3Q03sZmjCbWyQPHhwFc2h/V04/x3UuIKfwUflj9foF3b1K56OwnwPcm/DDM/KcW3IbPfjlfqRQ/147rJt4SSIxC09YX985S7aKtgSp7rV2tQI5A3+S8xTkDza7D+06vuJrf88Naoui1Pkrf4r454LyKbUhkXjgXPSQ/286Nnql3BVPbcu0i4/fkflHdiO3/omFdb0M/n2iH6EXFlLEGM9lmOm80YSk/rKx/t4MDI9UVaNCl+n35H8IQonF8sLLtLgx6J6lubQ/qKBMVZGbmep0p0PFcjxdBA31gF8k36hu5gqWxgLJMnVPHoxg/u/ys5Duln2esOXcvKt/pvBBlxBCqOOfLpIqvbUEf2kAJ/wTVuC3j9k3Unn4j6n9CpuyPp2+y+lfuRX+pFhYDpc0gu81Xy+4YGK+EQfdfhsOi56eByJOK4l6rfrFywZsY0Xn5qRUNnZO6Kflw7c4Ya6ANTeAde3seLP+1n8O60fI7Po0bWtpFI2tnR904i/PP586Ynx76vsTzyHe48ontIP4AndXQoYBL+XfpTervl+SDEDFfCx1d5zAK6t89b4J/VdBBbuPGO87T8f6ic2Odmn15x+x/z3l/oB37uq/jXBUuunl4Ih7UxUtQKPpBrv9A/Yf2t/en70AAdVmiBcpmV7a3+1/oFtXURZYvvOzgh9BofeYP+03FFhquJX1C9Wh2RVr0jXkehaG/FZvqlcWMrgcip0f+rN8U/P5+km2Q4saOvu+ah+bqdKjmjdh2nYUo3d/9D5117ZfiqUiuOMuHJrmSrdreyWxVbvLo5VHKRWB7iqgyzQDxBwXSTcdYtbd4PWD9kIEgrMgbWz72RhthO45yeZbQVHrsOcO2dXhduLyzjOfwnJbGdWyOLGXEhKFlmknl1TQC/tLox7zsGIL7JR6cSUtIuuaMz85Dr+5kLLYmgYlbdzvrfM9LI6Djs9rMILY+3LTrZ+efnxtJyyWoDyad67zmYvWfFd1zaty8vtepSaRnbUKYq+pjyxzE5aJwCDnZnJ668v5YGmj7O/W5etJy9VXjSZzDRrbQCiWBrq9115qrK/h8tl6+8V54217s2eVMsNtZgdTBAXaEz+1y0gl/tCN9bYZjWziNUW5tUDvG4cmU2acsOu5vbiM5OtudTWDqnbWYb0lMVz/StpJ0XpePBO9PiohHkkSHrm5l5k795Htoe2k8+4vp8VRYrzpgYeDiknpLz1UMriDK4WQrbTYbidYe7UgKB+SPcLDFjJ3hirFi+XKD8/sxeRSh3d2+X90ipH6Vc6jJo7qdx9ttj4dvk5490tSvP0lb4V2/QjWMHHfn7r5ddH9M/CfH7wVNvxf2D/uUI7COs0/UDZh8PN51cpI/RdMWpRWkZAiAoQQfMz/PwqxJS4s/P8WUOwfNQ908/wi6g7v9hSVC+K4uKQovy3P7fY+1+fVfyxUqLhZPiZcCgCogOOvXd07s8X1M/UnLN4+mmp7TeN1L7EREp+BK+sH6rZ21fl+gernP1XmCAIgiAIgiAIgiAIgvz/cMaNMgRBEARBEARBEARBkN8C7L8gCIIgCIIgCIIgCIIgCIIgvyX/Ac0P6tfK1n6sAAAAAElFTkSuQmCC-->
				</div>
                <div> 

                <aside>
                    <ul><b>
                    <li><div class="icons"><i class="fa fa-search"></i></div>
					
					<div class="linktitle" style="{{ (request()->routeIs('getsearchorders')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('getsearchorders') }}" style="text-decorations:none; color:inherit;">Global Search </a></div></li>
					<li><div class="icons"><i class="fas fa-store-alt"></i></div>
					
					<div class="linktitle" style="{{ (request()->routeIs('getinstoreorders')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('getinstoreorders') }}" style="text-decorations:none; color:inherit;">In Store Payment (<span id="instoreorderscount"></span>)</a></div></li>

					<li><div class="icons"><i class="fa fa-info-circle"></i></div>
					@if($currentRouteName =='getsearchorders')
					<div class="linktitle" style="{{ (request()->routeIs('getallorders')) ? 'color: #0f82da;' : '' }}">
					@elseif($currentRouteName =='login')
					<div class="linktitle" style="{{ (request()->routeIs('login')) ? 'color: #0f82da;' : '' }}">
					@else
					<div class="linktitle" >	
					@endif
					<a href="{{ route('getallorders') }}" style="text-decorations:none; color:inherit;">Quote Required (<span id="inboxorderscount"></span>)</a></div></li>
					
                    <li><div class="icons"><i class="fa fa-clock-o"></i></div>
					<div class="linktitle" style="{{ (request()->routeIs('awaitingauthorisation')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('awaitingauthorisation') }}" style="text-decorations:none; color:inherit;">Awaiting Authorisation (<span id="authoraisecount"></span>)</a></div></li>
                    <li><div class="icons"><i class="fa fa-check-circle"></i></div>
					<div class="linktitle" style="{{ (request()->routeIs('readytoprocess')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('readytoprocess') }}" style="text-decorations:none; color:inherit;">Ready To Process (<span id="processcount"></span>)</a></div></li>
                    <li><div class="icons"><i class="fa fa-car"></i></div>
					<div class="linktitle" style="{{ (request()->routeIs('deliverylist')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('deliverylist') }}" style="text-decorations:none; color:inherit;">Delivery List (<span id="deliverycount"></span>)</a></div></li>
                    <li><div class="icons"><i class="fa fa-shopping-basket" aria-hidden="true"></i></div>
					<div class="linktitle" style="{{ (request()->routeIs('pickuplist')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('pickuplist') }}" style="text-decorations:none; color:inherit;">Pickup List (<span id="pickupcount"></span>)</a></div></li>
					<li><div class="icons"><i class="fa fa-truck"></i></div>
					<div class="linktitle" style="{{ (request()->routeIs('transitlist')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('transitlist') }}" style="text-decorations:none; color:inherit;">In Transit (<span id="transitcount"></span>)</a></div></li>
                    <li><div class="icons"><i class="fa fa-th-list" aria-hidden="true"></i></div>
					<div class="linktitle" style="{{ (request()->routeIs('completedorders')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('completedorders') }}" style="text-decorations:none; color:inherit;">Completed Orders </a></div></li>
					<li><div class="icons"><i class="fa fa-ban"></i></div>
					<div class="linktitle" style="{{ (request()->routeIs('cancelledorders')) ? 'color: #0f82da;' : '' }}">
					<a href="{{ route('cancelledorders') }}" style="text-decorations:none; color:inherit;">Cancelled Orders </a></div></li>   </b>
                    </ul>
                </aside>
                </div>
         </div>

         <!-- right -->
         <div class="main-content">

           <div class="topbar" style="padding-bottom: 0px;display: flex;flex-direction: row;">
                
            <nav class="navbar navbar-expand-lg navbar-light" style="width: 100%;">

            <div class="collapse navbar-collapse" id="navbarTogglerDemo01" style="display: flex;flex-direction: row;">
            
                <div class="toggler">
                  <i class="fa fa-bars"></i>
                </div>
              
              <div style="margin-left: auto;">
              <ul class="navbar-nav mr-auto flex-row" style="align-items: center;">
                <!--<li class="nav-item active" style="border-left: 1px solid #898b8b;height: 100%;border-right: 1px solid #898b8b;padding-right: 15px;padding-left: 15px;">
                    <i class="fa fa-envelope fa-2x" style="color:rgb(139, 138, 138)"></i>
                </li> -->
                <li class="nav-item active" style="border-left: 1px solid #898b8b;height: 100%;border-right: 1px solid #898b8b;padding-right: 20px;padding-left: 20px; padding-top: 20px; position: inherit;overflow:inherit;display:none;">
                     <span class="badge" id="orderscount"></span>
					<i class="fa fa-bell fa-x" style="color:rgb(139, 138, 138)"></i>					
                    <ul class="sm">
                      <li><a href="{{ route('getallorders') }}" title="New Orders">New Orders - <span id="neworders"></span></a></li>
                      <li><a href="{{ route('readytoprocess') }}" title="payment done">Payment orders - <span id="paymentorders"></span></a></li>
                      <li><a href="{{ route('cancelledorders') }}" title="cancelled">Cancelled Orders - <span id="cancelledorders"></span></a></li>
                  </ul>
                </li>
				@if (Auth::check())
                <li class="nav-item" style="height: 100%;padding-right: 10px;padding-left: 15px;padding-top: 0px;">
                @if(isset(Auth::user()->profilepic))
                  <img src="{{asset('profilepic').'/'.Auth::user()->profilepic}}" width="40px" height="40px" alt="">
				      	@else
				        	<img src="{{asset('profilepic').'/default_profile.png'}}" width="40px" height="40px" alt="">	
				      	@endif
                </li>
				@endif
              </ul>
            </div>
				
              <div class="btn-group" style="padding-left: 0px;margin-left: 0px;">
			  @if (Auth::check())
                <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: white;">
                  {{Auth::user()->firstname}}
                </button>
				@endif
                <div class="dropdown-menu">
                 <a class="dropdown-item" href="{{route('settings')}}">Settings</a>
                  <!-- <a class="dropdown-item" href="#">Profile</a> -->
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">Logout</a>
				  <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
                </div>
              </div> 
			
            </div>
          </nav> 

       </div>

	 @yield('content')
	 <script type="text/javascript">
	 var url=window.location.href;
	 part=url.split('/');
	 function gettotalorderscount()
	{
		
            $.ajax({
				
                type: 'GET',              
                url: "{{ route('totalorderscount') }}",				
                success: function(result){
                   console.log(result);
				   if(result > 0)
				  document.getElementById("orderscount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });

	}
	function getneworderscount()
	{
		
            $.ajax({
				
                type: 'GET',              
                url: "{{ route('neworderscount') }}",				
                success: function(result){
                   console.log(result);
				   if(result > 0)
				  document.getElementById("neworders").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });

	}
	function getpaymentorderscount()
	{
		
            $.ajax({
				
                type: 'GET',              
                url: "{{ route('paymentorderscount') }}",				
                success: function(result){
                   console.log(result);
				   if(result > 0)
				  document.getElementById("paymentorders").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });

	}
	function getcancelledorderscount()
	{
		
            $.ajax({
				
                type: 'GET',              
                url: "{{ route('cancelledorderscount') }}",				
                success: function(result){
                   console.log(result);
				   if(result > 0)
				  document.getElementById("cancelledorders").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });

	}
	setInterval(function () { gettotalorderscount();}, 6000);
	setInterval(function () { getcancelledorderscount(); }, 6000);
	setInterval(function () { getpaymentorderscount(); }, 6000);
	setInterval(function () { getneworderscount(); }, 6000);
	if(part[4]!=='vieworder'||part[4]!=='editorder'||part[4]!=='settings'||part[4]!=='changepharmacy')
  {
    // do nothing
    // console.log('from if')
  }
  else{
  	setInterval(function () { window.location.reload(true); }, 60000);    
  }
	
	
	function getinboxorderscount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('inboxorderscount') }}",				
                success: function(result){
                   console.log(result);
				   
				  document.getElementById("inboxorderscount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	function getinstoreorderscount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('instoreorderscount') }}",				
                success: function(result){
                   console.log(result);
				   
				  document.getElementById("instoreorderscount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	
	function getauthoraisecount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('authoraisecount') }}",				
                success: function(result){
                   console.log(result);
				  
				  document.getElementById("authoraisecount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	function getprocesscount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('processcount') }}",				
                success: function(result){
                   console.log(result);
				  
				  document.getElementById("processcount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	function getdeliverycount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('deliverycount') }}",				
                success: function(result){
                   console.log(result);
				  
				  document.getElementById("deliverycount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	function getpickupcount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('pickupcount') }}",				
                success: function(result){
                   console.log(result);
				   
				  document.getElementById("pickupcount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	function gettransitcount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('transitcount') }}",				
                success: function(result){
                   console.log(result);
				   
				  document.getElementById("transitcount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	function getcompletedcount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('completedcount') }}",				
                success: function(result){
                   console.log(result);
				   
				  document.getElementById("completedcount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	function getcancelledcount()
	{
		$.ajax({
				
                type: 'GET',              
                url: "{{ route('cancelledcount') }}",				
                success: function(result){
                   console.log(result);
				   
				  document.getElementById("cancelledcount").innerHTML = result;
               },
                error: function(result){
                    return result;
               }
           });
	}
	$(document).ready(function(){
		getinboxorderscount();
		getinstoreorderscount();
		getauthoraisecount();
		getprocesscount();
		getdeliverycount();
		getpickupcount();
		gettransitcount();
		getcompletedcount();
		getcancelledcount();
	});
  </script>
  <script>
    $('.toggler').click(function(){
      if( ! $('.left-bar').is(':visible') ) {
          $('.left-bar').fadeIn();
            $(this).css('margin-left', '36%');
        } else {
          $('.left-bar').fadeOut();
            $(this).css('margin-left', '0');
        }
    });
  </script>
	</body>
</html>