@extends('header')

@section('title','| ViewOrder')

@section('content')


<div style="width: 100%;">
      
      <div class="orderbar" style="margin-bottom: 0px;padding-bottom: 0px;">
        <nav class="navbar navbar-expand-lg navbar-light" style="width: 100%;">
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item"
                style="height: 100%;font-size: 14px;padding-top: 15px;font-weight: 500;">
                <p class="col-white">Order: {{$order->uniqueorderid}}</p>
              </li>
              <li class="nav-item"
                style="height: 100%;font-size: 14px;padding-top: 15px;font-weight: 500;padding-left: 20px;">
                <p class="col-white">Date: {{date('d M Y', strtotime($order->orderDate))}}</p>
              </li>
              <li class="nav-item" style="font-weight: 500;font-size: 14px;padding-top: 15px;padding-left: 20px;">
                <p class="col-white">Status: {{$order->myorderstatus}}</p>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="titlebar" style="margin-bottom: 0px;padding-bottom: 0px;">
        <nav class="navbar navbar-expand-lg navbar-light" style="width: 100%;">
          <div class="logo">
            <img width="210px" height="50px" src="{{asset('pharmacy/portal').'/'.$order->logo}}" alt="Pharmacy Logo">
          </div>
          <div style="text-align: end;width: 100%;font-size: 14px;font-weight: 400;line-height: 18px;">
            <p style="margin: 0;">{{$order->locationname}}</p>
            <p style="margin: 0;">{{$order->address}}</p>
          </div>
        </nav>
      </div>
	  
	 
      <div class="scriptbar" style="margin-bottom: 0px;padding-bottom: 0px;display: flow-root;">
        <h3>Scripts</h3>
		@if(count($originalscriptslist)>0)
			@foreach($originalscriptslist as $oitemdetails)	
			
			@if(isset($oitemdetails->prescriptiontype))	
			
				
			<div class="gallery" id="gallery{{$oitemdetails->prescriptionid}}">
			
			  @if(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "Medisecure")
				@if(strpos($oitemdetails->barcode,'http')==false)
				<a target="_blank" href="{{ env('MK_SCRIPT_URL_MEDISECURE') }}{{$oitemdetails->barcode}}">
				@else
				<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
		  <div id='DivIdToPrint'>
				@if(strpos($oitemdetails->barcode,'http')==false)
				<iframe src="{{ env('MK_SCRIPT_URL_MEDISECURE') }}{{$oitemdetails->barcode}}"  width="1024" height="768">
				@else
				<iframe src="{{$oitemdetails->barcode}}"  width="1024" height="768">
				@endif
			   
				 </div>
			  </a>
			@elseif(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "ERX")  
				@if(strpos($oitemdetails->barcode,'http')==false)
					<a target="_blank" href="{{ env('MK_SCRIPT_URL_ERX') }}{{$oitemdetails->barcode}}">
				@else
					<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
				<div id='DivIdToPrint'>
				@if(strpos($oitemdetails->barcode,'http')==false)
				<iframe src="{{ env('MK_SCRIPT_URL_ERX') }}{{$oitemdetails->barcode}}"  width="1024" height="768">
				@else
					<iframe src="{{$oitemdetails->barcode}}"  width="1024" height="768">
				@endif
			   
				 </div>
			  </a>
			@else
				<a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}">
			<div id='DivIdToPrint'>
			   
				<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
			  </div>
			  </a>
			   
			
			  <div class="desc">
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv({{$oitemdetails->prescriptionid}});">Print</button>
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
          </div>
			</div>
			@endif
				<!--@if( $oitemdetails->prescriptiontype == 3)
				<div class="gallery">
				
				  <a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}">
				  <div id='DivIdToPrint'>
					<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
				  </div>
				  </a>
				 
				  <div class="desc">
					<button type="submit" class="btn btn-block mybtn"
					  style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv();">Print</button>
					<button type="submit" class="btn btn-block mybtn"
					  style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
				  </div>
				</div>
				@endif-->
			@endif
			
			@endforeach
	   @else
		   
		   @foreach($scriptslist as $oitemdetails)	
		  
				@if(isset($oitemdetails->prescriptiontype))	
				
				<div class="gallery" id="gallery{{$oitemdetails->prescriptionid}}">
				
				 @if(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "Medisecure")
			  @if(strpos($oitemdetails->barcode,'http')==false)
				<a target="_blank" href="{{ env('MK_SCRIPT_URL_MEDISECURE') }}{{$oitemdetails->barcode}}">
				@else
				<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
		  
			@elseif(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "ERX")  
				@if(strpos($oitemdetails->barcode,'http')==false)
				<a target="_blank" href="{{ env('MK_SCRIPT_URL_ERX') }}{{$oitemdetails->barcode}}">
				@else
				<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
			@else
				<a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}">
			@endif
				  <div id='DivIdToPrint'>
					@if(isset($oitemdetails->prescriptionimage))
					<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@else
					<img src="{{asset('prescriptions').'/default_qrcode.png'}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@endif
				  </div>
				  </a>
				
				  <div class="desc">
					<button type="submit" class="btn btn-block mybtn"
					  style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv({{$oitemdetails->prescriptionid}});">Print</button>
					<button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
				  </div>
				</div>
					<!--@if( $oitemdetails->prescriptiontype == 3)
					<div class="gallery">
					
					  <a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}">
					  <div id='DivIdToPrint'>
						<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					  </div>
					  </a>
					 
					 <div class="desc">
            <button type="submit" class="btn btn-block mybtn"
              style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv();">Print</button>
            <button type="submit" class="btn btn-block mybtn"style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
          </div>
					</div>
					@endif-->
				@endif
       @endforeach
	   @endif  
	  
      </div> 

      <div class="patientdetails" style="margin-bottom: 0px;padding-bottom: 20px;display: flow-root;">
        <h3 style="margin-bottom: 12px;">Patient Details</h3>
        <div class="details">
          <ul>
            <li><b>First Name  Last Name:</b><br>{{$order->fname}} {{$order->lname}}</li>
			<li><b>Date of Birth:</b><br>
			@if(isset($order->dateofbirth))
			{{date('d M Y', strtotime($order->dateofbirth))}}
			@endif 
			</li>
			<li><b>Gender:</b><br>{{$order->gender}}</li>           
            <li><b>Email:</b><br>{{$order->emailaddress}}</li>
			<li><b>Mobile:</b><br>{{$order-> mobilenumber}}</li>
			@if(!empty($useraddress))
			@foreach($useraddress as $addressVal)
			
            <li><b>Address :</b><br>{{$addressVal->addressline1}} , {{$addressVal->addressline2}}<br>
              {{$addressVal->suburb}}, {{$addressVal->state}}, {{$addressVal->postcode}}</li>@endforeach
			@endif 
			
          </ul>
        </div>
		<div>
		@foreach($locationinfo as $locationval)
		<input type="hidden" id="actualdeliveryfee" name="actualdeliveryfee" value="{{$locationval->deliveryfee}}">
		@endforeach
		</div>
        <div class="details">
          <ul>
            <!--<li>
              <label>Generic Alternative?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rbrand" value="no" {{ ($order->isGeneric=="0" || $order->isGeneric==" ")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rbrand" value="yes" {{ ($order->isGeneric=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>-->
            <li>
              <label>Need to Speak with Pharmacist?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPharmacist" value="no"  {{ ($order->speakwithpharmacist=="0" || $order->speakwithpharmacist==" ")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPharmacist" value="yes"  {{ ($order->speakwithpharmacist=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
            <li>
              <label>Special request to Pharmacy</label><br>
              <textarea class="form-control mat-5" rows="3" readonly="true">{{$order->orderinstructions}}</textarea>
            </li>
          </ul>
        </div>		
		@if(!empty($healthprofile))
		@foreach($healthprofile as $healthinfo)
        <div class="details">
          <ul>
            <li>
              <label for="medicareno">Medicare Number</label><br>
              <input type="text"  class="form-control mat-5" id="medicareno" readonly="true" value="{{\Illuminate\Support\Str::limit($healthinfo->medicarenumber, 10, $end='')}}"> 
            </li>
            <li>
              <div class="row" style="display: contents;width: 100%;">
                <div style="width: 48%;margin-right: 10px;">
                  <label for="medicareno">Ref Num</label><br>
                  <input type="text" class="form-control mat-5" id="medicarepos" readonly="true" value="{{\Illuminate\Support\Str::substr($healthinfo->medicarenumber , 10)}}">
                </div>
                <div style="width:48%;">
                  <label for="medicareno">Medicare Card Expiry</label><br>
                  <input type="text" class="form-control mat-5" id="medicareno" readonly="true" value="{{$healthinfo->medicare_exp}}"> 
                </div>
              </div>
            </li>
            <li>
              <label for="medicareno">Pension / Healthcare Card Number</label><br>
              <input type="text" class="form-control mat-5"  id="medicareno" readonly="true" value="{{$healthinfo->heathcarecard}}{{$healthinfo->pensionerconcessioncard}}">
            </li>
            <li>
              <label for="medicareno">Safety Net / Consession Safety Net</label><br>
              <input type="text" class="form-control mat-5" id="medicareno" readonly="true" value="{{$healthinfo->safetynet}}{{$healthinfo->concessionsafetynetcard}}">
            </li>
          </ul>
        </div>
		
        <div class="details">
          <ul>
            <li>
              <label>Allergies</label><br>
              <textarea class="form-control" rows="3" readonly="true">{{str_replace('"','',str_replace(']','',str_replace('[','',$healthinfo->allergies)))}}</textarea>
            </li>
            <li>
              <label>Medical Conditions</label><br>
              <textarea class="form-control" rows="3" readonly="true">{{str_replace('"','',str_replace(']','',str_replace('[','',$healthinfo->medicalconditions)))}}</textarea>
            </li>
          </ul>
        </div>
        <div class="details">
          <ul>
            <li>
              <label>Smoker?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rnonSmoker" value="no" {{ ($healthinfo->smoking=="0")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rnonSmoker" value="yes" {{ ($healthinfo->smoking=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
			@if($order->gender == "Female")
            <li>
              <label>Pregnant?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPregnant" value="no" {{ ($healthinfo->pregnency=="0")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPregnant" value="yes" {{ ($healthinfo->pregnency=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
			@if($healthinfo->pregnency == "1")
            <li>
              <label>Breasfeeding?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rBreasfeeding" value="no" {{ ($healthinfo->lactation=="0")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rBreasfeeding" value="yes" {{ ($healthinfo->lactation=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
			@endif
			@endif
          </ul>
        </div>
      </div>
@endforeach
@endif

@if(count($originalscriptslist)>0)
@if(!empty($originalscriptslist))
        <div class="revieworder">
        <h3 style="margin-bottom: 25px;">Order</h3>
		<!--- Original Order --->
        <div class="table-responsive">
          <h6>Original Order Items</h6>
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
               <!-- <th scope="col">UOM</th>-->
                <th scope="col">Unit $</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
              </tr>
            </thead>
            <tbody>
			
			 @foreach($originalscriptslist as $k => $originalitemdetails)
			
              <tr>
                @if(!empty($originalitemdetails->itemimage))
				<img src="{{$originalitemdetails->itemimage}}" width="50px" height="50px">
				@endif
                <td>{{$originalitemdetails->prescriptionid}}</td>
                <td><p>{{$originalitemdetails->drugname}}<br>@if($originalitemdetails->isgeneric==1)<span style="color:red;padding-left:5%">Generic Alternative Chosen<span>@endif</p></td>
                <!--<td>{{$originalitemdetails->drugquantity}}</td>-->
                <td>{{$originalitemdetails->originalprice}}</td>
                <td>{{$originalitemdetails->drugpack}}</td>               
                <td style="text-align:right;">{{$originalitemdetails->originalprice * $originalitemdetails->drugpack}}</td>
              </tr>
			 
        @endforeach  
		
            </tbody>
          </table>
        </div>
	@endif	
	@endif	

     
		<br>
		<!--- Original Order End --->
        </div> 
		
		<!----------------------------Current Order Screen ------------------------>
		
		
		<div class="revieworder">
        <h3 style="margin-bottom: 25px;">Order</h3>
		<!--- Original Order --->
        <div class="table-responsive">
          <h6>Current Order Items</h6>
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
                <!--<th scope="col">UOM</th>-->
                <th scope="col">Unit $ (inc GST)</th>
                <th scope="col">Qty</th>
                <th scope="col">Price  (inc GST)</th>
              </tr>
            </thead>
            <tbody>
			
			 @foreach($scriptslist as $k => $itemdetails)
			
              <tr>
			  <td>
                @if(!empty($itemdetails->itemimage))
				<img src="{{$itemdetails->itemimage}}" width="50px" height="50px">
				@endif</td>
                <td>{{$itemdetails->prescriptionid}}</td>
                <td><p>{{$itemdetails->drugname}}<br>@if($itemdetails->isgeneric==1)<span style="color:red;padding-left:5%">Generic Alternative Chosen<span>@endif</p></td>
                <!--<td>{{$itemdetails->drugquantity}}</td>-->				
                <td>{{$itemdetails->originalprice}}</td>				
                <td>{{$itemdetails->drugpack}}</td> 
				@if($itemdetails->originalprice > 0 && $itemdetails->drugpack > 0)				
                <td class="price" style="text-align:right;">{{$itemdetails->originalprice * $itemdetails->drugpack}}</td>
				@elseif($itemdetails->originalprice == 0 && $itemdetails->drugpack > 0 && $itemdetails->prescriptiontype!=3 && $itemdetails->istbc=='')				
                <td class="price" style="text-align:right;">{{$itemdetails->originalprice * $itemdetails->drugpack}}</td>
				@else
				<td  style="text-align:right;" class="price">TBC</td>
				@endif
              </tr>
			 
        @endforeach      
            </tbody>
          </table>
        </div>


     
		<div class="form-control">
		<table class="table">
			 @if($order->ordertype=='Delivery')
			<tr>
		<td colspan="6">Delivery Fee</td>
		<td style="float:right;" class="price">
		{{number_format($order->deliveryfee, 2)}}
		</td></tr>@endif
		<tr>
		<td colspan="6">Order Total</td>
		<td id="orderprice"style="float:right;">{{number_format($order->orderTotal, 2)}}
		</td></tr>
		</table>
		</div>
		<br>
		 <div style="width: 40%;display: flex;" id='btn_editorder' style="display:block">
          <button type="button" class="btn btn-block mybtn"id='btnedit' 
            style="font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;">EDIT ORDER 
          </button>
          <a type="button" class="btn btn-block mybtn" href="{{ route('changestatus',['orderstatus'=>'Awaiting Authorisation','orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;" onclick="checktotal()">PROCEED WITH
            ORDER</a>
        </div>
        </div> 
		
		<!----------------------------Current Order End Screen ------------------------>
		
		
		<!----------------------------Edit Order Screen ------------------------>
		
		<div class="revieworder" id="editorder" style="display:none">
        <h3 style="margin-bottom: 25px;">Edit Order</h3>
        <div class="table-responsive">
          <h6>Order Items</h6>
         
		  <div class="form-group clearfix">
                            <table class="table" id="customFields">
                                <tr>
									<th class="asia">Item ID</th>
                                    <th class="asia">Item Description</th>
                                    <!--<th class="asia">UOM</th>-->
                                    <th class="asia">Unit $ (inc GST)</th>
                                    <th class="asia">Qty</th>
                                    <th class="north motorcycle">Price (inc GST)</th>
                                    
                                       </tr>
									  @foreach($scriptslist as $k => $itemdetails) 
                                       <tr>
			                               <td class="asia"><input type="text" class="form-control" name="itemid[]" id="itemid{{$k+1}}" readonly data-toggle="tooltip" data-html="true" title="<img src='{{$itemdetails->itemimage}}'>"  required value="{{$itemdetails->prescriptionid}}"><input type="hidden" name="isscript[]" id="isscript{{$k+1}}" value="{{$itemdetails->isscript}}"></td>
                                           <td class="asia"><input type="text" class="form-control" name="itemdesc[]" id="itemdesc{{$k+1}}"required value="{{$itemdetails->drugname}}" onclick="searchdrug(this)"><div id="suggesstion-box{{$k+1}}"></div></td>
										   <!--<td class="asia"><input type="text" class="form-control"  name="drugpack[]" id="drugpack{{$k+1}}"  required  value="{{$itemdetails->drugquantity}}"  ></td>-->
										   <td class="asia"><input type="text" class="form-control" name="originalprice[]" id="originalprice{{$k+1}}"  required value="{{$itemdetails->originalprice}}" onchange="priceupdate(this)"><input type="hidden" name="itemimage[]" id="itemimage{{$k+1}}" value="{{$itemdetails->itemimage}}"></td>
										   <td class="asia"><input type="text" class="form-control"  name="drugquantity[]" id="drugquantity{{$k+1}}"  required value="{{$itemdetails->drugpack}}" onchange="quantityupdate(this)"></td>
										   <td class="asia price1" style="text-align:right;"><input type="text" style="text-align:right;" class="form-control" name="price[]" readonly id="price{{$k+1}}" onchange="pricecheck(this)"  required value="{{$itemdetails->drugpack * $itemdetails->originalprice}}"><input type="hidden" name="reference[]" id="reference{{$k+1}}"> </td>
                                        <td><a onclick="remove(this)" class="remCF"><i class="fa fa-trash" style="color:#808080;padding-top:10px;" title="Delete"></i></a></td>
										</tr>
										 @endforeach 
                                       <tr>
                                           <td colspan="6">
                                           <span class="btn btn-info pull-right" id="addnew"  onclick="addfield()"><i class="fa fa-plus"></i></span>
                                           </td> </tr>
                                      
                                       </table>
                                       </div>
                        
        </div>

<br>
       
        <br>
		<div class="form-group">
		<table id="ordertotal" class="table">
		 @if($order->ordertype=='Delivery')
			
			<tr>
		<td colspan="6">Delivery Fee</td>
		<td style="float:right;" class="price1">
		<input class="form-control" type="text"  id="deliveryfee" name="deliveryfee" value='{{number_format($order->deliveryfee, 2)}}' onchange="updatedeliveryfee()">
		</td></tr>
		@endif
		<tr>
		<td colspan="6">Order Total:</td>
		<td id="getprice" style="float:right;"></td>
		</tr>
		
		</table>
		
		</div>
        <div style="width: 40%;display: flex;">
          <button type="submit" class="btn btn-block mybtn" onclick="getdataintables({{$order->orderid}},{{$order->myuserid}})"
            style="font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;">CONFIRM ORDER
          </button>
          
        </div>

      </div>
		<!-------------------------------------------------------------------------------->
		
			<div class="patientdetails" style="margin-bottom: 0px;padding-bottom: 20px;display: flow-root;">
        <h3 style="margin-bottom: 12px;">{{$order->ordertype}} Details</h3>
        <div class="details">
          <ul>
		  @if($order->ordertype == "Delivery")
            <li><b>{{$order->ordertype}} Address:</b><br>{{$order->addr1}}{{$order->addr2}}<br>{{$order->addsuburb}} , {{$order->addstate}} , {{$order->addpostcode}}</li>
		  @endif
            <li><b>Phone:</b><br>{{$order->mobilenumber}}</li>
            <li><b>Slot Details:</b><br>{{$order->userpreffereddate}} {{$order->userprefferedtime}}</li>
            
          </ul>
        </div>
        
    
      </div>
		<br><br>
			<div style="width: 40%;display: flex;">
          <a type="button" class="btn btn-block mybtn" onclick="return myFunction();" href="{{ route('cancelOrder',['orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> CANCEL ORDER
          </a>
         <div></div>			
        </div>
		
       <br>
       <br>
	   
      </div>
    </div>
  </div>
  <script src="" async defer></script>
 
 
  <script type="text/javascript">
  $(document).ready(function(){
	  $('[data-toggle=tooltip]').tooltip(); 
  var sum = 0;
// iterate through each td based on class and add the values
$(".price").each(function() {

    var value = $(this).text();
    // add only if the value is number
    if(!isNaN(value) && value.length != 0) {
        sum += parseFloat(value);
    }
});
$('#orderprice').html(sum.toFixed(2));
var sumedit = 0;
// iterate through each td based on class and add the values
$(".price1").each(function() {
	
    var value = $(this).find('input').val();
	//var value1 =$(this).find('input').val("sprice");
    // add only if the value is number
    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
		//sumedit +=parseFloat(value1);
    }
	 if(sumedit>=50){
      $("#deliveryfee").val('0');
      $("#deliveryfee").attr('readonly', true); 
    }
});
$('#getprice').html(sumedit.toFixed(2));

  });
  function priceupdate(o){
	var fid=o.id;
	var idd=fid.split('originalprice');
	var id=idd[1];
	var unitprice=$(o).val();
	var quantity=$("#drugquantity"+id).val();
	var pr=unitprice*quantity;
	var actualdelivery = $("#deliveryfee").val();
	$('#price'+id).val(pr);
		var sumedit = 0;
	$(".price1").each(function() {
	
    var value = $(this).find('input').val();
	    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
		//sumedit +=parseFloat(value1);
    }
	 if(sumedit<=50){
      $("#deliveryfee").val(actualdelivery);
      $("#deliveryfee").attr('readonly', true); 
    }
	else
	{
		$("#deliveryfee").val('0');
        $("#deliveryfee").attr('readonly', false);
	}
});
$('#getprice').html(sumedit.toFixed(2));
}
function checktotal(){
	var sum=$('#orderprice').text();
	var su=0;
	var tbc=0;
	$(".price").each(function() {
        if($(this).text()=="0"||$(this).text()=="0.00"){
			su=su+1;
		}
		if($(this).text()=="TBC"){
			tbc=tbc+1;
		}
});
if(tbc>0){
	alert("Price need to Update for Item");
	$(".price").each(function() {
       	if($(this).text()=="TBC"){
			 $(this).css({'border':'1px solid red'});
		}
});
	event.preventDefault();	
}
	if(su>0){
		$(".price").each(function() {
       	if($(this).text()=="0" || $(this).text()=="0.00"){
			 $(this).css({'border':'1px solid red'});
		}
		});
	if(!confirm("Are You Sure you want proceed with this order"))
      event.preventDefault();
  }	
}
function quantityupdate(q){
	var fid=q.id;
	var idd=fid.split('drugquantity');
	var id=idd[1];
	var unitprice=$("#originalprice"+id).val();
	var quantity=$(q).val();
	var pr=unitprice*quantity;
	var actualdelivery = $("#deliveryfee").val();
	$('#price'+id).val(pr);
		var sumedit = 0;
	$(".price1").each(function() {
	
    var value = $(this).find('input').val();
	    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
		//sumedit +=parseFloat(value1);
    }
	 if(sumedit>=50){
      $("#deliveryfee").val('0');
      $("#deliveryfee").attr('readonly', true); 
    }
	else
	{
		 $("#deliveryfee").val(actualdelivery);
		 $("#deliveryfee").attr('readonly', false); 
	}
});
$('#getprice').html(sumedit.toFixed(2));
}

 var totalprice="{{$order->orderTotal}}";
  $("#btnedit").click(function(){
  $("#btn_editorder").hide();
  $("#editorder").show();
});
 function addfield(){
        var row=jQuery('#customFields tr').length;
        var len=row-1;
        var inc=len-1;
      jQuery("#customFields").find('tr:last').prev().after('<tr><td class="asia"><input type="text" readonly class="form-control" name="itemid['+inc+']" id="itemid'+len+'"  required data-toggle="tooltip" data-html="true" title=""><input type="hidden" name="isscript[]" id="isscript'+len+'" ></td><td class="asia"><input type="text"  name="itemdesc['+inc+']" required class="form-control" id="itemdesc'+len+'" onclick="searchdrug(this)" ><div id="suggesstion-box'+len+'"></div></td><td class="asia"><input type="text" name="originalprice['+inc+']"  required class="form-control" id="originalprice'+len+'" onchange="priceupdate(this)"><input type="hidden" name="itemimage[]" id="itemimage'+len+'"></td><td class="asia"><input type="text" name="drugquantity['+inc+']" id="drugquantity'+len+'" class="form-control" onchange="quantityupdate(this)"><input type="hidden" name="reference['+inc+']" id="reference'+len+'"></td><td class="north motorcycle price1" style="text-align:right;"><input type="text" name="price['+inc+']" id="price'+len+'" onchange="pricecheck(this)" class="form-control" readonly style="text-align:right;"></td><td><a onclick="remove(this)" class="remCF"><i class="fa fa-trash" style="color:#808080;padding-top:10px;" title="Delete"></i></a></td></tr>');
           }
 function remove(el){
        jQuery(el).parent().parent().remove();
		var sumedit = 0;
	$(".price1").each(function() {
	
    var value = $(this).find('input').val();
	    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
		//sumedit +=parseFloat(value1);
    }
	 if(sumedit>=50){
      $("#deliveryfee").val('0');
      $("#deliveryfee").attr('readonly', true); 
    }
});
$('#getprice').html(sumedit.toFixed(2));
        }
function addfield1(){
        var row=jQuery('#customFields1 tr').length;
        var len=row-1;
        var inc=len-1;
      jQuery("#customFields1").find('tr:last').prev().after('<tr><td class="asia"><input type="text"  class="form-control" name="sitemid['+inc+']" id="sitemid'+len+'"  required></td><td class="asia"><input type="text"  name="sitemdesc['+inc+']" required class="form-control" id="sitemdesc'+len+'" onclick="searchdrug1(this)" ><div id="ssuggesstion-box'+len+'"></div></td><td align="center" class="asia"><input type="text" class="form-control email-input" required name="sdrugpack['+inc+']"  id="sdrugpack'+len+'" ></td><td class="asia"><input type="text" name="soriginalprice['+inc+']"  required class="form-control" id="soriginalprice'+len+'" ></td><td class="asia"><input type="text" name="sdrugquantity['+inc+']" id="sdrugquantity'+len+'" class="form-control" onClick="pricecheck1(this)"><input type="hidden" name="sreference['+inc+']" id="sreference'+len+'"></td><td class="north motorcycle price1"><input type="text" name="sprice['+inc+']" onClick="pricecompute()" id="sprice'+len+'" class="form-control" ></td><td><a onclick="remove1(this)" class="remCF"><i class="fa fa-trash" style="color:#808080;padding-top:10px;" title="Delete"></i></a></td></tr>');
           }
 function remove1(el){
        jQuery(el).parent().parent().remove();
        }
function pricecheck(p){//console.log(p);
	var sumedit = 0;
	$(".price1").each(function() {
	
    var value = $(this).find('input').val();
	    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
		//sumedit +=parseFloat(value1);
    }
	if(sumedit>=50){
      $("#deliveryfee").val('0');
      $("#deliveryfee").attr('readonly', true); 
    }
});
$('#getprice').html(sumedit.toFixed(2));

	
}		
function pricecheck1(p){
	var row = $("#customFields1 td").closest("tr").length;
		var id=row-1;
	var quantity=$(p).val();
	var price=$("#soriginalprice"+id).val();
	var fprice=quantity*price;
	$('#sprice'+id).val(fprice);
}	
function searchdrug(v){ 
	$("#"+v.id).keyup(function(){
		var key=$(this).id;
		var row = $("#customFields td").closest("tr").length;
		var id=row-1;
		//var id=v.id.slice(-1);
		//var id=key.replace(/[^d.,]+/,'');
		//if($(this).val().length>4){
		$.ajax({
		type: "POST",
		url: "{{ route('ajaxGetItemsfromCatalogue') }}",
		data: {
						"_token": "{{ csrf_token() }}",
						"medication": $(this).val(),
						"userid":"{{$order->enduserid}}",
						"locationid":"{{$order->locationid}}"
			  },   
		/*beforeSend: function(){
			$("#"+v.id).css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},*/
		success: function(data){
			console.log(data);
			var html = [];
html.push('<ul id="country-list'+id+'" class="druglist" style="list-style: none;margin-top: -3px;padding: 0;width: 190px;position: absolute;">');
for(var i in data){
   html.push('<li onClick="selectCountry('+data[i].mastercatid+','+id+');">'+data[i].drug+'</li>');
}
html.push('</ul>');
			$("#suggesstion-box"+id).show();
			$("#suggesstion-box"+id).html(html);
			$("#search-box").css("background","#FFF");
		}
		});
		//}
	});
}
function selectCountry(drug,id){
	var location="{{$order->locationid}}";
	var userid="{{$order->enduserid}}";
	//alert(location);
	$.ajax({
		type: "POST",
		url: "{{ route('ajaxGetItemsfromCataloguedata') }}",
		data: {
						"_token": "{{ csrf_token() }}",
						"medication": drug,
						"userid":userid,
						"location":location
			  },  
			success: function(data){
			console.log(data);
			$('#itemid'+id).val(data[0].drugcode);
			$('#itemdesc'+id).val(data[0].drug);
			$('#originalprice'+id).val(data[0].price);
			$('#drugquantity'+id).val(1);
			$('#price'+id).val(data[0].price);
			$("#suggesstion-box"+id).hide();
      $("#isscript"+id).val(data[0].isscript);
      $('#itemimage'+id).val(data[0].itemimage);
	 $('#itemid'+id).prop('title', '<img src="'+data[0].itemimage+'">');
	 
			//totalprice=parseFloat(totalprice)+data[0].price;
			//$("#getprice").html(totalprice);
			var sumedit = 0;
// iterate through each td based on class and add the values
$(".price1").each(function() {

     var value = $(this).find('input').val();
    // add only if the value is number
    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
    }
	if(sumedit>=50){
      $("#deliveryfee").val('0');
      $("#deliveryfee").attr('readonly', true); 
    }
});
$('#getprice').html(sumedit.toFixed(2));

		}
		});
}	
function searchdrug1(v){ 
	$("#"+v.id).keyup(function(){
		var key=$(this).id;
		//var id=v.id.slice(-1);
		var row = $("#customFields1 td").closest("tr").length;
		var id=row-1;
		//var id=key.replace(/[^d.,]+/,'');
		//if($(this).val().length>4){
		$.ajax({
		type: "POST",
		url: "{{ route('ajaxGetItemsfromCatalogueOTC') }}",
		data: {
						"_token": "{{ csrf_token() }}",
						"medication": $(this).val(),
						"userid":"{{$order->enduserid}}",
						"locationid":"{{$order->locationid}}"
			  },   
		/*beforeSend: function(){
			$("#"+v.id).css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},*/
		success: function(data){
			console.log(data);
			var html = [];
html.push('<ul id="scountry-list'+id+'" style="list-style: none;margin-top: -3px;padding: 0;width: 190px;position: absolute;">');
for(var i in data){
   html.push('<li onClick="selectCountry1('+data[i].mastercatid+','+id+');">'+data[i].drug+'</li>');
}
html.push('</ul>');
			$("#ssuggesstion-box"+id).show();
			$("#ssuggesstion-box"+id).html(html);
			$("#search-box").css("background","#FFF");
		}
		});
		//}
	});
}
function selectCountry1(drug,id){
	var location="{{$order->locationid}}";
	var userid="{{$order->enduserid}}";
	//alert(location);
	$.ajax({
		type: "POST",
		url: "{{ route('ajaxGetItemsfromCatalogueOTCdata') }}",
		data: {
						"_token": "{{ csrf_token() }}",
						"medication": drug,
						"userid":userid,
						"location":location
			  },  
			success: function(data){
			console.log(data);
			$('#sitemid'+id).val(data[0].drugcode);
			$('#sitemdesc'+id).val(data[0].drug);
			$('#soriginalprice'+id).val(data[0].price);
			$('#sdrugquantity'+id).val(1);
			$('#sprice'+id).val(data[0].price);
			$("#ssuggesstion-box"+id).hide();
			totalprice=parseFloat(totalprice)+parseFloat(data[0].price);
			$("#getprice").html(totalprice);
			console.log(totalprice);
			var sumedit = 0;
// iterate through each td based on class and add the values
$(".price1").each(function() {

    var value = $(this).text();
    // add only if the value is number
    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
    }
	if(sumedit>=50){
      $("#deliveryfee").val('0');
      $("#deliveryfee").attr('readonly', true); 
    }
});
$('#getprice').html(sumedit.toFixed(2));

		}
		});
}	

  </script>
  <script type="text/javascript">
    $(
      function () {
        $("#test-table").FullTable({          
          "alwaysCreating": true,
          "selectable": false,
          "filterable": false,
          "orderable": false,
          "on": {
            "delete": function(){
              console.log($("#test-table").FullTable("getData"));
            }
          },
          "fields": {
            "itemid": {
              "mandatory": true,
			  "editable": true,
              "errors": {
                "mandatory": "First name is mandatory"
              }
            },
            "itemdesc": {
              "mandatory": true,
			  "editable": true,
              "errors": {
                "mandatory": "Last name is mandatory"
              }
            },
            "qty": {
              "type": "integer",
              "mandatory": true,
			  "editable": true,
              "validator": function (qty) {
                if (qty >= 0) {
                  return true;
                } else {
                  return false;
                }
              },
              "errors": {
                "type": "Qty must be an integer number",
                "mandatory": "Qty is mandatory",
                "validator": "Qty cannot be negative"
              }
            },
            "unit": {
              "type": "decimal",
              "mandatory": true,
			  "editable": true,
              "errors": {
                "type": "unit must be a number",
                "mandatory": "unit is mandatory"
              }
            },
            "id": {
              "mandatory": false
            },
            "uom": {
              "mandatory": true,
			  "editable": true,
            },
            
            "price": {
              "type": "decimal",
			  "editable": false,
              "mandatory": false,
              "errors": {
                "type": "Price must be a number",
                "mandatory": "Price is mandatory"
              }
            },
          }
        });
        $("#test-table-add-row").click(function () {
          $("#test-table").FullTable("addRow");
        });
        $("#test-table-get-value").click(function () {
          console.log($("#test-table").FullTable("getData"));
        });
        $("#test-table").FullTable("on", "error", function (errors) {
          for (var error in errors) {
            error = errors[error];
            console.log(error);
          }
        });
        $("#test-table").FullTable("draw");
      
	  
   
 
      $("#test-table1").FullTable({
		 
          "alwaysCreating": true,
          "selectable": false,
          "filterable": false,
          "orderable": false,
          "on": {
            "delete": function(){
              
            }
          },
          "fields": {
            "itemid": {
              "mandatory": true,
              "errors": {
                "mandatory": "First name is mandatory"
              }
            },
            "itemdesc": {
              "mandatory": true,
              "errors": {
                "mandatory": "Last name is mandatory"
              }
            },
            "qty": {
              "type": "integer",
              "mandatory": true,
              "validator": function (qty) {
                if (qty >= 0) {
                  return true;
                } else {
                  return false;
                }
              },
              "errors": {
                "type": "Qty must be an integer number",
                "mandatory": "Qty is mandatory",
                "validator": "Qty cannot be negative"
              }
            },
            "unit": {
              "type": "decimal",
              "mandatory": true,
              "errors": {
                "type": "unit must be a number",
                "mandatory": "unit is mandatory"
              }
            },
            "id": {
              "mandatory": false
            },
            "uom": {
              "mandatory": true
            },
			
            "price": {
              "type": "decimal",
			  "editable": false,
              "mandatory": true,
              "errors": {
                "type": "Price must be a number",
                "mandatory": "Price is mandatory"
              }
            },
			 
          }
        });
        $("#test-table1-add-row").click(function () {
          $("#test-table1").FullTable("addRow");
        });
        $("#test-table1-get-value").click(function () {
          console.log($("#test-table1").FullTable("getData"));
        });
        $("#test-table1").FullTable("on", "error", function (errors) {
          for (var error in errors) {
            error = errors[error];
            console.log(error);
          }
        });
        $("#test-table1").FullTable("draw");
      }
    );
	function getdataintables(orderid , userinfo)
	{
		
		v1 = $("#test-table").FullTable("getData");
		v2 = $("#test-table1").FullTable("getData");
		var scriptitems=[];
		var storeitems=[];
		var row=jQuery('#customFields tr').length;
		var orderTotal=$("#getprice").html();
		var deliveryfee=$('#deliveryfee').val();
        var len=row-1;
        var inc=len-1;
		ordertotal =0;
		for($i=1;$i<len;$i++){
			
			var obj={};
			obj.itemid=$("#itemid"+$i).val();
			obj.itemdesc=$("#itemdesc"+$i).val();
			obj.drugpack=$('#drugquantity'+$i).val();
			obj.originalprice=$('#originalprice'+$i).val();
			obj.quantity=$('#drugpack'+$i).val();
			obj.price=$('#price'+$i).val();
			obj.reference=$('#reference'+$i).val();
      obj.itemimage=$("#itemimage"+$i).val();
      obj.isscript=$('#isscript'+$i).val();
			
			scriptitems.push(obj);
		}
		var row1=jQuery('#customFields1 tr').length;
        var len1=row1-1;
        var inc1=len1-1;
		for($i=1;$i<len1;$i++){
			ordertotal =0;
			var obj={};
			obj.itemid=$("#sitemid"+$i).val();
			obj.itemdesc=$("#sitemdesc"+$i).val();
			obj.drugpack=$('#sdrugpack'+$i).val();
			obj.originalprice=$('#soriginalprice'+$i).val();
			obj.quantity=$('#sdrugquantity'+$i).val();
			obj.price=$('#sprice'+$i).val();
			obj.reference=$('#sreference'+$i).val();
			obj.itemimage=$("#itemimage"+$i).val();
      obj.isscript=$('#isscript'+$i).val();
			
			storeitems.push(obj);
		}
		// console.log(ordertotal);
		// console.log(scriptitems);
		// console.log(storeitems);
		// alert($('#refferencetable1').val());
		// v2['refferencetable'] = $('#refferencetable1').val();
		
		//var mydata = v1.concat(v2);
		var mydata=scriptitems.concat(storeitems);
		console.log(mydata);
		console.log(mydata);
		
            $.ajax({
				
                type: 'post',              
                url: "{{ route('updateeditorder') }}",
				data: {
						"_token": "{{ csrf_token() }}",
						"orderVal": orderid,
						"userinfo":userinfo,
						"scriptdata" : scriptitems,
						"storedata" : storeitems,
						"ordertotal":orderTotal,
						"deliveryfee":deliveryfee
						
						},    
               
                success: function(result){
					location.href="{{route('awaitingauthorisation')}}";
                    $("#btn_editorder").hide();
					$("#editorder").hide();
               },
                error: function(result){
                    return result;
               }
           });
        
		

	}
  </script>
  
<script>
  function myFunction() {
      if(!confirm("Are You Sure you want cancel this order"))
      event.preventDefault();
  }
  function printDiv(id) 
{

  var divToPrint=document.getElementById('DivIdToPrint');
	var url=$('#gallery'+id+' a').attr("href");
	var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()"><iframe style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;" src="'+url+'"></body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},1000);
}
 function printDiv1() 
{

  var divToPrint=document.getElementById('DivIdToPrint1');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
function printDivBack() 
{

  var divToPrint=document.getElementById('BackImagePrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
function updatedeliveryfee()
{
	var sumedit = 0;
	$(".price1").each(function() {
	
    var value = $(this).find('input').val();
	//var value1 =$(this).find('input').val("sprice");
    // add only if the value is number
    if(!isNaN(value) && value.length != 0) {
        sumedit += parseFloat(value);
		//sumedit +=parseFloat(value1);
    }
}); 

$('#getprice').html(sumedit.toFixed(2));
}

 </script>
@endsection
 