<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>Medmate :: Privacy & Security</title>
</head>
<style>
.header {
    width: 100%;
    text-align: center;
}
.content {
    margin-left: 70px;margin-top: 30px;align-content: space-around;width: 1200px;
}
.footer {
    background-color: #fff;
    height: 120px;
}

</style>

<body>
<div class="container">
    <nav class="navbar navbar-light  d-flex justify-content-center align-items-center" style="width: 100%;background-color: white;">
       <img height="100" width="300" src="https://medmate.com.au/wp-content/uploads/2018/12/medmate_logo_main-01-300x67.png" alt="Medmate" title="" style="max-height: 88px;">
      </nav>
    <div class="col-xs-12 col-sm-6 col-md-8" style="margin-left: 80px;margin-top: 30px;">
    <p><h3>	MedAdvisor takes the privacy and security of your personal health and medical data very seriously.</h3></p>
<p>At MedAdvisor, nothing is more important to us than the protection of your personal information, including data we keep on your behalf regarding your prescriptions and medications.</p>
<p>MedAdvisor has been built as a secure platform with a focus on the privacy and security of a patient's information.</p>
<p>MedAdvisor does not store any information about you until you enrol within the MedAdvisor system and consent to MedAdvisor End User Licence Agreement (EULA).</p>
<p>MedAdvisor DOES NOT store your Medicare number and is not able to link to identifying information beyond the MedAdvisor contact details you specify.</p>
<p>Only your current nominated favourite pharmacy is able to view your script history, and only within a computer secured within their pharmacy. MedAdvisor will only access your personal information when assisting yourself and/or your elected favourite pharmacist with a support query.</p>

		
		<div>
		<h2>Privacy</h2>
		<p><ul>
		<li>Read about our Privacy Policy <a href="https://www.medadvisor.com.au/About/PrivacyPolicy" target="_blank">here</a>. Learn what information MedAdvisor collects about you, why it’s kept, who can access it and under what circumstances it may be accessed.</li>
		<li>MedAdvisor fully complies with the Australian Privacy Act 1988 (Cth) and the Privacy Amendment (Private Sector) Act 2000.</li>
		<li>In addition, MedAdvisor keeps up-to-date with best practices adopted and recommended by groups including Australian Digital Health Agency (ADHA), My Health Record, DoHA and HL7.</li>
		<li>MedAdvisor's Carer Mode© capability allows a user to assist family members with their medication management by adding their information to the carer's MedAdvisor account. Special consent may be required by cared for individuals – read more <a href="https://www.medadvisor.com.au/About/PrivacyPolicy#CarerModePrivacy" target="_blank">here</a>.</li>
		</ul></p>
		</div>
		<div>
		<h2>Security</h2>
		<p><ul>		
		<li>MedAdvisor utilises security technology equivalent to that utilised by other high-security online services such as internet and mobile banking, e-commerce systems (e.g. PayPal), etc.</li>
		<li>This includes the use of SSL/https 128-bit encryption wherever sensitive data is transferred across the internet. It also includes minimum requirements for password strength.</li>
		<li>All MedAdvisor servers are hosted in high quality Australian data centres with locally managed backup and redundancy services. Your personal health and medical data is never sent or stored overseas, and no 3rd party organisations have access to identifiable MedAdvisor patient records.</li>
		</ul></p>
    </div>
    </div>

<footer class="footer">
<div class="col-xs-12 col-sm-6 col-md-8" style="margin-left: 80px;margin-top: 30px;">
      <p style="float: right;"> &copy; copyrights acquired by medmate </p>
    </div>
</footer>
</div>
</body>
</html>