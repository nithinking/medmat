<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Medmate - Order Page</title>
</head>
<style>
.small {
    width: 1200px;background-color: pink;
}
.blue {
    width: 1160px; height: 50px; background-color: #1c9ad5; display: flex; flex-direction: row;padding-left: 50px;
}
.topbar {
    margin-right: 40px; color: white;width: auto;
}
/* p:hover {
    background-color: red; color: #1c9ad5;
} */
.boxes {
    display: flex;flex-direction: row; margin-left: 20px;margin-top: 20px;
}
.greybox {
    width: 210px;height: 200px;background-color: #e3e6ed; margin-right: 10px;
    padding-left: 10px;
}
.one {
    font-weight: 600;font-size: 18px;margin-bottom: 10px;font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
}
.two {
    font-size: 13px; font-weight: lighter;margin-bottom: 5px;margin-top: 0px;font-family: 'Times New Roman', Times, serif; color: #666666;
}
.three {
    color: #303030;font-family: Arial, Helvetica, sans-serif;font-weight: 700;font-size: 14px;margin-bottom: 5px;margin-top: 5px;
}
.new, select {
  width: 90%;
  padding: 8px 20px;
  margin: auto;
  display: inline-block;
  border: 0px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;background-color: #e3e6ed;
}
.new1, select {
  width: 90%;
  padding: 8px 20px;
  margin: auto;
  display: inline-block;
  border: 1px solid #1c9ad5;
  border-radius: 4px;
  box-sizing: border-box;
}
.new1::placeholder {
  color: #1c9ad5;
  font-size: 1.2em;
  font-style: Bold;
}
.i2 {
    width: 720px; height: 40px;border: 1.5px solid #1c9ad5; margin-top: 22px;
}
.i1 {
    width: 120px; height: 40px;margin-left: 50px;border: 1.5px solid #1c9ad5;
}
.p1 {
    margin-left: 50px;margin-top: 5px;margin-bottom: 0px;
}
.p2 {
    margin-top: 5px;margin-bottom: 0px;
}
.logo {
    width: 200px;height: 30px;
}
.gy {   
    width: 400px;height: 200px;background-color: #e3e6ed; margin-right: 10px; margin-left: 30px;margin-top: 10px;
    padding-left: 00px;border: 1px solid #777777;
 
}
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<body>
    <div class="logo">
        <img src="https://medmate.com.au/wp-content/uploads/2018/12/medmate_logo_main-01-300x67.png" alt="" width="200px" height="50px">
    </div>
	
@foreach($orderData as $order)
    <div class="small"><p style="float: right;">{{ $order->created_at}}  <span style="color: #1c9ad5;"> &nbsp;&nbsp; {{ $order->pharmacyName}} </span></p> </div>

    <div class="blue">  <p class="topbar">INBOX</p> <p class="topbar">QUOTO SENT</p> <p class="topbar">READY TO DISPENSE</p>  <p class="topbar">DELIVERY LIST</p> <p class="topbar">PICKUP LIST</p> </div>

    <div class="boxes">
        <div class="greybox">
            <p class="one"> Date / Time Received</p>
            <p class="two">{{$order->statusCreatedat}} </p>
        </div>
        <div class="greybox">
             <p class="one">Order Number</p>
             <p class="two">{{$order->orderid}}</p></div>
        <div class="greybox">
             
             <p class="one">Name</p><p><span><img src="data:image/png;base64,{{$order->myImage}}" alt="" width="40px" height="40px" style="border-radius: 50%;"></span>&nbsp;&nbsp; <span style="height: 20px;"> {{$order->firstName}} {{$order->lastName}}</span></p>
             <p class="two">{{$order->streetAddress}} , {{$order->suite}}  {{$order->town}}  {{ $order->postcode}}.</p>
             <p class="three">Phone:{{$order->mobile}}</p><p class="three">Email:{{$order->emailaddress}}</p></div>
        <div class="greybox">
             <p class="one">Order Summery</p> 
             <p class="three">Prescription:</p>  <p class="two">{{$order->prescriptionTitle}} <span style="margin-left: 110px;"><a href=""> view</a></span></p>
<!--			 <p class="two">Script 2 <span  style="margin-left: 110px;"><a href="">print</a></span>  </p>
             <p class="three">Non-Prescription:</p>  <p class="two">Panamax</p> <p class="two">Panamax</p>-->
           </div>
        <div class="greybox"> 
          <p class="one">Status</p>
     <!--     <p>Total</p> -->
          <p id="id01">{{$order->orderStatus}}</p>
         </div>
    </div>
	
         
           
				<div style="display:flex;flex-direction: row;">
				 @if($order->prescriptiontype == "3")
				<div>
					<p style="margin-left: 30px;font-size: large;margin-bottom: 0px;">{{$order->prescriptionTitle}}</p>
				 <div class="gy">
					 <a href=""><img src="{{url('prescriptions/')}}/{{$order->prescriptionImage}}" alt="" width="400" height="200"></a>
				 </div>
				</div>
					
				
				<div>
					<p style="margin-left: 30px;font-size: large;margin-bottom: 0px;">{{$order->prescriptionTitle}}</p>
				 <div class="gy">
					 <a href=""><img src="{{url('prescriptions/')}}/{{$order->prescriptionBackImage}}" alt="" width="400" height="200"></a>
				 </div>
				</div>
				@endif
				
				</div>
			
	<div style="display:flex;flex-direction: row;margin:20px;">
		<div>
					<p style="margin-left: 30px;font-size: large;margin-bottom: 0px;">Is Generic</p>
				 <div style="margin-left: 30px;font-size: large;margin-bottom: 0px;">
					@if($order->isGeneric == 1) 
					<b>YES</b>
					@else
					<b>NO	</b>
					@endif
				 </div>
				</div>
				<div>
					<p style="margin-left: 30px;font-size: large;margin-bottom: 0px;">Want to speak with pharmacist</p>
				 <div style="margin-left: 30px;font-size: large;margin-bottom: 0px;">
					@if($order->speakwithpharmacist == 1) 
					<b>YES</b>
					@else
					<b>NO</b>	
					@endif
				 </div>
				</div>
	</div>
		<div style="display:flex;flex-direction: row;margin:20px;">
				 <div>
				<p style="margin-left: 30px;font-size: large;margin-bottom: 0px;">Special Instructions</p>
				<div class="gy">
				<textarea  id="w3mission" rows="10" cols="55"></textarea>		
				</div>	
				</div>	
				 <div>
        <label for="" style="font-size: large;margin-left: 20px;margin-top: 10px;">Pharmacist Name</label><span id="error-name"></span> <input type="text"  id="pharmacistname" name="pharmacistname" style="margin-top: 20px;width: 250px;height: 35px;">
        
		<label for="" style="font-size: large;margin-left: 20px;margin-top: 10px;">Total Amount</label><span id="error-total"></span> <input type="text"  id="ordertotal" name="ordertotal" style="margin-top: 20px;width: 250px;height: 35px;">
    </div>
 
				</div>
   
   <div style="display: flex;flex-direction: row;width: 1100px;inline-height:10px;">
       <!--  <p style="padding-top: 18px;color: #1c9ad5;margin-left: 970px;">Total &nbsp;</p> <p style="width: 100px;height: 35px;color: #1c9ad5;padding-left: 0px;padding-top: 0px;font-weight: bolder;font-size: 25px;"><input type="text" placeholder="$400" width="50px" class="new1" style="margin-top: 0px;" id="02"></p> -->
    </div>
    <div style="width: 1150px">
        <input type="submit" style="margin:20px;width: 170px;height: 40px;border-radius: 30px;background-color: #1c9ad5;border: 0px;color: white;float:right;" value="SEND QUOTE">
    </div>
  
	@endforeach
</body>
<script>


    </script>
</html> 