@extends('header')

@section('title','| ViewOrder')

@section('content')

<div style="width: 100%;">
      
      <div class="orderbar" style="margin-bottom: 0px;padding-bottom: 0px;">
        <nav class="navbar navbar-expand-lg navbar-light" style="width: 100%;">
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item"
                style="height: 100%;font-size: 14px;padding-top: 15px;font-weight: 500;">
                <p class="col-white">Order: {{$order->uniqueorderid}}</p>
              </li>
			  @if(!empty($order->transactionId))
			  <li class="nav-item"
                style="height: 100%;font-size: 14px;padding-top: 15px;font-weight: 500;">
                <p class="col-white">TransactionId: {{$order->transactionId}}</p>
              </li>
			  @endif
              <li class="nav-item"
                style="height: 100%;font-size: 14px;padding-top: 15px;font-weight: 500;padding-left: 20px;">
                <p class="col-white">Date: {{date('d M Y', strtotime($order->orderDate))}}</p>
              </li>
              <li class="nav-item" style="font-weight: 500;font-size: 14px;padding-top: 15px;padding-left: 20px;">
                <p class="col-white">Status: {{$order->myorderstatus}}</p>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="titlebar" style="margin-bottom: 0px;padding-bottom: 0px;">
        <nav class="navbar navbar-expand-lg navbar-light" style="width: 100%;">
          <div class="logo">
            <img width="210px" height="50px" src="{{asset('pharmacy/portal').'/'.$order->logo}}" alt="Pharmacy Logo">
          </div>
          <div style="text-align: end;width: 100%;font-size: 14px;font-weight: 400;line-height: 18px;">
            <p style="margin: 0;">{{$order->locationname}}</p>
            <p style="margin: 0;">{{$order->address}}</p>
          </div>
        </nav>
      </div>
	  
	 
      <div class="scriptbar" style="margin-bottom: 0px;padding-bottom: 0px;display: flow-root;">
        <h3>Scripts</h3>
		
		@if(count($originalscriptslist)>0)
			@foreach($originalscriptslist as $oitemdetails)
				
			@if(isset($oitemdetails->prescriptiontype))	
			
				
			<div class="gallery" id="gallery{{$oitemdetails->prescriptionid}}">
			
			  @if(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "Medisecure")
				@if(strpos($oitemdetails->barcode,'http')==false)
				<a target="_blank" href="{{ env('MK_SCRIPT_URL_MEDISECURE') }}{{$oitemdetails->barcode}}">
				@else
				<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
          @if(isset($oitemdetails->prescriptionimage))
					<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@else
					<img src="{{asset('prescriptions').'/default_qrcode.png'}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@endif
		  <!--<div id='DivIdToPrint'>
			   <iframe src="https://ausscripts-int.medisecure.com.au/scripts/{{$oitemdetails->barcode}}"  width="1024" height="768">
				 </div>-->
			  </a>
        <div class="desc">
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv({{$oitemdetails->prescriptionid}});">Print</button>
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
              </div>
			  @if($order->myorderstatus=="Process" || $order->myorderstatus=="Widget Order")
            <div class="desc">
            <button class="btn btn-block mybtn" onclick="sendtoMedview( {{$order->locationid}}, '{{$oitemdetails->barcode}}', {{$order->mobilenumber}}, {{$oitemdetails->issent}}, {{$oitemdetails->prescriptionitemid}}, {{$order->uniqueorderid}} );">
                @if($oitemdetails->issent==0)
                Send to Medview Flow
                @else
                Resend to Medview Flow
                @endif
              </button>
              </div>
			  @endif
              </div>  

			@elseif(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "ERX")  
				@if(strpos($oitemdetails->barcode,'http')==false)
					<a target="_blank" href="{{ env('MK_SCRIPT_URL_ERX') }}{{$oitemdetails->barcode}}">
				@else
					<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
				<!--<div id='DivIdToPrint'>
			   <iframe src="https://egw-etp-int-qrcode-web-au-se.azurewebsites.net/{{$oitemdetails->barcode}}"  width="1024" height="768">
				 </div>-->
				 @if(isset($oitemdetails->prescriptionimage))
					<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@else
					<img src="{{asset('prescriptions').'/default_qrcode.png'}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@endif
			  </a>
        <div class="desc">
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv({{$oitemdetails->prescriptionid}});">Print</button>
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
              </div>
			  @if($order->myorderstatus=="Process" || $order->myorderstatus=="Widget Order")
            <div class="desc">
            <button class="btn btn-block mybtn" onclick="sendtoMedview( {{$order->locationid}}, '{{$oitemdetails->barcode}}', {{$order->mobilenumber}}, {{$oitemdetails->issent}}, {{$oitemdetails->prescriptionitemid}}, {{$order->uniqueorderid}} );">
                @if($oitemdetails->issent==0)
                Send to Medview Flow
                @else
                Resend to Medview Flow
                @endif
              </button>
              </div>
			  @endif
              </div>  

			@else
				<a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}">
			<div id='DivIdToPrint'>
			   
      @if(isset($oitemdetails->prescriptionimage))
					<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@else
					<img src="{{asset('prescriptions').'/default_qrcode.png'}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@endif
			  </div>
			  </a>
			 <div class="desc">
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv({{$oitemdetails->prescriptionid}});">Print</button>
            <button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
          </div>
      </div>
      
			@endif
				<!--@if( $oitemdetails->prescriptiontype == 3)
				<div class="gallery">
				
				  <a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}">
				  <div id='DivIdToPrint'>
					<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
				  </div>
				  </a>
				 
				  <div class="desc">
					<button type="submit" class="btn btn-block mybtn"
					  style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv();">Print</button>
					<button type="submit" class="btn btn-block mybtn"
					  style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
				  </div>
				</div>
				@endif -->
			@endif
			@endforeach
	   @else
		   
		   @foreach($scriptslist as $oitemdetails)	
				@if(isset($oitemdetails->prescriptiontype))	
				
				<div class="gallery" id="gallery{{$oitemdetails->prescriptionid}}">
				
				 @if(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "Medisecure")
				@if(strpos($oitemdetails->barcode,'http')==false)
					<a target="_blank" href="{{ env('MK_SCRIPT_URL_MEDISECURE') }}{{$oitemdetails->barcode}}">
				@else
					<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
			@elseif(($oitemdetails->prescriptiontype == 1 || $oitemdetails->prescriptiontype == 2) && $oitemdetails->prescriptionissuer == "ERX")  
				@if(strpos($oitemdetails->barcode,'http')==false)
					<a target="_blank" href="{{ env('MK_SCRIPT_URL_ERX') }}{{$oitemdetails->barcode}}">
				@else
					<a target="_blank" href="{{$oitemdetails->barcode}}">
				@endif
			@else
				<a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}">
			@endif
				  <div id='DivIdToPrint'>
          @if(isset($oitemdetails->prescriptionimage))
					<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@else
					<img src="{{asset('prescriptions').'/default_qrcode.png'}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					@endif
				
				  </div>
				  </a>
				
				  <div class="desc">
					<button type="submit" class="btn btn-block mybtn"
					  style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv({{$oitemdetails->prescriptionid}});">Print</button>
					<button type="submit" class="btn btn-block mybtn" style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
          </div>
          @if($order->myorderstatus=="Process" || $order->myorderstatus=="Widget Order")
					<div class="desc">
					<button class="btn btn-block mybtn" onclick="sendtoMedview( {{$order->locationid}}, '{{$oitemdetails->barcode}}', {{$order->mobilenumber}}, {{$oitemdetails->issent}}, {{$oitemdetails->prescriptionitemid}}, {{$order->uniqueorderid}} );">
					@if($oitemdetails->issent==0)
					Send to Medview Flow
					@else
					Resend to Medview Flow
					@endif
					</button>
					</div>
					@endif
					</div>
					<!--@if( $oitemdetails->prescriptiontype == 3)
					<div class="gallery">
					
					  <a target="_blank" href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}">
					  <div id='DivIdToPrint'>
						<img src="{{asset('prescriptions').'/'.$oitemdetails->prescriptionbackimage}}" alt="{{$oitemdetails->prescriptiontitle}}" width="700" height="400">
					  </div>
					  </a>
					 
					 <div class="desc">
            <button type="submit" class="btn btn-block mybtn"
              style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;" onclick="printDiv();">Print</button>
            <button type="submit" class="btn btn-block mybtn"
              style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;"><a href="{{asset('prescriptions').'/'.$oitemdetails->prescriptionimage}}" download="{{$oitemdetails->prescriptionimage}}">Download</a></button>
          </div>
					</div>
					@endif -->
				@endif
       @endforeach
	   @endif
      </div>

      <div class="patientdetails" style="margin-bottom: 0px;padding-bottom: 20px;display: flow-root;">
        <h3 style="margin-bottom: 12px;">Patient Details</h3>
        <div class="details">
          <ul>
            <li><b>First Name  Last Name:</b><br>{{$order->fname}} {{$order->lname}}</li>
			<li><b>Date of Birth:</b><br>
			@if(isset($order->dateofbirth))
			{{date('d M Y', strtotime($order->dateofbirth))}}
			@endif
			</li>
			<li><b>Gender:</b><br>{{$order->gender}}</li>           
            <li><b>Email:</b><br>{{$order->emailaddress}}</li>
			<li><b>Mobile:</b><br>{{$order-> mobilenumber}}</li>
			@if(!empty($useraddress))
			@foreach($useraddress as $addressVal)
			
            <li><b>Address :</b><br>{{$addressVal->addressline1}} , {{$addressVal->addressline2}}<br>
              {{$addressVal->suburb}}, {{$addressVal->state}}, {{$addressVal->postcode}}</li>@endforeach
			@endif 
			
			
          </ul>
        </div>
        <div class="details">
          <ul>
            <!--<li>
              <label>Generic Alternative?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rbrand" value="no" {{ ($order->isGeneric=="0" || $order->isGeneric==" ")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rbrand" value="yes" {{ ($order->isGeneric=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>-->
            <li>
              <label>Need to Speak with Pharmacist?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPharmacist" value="no"  {{ ($order->speakwithpharmacist=="0" || $order->speakwithpharmacist==" ")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPharmacist" value="yes"  {{ ($order->speakwithpharmacist=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
            <li>
              <label>Special request to Pharmacy</label><br>
              <textarea class="form-control mat-5" rows="3" readonly="true">{{$order->orderinstructions}}</textarea>
            </li>
          </ul>
        </div>		
		@if(!empty($healthprofile))
		@foreach($healthprofile as $healthinfo)
        <div class="details">
          <ul>
            <li>
              <label for="medicareno">Medicare Number</label><br>
              <input type="text"  class="form-control mat-5" id="medicareno" readonly="true" value="{{\Illuminate\Support\Str::limit($healthinfo->medicarenumber, 10, $end='')}}"> 
            </li>
            <li>
              <div class="row" style="display: contents;width: 100%;">
                <div style="width: 48%;margin-right: 10px;">
                  <label for="medicareno">Ref Num</label><br>
                  <input type="text" class="form-control mat-5" id="medicarepos" readonly="true" value="{{\Illuminate\Support\Str::substr($healthinfo->medicarenumber , 10)}}">
                </div>
                <div style="width:48%;">
                  <label for="medicareno">Medicare Card Expiry</label><br>
                  <input type="text" class="form-control mat-5" id="medicareno" readonly="true" value="{{$healthinfo->medicare_exp}}">
                </div>
              </div>
            </li>
            <li>
              <label for="medicareno">Pension / Healthcare Card Number</label><br>
              <input type="text" class="form-control mat-5" id="medicareno" readonly="true" value="{{$healthinfo->pensionerconcessioncard}} {{$healthinfo->heathcarecard}}"> 
            </li>
            <li>
              <label for="medicareno">Safety Net / Consession Safety Net</label><br>
              <input type="text" class="form-control mat-5" id="medicareno" readonly="true" value="{{$healthinfo->safetynet}}{{$healthinfo->concessionsafetynetcard}}">
            </li>
          </ul>
        </div>
		
        <div class="details">
          <ul>
            <li>
              <label>Allergies</label><br>
              <textarea class="form-control" rows="3" readonly="true">{{str_replace('"','',str_replace(']','',str_replace('[','',$healthinfo->allergies)))}}</textarea>
            </li>
            <li>
              <label>Medical Conditions</label><br>
              <textarea class="form-control" rows="3" readonly="true">{{str_replace('"','',str_replace(']','',str_replace('[','',$healthinfo->medicalconditions)))}}</textarea>
            </li>
          </ul>
        </div>
        <div class="details">
          <ul>
            <li>
              <label>Smoker</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rnonSmoker" value="no" {{ ($healthinfo->smoking=="0")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rnonSmoker" value="yes" {{ ($healthinfo->smoking=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
			@if($order->gender == "Female")
            <li>
              <label>Pregnant</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPregnant" value="no" {{ ($healthinfo->pregnency=="0")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPregnant" value="yes" {{ ($healthinfo->pregnency=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
			@if($healthinfo->pregnency == "1")
            <li>
              <label>Breast feeding</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rBreasfeeding" value="no" {{ ($healthinfo->lactation=="0")? "checked" : "disabled" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rBreasfeeding" value="yes" {{ ($healthinfo->lactation=="1")? "checked" : "disabled" }}>Yes
              </label>
            </li>
			@endif
			@endif
          </ul>
        </div>
      </div>
@endforeach
@endif
@if(count($originalscriptslist)>0)
  <div class="revieworder">
        <h3 style="margin-bottom: 25px;">Original Order Details</h3>
		<!--- Original Order --->
        <div class="table-responsive">
         <!--- <h6>Original Order Items</h6> --->
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
                <!--<th scope="col">UOM</th>-->
                <th scope="col">Unit $</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
              </tr>
            </thead>
            <tbody>
			
			 @foreach($originalscriptslist as $k => $originalitemdetails)
			
              <tr>
                <td scope="row">
				@if(!empty($originalitemdetails->itemimage))
				<img src="{{$originalitemdetails->itemimage}}" width="50px" height="50px">
				@endif
				</td>
                <td>{{$originalitemdetails->prescriptionid}}</td>
                <td><p>{{$originalitemdetails->drugname}}<br>@if($originalitemdetails->isgeneric==1)<span style="color:red;padding-left:5%">Generic Alternative Chosen<span>@endif</p></td>
                <!--<td>{{$originalitemdetails->drugquantity}}</td>-->
                @if($originalitemdetails->type==2)
                <td style="text-align:right;"></td>
				@else
				<td>{{$originalitemdetails->originalprice}}</td>
				@endif
                <td>{{$originalitemdetails->drugpack}}</td>   
				@if($originalitemdetails->type==2)
                <td style="text-align:right;"></td>
				@else
				<td style="text-align:right;">{{$originalitemdetails->originalprice * $originalitemdetails->drugpack}}</td>
				@endif
			  </tr>
			 
        @endforeach      
            </tbody>
          </table>
        </div>

		<br>
		<!--- Original Order End --->
       
        </div> 
@endif		
           <div class="revieworder">
        <h3 style="margin-bottom: 25px;">Final Order Details</h3>
		
        <div class="table-responsive">
      <!---    <h6>Current Order Items</h6>--->
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
                <!--<th scope="col">UOM</th>-->
                <th scope="col">Unit $ (inc GST)</th>
                <th scope="col">Qty</th>
                <th scope="col">Price (inc GST)</th>
              </tr>
            </thead>
            <tbody>
			
			 @foreach($scriptslist as $k => $itemdetails)
			 
              <tr>
			  
                <td scope="row">
				@if(!empty($itemdetails->itemimage))
				<img src="{{$itemdetails->itemimage}}" width="50px" height="50px">
				@endif
				</td>
                <td>{{$itemdetails->prescriptionid}}</td>
				@if($itemdetails->prescriptiontype == "3")
					<td>Paper Prescription</td>
				@else
					<td><p>{{$itemdetails->drugname}}<br>@if($itemdetails->isgeneric==1)<span style="color:red;padding-left:5%">Generic Alternative Chosen<span>@endif</p></td>
				@endif
                <!--<td>{{$itemdetails->drugquantity}}</td>-->
                @if($itemdetails->type==2)
                <td style="text-align:right;"></td>
				@else
				<td>{{$itemdetails->originalprice}}</td>
				@endif
                <td>{{$itemdetails->drugpack}}</td> 
				@if($itemdetails->type==2)
                <td style="text-align:right;"></td>
				@else
                <td style="text-align:right;">{{$itemdetails->originalprice * $itemdetails->drugpack}}</td>
				@endif
			  </tr>
			 
        @endforeach      
            </tbody>
          </table>
        </div>


     
<div class="form-control">
		<table class="table">
		<tr>
		<td colspan="6">Delivery Fee</td>
		<td style="float:right;">{{$order->deliveryfee}}</td>
		</tr>
		<tr>
		<td colspan="6">Order Total</td>
		@if($order->type==2)
		<td id="orderprice"style="float:right;">In Store Payment</td>
		@else
		<td id="orderprice"style="float:right;">{{$order->orderTotal}}</td>
		@endif
		</tr>
		</table>
		</div>		
		<div style="padding-bottom: 20px;">		
		</div>		
			<div class="patientdetails" style="margin-bottom: 0px;padding-bottom: 20px;padding-top: 20px;display: flow-root;">
        <h3 style="margin-bottom: 12px;">{{$order->ordertype}} Details</h3>
        <div class="">
          <ul>
		  
		  @if($order->ordertype == "Delivery")
            <li id="delivery">
		<b>{{$order->ordertype}} Address:</b><br>
		{{$order->addr1}}{{$order->addr2}}<br>
		{{$order->addsuburb}} , {{$order->addstate}} , {{$order->addpostcode}}
		
		</li>
		<li style="width: 40%;display: flex;" id="editaddress_btn">
		<a type="button" class="btn btn-block mybtn" 
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> Edit Address
          </a>
		</li>
		<li>
		<form id="deliveryaddressform" action="{{ route('updatedeliveryaddress') }}" method="post">
		@csrf
		@if (session()->has('error'))
			<div id="div_showErrorMsg" class="alert-caret">
				<div id="divmsg"><span class="notification-info n-error" style="">
				{{session()->get('error')}}</span></div>
			</div>
			@endif 
		<table class="table" id="newaddress" style="display:none;">
              <tbody><tr><td class="asia">
			<input type="text" class="form-control asia" name="addr1" id="addr1" placeholder = "Address1" value="{{$order->addr1}}">
			</td><td class="asia">
			<input type="text" class="form-control asia" name="addr2" id="addr2" placeholder = "Address2" value="{{$order->addr2}}">
			</td><td class="asia">
			<input type="text" class="form-control" name="addsuburb" id="addsuburb" placeholder = "Suburb" value="{{$order->addsuburb}}">
			</td><td class="asia">
			<input type="text" class="form-control" name="addstate" id="addstate" placeholder = "state" value="{{$order->addstate}}">
			</td><td class="asia">
			<input type="text" class="form-control" name="addpostcode" id="addpostcode" placeholder = "Postcode" value="{{$order->addpostcode}}">
			</td><td class="asia">
			<input type="text" class="form-control" name="mobile" id="mobile" placeholder = "Mobile" value="{{$order->deliverymobile}}">
			</td>
			<td>
			<input type="hidden" class="form-control" name="orderid" id="orderid"  value="{{$order->orderid}}">
			<input type="hidden" class="form-control" name="myuserid" id="myuserid"  value="{{$order->myuserid}}">
			</td>
			<tr><td >
			<button type="submit" class="btn btn-block mybtn"
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> Save Address
          </button></td></tr>
		  </table>
		</form>
		</li>
		  @endif
		  
            <li><b>Phone:</b><br>{{$order->mobilenumber}}</li>
			
            <li id="slotdetails"><b>Slot Details:</b><br>{{$order->userpreffereddate}} {{$order->userprefferedtime}}</li>
            <li style="width: 40%;display: flex;" id="editslot_btn">
			<a type="button" class="btn btn-block mybtn" 
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> Edit Slot
          </a>
		</li>
		 </ul>
		 <form id="deliveryslotform" action="{{ route('updatedeliveryslot') }}" method="post">
		@csrf
		@if (session()->has('error'))
			<div id="div_showErrorMsg" class="alert-caret">
				<div id="divmsg"><span class="notification-info n-error" style="">
				{{session()->get('error')}}</span></div>
			</div>
			@endif 
		  <ul id="newslotdetails" style="display:none;">
		
		@if($order->ordertype == "Delivery")
		  <li>Delivery Date <input type="text" id="datepicker" name="datepicker" onchange="myselectedday()" value="{{$order->userpreffereddate}}"></li>
		  <li>Delivery Slot <select id="select_slot" name="select_slot"><option value="{{$order->userprefferedtime}}">{{$order->userprefferedtime}}</option></select></li>
		  <p> <input type="hidden" id="locationid" value="{{$order->locationid}}" name="locationid"></p>
		  <p> <input type="hidden" id="orderid" value="{{$order->orderid}}" name="orderid"></p>
		  <li style="width: 40%;display: flex;" id="saveslot_btn">
			<button type="submit" class="btn btn-block mybtn"
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> Save Slot
          </button>
		</li>
		@endif
		
          </ul>
		</form>
        </div>
        
    
      </div>
		<div class="revieworder">
		@if($order->myorderstatus == "Process")
        
		<div style="width:30%"><label for="medicareno">New Status</label><br>
              <span  class="form-control mat-5" id="medicareno">{{ $order->ordertype}}</span></div><br/>
				
        <div style="width: 40%;display: flex;">
          <a type="button" class="btn btn-block mybtn" onclick="return myFunction();" href="{{ route('cancelOrder',['orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> CANCEL ORDER
          </a>
		  <!-- href="{{ route('changestatus',['orderstatus'=>$order->ordertype,'orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}" -->
		  @if($order->deliverypartner == "DoorDash")
          <a type="button" class="btn btn-block mybtn" onclick="return doordashconfirmation({{ $order->orderid }});" style="width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;">PROCEED WITH
            ORDER</a>
		  @else
			   <a type="button" class="btn btn-block mybtn" href="{{ route('changestatus',['orderstatus'=>$order->ordertype,'orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;">PROCEED WITH
            ORDER</a>
		  @endif
        </div>
		<div></div>
		</div>
		@elseif($order->myorderstatus == "Widget Order")
        
		<div style="width:30%"><label for="medicareno">New Status</label><br>
              <span  class="form-control mat-5" id="medicareno">{{ $order->ordertype}}</span></div><br/>
				
        <div style="width: 40%;display: flex;">
          <a type="button" class="btn btn-block mybtn" onclick="return myFunction();" href="{{ route('cancelOrder',['orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> CANCEL ORDER
          </a>
		  <!-- href="{{ route('changestatus',['orderstatus'=>$order->ordertype,'orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}" -->
		  @if($order->deliverypartner == "DoorDash")
          <a type="button" class="btn btn-block mybtn" onclick="return doordashconfirmation({{ $order->orderid }});" style="width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;">PROCEED WITH
            ORDER</a>
		  @else
			   <a type="button" class="btn btn-block mybtn" href="{{ route('changestatus',['orderstatus'=>$order->ordertype,'orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;">PROCEED WITH
            ORDER</a>
		  @endif

        @elseif($order->myorderstatus == "Awaiting Authorisation") 
		<div style="width:30%">
		
		<!-- <p style="margin-top: 10px;margin-bottom: 15px;font-size: 15px;font-weight: 400;">Edit order to add suggestions
          or change to generic items, Otherwise proceed with order</p> -->
		
        <div style="width: 40%;display: flex;">
          <a type="submit" class="btn btn-block mybtn" onclick="return myFunction();" href="{{ route('cancelOrder',['orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}" 
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> CANCEL ORDER
          </a>
          
			
        </div>
		<div></div>
		</div>
		@elseif($order->myorderstatus == "Awaiting Pickup")
        
		<div style="width:30%"><label for="medicareno">New Status</label><br>
              <span  class="form-control mat-5" id="medicareno">Completed</span><br/>
		
		<!-- <p style="margin-top: 10px;margin-bottom: 15px;font-size: 15px;font-weight: 400;">Edit order to add suggestions
          or change to generic items, Otherwise proceed with order</p> -->
		
        <div style="width: 40%;display: flex;">
          <a type="submit" class="btn btn-block mybtn" onclick="return myFunction();" href="{{ route('cancelOrder',['orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}" 
            style="width:auto;font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> CANCEL ORDER
          </a>
          <a type="button" class="btn btn-block mybtn" href="{{ route('changestatus',['orderstatus'=>'Completed','orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;">PROCEED WITH
            ORDER</a>
			
        </div>
		<div></div>
		</div>
		@elseif($order->myorderstatus == "Awaiting Delivery")
		<div style="width:30%"><label for="medicareno">New Status</label><br>
              <span  class="form-control mat-5" id="medicareno">In Transit</span><br/>
			  
		<div class="form-group">
		  <label for="exampleFormControlTextarea2">Driver Instructions</label>
		  <textarea class="form-control rounded-0" id="instructions" name="instructions" rows="4" onchange="addinstructions({{$order->orderid}} , {{$order->myuserid}});">{{$order->driverinstructions}}</textarea>
		</div>
		<div></div><br/>
        <div style="width: 40%;display: flex;">
          <a type="submit" class="btn btn-block mybtn" onclick="return myFunction();" href="{{ route('cancelOrder',['orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto; font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> CANCEL ORDER
          </a>
          <a type="button" class="btn btn-block mybtn" href="{{ route('changestatus',['orderstatus'=>'In Transit','orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style=" width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;" >COLLECTED</a>
			
        </div>
		<div></div>		</div>
		@elseif($order->myorderstatus == "In Transit")
		<div style="width:30%"><label for="medicareno">New Status</label><br>
              <span  class="form-control mat-5" id="medicareno">Completed</span><br/>
		
		<!-- <p style="margin-top: 10px;margin-bottom: 15px;font-size: 15px;font-weight: 400;">Edit order to add suggestions
          or change to generic items, Otherwise proceed with order</p> -->
		
        <div style="width: 40%;display: flex;">
         
          <a type="button" class="btn btn-block mybtn" href="{{ route('changestatus',['orderstatus'=>'Completed','orderid'=>$order->orderid,'userid'=>$order->myuserid]) }}"
            style="width:auto;padding: 5px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;">PROCEED WITH
            ORDER</a>
			
        </div>
		<div></div>
		</div>
		@endif
	
        
      </div>
    </div>
  </div>
  <script src="" async defer></script>
  <script>
  function myFunction() {
      if(!confirm("Are You Sure you want cancel this order"))
      event.preventDefault();
  }
  function doordashconfirmation(orderid) {
      if(!confirm("Click Continue to book an express delivery via DoorDash. Ensure that your order is ready for ​collection as a Driver may arrive anytime."))
		event.preventDefault();
	else  
		$.ajax({
				
                type: 'post',              
                url: "{{ route('calldoordashdelivery') }}",
				data: {
						"_token": "{{ csrf_token() }}",						
						"orderid": orderid,
						},    
               
                success: function(result){
					alert(result);
					window.location.href = "{{ route('deliverylist')}}";
               },
                error: function(result){
                    alert(result);
					window.location.href = "{{ route('readytoprocess')}}";
               }
           });
  }
	function printDiv(id) 
	{
		var divToPrint=document.getElementById('DivIdToPrint');
		var url=$('#gallery'+id+' a').attr("href");
		var newWin=window.open('','Print-Window');

	  newWin.document.open();

	  newWin.document.write('<html><body onload="window.print()"><iframe style="position:fixed; top:0px; left:0px; bottom:0px; right:0px; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;" src="'+url+'"></body></html>');

	  newWin.document.close();

	  setTimeout(function(){newWin.close();},1000);

	}

$("#editaddress_btn").click(function(){
  $("#delivery").hide();
  $("#editaddress_btn").hide();
  $("#newaddress").show();
});
$("#editslot_btn").click(function(){
  $("#slotdetails").hide();
  $("#editslot_btn").hide();
  $("#newslotdetails").show();
  $("#saveslot_btn").show();
});
 </script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="/resources/demos/style.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
  $( function() {
    $( "#datepicker" ).datepicker({
		dateFormat: "yy-mm-dd"
	});
	
  } );
  
  function myselectedday()
  {
	  
	  selecteddate = $( "#datepicker" ).val();
	 
	  locationid = $( "#locationid" ).val();
	  $.ajax({
				
                type: 'post',              
                url: "{{ route('deliveryslot') }}",
				data: {
						"_token": "{{ csrf_token() }}",
						"selecteddate": selecteddate,
						"locationid": locationid
						
						},    
               
                success: function(result){
					var options = result;
					console.log(result);
					$('#select_slot').empty();
					 $.each(options, function(i, result) {
						 $('#select_slot').append($('<option>'+result.deliverywindow+'</option>').val(result.deliverywindow));
					 });
					
               },
                error: function(result){
                    return result;
               }
           }); 
	
		
  }
  function sendtoMedview( locationid, barcode, mobilenumber, issent, prescriptionitemid,order_id ) { 
  //alert(order_id);
    if(issent==0){
      $.ajax({
        type: 'post',              
        url: "{{ route('sendtomedview') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "locationid": locationid,
          "barcode": barcode,
          "mobilenumber": mobilenumber,
          "issent": issent,
		  "prescriptionitemid": prescriptionitemid,
		  "order_id":order_id
        },    
        success: function(result){
			alert("Send to Mediview Flow Successfully");
            console.log(result);
        },
        error: function(result){
			alert(result);
          console.log(result);
        }
      });
  }else{
      if (confirm('Are you sure you want Resend to Medview Flow?')) {
		  $.ajax({
        type: 'post',              
        url: "{{ route('sendtomedview') }}",
        data: {
          "_token": "{{ csrf_token() }}",
          "locationid": locationid,
          "barcode": barcode,
          "mobilenumber": mobilenumber,
          "issent": issent,
		  "prescriptionitemid": prescriptionitemid,
		  "order_id": order_id
        },    
        success: function(result){
			alert("Send to Mediview Flow Successfully");
            console.log(result);
        },
        error: function(result){
			alert(result);
          console.log(result);
        }
      });
	  }
  }
  }

  function addinstructions(orderid,userid)
  {
	  instruction = $( "#instructions" ).val();
	   $.ajax({
				
                type: 'post',              
                url: "{{ route('updatedriverinstruction') }}",
				data: {
						"_token": "{{ csrf_token() }}",
						"orderid": orderid,
						"userid": userid,
						"instruction":instruction
						
						},    
               
                success: function(result){
					val = $( "#instructions" ).val();
               },
                error: function(result){
                    return result;
               }
           });
  }


  </script>
 @endsection