@extends('header')

@section('title','| ViewOrder')

@section('content')

@foreach($orderData as $order)

<div style="width: 100%;">
      
      <div class="orderbar" style="margin-bottom: 0px;padding-bottom: 0px;">
        <nav class="navbar navbar-expand-lg navbar-light" style="width: 100%;">
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item"
                style="height: 100%;font-size: 14px;padding-top: 15px;font-weight: 500;">
                <p class="col-white">Order: {{$order->uniqueorderid}}</p>
              </li>
              <li class="nav-item"
                style="height: 100%;font-size: 14px;padding-top: 15px;font-weight: 500;padding-left: 20px;">
                <p class="col-white">Date: {{date('d M Y', strtotime($order->orderDate))}}</p>
              </li>
              <li class="nav-item" style="font-weight: 500;font-size: 14px;padding-top: 15px;padding-left: 20px;">
                <p class="col-white">Status: {{$order->myorderstatus}}</p>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <div class="titlebar" style="margin-bottom: 0px;padding-bottom: 0px;">
        <nav class="navbar navbar-expand-lg navbar-light" style="width: 100%;">
          <div class="logo">
            <img width="210px" height="50px" src="{{asset('pharmacy').'/'.$order->logo}}" alt="Pharmacy Logo">
          </div>
          <div style="text-align: end;width: 100%;font-size: 14px;font-weight: 400;line-height: 18px;">
            <p style="margin: 0;">{{$order->locationname}}</p>
            <p style="margin: 0;">{{$order->address}}</p>
          </div>
        </nav>
      </div>
	  
	 
      <div class="scriptbar" style="margin-bottom: 0px;padding-bottom: 0px;display: flow-root;">
        <h3>Scripts</h3>
		
		@foreach($scriptslist as $itemdetails)			
        <div class="gallery">
          <a target="_blank" href="{{asset('prescriptions').'/'.$itemdetails->prescriptionimage}}">
		  
            <img src="{{asset('prescriptions').'/'.$itemdetails->prescriptionimage}}" alt="{{$itemdetails->prescriptiontitle}}" width="700" height="400">
          </a>
          <div class="desc">
            <button type="submit" class="btn btn-block mybtn"
              style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;margin-right: 5px;">Print</button>
            <button type="submit" class="btn btn-block mybtn"
              style="padding: 0px;font-size: 14px;background-color: white;color: #1c9ad6;margin: 0px;">Download</button>
          </div>
        </div>
       @endforeach
      </div>

      <div class="patientdetails" style="margin-bottom: 0px;padding-bottom: 20px;display: flow-root;">
        <h3 style="margin-bottom: 12px;">Patient Details</h3>
        <div class="details">
          <ul>
            <li><b>Customer:</b><br>{{$order->fname}} {{$order->lname}}</li>
            <li><b>Phone:</b><br>{{$order-> mobilenumber}}</li>
            <li><b>Email:</b><br>{{$order->username}}</li>
			@if(!empty($useraddress))
			@foreach($useraddress as $addressVal)
			
            <li><b>Address:</b><br>{{$addressVal->addressline1}} , {{$addressVal->addressline2}}<br>
              {{$addressVal->suburb}}, {{$addressVal->state}}, {{$addressVal->postcode}}</li>@endforeach
			@endif 
			<li><b>Dob:</b><br>{{$order->dateofbirth}}</li>
			<li><b>Gender:</b><br>{{$order->gender}}</li>
          </ul>
        </div>
        <div class="details">
          <ul>
            <li>
              <label>Generic Brand?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rbrand" value="no" {{ ($order->isGeneric=="0" || $order->isGeneric==" ")? "checked" : "" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rbrand" value="yes" {{ ($order->isGeneric=="1")? "checked" : "" }}>Yes
              </label>
            </li>
            <li>
              <label>Need to Speak with Pharmacist?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPharmacist" value="no"  {{ ($order->speakwithpharmacist=="0" || $order->speakwithpharmacist==" ")? "checked" : "" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPharmacist" value="yes"  {{ ($order->speakwithpharmacist=="1")? "checked" : "" }}>Yes
              </label>
            </li>
            <li>
              <label>Special request to Pharmacy</label><br>
              <textarea class="form-control mat-5" rows="3">{{$order->orderinstructions}}</textarea>
            </li>
          </ul>
        </div>		
		@if(!empty($healthprofile))
		@foreach($healthprofile as $healthinfo)
        <div class="details">
          <ul>
            <li>
              <label for="medicareno">Medicare No.</label><br>
              <span  class="form-control mat-5" id="medicareno"> {{\Illuminate\Support\Str::limit($healthinfo->medicarenumber, 10, $end='')}}</span>
            </li>
            <li>
              <div class="row" style="display: contents;width: 100%;">
                <div style="width: 48%;margin-right: 10px;">
                  <label for="medicareno">Pos.</label><br>
                  <span class="form-control mat-5" id="medicarepos">{{\Illuminate\Support\Str::substr($healthinfo->medicarenumber , 10)}}</span>
                </div>
                <div style="width:48%;">
                  <label for="medicareno">Expiry</label><br>
                  <span type="text" class="form-control mat-5" id="medicareno">{{$healthinfo->medicare_exp}} </span>
                </div>
              </div>
            </li>
            <li>
              <label for="medicareno">Heathcare/Concession</label><br>
              <span type="text" class="form-control mat-5" id="medicareno"> {{$healthinfo->heathcarecard}}{{$healthinfo->pensionerconcessioncard}}</span>
            </li>
            <li>
              <label for="medicareno">Safetynet/Consessoin</label><br>
              <span type="text" class="form-control mat-5" id="medicareno">{{$healthinfo->safetynet}}{{$healthinfo->concessionsafetynetcard}}</span>
            </li>
          </ul>
        </div>
		
        <div class="details">
          <ul>
            <li>
              <label>Allergies</label><br>
              <textarea class="form-control" rows="3"></textarea>
            </li>
            <li>
              <label>Medical Conditions</label><br>
              <textarea class="form-control" rows="3"></textarea>
            </li>
          </ul>
        </div>
        <div class="details">
          <ul>
            <li>
              <label>Non-Smoker?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rnonSmoker" value="no" {{ ($healthinfo->smoking=="1")? "checked" : "" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rnonSmoker" value="yes" {{ ($healthinfo->smoking=="0")? "checked" : "" }}>Yes
              </label>
            </li>
			@if($order->gender == "Female")
            <li>
              <label>Pregnant?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPregnant" value="no" {{ ($healthinfo->pregnency=="0")? "checked" : "" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rPregnant" value="yes" {{ ($healthinfo->pregnency=="1")? "checked" : "" }}>Yes
              </label>
            </li>
			@if($healthinfo->pregnency == "1")
            <li>
              <label>Breasfeeding?</label><br>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rBreasfeeding" value="no" {{ ($healthinfo->lactation=="0")? "checked" : "" }}>No
              </label>
              <label class="radio-inline rWidth">
                <input class="mr" type="radio" name="rBreasfeeding" value="yes" {{ ($healthinfo->lactation=="1")? "checked" : "" }}>Yes
              </label>
            </li>
			@endif
			@endif
          </ul>
        </div>
      </div>
@endforeach
@endif
        <div class="revieworder">
        <h3 style="margin-bottom: 25px;">Edit Order</h3>
		<!--- Original Order --->
        <div class="table-responsive">
          <h6>Script Items  Original</h6>
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
                <th scope="col">UOM</th>
                <th scope="col">Unit $</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
              </tr>
            </thead>
            <tbody>
			
			 @foreach($originalscriptslist as $k => $originalitemdetails)
			
              <tr>
                <td scope="row">{{$k+1}}</td>
                <td>{{$originalitemdetails->prescriptionid}}</td>
                <td>{{$originalitemdetails->drugname}}</td>
                <td>{{$originalitemdetails->drugpack}}</td>
                <td>{{$originalitemdetails->originalprice}}</td>
                <td>{{$originalitemdetails->drugquantity}}</td>               
                <td>{{$originalitemdetails->originalprice * $originalitemdetails->drugquantity}}</td>
              </tr>
			 
        @endforeach      
            </tbody>
          </table>
        </div>


        <div class="table-responsive">
          <h6>Store Items Original</h6>
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
                <th scope="col">UOM</th>
                <th scope="col">Unit $</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
                
              </tr>
            </thead>
            <tbody>
			 @foreach($originalstorelist as $s => $originalstoreitems)
			
              <tr>
                <td scope="row">{{$s+1}}</td>
                <td>{{$originalstoreitems->prescriptionid}}</td>
                <td>{{$originalstoreitems->drugname}}</td>
                <td>{{$originalstoreitems->drugpack}}</td>
                <td>{{$originalstoreitems->originalprice}}</td>
                <td>{{$originalstoreitems->drugquantity}}</td>               
                <td>{{$originalstoreitems->originalprice * $originalstoreitems->drugquantity}}</td>
                
              </tr>
           @endforeach      
            </tbody>
          </table>
        </div> 
		<br>
		<!--- Original Order End --->
       
        </div> 
		
		<!----------------------------Current Order Screen ------------------------>
		
		
		<div class="revieworder">
        <h3 style="margin-bottom: 25px;">Edit Order</h3>
		<!--- Original Order --->
        <div class="table-responsive">
          <h6>Script Items  Current</h6>
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
                <th scope="col">UOM</th>
                <th scope="col">Unit $</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
              </tr>
            </thead>
            <tbody>
			
			 @foreach($scriptslist as $k => $itemdetails)
			
              <tr>
                <td scope="row">{{$k+1}}</td>
                <td>{{$itemdetails->prescriptionid}}</td>
                <td>{{$itemdetails->drugname}}</td>
                <td>{{$itemdetails->drugpack}}</td>
                <td>{{$itemdetails->originalprice}}</td>
                <td>{{$itemdetails->drugquantity}}</td>               
                <td>{{$itemdetails->originalprice * $itemdetails->drugquantity}}</td>
              </tr>
			 
        @endforeach      
            </tbody>
          </table>
        </div>


        <div class="table-responsive">
          <h6>Store Items Current</h6>
          <table class="table">
            <thead class="one">
              <tr>
                <th scope="col"></th>
                <th scope="col">Item ID</th>
                <th scope="col">Item Description</th>
                <th scope="col">UOM</th>
                <th scope="col">Unit $</th>
                <th scope="col">Qty</th>
                <th scope="col">Price</th>
                
              </tr>
            </thead>
            <tbody>
			 @foreach($storelist as $s => $storeitems)
			
              <tr>
                <td scope="row">{{$s+1}}</td>
                <td>{{$storeitems->prescriptionid}}</td>
                <td>{{$storeitems->drugname}}</td>
                <td>{{$storeitems->drugpack}}</td>
                <td>{{$storeitems->originalprice}}</td>
                <td>{{$storeitems->drugquantity}}</td>               
                <td>{{$storeitems->originalprice * $storeitems->drugquantity}}</td>
                
              </tr>
           @endforeach      
            </tbody>
          </table>
        </div> 
		<br>
		 <div style="width: 40%;display: flex;" id='btn_editorder' style="display:block">
          <button type="button" class="btn btn-block mybtn"id='btnedit' 
            style="font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;">EDIT ORDER
          </button>
         
        </div>
        </div>
		
		<!----------------------------Current Order End Screen ------------------------>
		
		
		<!----------------------------Edit Order Screen ------------------------>
		
		<div class="revieworder" id="editorder" style="display:none">
        <h3 style="margin-bottom: 25px;">Edit Order</h3>
        <div class="table-responsive">
          <h6>Script Items</h6>
          <table class="fulltable fulltable-editable" id="test-table">
            <thead>
              <tr class="heading">
                <th class="heading" fulltable-field-name="id"></th>
                <th class="heading" fulltable-field-name="itemid">Item ID</th>
                <th class="heading" fulltable-field-name="itemdesc">Item Description</th>
                <th class="heading" fulltable-field-name="uom">UOM</th>
                <th class="heading" fulltable-field-name="unit">Unit $</th>
                <th class="heading" fulltable-field-name="qty">Qty</th>
                <th class="heading" fulltable-field-name="price">Price</th>
              </tr>
            </thead>
            <tbody>
             @foreach($scriptslist as $k => $itemdetails)
			
              <tr>
                <td scope="row">{{$k+1}}</td>
                <td>{{$itemdetails->prescriptionid}}</td>
                <td>{{$itemdetails->drugname}}</td>
                <td>{{$itemdetails->drugpack}}</td>
                <td>{{$itemdetails->originalprice}}</td>
                <td>{{$itemdetails->drugquantity}}</td>               
                <td>{{$itemdetails->originalprice}}</td>
              </tr>
			 
        @endforeach      
              
            </tbody>
          </table>
        </div>

<br>
        <div class="table-responsive">
          <h6>Store Items</h6>
          <table class="fulltable fulltable-editable" id="test-table1">
            <thead class="one">
              <tr>
                <th class="heading" fulltable-field-name="id"></th>
                <th class="heading" fulltable-field-name="itemid">Item ID</th>
                <th class="heading" fulltable-field-name="itemdesc">Item Description</th>
				<th class="heading" fulltable-field-name="uom">UOM</th>                
                <th class="heading" fulltable-field-name="unit">Unit $</th>
                <th class="heading" fulltable-field-name="qty">Qty</th>
                <th class="heading" fulltable-field-name="price">Price</th>
                
              </tr>
            </thead>
            <tbody>
             @foreach($storelist as $s => $storeitems)
			
              <tr>
                <td scope="row">{{$s+1}}</td>
                <td>{{$storeitems->prescriptionid}}</td>
                <td>{{$storeitems->drugname}}</td>
                <td>{{$storeitems->drugpack}}</td>
                <td>{{$storeitems->originalprice}}</td>
                <td>{{$storeitems->drugquantity}}</td>               
                <td>{{$storeitems->originalprice}}</td> 
                
              </tr>
           @endforeach    
             
            </tbody>
          </table>
        </div>
        <br>

        <div style="width: 40%;display: flex;">
          <button type="submit" class="btn btn-block mybtn" onclick="getdataintables({{$order->orderid}},{{$order->myuserid}})"
            style="font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;">CONFIRM ORDER
          </button>
          <button type="submit" class="btn btn-block mybtn"
            style="padding: 0px;font-size: 14px;background-color: #1c9ad6;color: white;margin: 0px;">RETURN TO REVIEW ORDER</button>
        </div>

      </div>
		<!-------------------------------------------------------------------------------->
		
			<div class="patientdetails" style="margin-bottom: 0px;padding-bottom: 20px;display: flow-root;">
        <h3 style="margin-bottom: 12px;">{{$order->ordertype}} Details</h3>
        <div class="details">
          <ul>
		  @if($order->ordertype == "Delivery")
            <li><b>{{$order->ordertype}} Address:</b><br>{{$order->addr1}}{{$order->addr2}}<br>{{$order->addsuburb}} , {{$order->addstate}} , {{$order->addpostcode}}</li>
		  @endif
            <li><b>Phone:</b><br>{{$order->mobilenumber}}</li>
            <li><b>Slot Details:</b><br>{{$order->userpreffereddate}} {{$order->userprefferedtime}}</li>
            
          </ul>
        </div>
        
    
      </div>
		<br><br>
			<div style="width: 40%;display: flex;">
          <button type="submit" class="btn btn-block mybtn"
            style="font-size: 14px;background-color: #40AE49;color: white;margin: 0px;margin-right: 5px;"> Cancel
          </button>
         <div></div>			
        </div>
       <br>
       <br>
      </div>
    </div>
  </div>
  <script src="" async defer></script>
  <script type="text/javascript">
 
  $("#btnedit").click(function(){
  $("#btn_editorder").hide();
  $("#editorder").show();
});
  </script>
  <script type="text/javascript">
    $(
      function () {
        $("#test-table").FullTable({          
          "alwaysCreating": true,
          "selectable": false,
          "filterable": false,
          "orderable": false,
          "on": {
            "delete": function(){
              console.log($("#test-table").FullTable("getData"));
            }
          },
          "fields": {
            "itemid": {
              "mandatory": true,
			  "editable": true,
              "errors": {
                "mandatory": "First name is mandatory"
              }
            },
            "itemdesc": {
              "mandatory": true,
			  "editable": true,
              "errors": {
                "mandatory": "Last name is mandatory"
              }
            },
            "qty": {
              "type": "integer",
              "mandatory": true,
			  "editable": true,
              "validator": function (qty) {
                if (qty >= 0) {
                  return true;
                } else {
                  return false;
                }
              },
              "errors": {
                "type": "Qty must be an integer number",
                "mandatory": "Qty is mandatory",
                "validator": "Qty cannot be negative"
              }
            },
            "unit": {
              "type": "decimal",
              "mandatory": true,
			  "editable": true,
              "errors": {
                "type": "unit must be a number",
                "mandatory": "unit is mandatory"
              }
            },
            "id": {
              "mandatory": false
            },
            "uom": {
              "mandatory": true,
			  "editable": true,
            },
            
            "price": {
              "type": "decimal",
			  "editable": false,
              "mandatory": false,
              "errors": {
                "type": "Price must be a number",
                "mandatory": "Price is mandatory"
              }
            },
          }
        });
        $("#test-table-add-row").click(function () {
          $("#test-table").FullTable("addRow");
        });
        $("#test-table-get-value").click(function () {
          console.log($("#test-table").FullTable("getData"));
        });
        $("#test-table").FullTable("on", "error", function (errors) {
          for (var error in errors) {
            error = errors[error];
            console.log(error);
          }
        });
        $("#test-table").FullTable("draw");
      

 
      $("#test-table1").FullTable({
		 
          "alwaysCreating": true,
          "selectable": false,
          "filterable": false,
          "orderable": false,
          "on": {
            "delete": function(){
              
            }
          },
          "fields": {
            "itemid": {
              "mandatory": true,
              "errors": {
                "mandatory": "First name is mandatory"
              }
            },
            "itemdesc": {
              "mandatory": true,
              "errors": {
                "mandatory": "Last name is mandatory"
              }
            },
            "qty": {
              "type": "integer",
              "mandatory": true,
              "validator": function (qty) {
                if (qty >= 0) {
                  return true;
                } else {
                  return false;
                }
              },
              "errors": {
                "type": "Qty must be an integer number",
                "mandatory": "Qty is mandatory",
                "validator": "Qty cannot be negative"
              }
            },
            "unit": {
              "type": "decimal",
              "mandatory": true,
              "errors": {
                "type": "unit must be a number",
                "mandatory": "unit is mandatory"
              }
            },
            "id": {
              "mandatory": false
            },
            "uom": {
              "mandatory": true
            },
			
            "price": {
              "type": "decimal",
			  "editable": false,
              "mandatory": true,
              "errors": {
                "type": "Price must be a number",
                "mandatory": "Price is mandatory"
              }
            },
			 
          }
        });
        $("#test-table1-add-row").click(function () {
          $("#test-table1").FullTable("addRow");
        });
        $("#test-table1-get-value").click(function () {
          console.log($("#test-table1").FullTable("getData"));
        });
        $("#test-table1").FullTable("on", "error", function (errors) {
          for (var error in errors) {
            error = errors[error];
            console.log(error);
          }
        });
        $("#test-table1").FullTable("draw");
      }
    );
	function getdataintables(orderid , userinfo)
	{
		
		v1 = $("#test-table").FullTable("getData");
		v2 = $("#test-table1").FullTable("getData");
		// alert($('#refferencetable1').val());
		// v2['refferencetable'] = $('#refferencetable1').val();
		
		var mydata = v1.concat(v2);
		console.log(mydata);
		
            $.ajax({
				
                type: 'post',              
                url: "{{ route('updateeditorder') }}",
				data: {
						"_token": "{{ csrf_token() }}",
						"orderVal": orderid,
						"userinfo":userinfo,
						"scriptdata" : v1,
						"storedata" : v2,
						
						},    
               
                success: function(result){
                    $("#btn_editorder").hide();
					$("#editorder").hide();
               },
                error: function(result){
                    return result;
               }
           });
        
		

	}
  </script>
  

@endforeach
 @endsection