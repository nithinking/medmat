<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use App\models\Deliveryaddress; 
use App\models\Features; 
use App\models\Profiletypes; 
use App\models\Systemsetting; 
use App\models\Programs; 
use App\models\Useraccess; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
class DeliveryaddressController extends Controller
{
    public $successStatus = 200;
    
	/**
      @OA\Post(
          path="/v2/addDeliveryAddress",
          tags={"DeliveryAddress"},
          summary="Add Delivery Address",
          operationId="deliveryAddress",
		  security={{"bearerAuth": {}} },
      
          @OA\Parameter(
              name="firstname",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),    
          
		   @OA\Parameter(
              name="addresstype",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		   @OA\Parameter(
              name="addressline1",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="addressline2",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="suburb",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="postcode",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		  @OA\Parameter(
              name="state",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="mobile",
              in="query",
              required=false,
              @OA\Schema(
                  type="string"
              )
          ),
		 
			@OA\Parameter(
              name="lattitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			@OA\Parameter(
              name="longitude",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
			
          @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
      )
     */
	 public function addDeliveryAddress(Request $request) {
		 $validator = Validator::make($request->all(), 
              [ 
              'firstname' => 'required|firstname', 
              'streetaddress' => 'required|streetaddress', 
              'postcode' => 'required|postcode', 
              'country' => 'required|country', 
              'mobile' => 'required|mobile',
              'latitude' => 'required|latitude',
              'longitude' => 'required|longitude',
			  
             ]);
		 $response 	   = (object)array();
		 $user = Auth::user();
		 $user_details = User::findOrFail($user->userid);
	
		$useraccess = Useraccess ::  where('profiletypeid', $user->profiletypeid)
									->whereRaw("FIND_IN_SET('9',featureid)")->get();
		 if($useraccess->count() > 0  )
		 {			 
			$deliveryAddress = new Deliveryaddress;
			
			if($request->firstname != '' ||  $request->streetaddress !='' || $request->postcode !='' || $request->mobile !='' || $request->lattitude!="" || $request->longitude!='')
			{
				$deliveryAddress->userid = $user->userid;
				$deliveryAddress->firstname = $request->firstname;
				$deliveryAddress->addresstype = $request->addresstype;
				$deliveryAddress->addressline1 = $request->addressline1;
				$deliveryAddress->addressline2 = $request->addressline2;
				$deliveryAddress->suburb = $request->suburb;
				$deliveryAddress->postcode = $request->postcode;
				$deliveryAddress->state = $request->state;
				//$deliveryAddress->mobile = $request->mobile;
				$deliveryAddress->lattitude = $request->lattitude;
				$deliveryAddress->longitude = $request->longitude;
				$deliveryAddress->deliveryaddresssts = '1';
				$addressverify = $this->checkaddress($request->addressline1,$request->addressline2,$request->suburb,$request->postcode,$request->state);
				//echo $addressverify;exit;
				if($addressverify == "OK")
				{
					//echo $addressverify;exit;
					$deliveryAddress->save();
					$response->deliveryAddressDetails 	= $user;
					$response->msg 		= "Delivery Address Added";
					$response->status 		= $this->successStatus;
				}
				else
				{
					//echo $addressverify."Fail";exit;
					$response->msg 		= "Selected Address is not valid.";
					$response->status 		= $this->successStatus;
				}	
				
			}
			else
			{
				$response->msg 		= trans('messages.general_messgae');
				$response->status 		= $this->successStatus;
			}
		 }
		 else
			{
				$response->msg 		= trans('messages.user_feature_messgae');
				$response->status 		= $this->successStatus;
			}
			return json_encode($response);
	}
	/**
      @OA\Get(
          path="/v2/getDeliveryAddresses",
          tags={"DeliveryAddress"},
          summary="Get delivery Address",
          operationId="delivery Address",
		  security={{"bearerAuth": {}} },
     
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getDeliveryAddresses(){
		 $user = Auth::user();
		
         $deliveryaddresses = Deliveryaddress::where('userid', $user->userid)->where('deliveryaddresssts', "1")->get();
		
        return response()->json(['deliveryaddresses' => $deliveryaddresses], $this->successStatus); 
      
  }
  /**
      @OA\Get(
          path="/v2/removeDeliveryAddresses",
          tags={"DeliveryAddress"},
          summary="Remove delivery Address",
          operationId="delivery Address",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="deliveryaddressid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		@OA\Parameter(
              name="userid",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function removeDeliveryAddresses(Request $request){
		 $user = Auth::user();
		
         $deliveryaddresses = Deliveryaddress:: where('deliveryaddresses.userid', '=', $request->userid)
													->where('deliveryaddresses.deliveryaddressid', '=', $request->deliveryaddressid )
													->update(['deliveryaddresses.deliveryaddresssts' => '0']);
		$success['message'] = trans('messages.Remove_delivery_address');
        return response()->json(['deliveryaddresses' => $deliveryaddresses], $this->successStatus); 
      
  }
  
  /**
      @OA\POST(
          path="/v2/getbanners",
          tags={"Get Banners"},
          summary="Get Banners",
          operationId="Get Banners",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="bannername",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getbanners(Request $request){
		 $user = Auth::user();
		
         $bannerinfo = Systemsetting :: select('systemsetting.settingvalue','systemsetting.settingimage')->where('settingkeyname', 'like', '%' .$request->bannername. '%')->where('systemsettingstatus' , 1)->get();
        
        return response()->json(['bannerinfo' => $bannerinfo], $this->successStatus); 
      
  }
  /**
    *  @OA\post(
    *     path="/v2/verifyaddress",
    *      tags={"verifyaddress"},
    *      summary="verifyaddress",
    *      operationId="verifyaddress",
	*	  security={{"bearerAuth": {}} },
	*	@OA\Parameter(
    *          name="addressline1",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*		@OA\Parameter(
    *          name="addressline2",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*		@OA\Parameter(
    *          name="suburb",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*			@OA\Parameter(
    *          name="postcode",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*			@OA\Parameter(
    *          name="state",
    *          in="query",
    *          required=true,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),
	*	
	*      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
	 public function verifyaddress(Request $request)
	{
		
		$addressverify = $this->checkaddress($request->addressline1,$request->addressline2,$request->suburb,$request->postcode,$request->state);
		if($addressverify == "OK")
		{
			return response()->json(['addressverify' => $addressverify], $this->successStatus);
		}
			
	}
  /**
      @OA\get(
          path="/v2/getautocompleteaddresses",
          tags={"Get autocompleteaddresses"},
          summary="Get autocompleteaddresses",
          operationId="Get autocompleteaddresses",
		  security={{"bearerAuth": {}} },
		@OA\Parameter(
              name="address",
              in="query",
              required=true,
              @OA\Schema(
                  type="string"
              )
          ),
		
	      @OA\Response(
              response=200,
              description="Success",
              @OA\MediaType(
                  mediaType="application/json",
              )
          ),
          @OA\Response(
              response=401,
              description="Unauthorized"
          ),
          @OA\Response(
              response=400,
              description="Invalid request"
          ),
          @OA\Response(
              response=404,
              description="not found"
          ),
		 
      )
     */
	 public function getautocompleteaddresses(Request $request){
		 $user = Auth::user();
		$add = str_replace(" ","",$request->address);
        $addressvalinfo =file_get_contents('https://maps.googleapis.com/maps/api/place/autocomplete/json?input='.$add.'&key=AIzaSyCvYESmh7woXSFQnOLOr9WhN-A_GYUEKhE&components=country:aus');
        $res = json_decode($addressvalinfo);
       // print_r($res->status);
       // print_r($res->predictions[0]->description);
      //  print_r(count($res->predictions));exit;
	  $addressval = array();
		for($i=0;$i<count($res->predictions);$i++)
		{
			$addressval[$i]['description'] = $res->predictions[$i]->description;
			$addressval[$i]['terms'] = $res->predictions[$i]->terms;
		}
		return response()->json(['bannerinfo' => $addressval], $this->successStatus); 
      
  }
  
  public function checkaddress($addline1,$addline2,$suburb,$postcode,$state)
  {	
	$addline1 = str_replace(" ","",$addline1);
	$addline2 = str_replace(" ","",$addline2);
	$suburb = str_replace(" ","",$suburb);
	$state = str_replace(" ","",$state);
	$address = $addline1.'+'.$suburb.'+'.$postcode.'+'.$state;
	$curl = curl_init();
	$url="https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyCvYESmh7woXSFQnOLOr9WhN-A_GYUEKhE";
	curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$response=json_decode($response); 
	return $response->status;
  }
  
  
  /**
    *  @OA\Get(
    *     path="/v2/getnoscheduleinfo",
    *      tags={"getnoscheduleinfo"},
    *      summary="getnoscheduleinfo",
    *      operationId="getnoscheduleinfo",
	*	  security={{"bearerAuth": {}} },
    *  
    *     @OA\Parameter(
    *          name="packageunattended",
    *          in="query",
    *          required=false,
    *          @OA\Schema(
    *              type="string"
    *          )
    *      ),	  		      
    *     
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function getnoscheduleinfo(Request $request)
	{
		$response = (object)array();
		$user = Auth::user();
        $user_details = User::findOrFail($user->userid);
		$cart_itemschedule = DB::select("select * from cart where enduserid=".$user->userid." and cartstatus=1 and itemschedule LIKE 's%'"); 
		// if($request->packageunattended == "yes")
		// {
			$deliveryinstructions = Systemsetting :: select('systemsetting.settingvalue')->where('settingkeyname', 'like', '%deliveryInstructions_no_scheduled_items_leave_unattended%')->where('systemsettingstatus' , 1)->get();
		// }
		// else
		// {
			// $deliveryinstructions = Bannertext :: select('bannertext.bannertext')->where('bannername', 'like', '%deliveryInstructions_no_scheduled_items_personal_delivery%')->where('bannerstatus' , 1)->get();
		// }
		
		return response()->json(['deliveryinstructions' => $deliveryinstructions], $this->successStatus); 
	}
	/**
    *  @OA\Get(
    *     path="/v2/getscheduledinfo",
    *      tags={"getscheduledinfo"},
    *      summary="getscheduledinfo",
    *      operationId="getscheduledinfo",
	*	  security={{"bearerAuth": {}} },
    *     		      
    *     
    *      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
    public function getscheduledinfo(Request $request)
	{
		$response = (object)array();
		$user = Auth::user();
        $user_details = User::findOrFail($user->userid);
		
		$deliveryinstructions = Systemsetting :: select('systemsetting.settingvalue')->where('settingkeyname', 'like', '%deliveryInstructions_scheduled_items%')->where('systemsettingstatus' , 1)->get();
				
		return response()->json(['deliveryinstructions' => $deliveryinstructions], $this->successStatus); 
	}
	/**
    *  @OA\Get(
    *     path="/v2/getitemschedulecount",
    *      tags={"getitemscheduleinfo"},
    *      summary="getitemscheduleinfo",
    *      operationId="getitemscheduleinfo",
	*	  security={{"bearerAuth": {}} },
	*      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
	 public function getitemschedulecount(Request $request)
	{
		$response = (object)array();
		$user = Auth::user();
        $user_details = User::findOrFail($user->userid);
		$cart_itemschedule = DB::select("select * from cart where enduserid=".$user->userid." and cartstatus=1 and itemschedule LIKE 's%'"); 
		$itemschedulecount = count($cart_itemschedule);
		
		return response()->json(['itemschedulecount' => $itemschedulecount], $this->successStatus); 
	}
	/**
    *  @OA\Get(
    *     path="/v2/getitemschedulequestion",
    *      tags={"getitemschedulequestion"},
    *      summary="getitemschedulequestion",
    *      operationId="getitemschedulequestion",
	*	  security={{"bearerAuth": {}} },
	*      @OA\Response(
    *          response=200,
    *          description="Success",
    *          @OA\MediaType(
    *              mediaType="application/json",
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthorized"
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Invalid request"
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="not found"
    *      ),
    *  )
     */
	 public function getitemschedulequestion(Request $request)
	{
		$response = (object)array();
		$user = Auth::user();
        $user_details = User::findOrFail($user->userid);
		
		$itemschedulequestion = Systemsetting :: select('systemsetting.settingvalue')->where('settingkeyname', 'like', '%deliveryInsrtuctions_no_scheduled_question%')->where('systemsettingstatus' , 1)->get();
		
		return response()->json(['itemschedulequestion' => $itemschedulequestion], $this->successStatus); 
	}
	
	
	
  
}
