@extends('header')

@section('title','| Settings')

@section('content')

        <div class="outside">
          <!-- <div style="position: fixed; overflow: hidden;left:180px; top: 63px; width:90%; height:auto;"> -->
            <!-- <div style="overflow: hidden;  margin-left: -255px;margin-top:-63px;"> -->
            <!-- </div> -->
            
            <iframe src="https://test-app.medmate.com.au/iframe/?businessId={{$locationid}}" style="height: auto;min-height:650px; border: 0px none;top:63px; width: 98%;"></iframe>
            <!-- <iframe src="https://dev-app.medmate.com.au/admin/sideNav/pharmacy/createpharmacy" style="height: auto;min-height:650px; border: 0px none;top:63px; width: 98%;"></iframe> -->
            
         <!-- </div> -->
        </div>
        <script src="" async defer></script>
		<script>
        //setInterval(function () { window.location.reload(true); }, 60000);
		 
		 // View User Datatable
    $(document).ready(function() {
      //  $.fn.dataTable.ext.errMode = () => showErrorMessage('Something went wrong');
        $('#ordersinfo').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('getallprocess') }}",
            
            "columns":
            [
                { data: 'createddate', name: 'createddate' },                
				{ data: 'uniqueorderid', name: 'uniqueorderid' },
				{ data: 'customername', name: 'customername' },				
				{ data: 'suburb', name: 'suburb' },
				{ data: 'ordertype', name: 'ordertype' },
				{ data: 'deliverydatetime', name: 'deliverydatetime' },
				{data: 'awatingtime', name: 'awatingtime'},
				{ data: 'orderstatus', name: 'orderstatus' },
				{data: 'action', name: 'action', orderable: false, searchable: false},
				
            ],
			"order":[[5, 'desc']]
        });
    });
         </script>
     @endsection