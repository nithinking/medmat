<!DOCTYPE html>
<html class="no-js"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
		<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    </head>
    <style>
	body {
            font-family: 'Roboto';
        }

    .main-container {
        overflow-x: hidden;
        background-color: #f8f9fa;
    }
    
	.notification-info
	{
		color:red !important;
		font-size:16px !important;
		font-weight:600 !important;
	}

    .myform {
        max-width: 400px;
        margin: 0 auto;
        padding-top: 50px;
        padding-left: 20px;
        padding-right: 20px;
        padding-bottom: 20px;
    }
	
    @media only screen and (min-width: 1200px) {
        .myform {
            padding-top: 150px;
        }
    }
    </style>
    <body>
        <div class="tot">

            <div class="main-container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="myform form">
                             <div class="logo mb-4">
                                 <div>
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAABCCAYAAADZjSr3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyMDowNjoxMSAyMjozNTowME5y3RAAACEwSURBVHhe7Z0JgBxVmcf/1eccPfc9yeQOCQRyQCAHCRCuACKioCu6Iqx44bKygusFCAss4iKLywICUQQXEN0VkADLpSEEDSEYArkh18wkmXsmc/bd+/1f1ySTobuquqcnMyT1k3Iy1T2vql6993/f99733tP8gWAMNjY2NkcAhoLWEYjgg44gtrUHsUV+NvWE9U9sjnQcDmBCngfTi72YWujBlAI3PE5N/9TGZnSSUNA2i4A9sa0TK+p60NAXgebyANEoEJMDdqE+8mGRkPdMVZOfzmgQkwu9OH98Lj43xYeybJf6lo3NaOMQQesLR3Hfe+14bGsX/JobsVBQhCyif2pz1KJp0BwiYi43qjwR/POsQlw8OU//0MZm9HBA0Nr8YVz3RgvebI0iFvTr1piNzaFoThE2EbirpuXi+pNK4bANdptRBH0K9ISiuHZlU1zMAr22mNkkJRYJq2PZBwHcs65VP2tjMzpQgvYf69qwuk0KK8XMxsaMWAyxkB+/2NyNl2t79JM2NiOPY31LAE9+2C1uZp9+ysbGAiJq5KdrW9AVtC16m9GB45GNbQhp7gMF1MbGKrFwCLUBF17c1a2fsbEZWRxv7O2TghnQf7WxSZFYFH/c0aUCPWxsRhpHV9QVjzGzsUmDWCSELR0BtPTZQdc2I48aFLCxSZtYDJ2hGPbas0hsRgGOWNQuiEcdDic0t9f0iM8UMIF9rw6XmiZnYzPSaDXLtsRsl/PoQXO5MbvYicumFiBm0POlaRoe3dyBTR0RFXdmBNN88PRSLBmbq5+xsRkZbEE7ytA8Wfj0ODfuPLVCP5Oca1bsw0t7w4iFjAeNbEGzGS3YfWhHIZGotTHJiD10afMxw7bQjjJooV001oW7FlfqZ5Jz9Z/34dV9toV2pLB8Zzee2ukHjN6n5sT3TyrCjBKvfuLjhW2h2dgcJdR2BbGmHXirNZL8aIug42M888MWNBubowSXQ1PWNpcFMzo+ziuo2IJmY2NzxGALmo2NzRGDLWg2NjZHDLag2djYHDGkKGjsLTQ7kqCm23hU2IDmzY4fbvm3vqRzynCde/lbNU3Hw/Ry4mkyfac7vTRTof/6LnkmNVVIrsuf3FBGnnXYr6+QazBfXe5B9yDPz3uwGdVEYjGEozFED9PSXZz9YYXhKLp8xFCU+5bEhjW+cdjj0FQFF4pdURxT6MHYPBcKPU4E5EU29YWxY39IjiAiLq8aYTFd/ltVYA+c4QDGSVoT890oz3HB53IgqKe5q1PSlCPo8Kg1uzK20YvmiIuF4I2FUJ7tQHmuG/kuDW6nhqBcpkP+r6k3LPcRQdjpVc+j7iGThZZCKqLNVWNLs5yoznWh2OuEV9oG3kObP4L67hBagzGVV7EwN7uJ5+tIxKGx4rb4o1Jx+VuyfNDglea1WJ5nMHXdYaxt7MOmtiAaeoLyngGf24GxPjdmlmXjpDIPCuX5reKXSrWpLYD1rQHslPLX5g+pypYr77Ha58HxJV6cVO7N+O5WuzrDeKuhF+/Jc9RKmd8vLyss15XqIO/RhalFXsyrzJLnyUKe51BbY4vaSjIMzaQsLxqTjRJJizC/B+b2wxvacPfGPsQCBou5Sv369dmVmF+ZrZ8YSAxOi2q3PxBR+buuKYCt7X409kbQLZlMAfdKXSmQ91wj7495fXJFNqaJNmRidNWSoLHVv2JqNj4x0cff4icHI4W2R97Od1Y2ojUg2UgLRjLnpFIXvnBMPk6tyk5YWEPygFvlZT29vQtP7+hBd9SRuAIxPbFASl0RXDLZh3PH5ar9IrOkEA5GyqsSyRd29uCpD7vQGmKafv3TNKCQSR7kIiTPkYWza3IxSwpdpQhpdoLr88XtkUr4tyY/XtzdgzVSGaNKgIyFwRTdGhvjjeCCCXk4QwrvlAKpzJKvA++ChbhDRG1zewAv1fXixV096Ig41fX5HIdb0Li00MXL69AdlrtM0mBReBeUOXH/kir9DLBRKsQvN3diRb2Ui5huybNh4MF/84hEIBqAz0zOwxXT81VeJIOV7HdSHp6RcrZNKhnFPV7jB6Up9aHYFVPb9l01Ix9jpOINhb/s68WjWzqxusGPPlBs+BwiTErh+68rAsbFACJhjPc58dkpPnxR6k2uCDe55912PLBd3/MjIUwDeHxJCU7WxejG1c1Y1xqJN2hCWyCqGhZjo0GThsKFnMHlWtQmX+7lgSWVho3Hdql3v/ugGy/VdmNvr1yHBoBSVnle5nE//c8r13NFgtIwZeGyqXlSrnPhHoKyWRM0cel+fGIevjgtXz+TGJqTS5/ehYawR1lk180uxKdFfBj/YoXN0mreurYNa/kSuPNUP1KRHfLSLzvGh28cX6iExCr1Iix3rG3BK3tDUjH5YgdkqgUoAFlaBJdKhfn7aXmYJAKSKmsb/Xho436s2OeXsiR5nYbFyPsodMfwNalgvJdULBLmwS82dEhB6xJzQCy0au2wClqzCNo5T9eiJyIFOJmgyfMtLAF+fe4YaZBiePD9djy0qQt9ImTq+kaVkEIvfz8lN4bb5pXgxHIRqkFQFO9c14Ht3AIhLM+kJtwblAU9zQp3BDedXIxzpAFNlVZ/GHf+rR3PioDGxKqOP4d5+VPdMJKnM/I1/HhuMWbL89y7vg33bpbyYyJov11ajZPE4iGXv7QXb3UcNBBiSkSN67qCz07BGYiIT54WwsufHnfAAhwIG/GHNuzH49u60BVj45nCFphSt/u7iU4sceEHJxYpgyEdBt11cljIzKC15Y9qkMYFj55dgc+K4loVM3JssRcPLSnH0mp3vPUU+HILpPL+7NRS3HxKaUpiRtja/OfplfjCFPbZpSZGvIdZxU48Jib4TaeUpCVmZG5FFh5cUoF/nVuEfPEv+t1Wq/A+5pY48eS5lbjquMKUxIwwD26bX4q7F5Uh1xlT7tXhRhUDipLB4XE6xGWO4V9WNeHnm/rQKy6Z2uuCnxshFYcV/cOuCL7+erNYQodW+oc2tOObK1uwvZPfE5crEpKzJuVZT7OxL4Jr32xRq/KmAq3LL73cgGdqQ6IhegNtoQ4RtbOW3OfGjgiuWtGMN8XCS+SJmKECaeVZ+w9LYkZ4vwP+rv9wJqnLdIe//GoDfrG1D53B+L2n1GhLvtCKpPD+rTWMK/7UhN9t269/mBqWBc0KYcmwY4s8UnnLMU1+poPP7cRPFpZhVqGotjdHBEDDvYtLcaFyd9NDXHbcMLcE51QdFEpj6N56cekELx45qxKz02wtBkKv4vNi4T50RhkqsiR9vW/RDN7v+dUuEfoKTE5TUPthHt4zP1/1u406pFD7Rcx+tLoJy/folcJMyAZBq2B/UDyDVc2o66JoAQ+IpXfX+i4xyqSS6q5XKlBcKLI3rWnD+mZr3RYUs6+vaMKH3VJRaVFZFLLBsIJ3iqhf+3oDVtZLfqRx/8MNG48rX23E++3xBsCyaCaBz9wtz3zD2x14ZFO7ftY6GRW0LJcDP11Urjr7hgL7DW6eVwpq4q1iGS2oytE/SR+2VjfNK0OZS1oOMamNoCX35anZuH1+mep8ziR0Bx44owIl1Ca6FgZQzJZUOCVPK+Bjz3EGOH2sD9fMLtZ/Gz1QbN6WyvHMjt64mKUJB2CaQy7cva5VXL0u/Mf6/UqUUhXHQ5C/7406cOuaZgRMhujY+f3tlY1oonepu3pDgffeITq2WhKMu8mjBwr8NW+0oFUakUw86wGUhRjBHes68cz21CzjjNbWHBG0ihRdwmTMoPu5uAjnT0jfMhtMRY4TVx6Xb+jyUUQuqvHgB3NLlVU1HHBk544FxfDQhE9yEd7jxOwo7hBrlaNCmSRVl/VwEY7Jc6biqiSBbuoLu7rxw780iXEkQjYUMdNhhX1PvKD/2518hyv2fd+yuhG1/vgATMbg/WcgXzIJ+0WvW9UkFrHcHkfxM408M/ubb1vbhm1UdItk1vzIMLPKEg0dD41PT8lHsUNaugTLS1NEpubGcOMppcM+QfcMsZQun5qrXNuPICKn3GSxTosTdMAeuaTnmiUiKuKoFo0Yogs0mN9v64wPTiaAYqcGUY7gPW77Y9nuXNuC2oAIdypucP/IplVLQUS8M+rE7WIZW13Db1QL2nBQItbJqdXZH+3D0kXkhyeXqEGIw8E3ZhZhrFda3kGuJ0Vu6RgvFlcfxeuLMVRGrGXmBRua/n9brgwUx8F9VwPTdDIYObU0WXkZW1Wr988NxB+O4oH3UuzzkeuyHKrA8AOH3JNJV8RIwex0S1b9taEPz9WKCzwwEiEZImB8Jv70yWMVejTIf/p7Ne8T5jX+2hzBq3XJRncPxVlw0T/ebNZpyZd/WpUHs0tT6xz/UEzFP4rpz3iwtc0B9IRiqM51pjTyOZjdUpiW7+7F8h3deLspoNZuqhJXkiNkVuFO36/tFZdgQJ8EM52DBt84wXr/Urek81ZjH16p7Qb3N6VpHBGDgG63lTrCPkd+741GuY/+e5ETfOH/tqAsLfe9U+6JI3Pt+qYlvEa6PC/vbke3ubvDeMNPTsjBxPzEBbRXKvt/b9kft5gsWGEUMBb+iyfm4MppeSru8JRSydNYRN6/WEBs6VPsaGcZZszgBTVe/MM0Hy6VNBdWeCSvo9glaUbZr2rBNY04PZhZ7Mb0QYNef6rvxW8+6LVssbAyO+R6s0s9OFcar6VyX/PLXKjO1uAPRVTcoMLyc1IlgEun5KFa78N+RupIXZ/8Pd8fC5qVQkn6vzvwEEHKkmz/wrR8/NuaVklXvmfSp0fRKhAF/PzkXFw3pwjflLp1xXEFyks6vsitwloag3yXxvlOgW/u8ePiyXkqfMsIy3FoP5rjw5enF+hnjOEruG99K5Zt6UEvAyL7C7FcZ2axE7eJKzW9OPUVMR/Z1IH7N3Zhv3rZeprywo/J09TgwZwE8UeJ2CCt7KUv7NHNWB5inYkg/vc5lZirx/AYEZNrMqbrl1u6VPQ3XAOEJxTEvMpsXD+nELMsNADt/ggueLYOLUG5Dyl4HJCYV+LEY+eOUeXIKiv39qp7YkctRY0boOS6nZhRkqWCNM+tyZH0UkhQyGQc2tJnaqEMG7PCKxV9ok/D3YvKE66a+qK4dTesblOjf1b7lShmY3Id+NmpZQlj1Jh3//JmC1oDcm9mlVTqwlemZuF7c0v0M3G+JXn1SkPEktXCij6ryCmVvBCnSHkbXEl7QlG8sKsHd7/bjlbVR2VFJCUN+W9gHBpn4XSHpSTo7trT2zvxxA6/8fsU4fqx1KUTigflk6TNyJE+aZy+9GoDwpzi0F8HE8BnnFPswG3zy1QAfCI4enz/+1KnN+yXe5R3mVS8Nbikfj51XhVmmtSp9JtvA365sR3/udmPHhWTEh+1Ugc7VttjuHpFI/b1WiuM/Ty+pQN3vNuFDlH1Q9KUArStM4qrX2/CdnmBVqgUiy6PjZhuKbJCHl/kEkE0FzNO3bh5TQtufKdTiZmK0em/Fx4i2m+1hPGVPzerCHEzirKcOJNi029+i/VxwQSfZTFjobhldTOu+nMTXt4bQkNfVFlEDHJmVPjKhiCuWdWqOnA5HD6qkcqU54rhnsWJxYycP96Hm08plq+y6FrIJMnPXHnX9ywqTShm5LTqHNy5sMRwkOYgMdR1HyownGr2dpMIhQXhoSdwVpULj5xVgfmVOQktDo7yM4bz12dXoCaXbml6UQOTCtyYWeLBrDIRUDnG5Us6Kt+M0DCtMP79Q45Sr3ondP1opRqKmZTlEwodePCMiqRiRjxODdfOLsJV06W8G7qfMUTk89csuJ0ZF7TdnSGxzjpEvOTlJrD82GFaH3Lh3ndb9TPmUPzuWd8eH7ZO0CpTKFvFavvZOy0G2XwQFpg8OQ5EQzudWFKTq/rQzFgmYv3kThExuSbF7KNIiygi2ykW1/febEZDr3GLT84a55Pnku9J4fZEaeFZC1Phs97+dgse301h1a2D/paOh/xb3WdI3PS9UXxXRI0COFphob5MrEkGWBvxyYk+LCpnH5hRJYjD71wqrqvZANPpY3KxdCwn+Jt4DlKmWzl9aACcF9oRksJj5umIMHHQ6ScLyy2F4Uwr8urB0PJLf1kdAmyMrZAsiJ6B83/Z22NsxUoZznZEceu8UhRaHNBiGNG0PPlTzhZIhpTvv4qBYPYIGRe0F8Ql6HFIoTBwB1jxXqrtUX09Vnh+p7iZMfM0X9/bp+ZwmsG5YjkqvowKJodU+gUWRIQT3u97f3/8hbLPhZ23SQ6a0E2xbDwsJrUZM4o9yHdG1QsdK6WXkf1WeEXy8MntYhX6zYI3RWT9PXitKabc9lGJVAR3NIQLJ0nJtgD7U0yRNF2S5qcspnnJ1PwkjdRAYsrtGggFDUaVUYel7VqpvKkMOrHb4stS21Od5TIcNPSE432YIi7JYIPA/tTjkljYich2OdQMGKPQAhozOzqDal6wERkXtDUNfYbCo5CWrEssqg0t1iKv39pnIU2p0CExhd9tTjH+RzKRIy/j881FhC5kSW4WxmRFMVYafMMjR8MYVwAb2vxqnpsRpdlceSC+FNK4PI8yxc3gBPyHN7THa4klu1S+JcL9KxE0s0IxEnBgoSrbgQmqL8CcE6Sic8UTilZSxL0qz9IwycDtGch0sYgKxeU1d8sOhau7mMGO7Uk+TSzB1IPEP3dMPnwaLfiMV9eUqOsOoZexggb9oIxJ4wT1H/ylSTyURkvH9+W7nMxuOBNCrslsrk8wwjyQjOZQSNyZfaLipuJDpABzxNIMekh7euR7Jua8Qgr3LlHxVNA0J8pFUAosuACXiFXw/AUVWH7ROEvHcxeOwa/Ork64IsdAuCRLTV7csqu2aJ3REt0olkFKQY3yXtqjbqwSS3bUIZW1NMdlec5ikdeBfMYQGFRyvtsySZMB31ZgN0SRWE8fmZhtQgv7g00GO2jBzSnLstRYDYbLQ00vEheb1v8I0ih129AtJFLG3mmN4H93h/B0rbXjD/JdNfhk1i7LtdlHbERGBS0Qiap4HI4CWqHPwixp+v3x6SYW0pSvMJQiJaQ15vpsVkJJaBqz/y3Vw8oaUhXZUmClIhVbnOLEkJiwQywPi3l9ALkXjvKOOuS+clKo7JwozTXozEglnIdfVV0RFt7XQDgIY1o+JUnVaKXJODZ0KVqOmYbzZNWDmMBGln3l7Aaydsh3xXswRd7L4AGZwWQ0h6LysFzMzyoplpthI8dtvU9juMjn6oaCx2KZ59peaWWgvJ/2wOhzOYcLq43r8KPFR1HTJG7ZjWyFsbLizrAi12816XcfWckfJaQiwsMFrT/GjlnxrMlQgpPNghNtUsPaKq4xaYTSL2cdjJEb4XKalYK1O1yYlV1b0KSQcKXd0UIPe/stUMWxfJMg0ISIW1tjsePdxhoFtK7N+t2kpdrSnp6rz1CbrfxbhvaMIGrFabO+QoF9fWoKV8aPbNP9CGxBkxfU1hdGwIKocXSQw9acy1erfmbqCGFvd3zgg3siWIGxWgUuuWeGj1hGWrdwEAsszIawsU4VI3dNLAeGg6xrCVgOVRrIO0192MW11dJpwDIIByfiI5HJn5WxdufXZOGWE/Nw85zMHjfO9KoVsI3I6NQnTrn5xLO70cBoDJORTqZ57YxsXD3TeO4kBwQueLYWdb3mL5RpfnGiBz+eX6afSQwHGrjG/bZOSVPu0+eK4YVPjZeCadyB9eTWTty+bj8ckdRGUs3glCS2whGnGycVO/HkeWP0T4zh8jj/UxuGWljPAmo6SpGGx5eOseSyHs6pT7y3RWVO/Oqcav2MMQyFufCPtdgj5SJZWWOg7txSJ56Q57UCux4ueb4em/ZHk44e81mPK3DgmQtr9DPA7z/oxI/WdqoObiNoZXz1mCx896RS/Yw5tEiufHkPVrearTkm71P+Gzj1aTBcufeuDSbrzUkD+dg5lViQYJMUzr08/5k6tDNrkuW524PF5W788uyDe0McTmwLTSpYT9SldqYx47zxuSh1R+DXPGBDm/6hoS/qOPB7r7iZNBAZsLhrfxAd+sRyM9gYlDjDqpKZIgXVq0XxvRNLhtT/ZvNROCXIqRo543ylIHEC++t7uLGBNe5/r82CmB0euLMYPQOj8BHOEHqjIZj2EtpDxRY0Ii/ojT3msVmcd3mLWH9coUFzyEtlJ20qh6DmrEm5z2cAJ08d+EwOafVapfXb1m7NAuSMgn8/tRR57viS4cngNRm+cMvcIpxou5sZh0ujj8/j1oIm7r+8Z4Y1Xf9mK16uNRY1xnTeu74d974vlp+lyenDDz2Js7hZjEl/ITdjuUM8Gc5kSRda4P+7vdtwQc1E2IImsH/jz/U9apUDMzjn72cLS1Ca5VB7Hhj2YbFfRT6n2NAdZozTGZUecH+Aq2cVy7kEIuT0YEW9NReSLKrOwbIzyjG7hBXKHd9wub8TVW3m7MG0QhfuF5fwM1ONd+2ySQ+uKHwmZwBYsJTZbUIL/J9WNuH6VU1q9gknt7PLgd0r+3rDWL6rG5e/2oB7N3apLpH+xnA0cE6ND3maCKxRTFw0qhZC+M6bLfj5u+3yfNb6/lj/uPXjT99pw6eW1+OH7/rV/gypYAsakUJW73fgtTprLcrS8T787rwqfGmSV63coURLH4WJH/JvsYqcYnVVZmtYUuXGDXN8ePoTY/HQmRWqrymLbl+CjmSKK6eB9FoQ1364bNLjZ1di2Zll+Pq0LFw41q02VvmHKVm4b3EpnlpaqYTYZvj41OR8tbBAonf6EUSkIlLp/1gb3y3p/GfrVH9g/KgTIWjDOy1cAEHSG0ViRji6zsUBjDwChTwj13W7b3MPLn5+D256qxnP7ezG+hY/dnbGNxdf3xJQdW7Zxg58541GJWJ/92I9lm3rQ10P3exgyo9vC9oBYmqeo9kmGP3Q3bvxlBI1venxc6tw+9wCfPeEHHXcfGKeCEkJ/nDhOCz/ZA0eXFKJy6cXYBKXb9HhlLiEiLjWibi+lKKpzah5LoNz3Zxi3L24Aj8/vRLf505XNTmWp/7YpA93OVs6lhax+Rp4CqmpavNrEba2YAy7emLYLQdXaaGLOVrczER8dUYRCrSQasgNiYkoBf1idUbx2x0BXPfXdnz2+Xp84lkR7ufq5N91+ObKVvz0vR4srw/J87MR1+IzB9Ic0bVLug5HtTZ1AU9sTW01Cm44ckpFllq/6qvHF6njC9MK1Ma0xxV70lvOWwr5w9JqcbDA5uMDuxF8Vir6YMSaYQVWldhCnNdIM0Ya82tnFemDUdYsUjXFKRSQhlxDSIo1HRD+W4m3Pk0qHlc5tDJvC9oAYuEw/uv9zpT99nQwmpJDt/PDXodae83m4wMHB66ZWWBt1DlVrLiyh5HLpNG+qIZ9tikOMrHcDzwyjC1oA5GWpCus4fo3m+OrhgwjXk4jMXif7D94WExGK6ve2oweLj+2EBeNjQ/OZAqGSbi0zFf+ocAu4FvmlWFJuZbRZx0qtqANgibw9m5xH15vPLD79nCgVoEwaqHE9WB/3ndXtcQXEMwQUbnm2sZRuHzQEQLndd66oAznVemj4EO0rGgBzStz4/PHFJp3xB9muJLM3YvLxVKLT3VK2dW2QKrzrG1BSwA7azd2RHHFaw1YzQUrhwErK+WwT6U5CHxjRRPWNAzdUqOY3bG2Db/ZMjJBj0cLXGjgrkUVuHKKF1y4UsUepoqDgpiNhSUO3HtGpdokezgEY6hwI55/X1iOG+bkqf5k5YKmuJ7cR4nvo8AwpFQXUrAFLQnswOTQ8Vf+1KjiYlr91qL3rcAVTl9lrJm4uGbQYmzojeJrK5rVKGyQ++SlQWNvfKfrx3ZHMRyDnlyfzOYgXO7nB3NL8MDp5Ti2UI9FpIVlWNmlInNit1h2+V4XvjU9Bw+eWamEIsjF9Pm3JsdILJdEzeEo/lPnVeIz473KclMxkCqIXO7LFElAvqdETAmihhlFbtw404OvHV+of8caDqurYLotKCVbplSKtZUF+hi0mEqaVqb18DtW6h9HPoPhCJZt68UlL+zBf73XrvZwTAfOc319by+uX9Wo0nqxzq86/63A7/WEIvjJu1340isNaj8GqyOgXHbmN1v343Mv7sHz9fHYJqtTn6xYkao0iyVZ4ElecGPyBhkhfyDgN8mR6gYuakm4BOkMPFJNk8KRKJ2BhxIXi5xVk4PfLq3CXfOLcFqFR8RJ8omipdKSSs+Kr8cuMlZ1vM+pLLvfy998e3aRKv8kS35yRyyu0pv4gPrpNljih3OYVeA1r5n0yDFd0SIZk/I9+MmCUvzhgjH49owczClxwSf3dHD1jUHX4u9K5KHKz8wiF74yNRtPLK1WsZN/P60g5SgBbdyvPoyZxbzQ9FtcGd8OK6m8SCb4JcOe3NqBLo7LmrQUTHN+mRtzK+VBDdLkonKPS4Xcz30rTYa0meYJRQ6cPjbXME365U9Kmm28T5OJ+QfQXYecWFDtDXhyeRaOK/GoFQiKs1zgQqckJv+jMdfuD4tlFVEry77f4pcjgH0B3qNLiUq6w/Oq1ZP7n+hzYGFVtgqqHZ/nRrFUFBZm7jVKa5LBi2sa+7BKRHRf0BkPCZCD1z8m34Fzx/uS55HAlp4bDe/sjsb/NhlqxV8NL15Ug9LsxI0jxWzFnj61a5B6AYlQS6FrmGdxahbT4ubO8dkdydJ0oDRLs7QBDmEqzK8Oo7ImafJ5F1en1xHeKGWCm31wlZXWvpDamNrr0sSldGNigRuTRZlo4QyGKzF3SSOW/I3F31lJFjfdTvwtboq9Yp80okbLtksD9XdT8zBhQMxkusjtYF9fRC2LXy/P2yLP2804O/mMs2Z8ko8lIlhjfW6MkTJcluVI2cUcjHbCo1tjHRYqthqKNrPmWA4YLMgnsQAF6JBNehORcpqSnoVh81hQ1CUdUVGmsVxDKiDXp3LKQcv0oKDF16/izkAx7l/Ifg+5d2WNWXAxraKek/nHZwgHVAvODmlqhtqVSLV8jPNJcF2Ks4VdhJTwmtwzW9j5JQ48muLGyDY2w4H2rVdrY8/VM/DNHvlKC1WL5RhYmZX2UomtifCQOaAk/T8P37XpStxxcgEumZKnn7GxGTkcVxxXBI/aDiy5721jAIWDVhIt3P6Dvx8uMSPqHvT7OIzXpvs7KTuC8yYYL7pnY3O4cJxQ4sXl3MhUWlobG8tIA+jQYvje3FLkWtx6zsZmuFFm2TWzinBaqaZGOGxsTBEXl31w35rhS7pKrY3NSKAEjZ3ad59WfnAawwjv/2czelHBjk4X/vHYHGkIS/SzNjajA80f4Bh1HAZtPrihA49s6UJ3zA1uiMBloRHv5bY5WqFFpkZVXRiXFcN3ZhfiArvfzGbUAfw/50MuBhwlxE8AAAAASUVORK5CYII=" alt="" width="280px" height="48px">
                                 </div>
                            </div>
                            
                            <form id="loginform" action="{{ route('login') }}" method="post">
                                @csrf
                                 @if (session()->has('error'))
                                <div id="div_showErrorMsg" class="alert-caret">
                                    <div id="divmsg"><span class="notification-info n-error" style="">
                                    {{session()->get('error')}}</span></div>
                                </div>
                                @endif 
                                   <div class="form-group mb-4">
                                      <label for="exampleInputEmail1" ><b>User ID</b></label>
                                      
                                      <input id="username" type="text" name="username" class="form-control form-control-lg" placeholder="Enter ID" aria-describedby="emailHelp" placeholder="Enter Username" required />
                                   </div>
                                   <div class="form-group mb-4">
                                      <label for="exampleInputEmail1"><b>Password</b></label>
                                      
                                      <input id="password" type="password" name="password" class="form-control form-control-lg" aria-describedby="emailHelp" placeholder="Password" required />
                                   </div>
                                   <div class="form-group mb-4">
                                    <label for="remember-me" class="mb-0"><span><input id="remember-me" type="checkbox" name="remember-me" ></span><span>&nbsp;Remember me</span> </label>
                                    <a href="{{route('forgot')}}" class="text-info" style="float: right;">Forgot Password?</a>
                                   </div>  
                                   <div id="register-link" class="text-right">
                                    
                                </div>
                                   <div>
                                      <button type="submit" class="btn btn-lg btn-block mybtn" style="background-color: #1da9d6;color: white;">Login</button>
                                   </div>
                                </form>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <img src="{{ asset('login.png') }}" style="max-width: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <script src="" async defer></script>
    </body>
</html>