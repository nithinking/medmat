<!DOCTYPE html>
<html>
<head>
	<title>Medmate</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {
  font-family: Arial;
  font-size: 17px;
  padding: 8px;
}

* {
  box-sizing: border-box;
}

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  margin: 0 -16px;
}
.btn-primary{
	background-color:#1D9AD6 !important;
}

.col-25 {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
}

.col-50 {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
}

.col-75 {
  -ms-flex: 75%; /* IE10 */
  flex: 75%;
}

.col-25,
.col-50,
.col-75 {
  padding: 0 16px;
}

.container {
  background-color: #f2f2f2;
  padding: 5px 20px 15px 20px;
  border: 1px solid lightgrey;
  border-radius: 3px;
}

input[type=text] {
  width: 100%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}

label {
  margin-bottom: 10px;
  display: block;
}

.icon-container {
  margin-bottom: 20px;
  padding: 7px 0;
  font-size: 24px;
}

.btn {
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 100%;
  border-radius: 20px;
  cursor: pointer;
  font-size: 17px;
}



a {
  color: #2196F3;
}

hr {
  border: 1px solid lightgrey;
}

span.price {
  float: right;
  color: grey;
}
.topnav {
  overflow: hidden;
  border: 1px solid lightgrey;
  background-color: #f2f2f2;
  color: #ffffff;
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
}

.header{
  padding: 5px;
}

.w-25 
{
  width: 30%;
}

.mt-27
{
  margin-top: -27px;
}
.pl-1 {
  padding-left: 1px;
}
.pr-1 {
  padding-right: 1px;
}
.mlr {
  margin-left: 0px;
  margin-right: 0px;
}


/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
@media (max-width: 600px) {
  .row {
    /* flex-direction: column; */
  }
  .col-25 {
    margin-bottom: 20px;
  }
}
input[type=text] {
	margin-bottom: 0px !important;
}
label {
    margin-bottom: 3px !important;
}
</style>
    <style type="text/css">
        .panel-title {
        display: inline;
        font-weight: bold;
        }
        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 75%;
        }
		@media (min-width: 768px)
.container {
    width: 1000px !important;
}
    </style>
</head>
<body>
  
<!--<div class="container">-->
 <div class="topnav" id="myTopnav">
  <div class="header">
    <!--<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAhCAYAAAA7zlIvAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAGxVJREFUeAHtnAt0XVWZx/c5576SNIW2FPpIShH6SmBQigKKEkBQVAaQ1RRaAWXadJCHDo6OMz4ICurSpUtRlLaISJHSVscRkeJaaMtadiE4EaEmLW2BtEkDtEDbPG7u65wzv/++9yT3pimUx3INjw2nZ5+9v/3t139/37e/vW8cszr0TLPjT/vx4+NMzPtsaMw5xjhHO8bEiYc8RN/cgQ6GPE5oQp9XF719wHXcH25fPPtps3o149NM+tvhlYyABU39TzefaHz/127thLowlzFhPgsPIGUxpfdbJTjGicWNk6gyQd+eQcdz5u9YNOe30eJ7q4zC69FP5+g7th2eG8w95lbXTgrSfVmw5MFYz1s1BHS84CRSyTA7WHA9857tixoffRtcrwwObi6T+6I7dvykYLAvA6iSFI/xSJK9FR60nxXN0vgClMSzFlUSyZ11aw6JhYFzA9/GzDPkS2O+HQ5mBFyIzg8H+3k58YMp8KagcQQgqb2E6yR49I4nPMeLuSQXdX9o4kE2rY/31f3kianGwQRbvUbj9XY4iBGQdDoqLOSilXoQRd7gJAJO6CB5wgL93m0lFRgjVRKpmt6Ntz2UbAqszX6IFy8cztdOia03SXDmzZtXsUgaGxvD1tZWSe3XJQhYCm8dEQ+AJJ3YoHQR+UAy2Nfnh9XV28fN2TVt76YFJp68g7yi1IqGxn9TjY/mOlyzZk3FTpfvCAdlfbcD8Kr+iYD1qgq/YQtJYBnj79zZ/axpPb1AvJcndG7teDG0GELtlTSiCI0nz8sbPyClPAHqoosuer8x7r/iS0qj4ZHfTm0QmLtXrbrrNxHNa+1thTh8rczeYOWdI94xR5sVM/1nT9u3H5hEBaCiDvn5N4VEHzdunJ1voHRcTU3NgkQivigeTyyuqam9yHWd96m7qMTXpa8HkljRDikaWr3VKCpl9xSGATG7ikMLeEd5I0GqfInb8tUe0R1M49WGke04+PJFq4lFyb5OhncxqF7F2flhnafy1qaoqUnbt+O4vF++aU1NTRXjNnHixCHVoryZM2c6W7ZsCYmb9esNT+vIcTCSDJpo0alpKrNnz55gpIpS3ojgwNeL6JUnMKynIh5bT9SGqNyUKVNkPxXgbzvHeGQzmQyuS986LJnDFE9a9J2dnTHZWiPtLb7dnp4er9QvO17U56od8PdH0jv1yzqiQY/aEeIkxJulTWKUxSzhNA39QsFx2TqlsHFdxlbNxMANMrQp8KVSogFXBz0nWW0djuyoLKuwAI9shkZZviOBSBEb1OjAqJ5ECi4lloKIn1d5ssPyuoqlKv8VQNQPz4kjjNxSVTQxyAyEjhd3wiC/zQ/Sx/csOTF9zE33Jbdd85Fs/dKO8/Ff/Zq+FtsI0tRUNwznbl/S8Nem1nWx9UXVWVkbXxp4HnUsGrQKmkjFiE4ZvNXP/YLyeUblE/HYr1BZQqn8qLwjsvnzFyxEWt2Zz+ctEBOJRCybzX551aqVN0Y0ZW9r6L8c4AVmgKZ5sSECQvSNieE6TOCzplDYhWSKWckkX49r6rzq2kODwf7AHxxY7zrm76DcZ75nApsznKqaqjAz4FMmdJJVMQEuzKb/GmbNo8iLF/EAjYfLSU4ydWxYyAMdbbk4TqoMBUAAcGvcIN2b9jMDf8ME6ASYGRo2FsEzm/xj3UR1DIBoUDRJdhWWsSngNog58ZShrc8Ckr9B9kzohDEIZ0B3Av6phN/7fOAOHGFBkJx05KhgKONpo+uva/JbelrifX197w0CN8l68YOAMTJOPxO6QUTYLydR31lEJ/H0Mz6PBEH+N5oYgJGALie6efM+MSsWC8/GtnkH/VI/unzfuY/8DuUT1K+hdmni4GEn7uKLLz6DOs6A95GQuEignQzoH1euXHk/5QMBh7ZNgEXOcYJ4GLoDY8fWrEBiHe667ljU3kymaagOxaE/cv78+UeTrx3wHnhthpdAbqUobZ/ounGO+8IToZ1c0gI9fD88MDBw7z333NNXohfjsFJihWFBTkF/oPc/upc0fBvbI9U5fXrBnO4UkGwPA5oxgWMW7mxpZLKGQ90tHTM4/riJo5APq4UAjMFxru1qmfP7YSpiYG7ass2XMsm3IDVSSMBhcIV4uwFkmB/MmSD8DjLp9u4rG7dVlF8XxqY9+cRJYRBc56bGnBVkrP9N0sVKAWjhUR0D0HsB5HWBZ+7sXtT4YjmPuqWPH+c4sZvwMDQUwoGjihJrKxJrxstKLPHRAHtefFs8Hh/LijfJZNJks7nHMpl4UzKZ+4HnuZfGYsPrVZMG3V/C0Lts1aoVmwSQSZOmfBtWVyIpsOmGQy6X85mwG1eu/MV1pVQLrkgaLFiwYAZA/InneWfyDBckFpDh+4X7GP6ria7FhpqJFDJqC2rPf/bZntTkyVM/j031jWw2U6Ce4UaWONHWAv2KFQqFv/h+/pRISs2ff/Hnof8ceUfwrqi31L9O1zX/edddd91dytyfuYBBYbsyOrdPz5lPYVOBXBisSJjUiqdajt43d+n/xsWg7Zm+0DQ0hd3NzlY+z6lb1rGBGa5OZuIna6JMa+jOndzmtc2E7kEo4LvDmJ/XLWvfZfz87/j2WJNCuO+kAFV2cGvgefN2Lp71GGk2FOuaS13rQwGc8pIMZ9ctbb/OTVa14iEveqFCeCRSAtWToRt+uGtRCZRSPQ3XOaZjvWOQON2Os5Hyp7NQPu3ka6qIp5OTrB/P1vdy/zBRam8G+2Qs71w+XwAcYXUqlbsPkJ3CZPoAROMXgV3gezfp9yHNTif5u1VVVR8fHBwMScsy1qwBK3VYIE4C2q9edNGC/N1333UDIHZ3794t+6kwf/4njgc868DiOAGQEKk7iltDN0beRwD5HKbQS6fTAlsWOmyB8EXAGWzevMXiwv4zSkfVFoJy7O6xuAgmr04mUxdoEfGoX6o3qlt9dAH5dNf1Vl588cKjWBTfRHKNBBZMrYgM7KBYUCihYY3T3dz4I5jY0LbkRHQZyk1DzLg0rm5PtDc35mK+szAfyxW2XdOQFSDaljj5NjUCgJlWATR0G01HrL2lcW39svab3epDrgr69+Xwfiex03YFheDMnpaGrrlLw3jbTLjvNmFbs0NdpYCNM3dyraP6u5c0Xg+Qx3lVtZ/hOCqH+kuYXKYPN/q52xfN3HbMTUihF4/Jm+vgswa1Ituo1RibDui7Whp+LAlqrjImtWdQPSkLpNPBsoSKqNUcdvyNh0bX5mCGHoGKQfaYZ9KRG0VwCUBICHc65f6CqjmMSS9AFiMtiZTRQhYtEiQoQCtJ8lXU3S+ljlTxwoULx4KjXyExBCoZmRifFrgB/DyBAQAZ5SExj6J+umbBFlceMa+9vd2JxRK0ynYrAobYR4EMnMa0nQQrWCZNmnprKpW6IJMZ1BwIE7BzEpFERrKpnoA3/fNjtO8bqOGnAdbdQ6vKctdgFRFrP61E0m2Ssmsjul5z1PKnjoBQilk9cAQqXS/pvGJO587Fx3ersCZ/ys0b6wW6IqiQHICr3ewqdioIfo4dBRvOJxkIbKcreq48rksT39ZCx5BOus5Tt7qr6qjlG6mPADjanrnXtwfCfB7SE34BUDFiqBSMdDp5/fbFMzdF4DHmevWH9juyx2ywkpSYlYTKI9TOnGvflqAIGBsd9Xs4J4pJmkf8PSZ4K7O3j8HX2Kqv4hhj/AXAwwQ4qRteuwHVk5p4wFGidaAL86gr7CJnyM0PRL/A2jsaiaFdnEAlXg58qC9IU+dzpBm+U8QL8FZ/VO9Qv+RZJ70qmUwIHVLBQ3mK0wSAF0dgFj0vAOS8WMy7DFCpDwIb+TEWjUnTjg08f1a/WSCy8TQGqlfw+dYll1xSUwksSo8ShoaaXdOXwljsiUI4uHna8o7lBpsnApfAF6nI6UufmF2/bNOf2JS19+51NkJ7Fuo0sHebTJMFVsZ3N4OEbd7Yw7Dz83/oXjznv+mfY6VMacJRV1929vb9rRB6j4of6u8ky6fDhKqrvbUxR0e/71aP1Uahh7X2M7Xf8pAKVJ1IybrlHZ9H/f6hfnnHg/D5jhaHlbq6a0Xo29I21MficBcBp7yK4bcJ+/1jpRSD2g10TsU2OZZ5b0Br3F0CVwQ6qTxJKpe5/x5QmjVx4oRGAHQeZXuHwSVAaL2ak1UT6nAM5S6RZCNImglUmkhJqC8j7BoZLtV5ApN9r0BLfgSaqF/+1q1bx4D1X2JofxKQ30Y7lKe5oP0xByzeQ5ub+/sHPg37r8His/Ahu+gygq9L/jrXDd+Jmj6V5xRo3g+Qn1Y/RStQsyiOZLM5Tw3dP9DuocTWdZ4kRf3y9vNQXTeEuUEEJpu3Qw9fVLdtczfi6XpUDdvyMGhzimrLd4I7vEMOe7e/b3fIZmBmMLB3FQe5x3U3z9ppUGfwDnZf2dhfv3zTcyYWP4buWaNv7tK2WNuSuXYEAcHt8UlHX0ZZ3Ay+cavGTPb37vpz3a3tJ3Uvch7JrG63bXdi7n1+/x7mIny4ezGGutSu1J/TavtQP2XTSsDbbF0iJLvJmg8Evc9/cOqPNn1wZ/OcFwCf21YusYY6ftARuziZ56tWr/7Fhlb48fS0tLRc2tvbfyITeAwDLnCxJmMyjB9kUj5X4q6Zuwfp8CU02g8BmG2zQMM8TRYNwuV4hMm0IgsrhSThmMTgKjzlN5f46PU8z7nwugdXwrmATOMYCQ5YB1WoVtmXG1GzOXhcDk/Vh1nreoVC/iHcDfZcB1twCkVPKqk6K6ng9zj1nQH9UIDfQ+xuz0Zz/h1gJWk3NqMB2OYjlcBSNfZh7zcUihKGHfE8LaMwb3W8y4QnWILzkFo3Sm1Zm8qY/LTlm+aGjitQaTUEwcA+360aO87P9rFVNbc2msPd9hJv1lUuTPfRB/+RoepQsdNvbp/kB+H4/DNPriWdO2KOF/TvzZN1BA07gbRH2juKKrXrX2b3INm20rZNlsdp611pQEKh/paO+SyGZn/f8xpkq7Z97lgBtHcCrq+Q9lnT0ODMRWJhC76awKR4Un9dODjWiQGGtnvOOefEly1blsUIv5/sq5ggHZvQDa0/82vRXX311ckJEybIsYh9EtzLxH4TGkmnyKbUxsJ4XjAzFktKOpWD83HAaUEFgONyUOLYTNx+++3YX+5XAMG5FNXcRuByaIPVFOLJXoF6FLPBzjV16wDeBvLfxxqoklYlPQJnlt3hDXwfipTNI7mYfrrkhFm+9xCZRNs1Furj3EpgiW0ZpMxpTeyfrGEpU30CXkVND+qPlWBtU1M7bdPG2h34PbBabKNwyx+BbxN/ty9OVjerkOuHh4kgNXnYUGam3RCXgRd63DLALhv3lO1856cbnqPJ/6y0/QK9mBuyMUDKWCOfDQJt28F9l12inbsF476hyIdWNtEQJWsYuRpa7JzcFEwzeZTE6kT6WZUowoMPlpcFDGX2lIxqActvamqK2LwQRXgzEbYtkixm48YJ/vqiR162EXaLP8DkCFh2yoXDYln3kNKkKF22kLph3T2oSQ8AWyACUOsfYyP4pOclnpYRX5JIRTZmTOmtl04jyj6LUds4RWnHBKQkUtGOG9IMY8Tz3q2n1LyhwtBqtyhha9tBndoATK0EVlRZuSqMWNAWdRAwaFJUu81xq2qHsU+K6+NCiAvk0vPR7spB2xUBGrGzbzHitoHrlrbOHfOKZYZthAryYhknRLrYTkRShpp2B+6wgW4sH1GHMnT1tm0vxm2dqjY5d5mJRbxKea/qxVAUB2OodBOx9Zqg8nQbZ6XbCZw5s4ejmVYHQETjF9Had3HdSuSXyZYSf9aDdfcMVVcWweWADRceML+MtCIKGKL6SS+2sYKAj8ruDOeqTr6snVUCXqwSWBGtBVH0Ufkuq70yI/qyhyD6KG+oah05+CLRFZakm/cztXw9Z6TGzOkF+8OOuHcmQ5qDDUJ3uCwgYvMMiBCN/B/HNulFOjYgTWU/GGsvPSM+onAex8tC1NW+WJf4CHijE8kqJ5fpkDtEUiu1rC2aXLJF88pCBIIDlaqA14GIDpDOZO4uTZZa5kjVMHCnnHtuS/WaNcvSqN3kmDFjCrgTtNMbJFuSpa4krTQOBxWoZ2gMGJIeRJDKqU4ftSgVtwGpfAffh0Eb7Txl72lTYh+EaYZiAphbCawh1uI5MhxkG4sqcGThA3w7OYx74/RmZ0GwzaoxIolCVToby7fGDp/W6O95lt6VZlszBFB0/oeLwTiZAXuGKR5B7wsP2UrwfRnTZHvipbM/C9x9S7yxE2b5/UUHPB77Kuy+PL250dKvWYPxLklZCsOxKOUf/a5oAXPWjv2lNmiFBEwwEx2fXlPT/02+P7N27Vq5IBQGL7jg0gkI8+/L70wQMkoDp8+XDuUSC0/cRqrpBSBjAZhUmwrX4d34xYoVKwZemlMxtxJYUTNGU4UHw+0V04QozjgHbs65FP2d9ZvhAtjWPCM7/SebPlbYvWOVE0u+RzQaIhwxcisYrlI/hSD7OXr0ZA6az7FXqx3zAbvjbMb/xWyYBvxqze/aW79060c5F7wFBu+Bg+cP9m7BeLh2+xXH/TX6aVdp4/GKW/9yBV5Okh2gvJ0FBIhuGGiH+RibgHakRmPJANcOTjbPNewAp9Kv2103eAGBPAdb/YuAbgZ0EmsWiAeoY5TkouDAbktwNLMd3n9kd3k+Ukr+L0kt3AiFdtKv5TRpHVKynzxvYMAc6nmF2bi0zqMtZ+KV+Pidd965rRJYUXUvoQojktfnzW4PQxrttmDqsk3f29kyZ4udZOwOOVsByMl1Szs+5LiZY5FaHmDa5YaFJzJVwWPPXXr8AN77W+xPtdK9Oc4I50yrm3TODmN+27imI97e3Gydtl3NM56krWdNXf5YXcwk4/a3giTYepp1gjAiRItrRPKr+UQ1vIbgBNrpwSADyJaicm4iLtGhFuKB8UNsmwvZEFzI4bXhgoL8SNwdsOLtFdtYUVs5Y7StRup9C17nU5d4ca+lIH/dkVT/K+rZgSsFp6yTQLpNQrIdkeLGC87UDNJNJwN2S6p3ZfjHSSwX94XhekwtZ34/pBEfkuOyCK7TtP0pdBtzP+l69guAjUFgrLWjIIR+8HWzbt3a9tMbc9ExUySVohMBqJxjblqboJ5IhRTtsoi75RR9lN7yN+8XRkkaQVOyU1gfTIG2KQcXhugAjiSPwPJjhPCFeMVP48hHEyfAObmcjoqsO8BFemhXKEmWVL1MsFRhxCt6kzR6iKTrdC4dlCTlw0jKVupspU6BrQCgxJMqvGnUO02cVJckGqDSZx80VlhVSixVzyjwn0Xt9M71sc7WdcaeszEhKjky5HIvWB7PDTykNzKaezcjiex3kedzA7kYRzKBjlmoytH2mSswWXaXZ+Mwvb1r8ZxPWq84ZQSwFxITrMEQsYz1FcJtL3b7ahPt7CuOa+iFGX4DWF17fP02964uY5qjYybTMdERn33ZQ9xCbbfT+Skns+0ak9WPdBHfT+n2Q92j3fFuBo7Jo+2jzAF3DqL6i2+13dLZN13g+kwlhb4YfPZ0RTo+5S3nDHHYSI5K9HNJg5OUiKctQ7EgugRYunIzj8sKD3Ag/E8AygIH/mqXataZYQK3BTctsg/yPYXJl0q0zma+K9ovNStAEKx/jzhOzeJOEHeJXCa20fjKrgdcklRfh39c7OgD2CleENQihbWwEmcRyO0wkV2sQD8kscRIGy5LR8QaaJ2fOt2KNRGSk9Oy4xGR6BUCrp1Yv0z3te+1kMVjta84DaIpgZEENgu642IiOsVJhqfFsMuZn889rMtwds4Cbf++Y9HsDSWA7a+uKKvrLzA4Vb/chgcNY9IG++ExZh4q8n7quwbP+hbVE7klFFfQEQ9647LulsZj9d19bb1tO66AXttHJQ4FulEmsRhch2sz45PJKgYyx9lb0nCoPG6InIhcCcyPJqFa+by5u+XIX0U8bQe+nD6VKnCkYsZz4CvplNIk+X760KamJk2ykRMUf9Xuyy+//NR0OvMNyn4Sm2eMFqUC/DWpyKzsj9Lp/tbq6jEPkS9JEgMUeidVhyXmH+qqqq5OyelqDwYBqxkY6K9Rvm6EUqfmV/Qh4LqhuXnBn3H3/RddeD990JHR0GIv1R1Q/WZA9VPq6qScBdbT7LD4CVhe69INc2mlz+NIZRL3oqpRN9xpw8gOTaOdRGsUsr3U2VVoxuNp/wHYT9MKjdogZRrsRT6ByjZNPPVb2PDCacvaD0Mo1LCOAzyLkjgNYd5il60eyzszUHBSNSebfPZPgGMDxf/AIccTtAtDDP3smgkM5SwG5lTSTsGw165QS6/YUdqPzYanv/ZDZrB/IyD9H/bBGwLH3Un9XIsN5yAkP+ZWH3pCOLC3jzq+yzcX2vH7OM4A83OssSaKXYm6xYp14e/z8551vmpgsEH6Mpn8FYh+PI5OjrM3fGXOsxzyFpAsIgnlCVeE47hfcSlQtgjADfF+5xJ00+5eRYPKsQsUMPUCBHjK7eJwFSedBOSq04oVOUHlDL3tttv6SLuaI5dv674UGD8KfgxJ0IUG2rBmzYqnVW9z88IlnFfjbHW4LZEHXCGsM3uVp4BseIh2C6B28Km/GtrfKy9qO1ELLtooX9sDfD/ALYuGXC7/LrLqqVegRAA5PWwc2mfPPmYjdLa9pDu66Pc9t3b8vwV9L6oSORRxv7HAdC24KMZJounsxjDhIiQriTmhVymNbxFBSHvsHFxPWetdV2IUyniq7mIWQIKxPa8qp5V4jgEwBBHXtQRgiW1RyNWgtOLtVLVPzIrLlshQ4NIgsxpzuRqtZloetFG7S8XZWdpdkw6vi4FuMT/8ztBwFirBrdqy5CfDdO99O1oaPkpdxTZaiV0q9tIv0Wu8RgsvlTcavU1j4lxdf4ku4I0kFPh0i6FsgkeS6Pul6h41D8lZce14NKZKK9FpbMPKv90w2A+4dNXWVh6hT2UUJBVUcVmQX8xeBykfQNEMicoyYvE7CJ62hPjZVc9bLldQYVWr0kt1aaUW7TZbYr9/5OkHeMWypXYjKQEIIWqfQFweRKen9LcbMvzthnC/v92gCdQFvKhg+Y8pojS9BQTUSgXw+bYDX06n+MHyHI1WaeVtOAhe9gcZKqOy6gug2O8HFMorC/bue9RvlS2Pr169WueKpbkpAeX/31+bieas1M6ouVHyEL6jjLLuD0Uj4pE0B0pXQfD79l+bGRrB1xJxor+iYo9R3rJ/H4tf9CC9pTQBVxcD+vbfx3otqKLs/wGyXUnMdDY7oQAAAABJRU5ErkJggg==" alt="" />-->
	<img style="margin-top: 10px !important;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAV8AAABOCAYAAACHSBULAAAABHNCSVQICAgIfAhkiAAAFDxJREFUeF7tXc1uHMcRrhqJBCUnsAwESRASsHSLlgFMIQhyjPQEko45iQTiJZKLqScw+QSiTgbpBFrnBbx6AlM330wj4DKHAKaBZeCblwApMzY5FXTPznJ2dn6qen72h7WAYUDsn+qvq7+urumqRsj53Xn1zZ13fjx/jEBPAOEOADzMq6N/VwSYCOwBQY8A22fzC697a/d6zHpaTBGYegQwbQS//uTw7o0b/scIuDr1o9QBTAUCBNS6vPS2vvvr/aOpEFiFVAQKIJBIvou7h6sI/gsANJau/hSBuhHY6DYbL+vuVPtTBOpEYIR8F3cPXqm1W+cUaF9JCBgr+Gzu9nN1Rah+zCoCQ+SrxDur0zyd4zIEfNxcXptO6VVqRSAbgQH5Bq4GeqWAKQIThQDRVnd9eXOiZFJhFIESELDkaz6u3bzhf6U+3hIQ1SZKR+DiEu/pR7jSYdUGx4yAJd/FnU4LEZ6NWRbtXhFIRoCg3V1vPFV4FIFZQgDNPd6f/fTD97M0KB3L7CGg1u/szel1HxGqr/e6q8B0jJ8Inh+vN7anQ1qVUhHIRwCXdg/aAPg4v6iWUATGisCbbrOh0ZVjnQLtvEwEcGm3swcAfyqzUW1LESgfAep1m8vvld+utqgIjAcBQ740nq61V0VAhkC32UgNh5e1pKUVgfEjoOQ7/jmYJQnesAdDsAII77LLA4CSrwQtLTvpCCj5TvoMTZF8EnJ0cXdJ2p8i2FTUa4qAku81nfgqhi0hRyXfKmZA25wmBJR8p2m2JlxWJd8Jn6ApEO8X//j3zxd8+j1X1O7NhS9h7d45t/wklVPynaTZmHJZlHynfAInQPylnYM/AuKXXFGmOfhGyZc7y1ouFwEl31yItEAOAkq+qiKKgAMCSr4OoGmVIQSUfFUhFAEHBJR8HUDTKkq+qgOKQFEElHyLIqj11fJVHVAEHBBQ8nUATauo5as6oAgURUDJtyiCWl8tX9UBRcABASVfB9C0ilq+qgOKQFEElHyLIqj11fJVHVAEHBCYNfJd2j0cSrV6cQnflvWWXPCCzPkHUZhP5xa+7q3d6zlAX7hKkjxljpcr4KSRbxIuZixlYCMPsiA4AYR9LphpuYKJ6GsAbKGH+90P75ucwoOfedDT82gFAZ6Y/6TZr0xD0fZPby7sR5XaAnpxvkI+rQDQKiIOLQLB2KRF3wDRng+473loF5kZ++LuwQqid8c25vsPAWEFCM3/RVm/pMLEyg/JRuT3jpvL+0ufHtoE5gFWYOckTa5xk+/Sp4cfczG4uIDP4kTaf1LrGRmdADTjTfhRjwDaAPDS4MPtz5QzWBL5z8zcIsLdrPYRvc/i60LSF6csTx4AAtpHwNbFJb5O2nwWdzvbCMBaQwT42XHzfsvI96tP/vPLOe9/fxuWFRcB4S8c+a1eArxEouQNy/Oo++H9LW5bppzRgXd+PH+MQE/I6HvqPNneewBodKCdhk1W33LyBRC9KJCQL/gNIG5yFcsuiB/fbgDgBoeMiOCzSx83JRaKJT9A80RNFUnl3xBg62xuoS21an6z07GbT2WPmxKcEMDmpY9tGV6HqwC0iQDvR5Vr7OQryU2N+Ciqg8FzWv4LyQveBNQ6m7v9PG9e7QZGZDYG6UscexeXuCaZGw7RLO4cPrPzl0ksKVtDwphFSZKItrrry5uWfP/e+d2cD//iyOxahquTxuC7ccP/GAFXXfsCgvaFj8+581Uv+UaAlw7QECQQtNKsVGPpoudtcEk9qf/Fnc4GIryQypZoHwF8SwQb/11vGCup0K+SzYFo63T+9nYecWQJvrRzsAmIA2uTq+jWCnR4QSWvfdHDAH3yDTb3H14BwhO3SbKW8KM0K3hxp/MCETbc2g5qEeBaaC0WaccQzM0b9MphE4h1Sz2fcC3UbdFcThj5Wkv3p7cvCpFuHB2C7bP5W1t5a6se8jUWFtJD6TEtrmjBkeDt3igB0+vTudureYPlKG4pD4oW2GSyZCxjcyCAbwHoSdG5COW0GwPhnjmV5JFjdGyiBduvmNe+lHyNO+qdn95+ke5i4GiMpccRAi5O6sN9FyXgwPr2P5dY9nmjD2USzeUEka91u/j0yuUEkI8NGXfEWtY6q558dzq9Mog3HGywU/2wHx55jZvheL3hflRIQNGZgI0/3MMnRazvvEm1i8inNscFE2/LnA7O5m8/LGOTirYdEnB3vRH4rRk/0YKtgHx9gqeIZI6ZKb5dxiAiRYjg6Gz+1oMQ28Xdg6/KajvsxjWDl7M+MyAwBIxAZv3xXHYTQr5VYnIFW/apqHLyNQuzLCsrHFTfh/YFCP3PDF0aFBG/6lySdc+RMWptcspb26wi4o1awJJ5Hjf59j+WsDcLDs6hIbC4e/CqzGNspO+9brPxiCNLwlqRVBOWtR+eeFhOAPnWQ7whhOkEXDn5CmeRXdx8YT2bu7VZthUXCtA/Nh6xLMwaiddpURGcXPi4wv0QwJ6EAgXHT74FhM+oSgTbRX28WZJJ3A/B7Y2337CJsRpIhlsdM/maj9gewud1DHVAv7FTUfjvU0u+dYC3uNNpcW4aSBZEmXL3r/h8lNemOWKX8eEvrx/J32eVfCUYuJQ1176Om8sPOHWXdjvmdCi9YcFp2r3MGMk3+ODofzWWzYig3V1vPI0Cp+SboUb9WwZfZVoiFficJZq9uNs5il/5itUXXQ2U9F2krJKvO3oE9CDPxRNxzbl3VEXNMZLv2Dej2PVGJd8cBVvcOdhPDcIgODmdv3W3KtcHR/fzjlGuH2k4fRcpo+RbAD3GbZrFnc43VXzFLyB1UHVM5FuvnzcZJfNR9ni9cW9sbod+NNfjUAAf/P3/frj8uvCk9hsYRKh4/Qgiop5L9EkoT+bRnrEIssYVHIOuAhVcw0vTiKyKmyBlzdOkka+9Jw7QJvSOkPy7BHiX43Li4pHYvmP0Zt6H5jKsXisvBlGYljMB7uacsHhQjIt83TejN0TQNpG4AyzIYCG44RFBJuoCrM3y7YduGkf3iA/K7Ajo4VrRK1pZUUrcaKS4BqXumAWsXtOmiXhKsUz2AHFLgkWqjLFjDm91jJYKFjMQgX+Sd9zl9jEx5JtxPdBGPXl+u1D4OcGJD7Ca5HPvfxBrAeDAGOHhR71uc/m9tLLcbxWJ9Ym2LnyvlRxGXEIkaJR8//ndO3P/+/4PUTmI/N8i4Cc8HAB8gD97iN8ljwWo27z/Ju90mFTXpidAWM3Sdxv4BdiWbUr0uttctgE9tZBvP4ok5zJ79p24vMngHCvMx4qzuduPJG6CNL+viSk/bjZEkUtZG1B8fOar+fF643neuMO/L+10etGbGSaY4rjZSMkfkN+q3SDA/yjxripBGzx8Kdkg4j1OBPkybqmkB/bkY2hLMDZAF7LMcieJgk2uTFt2IBRnraWik3NarCKxjvjaKPCDtlz0I5y7Wsh3aafzOS98k3qnc7fvScjRTDLnw9hAGRxcBYnKzFhUUQXkbUAxlRXIGl/Ari6HYDcHc081N/jAnCaOm8trTBoaKjYZ5HuVZyBrDNY95NE+69phpCHuBi261hi2n6J/LlYeOJzinPoxstdMvn2D53uujrrch5fqR3g7qnLyFRGjnRt4frzeMElu2D+p5XA6d+s9CcGPkC/BiSSaywzEhWy4llOwAZnEMDZu3/5crpcFcwVfSK7iSK4+RSfUBY9Sw4uFcyjVMTNWiZ6JQ8dTyDeeb4O1iISGRNimU181k6/YSq8Fi8D1UDn5SifIZTFLj1lSgh9t/8pvw1FuZyshaJwV1dRPmvJNKI/LLYfMmx0ZA+VaeJNEvtKTgfwjlkxH4vOXq1dp5CtPWFToKiLjquPwUGomXxn/yOYsOjCZhR347KsnX7kyiCwG+aKw1rUoH8So5cs7rg4sBAcMohPLudfZt64prJdnJcYXt0xJE6hBaDGM2/KVbsBxfHPJUeAyCtuSEFma/NIrZi4npOjYxRZ73eQrW3vmY/dQbvHceY4UIN/kBk/L0zzcklmfNZDvwfeSY6wVUbCQxZMfYCDa7ePkK1XYMCG5ZCKjZS8u4IgTGhwhNNH4TF+ShZ80DocNzSg5LxlLv8O8DUV0AhLo2GATjX3UzJxPl/YlRJFCYiIMTDL/ZgNd9dLUE1vsNZNvFQmOiuA1qIv4qAby7QysMbbQAsV1tNhE5DSi0AL52GMuoaAr+YoXUKKs2defRixtCdFMCvlKZHbQEdFpoBzyFa2DNBUVEX7N5CuSrYQ1yG5CyZcH1dSQ71Vyc9GicnHdJCEn+cAkIhol31G4J4h8RacmJV87lzYVp8POIFvYkqddQhUTWA1q+V6tywgWsjkKnrkxSViK/STzJrEilXwnm3wl0WNKvsFcEm0p+TLoRi1fBkj2RVe8x/FNm9bU8h3FVITJBFm+IgNOyVfJt9tssFPtzTr5Su9iJ7t8ZXefRUSjlm9Vlq/oZlHSvIt1p2bydb0+yTM3CpRSy5cHXpx8JdeU4slzeD26lSLwzZPn5mXhzNj/5EWUm5oyRyjZHUkl32osX5H/1TEYJyq52O1XM/nK9IxeA8EggY7bKmTW8rw9dTswsEogX/Y94bI+ZjHEHCoi+fhl3QCxl4il/UmuB6rbIRldEVGkux2kV/hYQTxJEju9lFE3+Yr0WmZAiNdIrIKSLwPBEfIVvCbgSjQMsbKLCD5+mYbiD5NK+pfe8XXFJO9Oqsj3KMRHLHPV7aeRr4hsglmW3lsPdYP7ksqQLtVMvnWFF0vWS1hWyZeBWtKilnxccnnwkiFWdhGnCKurZ+C5/bskIhETWV8YJd/IrKSRr9PNFXlGQTGphaLXTL6ysF9zBUye+ZC7VuLllHwZyCWRr/TdNpvfAaAlzYyVIt4bAthHgNT321xyZJi+ZBsFP/VefByiI7aS76gaZJCY6AQwaJl6gN5TTprQpd2O0TtR8qurbrJD8ychpWRZBNwn/mdE8OB4vbGq5BsgILsHm3xXWewrc0u+HAhs8vMCwR4gbZsEzxxfssQ6jypGkOLw7QYhriYlijbWLgFuFnmUU8l3lE9FmGSQr0sGtoFhCtQCm6t6eeTD028+PXiMBJucdKOpNk3Nlq+RwyWxVZEHHvo4PUHzUgngnTS+UcvX0fK11Rz8eoF1adI/+mZi0l8wMC8sIOwD0R4htOOLgXXFx8H1EIej/+yTUSD7O725sC9Jx5kGr4ho1PKVWb5Orod4F9QDuHo6p7RXkMdAvsGac7vNY6xgIG8b0P86aUMKrNvzDwDhIfi0AkgPE3LZJBp7Sr5FyJeZ7jGrizi5SQgu73hpdu+z+VsPyiBLBkyiIkq+1Vm+lmyyHn4VzVTJhcdEvi7Wb/LIrX/8SGj9K/lGwCzD7WCbc/1SXIZK55Gv7aME67cMWeNtKPlWS74ct1QV85rb5pjI18jlonO54+EVUPKtgnxNQIPL00e8OcsuxSJfMK83ew+4Yb9lyMVpw2Uh6G2HCLKMTVX+dhln5gqWGSP5Sp/7KTjSaHUl32rIN7iectxcflDiZLGa4pGvbUr8cZAlQIFCSr7VWr6mdae34QrMKavqGMnXWr+l+MNZI1XyTYGpNLdD2H6RxyTFU9mvICBfs0E4P3bpKl9WPSXf6snX9CC7Oug408HH4XdZtcdMvgEmw+8dsuQuVkgt36os30G7BO3T+VtrdX3gkpCvlZFxVHXVscXdg1eSl4yVfOsh36rJxualBTJ3WHmvkkwA+Q4wIdpmbxquCyOop+RbOfna+7hk7keuJV1LKTZ/iYtV/EoIEWwfrzeelymLIV6T0CfPJxvtU8m3PvI1Pdnjtk/tMskmDDQSzeWEkG94KgCCFiJ+UOZ6iLeV5pbUq2YM1MUWpmkTcbP74f0tRvPiIvbDwU14BkSb4sr9DeLy0nta9CNc//kh81y9Tc+p5BuZDYc74GWSWJJe9OfLBFHwrNQ05SI48QFWwyCbMuWuIsItb43YpFKAG2VuTGGfJiAJ0Ns+bt43uA/90OE+YOn+0hFwBIrrmI2r+jHYUz4coYet05sLL8twRQQRcvBRkDayhB/ipqtsS58efgzkb0QvlFdKvpSfL1i0SQp0LERaRDJVt1/AhWTuvCLCdlL0Yp5WmSRKZ/O3NqL6LMJlgizf6FjDqM5SSNj6wGnPkG5WuLaxfEUp6IwJjeBt5E3S1d/J5Slms6hZeTWvctgKJKpnDMMCEbQJsJ0WKZMkvSFbQngfCaKhivyBcksStH2PWv6F93WWNby0e/gnInqCaHx8NmwyvpezE9QT+NtlXFSPCqDky53woJxxRZjnzgFoJevoba03wNalj+0k/ehnNzOGQe6PAFtJVuBgg9s5+CMgfpnbUL+Aawh9Vvt2cwIwG9Rd1ikhEo3qA+5zw+7R8el1LjZaLh2BwaZEBPuIMFBeArojJKYScR4JKzV+7DHKEwyNbL6BRuamb/2azJ9LmHRSNGJad1W3f3EBR0XdRlHZreV3cT5EoGX3wZmaX/zj3z9f8On3nLKmTPfmwpewdu+cW96lXNa8u8xzKAOW82y4y5C0jiLAR4CAHtTxEZMvkZZUBIohgKa61PVQrEutrQiIERD56MWtawVFYAwIWPJlZcgag3DapSJgEXD4eKXIKQKTjoAl34CAO+brZ2py7kkfiMo3mwi4PFE0m0joqGYNgQH5qvth1qZ2+sfj+kTR9I9cR3AdEBgiX/uI4o9v96qO+LgOwOoYiyGgxFsMP609+QgMke/ABbHTaSHCs8kXXyWcRQSSLvLP4jh1TNcbgUTytS4IewHb31Yr+HorSJ2jN+/UIeIq5xHHOuXSvhSBKhBIJd+ws34oorm8bqI+3q9CCG3z+iIQPgxKAG1uZND1RUtHPksI5JLvyGA3v1i48+6dhVkCQcdSPwK9k945bD6qNDKp/lFpj4oAH4H/A2FR0qS5k3d3AAAAAElFTkSuQmCC" alt="" width="35%"/>
  <span style="float:right">
  <img style="height: 100px !important;margin-bottom: -20px !important;margin-top: -25px !important;" class="img-responsive pull-right" src="https://dev-app.medmate.com.au/Stripe-Payment-Logo.png">
  </span>
  </div>
    <!-- <p class="header">Payment</p> -->
</div>
<div class="row mlr">
    <div class="container col-75 ">
      <h5 style="text-align: center;">Payment request from Medmate</h5>
      <!-- <span class="price" style="color:black"><i class="fa fa-shopping-cart"></i> <b>4</b></span> -->
      <p style="text-align: center;">${{number_format($amount,2)}}</p> 

      <!--<p>HElloooo<span class="price">Heelloo</span></p>-->
      <!-- <span class="price">$15</span> -->
      <!-- <hr>
      <p>Total <span class="price" style="color:black"><b>$30</b></span></p> -->
    </div>
  </div>
 <!--<p style="float:left;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATQAAABCCAYAAADZjSr3AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyMDowNjoxMSAyMjozNTowME5y3RAAACEwSURBVHhe7Z0JgBxVmcf/1eccPfc9yeQOCQRyQCAHCRCuACKioCu6Iqx44bKygusFCAss4iKLywICUQQXEN0VkADLpSEEDSEYArkh18wkmXsmc/bd+/1f1ySTobuquqcnMyT1k3Iy1T2vql6993/f99733tP8gWAMNjY2NkcAhoLWEYjgg44gtrUHsUV+NvWE9U9sjnQcDmBCngfTi72YWujBlAI3PE5N/9TGZnSSUNA2i4A9sa0TK+p60NAXgebyANEoEJMDdqE+8mGRkPdMVZOfzmgQkwu9OH98Lj43xYeybJf6lo3NaOMQQesLR3Hfe+14bGsX/JobsVBQhCyif2pz1KJp0BwiYi43qjwR/POsQlw8OU//0MZm9HBA0Nr8YVz3RgvebI0iFvTr1piNzaFoThE2EbirpuXi+pNK4bANdptRBH0K9ISiuHZlU1zMAr22mNkkJRYJq2PZBwHcs65VP2tjMzpQgvYf69qwuk0KK8XMxsaMWAyxkB+/2NyNl2t79JM2NiOPY31LAE9+2C1uZp9+ysbGAiJq5KdrW9AVtC16m9GB45GNbQhp7gMF1MbGKrFwCLUBF17c1a2fsbEZWRxv7O2TghnQf7WxSZFYFH/c0aUCPWxsRhpHV9QVjzGzsUmDWCSELR0BtPTZQdc2I48aFLCxSZtYDJ2hGPbas0hsRgGOWNQuiEcdDic0t9f0iM8UMIF9rw6XmiZnYzPSaDXLtsRsl/PoQXO5MbvYicumFiBm0POlaRoe3dyBTR0RFXdmBNN88PRSLBmbq5+xsRkZbEE7ytA8Wfj0ODfuPLVCP5Oca1bsw0t7w4iFjAeNbEGzGS3YfWhHIZGotTHJiD10afMxw7bQjjJooV001oW7FlfqZ5Jz9Z/34dV9toV2pLB8Zzee2ukHjN6n5sT3TyrCjBKvfuLjhW2h2dgcJdR2BbGmHXirNZL8aIug42M888MWNBubowSXQ1PWNpcFMzo+ziuo2IJmY2NzxGALmo2NzRGDLWg2NjZHDLag2djYHDGkKGjsLTQ7kqCm23hU2IDmzY4fbvm3vqRzynCde/lbNU3Hw/Ry4mkyfac7vTRTof/6LnkmNVVIrsuf3FBGnnXYr6+QazBfXe5B9yDPz3uwGdVEYjGEozFED9PSXZz9YYXhKLp8xFCU+5bEhjW+cdjj0FQFF4pdURxT6MHYPBcKPU4E5EU29YWxY39IjiAiLq8aYTFd/ltVYA+c4QDGSVoT890oz3HB53IgqKe5q1PSlCPo8Kg1uzK20YvmiIuF4I2FUJ7tQHmuG/kuDW6nhqBcpkP+r6k3LPcRQdjpVc+j7iGThZZCKqLNVWNLs5yoznWh2OuEV9oG3kObP4L67hBagzGVV7EwN7uJ5+tIxKGx4rb4o1Jx+VuyfNDglea1WJ5nMHXdYaxt7MOmtiAaeoLyngGf24GxPjdmlmXjpDIPCuX5reKXSrWpLYD1rQHslPLX5g+pypYr77Ha58HxJV6cVO7N+O5WuzrDeKuhF+/Jc9RKmd8vLyss15XqIO/RhalFXsyrzJLnyUKe51BbY4vaSjIMzaQsLxqTjRJJizC/B+b2wxvacPfGPsQCBou5Sv369dmVmF+ZrZ8YSAxOi2q3PxBR+buuKYCt7X409kbQLZlMAfdKXSmQ91wj7495fXJFNqaJNmRidNWSoLHVv2JqNj4x0cff4icHI4W2R97Od1Y2ojUg2UgLRjLnpFIXvnBMPk6tyk5YWEPygFvlZT29vQtP7+hBd9SRuAIxPbFASl0RXDLZh3PH5ar9IrOkEA5GyqsSyRd29uCpD7vQGmKafv3TNKCQSR7kIiTPkYWza3IxSwpdpQhpdoLr88XtkUr4tyY/XtzdgzVSGaNKgIyFwRTdGhvjjeCCCXk4QwrvlAKpzJKvA++ChbhDRG1zewAv1fXixV096Ig41fX5HIdb0Li00MXL69AdlrtM0mBReBeUOXH/kir9DLBRKsQvN3diRb2Ui5huybNh4MF/84hEIBqAz0zOwxXT81VeJIOV7HdSHp6RcrZNKhnFPV7jB6Up9aHYFVPb9l01Ix9jpOINhb/s68WjWzqxusGPPlBs+BwiTErh+68rAsbFACJhjPc58dkpPnxR6k2uCDe55912PLBd3/MjIUwDeHxJCU7WxejG1c1Y1xqJN2hCWyCqGhZjo0GThsKFnMHlWtQmX+7lgSWVho3Hdql3v/ugGy/VdmNvr1yHBoBSVnle5nE//c8r13NFgtIwZeGyqXlSrnPhHoKyWRM0cel+fGIevjgtXz+TGJqTS5/ehYawR1lk180uxKdFfBj/YoXN0mreurYNa/kSuPNUP1KRHfLSLzvGh28cX6iExCr1Iix3rG3BK3tDUjH5YgdkqgUoAFlaBJdKhfn7aXmYJAKSKmsb/Xho436s2OeXsiR5nYbFyPsodMfwNalgvJdULBLmwS82dEhB6xJzQCy0au2wClqzCNo5T9eiJyIFOJmgyfMtLAF+fe4YaZBiePD9djy0qQt9ImTq+kaVkEIvfz8lN4bb5pXgxHIRqkFQFO9c14Ht3AIhLM+kJtwblAU9zQp3BDedXIxzpAFNlVZ/GHf+rR3PioDGxKqOP4d5+VPdMJKnM/I1/HhuMWbL89y7vg33bpbyYyJov11ajZPE4iGXv7QXb3UcNBBiSkSN67qCz07BGYiIT54WwsufHnfAAhwIG/GHNuzH49u60BVj45nCFphSt/u7iU4sceEHJxYpgyEdBt11cljIzKC15Y9qkMYFj55dgc+K4loVM3JssRcPLSnH0mp3vPUU+HILpPL+7NRS3HxKaUpiRtja/OfplfjCFPbZpSZGvIdZxU48Jib4TaeUpCVmZG5FFh5cUoF/nVuEfPEv+t1Wq/A+5pY48eS5lbjquMKUxIwwD26bX4q7F5Uh1xlT7tXhRhUDipLB4XE6xGWO4V9WNeHnm/rQKy6Z2uuCnxshFYcV/cOuCL7+erNYQodW+oc2tOObK1uwvZPfE5crEpKzJuVZT7OxL4Jr32xRq/KmAq3LL73cgGdqQ6IhegNtoQ4RtbOW3OfGjgiuWtGMN8XCS+SJmKECaeVZ+w9LYkZ4vwP+rv9wJqnLdIe//GoDfrG1D53B+L2n1GhLvtCKpPD+rTWMK/7UhN9t269/mBqWBc0KYcmwY4s8UnnLMU1+poPP7cRPFpZhVqGotjdHBEDDvYtLcaFyd9NDXHbcMLcE51QdFEpj6N56cekELx45qxKz02wtBkKv4vNi4T50RhkqsiR9vW/RDN7v+dUuEfoKTE5TUPthHt4zP1/1u406pFD7Rcx+tLoJy/folcJMyAZBq2B/UDyDVc2o66JoAQ+IpXfX+i4xyqSS6q5XKlBcKLI3rWnD+mZr3RYUs6+vaMKH3VJRaVFZFLLBsIJ3iqhf+3oDVtZLfqRx/8MNG48rX23E++3xBsCyaCaBz9wtz3zD2x14ZFO7ftY6GRW0LJcDP11Urjr7hgL7DW6eVwpq4q1iGS2oytE/SR+2VjfNK0OZS1oOMamNoCX35anZuH1+mep8ziR0Bx44owIl1Ca6FgZQzJZUOCVPK+Bjz3EGOH2sD9fMLtZ/Gz1QbN6WyvHMjt64mKUJB2CaQy7cva5VXL0u/Mf6/UqUUhXHQ5C/7406cOuaZgRMhujY+f3tlY1oonepu3pDgffeITq2WhKMu8mjBwr8NW+0oFUakUw86wGUhRjBHes68cz21CzjjNbWHBG0ihRdwmTMoPu5uAjnT0jfMhtMRY4TVx6Xb+jyUUQuqvHgB3NLlVU1HHBk544FxfDQhE9yEd7jxOwo7hBrlaNCmSRVl/VwEY7Jc6biqiSBbuoLu7rxw780iXEkQjYUMdNhhX1PvKD/2518hyv2fd+yuhG1/vgATMbg/WcgXzIJ+0WvW9UkFrHcHkfxM408M/ubb1vbhm1UdItk1vzIMLPKEg0dD41PT8lHsUNaugTLS1NEpubGcOMppcM+QfcMsZQun5qrXNuPICKn3GSxTosTdMAeuaTnmiUiKuKoFo0Yogs0mN9v64wPTiaAYqcGUY7gPW77Y9nuXNuC2oAIdypucP/IplVLQUS8M+rE7WIZW13Db1QL2nBQItbJqdXZH+3D0kXkhyeXqEGIw8E3ZhZhrFda3kGuJ0Vu6RgvFlcfxeuLMVRGrGXmBRua/n9brgwUx8F9VwPTdDIYObU0WXkZW1Wr988NxB+O4oH3UuzzkeuyHKrA8AOH3JNJV8RIwex0S1b9taEPz9WKCzwwEiEZImB8Jv70yWMVejTIf/p7Ne8T5jX+2hzBq3XJRncPxVlw0T/ebNZpyZd/WpUHs0tT6xz/UEzFP4rpz3iwtc0B9IRiqM51pjTyOZjdUpiW7+7F8h3deLspoNZuqhJXkiNkVuFO36/tFZdgQJ8EM52DBt84wXr/Urek81ZjH16p7Qb3N6VpHBGDgG63lTrCPkd+741GuY/+e5ETfOH/tqAsLfe9U+6JI3Pt+qYlvEa6PC/vbke3ubvDeMNPTsjBxPzEBbRXKvt/b9kft5gsWGEUMBb+iyfm4MppeSru8JRSydNYRN6/WEBs6VPsaGcZZszgBTVe/MM0Hy6VNBdWeCSvo9glaUbZr2rBNY04PZhZ7Mb0QYNef6rvxW8+6LVssbAyO+R6s0s9OFcar6VyX/PLXKjO1uAPRVTcoMLyc1IlgEun5KFa78N+RupIXZ/8Pd8fC5qVQkn6vzvwEEHKkmz/wrR8/NuaVklXvmfSp0fRKhAF/PzkXFw3pwjflLp1xXEFyks6vsitwloag3yXxvlOgW/u8ePiyXkqfMsIy3FoP5rjw5enF+hnjOEruG99K5Zt6UEvAyL7C7FcZ2axE7eJKzW9OPUVMR/Z1IH7N3Zhv3rZeprywo/J09TgwZwE8UeJ2CCt7KUv7NHNWB5inYkg/vc5lZirx/AYEZNrMqbrl1u6VPQ3XAOEJxTEvMpsXD+nELMsNADt/ggueLYOLUG5Dyl4HJCYV+LEY+eOUeXIKiv39qp7YkctRY0boOS6nZhRkqWCNM+tyZH0UkhQyGQc2tJnaqEMG7PCKxV9ok/D3YvKE66a+qK4dTesblOjf1b7lShmY3Id+NmpZQlj1Jh3//JmC1oDcm9mlVTqwlemZuF7c0v0M3G+JXn1SkPEktXCij6ryCmVvBCnSHkbXEl7QlG8sKsHd7/bjlbVR2VFJCUN+W9gHBpn4XSHpSTo7trT2zvxxA6/8fsU4fqx1KUTigflk6TNyJE+aZy+9GoDwpzi0F8HE8BnnFPswG3zy1QAfCI4enz/+1KnN+yXe5R3mVS8Nbikfj51XhVmmtSp9JtvA365sR3/udmPHhWTEh+1Ugc7VttjuHpFI/b1WiuM/Ty+pQN3vNuFDlH1Q9KUArStM4qrX2/CdnmBVqgUiy6PjZhuKbJCHl/kEkE0FzNO3bh5TQtufKdTiZmK0em/Fx4i2m+1hPGVPzerCHEzirKcOJNi029+i/VxwQSfZTFjobhldTOu+nMTXt4bQkNfVFlEDHJmVPjKhiCuWdWqOnA5HD6qkcqU54rhnsWJxYycP96Hm08plq+y6FrIJMnPXHnX9ywqTShm5LTqHNy5sMRwkOYgMdR1HyownGr2dpMIhQXhoSdwVpULj5xVgfmVOQktDo7yM4bz12dXoCaXbml6UQOTCtyYWeLBrDIRUDnG5Us6Kt+M0DCtMP79Q45Sr3ondP1opRqKmZTlEwodePCMiqRiRjxODdfOLsJV06W8G7qfMUTk89csuJ0ZF7TdnSGxzjpEvOTlJrD82GFaH3Lh3ndb9TPmUPzuWd8eH7ZO0CpTKFvFavvZOy0G2XwQFpg8OQ5EQzudWFKTq/rQzFgmYv3kThExuSbF7KNIiygi2ykW1/febEZDr3GLT84a55Pnku9J4fZEaeFZC1Phs97+dgse301h1a2D/paOh/xb3WdI3PS9UXxXRI0COFphob5MrEkGWBvxyYk+LCpnH5hRJYjD71wqrqvZANPpY3KxdCwn+Jt4DlKmWzl9aACcF9oRksJj5umIMHHQ6ScLyy2F4Uwr8urB0PJLf1kdAmyMrZAsiJ6B83/Z22NsxUoZznZEceu8UhRaHNBiGNG0PPlTzhZIhpTvv4qBYPYIGRe0F8Ql6HFIoTBwB1jxXqrtUX09Vnh+p7iZMfM0X9/bp+ZwmsG5YjkqvowKJodU+gUWRIQT3u97f3/8hbLPhZ23SQ6a0E2xbDwsJrUZM4o9yHdG1QsdK6WXkf1WeEXy8MntYhX6zYI3RWT9PXitKabc9lGJVAR3NIQLJ0nJtgD7U0yRNF2S5qcspnnJ1PwkjdRAYsrtGggFDUaVUYel7VqpvKkMOrHb4stS21Od5TIcNPSE432YIi7JYIPA/tTjkljYich2OdQMGKPQAhozOzqDal6wERkXtDUNfYbCo5CWrEssqg0t1iKv39pnIU2p0CExhd9tTjH+RzKRIy/j881FhC5kSW4WxmRFMVYafMMjR8MYVwAb2vxqnpsRpdlceSC+FNK4PI8yxc3gBPyHN7THa4klu1S+JcL9KxE0s0IxEnBgoSrbgQmqL8CcE6Sic8UTilZSxL0qz9IwycDtGch0sYgKxeU1d8sOhau7mMGO7Uk+TSzB1IPEP3dMPnwaLfiMV9eUqOsOoZexggb9oIxJ4wT1H/ylSTyURkvH9+W7nMxuOBNCrslsrk8wwjyQjOZQSNyZfaLipuJDpABzxNIMekh7euR7Jua8Qgr3LlHxVNA0J8pFUAosuACXiFXw/AUVWH7ROEvHcxeOwa/Ork64IsdAuCRLTV7csqu2aJ3REt0olkFKQY3yXtqjbqwSS3bUIZW1NMdlec5ikdeBfMYQGFRyvtsySZMB31ZgN0SRWE8fmZhtQgv7g00GO2jBzSnLstRYDYbLQ00vEheb1v8I0ih129AtJFLG3mmN4H93h/B0rbXjD/JdNfhk1i7LtdlHbERGBS0Qiap4HI4CWqHPwixp+v3x6SYW0pSvMJQiJaQ15vpsVkJJaBqz/y3Vw8oaUhXZUmClIhVbnOLEkJiwQywPi3l9ALkXjvKOOuS+clKo7JwozTXozEglnIdfVV0RFt7XQDgIY1o+JUnVaKXJODZ0KVqOmYbzZNWDmMBGln3l7Aaydsh3xXswRd7L4AGZwWQ0h6LysFzMzyoplpthI8dtvU9juMjn6oaCx2KZ59peaWWgvJ/2wOhzOYcLq43r8KPFR1HTJG7ZjWyFsbLizrAi12816XcfWckfJaQiwsMFrT/GjlnxrMlQgpPNghNtUsPaKq4xaYTSL2cdjJEb4XKalYK1O1yYlV1b0KSQcKXd0UIPe/stUMWxfJMg0ISIW1tjsePdxhoFtK7N+t2kpdrSnp6rz1CbrfxbhvaMIGrFabO+QoF9fWoKV8aPbNP9CGxBkxfU1hdGwIKocXSQw9acy1erfmbqCGFvd3zgg3siWIGxWgUuuWeGj1hGWrdwEAsszIawsU4VI3dNLAeGg6xrCVgOVRrIO0192MW11dJpwDIIByfiI5HJn5WxdufXZOGWE/Nw85zMHjfO9KoVsI3I6NQnTrn5xLO70cBoDJORTqZ57YxsXD3TeO4kBwQueLYWdb3mL5RpfnGiBz+eX6afSQwHGrjG/bZOSVPu0+eK4YVPjZeCadyB9eTWTty+bj8ckdRGUs3glCS2whGnGycVO/HkeWP0T4zh8jj/UxuGWljPAmo6SpGGx5eOseSyHs6pT7y3RWVO/Oqcav2MMQyFufCPtdgj5SJZWWOg7txSJ56Q57UCux4ueb4em/ZHk44e81mPK3DgmQtr9DPA7z/oxI/WdqoObiNoZXz1mCx896RS/Yw5tEiufHkPVrearTkm71P+Gzj1aTBcufeuDSbrzUkD+dg5lViQYJMUzr08/5k6tDNrkuW524PF5W788uyDe0McTmwLTSpYT9SldqYx47zxuSh1R+DXPGBDm/6hoS/qOPB7r7iZNBAZsLhrfxAd+sRyM9gYlDjDqpKZIgXVq0XxvRNLhtT/ZvNROCXIqRo543ylIHEC++t7uLGBNe5/r82CmB0euLMYPQOj8BHOEHqjIZj2EtpDxRY0Ii/ojT3msVmcd3mLWH9coUFzyEtlJ20qh6DmrEm5z2cAJ08d+EwOafVapfXb1m7NAuSMgn8/tRR57viS4cngNRm+cMvcIpxou5sZh0ujj8/j1oIm7r+8Z4Y1Xf9mK16uNRY1xnTeu74d974vlp+lyenDDz2Js7hZjEl/ITdjuUM8Gc5kSRda4P+7vdtwQc1E2IImsH/jz/U9apUDMzjn72cLS1Ca5VB7Hhj2YbFfRT6n2NAdZozTGZUecH+Aq2cVy7kEIuT0YEW9NReSLKrOwbIzyjG7hBXKHd9wub8TVW3m7MG0QhfuF5fwM1ONd+2ySQ+uKHwmZwBYsJTZbUIL/J9WNuH6VU1q9gknt7PLgd0r+3rDWL6rG5e/2oB7N3apLpH+xnA0cE6ND3maCKxRTFw0qhZC+M6bLfj5u+3yfNb6/lj/uPXjT99pw6eW1+OH7/rV/gypYAsakUJW73fgtTprLcrS8T787rwqfGmSV63coURLH4WJH/JvsYqcYnVVZmtYUuXGDXN8ePoTY/HQmRWqrymLbl+CjmSKK6eB9FoQ1364bNLjZ1di2Zll+Pq0LFw41q02VvmHKVm4b3EpnlpaqYTYZvj41OR8tbBAonf6EUSkIlLp/1gb3y3p/GfrVH9g/KgTIWjDOy1cAEHSG0ViRji6zsUBjDwChTwj13W7b3MPLn5+D256qxnP7ezG+hY/dnbGNxdf3xJQdW7Zxg58541GJWJ/92I9lm3rQ10P3exgyo9vC9oBYmqeo9kmGP3Q3bvxlBI1venxc6tw+9wCfPeEHHXcfGKeCEkJ/nDhOCz/ZA0eXFKJy6cXYBKXb9HhlLiEiLjWibi+lKKpzah5LoNz3Zxi3L24Aj8/vRLf505XNTmWp/7YpA93OVs6lhax+Rp4CqmpavNrEba2YAy7emLYLQdXaaGLOVrczER8dUYRCrSQasgNiYkoBf1idUbx2x0BXPfXdnz2+Xp84lkR7ufq5N91+ObKVvz0vR4srw/J87MR1+IzB9Ic0bVLug5HtTZ1AU9sTW01Cm44ckpFllq/6qvHF6njC9MK1Ma0xxV70lvOWwr5w9JqcbDA5uMDuxF8Vir6YMSaYQVWldhCnNdIM0Ya82tnFemDUdYsUjXFKRSQhlxDSIo1HRD+W4m3Pk0qHlc5tDJvC9oAYuEw/uv9zpT99nQwmpJDt/PDXodae83m4wMHB66ZWWBt1DlVrLiyh5HLpNG+qIZ9tikOMrHcDzwyjC1oA5GWpCus4fo3m+OrhgwjXk4jMXif7D94WExGK6ve2oweLj+2EBeNjQ/OZAqGSbi0zFf+ocAu4FvmlWFJuZbRZx0qtqANgibw9m5xH15vPLD79nCgVoEwaqHE9WB/3ndXtcQXEMwQUbnm2sZRuHzQEQLndd66oAznVemj4EO0rGgBzStz4/PHFJp3xB9muJLM3YvLxVKLT3VK2dW2QKrzrG1BSwA7azd2RHHFaw1YzQUrhwErK+WwT6U5CHxjRRPWNAzdUqOY3bG2Db/ZMjJBj0cLXGjgrkUVuHKKF1y4UsUepoqDgpiNhSUO3HtGpdokezgEY6hwI55/X1iOG+bkqf5k5YKmuJ7cR4nvo8AwpFQXUrAFLQnswOTQ8Vf+1KjiYlr91qL3rcAVTl9lrJm4uGbQYmzojeJrK5rVKGyQ++SlQWNvfKfrx3ZHMRyDnlyfzOYgXO7nB3NL8MDp5Ti2UI9FpIVlWNmlInNit1h2+V4XvjU9Bw+eWamEIsjF9Pm3JsdILJdEzeEo/lPnVeIz473KclMxkCqIXO7LFElAvqdETAmihhlFbtw404OvHV+of8caDqurYLotKCVbplSKtZUF+hi0mEqaVqb18DtW6h9HPoPhCJZt68UlL+zBf73XrvZwTAfOc319by+uX9Wo0nqxzq86/63A7/WEIvjJu1340isNaj8GqyOgXHbmN1v343Mv7sHz9fHYJqtTn6xYkao0iyVZ4ElecGPyBhkhfyDgN8mR6gYuakm4BOkMPFJNk8KRKJ2BhxIXi5xVk4PfLq3CXfOLcFqFR8RJ8omipdKSSs+Kr8cuMlZ1vM+pLLvfy998e3aRKv8kS35yRyyu0pv4gPrpNljih3OYVeA1r5n0yDFd0SIZk/I9+MmCUvzhgjH49owczClxwSf3dHD1jUHX4u9K5KHKz8wiF74yNRtPLK1WsZN/P60g5SgBbdyvPoyZxbzQ9FtcGd8OK6m8SCb4JcOe3NqBLo7LmrQUTHN+mRtzK+VBDdLkonKPS4Xcz30rTYa0meYJRQ6cPjbXME365U9Kmm28T5OJ+QfQXYecWFDtDXhyeRaOK/GoFQiKs1zgQqckJv+jMdfuD4tlFVEry77f4pcjgH0B3qNLiUq6w/Oq1ZP7n+hzYGFVtgqqHZ/nRrFUFBZm7jVKa5LBi2sa+7BKRHRf0BkPCZCD1z8m34Fzx/uS55HAlp4bDe/sjsb/NhlqxV8NL15Ug9LsxI0jxWzFnj61a5B6AYlQS6FrmGdxahbT4ubO8dkdydJ0oDRLs7QBDmEqzK8Oo7ImafJ5F1en1xHeKGWCm31wlZXWvpDamNrr0sSldGNigRuTRZlo4QyGKzF3SSOW/I3F31lJFjfdTvwtboq9Yp80okbLtksD9XdT8zBhQMxkusjtYF9fRC2LXy/P2yLP2804O/mMs2Z8ko8lIlhjfW6MkTJcluVI2cUcjHbCo1tjHRYqthqKNrPmWA4YLMgnsQAF6JBNehORcpqSnoVh81hQ1CUdUVGmsVxDKiDXp3LKQcv0oKDF16/izkAx7l/Ifg+5d2WNWXAxraKek/nHZwgHVAvODmlqhtqVSLV8jPNJcF2Ks4VdhJTwmtwzW9j5JQ48muLGyDY2w4H2rVdrY8/VM/DNHvlKC1WL5RhYmZX2UomtifCQOaAk/T8P37XpStxxcgEumZKnn7GxGTkcVxxXBI/aDiy5721jAIWDVhIt3P6Dvx8uMSPqHvT7OIzXpvs7KTuC8yYYL7pnY3O4cJxQ4sXl3MhUWlobG8tIA+jQYvje3FLkWtx6zsZmuFFm2TWzinBaqaZGOGxsTBEXl31w35rhS7pKrY3NSKAEjZ3ad59WfnAawwjv/2czelHBjk4X/vHYHGkIS/SzNjajA80f4Bh1HAZtPrihA49s6UJ3zA1uiMBloRHv5bY5WqFFpkZVXRiXFcN3ZhfiArvfzGbUAfw/50MuBhwlxE8AAAAASUVORK5CYII=" alt="" width="280px" height="48px"></p>-->

  <div class="row">
	<div class="col-75">
    <div class="container">
	    <!-- <div class="col-md-6 col-md-offset-3">-->
            <!--<div class="panel panel-default credit-card-box">-->
                <div class="panel-heading display-table" >
                    <div class="row display-tr" >
                        <h3 class="panel-title display-td" >Payment Details</h3>
                        <div class="display-td" >                            
                            <img class="img-responsive pull-right" src="https://test-app.medmate.com.au/accepted_c22e0.png">
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">
  
                    @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif
  
                    <form role="form" action="{{ route('stripe.post') }}" method="post" class="require-validation"
                                                     data-cc-on-file="false"
                                                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                    id="payment-form">
                        @csrf
  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                    class='form-control'  size='4' type='text'>
									<input type='hidden' name='amount' value='{{$amount}}'/> 
									<input type='hidden' name='order_id' value='{{$order_id}}'/> 
                            </div>
                        </div>  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' maxlength='19'  size='19'
                                    type='text'>
                            </div>
                        </div>  
                        <label class='control-label'>Expiry Date</label>
                        <div class='form-row row'>
                          <div class='col-xs-12 col-md-4 form-group w-25 pr-1 expiration required'>
                                 <input
                                    class='form-control card-expiry-month' placeholder='MM' maxlength='2'  size='2'
                                    type='text'>
                            </div>
						
							<div class='col-xs-12 col-md-4 form-group w-25 pl-1 expiration required'>  
                                 <input
                                    class='form-control card-expiry-year' placeholder='YYYY' maxlength='4' size='4'
                                    type='text'>
                            </div>

                            <div class='col-xs-12 col-md-4 form-group w-25 mt-27 cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='311' maxlength='4'  size='4'
                                    type='text'>
                            </div> 
							                           
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
  
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
                            </div>
                        </div>
                          
                    </form>
                </div>
            <!--</div>-->        
       <!-- </div>-->
    </div>
	</div>
	
   
  </div>
<!--</div>-->
  
</body>
  
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$('.card-number').on('keyup', function(e){
        var val = $(this).val();
        var newval = '';
        val = val.replace(/\s/g, '');
        for(var i = 0; i < val.length; i++) {
            if(i%4 == 0 && i > 0) newval = newval.concat(' ');
            newval = newval.concat(val[i]);
        }
        $(this).val(newval);
    });
var counter=0;
$(function() {
    var $form         = $(".require-validation");
$('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
			// token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
			$form.get(0).submit();
			$('.btn-primary').hide();
			}
    }
  
});
</script>
</html>