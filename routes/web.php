<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('test', 'HomeController@checkaddress');
Route::get('rejectorder/{orderid}','HomeController@rejectorder');
Route::get('stripe/{amount}/{orderid}', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
Route::get('/', 'Pharmacy\LoginController@index')->name('root');
Route::get('verifyaccount/{token}', 'Auth\LoginController@verifyNewAccount')->name('newuser_verify'); 
Route::get('emailverified', 'HomeController@emailVerified')->name('emailverified'); 
Route::get('stripe/{amount}/{orderid}', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
Route::get('success','PaymentCallBackController@paymentsuccess')->name('success');
Route::get('failure','PaymentCallBackController@paymentfailure')->name('failure');
Route::get('cancel','PaymentCallBackController@paymentcancel')->name('cancel');
Route::get('resetpass/{token}','HomeController@resetpass')->name('resetpass');
Route::post('resetPassword','HomeController@resetPassword')->name('resetPassword');
// Route::get('file-upload', 'UploadPdfController@fileUpload')->name('file.upload');
// Route::post('file-upload', 'UploadPdfController@fileUploadPost')->name('file.upload.post');
// Route::get('getorderdetails/{orderId}', 'OrderDetailstoPharmacyController@getorderdetails');
// Route::get('privacy', 'HomeController@privacy');
 Route::get('termsandconditions', 'HomeController@termsandconditions');
// Route::get('testmail','HomeController@basic_email');

Route::prefix('pharmacy')->group(function(){
	Route::get('terms','Pharmacy\LoginController@terms')->name('terms');
	Route::get('index','Pharmacy\LoginController@index')->name('index'); 
	Route::post('login','Pharmacy\LoginController@login')->name('login');	
	Route::get('forgot','Pharmacy\LoginController@forgot')->name('forgot');
    Route::post('forgetPassword','Pharmacy\LoginController@forgetPassword')->name('forgetPassword');
	Route::get('resetpass/{token}','HomeController@resetpass')->name('resetpass');
    Route::post('resetPassword','HomeController@resetPassword')->name('resetPassword');
	Route::group(['middleware' => ['auth']], function() {	
		Route::post('logout','Pharmacy\LoginController@logout')->name('logout');
		Route::get('settings','Pharmacy\LoginController@settings')->name('settings'); 
		
		Route::get('getorderdetails/{orderId}', 'Pharmacy\OrderDetailstoPharmacyController@getorderdetails');
		
		Route::get('getsearchorders', 'Pharmacy\OrdersController@getsearchorders')->name('getsearchorders');
		Route::get('getinstoreorders', 'Pharmacy\OrdersController@getinstoreorders')->name('getinstoreorders');
		Route::get('instoreorders', 'Pharmacy\OrdersController@instoreorders')->name('instoreorders');
		Route::get('searchorders', 'Pharmacy\OrdersController@searchorders')->name('searchorders');
		Route::get('changepharmacy/{orderid}', 'Pharmacy\OrdersController@changepharmacy')->name('changepharmacy');
		Route::post('updatepharmacy', 'Pharmacy\OrdersController@updatepharmacy')->name('updatepharmacy');
		Route::get('instoreorderscount', 'Pharmacy\OrdersController@instoreorderscount')->name('instoreorderscount');
		
		Route::get('getallorders', 'Pharmacy\OrdersController@getallorders')->name('getallorders');
		Route::get('allorders', 'Pharmacy\OrdersController@allorders')->name('allorders');
		
		Route::get('awaitingauthorisation', 'Pharmacy\OrdersController@awaitingauthorisation')->name('awaitingauthorisation');
		Route::get('getauthorisation', 'Pharmacy\OrdersController@getauthorisation')->name('getauthorisation');
		
		Route::get('readytoprocess', 'Pharmacy\OrdersController@readytoprocess')->name('readytoprocess');
		Route::get('getallprocess', 'Pharmacy\OrdersController@getallprocess')->name('getallprocess');
		
		Route::get('deliverylist', 'Pharmacy\OrdersController@deliverylist')->name('deliverylist');
		Route::get('getdeliverylist', 'Pharmacy\OrdersController@getdeliverylist')->name('getdeliverylist');
		
		Route::get('pickuplist', 'Pharmacy\OrdersController@pickuplist')->name('pickuplist');
		Route::get('getpickuplist', 'Pharmacy\OrdersController@getpickuplist')->name('getpickuplist');
		
		Route::get('transitlist', 'Pharmacy\OrdersController@transitlist')->name('transitlist');
		Route::get('gettransitlist', 'Pharmacy\OrdersController@gettransitlist')->name('gettransitlist');
		
		Route::get('completedorders', 'Pharmacy\OrdersController@completedorders')->name('completedorders');
		Route::get('getcompletedorders', 'Pharmacy\OrdersController@getcompletedorders')->name('getcompletedorders');
		
		Route::get('cancelledorders', 'Pharmacy\OrdersController@cancelledorders')->name('cancelledorders');
		Route::get('getcancelledorders', 'Pharmacy\OrdersController@getcancelledorders')->name('getcancelledorders');
		
		Route::get('editorder/{orderid}', 'Pharmacy\OrdersController@editorder')->name('editorder');
		Route::get('viewglobalsearchorders/{orderid}', 'Pharmacy\OrdersController@viewglobalsearchorders')->name('viewglobalsearchorders');
		Route::get('viewinstoreorders/{orderid}', 'Pharmacy\OrdersController@viewinstoreorders')->name('viewinstoreorders');
		Route::get('viewinboxorder/{orderid}', 'Pharmacy\OrdersController@viewinboxorder')->name('viewinboxorder');
		Route::get('viewauthenticatoinorder/{orderid}', 'Pharmacy\OrdersController@viewauthenticatoinorder')->name('viewauthenticatoinorder');
		Route::get('viewprocessorder/{orderid}', 'Pharmacy\OrdersController@viewprocessorder')->name('viewprocessorder');
		Route::get('viewdeliveryorder/{orderid}', 'Pharmacy\OrdersController@viewdeliveryorder')->name('viewdeliveryorder');
		Route::get('viewpickuporder/{orderid}', 'Pharmacy\OrdersController@viewpickuporder')->name('viewpickuporder');
		Route::get('viewtransitorder/{orderid}', 'Pharmacy\OrdersController@viewtransitorder')->name('viewtransitorder');
		Route::get('viewcompletedorder/{orderid}', 'Pharmacy\OrdersController@viewcompletedorder')->name('viewcompletedorder');
		Route::get('viewcanelledorder/{orderid}', 'Pharmacy\OrdersController@viewcanelledorder')->name('viewcanelledorder');
		Route::get('changestatus/{orderstatus}/{orderid}/{userid}', 'Pharmacy\OrdersController@changestatus')->name('changestatus');
		Route::post('updateeditorder', 'Pharmacy\OrdersController@updateeditorder')->name('updateeditorder');
		Route::post('ajaxGetItemsfromCatalogue', 'Pharmacy\OrdersController@ajaxGetItemsfromCatalogue')->name('ajaxGetItemsfromCatalogue');
		Route::post('ajaxGetItemsfromCataloguedata', 'Pharmacy\OrdersController@ajaxGetItemsfromCataloguedata')->name('ajaxGetItemsfromCataloguedata');
		Route::post('ajaxGetItemsfromCatalogueOTC', 'Pharmacy\OrdersController@ajaxGetItemsfromCatalogueOTC')->name('ajaxGetItemsfromCatalogueOTC');
		Route::post('ajaxGetItemsfromCatalogueOTCdata', 'Pharmacy\OrdersController@ajaxGetItemsfromCatalogueOTCdata')->name('ajaxGetItemsfromCatalogueOTCdata');
		Route::get('totalorderscount', 'Pharmacy\OrdersController@totalorderscount')->name('totalorderscount');		
		Route::get('neworderscount', 'Pharmacy\OrdersController@neworderscount')->name('neworderscount');
		Route::get('paymentorderscount', 'Pharmacy\OrdersController@paymentorderscount')->name('paymentorderscount');
		Route::get('cancelledorderscount', 'Pharmacy\OrdersController@cancelledorderscount')->name('cancelledorderscount');
		Route::get('cancelOrder/{orderid}/{userid}', 'Pharmacy\OrdersController@cancelOrder')->name('cancelOrder');
		Route::post('updatedeliveryaddress', 'Pharmacy\OrdersController@updatedeliveryaddress')->name('updatedeliveryaddress');
		Route::get('inboxorderscount', 'Pharmacy\OrdersController@inboxorderscount')->name('inboxorderscount');
		Route::get('authoraisecount', 'Pharmacy\OrdersController@authoraisecount')->name('authoraisecount');
		Route::get('processcount', 'Pharmacy\OrdersController@processcount')->name('processcount');
		Route::get('deliverycount', 'Pharmacy\OrdersController@deliverycount')->name('deliverycount');
		Route::get('pickupcount', 'Pharmacy\OrdersController@pickupcount')->name('pickupcount');
		Route::get('transitcount', 'Pharmacy\OrdersController@transitcount')->name('transitcount');
		Route::get('completedcount', 'Pharmacy\OrdersController@completedcount')->name('completedcount');
		Route::get('cancelledcount', 'Pharmacy\OrdersController@cancelledcount')->name('cancelledcount');
		Route::post('updatedriverinstruction', 'Pharmacy\OrdersController@updatedriverinstruction')->name('updatedriverinstruction');
		Route::post('deliveryslot', 'Pharmacy\OrdersController@deliveryslot')->name('deliveryslot');
		Route::post('updatedeliveryslot', 'Pharmacy\OrdersController@updatedeliveryslot')->name('updatedeliveryslot');
		Route::post('calldoordashdelivery', 'Pharmacy\OrdersController@calldoordashdelivery')->name('calldoordashdelivery');
		Route::post('sendtomedview','Pharmacy\OrdersController@sendtomedview')->name('sendtomedview');
	
	
	});
});