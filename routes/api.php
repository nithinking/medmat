<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::prefix('v1')->group(function(){
    // Route::post('login', 'Api\AuthController@login');
    // Route::post('register', 'Api\AuthController@register');
    // Route::group(['middleware' => 'auth:api'], function() {
    // Route::get('getUser', 'Api\AuthController@getUser');
    // Route::post('updateProfile', 'Api\AuthController@updateProfile');
    // Route::post('addDeliveryAddress', 'Api\DeliveryaddressController@addDeliveryAddress');
	// Route::get('getDeliveryAddresses', 'Api\DeliveryaddressController@getDeliveryAddresses');
	// Route::post('updateDeviceDetails', 'Api\AuthController@updateDeviceDetails');
	// Route::get('getPharmacies', 'Api\pharmacyController@getPharmacies');
	// Route::post('logout', 'Api\AuthController@logout');
	// Route::post('addPrescriptions', 'Api\ScriptsController@addPrescriptions');
	// Route::get('getPrescriptions', 'Api\ScriptsController@getPrescriptions');
	// Route::post('placeOrder', 'Api\PlaceorderController@placeOrder');
    // });
// });
Route::prefix('v2')->group(function(){
	Route::post('login', 'Api\UserController@login');
    Route::post('register', 'Api\UserController@register');
    Route::post('forgotpassword', 'Api\UserController@forgotpassword');	
    Route::group(['middleware' => 'auth:api'], function() {
    Route::post('updateDeviceDetails', 'Api\UserController@updateDeviceDetails');
	Route::get('getUser', 'Api\UserController@getUser');
	Route::post('updateProfile', 'Api\UserController@updateProfile');
	Route::post('logout', 'Api\UserController@logout');
	Route::get('getPharmacies', 'Api\BusinessController@getPharmacies');
	Route::post('addDeliveryAddress', 'Api\DeliveryaddressController@addDeliveryAddress');
	Route::get('getDeliveryAddresses', 'Api\DeliveryaddressController@getDeliveryAddresses');
	Route::get('removeDeliveryAddresses', 'Api\DeliveryaddressController@removeDeliveryAddresses');
	Route::post('addPrescriptions', 'Api\ScriptsController@addPrescriptions');
	Route::get('getPrescriptions', 'Api\ScriptsController@getPrescriptions');
	Route::post('placeOrder', 'Api\PlaceorderController@placeOrder');
	Route::post('addtomedlist', 'Api\ScriptsController@addtomedlist');
	Route::get('getmedlist', 'Api\ScriptsController@getmedlist');
	Route::post('gplocations', 'Api\UserController@gplocations');
	Route::post('updategpinfo', 'Api\UserController@updategpinfo');
	Route::post('removemedlist', 'Api\ScriptsController@removemedlist');
	Route::post('editrepeatsinmedlist', 'Api\ScriptsController@editrepeatsinmedlist');
	Route::post('removePrescription', 'Api\ScriptsController@removePrescription');
	Route::post('addFamilyMember', 'Api\UserController@addFamilyMember');
	Route::post('updateFamilyMember', 'Api\UserController@updateFamilyMember');
	Route::post('addtoCart', 'Api\CartController@addtoCart');
	Route::get('getCartItems', 'Api\CartController@getCartItems');
	Route::post('removeCartItems', 'Api\CartController@removeCartItems');
	Route::post('updateCartItem', 'Api\CartController@updateCartItem');
	Route::get('getAllProfiles', 'Api\UserController@getAllProfiles');
	Route::post('precheckout','Api\PrecheckoutController@preCheckout');
	Route::post('specialproducts','Api\SpecialProductsController@SpecialProducts');
	Route::post('getSuburbs', 'Api\BusinessController@getSuburbs');
	Route::post('getDeliveryWindow', 'Api\BusinessController@getDeliveryWindow');
	Route::post('medisearch','Api\MedisearchController@medisearch');
	Route::get('myOrders','Api\PlaceorderController@myOrders');
	Route::get('myOrderSummary','Api\PlaceorderController@myOrderSummary');
	Route::post('myOrderItemUpdate','Api\PlaceorderController@myOrderItemUpdate');
	Route::post('myOrderItemRemove','Api\PlaceorderController@myOrderItemRemove');
	Route::post('cancelOrder','Api\PlaceorderController@cancelOrder');
	Route::post('storeitems','Api\StoreController@storeitems');
	Route::post('updatehealthprofile', 'Api\UserController@updatehealthprofile');
	Route::post('payment','Api\PaymentController@createinvoice');
	Route::post('paymentstatus','Api\PaymentController@paymentstatus');
	Route::post('medicalconditions', 'Api\UserController@medicalconditions');
	Route::post('allergies', 'Api\UserController@allergies');
	Route::post('storesearch','Api\StoreController@storesearch');
	Route::post('testpush','Api\UserController@testPush');
	Route::post('addmedlistfromscript','Api\ScriptsController@addmedlistfromscript');
	Route::post('scriptsearch','Api\ScriptsController@scriptsearch');
	Route::get('getNotifications','Api\UserController@getNotifications');
	Route::post('updatenotification','Api\UserController@updatenotification');
	Route::get('getFamilyMember','Api\UserController@getFamilyMember');
	Route::post('emailverification','Api\UserController@emailverification');
	Route::post('completeOrder','Api\PlaceorderController@completeOrder');
	Route::get('getNotificationsCount','Api\UserController@getNotificationsCount');
	Route::post('updatemedicareinfo','Api\UserController@updatemedicareinfo');
	Route::get('getprevoiuspharmacy','Api\BusinessController@getprevoiuspharmacy');
	Route::get('getspecialproductimage','Api\BusinessController@getspecialproductimage');
	Route::post('emptyCartItems','Api\CartController@emptyCartItems');
	Route::post('getbanners','Api\DeliveryaddressController@getbanners');
	Route::get('getautocompleteaddresses','Api\DeliveryaddressController@getautocompleteaddresses');
	Route::get('getitemschedulecount','Api\DeliveryaddressController@getitemschedulecount');
	Route::get('getscheduledinfo','Api\DeliveryaddressController@getscheduledinfo');
	Route::get('getitemschedulequestion','Api\DeliveryaddressController@getitemschedulequestion');
	Route::get('getnoscheduleinfo','Api\DeliveryaddressController@getnoscheduleinfo');
	Route::get('getdeliverypartners','Api\PlaceorderController@getdeliverypartners');
	Route::post('verifyaddress','Api\DeliveryaddressController@verifyaddress');
	Route::get('getpostcode','Api\DeliveryaddressController@getpostcode');
	Route::post('updatemiaccess','Api\ScriptsController@updatemiaccess');
	Route::post('updatedeviceinfo','Api\ScriptsController@updatedeviceinfo');
    });
});

Route::prefix('v3')->group(function(){
	Route::post('login', 'Api\V3\V3UserController@login');
    Route::post('register', 'Api\V3\V3UserController@register');
    Route::post('forgotpassword', 'Api\V3\V3UserController@forgotpassword');	
	Route::post('getlogs','Api\ConformanceController@getlogs');
	Route::get('getPharmacies', 'Api\V3\V3BusinessController@getPharmacies');
	Route::get('getdPharmacies', 'Api\V3\V3BusinessController@getdPharmacies');
    Route::group(['middleware' => 'auth:api'], function() {
    Route::post('updateDeviceDetails', 'Api\V3\V3UserController@updateDeviceDetails');
	Route::get('getUser', 'Api\V3\V3UserController@getUser');
	Route::post('updateProfile', 'Api\V3\V3UserController@updateProfile');
	Route::post('logout', 'Api\V3\V3UserController@logout');
	//Route::get('getdPharmacies', 'Api\V3\V3BusinessController@getdPharmacies');
	Route::post('addDeliveryAddress', 'Api\V3\V3DeliveryaddressController@addDeliveryAddress');
	Route::get('getDeliveryAddresses', 'Api\V3\V3DeliveryaddressController@getDeliveryAddresses');
	Route::get('removeDeliveryAddresses', 'Api\V3\V3DeliveryaddressController@removeDeliveryAddresses');
	Route::post('addPrescriptions', 'Api\V3\V3ScriptsController@addPrescriptions');
	Route::get('getPrescriptions', 'Api\V3\V3ScriptsController@getPrescriptions');
	Route::post('placeOrder', 'Api\V3\V3PlaceorderController@placeOrder');
	Route::post('addtomedlist', 'Api\V3\V3ScriptsController@addtomedlist');
	Route::get('getmedlist', 'Api\V3\V3ScriptsController@getmedlist');
	Route::post('gplocations', 'Api\V3\V3UserController@gplocations');
	Route::post('updategpinfo', 'Api\V3\V3UserController@updategpinfo');
	Route::post('removemedlist', 'Api\V3\V3ScriptsController@removemedlist');
	Route::post('editrepeatsinmedlist', 'Api\V3\V3ScriptsController@editrepeatsinmedlist');
	Route::post('removePrescription', 'Api\V3\V3ScriptsController@removePrescription');
	Route::post('addFamilyMember', 'Api\V3\V3UserController@addFamilyMember');
	Route::post('updateFamilyMember', 'Api\V3\V3UserController@updateFamilyMember');
	Route::post('addtoCart', 'Api\V3\V3CartController@addtoCart');
	Route::get('getCartItems', 'Api\V3\V3CartController@getCartItems');
	Route::post('removeCartItems', 'Api\V3\V3CartController@removeCartItems');
	Route::post('updateCartItem', 'Api\V3\V3CartController@updateCartItem');
	Route::get('getAllProfiles', 'Api\V3\V3UserController@getAllProfiles');
	Route::post('precheckout','Api\V3\V3PrecheckoutController@preCheckout');
	Route::post('specialproducts','Api\V3\V3SpecialProductsController@SpecialProducts');
	Route::post('getSuburbs', 'Api\V3\V3BusinessController@getSuburbs');
	Route::post('getDeliveryWindow', 'Api\V3\V3BusinessController@getDeliveryWindow');
	Route::post('medisearch','Api\V3\V3MedisearchController@medisearch');
	Route::get('myOrders','Api\V3\V3PlaceorderController@myOrders');
	Route::get('myOrderSummary','Api\V3\V3PlaceorderController@myOrderSummary');
	Route::post('myOrderItemUpdate','Api\V3\V3PlaceorderController@myOrderItemUpdate');
	Route::post('myOrderItemRemove','Api\V3\V3PlaceorderController@myOrderItemRemove');
	Route::post('cancelOrder','Api\V3\V3PlaceorderController@cancelOrder');
	Route::post('storeitems','Api\V3\V3StoreController@storeitems');
	Route::post('updatehealthprofile', 'Api\V3\V3UserController@updatehealthprofile');
	Route::post('payment','Api\V3\V3PaymentController@createinvoice');
	Route::post('paymentstatus','Api\V3\V3PaymentController@paymentstatus');
	Route::post('medicalconditions', 'Api\V3\V3UserController@medicalconditions');
	Route::post('allergies', 'Api\V3\V3UserController@allergies');
	Route::post('storesearch','Api\V3\V3StoreController@storesearch');
	Route::post('testpush','Api\V3\V3UserController@testPush');
	Route::post('addmedlistfromscript','Api\V3\V3ScriptsController@addmedlistfromscript');
	Route::post('scriptsearch','Api\V3\V3ScriptsController@scriptsearch');
	Route::get('getNotifications','Api\V3\V3UserController@getNotifications');
	Route::post('updatenotification','Api\V3\V3UserController@updatenotification');
	Route::get('getFamilyMember','Api\V3\V3UserController@getFamilyMember');
	Route::post('emailverification','Api\V3\V3UserController@emailverification');
	Route::post('completeOrder','Api\V3\V3PlaceorderController@completeOrder');
	Route::get('getNotificationsCount','Api\V3\V3UserController@getNotificationsCount');
	Route::post('updatemedicareinfo','Api\V3\V3UserController@updatemedicareinfo');
	Route::get('getprevoiuspharmacy','Api\V3\V3BusinessController@getprevoiuspharmacy');
	Route::get('getspecialproductimage','Api\V3\V3BusinessController@getspecialproductimage');
	Route::post('emptyCartItems','Api\V3\V3CartController@emptyCartItems');
	Route::post('getbanners','Api\V3\V3DeliveryaddressController@getbanners');
	Route::get('getautocompleteaddresses','Api\V3\V3DeliveryaddressController@getautocompleteaddresses');
	Route::get('getitemschedulecount','Api\V3\V3DeliveryaddressController@getitemschedulecount');
	Route::get('getscheduledinfo','Api\V3\V3DeliveryaddressController@getscheduledinfo');
	Route::get('getitemschedulequestion','Api\V3\V3DeliveryaddressController@getitemschedulequestion');
	Route::get('getnoscheduleinfo','Api\V3\V3DeliveryaddressController@getnoscheduleinfo');
	Route::get('getdeliverypartners','Api\V3\V3PlaceorderController@getdeliverypartners');
	Route::post('verifyaddress','Api\V3\V3DeliveryaddressController@verifyaddress');
	Route::get('getpostcode','Api\V3\V3DeliveryaddressController@getpostcode');
	Route::post('updatemiaccess','Api\V3\V3ScriptsController@updatemiaccess');
	Route::post('deactivateuser','Api\V3\V3ScriptsController@deactivateuser');
	Route::post('updatedeviceinfo','Api\V3\V3ScriptsController@updatedeviceinfo');
	Route::get('getpromodeliverylimit','Api\V3\V3PrecheckoutController@getpromodeliverylimit');
	Route::post('genescriptsearch','Api\V3\V3ScriptsController@genescriptsearch');
	Route::get('getlastselectaddress','Api\V3\V3DeliveryaddressController@getlastselectaddress');
	Route::post('removeAllCartItems','Api\V3\V3CartController@removeAllCartItems');
	Route::post('adduserlocation','Api\V3\V3UserController@adduserlocation');
	Route::get('getusercurrentlocation','Api\V3\V3UserController@getusercurrentlocation');
	Route::get('changecurrentlocation','Api\V3\V3UserController@changecurrentlocation');
	Route::post('addlastselectaddress','Api\V3\V3DeliveryaddressController@addlastselectaddress');
	Route::post('changeCartOwner','Api\V3\V3CartController@changeCartOwner');
    });
});



Route::prefix('v1')->group(function(){
    Route::post('login', 'Admin\LoginController@login');   
	Route::post('forgotpassword', 'Admin\LoginController@forgotpassword');	
	Route::get('getAllBusinesses', 'Admin\BusinessController@getAllBusinesses');
	Route::get('getAllDeliverypartners', 'Admin\LocationsController@getAllDeliverypartners');
	Route::get('getAllLocations', 'Admin\LocationsController@getAllLocations');
	Route::post('updateLocations', 'Admin\LocationsController@updateLocations');
     Route::group(['middleware' => 'auth:api'], function() {
			Route::post('logout', 'Admin\LoginController@logout');
			Route::post('addPharmacyUser', 'Admin\UsersController@addPharmacyUser');
			Route::get('getPharmacyUsers', 'Admin\UsersController@getPharmacyUsers');
			Route::post('deletePharmacyUser', 'Admin\UsersController@deletePharmacyUser');
			Route::post('updatePharmacyUser', 'Admin\UsersController@updatePharmacyUser');
			Route::post('addBusiness', 'Admin\BusinessController@addBusiness');
			Route::post('updateDeliveryPartners', 'Admin\LocationsController@updateDeliveryPartners');
			//Route::get('getAllBusinesses', 'Admin\BusinessController@getAllBusinesses');
			Route::post('deleteBusiness', 'Admin\BusinessController@deleteBusiness');
			Route::post('updateBusiness', 'Admin\BusinessController@updateBusiness');
			Route::post('addLocations', 'Admin\LocationsController@addLocations');
			// Route::get('getAllLocations', 'Admin\LocationsController@getAllLocations');
			Route::get('getlocdeliverypartners', 'Admin\LocationsController@getLocdeliverypartners');
			Route::post('deleteLocations', 'Admin\LocationsController@deleteLocations');
			// Route::post('updateLocations', 'Admin\LocationsController@updateLocations');			
			Route::post('addDeliveryTiming', 'Admin\LocationsController@addDeliveryTiming');
			Route::post('addDeliveryPartners', 'Admin\LocationsController@addDeliveryPartners');
			Route::post('updateDeliveryPartners', 'Admin\LocationsController@updateDeliveryPartners');
			Route::get('getAllDeliverypartners', 'Admin\LocationsController@getAllDeliverypartners');			
			Route::post('addGplocations', 'Admin\GplocationsController@addGplocations');
			Route::get('getAllGplocations', 'Admin\GplocationsController@getAllGplocations');
			Route::post('deleteGplocations', 'Admin\GplocationsController@deleteGplocations');
			Route::post('updateGplocations', 'Admin\GplocationsController@updateGplocations');			
			Route::post('addAllergies', 'Admin\AllergiesController@addAllergies');
			Route::get('getAllAllergies', 'Admin\AllergiesController@getAllAllergies');
			Route::post('deleteAllergies', 'Admin\AllergiesController@deleteAllergies');
			Route::post('updateAllergies', 'Admin\AllergiesController@updateAllergies');			
			Route::post('addMedicalconditions', 'Admin\MedicalconditionsController@addMedicalconditions');
			Route::get('getAllMedicalconditions', 'Admin\MedicalconditionsController@getAllMedicalconditions');
			Route::post('deleteMedicalconditions', 'Admin\MedicalconditionsController@deleteMedicalconditions');
			Route::post('updateMedicalconditions', 'Admin\MedicalconditionsController@updateMedicalconditions');			
			Route::post('addImmunisation', 'Admin\ImmunisatoinController@addImmunisation');
			Route::get('getAllImmunisation', 'Admin\ImmunisatoinController@getAllImmunisation');
			Route::post('deleteImmunisation', 'Admin\ImmunisatoinController@deleteImmunisation');
			Route::post('updateImmunisation', 'Admin\ImmunisatoinController@updateImmunisation');
			
			Route::post('addPathologylocation', 'Admin\PathologylocationsController@addPathologylocation');
			Route::get('getAllPathologylocation', 'Admin\PathologylocationsController@getAllPathologylocation');
			Route::post('deletePathologylocation', 'Admin\PathologylocationsController@deletePathologylocation');
			Route::post('updatePathologylocation', 'Admin\PathologylocationsController@updatePathologylocation');
     });
});
