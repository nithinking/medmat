<?php

use Illuminate\Database\Seeder;

class UserrolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('Userroles')->insert([
            [
                'roleType' => 'superAdmin',
                'roleStatus' => '1',
                'created_at' => new \dateTime,                
                'updated_at' => new \dateTime
            ],
			[   
                'roleType' => 'admin',
                'roleStatus' => '1',
                'created_at' => new \dateTime,                
                'updated_at' => new \dateTime
            ],
            [
                'roleType' => 'paharmacy',
                'roleStatus' => '1',
                'created_at' => new \dateTime,                
                'updated_at' => new \dateTime
            ],
			[
                'roleType' => 'user',
                'roleStatus' => '1',
                'created_at' => new \dateTime,                
                'updated_at' => new \dateTime
            ]
        ]);
    }
}
