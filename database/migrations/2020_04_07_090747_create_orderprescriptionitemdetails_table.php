<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderprescriptionitemdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderprescriptionitemdetails', function (Blueprint $table) {
            $table->bigIncrements('orderPrescriptionItemId');
            $table->bigInteger('orderId')->unsigned();
            $table->foreign('orderId')->references('orderId')->on('orderdetails')->onDelete('cascade');
            $table->bigInteger('orderPrescriptionId')->unsigned();
            $table->foreign('orderPrescriptionId')->references('orderPrescriptionId')->on('orderprescriptions')->onDelete('cascade');
            $table->string('drugname')->nullable();;
            $table->integer('drugquantity')->nullable();
            $table->text('drugspecification')->nullable();
            $table->boolean('isgeneric')->nullable();;
            $table->decimal('originalPrice',5,2)->nullable();
            $table->decimal('pharmacyPrice',5,2)->nullable();
            $table->boolean('drugStatus')->nullable();
            $table->text('drugInstructions')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderprescriptionitemdetails');
    }
}
