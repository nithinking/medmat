<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrugdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugdetails', function (Blueprint $table) {
            $table->bigIncrements('drugId');
            $table->string('drugName');
            $table->bigInteger('productCode');
            $table->text('section');
            $table->text('subsection');
            $table->text('pc');
            $table->text('p-text');
			$table->bigInteger('cmpcode')->unsigned();            
            $table->foreign('cmpcode')->references('cmpcode')->on('companyinformation')->onDelete('cascade');
            $table->bigInteger('packcode');
            $table->text('pbscode');
            $table->text('pbsprice');
            $table->text('price');
            $table->boolean('drugstatus');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugdetails');
    }
}
