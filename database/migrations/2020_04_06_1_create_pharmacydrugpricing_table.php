<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacydrugpricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacydrugpricing', function (Blueprint $table) {
            $table->bigIncrements('drugPriceId');
            $table->bigInteger('pharmacyId')->unsigned();            
            $table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');
            $table->bigInteger('drugId')->unsigned();            
            $table->foreign('drugId')->references('drugId')->on('drugdetails')->onDelete('cascade');
            $table->decimal('price',5,2)->nullable();
            $table->boolean('drugpricestatus')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacydrugpricing');
    }
}
