<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescriptionitemdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescriptionitemdetails', function (Blueprint $table) {
            $table->bigIncrements('prescriptionItemId');
            $table->bigInteger('prescriptionId')->unsigned();            
            $table->foreign('prescriptionId')->references('prescriptionId')->on('prescriptiondetails')->onDelete('cascade');
            $table->longtext('prescriptionguid')->nullable();
            $table->longtext('prescriptionitemguid')->nullable();
            $table->string('drugname')->nullable();
            $table->text('drugstrength')->nullable();
            $table->string('drugform')->nullable();
            $table->text('drugquantity')->nullable();
            $table->string('substitutionallowed')->nullable();
            $table->text('pbsitemno')->nullable();
            $table->text('pbsitemmc')->nullable();
            $table->text('amtitemno')->nullable();
            $table->integer('repeats')->nullable();
            $table->date('itemexpirydate')->nullable();
            $table->string('prescribertitle')->nullable();
            $table->string('prescriberfirstname')->nullable();
            $table->string('prescriberlastname')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescriptionitemdetails');
    }
}
