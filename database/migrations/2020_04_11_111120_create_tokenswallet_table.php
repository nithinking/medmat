<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTokenswalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokenswallet', function (Blueprint $table) {
            $table->bigIncrements('tokenId');
            $table->bigInteger('userId');			
            $table->bigInteger('createdBy')->unsigned();            
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');
			$table->longtext('tokenImage')->nullable();
			$table->longtext('tokenBackImage')->nullable();
			$table->bigInteger('prescriptionId')->unsigned();
			$table->foreign('prescriptionId')->references('prescriptionId')->on('prescriptiondetails')->onDelete('cascade');
			$table->boolean('isused')->nullable();
			$table->integer('repeats')->nullable();
			$table->boolean('tokenStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokenswallet');
    }
}
