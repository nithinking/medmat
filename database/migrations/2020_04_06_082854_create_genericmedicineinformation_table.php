<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenericmedicineinformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genericmedicineinformation', function (Blueprint $table) {
            $table->bigIncrements('genericMedicineId');
            $table->bigInteger('orderId')->unsigned();            
            $table->foreign('orderId')->references('orderId')->on('orderdetails')->onDelete('cascade');
            $table->bigInteger('pharmacyId')->unsigned();            
            $table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');
            $table->bigInteger('userId')->unsigned();            
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->string('MedicineName')->nullable();
            $table->string('BrandName')->nullable();
            $table->string('genericName')->nullable();
            $table->bigInteger('prescriptionId')->unsigned();            
            $table->foreign('prescriptionId')->references('prescriptionId')->on('prescriptiondetails')->onDelete('cascade');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genericmedicineinformation');
    }
}
