<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacy', function (Blueprint $table) {
            $table->bigIncrements('pharmacyId');
            $table->string('pharmacyName');
            $table->text('pharmacyAddress');
            $table->string('pharmacyPincode');
            $table->longtext('pharmacyLogo');
            $table->string('lattitude');
            $table->string('longitude');
            $table->string('rating')->nullable();
            $table->string('pharmacyEmail')->nullable();
            $table->string('pharmacyWebsite')->nullable();
            $table->string('pharmacyType')->nullable();
            $table->longtext('pharmacyServices')->nullable();
            $table->bigInteger('userId')->unsigned();            
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->text('pharmacyDescription')->nullable();
            $table->text('shortDescription')->nullable();
            $table->string('pharmacyPhone');
            $table->string('deliveryOption');
            $table->decimal('deliveryCharge',5,2);			
            $table->bigInteger('pharmacyGroupId')->unsigned();            
            $table->foreign('pharmacyGroupId')->references('pharmacyGroupId')->on('pharmacygroups')->onDelete('cascade');
            $table->boolean('phamacyStatus')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacy');
    }
}
