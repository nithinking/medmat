<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacypromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacypromotion', function (Blueprint $table) {
            $table->bigIncrements('promotionId');
            $table->bigInteger('pharmacyId')->unsigned(); 
			$table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');	
            $table->string('promotionCode');
            $table->text('promotionText')->nullable();;
            $table->longtext('promotionImage')->nullable();;
            $table->dateTime('promotionStartDate');
            $table->dateTime('promotionEndDate');
            $table->boolean('promotionStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacypromotion');
    }
}
