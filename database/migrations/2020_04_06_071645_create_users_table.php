<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
             $table->bigIncrements('id');
			$table->string('firstName');
			$table->string('lastName')->nullable();
			$table->longtext('profileImage');
			$table->date('dob');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('gender');
			$table->text('address');
			$table->string('mobile');
			$table->string('medicareNumber')->nullable();
			$table->string('medicareNumberPos')->nullable();
			$table->string('medicareNumberExpiry')->nullable();
			$table->string('heathcareCard')->nullable();
			$table->string('heathcareCardExpiry')->nullable();
			$table->string('veteransaffairsCard')->nullable();
			$table->string('veteransaffairsCardExpiry')->nullable();
			$table->string('ihi')->nullable();
			$table->string('ihiExpiry')->nullable();
			$table->string('dvano')->nullable();
			$table->string('dvanoExpiry')->nullable();
			$table->string('safetyNet')->nullable();
			$table->string('safetyNetExpiry')->nullable();
			$table->text('socGuid')->nullable();
			$table->boolean('emailVerified')->nullable();
			$table->bigInteger('userRoleType')->unsigned();
            $table->foreign('userRoleType')->references('roleId')->on('userroles')->onDelete('cascade');
			$table->integer('age')->nullable();
			$table->boolean('smoking')->nullable();
			$table->boolean('pregnant')->nullable();
			$table->dateTime('lastLoginDate')->nullable();
			$table->boolean('userStatus')->nullable();
			$table->text('hashCode');
			$table->string('deviceId')->nullable();
			$table->text('deviceToken')->nullable();
			$table->string('deviceType')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
