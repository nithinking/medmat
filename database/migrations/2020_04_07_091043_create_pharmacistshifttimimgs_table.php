<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacistshifttimimgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacistshifttimimgs', function (Blueprint $table) {
            $table->bigIncrements('shifttimingId');
            $table->bigInteger('pharmacyId')->unsigned();
            $table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');
			$table->text('shiftDayofTheWeek');
			$table->time('shiftStartTime');
			$table->time('shiftEndTime');
			$table->boolean('shiftTimingstatus');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacistshifttimimgs');
    }
}
