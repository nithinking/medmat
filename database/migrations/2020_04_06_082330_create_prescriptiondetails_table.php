<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescriptiondetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescriptiondetails', function (Blueprint $table) {
            $table->bigIncrements('prescriptionId');
            $table->bigInteger('userId')->unsigned();            
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->string('prescriptionType');
            $table->string('prescriptionTitle');
            $table->longtext('prescriptionImage')->nullable();
            $table->longtext('prescriptionBackImage')->nullable();
            $table->longtext('prescriptionguid')->nullable();
            $table->longtext('prescriptionitemguid')->nullable();
            $table->text('barcode')->nullable();
            $table->string('drugname')->nullable();
            $table->date('prescriptiondate')->nullable();
            $table->integer('repeats')->nullable();
            $table->date('expirydate')->nullable();
            $table->string('prescribertitle')->nullable();
            $table->string('prescriberfirstname')->nullable();
            $table->string('prescriberlastname')->nullable();
            $table->boolean('prescriptionStatus')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescriptiondetails');
    }
}
