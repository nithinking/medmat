<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderdetails', function (Blueprint $table) {
            $table->bigIncrements('orderId');
            $table->bigInteger('userId');
			$table->bigInteger('createdBy')->unsigned();            
            $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('pharmacyId')->unsigned();            
            $table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');
            $table->bigInteger('prescriptionId')->unsigned();            
            $table->foreign('prescriptionId')->references('prescriptionId')->on('prescriptiondetails')->onDelete('cascade');
            $table->bigInteger('pharmacistId')->nullable();
            $table->string('pharmacistName')->nullable();
            $table->decimal('originalPrice',5,2)->nullable();
            $table->decimal('insurancePrice',5,2)->nullable();
            $table->string('insuranceType')->nullable();
            $table->string('orderstatus');
            $table->decimal('orderTotal',5,2);
            $table->string('orderType')->nullable();
            $table->string('paymentStatus')->nullable();
            $table->string('paymentType')->nullable();
            $table->text('orderInstructions')->nullable();
            $table->boolean('isGeneric');
            $table->boolean('speakwithpharmacist');
            $table->date('orderDate');
            $table->string('transactionId')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderdetails');
    }
}
