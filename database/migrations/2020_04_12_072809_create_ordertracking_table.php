<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdertrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordertracking', function (Blueprint $table) {
            $table->bigIncrements('trackingId');
            $table->bigInteger('orderId')->unsigned(); 
			$table->foreign('orderId')->references('orderId')->on('orderdetails')->onDelete('cascade');
            $table->string('orderStatus');
            $table->text('orderRemarks');
            $table->dateTime('statusCreatedat');
            $table->dateTime('statusupdatedat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordertracking');
    }
}
