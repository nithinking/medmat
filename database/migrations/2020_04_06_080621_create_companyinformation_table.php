<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyinformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companyinformation', function (Blueprint $table) {
            $table->bigIncrements('cmpcode');
            $table->text('companyName')->nullable();
            $table->text('companyAbbrev')->nullable();
            $table->text('address1')->nullable();
            $table->text('address2')->nullable();
            $table->text('address3')->nullable();
            $table->text('postcode')->nullable();
            $table->text('phone')->nullable();
            $table->text('fax')->nullable();
            $table->text('toll_free')->nullable();
            $table->text('email')->nullable();
            $table->text('website')->nullable();
            $table->text('mail-order')->nullable();
            $table->text('subtitle')->nullable();
			$table->timestamps();
			
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyinformation');
    }
}
