<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('notificationId');
            $table->string('notificationType')->nullable();
            $table->text('notificationMessage')->nullable();
            $table->text('notificationLink')->nullable();
            $table->boolean('notificationStatus')->nullable();
            $table->datetime('notificationCreated')->nullable();
            $table->datetime('notificationUpdated')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
