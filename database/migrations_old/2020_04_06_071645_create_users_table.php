<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
             $table->bigIncrements('id');
			$table->string('firstName');
			$table->string('lastName');
			$table->text('profileImage');
			$table->date('dob');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('gender');
			$table->text('address');
			$table->string('mobile');
			$table->string('medicareNumber');
			$table->string('heathcareCard');
			$table->string('veteransaffairsCard');
			$table->string('ihi');
			$table->string('dvano');
			$table->text('socGuid');
			$table->boolean('emailVerified');
			$table->bigInteger('userRoleType')->unsigned();
            $table->foreign('userRoleType')->references('roleId')->on('userroles')->onDelete('cascade');
			$table->integer('age');
			$table->string('smoking');
			$table->string('pregnant');
			$table->dateTime('lastLoginDate');
			$table->boolean('userStatus');
			$table->text('hashCode');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
