<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacytimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacytimings', function (Blueprint $table) {
            $table->bigIncrements('timimgId');
            $table->bigInteger('pharmacyId')->unsigned();            
            $table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');
            $table->time('openTime');
            $table->time('closeTime');
            $table->string('dayofTheWeek');
            $table->boolean('timingstatus');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacytimings');
    }
}
