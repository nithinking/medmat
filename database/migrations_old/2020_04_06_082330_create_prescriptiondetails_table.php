<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescriptiondetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescriptiondetails', function (Blueprint $table) {
            $table->bigIncrements('prescriptionId');
            $table->bigInteger('userId')->unsigned();            
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->string('prescriptionType');
            $table->text('prescriptionImage');
            $table->text('prescriptionguid');
            $table->text('prescriptionitemguid');
            $table->text('barcode');
            $table->string('drugname');
            $table->date('prescriptiondate');
            $table->integer('repeats');
            $table->date('expirydate');
            $table->string('prescribertitle');
            $table->string('prescriberfirstname');
            $table->string('prescriberlastname');
            $table->boolean('prescriptionStatus');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescriptiondetails');
    }
}
