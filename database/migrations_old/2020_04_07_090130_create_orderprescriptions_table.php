<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderprescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderprescriptions', function (Blueprint $table) {
            $table->bigIncrements('orderPrescriptionId');
            $table->bigInteger('orderId')->unsigned();
            $table->foreign('orderId')->references('orderId')->on('orderdetails')->onDelete('cascade');
            $table->bigInteger('prescriptionId')->unsigned();
            $table->foreign('prescriptionId')->references('prescriptionId')->on('prescriptiondetails')->onDelete('cascade');
            $table->decimal('prescriptionTotal',5,2);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderprescriptions');
    }
}
