<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacyratingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacyrating', function (Blueprint $table) {
            $table->bigIncrements('ratingId');
            $table->bigInteger('userId')->unsigned();            
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->integer('rating')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacyrating');
    }
}
