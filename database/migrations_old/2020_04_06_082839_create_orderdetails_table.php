<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderdetails', function (Blueprint $table) {
            $table->bigIncrements('orderId');
            $table->bigInteger('userId')->unsigned();            
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('pharmacyId')->unsigned();            
            $table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');
            $table->bigInteger('prescriptionId')->unsigned();            
            $table->foreign('prescriptionId')->references('prescriptionId')->on('prescriptiondetails')->onDelete('cascade');
            $table->bigInteger('pharmacistId');
            $table->string('pharmacistName');
            $table->decimal('originalPrice',5,2);
            $table->decimal('insurancePrice',5,2);
            $table->string('insuranceType');
            $table->string('orderstatus');
            $table->decimal('orderTotal',5,2);
            $table->string('orderType');
            $table->string('paymentStatus');
            $table->string('paymentType');
            $table->text('orderInstructions');
            $table->date('orderDate');
            $table->string('transactionId');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderdetails');
    }
}
