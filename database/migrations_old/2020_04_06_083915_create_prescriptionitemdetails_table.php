<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescriptionitemdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescriptionitemdetails', function (Blueprint $table) {
            $table->bigIncrements('prescriptionItemId');
            $table->bigInteger('prescriptionId')->unsigned();            
            $table->foreign('prescriptionId')->references('prescriptionId')->on('prescriptiondetails')->onDelete('cascade');
            $table->text('prescriptionguid');
            $table->text('prescriptionitemguid');
            $table->string('drugname');
            $table->text('drugstrength');
            $table->string('drugform');
            $table->text('drugquantity');
            $table->string('substitutionallowed');
            $table->text('pbsitemno');
            $table->text('pbsitemmc');
            $table->text('amtitemno');
            $table->integer('repeats');
            $table->date('itemexpirydate');
            $table->string('prescribertitle');
            $table->string('prescriberfirstname');
            $table->string('prescriberlastname');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescriptionitemdetails');
    }
}
