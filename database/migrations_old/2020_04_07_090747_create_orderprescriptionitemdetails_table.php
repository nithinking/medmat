<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderprescriptionitemdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderprescriptionitemdetails', function (Blueprint $table) {
            $table->bigIncrements('orderPrescriptionItemId');
            $table->bigInteger('orderId')->unsigned();
            $table->foreign('orderId')->references('orderId')->on('orderdetails')->onDelete('cascade');
            $table->bigInteger('orderPrescriptionId')->unsigned();
            $table->foreign('orderPrescriptionId')->references('orderPrescriptionId')->on('orderprescriptions')->onDelete('cascade');
            $table->string('drugname');
            $table->integer('drugquantity');
            $table->text('drugspecification');
            $table->boolean('isgeneric');
            $table->decimal('originalPrice',5,2);
            $table->decimal('pharmacyPrice',5,2);
            $table->boolean('drugStatus');
            $table->text('drugInstructions');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderprescriptionitemdetails');
    }
}
