<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacy', function (Blueprint $table) {
            $table->bigIncrements('pharmacyId');
            $table->string('pharmacyName');
            $table->text('pharmacyAddress');
            $table->string('pharmacyEmail');
            $table->string('pharmacyWebsite');
            $table->bigInteger('pharmacyPhone');
            $table->string('deliveryOption');
            $table->decimal('deliveryCharge',5,2);
            $table->bigInteger('pharmacyGroupId')->unsigned();            
            $table->foreign('pharmacyGroupId')->references('pharmacyGroupId')->on('pharmacygroups')->onDelete('cascade');
            $table->boolean('phamacyStatus');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacy');
    }
}
