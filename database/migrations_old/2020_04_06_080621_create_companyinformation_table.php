<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyinformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companyinformation', function (Blueprint $table) {
            $table->bigIncrements('cmpcode');
            $table->text('companyName');
            $table->text('companyAbbrev');
            $table->text('address1');
            $table->text('address2');
            $table->text('address3');
            $table->text('postcode');
            $table->text('phone');
            $table->text('fax');
            $table->text('toll_free');
            $table->text('email');
            $table->text('website');
            $table->text('mail-order');
            $table->text('subtitle');
			$table->timestamps();
			
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyinformation');
    }
}
