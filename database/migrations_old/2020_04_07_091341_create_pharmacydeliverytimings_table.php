<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePharmacydeliverytimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacydeliverytimings', function (Blueprint $table) {
            $table->bigIncrements('deliveryId');
            $table->bigInteger('pharmacyId')->unsigned();
            $table->foreign('pharmacyId')->references('pharmacyId')->on('pharmacy')->onDelete('cascade');
            $table->string('deliveryType');
            $table->string('timings');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacydeliverytimings');
    }
}
